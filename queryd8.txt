d8:
  input: link
    obj/d8/async-hooks-wrapper.o
    obj/d8/d8-console.o
    obj/d8/d8-js.o
    obj/d8/d8-platforms.o
    obj/d8/d8.o
    obj/d8/d8-posix.o
    | libv8.so.TOC
    | libv8_libbase.so.TOC
    | libv8_libplatform.so.TOC
    | libicui18n.so.TOC
    | libicuuc.so.TOC
    | libc++.so.TOC
    || obj/v8_tracing.stamp
    || obj/build/win/default_exe_manifest.stamp
    || obj/v8_dump_build_config.stamp
    || obj/build/config/executable_deps.stamp
  outputs:
    obj/gn_all.stamp
    obj/v8_archive.stamp
    obj/v8_clusterfuzz.stamp
    obj/test/v8_perf.stamp
    obj/test/benchmarks/v8_benchmarks.stamp
    obj/test/debugger/v8_debugger.stamp
    obj/test/intl/v8_intl.stamp
    obj/test/message/v8_message.stamp
    obj/test/mjsunit/v8_mjsunit.stamp
    obj/test/mozilla/v8_mozilla.stamp
    obj/test/test262/v8_test262.stamp
    obj/test/wasm-js/v8_wasm_js.stamp
    obj/test/wasm-spec-tests/v8_wasm_spec_tests.stamp
    obj/test/webkit/v8_webkit.stamp
    obj/tools/v8_check_static_initializers.stamp
    obj/tools/jsfunfuzz/v8_jsfunfuzz.stamp
    :d8
    all
