#include "src/builtins/builtins-array-gen.h"
#include "src/builtins/builtins-bigint-gen.h"
#include "src/builtins/builtins-collections-gen.h"
#include "src/builtins/builtins-constructor-gen.h"
#include "src/builtins/builtins-data-view-gen.h"
#include "src/builtins/builtins-iterator-gen.h"
#include "src/builtins/builtins-promise-gen.h"
#include "src/builtins/builtins-promise.h"
#include "src/builtins/builtins-proxy-gen.h"
#include "src/builtins/builtins-regexp-gen.h"
#include "src/builtins/builtins-string-gen.h"
#include "src/builtins/builtins-typed-array-gen.h"
#include "src/builtins/builtins-utils-gen.h"
#include "src/builtins/builtins-wasm-gen.h"
#include "src/builtins/builtins.h"
#include "src/codegen/code-factory.h"
#include "src/heap/factory-inl.h"
#include "src/ic/binary-op-assembler.h"
#include "src/objects/arguments.h"
#include "src/objects/bigint.h"
#include "src/objects/elements-kind.h"
#include "src/objects/free-space.h"
#include "src/objects/js-break-iterator.h"
#include "src/objects/js-collator.h"
#include "src/objects/js-date-time-format.h"
#include "src/objects/js-display-names.h"
#include "src/objects/js-generator.h"
#include "src/objects/js-list-format.h"
#include "src/objects/js-locale.h"
#include "src/objects/js-number-format.h"
#include "src/objects/js-objects.h"
#include "src/objects/js-plural-rules.h"
#include "src/objects/js-promise.h"
#include "src/objects/js-regexp-string-iterator.h"
#include "src/objects/js-relative-time-format.h"
#include "src/objects/js-segment-iterator.h"
#include "src/objects/js-segmenter.h"
#include "src/objects/js-weak-refs.h"
#include "src/objects/objects.h"
#include "src/objects/ordered-hash-table.h"
#include "src/objects/property-array.h"
#include "src/objects/property-descriptor-object.h"
#include "src/objects/source-text-module.h"
#include "src/objects/stack-frame-info.h"
#include "src/objects/synthetic-module.h"
#include "src/objects/template-objects.h"
#include "src/torque/runtime-support.h"
#include "torque-generated/src/builtins/aggregate-error-tq-csa.h"
#include "torque-generated/src/builtins/array-copywithin-tq-csa.h"
#include "torque-generated/src/builtins/array-every-tq-csa.h"
#include "torque-generated/src/builtins/array-filter-tq-csa.h"
#include "torque-generated/src/builtins/array-find-tq-csa.h"
#include "torque-generated/src/builtins/array-findindex-tq-csa.h"
#include "torque-generated/src/builtins/array-foreach-tq-csa.h"
#include "torque-generated/src/builtins/array-from-tq-csa.h"
#include "torque-generated/src/builtins/array-isarray-tq-csa.h"
#include "torque-generated/src/builtins/array-join-tq-csa.h"
#include "torque-generated/src/builtins/array-lastindexof-tq-csa.h"
#include "torque-generated/src/builtins/array-map-tq-csa.h"
#include "torque-generated/src/builtins/array-of-tq-csa.h"
#include "torque-generated/src/builtins/array-reduce-right-tq-csa.h"
#include "torque-generated/src/builtins/array-reduce-tq-csa.h"
#include "torque-generated/src/builtins/array-reverse-tq-csa.h"
#include "torque-generated/src/builtins/array-shift-tq-csa.h"
#include "torque-generated/src/builtins/array-slice-tq-csa.h"
#include "torque-generated/src/builtins/array-some-tq-csa.h"
#include "torque-generated/src/builtins/array-splice-tq-csa.h"
#include "torque-generated/src/builtins/array-unshift-tq-csa.h"
#include "torque-generated/src/builtins/array-tq-csa.h"
#include "torque-generated/src/builtins/base-tq-csa.h"
#include "torque-generated/src/builtins/bigint-tq-csa.h"
#include "torque-generated/src/builtins/boolean-tq-csa.h"
#include "torque-generated/src/builtins/builtins-string-tq-csa.h"
#include "torque-generated/src/builtins/cast-tq-csa.h"
#include "torque-generated/src/builtins/collections-tq-csa.h"
#include "torque-generated/src/builtins/conversion-tq-csa.h"
#include "torque-generated/src/builtins/convert-tq-csa.h"
#include "torque-generated/src/builtins/console-tq-csa.h"
#include "torque-generated/src/builtins/data-view-tq-csa.h"
#include "torque-generated/src/builtins/finalization-registry-tq-csa.h"
#include "torque-generated/src/builtins/frames-tq-csa.h"
#include "torque-generated/src/builtins/frame-arguments-tq-csa.h"
#include "torque-generated/src/builtins/function-tq-csa.h"
#include "torque-generated/src/builtins/growable-fixed-array-tq-csa.h"
#include "torque-generated/src/builtins/ic-callable-tq-csa.h"
#include "torque-generated/src/builtins/ic-tq-csa.h"
#include "torque-generated/src/builtins/internal-coverage-tq-csa.h"
#include "torque-generated/src/builtins/internal-tq-csa.h"
#include "torque-generated/src/builtins/iterator-tq-csa.h"
#include "torque-generated/src/builtins/math-tq-csa.h"
#include "torque-generated/src/builtins/number-tq-csa.h"
#include "torque-generated/src/builtins/object-fromentries-tq-csa.h"
#include "torque-generated/src/builtins/object-tq-csa.h"
#include "torque-generated/src/builtins/promise-abstract-operations-tq-csa.h"
#include "torque-generated/src/builtins/promise-all-tq-csa.h"
#include "torque-generated/src/builtins/promise-all-element-closure-tq-csa.h"
#include "torque-generated/src/builtins/promise-any-tq-csa.h"
#include "torque-generated/src/builtins/promise-constructor-tq-csa.h"
#include "torque-generated/src/builtins/promise-finally-tq-csa.h"
#include "torque-generated/src/builtins/promise-misc-tq-csa.h"
#include "torque-generated/src/builtins/promise-race-tq-csa.h"
#include "torque-generated/src/builtins/promise-reaction-job-tq-csa.h"
#include "torque-generated/src/builtins/promise-resolve-tq-csa.h"
#include "torque-generated/src/builtins/promise-then-tq-csa.h"
#include "torque-generated/src/builtins/promise-jobs-tq-csa.h"
#include "torque-generated/src/builtins/proxy-constructor-tq-csa.h"
#include "torque-generated/src/builtins/proxy-delete-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-get-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-get-prototype-of-tq-csa.h"
#include "torque-generated/src/builtins/proxy-has-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-is-extensible-tq-csa.h"
#include "torque-generated/src/builtins/proxy-prevent-extensions-tq-csa.h"
#include "torque-generated/src/builtins/proxy-revocable-tq-csa.h"
#include "torque-generated/src/builtins/proxy-revoke-tq-csa.h"
#include "torque-generated/src/builtins/proxy-set-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-set-prototype-of-tq-csa.h"
#include "torque-generated/src/builtins/proxy-tq-csa.h"
#include "torque-generated/src/builtins/reflect-tq-csa.h"
#include "torque-generated/src/builtins/regexp-exec-tq-csa.h"
#include "torque-generated/src/builtins/regexp-match-all-tq-csa.h"
#include "torque-generated/src/builtins/regexp-match-tq-csa.h"
#include "torque-generated/src/builtins/regexp-replace-tq-csa.h"
#include "torque-generated/src/builtins/regexp-search-tq-csa.h"
#include "torque-generated/src/builtins/regexp-source-tq-csa.h"
#include "torque-generated/src/builtins/regexp-split-tq-csa.h"
#include "torque-generated/src/builtins/regexp-test-tq-csa.h"
#include "torque-generated/src/builtins/regexp-tq-csa.h"
#include "torque-generated/src/builtins/string-endswith-tq-csa.h"
#include "torque-generated/src/builtins/string-html-tq-csa.h"
#include "torque-generated/src/builtins/string-iterator-tq-csa.h"
#include "torque-generated/src/builtins/string-pad-tq-csa.h"
#include "torque-generated/src/builtins/string-repeat-tq-csa.h"
#include "torque-generated/src/builtins/string-replaceall-tq-csa.h"
#include "torque-generated/src/builtins/string-slice-tq-csa.h"
#include "torque-generated/src/builtins/string-startswith-tq-csa.h"
#include "torque-generated/src/builtins/string-substring-tq-csa.h"
#include "torque-generated/src/builtins/string-substr-tq-csa.h"
#include "torque-generated/src/builtins/symbol-tq-csa.h"
#include "torque-generated/src/builtins/torque-internal-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-createtypedarray-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-every-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-entries-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-filter-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-find-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-findindex-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-foreach-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-from-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-keys-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-of-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-reduce-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-reduceright-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-set-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-slice-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-some-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-sort-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-subarray-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-values-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-tq-csa.h"
#include "torque-generated/src/builtins/wasm-tq-csa.h"
#include "torque-generated/src/builtins/weak-ref-tq-csa.h"
#include "torque-generated/src/ic/handler-configuration-tq-csa.h"
#include "torque-generated/src/objects/allocation-site-tq-csa.h"
#include "torque-generated/src/objects/api-callbacks-tq-csa.h"
#include "torque-generated/src/objects/arguments-tq-csa.h"
#include "torque-generated/src/objects/cell-tq-csa.h"
#include "torque-generated/src/objects/code-tq-csa.h"
#include "torque-generated/src/objects/contexts-tq-csa.h"
#include "torque-generated/src/objects/data-handler-tq-csa.h"
#include "torque-generated/src/objects/debug-objects-tq-csa.h"
#include "torque-generated/src/objects/descriptor-array-tq-csa.h"
#include "torque-generated/src/objects/embedder-data-array-tq-csa.h"
#include "torque-generated/src/objects/feedback-cell-tq-csa.h"
#include "torque-generated/src/objects/feedback-vector-tq-csa.h"
#include "torque-generated/src/objects/fixed-array-tq-csa.h"
#include "torque-generated/src/objects/foreign-tq-csa.h"
#include "torque-generated/src/objects/free-space-tq-csa.h"
#include "torque-generated/src/objects/heap-number-tq-csa.h"
#include "torque-generated/src/objects/heap-object-tq-csa.h"
#include "torque-generated/src/objects/intl-objects-tq-csa.h"
#include "torque-generated/src/objects/js-array-buffer-tq-csa.h"
#include "torque-generated/src/objects/js-array-tq-csa.h"
#include "torque-generated/src/objects/js-collection-iterator-tq-csa.h"
#include "torque-generated/src/objects/js-collection-tq-csa.h"
#include "torque-generated/src/objects/js-generator-tq-csa.h"
#include "torque-generated/src/objects/js-objects-tq-csa.h"
#include "torque-generated/src/objects/js-promise-tq-csa.h"
#include "torque-generated/src/objects/js-proxy-tq-csa.h"
#include "torque-generated/src/objects/js-regexp-string-iterator-tq-csa.h"
#include "torque-generated/src/objects/js-regexp-tq-csa.h"
#include "torque-generated/src/objects/js-weak-refs-tq-csa.h"
#include "torque-generated/src/objects/literal-objects-tq-csa.h"
#include "torque-generated/src/objects/map-tq-csa.h"
#include "torque-generated/src/objects/microtask-tq-csa.h"
#include "torque-generated/src/objects/module-tq-csa.h"
#include "torque-generated/src/objects/name-tq-csa.h"
#include "torque-generated/src/objects/oddball-tq-csa.h"
#include "torque-generated/src/objects/ordered-hash-table-tq-csa.h"
#include "torque-generated/src/objects/primitive-heap-object-tq-csa.h"
#include "torque-generated/src/objects/promise-tq-csa.h"
#include "torque-generated/src/objects/property-array-tq-csa.h"
#include "torque-generated/src/objects/property-cell-tq-csa.h"
#include "torque-generated/src/objects/property-descriptor-object-tq-csa.h"
#include "torque-generated/src/objects/prototype-info-tq-csa.h"
#include "torque-generated/src/objects/regexp-match-info-tq-csa.h"
#include "torque-generated/src/objects/scope-info-tq-csa.h"
#include "torque-generated/src/objects/script-tq-csa.h"
#include "torque-generated/src/objects/shared-function-info-tq-csa.h"
#include "torque-generated/src/objects/source-text-module-tq-csa.h"
#include "torque-generated/src/objects/stack-frame-info-tq-csa.h"
#include "torque-generated/src/objects/string-tq-csa.h"
#include "torque-generated/src/objects/struct-tq-csa.h"
#include "torque-generated/src/objects/synthetic-module-tq-csa.h"
#include "torque-generated/src/objects/template-objects-tq-csa.h"
#include "torque-generated/src/objects/template-tq-csa.h"
#include "torque-generated/src/wasm/wasm-objects-tq-csa.h"
#include "torque-generated/test/torque/test-torque-tq-csa.h"
#include "torque-generated/third_party/v8/builtins/array-sort-tq-csa.h"

namespace v8 {
namespace internal {

const char* kBuiltinNameFilter_0(compiler::CodeAssemblerState* state_) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

    ca_.Bind(&block0);
  return "%TypedArray%.prototype.filter";
}

TF_BUILTIN(TypedArrayPrototypeFilter, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  Node* argc = Parameter(Descriptor::kJSActualArgumentsCount);
  TNode<IntPtrT> arguments_length(ChangeInt32ToIntPtr(UncheckedCast<Int32T>(argc)));
  TNode<RawPtrT> arguments_frame = UncheckedCast<RawPtrT>(LoadFramePointer());
  TorqueStructArguments torque_arguments(GetFrameArguments(arguments_frame, arguments_length));
  CodeStubArguments arguments(this, torque_arguments);
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = arguments.GetReceiver();
USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, JSTypedArray, UintPtrT> block15(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, JSTypedArray, UintPtrT> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, JSTypedArray, UintPtrT> block18(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, JSTypedArray, UintPtrT> block19(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, UintPtrT> block22(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, UintPtrT> block27(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, UintPtrT> block26(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, UintPtrT> block28(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, UintPtrT> block32(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, UintPtrT> block31(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, UintPtrT> block34(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, UintPtrT> block33(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, UintPtrT> block36(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, UintPtrT> block35(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, UintPtrT> block29(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, UintPtrT, FixedArray, FixedArray, IntPtrT, IntPtrT, HeapObject, IntPtrT, IntPtrT> block41(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, UintPtrT, FixedArray, FixedArray, IntPtrT, IntPtrT, HeapObject, IntPtrT, IntPtrT> block42(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, UintPtrT> block23(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, JSTypedArray, UintPtrT> block14(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, JSTypedArray, IntPtrT, IntPtrT> block47(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, JSTypedArray, IntPtrT, IntPtrT> block46(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, JSTypedArray, IntPtrT, IntPtrT> block49(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, JSTypedArray, IntPtrT, IntPtrT> block48(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, JSTypedArray, IntPtrT, IntPtrT> block51(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, JSTypedArray, IntPtrT, IntPtrT> block50(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    ca_.Goto(&block0);

  TNode<JSTypedArray> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 16);
    compiler::CodeAssemblerLabel label1(&ca_);
    tmp0 = Cast_JSTypedArray_1(state_, TNode<Context>{parameter0}, TNode<Object>{parameter1}, &label1);
    ca_.Goto(&block5);
    if (label1.is_used()) {
      ca_.Bind(&label1);
      ca_.Goto(&block6);
    }
  }

  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 17);
    CodeStubAssembler(state_).ThrowTypeError(TNode<Context>{parameter0}, MessageTemplate::kNotTypedArray, kBuiltinNameFilter_0(state_));
  }

  TNode<JSTypedArray> tmp2;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 19);
    compiler::CodeAssemblerLabel label3(&ca_);
    tmp2 = EnsureAttached_0(state_, TNode<JSTypedArray>{tmp0}, &label3);
    ca_.Goto(&block7);
    if (label3.is_used()) {
      ca_.Bind(&label3);
      ca_.Goto(&block8);
    }
  }

  if (block8.is_used()) {
    ca_.Bind(&block8);
    ca_.Goto(&block2);
  }

  TNode<IntPtrT> tmp4;
  TNode<UintPtrT> tmp5;
  TNode<IntPtrT> tmp6;
  TNode<Object> tmp7;
  TNode<JSReceiver> tmp8;
  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 22);
    tmp4 = FromConstexpr_intptr_constexpr_int31_0(state_, 32);
    tmp5 = CodeStubAssembler(state_).LoadReference<UintPtrT>(CodeStubAssembler::Reference{tmp2, tmp4});
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 25);
    tmp6 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp7 = CodeStubAssembler(state_).GetArgumentValue(TorqueStructArguments{TNode<RawPtrT>{torque_arguments.frame}, TNode<RawPtrT>{torque_arguments.base}, TNode<IntPtrT>{torque_arguments.length}}, TNode<IntPtrT>{tmp6});
    compiler::CodeAssemblerLabel label9(&ca_);
    tmp8 = Cast_Callable_1(state_, TNode<Context>{parameter0}, TNode<Object>{tmp7}, &label9);
    ca_.Goto(&block11);
    if (label9.is_used()) {
      ca_.Bind(&label9);
      ca_.Goto(&block12);
    }
  }

  TNode<IntPtrT> tmp10;
  TNode<Object> tmp11;
  if (block12.is_used()) {
    ca_.Bind(&block12);
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 26);
    tmp10 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp11 = CodeStubAssembler(state_).GetArgumentValue(TorqueStructArguments{TNode<RawPtrT>{torque_arguments.frame}, TNode<RawPtrT>{torque_arguments.base}, TNode<IntPtrT>{torque_arguments.length}}, TNode<IntPtrT>{tmp10});
    CodeStubAssembler(state_).ThrowTypeError(TNode<Context>{parameter0}, MessageTemplate::kCalledNonCallable, TNode<Object>{tmp11});
  }

  TNode<IntPtrT> tmp12;
  TNode<Object> tmp13;
  TNode<FixedArray> tmp14;
  TNode<IntPtrT> tmp15;
  TNode<IntPtrT> tmp16;
  TNode<JSTypedArray> tmp17;
  TNode<JSTypedArray> tmp18;
  TNode<BuiltinPtr> tmp19;
  TNode<UintPtrT> tmp20;
  if (block11.is_used()) {
    ca_.Bind(&block11);
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 29);
    tmp12 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp13 = CodeStubAssembler(state_).GetArgumentValue(TorqueStructArguments{TNode<RawPtrT>{torque_arguments.frame}, TNode<RawPtrT>{torque_arguments.base}, TNode<IntPtrT>{torque_arguments.length}}, TNode<IntPtrT>{tmp12});
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 34);
    std::tie(tmp14, tmp15, tmp16) = NewGrowableFixedArray_0(state_).Flatten();
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 35);
    std::tie(tmp17, tmp18, tmp19) = NewAttachedJSTypedArrayWitness_0(state_, TNode<JSTypedArray>{tmp2}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 40);
    tmp20 = FromConstexpr_uintptr_constexpr_int31_0(state_, 0);
    ca_.Goto(&block15, tmp14, tmp15, tmp16, tmp18, tmp20);
  }

  TNode<FixedArray> phi_bb15_10;
  TNode<IntPtrT> phi_bb15_11;
  TNode<IntPtrT> phi_bb15_12;
  TNode<JSTypedArray> phi_bb15_14;
  TNode<UintPtrT> phi_bb15_16;
  TNode<BoolT> tmp21;
  if (block15.is_used()) {
    ca_.Bind(&block15, &phi_bb15_10, &phi_bb15_11, &phi_bb15_12, &phi_bb15_14, &phi_bb15_16);
    tmp21 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{phi_bb15_16}, TNode<UintPtrT>{tmp5});
    ca_.Branch(tmp21, &block13, std::vector<Node*>{phi_bb15_10, phi_bb15_11, phi_bb15_12, phi_bb15_14, phi_bb15_16}, &block14, std::vector<Node*>{phi_bb15_10, phi_bb15_11, phi_bb15_12, phi_bb15_14, phi_bb15_16});
  }

  TNode<FixedArray> phi_bb13_10;
  TNode<IntPtrT> phi_bb13_11;
  TNode<IntPtrT> phi_bb13_12;
  TNode<JSTypedArray> phi_bb13_14;
  TNode<UintPtrT> phi_bb13_16;
  TNode<IntPtrT> tmp22;
  TNode<JSArrayBuffer> tmp23;
  TNode<BoolT> tmp24;
  if (block13.is_used()) {
    ca_.Bind(&block13, &phi_bb13_10, &phi_bb13_11, &phi_bb13_12, &phi_bb13_14, &phi_bb13_16);
    ca_.SetSourcePosition("../../src/builtins/typed-array.tq", 184);
    tmp22 = FromConstexpr_intptr_constexpr_int31_0(state_, 12);
    tmp23 = CodeStubAssembler(state_).LoadReference<JSArrayBuffer>(CodeStubAssembler::Reference{tmp17, tmp22});
    tmp24 = IsDetachedBuffer_0(state_, TNode<JSArrayBuffer>{tmp23});
    ca_.Branch(tmp24, &block18, std::vector<Node*>{phi_bb13_10, phi_bb13_11, phi_bb13_12, phi_bb13_14, phi_bb13_16}, &block19, std::vector<Node*>{phi_bb13_10, phi_bb13_11, phi_bb13_12, phi_bb13_14, phi_bb13_16});
  }

  TNode<FixedArray> phi_bb18_10;
  TNode<IntPtrT> phi_bb18_11;
  TNode<IntPtrT> phi_bb18_12;
  TNode<JSTypedArray> phi_bb18_14;
  TNode<UintPtrT> phi_bb18_16;
  if (block18.is_used()) {
    ca_.Bind(&block18, &phi_bb18_10, &phi_bb18_11, &phi_bb18_12, &phi_bb18_14, &phi_bb18_16);
    ca_.Goto(&block2);
  }

  TNode<FixedArray> phi_bb19_10;
  TNode<IntPtrT> phi_bb19_11;
  TNode<IntPtrT> phi_bb19_12;
  TNode<JSTypedArray> phi_bb19_14;
  TNode<UintPtrT> phi_bb19_16;
  TNode<JSTypedArray> tmp25;
  TNode<Numeric> tmp26;
  TNode<Number> tmp27;
  TNode<Object> tmp28;
  TNode<BoolT> tmp29;
  if (block19.is_used()) {
    ca_.Bind(&block19, &phi_bb19_10, &phi_bb19_11, &phi_bb19_12, &phi_bb19_14, &phi_bb19_16);
    ca_.SetSourcePosition("../../src/builtins/typed-array.tq", 185);
    tmp25 = (TNode<JSTypedArray>{tmp17});
    ca_.SetSourcePosition("../../src/builtins/typed-array.tq", 190);
tmp26 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltinPointer(Builtins::CallableFor(ca_.isolate(),ExampleBuiltinForTorqueFunctionPointerType(1)).descriptor(), tmp19, TNode<Object>(), tmp25, phi_bb19_16));
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 52);
    tmp27 = Convert_Number_uintptr_0(state_, TNode<UintPtrT>{phi_bb19_16});
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 51);
    tmp28 = CodeStubAssembler(state_).Call(TNode<Context>{parameter0}, TNode<Object>{tmp8}, TNode<Object>{tmp13}, TNode<Object>{tmp26}, TNode<Object>{tmp27}, TNode<Object>{tmp17});
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 58);
    tmp29 = ToBoolean_0(state_, TNode<Object>{tmp28});
    ca_.Branch(tmp29, &block22, std::vector<Node*>{phi_bb19_10, phi_bb19_11, phi_bb19_12, phi_bb19_16}, &block23, std::vector<Node*>{phi_bb19_10, phi_bb19_11, phi_bb19_12, phi_bb19_16});
  }

  TNode<FixedArray> phi_bb22_10;
  TNode<IntPtrT> phi_bb22_11;
  TNode<IntPtrT> phi_bb22_12;
  TNode<UintPtrT> phi_bb22_16;
  TNode<BoolT> tmp30;
  if (block22.is_used()) {
    ca_.Bind(&block22, &phi_bb22_10, &phi_bb22_11, &phi_bb22_12, &phi_bb22_16);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 20);
    tmp30 = CodeStubAssembler(state_).IntPtrLessThanOrEqual(TNode<IntPtrT>{phi_bb22_12}, TNode<IntPtrT>{phi_bb22_11});
    ca_.Branch(tmp30, &block26, std::vector<Node*>{phi_bb22_10, phi_bb22_11, phi_bb22_12, phi_bb22_16}, &block27, std::vector<Node*>{phi_bb22_10, phi_bb22_11, phi_bb22_12, phi_bb22_16});
  }

  TNode<FixedArray> phi_bb27_10;
  TNode<IntPtrT> phi_bb27_11;
  TNode<IntPtrT> phi_bb27_12;
  TNode<UintPtrT> phi_bb27_16;
  if (block27.is_used()) {
    ca_.Bind(&block27, &phi_bb27_10, &phi_bb27_11, &phi_bb27_12, &phi_bb27_16);
    CodeStubAssembler(state_).FailAssert("Torque assert 'this.length <= this.capacity' failed", "src/builtins/growable-fixed-array.tq", 20);
  }

  TNode<FixedArray> phi_bb26_10;
  TNode<IntPtrT> phi_bb26_11;
  TNode<IntPtrT> phi_bb26_12;
  TNode<UintPtrT> phi_bb26_16;
  TNode<BoolT> tmp31;
  if (block26.is_used()) {
    ca_.Bind(&block26, &phi_bb26_10, &phi_bb26_11, &phi_bb26_12, &phi_bb26_16);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 21);
    tmp31 = CodeStubAssembler(state_).WordEqual(TNode<IntPtrT>{phi_bb26_11}, TNode<IntPtrT>{phi_bb26_12});
    ca_.Branch(tmp31, &block28, std::vector<Node*>{phi_bb26_10, phi_bb26_11, phi_bb26_12, phi_bb26_16}, &block29, std::vector<Node*>{phi_bb26_10, phi_bb26_11, phi_bb26_12, phi_bb26_16});
  }

  TNode<FixedArray> phi_bb28_10;
  TNode<IntPtrT> phi_bb28_11;
  TNode<IntPtrT> phi_bb28_12;
  TNode<UintPtrT> phi_bb28_16;
  TNode<IntPtrT> tmp32;
  TNode<IntPtrT> tmp33;
  TNode<IntPtrT> tmp34;
  TNode<IntPtrT> tmp35;
  TNode<IntPtrT> tmp36;
  TNode<IntPtrT> tmp37;
  TNode<BoolT> tmp38;
  if (block28.is_used()) {
    ca_.Bind(&block28, &phi_bb28_10, &phi_bb28_11, &phi_bb28_12, &phi_bb28_16);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 24);
    tmp32 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp33 = CodeStubAssembler(state_).WordSar(TNode<IntPtrT>{phi_bb28_11}, TNode<IntPtrT>{tmp32});
    tmp34 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{phi_bb28_11}, TNode<IntPtrT>{tmp33});
    tmp35 = FromConstexpr_intptr_constexpr_int31_0(state_, 16);
    tmp36 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp34}, TNode<IntPtrT>{tmp35});
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 13);
    tmp37 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp38 = CodeStubAssembler(state_).IntPtrGreaterThanOrEqual(TNode<IntPtrT>{phi_bb28_12}, TNode<IntPtrT>{tmp37});
    ca_.Branch(tmp38, &block31, std::vector<Node*>{phi_bb28_10, phi_bb28_12, phi_bb28_16}, &block32, std::vector<Node*>{phi_bb28_10, phi_bb28_12, phi_bb28_16});
  }

  TNode<FixedArray> phi_bb32_10;
  TNode<IntPtrT> phi_bb32_12;
  TNode<UintPtrT> phi_bb32_16;
  if (block32.is_used()) {
    ca_.Bind(&block32, &phi_bb32_10, &phi_bb32_12, &phi_bb32_16);
    CodeStubAssembler(state_).FailAssert("Torque assert 'this.length >= 0' failed", "src/builtins/growable-fixed-array.tq", 13);
  }

  TNode<FixedArray> phi_bb31_10;
  TNode<IntPtrT> phi_bb31_12;
  TNode<UintPtrT> phi_bb31_16;
  TNode<IntPtrT> tmp39;
  TNode<BoolT> tmp40;
  if (block31.is_used()) {
    ca_.Bind(&block31, &phi_bb31_10, &phi_bb31_12, &phi_bb31_16);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 14);
    tmp39 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp40 = CodeStubAssembler(state_).IntPtrGreaterThanOrEqual(TNode<IntPtrT>{tmp36}, TNode<IntPtrT>{tmp39});
    ca_.Branch(tmp40, &block33, std::vector<Node*>{phi_bb31_10, phi_bb31_12, phi_bb31_16}, &block34, std::vector<Node*>{phi_bb31_10, phi_bb31_12, phi_bb31_16});
  }

  TNode<FixedArray> phi_bb34_10;
  TNode<IntPtrT> phi_bb34_12;
  TNode<UintPtrT> phi_bb34_16;
  if (block34.is_used()) {
    ca_.Bind(&block34, &phi_bb34_10, &phi_bb34_12, &phi_bb34_16);
    CodeStubAssembler(state_).FailAssert("Torque assert 'newCapacity >= 0' failed", "src/builtins/growable-fixed-array.tq", 14);
  }

  TNode<FixedArray> phi_bb33_10;
  TNode<IntPtrT> phi_bb33_12;
  TNode<UintPtrT> phi_bb33_16;
  TNode<BoolT> tmp41;
  if (block33.is_used()) {
    ca_.Bind(&block33, &phi_bb33_10, &phi_bb33_12, &phi_bb33_16);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 15);
    tmp41 = CodeStubAssembler(state_).IntPtrGreaterThanOrEqual(TNode<IntPtrT>{tmp36}, TNode<IntPtrT>{phi_bb33_12});
    ca_.Branch(tmp41, &block35, std::vector<Node*>{phi_bb33_10, phi_bb33_12, phi_bb33_16}, &block36, std::vector<Node*>{phi_bb33_10, phi_bb33_12, phi_bb33_16});
  }

  TNode<FixedArray> phi_bb36_10;
  TNode<IntPtrT> phi_bb36_12;
  TNode<UintPtrT> phi_bb36_16;
  if (block36.is_used()) {
    ca_.Bind(&block36, &phi_bb36_10, &phi_bb36_12, &phi_bb36_16);
    CodeStubAssembler(state_).FailAssert("Torque assert 'newCapacity >= this.length' failed", "src/builtins/growable-fixed-array.tq", 15);
  }

  TNode<FixedArray> phi_bb35_10;
  TNode<IntPtrT> phi_bb35_12;
  TNode<UintPtrT> phi_bb35_16;
  TNode<IntPtrT> tmp42;
  TNode<FixedArray> tmp43;
  if (block35.is_used()) {
    ca_.Bind(&block35, &phi_bb35_10, &phi_bb35_12, &phi_bb35_16);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 16);
    tmp42 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 17);
    tmp43 = ExtractFixedArray_0(state_, TNode<FixedArray>{phi_bb35_10}, TNode<IntPtrT>{tmp42}, TNode<IntPtrT>{phi_bb35_12}, TNode<IntPtrT>{tmp36});
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 21);
    ca_.Goto(&block29, tmp43, tmp36, phi_bb35_12, phi_bb35_16);
  }

  TNode<FixedArray> phi_bb29_10;
  TNode<IntPtrT> phi_bb29_11;
  TNode<IntPtrT> phi_bb29_12;
  TNode<UintPtrT> phi_bb29_16;
  TNode<IntPtrT> tmp44;
  TNode<IntPtrT> tmp45;
  TNode<IntPtrT> tmp46;
  TNode<Smi> tmp47;
  TNode<IntPtrT> tmp48;
  TNode<IntPtrT> tmp49;
  TNode<IntPtrT> tmp50;
  TNode<UintPtrT> tmp51;
  TNode<UintPtrT> tmp52;
  TNode<BoolT> tmp53;
  if (block29.is_used()) {
    ca_.Bind(&block29, &phi_bb29_10, &phi_bb29_11, &phi_bb29_12, &phi_bb29_16);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 10);
    tmp44 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp45 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp46 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp47 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{phi_bb29_10, tmp46});
    tmp48 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp47});
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 10);
    tmp49 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp50 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{phi_bb29_12}, TNode<IntPtrT>{tmp49});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp51 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{phi_bb29_12});
    tmp52 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp48});
    tmp53 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp51}, TNode<UintPtrT>{tmp52});
    ca_.Branch(tmp53, &block41, std::vector<Node*>{phi_bb29_10, phi_bb29_11, phi_bb29_16, phi_bb29_10, phi_bb29_10, phi_bb29_12, phi_bb29_12, phi_bb29_10, phi_bb29_12, phi_bb29_12}, &block42, std::vector<Node*>{phi_bb29_10, phi_bb29_11, phi_bb29_16, phi_bb29_10, phi_bb29_10, phi_bb29_12, phi_bb29_12, phi_bb29_10, phi_bb29_12, phi_bb29_12});
  }

  TNode<FixedArray> phi_bb41_10;
  TNode<IntPtrT> phi_bb41_11;
  TNode<UintPtrT> phi_bb41_16;
  TNode<FixedArray> phi_bb41_21;
  TNode<FixedArray> phi_bb41_22;
  TNode<IntPtrT> phi_bb41_25;
  TNode<IntPtrT> phi_bb41_26;
  TNode<HeapObject> phi_bb41_27;
  TNode<IntPtrT> phi_bb41_30;
  TNode<IntPtrT> phi_bb41_31;
  TNode<IntPtrT> tmp54;
  TNode<IntPtrT> tmp55;
  TNode<IntPtrT> tmp56;
  TNode<HeapObject> tmp57;
  TNode<IntPtrT> tmp58;
  if (block41.is_used()) {
    ca_.Bind(&block41, &phi_bb41_10, &phi_bb41_11, &phi_bb41_16, &phi_bb41_21, &phi_bb41_22, &phi_bb41_25, &phi_bb41_26, &phi_bb41_27, &phi_bb41_30, &phi_bb41_31);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp54 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp55 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{phi_bb41_31}, TNode<IntPtrT>{tmp54});
    tmp56 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp44}, TNode<IntPtrT>{tmp55});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp57, tmp58) = NewReference_Object_0(state_, TNode<HeapObject>{phi_bb41_27}, TNode<IntPtrT>{tmp56}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 10);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp57, tmp58}, tmp26);
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 58);
    ca_.Goto(&block23, phi_bb41_10, phi_bb41_11, tmp50, phi_bb41_16);
  }

  TNode<FixedArray> phi_bb42_10;
  TNode<IntPtrT> phi_bb42_11;
  TNode<UintPtrT> phi_bb42_16;
  TNode<FixedArray> phi_bb42_21;
  TNode<FixedArray> phi_bb42_22;
  TNode<IntPtrT> phi_bb42_25;
  TNode<IntPtrT> phi_bb42_26;
  TNode<HeapObject> phi_bb42_27;
  TNode<IntPtrT> phi_bb42_30;
  TNode<IntPtrT> phi_bb42_31;
  if (block42.is_used()) {
    ca_.Bind(&block42, &phi_bb42_10, &phi_bb42_11, &phi_bb42_16, &phi_bb42_21, &phi_bb42_22, &phi_bb42_25, &phi_bb42_26, &phi_bb42_27, &phi_bb42_30, &phi_bb42_31);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb23_10;
  TNode<IntPtrT> phi_bb23_11;
  TNode<IntPtrT> phi_bb23_12;
  TNode<UintPtrT> phi_bb23_16;
  TNode<UintPtrT> tmp59;
  TNode<UintPtrT> tmp60;
  if (block23.is_used()) {
    ca_.Bind(&block23, &phi_bb23_10, &phi_bb23_11, &phi_bb23_12, &phi_bb23_16);
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 40);
    tmp59 = FromConstexpr_uintptr_constexpr_int31_0(state_, 1);
    tmp60 = CodeStubAssembler(state_).UintPtrAdd(TNode<UintPtrT>{phi_bb23_16}, TNode<UintPtrT>{tmp59});
    ca_.Goto(&block15, phi_bb23_10, phi_bb23_11, phi_bb23_12, tmp25, tmp60);
  }

  TNode<FixedArray> phi_bb14_10;
  TNode<IntPtrT> phi_bb14_11;
  TNode<IntPtrT> phi_bb14_12;
  TNode<JSTypedArray> phi_bb14_14;
  TNode<UintPtrT> phi_bb14_16;
  TNode<UintPtrT> tmp61;
  TNode<JSTypedArray> tmp62;
  TNode<UintPtrT> tmp63;
  TNode<Number> tmp64;
  TNode<NativeContext> tmp65;
  TNode<Map> tmp66;
  TNode<IntPtrT> tmp67;
  TNode<BoolT> tmp68;
  if (block14.is_used()) {
    ca_.Bind(&block14, &phi_bb14_10, &phi_bb14_11, &phi_bb14_12, &phi_bb14_14, &phi_bb14_16);
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 65);
    tmp61 = CodeStubAssembler(state_).Unsigned(TNode<IntPtrT>{phi_bb14_12});
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 64);
    tmp62 = TypedArraySpeciesCreateByLength_0(state_, TNode<Context>{parameter0}, kBuiltinNameFilter_0(state_), TNode<JSTypedArray>{tmp0}, TNode<UintPtrT>{tmp61});
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 74);
    tmp63 = CodeStubAssembler(state_).Unsigned(TNode<IntPtrT>{phi_bb14_12});
    tmp64 = Convert_Number_uintptr_0(state_, TNode<UintPtrT>{tmp63});
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 30);
    tmp65 = CodeStubAssembler(state_).LoadNativeContext(TNode<Context>{parameter0});
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 32);
    tmp66 = CodeStubAssembler(state_).LoadJSArrayElementsMap(ElementsKind::PACKED_ELEMENTS, TNode<NativeContext>{tmp65});
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 13);
    tmp67 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp68 = CodeStubAssembler(state_).IntPtrGreaterThanOrEqual(TNode<IntPtrT>{phi_bb14_12}, TNode<IntPtrT>{tmp67});
    ca_.Branch(tmp68, &block46, std::vector<Node*>{phi_bb14_10, phi_bb14_11, phi_bb14_12, phi_bb14_14, phi_bb14_12, phi_bb14_12}, &block47, std::vector<Node*>{phi_bb14_10, phi_bb14_11, phi_bb14_12, phi_bb14_14, phi_bb14_12, phi_bb14_12});
  }

  TNode<FixedArray> phi_bb47_10;
  TNode<IntPtrT> phi_bb47_11;
  TNode<IntPtrT> phi_bb47_12;
  TNode<JSTypedArray> phi_bb47_14;
  TNode<IntPtrT> phi_bb47_24;
  TNode<IntPtrT> phi_bb47_25;
  if (block47.is_used()) {
    ca_.Bind(&block47, &phi_bb47_10, &phi_bb47_11, &phi_bb47_12, &phi_bb47_14, &phi_bb47_24, &phi_bb47_25);
    CodeStubAssembler(state_).FailAssert("Torque assert 'this.length >= 0' failed", "src/builtins/growable-fixed-array.tq", 13);
  }

  TNode<FixedArray> phi_bb46_10;
  TNode<IntPtrT> phi_bb46_11;
  TNode<IntPtrT> phi_bb46_12;
  TNode<JSTypedArray> phi_bb46_14;
  TNode<IntPtrT> phi_bb46_24;
  TNode<IntPtrT> phi_bb46_25;
  TNode<IntPtrT> tmp69;
  TNode<BoolT> tmp70;
  if (block46.is_used()) {
    ca_.Bind(&block46, &phi_bb46_10, &phi_bb46_11, &phi_bb46_12, &phi_bb46_14, &phi_bb46_24, &phi_bb46_25);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 14);
    tmp69 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp70 = CodeStubAssembler(state_).IntPtrGreaterThanOrEqual(TNode<IntPtrT>{phi_bb46_25}, TNode<IntPtrT>{tmp69});
    ca_.Branch(tmp70, &block48, std::vector<Node*>{phi_bb46_10, phi_bb46_11, phi_bb46_12, phi_bb46_14, phi_bb46_24, phi_bb46_25}, &block49, std::vector<Node*>{phi_bb46_10, phi_bb46_11, phi_bb46_12, phi_bb46_14, phi_bb46_24, phi_bb46_25});
  }

  TNode<FixedArray> phi_bb49_10;
  TNode<IntPtrT> phi_bb49_11;
  TNode<IntPtrT> phi_bb49_12;
  TNode<JSTypedArray> phi_bb49_14;
  TNode<IntPtrT> phi_bb49_24;
  TNode<IntPtrT> phi_bb49_25;
  if (block49.is_used()) {
    ca_.Bind(&block49, &phi_bb49_10, &phi_bb49_11, &phi_bb49_12, &phi_bb49_14, &phi_bb49_24, &phi_bb49_25);
    CodeStubAssembler(state_).FailAssert("Torque assert 'newCapacity >= 0' failed", "src/builtins/growable-fixed-array.tq", 14);
  }

  TNode<FixedArray> phi_bb48_10;
  TNode<IntPtrT> phi_bb48_11;
  TNode<IntPtrT> phi_bb48_12;
  TNode<JSTypedArray> phi_bb48_14;
  TNode<IntPtrT> phi_bb48_24;
  TNode<IntPtrT> phi_bb48_25;
  TNode<BoolT> tmp71;
  if (block48.is_used()) {
    ca_.Bind(&block48, &phi_bb48_10, &phi_bb48_11, &phi_bb48_12, &phi_bb48_14, &phi_bb48_24, &phi_bb48_25);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 15);
    tmp71 = CodeStubAssembler(state_).IntPtrGreaterThanOrEqual(TNode<IntPtrT>{phi_bb48_25}, TNode<IntPtrT>{phi_bb48_12});
    ca_.Branch(tmp71, &block50, std::vector<Node*>{phi_bb48_10, phi_bb48_11, phi_bb48_12, phi_bb48_14, phi_bb48_24, phi_bb48_25}, &block51, std::vector<Node*>{phi_bb48_10, phi_bb48_11, phi_bb48_12, phi_bb48_14, phi_bb48_24, phi_bb48_25});
  }

  TNode<FixedArray> phi_bb51_10;
  TNode<IntPtrT> phi_bb51_11;
  TNode<IntPtrT> phi_bb51_12;
  TNode<JSTypedArray> phi_bb51_14;
  TNode<IntPtrT> phi_bb51_24;
  TNode<IntPtrT> phi_bb51_25;
  if (block51.is_used()) {
    ca_.Bind(&block51, &phi_bb51_10, &phi_bb51_11, &phi_bb51_12, &phi_bb51_14, &phi_bb51_24, &phi_bb51_25);
    CodeStubAssembler(state_).FailAssert("Torque assert 'newCapacity >= this.length' failed", "src/builtins/growable-fixed-array.tq", 15);
  }

  TNode<FixedArray> phi_bb50_10;
  TNode<IntPtrT> phi_bb50_11;
  TNode<IntPtrT> phi_bb50_12;
  TNode<JSTypedArray> phi_bb50_14;
  TNode<IntPtrT> phi_bb50_24;
  TNode<IntPtrT> phi_bb50_25;
  TNode<IntPtrT> tmp72;
  TNode<FixedArray> tmp73;
  TNode<Smi> tmp74;
  TNode<JSArray> tmp75;
  if (block50.is_used()) {
    ca_.Bind(&block50, &phi_bb50_10, &phi_bb50_11, &phi_bb50_12, &phi_bb50_14, &phi_bb50_24, &phi_bb50_25);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 16);
    tmp72 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 17);
    tmp73 = ExtractFixedArray_0(state_, TNode<FixedArray>{phi_bb50_10}, TNode<IntPtrT>{tmp72}, TNode<IntPtrT>{phi_bb50_12}, TNode<IntPtrT>{phi_bb50_25});
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 34);
    tmp74 = Convert_Smi_intptr_0(state_, TNode<IntPtrT>{phi_bb50_12});
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 35);
    tmp75 = CodeStubAssembler(state_).AllocateJSArray(TNode<Map>{tmp66}, TNode<FixedArrayBase>{tmp73}, TNode<Smi>{tmp74});
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 75);
    CodeStubAssembler(state_).CallRuntime(Runtime::kTypedArrayCopyElements, parameter0, tmp62, tmp75, tmp64);
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 78);
    arguments.PopAndReturn(tmp62);
  }

  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../src/builtins/typed-array-filter.tq", 80);
    CodeStubAssembler(state_).ThrowTypeError(TNode<Context>{parameter0}, MessageTemplate::kDetachedOperation, kBuiltinNameFilter_0(state_));
  }
}

}  // namespace internal
}  // namespace v8

