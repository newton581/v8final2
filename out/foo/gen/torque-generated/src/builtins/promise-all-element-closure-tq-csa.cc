#include "src/builtins/builtins-array-gen.h"
#include "src/builtins/builtins-bigint-gen.h"
#include "src/builtins/builtins-collections-gen.h"
#include "src/builtins/builtins-constructor-gen.h"
#include "src/builtins/builtins-data-view-gen.h"
#include "src/builtins/builtins-iterator-gen.h"
#include "src/builtins/builtins-promise-gen.h"
#include "src/builtins/builtins-promise.h"
#include "src/builtins/builtins-proxy-gen.h"
#include "src/builtins/builtins-regexp-gen.h"
#include "src/builtins/builtins-string-gen.h"
#include "src/builtins/builtins-typed-array-gen.h"
#include "src/builtins/builtins-utils-gen.h"
#include "src/builtins/builtins-wasm-gen.h"
#include "src/builtins/builtins.h"
#include "src/codegen/code-factory.h"
#include "src/heap/factory-inl.h"
#include "src/ic/binary-op-assembler.h"
#include "src/objects/arguments.h"
#include "src/objects/bigint.h"
#include "src/objects/elements-kind.h"
#include "src/objects/free-space.h"
#include "src/objects/js-break-iterator.h"
#include "src/objects/js-collator.h"
#include "src/objects/js-date-time-format.h"
#include "src/objects/js-display-names.h"
#include "src/objects/js-generator.h"
#include "src/objects/js-list-format.h"
#include "src/objects/js-locale.h"
#include "src/objects/js-number-format.h"
#include "src/objects/js-objects.h"
#include "src/objects/js-plural-rules.h"
#include "src/objects/js-promise.h"
#include "src/objects/js-regexp-string-iterator.h"
#include "src/objects/js-relative-time-format.h"
#include "src/objects/js-segment-iterator.h"
#include "src/objects/js-segmenter.h"
#include "src/objects/js-weak-refs.h"
#include "src/objects/objects.h"
#include "src/objects/ordered-hash-table.h"
#include "src/objects/property-array.h"
#include "src/objects/property-descriptor-object.h"
#include "src/objects/source-text-module.h"
#include "src/objects/stack-frame-info.h"
#include "src/objects/synthetic-module.h"
#include "src/objects/template-objects.h"
#include "src/torque/runtime-support.h"
#include "torque-generated/src/builtins/aggregate-error-tq-csa.h"
#include "torque-generated/src/builtins/array-copywithin-tq-csa.h"
#include "torque-generated/src/builtins/array-every-tq-csa.h"
#include "torque-generated/src/builtins/array-filter-tq-csa.h"
#include "torque-generated/src/builtins/array-find-tq-csa.h"
#include "torque-generated/src/builtins/array-findindex-tq-csa.h"
#include "torque-generated/src/builtins/array-foreach-tq-csa.h"
#include "torque-generated/src/builtins/array-from-tq-csa.h"
#include "torque-generated/src/builtins/array-isarray-tq-csa.h"
#include "torque-generated/src/builtins/array-join-tq-csa.h"
#include "torque-generated/src/builtins/array-lastindexof-tq-csa.h"
#include "torque-generated/src/builtins/array-map-tq-csa.h"
#include "torque-generated/src/builtins/array-of-tq-csa.h"
#include "torque-generated/src/builtins/array-reduce-right-tq-csa.h"
#include "torque-generated/src/builtins/array-reduce-tq-csa.h"
#include "torque-generated/src/builtins/array-reverse-tq-csa.h"
#include "torque-generated/src/builtins/array-shift-tq-csa.h"
#include "torque-generated/src/builtins/array-slice-tq-csa.h"
#include "torque-generated/src/builtins/array-some-tq-csa.h"
#include "torque-generated/src/builtins/array-splice-tq-csa.h"
#include "torque-generated/src/builtins/array-unshift-tq-csa.h"
#include "torque-generated/src/builtins/array-tq-csa.h"
#include "torque-generated/src/builtins/base-tq-csa.h"
#include "torque-generated/src/builtins/bigint-tq-csa.h"
#include "torque-generated/src/builtins/boolean-tq-csa.h"
#include "torque-generated/src/builtins/builtins-string-tq-csa.h"
#include "torque-generated/src/builtins/cast-tq-csa.h"
#include "torque-generated/src/builtins/collections-tq-csa.h"
#include "torque-generated/src/builtins/conversion-tq-csa.h"
#include "torque-generated/src/builtins/convert-tq-csa.h"
#include "torque-generated/src/builtins/console-tq-csa.h"
#include "torque-generated/src/builtins/data-view-tq-csa.h"
#include "torque-generated/src/builtins/finalization-registry-tq-csa.h"
#include "torque-generated/src/builtins/frames-tq-csa.h"
#include "torque-generated/src/builtins/frame-arguments-tq-csa.h"
#include "torque-generated/src/builtins/function-tq-csa.h"
#include "torque-generated/src/builtins/growable-fixed-array-tq-csa.h"
#include "torque-generated/src/builtins/ic-callable-tq-csa.h"
#include "torque-generated/src/builtins/ic-tq-csa.h"
#include "torque-generated/src/builtins/internal-coverage-tq-csa.h"
#include "torque-generated/src/builtins/internal-tq-csa.h"
#include "torque-generated/src/builtins/iterator-tq-csa.h"
#include "torque-generated/src/builtins/math-tq-csa.h"
#include "torque-generated/src/builtins/number-tq-csa.h"
#include "torque-generated/src/builtins/object-fromentries-tq-csa.h"
#include "torque-generated/src/builtins/object-tq-csa.h"
#include "torque-generated/src/builtins/promise-abstract-operations-tq-csa.h"
#include "torque-generated/src/builtins/promise-all-tq-csa.h"
#include "torque-generated/src/builtins/promise-all-element-closure-tq-csa.h"
#include "torque-generated/src/builtins/promise-any-tq-csa.h"
#include "torque-generated/src/builtins/promise-constructor-tq-csa.h"
#include "torque-generated/src/builtins/promise-finally-tq-csa.h"
#include "torque-generated/src/builtins/promise-misc-tq-csa.h"
#include "torque-generated/src/builtins/promise-race-tq-csa.h"
#include "torque-generated/src/builtins/promise-reaction-job-tq-csa.h"
#include "torque-generated/src/builtins/promise-resolve-tq-csa.h"
#include "torque-generated/src/builtins/promise-then-tq-csa.h"
#include "torque-generated/src/builtins/promise-jobs-tq-csa.h"
#include "torque-generated/src/builtins/proxy-constructor-tq-csa.h"
#include "torque-generated/src/builtins/proxy-delete-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-get-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-get-prototype-of-tq-csa.h"
#include "torque-generated/src/builtins/proxy-has-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-is-extensible-tq-csa.h"
#include "torque-generated/src/builtins/proxy-prevent-extensions-tq-csa.h"
#include "torque-generated/src/builtins/proxy-revocable-tq-csa.h"
#include "torque-generated/src/builtins/proxy-revoke-tq-csa.h"
#include "torque-generated/src/builtins/proxy-set-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-set-prototype-of-tq-csa.h"
#include "torque-generated/src/builtins/proxy-tq-csa.h"
#include "torque-generated/src/builtins/reflect-tq-csa.h"
#include "torque-generated/src/builtins/regexp-exec-tq-csa.h"
#include "torque-generated/src/builtins/regexp-match-all-tq-csa.h"
#include "torque-generated/src/builtins/regexp-match-tq-csa.h"
#include "torque-generated/src/builtins/regexp-replace-tq-csa.h"
#include "torque-generated/src/builtins/regexp-search-tq-csa.h"
#include "torque-generated/src/builtins/regexp-source-tq-csa.h"
#include "torque-generated/src/builtins/regexp-split-tq-csa.h"
#include "torque-generated/src/builtins/regexp-test-tq-csa.h"
#include "torque-generated/src/builtins/regexp-tq-csa.h"
#include "torque-generated/src/builtins/string-endswith-tq-csa.h"
#include "torque-generated/src/builtins/string-html-tq-csa.h"
#include "torque-generated/src/builtins/string-iterator-tq-csa.h"
#include "torque-generated/src/builtins/string-pad-tq-csa.h"
#include "torque-generated/src/builtins/string-repeat-tq-csa.h"
#include "torque-generated/src/builtins/string-replaceall-tq-csa.h"
#include "torque-generated/src/builtins/string-slice-tq-csa.h"
#include "torque-generated/src/builtins/string-startswith-tq-csa.h"
#include "torque-generated/src/builtins/string-substring-tq-csa.h"
#include "torque-generated/src/builtins/string-substr-tq-csa.h"
#include "torque-generated/src/builtins/symbol-tq-csa.h"
#include "torque-generated/src/builtins/torque-internal-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-createtypedarray-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-every-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-entries-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-filter-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-find-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-findindex-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-foreach-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-from-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-keys-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-of-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-reduce-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-reduceright-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-set-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-slice-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-some-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-sort-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-subarray-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-values-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-tq-csa.h"
#include "torque-generated/src/builtins/wasm-tq-csa.h"
#include "torque-generated/src/builtins/weak-ref-tq-csa.h"
#include "torque-generated/src/ic/handler-configuration-tq-csa.h"
#include "torque-generated/src/objects/allocation-site-tq-csa.h"
#include "torque-generated/src/objects/api-callbacks-tq-csa.h"
#include "torque-generated/src/objects/arguments-tq-csa.h"
#include "torque-generated/src/objects/cell-tq-csa.h"
#include "torque-generated/src/objects/code-tq-csa.h"
#include "torque-generated/src/objects/contexts-tq-csa.h"
#include "torque-generated/src/objects/data-handler-tq-csa.h"
#include "torque-generated/src/objects/debug-objects-tq-csa.h"
#include "torque-generated/src/objects/descriptor-array-tq-csa.h"
#include "torque-generated/src/objects/embedder-data-array-tq-csa.h"
#include "torque-generated/src/objects/feedback-cell-tq-csa.h"
#include "torque-generated/src/objects/feedback-vector-tq-csa.h"
#include "torque-generated/src/objects/fixed-array-tq-csa.h"
#include "torque-generated/src/objects/foreign-tq-csa.h"
#include "torque-generated/src/objects/free-space-tq-csa.h"
#include "torque-generated/src/objects/heap-number-tq-csa.h"
#include "torque-generated/src/objects/heap-object-tq-csa.h"
#include "torque-generated/src/objects/intl-objects-tq-csa.h"
#include "torque-generated/src/objects/js-array-buffer-tq-csa.h"
#include "torque-generated/src/objects/js-array-tq-csa.h"
#include "torque-generated/src/objects/js-collection-iterator-tq-csa.h"
#include "torque-generated/src/objects/js-collection-tq-csa.h"
#include "torque-generated/src/objects/js-generator-tq-csa.h"
#include "torque-generated/src/objects/js-objects-tq-csa.h"
#include "torque-generated/src/objects/js-promise-tq-csa.h"
#include "torque-generated/src/objects/js-proxy-tq-csa.h"
#include "torque-generated/src/objects/js-regexp-string-iterator-tq-csa.h"
#include "torque-generated/src/objects/js-regexp-tq-csa.h"
#include "torque-generated/src/objects/js-weak-refs-tq-csa.h"
#include "torque-generated/src/objects/literal-objects-tq-csa.h"
#include "torque-generated/src/objects/map-tq-csa.h"
#include "torque-generated/src/objects/microtask-tq-csa.h"
#include "torque-generated/src/objects/module-tq-csa.h"
#include "torque-generated/src/objects/name-tq-csa.h"
#include "torque-generated/src/objects/oddball-tq-csa.h"
#include "torque-generated/src/objects/ordered-hash-table-tq-csa.h"
#include "torque-generated/src/objects/primitive-heap-object-tq-csa.h"
#include "torque-generated/src/objects/promise-tq-csa.h"
#include "torque-generated/src/objects/property-array-tq-csa.h"
#include "torque-generated/src/objects/property-cell-tq-csa.h"
#include "torque-generated/src/objects/property-descriptor-object-tq-csa.h"
#include "torque-generated/src/objects/prototype-info-tq-csa.h"
#include "torque-generated/src/objects/regexp-match-info-tq-csa.h"
#include "torque-generated/src/objects/scope-info-tq-csa.h"
#include "torque-generated/src/objects/script-tq-csa.h"
#include "torque-generated/src/objects/shared-function-info-tq-csa.h"
#include "torque-generated/src/objects/source-text-module-tq-csa.h"
#include "torque-generated/src/objects/stack-frame-info-tq-csa.h"
#include "torque-generated/src/objects/string-tq-csa.h"
#include "torque-generated/src/objects/struct-tq-csa.h"
#include "torque-generated/src/objects/synthetic-module-tq-csa.h"
#include "torque-generated/src/objects/template-objects-tq-csa.h"
#include "torque-generated/src/objects/template-tq-csa.h"
#include "torque-generated/src/wasm/wasm-objects-tq-csa.h"
#include "torque-generated/test/torque/test-torque-tq-csa.h"
#include "torque-generated/third_party/v8/builtins/array-sort-tq-csa.h"

namespace v8 {
namespace internal {

TNode<Int32T> FromConstexpr_PromiseAllResolveElementContextSlots_constexpr_kPromiseAllResolveElementRemainingSlot_0(compiler::CodeAssemblerState* state_, PromiseBuiltins::PromiseAllResolveElementContextSlots p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Int32T> tmp0;
  TNode<Int32T> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 65);
    tmp0 = ca_.Int32Constant(CastToUnderlyingTypeIfEnum(p_o));
    tmp1 = (TNode<Int32T>{tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<Int32T>{tmp1};
}

TNode<Int32T> FromConstexpr_PromiseAllResolveElementContextSlots_constexpr_kPromiseAllResolveElementCapabilitySlot_0(compiler::CodeAssemblerState* state_, PromiseBuiltins::PromiseAllResolveElementContextSlots p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Int32T> tmp0;
  TNode<Int32T> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 65);
    tmp0 = ca_.Int32Constant(CastToUnderlyingTypeIfEnum(p_o));
    tmp1 = (TNode<Int32T>{tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<Int32T>{tmp1};
}

TNode<Int32T> FromConstexpr_PromiseAllResolveElementContextSlots_constexpr_kPromiseAllResolveElementValuesSlot_0(compiler::CodeAssemblerState* state_, PromiseBuiltins::PromiseAllResolveElementContextSlots p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Int32T> tmp0;
  TNode<Int32T> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 65);
    tmp0 = ca_.Int32Constant(CastToUnderlyingTypeIfEnum(p_o));
    tmp1 = (TNode<Int32T>{tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<Int32T>{tmp1};
}

TNode<Int32T> FromConstexpr_PromiseAllResolveElementContextSlots_constexpr_kPromiseAllResolveElementLength_0(compiler::CodeAssemblerState* state_, PromiseBuiltins::PromiseAllResolveElementContextSlots p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Int32T> tmp0;
  TNode<Int32T> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 65);
    tmp0 = ca_.Int32Constant(CastToUnderlyingTypeIfEnum(p_o));
    tmp1 = (TNode<Int32T>{tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<Int32T>{tmp1};
}

TF_BUILTIN(PromiseAllResolveElementClosure, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kReceiver));
USE(parameter1);
  TNode<JSFunction> parameter2 = UncheckedCast<JSFunction>(Parameter(Descriptor::kJSTarget));
USE(parameter2);
  TNode<Object> parameter3 = UncheckedCast<Object>(Parameter(Descriptor::kValue));
  USE(parameter3);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Object> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 168);
    tmp0 = PromiseAllResolveElementClosure_PromiseAllWrapResultAsFulfilledFunctor_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter3}, TNode<JSFunction>{parameter2}, TorqueStructPromiseAllWrapResultAsFulfilledFunctor_0{}, false);
    CodeStubAssembler(state_).Return(tmp0);
  }
}

TF_BUILTIN(PromiseAllSettledResolveElementClosure, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kReceiver));
USE(parameter1);
  TNode<JSFunction> parameter2 = UncheckedCast<JSFunction>(Parameter(Descriptor::kJSTarget));
USE(parameter2);
  TNode<Object> parameter3 = UncheckedCast<Object>(Parameter(Descriptor::kValue));
  USE(parameter3);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Object> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 176);
    tmp0 = PromiseAllResolveElementClosure_PromiseAllSettledWrapResultAsFulfilledFunctor_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter3}, TNode<JSFunction>{parameter2}, TorqueStructPromiseAllSettledWrapResultAsFulfilledFunctor_0{}, true);
    CodeStubAssembler(state_).Return(tmp0);
  }
}

TF_BUILTIN(PromiseAllSettledRejectElementClosure, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kReceiver));
USE(parameter1);
  TNode<JSFunction> parameter2 = UncheckedCast<JSFunction>(Parameter(Descriptor::kJSTarget));
USE(parameter2);
  TNode<Object> parameter3 = UncheckedCast<Object>(Parameter(Descriptor::kValue));
  USE(parameter3);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Object> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 184);
    tmp0 = PromiseAllResolveElementClosure_PromiseAllSettledWrapResultAsRejectedFunctor_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter3}, TNode<JSFunction>{parameter2}, TorqueStructPromiseAllSettledWrapResultAsRejectedFunctor_0{}, true);
    CodeStubAssembler(state_).Return(tmp0);
  }
}

TNode<Object> PromiseAllResolveElementClosure_PromiseAllWrapResultAsFulfilledFunctor_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<Object> p_value, TNode<JSFunction> p_function, TorqueStructPromiseAllWrapResultAsFulfilledFunctor_0 p_wrapResultFunctor, bool p_hasResolveAndRejectClosures) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block18(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block19(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block25(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block26(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block28(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block34(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block35(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block29(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block37(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, FixedArray, FixedArray, HeapObject> block45(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, FixedArray, FixedArray, HeapObject> block46(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block39(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block40(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block38(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, FixedArray, FixedArray, HeapObject> block53(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, FixedArray, FixedArray, HeapObject> block54(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block60(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block61(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block63(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block69(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block70(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block76(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block77(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block64(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block79(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<BoolT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 92);
    tmp0 = IsNativeContext_0(state_, TNode<HeapObject>{p_context});
    ca_.Branch(tmp0, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  TNode<Oddball> tmp1;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 93);
    tmp1 = Undefined_0(state_);
    ca_.Goto(&block1, tmp1);
  }

  TNode<IntPtrT> tmp2;
  TNode<Smi> tmp3;
  TNode<Smi> tmp4;
  TNode<BoolT> tmp5;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 97);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp3 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_context, tmp2});
    tmp4 = FromConstexpr_Smi_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementLength);
    tmp5 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp3}, TNode<Smi>{tmp4});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 96);
    ca_.Branch(tmp5, &block4, std::vector<Node*>{}, &block5, std::vector<Node*>{});
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    CodeStubAssembler(state_).FailAssert("Torque assert 'context.length == PromiseAllResolveElementContextSlots::kPromiseAllResolveElementLength' failed", "src/builtins/promise-all-element-closure.tq", 96);
  }

  TNode<NativeContext> tmp6;
  TNode<IntPtrT> tmp7;
  TNode<BoolT> tmp8;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 99);
    tmp6 = CodeStubAssembler(state_).LoadNativeContext(TNode<Context>{p_context});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 100);
    tmp7 = FromConstexpr_intptr_constexpr_int31_0(state_, 16);
    CodeStubAssembler(state_).StoreReference<Context>(CodeStubAssembler::Reference{p_function, tmp7}, tmp6);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 103);
    tmp8 = FromConstexpr_bool_constexpr_bool_0(state_, (CodeStubAssembler(state_).ConstexprInt31Equal(PropertyArray::kNoHashSentinel, 0)));
    ca_.Branch(tmp8, &block6, std::vector<Node*>{}, &block7, std::vector<Node*>{});
  }

  if (block7.is_used()) {
    ca_.Bind(&block7);
    CodeStubAssembler(state_).FailAssert("Torque assert 'kPropertyArrayNoHashSentinel == 0' failed", "src/builtins/promise-all-element-closure.tq", 103);
  }

  TNode<IntPtrT> tmp9;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 105);
    compiler::CodeAssemblerLabel label10(&ca_);
    tmp9 = CodeStubAssembler(state_).LoadJSReceiverIdentityHash(TNode<Object>{p_function}, &label10);
    ca_.Goto(&block10);
    if (label10.is_used()) {
      ca_.Bind(&label10);
      ca_.Goto(&block11);
    }
  }

  if (block11.is_used()) {
    ca_.Bind(&block11);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/promise-all-element-closure.tq:105:54");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp11;
  TNode<BoolT> tmp12;
  if (block10.is_used()) {
    ca_.Bind(&block10);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 106);
    tmp11 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp12 = CodeStubAssembler(state_).IntPtrGreaterThan(TNode<IntPtrT>{tmp9}, TNode<IntPtrT>{tmp11});
    ca_.Branch(tmp12, &block12, std::vector<Node*>{}, &block13, std::vector<Node*>{});
  }

  if (block13.is_used()) {
    ca_.Bind(&block13);
    CodeStubAssembler(state_).FailAssert("Torque assert 'identityHash > 0' failed", "src/builtins/promise-all-element-closure.tq", 106);
  }

  TNode<IntPtrT> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<IntPtrT> tmp15;
  TNode<IntPtrT> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<Smi> tmp18;
  TNode<IntPtrT> tmp19;
  TNode<IntPtrT> tmp20;
  TNode<UintPtrT> tmp21;
  TNode<UintPtrT> tmp22;
  TNode<BoolT> tmp23;
  if (block12.is_used()) {
    ca_.Bind(&block12);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 107);
    tmp13 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp14 = CodeStubAssembler(state_).IntPtrSub(TNode<IntPtrT>{tmp9}, TNode<IntPtrT>{tmp13});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 110);
    tmp15 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp16 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp17 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp18 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_context, tmp17});
    tmp19 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp18});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp20 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp21 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp20});
    tmp22 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp19});
    tmp23 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp21}, TNode<UintPtrT>{tmp22});
    ca_.Branch(tmp23, &block18, std::vector<Node*>{}, &block19, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp24;
  TNode<IntPtrT> tmp25;
  TNode<IntPtrT> tmp26;
  TNode<HeapObject> tmp27;
  TNode<IntPtrT> tmp28;
  TNode<Object> tmp29;
  TNode<Smi> tmp30;
  TNode<IntPtrT> tmp31;
  TNode<IntPtrT> tmp32;
  TNode<IntPtrT> tmp33;
  TNode<Smi> tmp34;
  TNode<IntPtrT> tmp35;
  TNode<IntPtrT> tmp36;
  TNode<UintPtrT> tmp37;
  TNode<UintPtrT> tmp38;
  TNode<BoolT> tmp39;
  if (block18.is_used()) {
    ca_.Bind(&block18);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp24 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp25 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp20}, TNode<IntPtrT>{tmp24});
    tmp26 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp15}, TNode<IntPtrT>{tmp25});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp27, tmp28) = NewReference_Object_0(state_, TNode<HeapObject>{p_context}, TNode<IntPtrT>{tmp26}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 110);
    tmp29 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp27, tmp28});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 109);
    tmp30 = UnsafeCast_Smi_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp29});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 114);
    tmp31 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp32 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp33 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp34 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_context, tmp33});
    tmp35 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp34});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp36 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementValuesSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp37 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp36});
    tmp38 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp35});
    tmp39 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp37}, TNode<UintPtrT>{tmp38});
    ca_.Branch(tmp39, &block25, std::vector<Node*>{}, &block26, std::vector<Node*>{});
  }

  if (block19.is_used()) {
    ca_.Bind(&block19);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp40;
  TNode<IntPtrT> tmp41;
  TNode<IntPtrT> tmp42;
  TNode<HeapObject> tmp43;
  TNode<IntPtrT> tmp44;
  TNode<Object> tmp45;
  TNode<FixedArray> tmp46;
  TNode<IntPtrT> tmp47;
  TNode<IntPtrT> tmp48;
  TNode<IntPtrT> tmp49;
  TNode<BoolT> tmp50;
  if (block25.is_used()) {
    ca_.Bind(&block25);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp40 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp41 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp36}, TNode<IntPtrT>{tmp40});
    tmp42 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp31}, TNode<IntPtrT>{tmp41});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp43, tmp44) = NewReference_Object_0(state_, TNode<HeapObject>{p_context}, TNode<IntPtrT>{tmp42}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 114);
    tmp45 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp43, tmp44});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 113);
    tmp46 = UnsafeCast_FixedArray_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp45});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 116);
    tmp47 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp48 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp14}, TNode<IntPtrT>{tmp47});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 117);
    tmp49 = CodeStubAssembler(state_).LoadAndUntagFixedArrayBaseLength(TNode<FixedArrayBase>{tmp46});
    tmp50 = CodeStubAssembler(state_).IntPtrGreaterThan(TNode<IntPtrT>{tmp48}, TNode<IntPtrT>{tmp49});
    ca_.Branch(tmp50, &block28, std::vector<Node*>{}, &block29, std::vector<Node*>{tmp46});
  }

  if (block26.is_used()) {
    ca_.Bind(&block26);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp51;
  TNode<IntPtrT> tmp52;
  TNode<FixedArray> tmp53;
  TNode<IntPtrT> tmp54;
  TNode<IntPtrT> tmp55;
  TNode<IntPtrT> tmp56;
  TNode<Smi> tmp57;
  TNode<IntPtrT> tmp58;
  TNode<IntPtrT> tmp59;
  TNode<UintPtrT> tmp60;
  TNode<UintPtrT> tmp61;
  TNode<BoolT> tmp62;
  if (block28.is_used()) {
    ca_.Bind(&block28);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 119);
    tmp51 = CodeStubAssembler(state_).LoadAndUntagFixedArrayBaseLength(TNode<FixedArrayBase>{tmp46});
    tmp52 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp53 = ExtractFixedArray_0(state_, TNode<FixedArray>{tmp46}, TNode<IntPtrT>{tmp52}, TNode<IntPtrT>{tmp51}, TNode<IntPtrT>{tmp48});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 120);
    tmp54 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp55 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp56 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp57 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_context, tmp56});
    tmp58 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp57});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp59 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementValuesSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp60 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp59});
    tmp61 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp58});
    tmp62 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp60}, TNode<UintPtrT>{tmp61});
    ca_.Branch(tmp62, &block34, std::vector<Node*>{}, &block35, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp63;
  TNode<IntPtrT> tmp64;
  TNode<IntPtrT> tmp65;
  TNode<HeapObject> tmp66;
  TNode<IntPtrT> tmp67;
  if (block34.is_used()) {
    ca_.Bind(&block34);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp63 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp64 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp59}, TNode<IntPtrT>{tmp63});
    tmp65 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp54}, TNode<IntPtrT>{tmp64});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp66, tmp67) = NewReference_Object_0(state_, TNode<HeapObject>{p_context}, TNode<IntPtrT>{tmp65}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 120);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp66, tmp67}, tmp53);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 117);
    ca_.Goto(&block29, tmp53);
  }

  if (block35.is_used()) {
    ca_.Bind(&block35);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb29_7;
  TNode<BoolT> tmp68;
  if (block29.is_used()) {
    ca_.Bind(&block29, &phi_bb29_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 134);
    tmp68 = FromConstexpr_bool_constexpr_bool_0(state_, p_hasResolveAndRejectClosures);
    ca_.Branch(tmp68, &block37, std::vector<Node*>{phi_bb29_7}, &block38, std::vector<Node*>{phi_bb29_7});
  }

  TNode<FixedArray> phi_bb37_7;
  TNode<IntPtrT> tmp69;
  TNode<IntPtrT> tmp70;
  TNode<IntPtrT> tmp71;
  TNode<Smi> tmp72;
  TNode<IntPtrT> tmp73;
  TNode<UintPtrT> tmp74;
  TNode<UintPtrT> tmp75;
  TNode<BoolT> tmp76;
  if (block37.is_used()) {
    ca_.Bind(&block37, &phi_bb37_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 135);
    tmp69 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp70 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp71 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp72 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{phi_bb37_7, tmp71});
    tmp73 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp72});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp74 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp14});
    tmp75 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp73});
    tmp76 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp74}, TNode<UintPtrT>{tmp75});
    ca_.Branch(tmp76, &block45, std::vector<Node*>{phi_bb37_7, phi_bb37_7, phi_bb37_7, phi_bb37_7}, &block46, std::vector<Node*>{phi_bb37_7, phi_bb37_7, phi_bb37_7, phi_bb37_7});
  }

  TNode<FixedArray> phi_bb45_7;
  TNode<FixedArray> phi_bb45_9;
  TNode<FixedArray> phi_bb45_10;
  TNode<HeapObject> phi_bb45_15;
  TNode<IntPtrT> tmp77;
  TNode<IntPtrT> tmp78;
  TNode<IntPtrT> tmp79;
  TNode<HeapObject> tmp80;
  TNode<IntPtrT> tmp81;
  TNode<Object> tmp82;
  TNode<Oddball> tmp83;
  TNode<BoolT> tmp84;
  if (block45.is_used()) {
    ca_.Bind(&block45, &phi_bb45_7, &phi_bb45_9, &phi_bb45_10, &phi_bb45_15);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp77 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp78 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp14}, TNode<IntPtrT>{tmp77});
    tmp79 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp69}, TNode<IntPtrT>{tmp78});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp80, tmp81) = NewReference_Object_0(state_, TNode<HeapObject>{phi_bb45_15}, TNode<IntPtrT>{tmp79}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 135);
    tmp82 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp80, tmp81});
    tmp83 = TheHole_0(state_);
    tmp84 = CodeStubAssembler(state_).TaggedNotEqual(TNode<Object>{tmp82}, TNode<HeapObject>{tmp83});
    ca_.Branch(tmp84, &block39, std::vector<Node*>{phi_bb45_7}, &block40, std::vector<Node*>{phi_bb45_7});
  }

  TNode<FixedArray> phi_bb46_7;
  TNode<FixedArray> phi_bb46_9;
  TNode<FixedArray> phi_bb46_10;
  TNode<HeapObject> phi_bb46_15;
  if (block46.is_used()) {
    ca_.Bind(&block46, &phi_bb46_7, &phi_bb46_9, &phi_bb46_10, &phi_bb46_15);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb39_7;
  TNode<Oddball> tmp85;
  if (block39.is_used()) {
    ca_.Bind(&block39, &phi_bb39_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 136);
    tmp85 = Undefined_0(state_);
    ca_.Goto(&block1, tmp85);
  }

  TNode<FixedArray> phi_bb40_7;
  if (block40.is_used()) {
    ca_.Bind(&block40, &phi_bb40_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 134);
    ca_.Goto(&block38, phi_bb40_7);
  }

  TNode<FixedArray> phi_bb38_7;
  TNode<IntPtrT> tmp86;
  TNode<IntPtrT> tmp87;
  TNode<IntPtrT> tmp88;
  TNode<Smi> tmp89;
  TNode<IntPtrT> tmp90;
  TNode<UintPtrT> tmp91;
  TNode<UintPtrT> tmp92;
  TNode<BoolT> tmp93;
  if (block38.is_used()) {
    ca_.Bind(&block38, &phi_bb38_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 144);
    tmp86 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp87 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp88 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp89 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{phi_bb38_7, tmp88});
    tmp90 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp89});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp91 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp14});
    tmp92 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp90});
    tmp93 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp91}, TNode<UintPtrT>{tmp92});
    ca_.Branch(tmp93, &block53, std::vector<Node*>{phi_bb38_7, phi_bb38_7, phi_bb38_7, phi_bb38_7}, &block54, std::vector<Node*>{phi_bb38_7, phi_bb38_7, phi_bb38_7, phi_bb38_7});
  }

  TNode<FixedArray> phi_bb53_7;
  TNode<FixedArray> phi_bb53_10;
  TNode<FixedArray> phi_bb53_11;
  TNode<HeapObject> phi_bb53_16;
  TNode<IntPtrT> tmp94;
  TNode<IntPtrT> tmp95;
  TNode<IntPtrT> tmp96;
  TNode<HeapObject> tmp97;
  TNode<IntPtrT> tmp98;
  TNode<Smi> tmp99;
  TNode<Smi> tmp100;
  TNode<IntPtrT> tmp101;
  TNode<IntPtrT> tmp102;
  TNode<IntPtrT> tmp103;
  TNode<Smi> tmp104;
  TNode<IntPtrT> tmp105;
  TNode<IntPtrT> tmp106;
  TNode<UintPtrT> tmp107;
  TNode<UintPtrT> tmp108;
  TNode<BoolT> tmp109;
  if (block53.is_used()) {
    ca_.Bind(&block53, &phi_bb53_7, &phi_bb53_10, &phi_bb53_11, &phi_bb53_16);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp94 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp95 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp14}, TNode<IntPtrT>{tmp94});
    tmp96 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp86}, TNode<IntPtrT>{tmp95});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp97, tmp98) = NewReference_Object_0(state_, TNode<HeapObject>{phi_bb53_16}, TNode<IntPtrT>{tmp96}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 144);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp97, tmp98}, p_value);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 146);
    tmp99 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp100 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp30}, TNode<Smi>{tmp99});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 147);
    tmp101 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp102 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp103 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp104 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_context, tmp103});
    tmp105 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp104});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp106 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp107 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp106});
    tmp108 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp105});
    tmp109 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp107}, TNode<UintPtrT>{tmp108});
    ca_.Branch(tmp109, &block60, std::vector<Node*>{phi_bb53_7}, &block61, std::vector<Node*>{phi_bb53_7});
  }

  TNode<FixedArray> phi_bb54_7;
  TNode<FixedArray> phi_bb54_10;
  TNode<FixedArray> phi_bb54_11;
  TNode<HeapObject> phi_bb54_16;
  if (block54.is_used()) {
    ca_.Bind(&block54, &phi_bb54_7, &phi_bb54_10, &phi_bb54_11, &phi_bb54_16);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb60_7;
  TNode<IntPtrT> tmp110;
  TNode<IntPtrT> tmp111;
  TNode<IntPtrT> tmp112;
  TNode<HeapObject> tmp113;
  TNode<IntPtrT> tmp114;
  TNode<Smi> tmp115;
  TNode<BoolT> tmp116;
  if (block60.is_used()) {
    ca_.Bind(&block60, &phi_bb60_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp110 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp111 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp106}, TNode<IntPtrT>{tmp110});
    tmp112 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp101}, TNode<IntPtrT>{tmp111});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp113, tmp114) = NewReference_Object_0(state_, TNode<HeapObject>{p_context}, TNode<IntPtrT>{tmp112}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 147);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp113, tmp114}, tmp100);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 150);
    tmp115 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp116 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp100}, TNode<Smi>{tmp115});
    ca_.Branch(tmp116, &block63, std::vector<Node*>{phi_bb60_7}, &block64, std::vector<Node*>{phi_bb60_7});
  }

  TNode<FixedArray> phi_bb61_7;
  if (block61.is_used()) {
    ca_.Bind(&block61, &phi_bb61_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb63_7;
  TNode<IntPtrT> tmp117;
  TNode<IntPtrT> tmp118;
  TNode<IntPtrT> tmp119;
  TNode<Smi> tmp120;
  TNode<IntPtrT> tmp121;
  TNode<IntPtrT> tmp122;
  TNode<UintPtrT> tmp123;
  TNode<UintPtrT> tmp124;
  TNode<BoolT> tmp125;
  if (block63.is_used()) {
    ca_.Bind(&block63, &phi_bb63_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 152);
    tmp117 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp118 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp119 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp120 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_context, tmp119});
    tmp121 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp120});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp122 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementCapabilitySlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp123 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp122});
    tmp124 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp121});
    tmp125 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp123}, TNode<UintPtrT>{tmp124});
    ca_.Branch(tmp125, &block69, std::vector<Node*>{phi_bb63_7}, &block70, std::vector<Node*>{phi_bb63_7});
  }

  TNode<FixedArray> phi_bb69_7;
  TNode<IntPtrT> tmp126;
  TNode<IntPtrT> tmp127;
  TNode<IntPtrT> tmp128;
  TNode<HeapObject> tmp129;
  TNode<IntPtrT> tmp130;
  TNode<Object> tmp131;
  TNode<PromiseCapability> tmp132;
  TNode<IntPtrT> tmp133;
  TNode<Object> tmp134;
  TNode<Object> tmp135;
  TNode<IntPtrT> tmp136;
  TNode<IntPtrT> tmp137;
  TNode<IntPtrT> tmp138;
  TNode<Smi> tmp139;
  TNode<IntPtrT> tmp140;
  TNode<IntPtrT> tmp141;
  TNode<UintPtrT> tmp142;
  TNode<UintPtrT> tmp143;
  TNode<BoolT> tmp144;
  if (block69.is_used()) {
    ca_.Bind(&block69, &phi_bb69_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp126 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp127 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp122}, TNode<IntPtrT>{tmp126});
    tmp128 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp117}, TNode<IntPtrT>{tmp127});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp129, tmp130) = NewReference_Object_0(state_, TNode<HeapObject>{p_context}, TNode<IntPtrT>{tmp128}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 152);
    tmp131 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp129, tmp130});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 151);
    tmp132 = UnsafeCast_PromiseCapability_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp131});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 154);
    tmp133 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp134 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp132, tmp133});
    tmp135 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp134});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 156);
    tmp136 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp137 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp138 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp139 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp6, tmp138});
    tmp140 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp139});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 156);
    tmp141 = FromConstexpr_intptr_constexpr_intptr_0(state_, Context::Field::JS_ARRAY_PACKED_ELEMENTS_MAP_INDEX);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp142 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp141});
    tmp143 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp140});
    tmp144 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp142}, TNode<UintPtrT>{tmp143});
    ca_.Branch(tmp144, &block76, std::vector<Node*>{phi_bb69_7}, &block77, std::vector<Node*>{phi_bb69_7});
  }

  TNode<FixedArray> phi_bb70_7;
  if (block70.is_used()) {
    ca_.Bind(&block70, &phi_bb70_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb76_7;
  TNode<IntPtrT> tmp145;
  TNode<IntPtrT> tmp146;
  TNode<IntPtrT> tmp147;
  TNode<HeapObject> tmp148;
  TNode<IntPtrT> tmp149;
  TNode<Object> tmp150;
  TNode<Map> tmp151;
  TNode<JSArray> tmp152;
  TNode<Oddball> tmp153;
  TNode<Object> tmp154;
  if (block76.is_used()) {
    ca_.Bind(&block76, &phi_bb76_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp145 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp146 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp141}, TNode<IntPtrT>{tmp145});
    tmp147 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp136}, TNode<IntPtrT>{tmp146});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp148, tmp149) = NewReference_Object_0(state_, TNode<HeapObject>{tmp6}, TNode<IntPtrT>{tmp147}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 156);
    tmp150 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp148, tmp149});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 155);
    tmp151 = UnsafeCast_Map_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp150});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 158);
    tmp152 = NewJSArray_0(state_, TNode<Context>{p_context}, TNode<Map>{tmp151}, TNode<FixedArrayBase>{phi_bb76_7});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 159);
    tmp153 = Undefined_0(state_);
    tmp154 = CodeStubAssembler(state_).Call(TNode<Context>{p_context}, TNode<Object>{tmp135}, TNode<Object>{tmp153}, TNode<Object>{tmp152});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 150);
    ca_.Goto(&block64, phi_bb76_7);
  }

  TNode<FixedArray> phi_bb77_7;
  if (block77.is_used()) {
    ca_.Bind(&block77, &phi_bb77_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb64_7;
  TNode<Oddball> tmp155;
  if (block64.is_used()) {
    ca_.Bind(&block64, &phi_bb64_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 161);
    tmp155 = Undefined_0(state_);
    ca_.Goto(&block1, tmp155);
  }

  TNode<Object> phi_bb1_3;
  if (block1.is_used()) {
    ca_.Bind(&block1, &phi_bb1_3);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 168);
    ca_.Goto(&block79, phi_bb1_3);
  }

  TNode<Object> phi_bb79_3;
    ca_.Bind(&block79, &phi_bb79_3);
  return TNode<Object>{phi_bb79_3};
}

TNode<Object> PromiseAllResolveElementClosure_PromiseAllSettledWrapResultAsFulfilledFunctor_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<Object> p_value, TNode<JSFunction> p_function, TorqueStructPromiseAllSettledWrapResultAsFulfilledFunctor_0 p_wrapResultFunctor, bool p_hasResolveAndRejectClosures) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block18(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block19(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block25(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block26(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block28(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block34(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block35(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block29(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block37(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, FixedArray, FixedArray, HeapObject> block45(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, FixedArray, FixedArray, HeapObject> block46(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block39(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block40(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block38(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block53(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block54(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, FixedArray, FixedArray, HeapObject> block60(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, FixedArray, FixedArray, HeapObject> block61(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block67(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block68(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block70(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block76(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block77(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block83(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block84(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block71(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block86(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<BoolT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 92);
    tmp0 = IsNativeContext_0(state_, TNode<HeapObject>{p_context});
    ca_.Branch(tmp0, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  TNode<Oddball> tmp1;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 93);
    tmp1 = Undefined_0(state_);
    ca_.Goto(&block1, tmp1);
  }

  TNode<IntPtrT> tmp2;
  TNode<Smi> tmp3;
  TNode<Smi> tmp4;
  TNode<BoolT> tmp5;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 97);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp3 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_context, tmp2});
    tmp4 = FromConstexpr_Smi_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementLength);
    tmp5 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp3}, TNode<Smi>{tmp4});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 96);
    ca_.Branch(tmp5, &block4, std::vector<Node*>{}, &block5, std::vector<Node*>{});
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    CodeStubAssembler(state_).FailAssert("Torque assert 'context.length == PromiseAllResolveElementContextSlots::kPromiseAllResolveElementLength' failed", "src/builtins/promise-all-element-closure.tq", 96);
  }

  TNode<NativeContext> tmp6;
  TNode<IntPtrT> tmp7;
  TNode<BoolT> tmp8;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 99);
    tmp6 = CodeStubAssembler(state_).LoadNativeContext(TNode<Context>{p_context});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 100);
    tmp7 = FromConstexpr_intptr_constexpr_int31_0(state_, 16);
    CodeStubAssembler(state_).StoreReference<Context>(CodeStubAssembler::Reference{p_function, tmp7}, tmp6);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 103);
    tmp8 = FromConstexpr_bool_constexpr_bool_0(state_, (CodeStubAssembler(state_).ConstexprInt31Equal(PropertyArray::kNoHashSentinel, 0)));
    ca_.Branch(tmp8, &block6, std::vector<Node*>{}, &block7, std::vector<Node*>{});
  }

  if (block7.is_used()) {
    ca_.Bind(&block7);
    CodeStubAssembler(state_).FailAssert("Torque assert 'kPropertyArrayNoHashSentinel == 0' failed", "src/builtins/promise-all-element-closure.tq", 103);
  }

  TNode<IntPtrT> tmp9;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 105);
    compiler::CodeAssemblerLabel label10(&ca_);
    tmp9 = CodeStubAssembler(state_).LoadJSReceiverIdentityHash(TNode<Object>{p_function}, &label10);
    ca_.Goto(&block10);
    if (label10.is_used()) {
      ca_.Bind(&label10);
      ca_.Goto(&block11);
    }
  }

  if (block11.is_used()) {
    ca_.Bind(&block11);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/promise-all-element-closure.tq:105:54");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp11;
  TNode<BoolT> tmp12;
  if (block10.is_used()) {
    ca_.Bind(&block10);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 106);
    tmp11 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp12 = CodeStubAssembler(state_).IntPtrGreaterThan(TNode<IntPtrT>{tmp9}, TNode<IntPtrT>{tmp11});
    ca_.Branch(tmp12, &block12, std::vector<Node*>{}, &block13, std::vector<Node*>{});
  }

  if (block13.is_used()) {
    ca_.Bind(&block13);
    CodeStubAssembler(state_).FailAssert("Torque assert 'identityHash > 0' failed", "src/builtins/promise-all-element-closure.tq", 106);
  }

  TNode<IntPtrT> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<IntPtrT> tmp15;
  TNode<IntPtrT> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<Smi> tmp18;
  TNode<IntPtrT> tmp19;
  TNode<IntPtrT> tmp20;
  TNode<UintPtrT> tmp21;
  TNode<UintPtrT> tmp22;
  TNode<BoolT> tmp23;
  if (block12.is_used()) {
    ca_.Bind(&block12);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 107);
    tmp13 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp14 = CodeStubAssembler(state_).IntPtrSub(TNode<IntPtrT>{tmp9}, TNode<IntPtrT>{tmp13});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 110);
    tmp15 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp16 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp17 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp18 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_context, tmp17});
    tmp19 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp18});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp20 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp21 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp20});
    tmp22 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp19});
    tmp23 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp21}, TNode<UintPtrT>{tmp22});
    ca_.Branch(tmp23, &block18, std::vector<Node*>{}, &block19, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp24;
  TNode<IntPtrT> tmp25;
  TNode<IntPtrT> tmp26;
  TNode<HeapObject> tmp27;
  TNode<IntPtrT> tmp28;
  TNode<Object> tmp29;
  TNode<Smi> tmp30;
  TNode<IntPtrT> tmp31;
  TNode<IntPtrT> tmp32;
  TNode<IntPtrT> tmp33;
  TNode<Smi> tmp34;
  TNode<IntPtrT> tmp35;
  TNode<IntPtrT> tmp36;
  TNode<UintPtrT> tmp37;
  TNode<UintPtrT> tmp38;
  TNode<BoolT> tmp39;
  if (block18.is_used()) {
    ca_.Bind(&block18);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp24 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp25 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp20}, TNode<IntPtrT>{tmp24});
    tmp26 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp15}, TNode<IntPtrT>{tmp25});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp27, tmp28) = NewReference_Object_0(state_, TNode<HeapObject>{p_context}, TNode<IntPtrT>{tmp26}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 110);
    tmp29 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp27, tmp28});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 109);
    tmp30 = UnsafeCast_Smi_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp29});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 114);
    tmp31 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp32 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp33 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp34 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_context, tmp33});
    tmp35 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp34});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp36 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementValuesSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp37 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp36});
    tmp38 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp35});
    tmp39 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp37}, TNode<UintPtrT>{tmp38});
    ca_.Branch(tmp39, &block25, std::vector<Node*>{}, &block26, std::vector<Node*>{});
  }

  if (block19.is_used()) {
    ca_.Bind(&block19);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp40;
  TNode<IntPtrT> tmp41;
  TNode<IntPtrT> tmp42;
  TNode<HeapObject> tmp43;
  TNode<IntPtrT> tmp44;
  TNode<Object> tmp45;
  TNode<FixedArray> tmp46;
  TNode<IntPtrT> tmp47;
  TNode<IntPtrT> tmp48;
  TNode<IntPtrT> tmp49;
  TNode<BoolT> tmp50;
  if (block25.is_used()) {
    ca_.Bind(&block25);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp40 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp41 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp36}, TNode<IntPtrT>{tmp40});
    tmp42 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp31}, TNode<IntPtrT>{tmp41});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp43, tmp44) = NewReference_Object_0(state_, TNode<HeapObject>{p_context}, TNode<IntPtrT>{tmp42}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 114);
    tmp45 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp43, tmp44});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 113);
    tmp46 = UnsafeCast_FixedArray_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp45});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 116);
    tmp47 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp48 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp14}, TNode<IntPtrT>{tmp47});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 117);
    tmp49 = CodeStubAssembler(state_).LoadAndUntagFixedArrayBaseLength(TNode<FixedArrayBase>{tmp46});
    tmp50 = CodeStubAssembler(state_).IntPtrGreaterThan(TNode<IntPtrT>{tmp48}, TNode<IntPtrT>{tmp49});
    ca_.Branch(tmp50, &block28, std::vector<Node*>{}, &block29, std::vector<Node*>{tmp46});
  }

  if (block26.is_used()) {
    ca_.Bind(&block26);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp51;
  TNode<IntPtrT> tmp52;
  TNode<FixedArray> tmp53;
  TNode<IntPtrT> tmp54;
  TNode<IntPtrT> tmp55;
  TNode<IntPtrT> tmp56;
  TNode<Smi> tmp57;
  TNode<IntPtrT> tmp58;
  TNode<IntPtrT> tmp59;
  TNode<UintPtrT> tmp60;
  TNode<UintPtrT> tmp61;
  TNode<BoolT> tmp62;
  if (block28.is_used()) {
    ca_.Bind(&block28);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 119);
    tmp51 = CodeStubAssembler(state_).LoadAndUntagFixedArrayBaseLength(TNode<FixedArrayBase>{tmp46});
    tmp52 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp53 = ExtractFixedArray_0(state_, TNode<FixedArray>{tmp46}, TNode<IntPtrT>{tmp52}, TNode<IntPtrT>{tmp51}, TNode<IntPtrT>{tmp48});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 120);
    tmp54 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp55 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp56 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp57 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_context, tmp56});
    tmp58 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp57});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp59 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementValuesSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp60 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp59});
    tmp61 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp58});
    tmp62 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp60}, TNode<UintPtrT>{tmp61});
    ca_.Branch(tmp62, &block34, std::vector<Node*>{}, &block35, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp63;
  TNode<IntPtrT> tmp64;
  TNode<IntPtrT> tmp65;
  TNode<HeapObject> tmp66;
  TNode<IntPtrT> tmp67;
  if (block34.is_used()) {
    ca_.Bind(&block34);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp63 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp64 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp59}, TNode<IntPtrT>{tmp63});
    tmp65 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp54}, TNode<IntPtrT>{tmp64});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp66, tmp67) = NewReference_Object_0(state_, TNode<HeapObject>{p_context}, TNode<IntPtrT>{tmp65}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 120);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp66, tmp67}, tmp53);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 117);
    ca_.Goto(&block29, tmp53);
  }

  if (block35.is_used()) {
    ca_.Bind(&block35);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb29_7;
  TNode<BoolT> tmp68;
  if (block29.is_used()) {
    ca_.Bind(&block29, &phi_bb29_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 134);
    tmp68 = FromConstexpr_bool_constexpr_bool_0(state_, p_hasResolveAndRejectClosures);
    ca_.Branch(tmp68, &block37, std::vector<Node*>{phi_bb29_7}, &block38, std::vector<Node*>{phi_bb29_7});
  }

  TNode<FixedArray> phi_bb37_7;
  TNode<IntPtrT> tmp69;
  TNode<IntPtrT> tmp70;
  TNode<IntPtrT> tmp71;
  TNode<Smi> tmp72;
  TNode<IntPtrT> tmp73;
  TNode<UintPtrT> tmp74;
  TNode<UintPtrT> tmp75;
  TNode<BoolT> tmp76;
  if (block37.is_used()) {
    ca_.Bind(&block37, &phi_bb37_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 135);
    tmp69 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp70 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp71 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp72 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{phi_bb37_7, tmp71});
    tmp73 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp72});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp74 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp14});
    tmp75 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp73});
    tmp76 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp74}, TNode<UintPtrT>{tmp75});
    ca_.Branch(tmp76, &block45, std::vector<Node*>{phi_bb37_7, phi_bb37_7, phi_bb37_7, phi_bb37_7}, &block46, std::vector<Node*>{phi_bb37_7, phi_bb37_7, phi_bb37_7, phi_bb37_7});
  }

  TNode<FixedArray> phi_bb45_7;
  TNode<FixedArray> phi_bb45_9;
  TNode<FixedArray> phi_bb45_10;
  TNode<HeapObject> phi_bb45_15;
  TNode<IntPtrT> tmp77;
  TNode<IntPtrT> tmp78;
  TNode<IntPtrT> tmp79;
  TNode<HeapObject> tmp80;
  TNode<IntPtrT> tmp81;
  TNode<Object> tmp82;
  TNode<Oddball> tmp83;
  TNode<BoolT> tmp84;
  if (block45.is_used()) {
    ca_.Bind(&block45, &phi_bb45_7, &phi_bb45_9, &phi_bb45_10, &phi_bb45_15);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp77 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp78 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp14}, TNode<IntPtrT>{tmp77});
    tmp79 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp69}, TNode<IntPtrT>{tmp78});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp80, tmp81) = NewReference_Object_0(state_, TNode<HeapObject>{phi_bb45_15}, TNode<IntPtrT>{tmp79}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 135);
    tmp82 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp80, tmp81});
    tmp83 = TheHole_0(state_);
    tmp84 = CodeStubAssembler(state_).TaggedNotEqual(TNode<Object>{tmp82}, TNode<HeapObject>{tmp83});
    ca_.Branch(tmp84, &block39, std::vector<Node*>{phi_bb45_7}, &block40, std::vector<Node*>{phi_bb45_7});
  }

  TNode<FixedArray> phi_bb46_7;
  TNode<FixedArray> phi_bb46_9;
  TNode<FixedArray> phi_bb46_10;
  TNode<HeapObject> phi_bb46_15;
  if (block46.is_used()) {
    ca_.Bind(&block46, &phi_bb46_7, &phi_bb46_9, &phi_bb46_10, &phi_bb46_15);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb39_7;
  TNode<Oddball> tmp85;
  if (block39.is_used()) {
    ca_.Bind(&block39, &phi_bb39_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 136);
    tmp85 = Undefined_0(state_);
    ca_.Goto(&block1, tmp85);
  }

  TNode<FixedArray> phi_bb40_7;
  if (block40.is_used()) {
    ca_.Bind(&block40, &phi_bb40_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 134);
    ca_.Goto(&block38, phi_bb40_7);
  }

  TNode<FixedArray> phi_bb38_7;
  TNode<IntPtrT> tmp86;
  TNode<IntPtrT> tmp87;
  TNode<IntPtrT> tmp88;
  TNode<Smi> tmp89;
  TNode<IntPtrT> tmp90;
  TNode<IntPtrT> tmp91;
  TNode<UintPtrT> tmp92;
  TNode<UintPtrT> tmp93;
  TNode<BoolT> tmp94;
  if (block38.is_used()) {
    ca_.Bind(&block38, &phi_bb38_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 25);
    tmp86 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp87 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp88 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp89 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp6, tmp88});
    tmp90 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp89});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 25);
    tmp91 = FromConstexpr_intptr_constexpr_intptr_0(state_, Context::Field::OBJECT_FUNCTION_INDEX);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp92 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp91});
    tmp93 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp90});
    tmp94 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp92}, TNode<UintPtrT>{tmp93});
    ca_.Branch(tmp94, &block53, std::vector<Node*>{phi_bb38_7}, &block54, std::vector<Node*>{phi_bb38_7});
  }

  TNode<FixedArray> phi_bb53_7;
  TNode<IntPtrT> tmp95;
  TNode<IntPtrT> tmp96;
  TNode<IntPtrT> tmp97;
  TNode<HeapObject> tmp98;
  TNode<IntPtrT> tmp99;
  TNode<Object> tmp100;
  TNode<JSFunction> tmp101;
  TNode<IntPtrT> tmp102;
  TNode<HeapObject> tmp103;
  TNode<Map> tmp104;
  TNode<JSObject> tmp105;
  TNode<String> tmp106;
  TNode<String> tmp107;
  TNode<Object> tmp108;
  TNode<String> tmp109;
  TNode<Object> tmp110;
  TNode<IntPtrT> tmp111;
  TNode<IntPtrT> tmp112;
  TNode<IntPtrT> tmp113;
  TNode<Smi> tmp114;
  TNode<IntPtrT> tmp115;
  TNode<UintPtrT> tmp116;
  TNode<UintPtrT> tmp117;
  TNode<BoolT> tmp118;
  if (block53.is_used()) {
    ca_.Bind(&block53, &phi_bb53_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp95 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp96 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp91}, TNode<IntPtrT>{tmp95});
    tmp97 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp86}, TNode<IntPtrT>{tmp96});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp98, tmp99) = NewReference_Object_0(state_, TNode<HeapObject>{tmp6}, TNode<IntPtrT>{tmp97}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 25);
    tmp100 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp98, tmp99});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 24);
    tmp101 = UnsafeCast_JSFunction_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp100});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 27);
    tmp102 = FromConstexpr_intptr_constexpr_int31_0(state_, 28);
    tmp103 = CodeStubAssembler(state_).LoadReference<HeapObject>(CodeStubAssembler::Reference{tmp101, tmp102});
    tmp104 = UnsafeCast_Map_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp103});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 28);
    tmp105 = CodeStubAssembler(state_).AllocateJSObjectFromMap(TNode<Map>{tmp104});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 32);
    tmp106 = CodeStubAssembler(state_).StringConstant("status");
    tmp107 = CodeStubAssembler(state_).StringConstant("fulfilled");
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 31);
    tmp108 = CodeStubAssembler(state_).CallBuiltin(Builtins::kFastCreateDataProperty, p_context, tmp105, tmp106, tmp107);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 35);
    tmp109 = CodeStubAssembler(state_).StringConstant("value");
    tmp110 = CodeStubAssembler(state_).CallBuiltin(Builtins::kFastCreateDataProperty, p_context, tmp105, tmp109, p_value);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 144);
    tmp111 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp112 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp113 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp114 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{phi_bb53_7, tmp113});
    tmp115 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp114});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp116 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp14});
    tmp117 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp115});
    tmp118 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp116}, TNode<UintPtrT>{tmp117});
    ca_.Branch(tmp118, &block60, std::vector<Node*>{phi_bb53_7, phi_bb53_7, phi_bb53_7, phi_bb53_7}, &block61, std::vector<Node*>{phi_bb53_7, phi_bb53_7, phi_bb53_7, phi_bb53_7});
  }

  TNode<FixedArray> phi_bb54_7;
  if (block54.is_used()) {
    ca_.Bind(&block54, &phi_bb54_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb60_7;
  TNode<FixedArray> phi_bb60_10;
  TNode<FixedArray> phi_bb60_11;
  TNode<HeapObject> phi_bb60_16;
  TNode<IntPtrT> tmp119;
  TNode<IntPtrT> tmp120;
  TNode<IntPtrT> tmp121;
  TNode<HeapObject> tmp122;
  TNode<IntPtrT> tmp123;
  TNode<Smi> tmp124;
  TNode<Smi> tmp125;
  TNode<IntPtrT> tmp126;
  TNode<IntPtrT> tmp127;
  TNode<IntPtrT> tmp128;
  TNode<Smi> tmp129;
  TNode<IntPtrT> tmp130;
  TNode<IntPtrT> tmp131;
  TNode<UintPtrT> tmp132;
  TNode<UintPtrT> tmp133;
  TNode<BoolT> tmp134;
  if (block60.is_used()) {
    ca_.Bind(&block60, &phi_bb60_7, &phi_bb60_10, &phi_bb60_11, &phi_bb60_16);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp119 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp120 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp14}, TNode<IntPtrT>{tmp119});
    tmp121 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp111}, TNode<IntPtrT>{tmp120});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp122, tmp123) = NewReference_Object_0(state_, TNode<HeapObject>{phi_bb60_16}, TNode<IntPtrT>{tmp121}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 144);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp122, tmp123}, tmp105);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 146);
    tmp124 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp125 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp30}, TNode<Smi>{tmp124});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 147);
    tmp126 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp127 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp128 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp129 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_context, tmp128});
    tmp130 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp129});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp131 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp132 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp131});
    tmp133 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp130});
    tmp134 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp132}, TNode<UintPtrT>{tmp133});
    ca_.Branch(tmp134, &block67, std::vector<Node*>{phi_bb60_7}, &block68, std::vector<Node*>{phi_bb60_7});
  }

  TNode<FixedArray> phi_bb61_7;
  TNode<FixedArray> phi_bb61_10;
  TNode<FixedArray> phi_bb61_11;
  TNode<HeapObject> phi_bb61_16;
  if (block61.is_used()) {
    ca_.Bind(&block61, &phi_bb61_7, &phi_bb61_10, &phi_bb61_11, &phi_bb61_16);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb67_7;
  TNode<IntPtrT> tmp135;
  TNode<IntPtrT> tmp136;
  TNode<IntPtrT> tmp137;
  TNode<HeapObject> tmp138;
  TNode<IntPtrT> tmp139;
  TNode<Smi> tmp140;
  TNode<BoolT> tmp141;
  if (block67.is_used()) {
    ca_.Bind(&block67, &phi_bb67_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp135 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp136 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp131}, TNode<IntPtrT>{tmp135});
    tmp137 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp126}, TNode<IntPtrT>{tmp136});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp138, tmp139) = NewReference_Object_0(state_, TNode<HeapObject>{p_context}, TNode<IntPtrT>{tmp137}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 147);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp138, tmp139}, tmp125);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 150);
    tmp140 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp141 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp125}, TNode<Smi>{tmp140});
    ca_.Branch(tmp141, &block70, std::vector<Node*>{phi_bb67_7}, &block71, std::vector<Node*>{phi_bb67_7});
  }

  TNode<FixedArray> phi_bb68_7;
  if (block68.is_used()) {
    ca_.Bind(&block68, &phi_bb68_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb70_7;
  TNode<IntPtrT> tmp142;
  TNode<IntPtrT> tmp143;
  TNode<IntPtrT> tmp144;
  TNode<Smi> tmp145;
  TNode<IntPtrT> tmp146;
  TNode<IntPtrT> tmp147;
  TNode<UintPtrT> tmp148;
  TNode<UintPtrT> tmp149;
  TNode<BoolT> tmp150;
  if (block70.is_used()) {
    ca_.Bind(&block70, &phi_bb70_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 152);
    tmp142 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp143 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp144 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp145 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_context, tmp144});
    tmp146 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp145});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp147 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementCapabilitySlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp148 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp147});
    tmp149 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp146});
    tmp150 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp148}, TNode<UintPtrT>{tmp149});
    ca_.Branch(tmp150, &block76, std::vector<Node*>{phi_bb70_7}, &block77, std::vector<Node*>{phi_bb70_7});
  }

  TNode<FixedArray> phi_bb76_7;
  TNode<IntPtrT> tmp151;
  TNode<IntPtrT> tmp152;
  TNode<IntPtrT> tmp153;
  TNode<HeapObject> tmp154;
  TNode<IntPtrT> tmp155;
  TNode<Object> tmp156;
  TNode<PromiseCapability> tmp157;
  TNode<IntPtrT> tmp158;
  TNode<Object> tmp159;
  TNode<Object> tmp160;
  TNode<IntPtrT> tmp161;
  TNode<IntPtrT> tmp162;
  TNode<IntPtrT> tmp163;
  TNode<Smi> tmp164;
  TNode<IntPtrT> tmp165;
  TNode<IntPtrT> tmp166;
  TNode<UintPtrT> tmp167;
  TNode<UintPtrT> tmp168;
  TNode<BoolT> tmp169;
  if (block76.is_used()) {
    ca_.Bind(&block76, &phi_bb76_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp151 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp152 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp147}, TNode<IntPtrT>{tmp151});
    tmp153 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp142}, TNode<IntPtrT>{tmp152});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp154, tmp155) = NewReference_Object_0(state_, TNode<HeapObject>{p_context}, TNode<IntPtrT>{tmp153}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 152);
    tmp156 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp154, tmp155});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 151);
    tmp157 = UnsafeCast_PromiseCapability_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp156});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 154);
    tmp158 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp159 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp157, tmp158});
    tmp160 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp159});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 156);
    tmp161 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp162 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp163 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp164 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp6, tmp163});
    tmp165 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp164});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 156);
    tmp166 = FromConstexpr_intptr_constexpr_intptr_0(state_, Context::Field::JS_ARRAY_PACKED_ELEMENTS_MAP_INDEX);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp167 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp166});
    tmp168 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp165});
    tmp169 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp167}, TNode<UintPtrT>{tmp168});
    ca_.Branch(tmp169, &block83, std::vector<Node*>{phi_bb76_7}, &block84, std::vector<Node*>{phi_bb76_7});
  }

  TNode<FixedArray> phi_bb77_7;
  if (block77.is_used()) {
    ca_.Bind(&block77, &phi_bb77_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb83_7;
  TNode<IntPtrT> tmp170;
  TNode<IntPtrT> tmp171;
  TNode<IntPtrT> tmp172;
  TNode<HeapObject> tmp173;
  TNode<IntPtrT> tmp174;
  TNode<Object> tmp175;
  TNode<Map> tmp176;
  TNode<JSArray> tmp177;
  TNode<Oddball> tmp178;
  TNode<Object> tmp179;
  if (block83.is_used()) {
    ca_.Bind(&block83, &phi_bb83_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp170 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp171 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp166}, TNode<IntPtrT>{tmp170});
    tmp172 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp161}, TNode<IntPtrT>{tmp171});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp173, tmp174) = NewReference_Object_0(state_, TNode<HeapObject>{tmp6}, TNode<IntPtrT>{tmp172}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 156);
    tmp175 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp173, tmp174});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 155);
    tmp176 = UnsafeCast_Map_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp175});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 158);
    tmp177 = NewJSArray_0(state_, TNode<Context>{p_context}, TNode<Map>{tmp176}, TNode<FixedArrayBase>{phi_bb83_7});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 159);
    tmp178 = Undefined_0(state_);
    tmp179 = CodeStubAssembler(state_).Call(TNode<Context>{p_context}, TNode<Object>{tmp160}, TNode<Object>{tmp178}, TNode<Object>{tmp177});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 150);
    ca_.Goto(&block71, phi_bb83_7);
  }

  TNode<FixedArray> phi_bb84_7;
  if (block84.is_used()) {
    ca_.Bind(&block84, &phi_bb84_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb71_7;
  TNode<Oddball> tmp180;
  if (block71.is_used()) {
    ca_.Bind(&block71, &phi_bb71_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 161);
    tmp180 = Undefined_0(state_);
    ca_.Goto(&block1, tmp180);
  }

  TNode<Object> phi_bb1_3;
  if (block1.is_used()) {
    ca_.Bind(&block1, &phi_bb1_3);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 176);
    ca_.Goto(&block86, phi_bb1_3);
  }

  TNode<Object> phi_bb86_3;
    ca_.Bind(&block86, &phi_bb86_3);
  return TNode<Object>{phi_bb86_3};
}

TNode<Object> PromiseAllResolveElementClosure_PromiseAllSettledWrapResultAsRejectedFunctor_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<Object> p_value, TNode<JSFunction> p_function, TorqueStructPromiseAllSettledWrapResultAsRejectedFunctor_0 p_wrapResultFunctor, bool p_hasResolveAndRejectClosures) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block18(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block19(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block25(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block26(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block28(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block34(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block35(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block29(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block37(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, FixedArray, FixedArray, HeapObject> block45(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, FixedArray, FixedArray, HeapObject> block46(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block39(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block40(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block38(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block53(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block54(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, FixedArray, FixedArray, HeapObject> block60(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, FixedArray, FixedArray, HeapObject> block61(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block67(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block68(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block70(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block76(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block77(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block83(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block84(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block71(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block86(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<BoolT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 92);
    tmp0 = IsNativeContext_0(state_, TNode<HeapObject>{p_context});
    ca_.Branch(tmp0, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  TNode<Oddball> tmp1;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 93);
    tmp1 = Undefined_0(state_);
    ca_.Goto(&block1, tmp1);
  }

  TNode<IntPtrT> tmp2;
  TNode<Smi> tmp3;
  TNode<Smi> tmp4;
  TNode<BoolT> tmp5;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 97);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp3 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_context, tmp2});
    tmp4 = FromConstexpr_Smi_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementLength);
    tmp5 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp3}, TNode<Smi>{tmp4});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 96);
    ca_.Branch(tmp5, &block4, std::vector<Node*>{}, &block5, std::vector<Node*>{});
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    CodeStubAssembler(state_).FailAssert("Torque assert 'context.length == PromiseAllResolveElementContextSlots::kPromiseAllResolveElementLength' failed", "src/builtins/promise-all-element-closure.tq", 96);
  }

  TNode<NativeContext> tmp6;
  TNode<IntPtrT> tmp7;
  TNode<BoolT> tmp8;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 99);
    tmp6 = CodeStubAssembler(state_).LoadNativeContext(TNode<Context>{p_context});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 100);
    tmp7 = FromConstexpr_intptr_constexpr_int31_0(state_, 16);
    CodeStubAssembler(state_).StoreReference<Context>(CodeStubAssembler::Reference{p_function, tmp7}, tmp6);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 103);
    tmp8 = FromConstexpr_bool_constexpr_bool_0(state_, (CodeStubAssembler(state_).ConstexprInt31Equal(PropertyArray::kNoHashSentinel, 0)));
    ca_.Branch(tmp8, &block6, std::vector<Node*>{}, &block7, std::vector<Node*>{});
  }

  if (block7.is_used()) {
    ca_.Bind(&block7);
    CodeStubAssembler(state_).FailAssert("Torque assert 'kPropertyArrayNoHashSentinel == 0' failed", "src/builtins/promise-all-element-closure.tq", 103);
  }

  TNode<IntPtrT> tmp9;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 105);
    compiler::CodeAssemblerLabel label10(&ca_);
    tmp9 = CodeStubAssembler(state_).LoadJSReceiverIdentityHash(TNode<Object>{p_function}, &label10);
    ca_.Goto(&block10);
    if (label10.is_used()) {
      ca_.Bind(&label10);
      ca_.Goto(&block11);
    }
  }

  if (block11.is_used()) {
    ca_.Bind(&block11);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/promise-all-element-closure.tq:105:54");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp11;
  TNode<BoolT> tmp12;
  if (block10.is_used()) {
    ca_.Bind(&block10);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 106);
    tmp11 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp12 = CodeStubAssembler(state_).IntPtrGreaterThan(TNode<IntPtrT>{tmp9}, TNode<IntPtrT>{tmp11});
    ca_.Branch(tmp12, &block12, std::vector<Node*>{}, &block13, std::vector<Node*>{});
  }

  if (block13.is_used()) {
    ca_.Bind(&block13);
    CodeStubAssembler(state_).FailAssert("Torque assert 'identityHash > 0' failed", "src/builtins/promise-all-element-closure.tq", 106);
  }

  TNode<IntPtrT> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<IntPtrT> tmp15;
  TNode<IntPtrT> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<Smi> tmp18;
  TNode<IntPtrT> tmp19;
  TNode<IntPtrT> tmp20;
  TNode<UintPtrT> tmp21;
  TNode<UintPtrT> tmp22;
  TNode<BoolT> tmp23;
  if (block12.is_used()) {
    ca_.Bind(&block12);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 107);
    tmp13 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp14 = CodeStubAssembler(state_).IntPtrSub(TNode<IntPtrT>{tmp9}, TNode<IntPtrT>{tmp13});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 110);
    tmp15 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp16 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp17 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp18 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_context, tmp17});
    tmp19 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp18});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp20 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp21 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp20});
    tmp22 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp19});
    tmp23 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp21}, TNode<UintPtrT>{tmp22});
    ca_.Branch(tmp23, &block18, std::vector<Node*>{}, &block19, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp24;
  TNode<IntPtrT> tmp25;
  TNode<IntPtrT> tmp26;
  TNode<HeapObject> tmp27;
  TNode<IntPtrT> tmp28;
  TNode<Object> tmp29;
  TNode<Smi> tmp30;
  TNode<IntPtrT> tmp31;
  TNode<IntPtrT> tmp32;
  TNode<IntPtrT> tmp33;
  TNode<Smi> tmp34;
  TNode<IntPtrT> tmp35;
  TNode<IntPtrT> tmp36;
  TNode<UintPtrT> tmp37;
  TNode<UintPtrT> tmp38;
  TNode<BoolT> tmp39;
  if (block18.is_used()) {
    ca_.Bind(&block18);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp24 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp25 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp20}, TNode<IntPtrT>{tmp24});
    tmp26 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp15}, TNode<IntPtrT>{tmp25});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp27, tmp28) = NewReference_Object_0(state_, TNode<HeapObject>{p_context}, TNode<IntPtrT>{tmp26}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 110);
    tmp29 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp27, tmp28});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 109);
    tmp30 = UnsafeCast_Smi_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp29});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 114);
    tmp31 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp32 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp33 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp34 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_context, tmp33});
    tmp35 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp34});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp36 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementValuesSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp37 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp36});
    tmp38 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp35});
    tmp39 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp37}, TNode<UintPtrT>{tmp38});
    ca_.Branch(tmp39, &block25, std::vector<Node*>{}, &block26, std::vector<Node*>{});
  }

  if (block19.is_used()) {
    ca_.Bind(&block19);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp40;
  TNode<IntPtrT> tmp41;
  TNode<IntPtrT> tmp42;
  TNode<HeapObject> tmp43;
  TNode<IntPtrT> tmp44;
  TNode<Object> tmp45;
  TNode<FixedArray> tmp46;
  TNode<IntPtrT> tmp47;
  TNode<IntPtrT> tmp48;
  TNode<IntPtrT> tmp49;
  TNode<BoolT> tmp50;
  if (block25.is_used()) {
    ca_.Bind(&block25);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp40 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp41 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp36}, TNode<IntPtrT>{tmp40});
    tmp42 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp31}, TNode<IntPtrT>{tmp41});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp43, tmp44) = NewReference_Object_0(state_, TNode<HeapObject>{p_context}, TNode<IntPtrT>{tmp42}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 114);
    tmp45 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp43, tmp44});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 113);
    tmp46 = UnsafeCast_FixedArray_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp45});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 116);
    tmp47 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp48 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp14}, TNode<IntPtrT>{tmp47});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 117);
    tmp49 = CodeStubAssembler(state_).LoadAndUntagFixedArrayBaseLength(TNode<FixedArrayBase>{tmp46});
    tmp50 = CodeStubAssembler(state_).IntPtrGreaterThan(TNode<IntPtrT>{tmp48}, TNode<IntPtrT>{tmp49});
    ca_.Branch(tmp50, &block28, std::vector<Node*>{}, &block29, std::vector<Node*>{tmp46});
  }

  if (block26.is_used()) {
    ca_.Bind(&block26);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp51;
  TNode<IntPtrT> tmp52;
  TNode<FixedArray> tmp53;
  TNode<IntPtrT> tmp54;
  TNode<IntPtrT> tmp55;
  TNode<IntPtrT> tmp56;
  TNode<Smi> tmp57;
  TNode<IntPtrT> tmp58;
  TNode<IntPtrT> tmp59;
  TNode<UintPtrT> tmp60;
  TNode<UintPtrT> tmp61;
  TNode<BoolT> tmp62;
  if (block28.is_used()) {
    ca_.Bind(&block28);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 119);
    tmp51 = CodeStubAssembler(state_).LoadAndUntagFixedArrayBaseLength(TNode<FixedArrayBase>{tmp46});
    tmp52 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp53 = ExtractFixedArray_0(state_, TNode<FixedArray>{tmp46}, TNode<IntPtrT>{tmp52}, TNode<IntPtrT>{tmp51}, TNode<IntPtrT>{tmp48});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 120);
    tmp54 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp55 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp56 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp57 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_context, tmp56});
    tmp58 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp57});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp59 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementValuesSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp60 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp59});
    tmp61 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp58});
    tmp62 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp60}, TNode<UintPtrT>{tmp61});
    ca_.Branch(tmp62, &block34, std::vector<Node*>{}, &block35, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp63;
  TNode<IntPtrT> tmp64;
  TNode<IntPtrT> tmp65;
  TNode<HeapObject> tmp66;
  TNode<IntPtrT> tmp67;
  if (block34.is_used()) {
    ca_.Bind(&block34);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp63 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp64 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp59}, TNode<IntPtrT>{tmp63});
    tmp65 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp54}, TNode<IntPtrT>{tmp64});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp66, tmp67) = NewReference_Object_0(state_, TNode<HeapObject>{p_context}, TNode<IntPtrT>{tmp65}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 120);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp66, tmp67}, tmp53);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 117);
    ca_.Goto(&block29, tmp53);
  }

  if (block35.is_used()) {
    ca_.Bind(&block35);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb29_7;
  TNode<BoolT> tmp68;
  if (block29.is_used()) {
    ca_.Bind(&block29, &phi_bb29_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 134);
    tmp68 = FromConstexpr_bool_constexpr_bool_0(state_, p_hasResolveAndRejectClosures);
    ca_.Branch(tmp68, &block37, std::vector<Node*>{phi_bb29_7}, &block38, std::vector<Node*>{phi_bb29_7});
  }

  TNode<FixedArray> phi_bb37_7;
  TNode<IntPtrT> tmp69;
  TNode<IntPtrT> tmp70;
  TNode<IntPtrT> tmp71;
  TNode<Smi> tmp72;
  TNode<IntPtrT> tmp73;
  TNode<UintPtrT> tmp74;
  TNode<UintPtrT> tmp75;
  TNode<BoolT> tmp76;
  if (block37.is_used()) {
    ca_.Bind(&block37, &phi_bb37_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 135);
    tmp69 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp70 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp71 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp72 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{phi_bb37_7, tmp71});
    tmp73 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp72});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp74 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp14});
    tmp75 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp73});
    tmp76 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp74}, TNode<UintPtrT>{tmp75});
    ca_.Branch(tmp76, &block45, std::vector<Node*>{phi_bb37_7, phi_bb37_7, phi_bb37_7, phi_bb37_7}, &block46, std::vector<Node*>{phi_bb37_7, phi_bb37_7, phi_bb37_7, phi_bb37_7});
  }

  TNode<FixedArray> phi_bb45_7;
  TNode<FixedArray> phi_bb45_9;
  TNode<FixedArray> phi_bb45_10;
  TNode<HeapObject> phi_bb45_15;
  TNode<IntPtrT> tmp77;
  TNode<IntPtrT> tmp78;
  TNode<IntPtrT> tmp79;
  TNode<HeapObject> tmp80;
  TNode<IntPtrT> tmp81;
  TNode<Object> tmp82;
  TNode<Oddball> tmp83;
  TNode<BoolT> tmp84;
  if (block45.is_used()) {
    ca_.Bind(&block45, &phi_bb45_7, &phi_bb45_9, &phi_bb45_10, &phi_bb45_15);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp77 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp78 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp14}, TNode<IntPtrT>{tmp77});
    tmp79 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp69}, TNode<IntPtrT>{tmp78});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp80, tmp81) = NewReference_Object_0(state_, TNode<HeapObject>{phi_bb45_15}, TNode<IntPtrT>{tmp79}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 135);
    tmp82 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp80, tmp81});
    tmp83 = TheHole_0(state_);
    tmp84 = CodeStubAssembler(state_).TaggedNotEqual(TNode<Object>{tmp82}, TNode<HeapObject>{tmp83});
    ca_.Branch(tmp84, &block39, std::vector<Node*>{phi_bb45_7}, &block40, std::vector<Node*>{phi_bb45_7});
  }

  TNode<FixedArray> phi_bb46_7;
  TNode<FixedArray> phi_bb46_9;
  TNode<FixedArray> phi_bb46_10;
  TNode<HeapObject> phi_bb46_15;
  if (block46.is_used()) {
    ca_.Bind(&block46, &phi_bb46_7, &phi_bb46_9, &phi_bb46_10, &phi_bb46_15);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb39_7;
  TNode<Oddball> tmp85;
  if (block39.is_used()) {
    ca_.Bind(&block39, &phi_bb39_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 136);
    tmp85 = Undefined_0(state_);
    ca_.Goto(&block1, tmp85);
  }

  TNode<FixedArray> phi_bb40_7;
  if (block40.is_used()) {
    ca_.Bind(&block40, &phi_bb40_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 134);
    ca_.Goto(&block38, phi_bb40_7);
  }

  TNode<FixedArray> phi_bb38_7;
  TNode<IntPtrT> tmp86;
  TNode<IntPtrT> tmp87;
  TNode<IntPtrT> tmp88;
  TNode<Smi> tmp89;
  TNode<IntPtrT> tmp90;
  TNode<IntPtrT> tmp91;
  TNode<UintPtrT> tmp92;
  TNode<UintPtrT> tmp93;
  TNode<BoolT> tmp94;
  if (block38.is_used()) {
    ca_.Bind(&block38, &phi_bb38_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 48);
    tmp86 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp87 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp88 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp89 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp6, tmp88});
    tmp90 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp89});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 48);
    tmp91 = FromConstexpr_intptr_constexpr_intptr_0(state_, Context::Field::OBJECT_FUNCTION_INDEX);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp92 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp91});
    tmp93 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp90});
    tmp94 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp92}, TNode<UintPtrT>{tmp93});
    ca_.Branch(tmp94, &block53, std::vector<Node*>{phi_bb38_7}, &block54, std::vector<Node*>{phi_bb38_7});
  }

  TNode<FixedArray> phi_bb53_7;
  TNode<IntPtrT> tmp95;
  TNode<IntPtrT> tmp96;
  TNode<IntPtrT> tmp97;
  TNode<HeapObject> tmp98;
  TNode<IntPtrT> tmp99;
  TNode<Object> tmp100;
  TNode<JSFunction> tmp101;
  TNode<IntPtrT> tmp102;
  TNode<HeapObject> tmp103;
  TNode<Map> tmp104;
  TNode<JSObject> tmp105;
  TNode<String> tmp106;
  TNode<String> tmp107;
  TNode<Object> tmp108;
  TNode<String> tmp109;
  TNode<Object> tmp110;
  TNode<IntPtrT> tmp111;
  TNode<IntPtrT> tmp112;
  TNode<IntPtrT> tmp113;
  TNode<Smi> tmp114;
  TNode<IntPtrT> tmp115;
  TNode<UintPtrT> tmp116;
  TNode<UintPtrT> tmp117;
  TNode<BoolT> tmp118;
  if (block53.is_used()) {
    ca_.Bind(&block53, &phi_bb53_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp95 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp96 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp91}, TNode<IntPtrT>{tmp95});
    tmp97 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp86}, TNode<IntPtrT>{tmp96});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp98, tmp99) = NewReference_Object_0(state_, TNode<HeapObject>{tmp6}, TNode<IntPtrT>{tmp97}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 48);
    tmp100 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp98, tmp99});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 47);
    tmp101 = UnsafeCast_JSFunction_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp100});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 50);
    tmp102 = FromConstexpr_intptr_constexpr_int31_0(state_, 28);
    tmp103 = CodeStubAssembler(state_).LoadReference<HeapObject>(CodeStubAssembler::Reference{tmp101, tmp102});
    tmp104 = UnsafeCast_Map_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp103});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 51);
    tmp105 = CodeStubAssembler(state_).AllocateJSObjectFromMap(TNode<Map>{tmp104});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 55);
    tmp106 = CodeStubAssembler(state_).StringConstant("status");
    tmp107 = CodeStubAssembler(state_).StringConstant("rejected");
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 54);
    tmp108 = CodeStubAssembler(state_).CallBuiltin(Builtins::kFastCreateDataProperty, p_context, tmp105, tmp106, tmp107);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 58);
    tmp109 = CodeStubAssembler(state_).StringConstant("reason");
    tmp110 = CodeStubAssembler(state_).CallBuiltin(Builtins::kFastCreateDataProperty, p_context, tmp105, tmp109, p_value);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 144);
    tmp111 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp112 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp113 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp114 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{phi_bb53_7, tmp113});
    tmp115 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp114});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp116 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp14});
    tmp117 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp115});
    tmp118 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp116}, TNode<UintPtrT>{tmp117});
    ca_.Branch(tmp118, &block60, std::vector<Node*>{phi_bb53_7, phi_bb53_7, phi_bb53_7, phi_bb53_7}, &block61, std::vector<Node*>{phi_bb53_7, phi_bb53_7, phi_bb53_7, phi_bb53_7});
  }

  TNode<FixedArray> phi_bb54_7;
  if (block54.is_used()) {
    ca_.Bind(&block54, &phi_bb54_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb60_7;
  TNode<FixedArray> phi_bb60_10;
  TNode<FixedArray> phi_bb60_11;
  TNode<HeapObject> phi_bb60_16;
  TNode<IntPtrT> tmp119;
  TNode<IntPtrT> tmp120;
  TNode<IntPtrT> tmp121;
  TNode<HeapObject> tmp122;
  TNode<IntPtrT> tmp123;
  TNode<Smi> tmp124;
  TNode<Smi> tmp125;
  TNode<IntPtrT> tmp126;
  TNode<IntPtrT> tmp127;
  TNode<IntPtrT> tmp128;
  TNode<Smi> tmp129;
  TNode<IntPtrT> tmp130;
  TNode<IntPtrT> tmp131;
  TNode<UintPtrT> tmp132;
  TNode<UintPtrT> tmp133;
  TNode<BoolT> tmp134;
  if (block60.is_used()) {
    ca_.Bind(&block60, &phi_bb60_7, &phi_bb60_10, &phi_bb60_11, &phi_bb60_16);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp119 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp120 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp14}, TNode<IntPtrT>{tmp119});
    tmp121 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp111}, TNode<IntPtrT>{tmp120});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp122, tmp123) = NewReference_Object_0(state_, TNode<HeapObject>{phi_bb60_16}, TNode<IntPtrT>{tmp121}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 144);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp122, tmp123}, tmp105);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 146);
    tmp124 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp125 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp30}, TNode<Smi>{tmp124});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 147);
    tmp126 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp127 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp128 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp129 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_context, tmp128});
    tmp130 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp129});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp131 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp132 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp131});
    tmp133 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp130});
    tmp134 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp132}, TNode<UintPtrT>{tmp133});
    ca_.Branch(tmp134, &block67, std::vector<Node*>{phi_bb60_7}, &block68, std::vector<Node*>{phi_bb60_7});
  }

  TNode<FixedArray> phi_bb61_7;
  TNode<FixedArray> phi_bb61_10;
  TNode<FixedArray> phi_bb61_11;
  TNode<HeapObject> phi_bb61_16;
  if (block61.is_used()) {
    ca_.Bind(&block61, &phi_bb61_7, &phi_bb61_10, &phi_bb61_11, &phi_bb61_16);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb67_7;
  TNode<IntPtrT> tmp135;
  TNode<IntPtrT> tmp136;
  TNode<IntPtrT> tmp137;
  TNode<HeapObject> tmp138;
  TNode<IntPtrT> tmp139;
  TNode<Smi> tmp140;
  TNode<BoolT> tmp141;
  if (block67.is_used()) {
    ca_.Bind(&block67, &phi_bb67_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp135 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp136 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp131}, TNode<IntPtrT>{tmp135});
    tmp137 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp126}, TNode<IntPtrT>{tmp136});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp138, tmp139) = NewReference_Object_0(state_, TNode<HeapObject>{p_context}, TNode<IntPtrT>{tmp137}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 147);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp138, tmp139}, tmp125);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 150);
    tmp140 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp141 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp125}, TNode<Smi>{tmp140});
    ca_.Branch(tmp141, &block70, std::vector<Node*>{phi_bb67_7}, &block71, std::vector<Node*>{phi_bb67_7});
  }

  TNode<FixedArray> phi_bb68_7;
  if (block68.is_used()) {
    ca_.Bind(&block68, &phi_bb68_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb70_7;
  TNode<IntPtrT> tmp142;
  TNode<IntPtrT> tmp143;
  TNode<IntPtrT> tmp144;
  TNode<Smi> tmp145;
  TNode<IntPtrT> tmp146;
  TNode<IntPtrT> tmp147;
  TNode<UintPtrT> tmp148;
  TNode<UintPtrT> tmp149;
  TNode<BoolT> tmp150;
  if (block70.is_used()) {
    ca_.Bind(&block70, &phi_bb70_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 152);
    tmp142 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp143 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp144 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp145 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_context, tmp144});
    tmp146 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp145});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp147 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementCapabilitySlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp148 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp147});
    tmp149 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp146});
    tmp150 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp148}, TNode<UintPtrT>{tmp149});
    ca_.Branch(tmp150, &block76, std::vector<Node*>{phi_bb70_7}, &block77, std::vector<Node*>{phi_bb70_7});
  }

  TNode<FixedArray> phi_bb76_7;
  TNode<IntPtrT> tmp151;
  TNode<IntPtrT> tmp152;
  TNode<IntPtrT> tmp153;
  TNode<HeapObject> tmp154;
  TNode<IntPtrT> tmp155;
  TNode<Object> tmp156;
  TNode<PromiseCapability> tmp157;
  TNode<IntPtrT> tmp158;
  TNode<Object> tmp159;
  TNode<Object> tmp160;
  TNode<IntPtrT> tmp161;
  TNode<IntPtrT> tmp162;
  TNode<IntPtrT> tmp163;
  TNode<Smi> tmp164;
  TNode<IntPtrT> tmp165;
  TNode<IntPtrT> tmp166;
  TNode<UintPtrT> tmp167;
  TNode<UintPtrT> tmp168;
  TNode<BoolT> tmp169;
  if (block76.is_used()) {
    ca_.Bind(&block76, &phi_bb76_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp151 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp152 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp147}, TNode<IntPtrT>{tmp151});
    tmp153 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp142}, TNode<IntPtrT>{tmp152});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp154, tmp155) = NewReference_Object_0(state_, TNode<HeapObject>{p_context}, TNode<IntPtrT>{tmp153}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 152);
    tmp156 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp154, tmp155});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 151);
    tmp157 = UnsafeCast_PromiseCapability_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp156});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 154);
    tmp158 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp159 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp157, tmp158});
    tmp160 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp159});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 156);
    tmp161 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp162 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp163 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp164 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp6, tmp163});
    tmp165 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp164});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 156);
    tmp166 = FromConstexpr_intptr_constexpr_intptr_0(state_, Context::Field::JS_ARRAY_PACKED_ELEMENTS_MAP_INDEX);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp167 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp166});
    tmp168 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp165});
    tmp169 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp167}, TNode<UintPtrT>{tmp168});
    ca_.Branch(tmp169, &block83, std::vector<Node*>{phi_bb76_7}, &block84, std::vector<Node*>{phi_bb76_7});
  }

  TNode<FixedArray> phi_bb77_7;
  if (block77.is_used()) {
    ca_.Bind(&block77, &phi_bb77_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb83_7;
  TNode<IntPtrT> tmp170;
  TNode<IntPtrT> tmp171;
  TNode<IntPtrT> tmp172;
  TNode<HeapObject> tmp173;
  TNode<IntPtrT> tmp174;
  TNode<Object> tmp175;
  TNode<Map> tmp176;
  TNode<JSArray> tmp177;
  TNode<Oddball> tmp178;
  TNode<Object> tmp179;
  if (block83.is_used()) {
    ca_.Bind(&block83, &phi_bb83_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp170 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp171 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp166}, TNode<IntPtrT>{tmp170});
    tmp172 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp161}, TNode<IntPtrT>{tmp171});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp173, tmp174) = NewReference_Object_0(state_, TNode<HeapObject>{tmp6}, TNode<IntPtrT>{tmp172}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 156);
    tmp175 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp173, tmp174});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 155);
    tmp176 = UnsafeCast_Map_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp175});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 158);
    tmp177 = NewJSArray_0(state_, TNode<Context>{p_context}, TNode<Map>{tmp176}, TNode<FixedArrayBase>{phi_bb83_7});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 159);
    tmp178 = Undefined_0(state_);
    tmp179 = CodeStubAssembler(state_).Call(TNode<Context>{p_context}, TNode<Object>{tmp160}, TNode<Object>{tmp178}, TNode<Object>{tmp177});
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 150);
    ca_.Goto(&block71, phi_bb83_7);
  }

  TNode<FixedArray> phi_bb84_7;
  if (block84.is_used()) {
    ca_.Bind(&block84, &phi_bb84_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb71_7;
  TNode<Oddball> tmp180;
  if (block71.is_used()) {
    ca_.Bind(&block71, &phi_bb71_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 161);
    tmp180 = Undefined_0(state_);
    ca_.Goto(&block1, tmp180);
  }

  TNode<Object> phi_bb1_3;
  if (block1.is_used()) {
    ca_.Bind(&block1, &phi_bb1_3);
    ca_.SetSourcePosition("../../src/builtins/promise-all-element-closure.tq", 184);
    ca_.Goto(&block86, phi_bb1_3);
  }

  TNode<Object> phi_bb86_3;
    ca_.Bind(&block86, &phi_bb86_3);
  return TNode<Object>{phi_bb86_3};
}

}  // namespace internal
}  // namespace v8

