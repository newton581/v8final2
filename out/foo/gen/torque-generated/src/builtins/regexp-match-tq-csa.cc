#include "src/builtins/builtins-array-gen.h"
#include "src/builtins/builtins-bigint-gen.h"
#include "src/builtins/builtins-collections-gen.h"
#include "src/builtins/builtins-constructor-gen.h"
#include "src/builtins/builtins-data-view-gen.h"
#include "src/builtins/builtins-iterator-gen.h"
#include "src/builtins/builtins-promise-gen.h"
#include "src/builtins/builtins-promise.h"
#include "src/builtins/builtins-proxy-gen.h"
#include "src/builtins/builtins-regexp-gen.h"
#include "src/builtins/builtins-string-gen.h"
#include "src/builtins/builtins-typed-array-gen.h"
#include "src/builtins/builtins-utils-gen.h"
#include "src/builtins/builtins-wasm-gen.h"
#include "src/builtins/builtins.h"
#include "src/codegen/code-factory.h"
#include "src/heap/factory-inl.h"
#include "src/ic/binary-op-assembler.h"
#include "src/objects/arguments.h"
#include "src/objects/bigint.h"
#include "src/objects/elements-kind.h"
#include "src/objects/free-space.h"
#include "src/objects/js-break-iterator.h"
#include "src/objects/js-collator.h"
#include "src/objects/js-date-time-format.h"
#include "src/objects/js-display-names.h"
#include "src/objects/js-generator.h"
#include "src/objects/js-list-format.h"
#include "src/objects/js-locale.h"
#include "src/objects/js-number-format.h"
#include "src/objects/js-objects.h"
#include "src/objects/js-plural-rules.h"
#include "src/objects/js-promise.h"
#include "src/objects/js-regexp-string-iterator.h"
#include "src/objects/js-relative-time-format.h"
#include "src/objects/js-segment-iterator.h"
#include "src/objects/js-segmenter.h"
#include "src/objects/js-weak-refs.h"
#include "src/objects/objects.h"
#include "src/objects/ordered-hash-table.h"
#include "src/objects/property-array.h"
#include "src/objects/property-descriptor-object.h"
#include "src/objects/source-text-module.h"
#include "src/objects/stack-frame-info.h"
#include "src/objects/synthetic-module.h"
#include "src/objects/template-objects.h"
#include "src/torque/runtime-support.h"
#include "torque-generated/src/builtins/aggregate-error-tq-csa.h"
#include "torque-generated/src/builtins/array-copywithin-tq-csa.h"
#include "torque-generated/src/builtins/array-every-tq-csa.h"
#include "torque-generated/src/builtins/array-filter-tq-csa.h"
#include "torque-generated/src/builtins/array-find-tq-csa.h"
#include "torque-generated/src/builtins/array-findindex-tq-csa.h"
#include "torque-generated/src/builtins/array-foreach-tq-csa.h"
#include "torque-generated/src/builtins/array-from-tq-csa.h"
#include "torque-generated/src/builtins/array-isarray-tq-csa.h"
#include "torque-generated/src/builtins/array-join-tq-csa.h"
#include "torque-generated/src/builtins/array-lastindexof-tq-csa.h"
#include "torque-generated/src/builtins/array-map-tq-csa.h"
#include "torque-generated/src/builtins/array-of-tq-csa.h"
#include "torque-generated/src/builtins/array-reduce-right-tq-csa.h"
#include "torque-generated/src/builtins/array-reduce-tq-csa.h"
#include "torque-generated/src/builtins/array-reverse-tq-csa.h"
#include "torque-generated/src/builtins/array-shift-tq-csa.h"
#include "torque-generated/src/builtins/array-slice-tq-csa.h"
#include "torque-generated/src/builtins/array-some-tq-csa.h"
#include "torque-generated/src/builtins/array-splice-tq-csa.h"
#include "torque-generated/src/builtins/array-unshift-tq-csa.h"
#include "torque-generated/src/builtins/array-tq-csa.h"
#include "torque-generated/src/builtins/base-tq-csa.h"
#include "torque-generated/src/builtins/bigint-tq-csa.h"
#include "torque-generated/src/builtins/boolean-tq-csa.h"
#include "torque-generated/src/builtins/builtins-string-tq-csa.h"
#include "torque-generated/src/builtins/cast-tq-csa.h"
#include "torque-generated/src/builtins/collections-tq-csa.h"
#include "torque-generated/src/builtins/conversion-tq-csa.h"
#include "torque-generated/src/builtins/convert-tq-csa.h"
#include "torque-generated/src/builtins/console-tq-csa.h"
#include "torque-generated/src/builtins/data-view-tq-csa.h"
#include "torque-generated/src/builtins/finalization-registry-tq-csa.h"
#include "torque-generated/src/builtins/frames-tq-csa.h"
#include "torque-generated/src/builtins/frame-arguments-tq-csa.h"
#include "torque-generated/src/builtins/function-tq-csa.h"
#include "torque-generated/src/builtins/growable-fixed-array-tq-csa.h"
#include "torque-generated/src/builtins/ic-callable-tq-csa.h"
#include "torque-generated/src/builtins/ic-tq-csa.h"
#include "torque-generated/src/builtins/internal-coverage-tq-csa.h"
#include "torque-generated/src/builtins/internal-tq-csa.h"
#include "torque-generated/src/builtins/iterator-tq-csa.h"
#include "torque-generated/src/builtins/math-tq-csa.h"
#include "torque-generated/src/builtins/number-tq-csa.h"
#include "torque-generated/src/builtins/object-fromentries-tq-csa.h"
#include "torque-generated/src/builtins/object-tq-csa.h"
#include "torque-generated/src/builtins/promise-abstract-operations-tq-csa.h"
#include "torque-generated/src/builtins/promise-all-tq-csa.h"
#include "torque-generated/src/builtins/promise-all-element-closure-tq-csa.h"
#include "torque-generated/src/builtins/promise-any-tq-csa.h"
#include "torque-generated/src/builtins/promise-constructor-tq-csa.h"
#include "torque-generated/src/builtins/promise-finally-tq-csa.h"
#include "torque-generated/src/builtins/promise-misc-tq-csa.h"
#include "torque-generated/src/builtins/promise-race-tq-csa.h"
#include "torque-generated/src/builtins/promise-reaction-job-tq-csa.h"
#include "torque-generated/src/builtins/promise-resolve-tq-csa.h"
#include "torque-generated/src/builtins/promise-then-tq-csa.h"
#include "torque-generated/src/builtins/promise-jobs-tq-csa.h"
#include "torque-generated/src/builtins/proxy-constructor-tq-csa.h"
#include "torque-generated/src/builtins/proxy-delete-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-get-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-get-prototype-of-tq-csa.h"
#include "torque-generated/src/builtins/proxy-has-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-is-extensible-tq-csa.h"
#include "torque-generated/src/builtins/proxy-prevent-extensions-tq-csa.h"
#include "torque-generated/src/builtins/proxy-revocable-tq-csa.h"
#include "torque-generated/src/builtins/proxy-revoke-tq-csa.h"
#include "torque-generated/src/builtins/proxy-set-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-set-prototype-of-tq-csa.h"
#include "torque-generated/src/builtins/proxy-tq-csa.h"
#include "torque-generated/src/builtins/reflect-tq-csa.h"
#include "torque-generated/src/builtins/regexp-exec-tq-csa.h"
#include "torque-generated/src/builtins/regexp-match-all-tq-csa.h"
#include "torque-generated/src/builtins/regexp-match-tq-csa.h"
#include "torque-generated/src/builtins/regexp-replace-tq-csa.h"
#include "torque-generated/src/builtins/regexp-search-tq-csa.h"
#include "torque-generated/src/builtins/regexp-source-tq-csa.h"
#include "torque-generated/src/builtins/regexp-split-tq-csa.h"
#include "torque-generated/src/builtins/regexp-test-tq-csa.h"
#include "torque-generated/src/builtins/regexp-tq-csa.h"
#include "torque-generated/src/builtins/string-endswith-tq-csa.h"
#include "torque-generated/src/builtins/string-html-tq-csa.h"
#include "torque-generated/src/builtins/string-iterator-tq-csa.h"
#include "torque-generated/src/builtins/string-pad-tq-csa.h"
#include "torque-generated/src/builtins/string-repeat-tq-csa.h"
#include "torque-generated/src/builtins/string-replaceall-tq-csa.h"
#include "torque-generated/src/builtins/string-slice-tq-csa.h"
#include "torque-generated/src/builtins/string-startswith-tq-csa.h"
#include "torque-generated/src/builtins/string-substring-tq-csa.h"
#include "torque-generated/src/builtins/string-substr-tq-csa.h"
#include "torque-generated/src/builtins/symbol-tq-csa.h"
#include "torque-generated/src/builtins/torque-internal-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-createtypedarray-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-every-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-entries-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-filter-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-find-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-findindex-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-foreach-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-from-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-keys-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-of-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-reduce-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-reduceright-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-set-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-slice-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-some-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-sort-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-subarray-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-values-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-tq-csa.h"
#include "torque-generated/src/builtins/wasm-tq-csa.h"
#include "torque-generated/src/builtins/weak-ref-tq-csa.h"
#include "torque-generated/src/ic/handler-configuration-tq-csa.h"
#include "torque-generated/src/objects/allocation-site-tq-csa.h"
#include "torque-generated/src/objects/api-callbacks-tq-csa.h"
#include "torque-generated/src/objects/arguments-tq-csa.h"
#include "torque-generated/src/objects/cell-tq-csa.h"
#include "torque-generated/src/objects/code-tq-csa.h"
#include "torque-generated/src/objects/contexts-tq-csa.h"
#include "torque-generated/src/objects/data-handler-tq-csa.h"
#include "torque-generated/src/objects/debug-objects-tq-csa.h"
#include "torque-generated/src/objects/descriptor-array-tq-csa.h"
#include "torque-generated/src/objects/embedder-data-array-tq-csa.h"
#include "torque-generated/src/objects/feedback-cell-tq-csa.h"
#include "torque-generated/src/objects/feedback-vector-tq-csa.h"
#include "torque-generated/src/objects/fixed-array-tq-csa.h"
#include "torque-generated/src/objects/foreign-tq-csa.h"
#include "torque-generated/src/objects/free-space-tq-csa.h"
#include "torque-generated/src/objects/heap-number-tq-csa.h"
#include "torque-generated/src/objects/heap-object-tq-csa.h"
#include "torque-generated/src/objects/intl-objects-tq-csa.h"
#include "torque-generated/src/objects/js-array-buffer-tq-csa.h"
#include "torque-generated/src/objects/js-array-tq-csa.h"
#include "torque-generated/src/objects/js-collection-iterator-tq-csa.h"
#include "torque-generated/src/objects/js-collection-tq-csa.h"
#include "torque-generated/src/objects/js-generator-tq-csa.h"
#include "torque-generated/src/objects/js-objects-tq-csa.h"
#include "torque-generated/src/objects/js-promise-tq-csa.h"
#include "torque-generated/src/objects/js-proxy-tq-csa.h"
#include "torque-generated/src/objects/js-regexp-string-iterator-tq-csa.h"
#include "torque-generated/src/objects/js-regexp-tq-csa.h"
#include "torque-generated/src/objects/js-weak-refs-tq-csa.h"
#include "torque-generated/src/objects/literal-objects-tq-csa.h"
#include "torque-generated/src/objects/map-tq-csa.h"
#include "torque-generated/src/objects/microtask-tq-csa.h"
#include "torque-generated/src/objects/module-tq-csa.h"
#include "torque-generated/src/objects/name-tq-csa.h"
#include "torque-generated/src/objects/oddball-tq-csa.h"
#include "torque-generated/src/objects/ordered-hash-table-tq-csa.h"
#include "torque-generated/src/objects/primitive-heap-object-tq-csa.h"
#include "torque-generated/src/objects/promise-tq-csa.h"
#include "torque-generated/src/objects/property-array-tq-csa.h"
#include "torque-generated/src/objects/property-cell-tq-csa.h"
#include "torque-generated/src/objects/property-descriptor-object-tq-csa.h"
#include "torque-generated/src/objects/prototype-info-tq-csa.h"
#include "torque-generated/src/objects/regexp-match-info-tq-csa.h"
#include "torque-generated/src/objects/scope-info-tq-csa.h"
#include "torque-generated/src/objects/script-tq-csa.h"
#include "torque-generated/src/objects/shared-function-info-tq-csa.h"
#include "torque-generated/src/objects/source-text-module-tq-csa.h"
#include "torque-generated/src/objects/stack-frame-info-tq-csa.h"
#include "torque-generated/src/objects/string-tq-csa.h"
#include "torque-generated/src/objects/struct-tq-csa.h"
#include "torque-generated/src/objects/synthetic-module-tq-csa.h"
#include "torque-generated/src/objects/template-objects-tq-csa.h"
#include "torque-generated/src/objects/template-tq-csa.h"
#include "torque-generated/src/wasm/wasm-objects-tq-csa.h"
#include "torque-generated/test/torque/test-torque-tq-csa.h"
#include "torque-generated/third_party/v8/builtins/array-sort-tq-csa.h"

namespace v8 {
namespace internal {

TNode<Object> RegExpPrototypeMatchBody_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<JSReceiver> p_regexp, TNode<String> p_string, bool p_isFastPath) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block14(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block15(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block24(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block25(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block18(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block31(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block32(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, String> block19(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block16(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, String> block17(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String> block36(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String> block34(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String> block41(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String> block45(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String> block44(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String> block46(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String> block47(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String, String> block48(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String> block42(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String> block50(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String> block49(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String> block51(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String> block52(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String, String> block43(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String> block40(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String> block53(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String> block54(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String, IntPtrT, IntPtrT> block60(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String, IntPtrT, IntPtrT> block59(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String, IntPtrT, IntPtrT> block62(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String, IntPtrT, IntPtrT> block61(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String, IntPtrT, IntPtrT> block64(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String, IntPtrT, IntPtrT> block63(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String, HeapObject> block55(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String> block68(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String> block67(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String> block69(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, BoolT, String> block73(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, BoolT, String> block72(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, BoolT, String> block75(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, BoolT, String> block74(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, BoolT, String> block77(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, BoolT, String> block76(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String> block70(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, BoolT, String, FixedArray, FixedArray, IntPtrT, IntPtrT, HeapObject, IntPtrT, IntPtrT> block82(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, BoolT, String, FixedArray, FixedArray, IntPtrT, IntPtrT, HeapObject, IntPtrT, IntPtrT> block83(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, BoolT, String> block85(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, BoolT, String> block86(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, BoolT, String> block87(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, BoolT, String> block91(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, BoolT, String> block90(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, BoolT, String> block88(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, BoolT, String, Object> block89(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, BoolT, String> block92(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, BoolT, String> block96(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, BoolT, String> block95(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, BoolT, String> block93(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, BoolT, String> block94(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, BoolT, String> block35(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block97(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 24);
    if ((p_isFastPath)) {
      ca_.Goto(&block2);
    } else {
      ca_.Goto(&block3);
    }
  }

  TNode<BoolT> tmp0;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 25);
    tmp0 = Is_FastJSRegExp_JSReceiver_0(state_, TNode<Context>{p_context}, TNode<JSReceiver>{p_regexp});
    ca_.Branch(tmp0, &block5, std::vector<Node*>{}, &block6, std::vector<Node*>{});
  }

  if (block6.is_used()) {
    ca_.Bind(&block6);
    CodeStubAssembler(state_).FailAssert("Torque assert 'Is<FastJSRegExp>(regexp)' failed", "src/builtins/regexp-match.tq", 25);
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 24);
    ca_.Goto(&block4);
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.Goto(&block4);
  }

  TNode<BoolT> tmp1;
  TNode<BoolT> tmp2;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 28);
    tmp1 = RegExpBuiltinsAssembler(state_).FlagGetter(TNode<Context>{p_context}, TNode<Object>{p_regexp}, JSRegExp::Flag::kGlobal, p_isFastPath);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 30);
    tmp2 = CodeStubAssembler(state_).Word32BinaryNot(TNode<BoolT>{tmp1});
    ca_.Branch(tmp2, &block7, std::vector<Node*>{}, &block8, std::vector<Node*>{});
  }

  TNode<BoolT> tmp3;
  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 31);
    tmp3 = FromConstexpr_bool_constexpr_bool_0(state_, p_isFastPath);
    ca_.Branch(tmp3, &block9, std::vector<Node*>{}, &block10, std::vector<Node*>{});
  }

  TNode<Object> tmp4;
  if (block9.is_used()) {
    ca_.Bind(&block9);
    tmp4 = RegExpPrototypeExecBodyFast_0(state_, TNode<Context>{p_context}, TNode<JSReceiver>{p_regexp}, TNode<String>{p_string});
    ca_.Goto(&block11, tmp4);
  }

  TNode<Object> tmp5;
  if (block10.is_used()) {
    ca_.Bind(&block10);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 32);
    tmp5 = RegExpExec_0(state_, TNode<Context>{p_context}, TNode<JSReceiver>{p_regexp}, TNode<String>{p_string});
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 31);
    ca_.Goto(&block11, tmp5);
  }

  TNode<Object> phi_bb11_4;
  if (block11.is_used()) {
    ca_.Bind(&block11, &phi_bb11_4);
    ca_.Goto(&block1, phi_bb11_4);
  }

  if (block8.is_used()) {
    ca_.Bind(&block8);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 35);
    ca_.Branch(tmp1, &block13, std::vector<Node*>{}, &block14, std::vector<Node*>{});
  }

  if (block14.is_used()) {
    ca_.Bind(&block14);
    CodeStubAssembler(state_).FailAssert("Torque assert 'isGlobal' failed", "src/builtins/regexp-match.tq", 35);
  }

  TNode<BoolT> tmp6;
  TNode<Number> tmp7;
  TNode<FixedArray> tmp8;
  TNode<IntPtrT> tmp9;
  TNode<IntPtrT> tmp10;
  TNode<BoolT> tmp11;
  TNode<String> tmp12;
  if (block13.is_used()) {
    ca_.Bind(&block13);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 36);
    tmp6 = RegExpBuiltinsAssembler(state_).FlagGetter(TNode<Context>{p_context}, TNode<Object>{p_regexp}, JSRegExp::Flag::kUnicode, p_isFastPath);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 38);
    tmp7 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    StoreLastIndex_0(state_, TNode<Context>{p_context}, TNode<Object>{p_regexp}, TNode<Number>{tmp7}, p_isFastPath);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 42);
    std::tie(tmp8, tmp9, tmp10) = NewGrowableFixedArray_0(state_).Flatten();
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 46);
    tmp11 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 47);
    tmp12 = CodeStubAssembler(state_).EmptyStringConstant();
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 48);
    if ((p_isFastPath)) {
      ca_.Goto(&block15);
    } else {
      ca_.Goto(&block16);
    }
  }

  TNode<JSRegExp> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<HeapObject> tmp15;
  TNode<FixedArray> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<IntPtrT> tmp18;
  TNode<IntPtrT> tmp19;
  TNode<Smi> tmp20;
  TNode<IntPtrT> tmp21;
  TNode<IntPtrT> tmp22;
  TNode<UintPtrT> tmp23;
  TNode<UintPtrT> tmp24;
  TNode<BoolT> tmp25;
  if (block15.is_used()) {
    ca_.Bind(&block15);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 49);
    tmp13 = UnsafeCast_JSRegExp_0(state_, TNode<Context>{p_context}, TNode<Object>{p_regexp});
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 50);
    tmp14 = FromConstexpr_intptr_constexpr_int31_0(state_, 12);
    tmp15 = CodeStubAssembler(state_).LoadReference<HeapObject>(CodeStubAssembler::Reference{tmp13, tmp14});
    tmp16 = UnsafeCast_FixedArray_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp15});
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 51);
    tmp17 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp18 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp19 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp20 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp16, tmp19});
    tmp21 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp20});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp22 = Convert_intptr_constexpr_int31_0(state_, JSRegExp::kTagIndex);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp23 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp22});
    tmp24 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp21});
    tmp25 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp23}, TNode<UintPtrT>{tmp24});
    ca_.Branch(tmp25, &block24, std::vector<Node*>{}, &block25, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp26;
  TNode<IntPtrT> tmp27;
  TNode<IntPtrT> tmp28;
  TNode<HeapObject> tmp29;
  TNode<IntPtrT> tmp30;
  TNode<Object> tmp31;
  TNode<Smi> tmp32;
  TNode<Smi> tmp33;
  TNode<BoolT> tmp34;
  if (block24.is_used()) {
    ca_.Bind(&block24);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp26 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp27 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp22}, TNode<IntPtrT>{tmp26});
    tmp28 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp17}, TNode<IntPtrT>{tmp27});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp29, tmp30) = NewReference_Object_0(state_, TNode<HeapObject>{tmp16}, TNode<IntPtrT>{tmp28}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 51);
    tmp31 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp29, tmp30});
    tmp32 = UnsafeCast_Smi_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp31});
    tmp33 = FromConstexpr_Smi_constexpr_int31_0(state_, JSRegExp::ATOM);
    tmp34 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp32}, TNode<Smi>{tmp33});
    ca_.Branch(tmp34, &block18, std::vector<Node*>{}, &block19, std::vector<Node*>{tmp11, tmp12});
  }

  if (block25.is_used()) {
    ca_.Bind(&block25);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp35;
  TNode<IntPtrT> tmp36;
  TNode<IntPtrT> tmp37;
  TNode<Smi> tmp38;
  TNode<IntPtrT> tmp39;
  TNode<IntPtrT> tmp40;
  TNode<UintPtrT> tmp41;
  TNode<UintPtrT> tmp42;
  TNode<BoolT> tmp43;
  if (block18.is_used()) {
    ca_.Bind(&block18);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 52);
    tmp35 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp36 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp37 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp38 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp16, tmp37});
    tmp39 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp38});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp40 = Convert_intptr_constexpr_int31_0(state_, JSRegExp::kAtomPatternIndex);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp41 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp40});
    tmp42 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp39});
    tmp43 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp41}, TNode<UintPtrT>{tmp42});
    ca_.Branch(tmp43, &block31, std::vector<Node*>{}, &block32, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp44;
  TNode<IntPtrT> tmp45;
  TNode<IntPtrT> tmp46;
  TNode<HeapObject> tmp47;
  TNode<IntPtrT> tmp48;
  TNode<Object> tmp49;
  TNode<String> tmp50;
  TNode<BoolT> tmp51;
  if (block31.is_used()) {
    ca_.Bind(&block31);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp44 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp45 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp40}, TNode<IntPtrT>{tmp44});
    tmp46 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp35}, TNode<IntPtrT>{tmp45});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp47, tmp48) = NewReference_Object_0(state_, TNode<HeapObject>{tmp16}, TNode<IntPtrT>{tmp46}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 52);
    tmp49 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp47, tmp48});
    tmp50 = UnsafeCast_String_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp49});
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 53);
    tmp51 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 51);
    ca_.Goto(&block19, tmp51, tmp50);
  }

  if (block32.is_used()) {
    ca_.Bind(&block32);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<BoolT> phi_bb19_8;
  TNode<String> phi_bb19_9;
  if (block19.is_used()) {
    ca_.Bind(&block19, &phi_bb19_8, &phi_bb19_9);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 48);
    ca_.Goto(&block17, phi_bb19_8, phi_bb19_9);
  }

  if (block16.is_used()) {
    ca_.Bind(&block16);
    ca_.Goto(&block17, tmp11, tmp12);
  }

  TNode<BoolT> phi_bb17_8;
  TNode<String> phi_bb17_9;
  if (block17.is_used()) {
    ca_.Bind(&block17, &phi_bb17_8, &phi_bb17_9);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 57);
    ca_.Goto(&block36, tmp8, tmp9, tmp10, phi_bb17_8, phi_bb17_9);
  }

  TNode<FixedArray> phi_bb36_5;
  TNode<IntPtrT> phi_bb36_6;
  TNode<IntPtrT> phi_bb36_7;
  TNode<BoolT> phi_bb36_8;
  TNode<String> phi_bb36_9;
  TNode<BoolT> tmp52;
  if (block36.is_used()) {
    ca_.Bind(&block36, &phi_bb36_5, &phi_bb36_6, &phi_bb36_7, &phi_bb36_8, &phi_bb36_9);
    tmp52 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    ca_.Branch(tmp52, &block34, std::vector<Node*>{phi_bb36_5, phi_bb36_6, phi_bb36_7, phi_bb36_8, phi_bb36_9}, &block35, std::vector<Node*>{phi_bb36_5, phi_bb36_6, phi_bb36_7, phi_bb36_8, phi_bb36_9});
  }

  TNode<FixedArray> phi_bb34_5;
  TNode<IntPtrT> phi_bb34_6;
  TNode<IntPtrT> phi_bb34_7;
  TNode<BoolT> phi_bb34_8;
  TNode<String> phi_bb34_9;
  TNode<String> tmp53;
  if (block34.is_used()) {
    ca_.Bind(&block34, &phi_bb34_5, &phi_bb34_6, &phi_bb34_7, &phi_bb34_8, &phi_bb34_9);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 58);
    tmp53 = CodeStubAssembler(state_).EmptyStringConstant();
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 60);
    if ((p_isFastPath)) {
      ca_.Goto(&block41, phi_bb34_5, phi_bb34_6, phi_bb34_7, phi_bb34_8, phi_bb34_9);
    } else {
      ca_.Goto(&block42, phi_bb34_5, phi_bb34_6, phi_bb34_7, phi_bb34_8, phi_bb34_9);
    }
  }

  TNode<FixedArray> phi_bb41_5;
  TNode<IntPtrT> phi_bb41_6;
  TNode<IntPtrT> phi_bb41_7;
  TNode<BoolT> phi_bb41_8;
  TNode<String> phi_bb41_9;
  TNode<JSRegExp> tmp54;
  TNode<RegExpMatchInfo> tmp55;
  if (block41.is_used()) {
    ca_.Bind(&block41, &phi_bb41_5, &phi_bb41_6, &phi_bb41_7, &phi_bb41_8, &phi_bb41_9);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 65);
    tmp54 = UnsafeCast_JSRegExp_0(state_, TNode<Context>{p_context}, TNode<Object>{p_regexp});
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 64);
    compiler::CodeAssemblerLabel label56(&ca_);
    tmp55 = RegExpPrototypeExecBodyWithoutResultFast_0(state_, TNode<Context>{p_context}, TNode<JSRegExp>{tmp54}, TNode<String>{p_string}, &label56);
    ca_.Goto(&block44, phi_bb41_5, phi_bb41_6, phi_bb41_7, phi_bb41_8, phi_bb41_9);
    if (label56.is_used()) {
      ca_.Bind(&label56);
      ca_.Goto(&block45, phi_bb41_5, phi_bb41_6, phi_bb41_7, phi_bb41_8, phi_bb41_9);
    }
  }

  TNode<FixedArray> phi_bb45_5;
  TNode<IntPtrT> phi_bb45_6;
  TNode<IntPtrT> phi_bb45_7;
  TNode<BoolT> phi_bb45_8;
  TNode<String> phi_bb45_9;
  if (block45.is_used()) {
    ca_.Bind(&block45, &phi_bb45_5, &phi_bb45_6, &phi_bb45_7, &phi_bb45_8, &phi_bb45_9);
    ca_.Goto(&block40, phi_bb45_5, phi_bb45_6, phi_bb45_7, phi_bb45_8, phi_bb45_9);
  }

  TNode<FixedArray> phi_bb44_5;
  TNode<IntPtrT> phi_bb44_6;
  TNode<IntPtrT> phi_bb44_7;
  TNode<BoolT> phi_bb44_8;
  TNode<String> phi_bb44_9;
  if (block44.is_used()) {
    ca_.Bind(&block44, &phi_bb44_5, &phi_bb44_6, &phi_bb44_7, &phi_bb44_8, &phi_bb44_9);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 66);
    ca_.Branch(phi_bb44_8, &block46, std::vector<Node*>{phi_bb44_5, phi_bb44_6, phi_bb44_7, phi_bb44_8, phi_bb44_9}, &block47, std::vector<Node*>{phi_bb44_5, phi_bb44_6, phi_bb44_7, phi_bb44_8, phi_bb44_9});
  }

  TNode<FixedArray> phi_bb46_5;
  TNode<IntPtrT> phi_bb46_6;
  TNode<IntPtrT> phi_bb46_7;
  TNode<BoolT> phi_bb46_8;
  TNode<String> phi_bb46_9;
  if (block46.is_used()) {
    ca_.Bind(&block46, &phi_bb46_5, &phi_bb46_6, &phi_bb46_7, &phi_bb46_8, &phi_bb46_9);
    ca_.Goto(&block48, phi_bb46_5, phi_bb46_6, phi_bb46_7, phi_bb46_8, phi_bb46_9, phi_bb46_9);
  }

  TNode<FixedArray> phi_bb47_5;
  TNode<IntPtrT> phi_bb47_6;
  TNode<IntPtrT> phi_bb47_7;
  TNode<BoolT> phi_bb47_8;
  TNode<String> phi_bb47_9;
  TNode<Object> tmp57;
  TNode<Object> tmp58;
  TNode<Smi> tmp59;
  TNode<Smi> tmp60;
  TNode<String> tmp61;
  if (block47.is_used()) {
    ca_.Bind(&block47, &phi_bb47_5, &phi_bb47_6, &phi_bb47_7, &phi_bb47_8, &phi_bb47_9);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 69);
    tmp57 = CodeStubAssembler(state_).UnsafeLoadFixedArrayElement(TNode<RegExpMatchInfo>{tmp55}, RegExpMatchInfo::kFirstCaptureIndex);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 71);
    tmp58 = CodeStubAssembler(state_).UnsafeLoadFixedArrayElement(TNode<RegExpMatchInfo>{tmp55}, (CodeStubAssembler(state_).ConstexprInt31Add(RegExpMatchInfo::kFirstCaptureIndex, 1)));
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 74);
    tmp59 = UnsafeCast_Smi_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp57});
    tmp60 = UnsafeCast_Smi_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp58});
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 73);
    tmp61 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kSubString, p_context, p_string, tmp59, tmp60));
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 66);
    ca_.Goto(&block48, phi_bb47_5, phi_bb47_6, phi_bb47_7, phi_bb47_8, phi_bb47_9, tmp61);
  }

  TNode<FixedArray> phi_bb48_5;
  TNode<IntPtrT> phi_bb48_6;
  TNode<IntPtrT> phi_bb48_7;
  TNode<BoolT> phi_bb48_8;
  TNode<String> phi_bb48_9;
  TNode<String> phi_bb48_10;
  if (block48.is_used()) {
    ca_.Bind(&block48, &phi_bb48_5, &phi_bb48_6, &phi_bb48_7, &phi_bb48_8, &phi_bb48_9, &phi_bb48_10);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 60);
    ca_.Goto(&block43, phi_bb48_5, phi_bb48_6, phi_bb48_7, phi_bb48_8, phi_bb48_9, phi_bb48_10);
  }

  TNode<FixedArray> phi_bb42_5;
  TNode<IntPtrT> phi_bb42_6;
  TNode<IntPtrT> phi_bb42_7;
  TNode<BoolT> phi_bb42_8;
  TNode<String> phi_bb42_9;
  TNode<BoolT> tmp62;
  if (block42.is_used()) {
    ca_.Bind(&block42, &phi_bb42_5, &phi_bb42_6, &phi_bb42_7, &phi_bb42_8, &phi_bb42_9);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 77);
    tmp62 = FromConstexpr_bool_constexpr_bool_0(state_, (CodeStubAssembler(state_).ConstexprBoolNot(p_isFastPath)));
    ca_.Branch(tmp62, &block49, std::vector<Node*>{phi_bb42_5, phi_bb42_6, phi_bb42_7, phi_bb42_8, phi_bb42_9}, &block50, std::vector<Node*>{phi_bb42_5, phi_bb42_6, phi_bb42_7, phi_bb42_8, phi_bb42_9});
  }

  TNode<FixedArray> phi_bb50_5;
  TNode<IntPtrT> phi_bb50_6;
  TNode<IntPtrT> phi_bb50_7;
  TNode<BoolT> phi_bb50_8;
  TNode<String> phi_bb50_9;
  if (block50.is_used()) {
    ca_.Bind(&block50, &phi_bb50_5, &phi_bb50_6, &phi_bb50_7, &phi_bb50_8, &phi_bb50_9);
    CodeStubAssembler(state_).FailAssert("Torque assert '!isFastPath' failed", "src/builtins/regexp-match.tq", 77);
  }

  TNode<FixedArray> phi_bb49_5;
  TNode<IntPtrT> phi_bb49_6;
  TNode<IntPtrT> phi_bb49_7;
  TNode<BoolT> phi_bb49_8;
  TNode<String> phi_bb49_9;
  TNode<Object> tmp63;
  TNode<Oddball> tmp64;
  TNode<BoolT> tmp65;
  if (block49.is_used()) {
    ca_.Bind(&block49, &phi_bb49_5, &phi_bb49_6, &phi_bb49_7, &phi_bb49_8, &phi_bb49_9);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 78);
    tmp63 = RegExpExec_0(state_, TNode<Context>{p_context}, TNode<JSReceiver>{p_regexp}, TNode<String>{p_string});
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 79);
    tmp64 = Null_0(state_);
    tmp65 = CodeStubAssembler(state_).TaggedEqual(TNode<Object>{tmp63}, TNode<HeapObject>{tmp64});
    ca_.Branch(tmp65, &block51, std::vector<Node*>{phi_bb49_5, phi_bb49_6, phi_bb49_7, phi_bb49_8, phi_bb49_9}, &block52, std::vector<Node*>{phi_bb49_5, phi_bb49_6, phi_bb49_7, phi_bb49_8, phi_bb49_9});
  }

  TNode<FixedArray> phi_bb51_5;
  TNode<IntPtrT> phi_bb51_6;
  TNode<IntPtrT> phi_bb51_7;
  TNode<BoolT> phi_bb51_8;
  TNode<String> phi_bb51_9;
  if (block51.is_used()) {
    ca_.Bind(&block51, &phi_bb51_5, &phi_bb51_6, &phi_bb51_7, &phi_bb51_8, &phi_bb51_9);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 80);
    ca_.Goto(&block40, phi_bb51_5, phi_bb51_6, phi_bb51_7, phi_bb51_8, phi_bb51_9);
  }

  TNode<FixedArray> phi_bb52_5;
  TNode<IntPtrT> phi_bb52_6;
  TNode<IntPtrT> phi_bb52_7;
  TNode<BoolT> phi_bb52_8;
  TNode<String> phi_bb52_9;
  TNode<Smi> tmp66;
  TNode<Object> tmp67;
  TNode<String> tmp68;
  if (block52.is_used()) {
    ca_.Bind(&block52, &phi_bb52_5, &phi_bb52_6, &phi_bb52_7, &phi_bb52_8, &phi_bb52_9);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 82);
    tmp66 = CodeStubAssembler(state_).SmiConstant(0);
    tmp67 = CodeStubAssembler(state_).GetProperty(TNode<Context>{p_context}, TNode<Object>{tmp63}, TNode<Object>{tmp66});
    tmp68 = CodeStubAssembler(state_).ToString_Inline(TNode<Context>{p_context}, TNode<Object>{tmp67});
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 60);
    ca_.Goto(&block43, phi_bb52_5, phi_bb52_6, phi_bb52_7, phi_bb52_8, phi_bb52_9, tmp68);
  }

  TNode<FixedArray> phi_bb43_5;
  TNode<IntPtrT> phi_bb43_6;
  TNode<IntPtrT> phi_bb43_7;
  TNode<BoolT> phi_bb43_8;
  TNode<String> phi_bb43_9;
  TNode<String> phi_bb43_10;
  TNode<BoolT> tmp69;
  if (block43.is_used()) {
    ca_.Bind(&block43, &phi_bb43_5, &phi_bb43_6, &phi_bb43_7, &phi_bb43_8, &phi_bb43_9, &phi_bb43_10);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 20);
    tmp69 = CodeStubAssembler(state_).IntPtrLessThanOrEqual(TNode<IntPtrT>{phi_bb43_7}, TNode<IntPtrT>{phi_bb43_6});
    ca_.Branch(tmp69, &block67, std::vector<Node*>{phi_bb43_5, phi_bb43_6, phi_bb43_7, phi_bb43_8, phi_bb43_9}, &block68, std::vector<Node*>{phi_bb43_5, phi_bb43_6, phi_bb43_7, phi_bb43_8, phi_bb43_9});
  }

  TNode<FixedArray> phi_bb40_5;
  TNode<IntPtrT> phi_bb40_6;
  TNode<IntPtrT> phi_bb40_7;
  TNode<BoolT> phi_bb40_8;
  TNode<String> phi_bb40_9;
  TNode<IntPtrT> tmp70;
  TNode<BoolT> tmp71;
  if (block40.is_used()) {
    ca_.Bind(&block40, &phi_bb40_5, &phi_bb40_6, &phi_bb40_7, &phi_bb40_8, &phi_bb40_9);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 86);
    tmp70 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp71 = CodeStubAssembler(state_).WordEqual(TNode<IntPtrT>{phi_bb40_7}, TNode<IntPtrT>{tmp70});
    ca_.Branch(tmp71, &block53, std::vector<Node*>{phi_bb40_5, phi_bb40_6, phi_bb40_7, phi_bb40_8, phi_bb40_9}, &block54, std::vector<Node*>{phi_bb40_5, phi_bb40_6, phi_bb40_7, phi_bb40_8, phi_bb40_9});
  }

  TNode<FixedArray> phi_bb53_5;
  TNode<IntPtrT> phi_bb53_6;
  TNode<IntPtrT> phi_bb53_7;
  TNode<BoolT> phi_bb53_8;
  TNode<String> phi_bb53_9;
  TNode<Oddball> tmp72;
  if (block53.is_used()) {
    ca_.Bind(&block53, &phi_bb53_5, &phi_bb53_6, &phi_bb53_7, &phi_bb53_8, &phi_bb53_9);
    tmp72 = Null_0(state_);
    ca_.Goto(&block55, phi_bb53_5, phi_bb53_6, phi_bb53_7, phi_bb53_8, phi_bb53_9, tmp72);
  }

  TNode<FixedArray> phi_bb54_5;
  TNode<IntPtrT> phi_bb54_6;
  TNode<IntPtrT> phi_bb54_7;
  TNode<BoolT> phi_bb54_8;
  TNode<String> phi_bb54_9;
  TNode<NativeContext> tmp73;
  TNode<Map> tmp74;
  TNode<IntPtrT> tmp75;
  TNode<BoolT> tmp76;
  if (block54.is_used()) {
    ca_.Bind(&block54, &phi_bb54_5, &phi_bb54_6, &phi_bb54_7, &phi_bb54_8, &phi_bb54_9);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 30);
    tmp73 = CodeStubAssembler(state_).LoadNativeContext(TNode<Context>{p_context});
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 32);
    tmp74 = CodeStubAssembler(state_).LoadJSArrayElementsMap(ElementsKind::PACKED_ELEMENTS, TNode<NativeContext>{tmp73});
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 13);
    tmp75 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp76 = CodeStubAssembler(state_).IntPtrGreaterThanOrEqual(TNode<IntPtrT>{phi_bb54_7}, TNode<IntPtrT>{tmp75});
    ca_.Branch(tmp76, &block59, std::vector<Node*>{phi_bb54_5, phi_bb54_6, phi_bb54_7, phi_bb54_8, phi_bb54_9, phi_bb54_7, phi_bb54_7}, &block60, std::vector<Node*>{phi_bb54_5, phi_bb54_6, phi_bb54_7, phi_bb54_8, phi_bb54_9, phi_bb54_7, phi_bb54_7});
  }

  TNode<FixedArray> phi_bb60_5;
  TNode<IntPtrT> phi_bb60_6;
  TNode<IntPtrT> phi_bb60_7;
  TNode<BoolT> phi_bb60_8;
  TNode<String> phi_bb60_9;
  TNode<IntPtrT> phi_bb60_15;
  TNode<IntPtrT> phi_bb60_16;
  if (block60.is_used()) {
    ca_.Bind(&block60, &phi_bb60_5, &phi_bb60_6, &phi_bb60_7, &phi_bb60_8, &phi_bb60_9, &phi_bb60_15, &phi_bb60_16);
    CodeStubAssembler(state_).FailAssert("Torque assert 'this.length >= 0' failed", "src/builtins/growable-fixed-array.tq", 13);
  }

  TNode<FixedArray> phi_bb59_5;
  TNode<IntPtrT> phi_bb59_6;
  TNode<IntPtrT> phi_bb59_7;
  TNode<BoolT> phi_bb59_8;
  TNode<String> phi_bb59_9;
  TNode<IntPtrT> phi_bb59_15;
  TNode<IntPtrT> phi_bb59_16;
  TNode<IntPtrT> tmp77;
  TNode<BoolT> tmp78;
  if (block59.is_used()) {
    ca_.Bind(&block59, &phi_bb59_5, &phi_bb59_6, &phi_bb59_7, &phi_bb59_8, &phi_bb59_9, &phi_bb59_15, &phi_bb59_16);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 14);
    tmp77 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp78 = CodeStubAssembler(state_).IntPtrGreaterThanOrEqual(TNode<IntPtrT>{phi_bb59_16}, TNode<IntPtrT>{tmp77});
    ca_.Branch(tmp78, &block61, std::vector<Node*>{phi_bb59_5, phi_bb59_6, phi_bb59_7, phi_bb59_8, phi_bb59_9, phi_bb59_15, phi_bb59_16}, &block62, std::vector<Node*>{phi_bb59_5, phi_bb59_6, phi_bb59_7, phi_bb59_8, phi_bb59_9, phi_bb59_15, phi_bb59_16});
  }

  TNode<FixedArray> phi_bb62_5;
  TNode<IntPtrT> phi_bb62_6;
  TNode<IntPtrT> phi_bb62_7;
  TNode<BoolT> phi_bb62_8;
  TNode<String> phi_bb62_9;
  TNode<IntPtrT> phi_bb62_15;
  TNode<IntPtrT> phi_bb62_16;
  if (block62.is_used()) {
    ca_.Bind(&block62, &phi_bb62_5, &phi_bb62_6, &phi_bb62_7, &phi_bb62_8, &phi_bb62_9, &phi_bb62_15, &phi_bb62_16);
    CodeStubAssembler(state_).FailAssert("Torque assert 'newCapacity >= 0' failed", "src/builtins/growable-fixed-array.tq", 14);
  }

  TNode<FixedArray> phi_bb61_5;
  TNode<IntPtrT> phi_bb61_6;
  TNode<IntPtrT> phi_bb61_7;
  TNode<BoolT> phi_bb61_8;
  TNode<String> phi_bb61_9;
  TNode<IntPtrT> phi_bb61_15;
  TNode<IntPtrT> phi_bb61_16;
  TNode<BoolT> tmp79;
  if (block61.is_used()) {
    ca_.Bind(&block61, &phi_bb61_5, &phi_bb61_6, &phi_bb61_7, &phi_bb61_8, &phi_bb61_9, &phi_bb61_15, &phi_bb61_16);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 15);
    tmp79 = CodeStubAssembler(state_).IntPtrGreaterThanOrEqual(TNode<IntPtrT>{phi_bb61_16}, TNode<IntPtrT>{phi_bb61_7});
    ca_.Branch(tmp79, &block63, std::vector<Node*>{phi_bb61_5, phi_bb61_6, phi_bb61_7, phi_bb61_8, phi_bb61_9, phi_bb61_15, phi_bb61_16}, &block64, std::vector<Node*>{phi_bb61_5, phi_bb61_6, phi_bb61_7, phi_bb61_8, phi_bb61_9, phi_bb61_15, phi_bb61_16});
  }

  TNode<FixedArray> phi_bb64_5;
  TNode<IntPtrT> phi_bb64_6;
  TNode<IntPtrT> phi_bb64_7;
  TNode<BoolT> phi_bb64_8;
  TNode<String> phi_bb64_9;
  TNode<IntPtrT> phi_bb64_15;
  TNode<IntPtrT> phi_bb64_16;
  if (block64.is_used()) {
    ca_.Bind(&block64, &phi_bb64_5, &phi_bb64_6, &phi_bb64_7, &phi_bb64_8, &phi_bb64_9, &phi_bb64_15, &phi_bb64_16);
    CodeStubAssembler(state_).FailAssert("Torque assert 'newCapacity >= this.length' failed", "src/builtins/growable-fixed-array.tq", 15);
  }

  TNode<FixedArray> phi_bb63_5;
  TNode<IntPtrT> phi_bb63_6;
  TNode<IntPtrT> phi_bb63_7;
  TNode<BoolT> phi_bb63_8;
  TNode<String> phi_bb63_9;
  TNode<IntPtrT> phi_bb63_15;
  TNode<IntPtrT> phi_bb63_16;
  TNode<IntPtrT> tmp80;
  TNode<FixedArray> tmp81;
  TNode<Smi> tmp82;
  TNode<JSArray> tmp83;
  if (block63.is_used()) {
    ca_.Bind(&block63, &phi_bb63_5, &phi_bb63_6, &phi_bb63_7, &phi_bb63_8, &phi_bb63_9, &phi_bb63_15, &phi_bb63_16);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 16);
    tmp80 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 17);
    tmp81 = ExtractFixedArray_0(state_, TNode<FixedArray>{phi_bb63_5}, TNode<IntPtrT>{tmp80}, TNode<IntPtrT>{phi_bb63_7}, TNode<IntPtrT>{phi_bb63_16});
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 34);
    tmp82 = Convert_Smi_intptr_0(state_, TNode<IntPtrT>{phi_bb63_7});
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 35);
    tmp83 = CodeStubAssembler(state_).AllocateJSArray(TNode<Map>{tmp74}, TNode<FixedArrayBase>{tmp81}, TNode<Smi>{tmp82});
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 86);
    ca_.Goto(&block55, phi_bb63_5, phi_bb63_6, phi_bb63_7, phi_bb63_8, phi_bb63_9, tmp83);
  }

  TNode<FixedArray> phi_bb55_5;
  TNode<IntPtrT> phi_bb55_6;
  TNode<IntPtrT> phi_bb55_7;
  TNode<BoolT> phi_bb55_8;
  TNode<String> phi_bb55_9;
  TNode<HeapObject> phi_bb55_11;
  if (block55.is_used()) {
    ca_.Bind(&block55, &phi_bb55_5, &phi_bb55_6, &phi_bb55_7, &phi_bb55_8, &phi_bb55_9, &phi_bb55_11);
    ca_.Goto(&block1, phi_bb55_11);
  }

  TNode<FixedArray> phi_bb68_5;
  TNode<IntPtrT> phi_bb68_6;
  TNode<IntPtrT> phi_bb68_7;
  TNode<BoolT> phi_bb68_8;
  TNode<String> phi_bb68_9;
  if (block68.is_used()) {
    ca_.Bind(&block68, &phi_bb68_5, &phi_bb68_6, &phi_bb68_7, &phi_bb68_8, &phi_bb68_9);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 20);
    CodeStubAssembler(state_).FailAssert("Torque assert 'this.length <= this.capacity' failed", "src/builtins/growable-fixed-array.tq", 20);
  }

  TNode<FixedArray> phi_bb67_5;
  TNode<IntPtrT> phi_bb67_6;
  TNode<IntPtrT> phi_bb67_7;
  TNode<BoolT> phi_bb67_8;
  TNode<String> phi_bb67_9;
  TNode<BoolT> tmp84;
  if (block67.is_used()) {
    ca_.Bind(&block67, &phi_bb67_5, &phi_bb67_6, &phi_bb67_7, &phi_bb67_8, &phi_bb67_9);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 21);
    tmp84 = CodeStubAssembler(state_).WordEqual(TNode<IntPtrT>{phi_bb67_6}, TNode<IntPtrT>{phi_bb67_7});
    ca_.Branch(tmp84, &block69, std::vector<Node*>{phi_bb67_5, phi_bb67_6, phi_bb67_7, phi_bb67_8, phi_bb67_9}, &block70, std::vector<Node*>{phi_bb67_5, phi_bb67_6, phi_bb67_7, phi_bb67_8, phi_bb67_9});
  }

  TNode<FixedArray> phi_bb69_5;
  TNode<IntPtrT> phi_bb69_6;
  TNode<IntPtrT> phi_bb69_7;
  TNode<BoolT> phi_bb69_8;
  TNode<String> phi_bb69_9;
  TNode<IntPtrT> tmp85;
  TNode<IntPtrT> tmp86;
  TNode<IntPtrT> tmp87;
  TNode<IntPtrT> tmp88;
  TNode<IntPtrT> tmp89;
  TNode<IntPtrT> tmp90;
  TNode<BoolT> tmp91;
  if (block69.is_used()) {
    ca_.Bind(&block69, &phi_bb69_5, &phi_bb69_6, &phi_bb69_7, &phi_bb69_8, &phi_bb69_9);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 24);
    tmp85 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp86 = CodeStubAssembler(state_).WordSar(TNode<IntPtrT>{phi_bb69_6}, TNode<IntPtrT>{tmp85});
    tmp87 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{phi_bb69_6}, TNode<IntPtrT>{tmp86});
    tmp88 = FromConstexpr_intptr_constexpr_int31_0(state_, 16);
    tmp89 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp87}, TNode<IntPtrT>{tmp88});
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 13);
    tmp90 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp91 = CodeStubAssembler(state_).IntPtrGreaterThanOrEqual(TNode<IntPtrT>{phi_bb69_7}, TNode<IntPtrT>{tmp90});
    ca_.Branch(tmp91, &block72, std::vector<Node*>{phi_bb69_5, phi_bb69_7, phi_bb69_8, phi_bb69_9}, &block73, std::vector<Node*>{phi_bb69_5, phi_bb69_7, phi_bb69_8, phi_bb69_9});
  }

  TNode<FixedArray> phi_bb73_5;
  TNode<IntPtrT> phi_bb73_7;
  TNode<BoolT> phi_bb73_8;
  TNode<String> phi_bb73_9;
  if (block73.is_used()) {
    ca_.Bind(&block73, &phi_bb73_5, &phi_bb73_7, &phi_bb73_8, &phi_bb73_9);
    CodeStubAssembler(state_).FailAssert("Torque assert 'this.length >= 0' failed", "src/builtins/growable-fixed-array.tq", 13);
  }

  TNode<FixedArray> phi_bb72_5;
  TNode<IntPtrT> phi_bb72_7;
  TNode<BoolT> phi_bb72_8;
  TNode<String> phi_bb72_9;
  TNode<IntPtrT> tmp92;
  TNode<BoolT> tmp93;
  if (block72.is_used()) {
    ca_.Bind(&block72, &phi_bb72_5, &phi_bb72_7, &phi_bb72_8, &phi_bb72_9);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 14);
    tmp92 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp93 = CodeStubAssembler(state_).IntPtrGreaterThanOrEqual(TNode<IntPtrT>{tmp89}, TNode<IntPtrT>{tmp92});
    ca_.Branch(tmp93, &block74, std::vector<Node*>{phi_bb72_5, phi_bb72_7, phi_bb72_8, phi_bb72_9}, &block75, std::vector<Node*>{phi_bb72_5, phi_bb72_7, phi_bb72_8, phi_bb72_9});
  }

  TNode<FixedArray> phi_bb75_5;
  TNode<IntPtrT> phi_bb75_7;
  TNode<BoolT> phi_bb75_8;
  TNode<String> phi_bb75_9;
  if (block75.is_used()) {
    ca_.Bind(&block75, &phi_bb75_5, &phi_bb75_7, &phi_bb75_8, &phi_bb75_9);
    CodeStubAssembler(state_).FailAssert("Torque assert 'newCapacity >= 0' failed", "src/builtins/growable-fixed-array.tq", 14);
  }

  TNode<FixedArray> phi_bb74_5;
  TNode<IntPtrT> phi_bb74_7;
  TNode<BoolT> phi_bb74_8;
  TNode<String> phi_bb74_9;
  TNode<BoolT> tmp94;
  if (block74.is_used()) {
    ca_.Bind(&block74, &phi_bb74_5, &phi_bb74_7, &phi_bb74_8, &phi_bb74_9);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 15);
    tmp94 = CodeStubAssembler(state_).IntPtrGreaterThanOrEqual(TNode<IntPtrT>{tmp89}, TNode<IntPtrT>{phi_bb74_7});
    ca_.Branch(tmp94, &block76, std::vector<Node*>{phi_bb74_5, phi_bb74_7, phi_bb74_8, phi_bb74_9}, &block77, std::vector<Node*>{phi_bb74_5, phi_bb74_7, phi_bb74_8, phi_bb74_9});
  }

  TNode<FixedArray> phi_bb77_5;
  TNode<IntPtrT> phi_bb77_7;
  TNode<BoolT> phi_bb77_8;
  TNode<String> phi_bb77_9;
  if (block77.is_used()) {
    ca_.Bind(&block77, &phi_bb77_5, &phi_bb77_7, &phi_bb77_8, &phi_bb77_9);
    CodeStubAssembler(state_).FailAssert("Torque assert 'newCapacity >= this.length' failed", "src/builtins/growable-fixed-array.tq", 15);
  }

  TNode<FixedArray> phi_bb76_5;
  TNode<IntPtrT> phi_bb76_7;
  TNode<BoolT> phi_bb76_8;
  TNode<String> phi_bb76_9;
  TNode<IntPtrT> tmp95;
  TNode<FixedArray> tmp96;
  if (block76.is_used()) {
    ca_.Bind(&block76, &phi_bb76_5, &phi_bb76_7, &phi_bb76_8, &phi_bb76_9);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 16);
    tmp95 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 17);
    tmp96 = ExtractFixedArray_0(state_, TNode<FixedArray>{phi_bb76_5}, TNode<IntPtrT>{tmp95}, TNode<IntPtrT>{phi_bb76_7}, TNode<IntPtrT>{tmp89});
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 21);
    ca_.Goto(&block70, tmp96, tmp89, phi_bb76_7, phi_bb76_8, phi_bb76_9);
  }

  TNode<FixedArray> phi_bb70_5;
  TNode<IntPtrT> phi_bb70_6;
  TNode<IntPtrT> phi_bb70_7;
  TNode<BoolT> phi_bb70_8;
  TNode<String> phi_bb70_9;
  TNode<IntPtrT> tmp97;
  TNode<IntPtrT> tmp98;
  TNode<IntPtrT> tmp99;
  TNode<Smi> tmp100;
  TNode<IntPtrT> tmp101;
  TNode<IntPtrT> tmp102;
  TNode<IntPtrT> tmp103;
  TNode<UintPtrT> tmp104;
  TNode<UintPtrT> tmp105;
  TNode<BoolT> tmp106;
  if (block70.is_used()) {
    ca_.Bind(&block70, &phi_bb70_5, &phi_bb70_6, &phi_bb70_7, &phi_bb70_8, &phi_bb70_9);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 10);
    tmp97 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp98 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp99 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp100 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{phi_bb70_5, tmp99});
    tmp101 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp100});
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 10);
    tmp102 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp103 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{phi_bb70_7}, TNode<IntPtrT>{tmp102});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp104 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{phi_bb70_7});
    tmp105 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp101});
    tmp106 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp104}, TNode<UintPtrT>{tmp105});
    ca_.Branch(tmp106, &block82, std::vector<Node*>{phi_bb70_5, phi_bb70_6, phi_bb70_8, phi_bb70_9, phi_bb70_5, phi_bb70_5, phi_bb70_7, phi_bb70_7, phi_bb70_5, phi_bb70_7, phi_bb70_7}, &block83, std::vector<Node*>{phi_bb70_5, phi_bb70_6, phi_bb70_8, phi_bb70_9, phi_bb70_5, phi_bb70_5, phi_bb70_7, phi_bb70_7, phi_bb70_5, phi_bb70_7, phi_bb70_7});
  }

  TNode<FixedArray> phi_bb82_5;
  TNode<IntPtrT> phi_bb82_6;
  TNode<BoolT> phi_bb82_8;
  TNode<String> phi_bb82_9;
  TNode<FixedArray> phi_bb82_13;
  TNode<FixedArray> phi_bb82_14;
  TNode<IntPtrT> phi_bb82_17;
  TNode<IntPtrT> phi_bb82_18;
  TNode<HeapObject> phi_bb82_19;
  TNode<IntPtrT> phi_bb82_22;
  TNode<IntPtrT> phi_bb82_23;
  TNode<IntPtrT> tmp107;
  TNode<IntPtrT> tmp108;
  TNode<IntPtrT> tmp109;
  TNode<HeapObject> tmp110;
  TNode<IntPtrT> tmp111;
  TNode<Smi> tmp112;
  TNode<Smi> tmp113;
  TNode<BoolT> tmp114;
  if (block82.is_used()) {
    ca_.Bind(&block82, &phi_bb82_5, &phi_bb82_6, &phi_bb82_8, &phi_bb82_9, &phi_bb82_13, &phi_bb82_14, &phi_bb82_17, &phi_bb82_18, &phi_bb82_19, &phi_bb82_22, &phi_bb82_23);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp107 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp108 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{phi_bb82_23}, TNode<IntPtrT>{tmp107});
    tmp109 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp97}, TNode<IntPtrT>{tmp108});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp110, tmp111) = NewReference_Object_0(state_, TNode<HeapObject>{phi_bb82_19}, TNode<IntPtrT>{tmp109}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 10);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp110, tmp111}, phi_bb43_10);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 93);
    tmp112 = CodeStubAssembler(state_).LoadStringLengthAsSmi(TNode<String>{phi_bb43_10});
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 94);
    tmp113 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp114 = CodeStubAssembler(state_).SmiNotEqual(TNode<Smi>{tmp112}, TNode<Smi>{tmp113});
    ca_.Branch(tmp114, &block85, std::vector<Node*>{phi_bb82_5, phi_bb82_6, phi_bb82_8, phi_bb82_9}, &block86, std::vector<Node*>{phi_bb82_5, phi_bb82_6, phi_bb82_8, phi_bb82_9});
  }

  TNode<FixedArray> phi_bb83_5;
  TNode<IntPtrT> phi_bb83_6;
  TNode<BoolT> phi_bb83_8;
  TNode<String> phi_bb83_9;
  TNode<FixedArray> phi_bb83_13;
  TNode<FixedArray> phi_bb83_14;
  TNode<IntPtrT> phi_bb83_17;
  TNode<IntPtrT> phi_bb83_18;
  TNode<HeapObject> phi_bb83_19;
  TNode<IntPtrT> phi_bb83_22;
  TNode<IntPtrT> phi_bb83_23;
  if (block83.is_used()) {
    ca_.Bind(&block83, &phi_bb83_5, &phi_bb83_6, &phi_bb83_8, &phi_bb83_9, &phi_bb83_13, &phi_bb83_14, &phi_bb83_17, &phi_bb83_18, &phi_bb83_19, &phi_bb83_22, &phi_bb83_23);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb85_5;
  TNode<IntPtrT> phi_bb85_6;
  TNode<BoolT> phi_bb85_8;
  TNode<String> phi_bb85_9;
  if (block85.is_used()) {
    ca_.Bind(&block85, &phi_bb85_5, &phi_bb85_6, &phi_bb85_8, &phi_bb85_9);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 95);
    ca_.Goto(&block36, phi_bb85_5, phi_bb85_6, tmp103, phi_bb85_8, phi_bb85_9);
  }

  TNode<FixedArray> phi_bb86_5;
  TNode<IntPtrT> phi_bb86_6;
  TNode<BoolT> phi_bb86_8;
  TNode<String> phi_bb86_9;
  TNode<Object> tmp115;
  if (block86.is_used()) {
    ca_.Bind(&block86, &phi_bb86_5, &phi_bb86_6, &phi_bb86_8, &phi_bb86_9);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 97);
    tmp115 = LoadLastIndex_0(state_, TNode<Context>{p_context}, TNode<Object>{p_regexp}, p_isFastPath);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 98);
    if ((p_isFastPath)) {
      ca_.Goto(&block87, phi_bb86_5, phi_bb86_6, phi_bb86_8, phi_bb86_9);
    } else {
      ca_.Goto(&block88, phi_bb86_5, phi_bb86_6, phi_bb86_8, phi_bb86_9);
    }
  }

  TNode<FixedArray> phi_bb87_5;
  TNode<IntPtrT> phi_bb87_6;
  TNode<BoolT> phi_bb87_8;
  TNode<String> phi_bb87_9;
  TNode<BoolT> tmp116;
  if (block87.is_used()) {
    ca_.Bind(&block87, &phi_bb87_5, &phi_bb87_6, &phi_bb87_8, &phi_bb87_9);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 99);
    tmp116 = CodeStubAssembler(state_).TaggedIsPositiveSmi(TNode<Object>{tmp115});
    ca_.Branch(tmp116, &block90, std::vector<Node*>{phi_bb87_5, phi_bb87_6, phi_bb87_8, phi_bb87_9}, &block91, std::vector<Node*>{phi_bb87_5, phi_bb87_6, phi_bb87_8, phi_bb87_9});
  }

  TNode<FixedArray> phi_bb91_5;
  TNode<IntPtrT> phi_bb91_6;
  TNode<BoolT> phi_bb91_8;
  TNode<String> phi_bb91_9;
  if (block91.is_used()) {
    ca_.Bind(&block91, &phi_bb91_5, &phi_bb91_6, &phi_bb91_8, &phi_bb91_9);
    CodeStubAssembler(state_).FailAssert("Torque assert 'TaggedIsPositiveSmi(lastIndex)' failed", "src/builtins/regexp-match.tq", 99);
  }

  TNode<FixedArray> phi_bb90_5;
  TNode<IntPtrT> phi_bb90_6;
  TNode<BoolT> phi_bb90_8;
  TNode<String> phi_bb90_9;
  if (block90.is_used()) {
    ca_.Bind(&block90, &phi_bb90_5, &phi_bb90_6, &phi_bb90_8, &phi_bb90_9);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 98);
    ca_.Goto(&block89, phi_bb90_5, phi_bb90_6, phi_bb90_8, phi_bb90_9, tmp115);
  }

  TNode<FixedArray> phi_bb88_5;
  TNode<IntPtrT> phi_bb88_6;
  TNode<BoolT> phi_bb88_8;
  TNode<String> phi_bb88_9;
  TNode<Number> tmp117;
  if (block88.is_used()) {
    ca_.Bind(&block88, &phi_bb88_5, &phi_bb88_6, &phi_bb88_8, &phi_bb88_9);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 101);
    tmp117 = CodeStubAssembler(state_).ToLength_Inline(TNode<Context>{p_context}, TNode<Object>{tmp115});
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 98);
    ca_.Goto(&block89, phi_bb88_5, phi_bb88_6, phi_bb88_8, phi_bb88_9, tmp117);
  }

  TNode<FixedArray> phi_bb89_5;
  TNode<IntPtrT> phi_bb89_6;
  TNode<BoolT> phi_bb89_8;
  TNode<String> phi_bb89_9;
  TNode<Object> phi_bb89_12;
  TNode<Number> tmp118;
  TNode<Number> tmp119;
  if (block89.is_used()) {
    ca_.Bind(&block89, &phi_bb89_5, &phi_bb89_6, &phi_bb89_8, &phi_bb89_9, &phi_bb89_12);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 105);
    tmp118 = UnsafeCast_Number_0(state_, TNode<Context>{p_context}, TNode<Object>{phi_bb89_12});
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 104);
    tmp119 = RegExpBuiltinsAssembler(state_).AdvanceStringIndex(TNode<String>{p_string}, TNode<Number>{tmp118}, TNode<BoolT>{tmp6}, p_isFastPath);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 107);
    if ((p_isFastPath)) {
      ca_.Goto(&block92, phi_bb89_5, phi_bb89_6, phi_bb89_8, phi_bb89_9);
    } else {
      ca_.Goto(&block93, phi_bb89_5, phi_bb89_6, phi_bb89_8, phi_bb89_9);
    }
  }

  TNode<FixedArray> phi_bb92_5;
  TNode<IntPtrT> phi_bb92_6;
  TNode<BoolT> phi_bb92_8;
  TNode<String> phi_bb92_9;
  TNode<BoolT> tmp120;
  TNode<BoolT> tmp121;
  if (block92.is_used()) {
    ca_.Bind(&block92, &phi_bb92_5, &phi_bb92_6, &phi_bb92_8, &phi_bb92_9);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 113);
    tmp120 = FromConstexpr_bool_constexpr_bool_0(state_, (CodeStubAssembler(state_).ConstexprUintPtrLessThan(String::kMaxLength, kSmiMaxValue)));
    CodeStubAssembler(state_).StaticAssert(TNode<BoolT>{tmp120});
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 114);
    tmp121 = CodeStubAssembler(state_).TaggedIsPositiveSmi(TNode<Object>{tmp119});
    ca_.Branch(tmp121, &block95, std::vector<Node*>{phi_bb92_5, phi_bb92_6, phi_bb92_8, phi_bb92_9}, &block96, std::vector<Node*>{phi_bb92_5, phi_bb92_6, phi_bb92_8, phi_bb92_9});
  }

  TNode<FixedArray> phi_bb96_5;
  TNode<IntPtrT> phi_bb96_6;
  TNode<BoolT> phi_bb96_8;
  TNode<String> phi_bb96_9;
  if (block96.is_used()) {
    ca_.Bind(&block96, &phi_bb96_5, &phi_bb96_6, &phi_bb96_8, &phi_bb96_9);
    CodeStubAssembler(state_).FailAssert("Torque assert 'TaggedIsPositiveSmi(newLastIndex)' failed", "src/builtins/regexp-match.tq", 114);
  }

  TNode<FixedArray> phi_bb95_5;
  TNode<IntPtrT> phi_bb95_6;
  TNode<BoolT> phi_bb95_8;
  TNode<String> phi_bb95_9;
  if (block95.is_used()) {
    ca_.Bind(&block95, &phi_bb95_5, &phi_bb95_6, &phi_bb95_8, &phi_bb95_9);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 107);
    ca_.Goto(&block94, phi_bb95_5, phi_bb95_6, phi_bb95_8, phi_bb95_9);
  }

  TNode<FixedArray> phi_bb93_5;
  TNode<IntPtrT> phi_bb93_6;
  TNode<BoolT> phi_bb93_8;
  TNode<String> phi_bb93_9;
  if (block93.is_used()) {
    ca_.Bind(&block93, &phi_bb93_5, &phi_bb93_6, &phi_bb93_8, &phi_bb93_9);
    ca_.Goto(&block94, phi_bb93_5, phi_bb93_6, phi_bb93_8, phi_bb93_9);
  }

  TNode<FixedArray> phi_bb94_5;
  TNode<IntPtrT> phi_bb94_6;
  TNode<BoolT> phi_bb94_8;
  TNode<String> phi_bb94_9;
  if (block94.is_used()) {
    ca_.Bind(&block94, &phi_bb94_5, &phi_bb94_6, &phi_bb94_8, &phi_bb94_9);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 117);
    StoreLastIndex_0(state_, TNode<Context>{p_context}, TNode<Object>{p_regexp}, TNode<Number>{tmp119}, p_isFastPath);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 57);
    ca_.Goto(&block36, phi_bb94_5, phi_bb94_6, tmp103, phi_bb94_8, phi_bb94_9);
  }

  TNode<FixedArray> phi_bb35_5;
  TNode<IntPtrT> phi_bb35_6;
  TNode<IntPtrT> phi_bb35_7;
  TNode<BoolT> phi_bb35_8;
  TNode<String> phi_bb35_9;
  if (block35.is_used()) {
    ca_.Bind(&block35, &phi_bb35_5, &phi_bb35_6, &phi_bb35_7, &phi_bb35_8, &phi_bb35_9);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 121);
    VerifiedUnreachable_0(state_);
  }

  TNode<Object> phi_bb1_3;
  if (block1.is_used()) {
    ca_.Bind(&block1, &phi_bb1_3);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 22);
    ca_.Goto(&block97, phi_bb1_3);
  }

  TNode<Object> phi_bb97_3;
    ca_.Bind(&block97, &phi_bb97_3);
  return TNode<Object>{phi_bb97_3};
}

TNode<Object> FastRegExpPrototypeMatchBody_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<JSRegExp> p_receiver, TNode<String> p_string) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Object> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 126);
    tmp0 = RegExpPrototypeMatchBody_0(state_, TNode<Context>{p_context}, TNode<JSReceiver>{p_receiver}, TNode<String>{p_string}, true);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 124);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<Object>{tmp0};
}

TNode<Object> SlowRegExpPrototypeMatchBody_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<JSReceiver> p_receiver, TNode<String> p_string) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Object> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 131);
    tmp0 = RegExpPrototypeMatchBody_0(state_, TNode<Context>{p_context}, TNode<JSReceiver>{p_receiver}, TNode<String>{p_string}, false);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 129);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<Object>{tmp0};
}

TF_BUILTIN(RegExpMatchFast, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<JSRegExp> parameter1 = UncheckedCast<JSRegExp>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<String> parameter2 = UncheckedCast<String>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Object> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 139);
    tmp0 = FastRegExpPrototypeMatchBody_0(state_, TNode<Context>{parameter0}, TNode<JSRegExp>{parameter1}, TNode<String>{parameter2});
    CodeStubAssembler(state_).Return(tmp0);
  }
}

TF_BUILTIN(RegExpPrototypeMatch, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kReceiver));
USE(parameter1);
  TNode<Object> parameter2 = UncheckedCast<Object>(Parameter(Descriptor::kString));
  USE(parameter2);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<JSReceiver> tmp0;
  TNode<String> tmp1;
  TNode<JSRegExp> tmp2;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 146);
    CodeStubAssembler(state_).ThrowIfNotJSReceiver(TNode<Context>{parameter0}, TNode<Object>{parameter1}, MessageTemplate::kIncompatibleMethodReceiver, "RegExp.prototype.@@match");
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 149);
    tmp0 = UnsafeCast_JSReceiver_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter1});
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 150);
    tmp1 = CodeStubAssembler(state_).ToString_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter2});
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 155);
    compiler::CodeAssemblerLabel label3(&ca_);
    tmp2 = Cast_FastJSRegExp_0(state_, TNode<Context>{parameter0}, TNode<HeapObject>{tmp0}, &label3);
    ca_.Goto(&block3);
    if (label3.is_used()) {
      ca_.Bind(&label3);
      ca_.Goto(&block4);
    }
  }

  TNode<Object> tmp4;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 156);
    tmp4 = SlowRegExpPrototypeMatchBody_0(state_, TNode<Context>{parameter0}, TNode<JSReceiver>{tmp0}, TNode<String>{tmp1});
    CodeStubAssembler(state_).Return(tmp4);
  }

  TNode<Object> tmp5;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 160);
    tmp5 = CodeStubAssembler(state_).CallBuiltin(Builtins::kRegExpMatchFast, parameter0, tmp2, tmp1);
    CodeStubAssembler(state_).Return(tmp5);
  }
}

TNode<BoolT> Is_FastJSRegExp_JSReceiver_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<JSReceiver> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<JSRegExp> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/cast.tq", 616);
    compiler::CodeAssemblerLabel label1(&ca_);
    tmp0 = Cast_FastJSRegExp_0(state_, TNode<Context>{p_context}, TNode<HeapObject>{p_o}, &label1);
    ca_.Goto(&block4);
    if (label1.is_used()) {
      ca_.Bind(&label1);
      ca_.Goto(&block5);
    }
  }

  TNode<BoolT> tmp2;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    tmp2 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block1, tmp2);
  }

  TNode<BoolT> tmp3;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../src/builtins/cast.tq", 617);
    tmp3 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    ca_.Goto(&block1, tmp3);
  }

  TNode<BoolT> phi_bb1_2;
  if (block1.is_used()) {
    ca_.Bind(&block1, &phi_bb1_2);
    ca_.SetSourcePosition("../../src/builtins/regexp-match.tq", 25);
    ca_.Goto(&block6);
  }

    ca_.Bind(&block6);
  return TNode<BoolT>{phi_bb1_2};
}

}  // namespace internal
}  // namespace v8

