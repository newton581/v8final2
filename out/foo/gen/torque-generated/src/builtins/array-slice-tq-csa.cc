#include "src/builtins/builtins-array-gen.h"
#include "src/builtins/builtins-bigint-gen.h"
#include "src/builtins/builtins-collections-gen.h"
#include "src/builtins/builtins-constructor-gen.h"
#include "src/builtins/builtins-data-view-gen.h"
#include "src/builtins/builtins-iterator-gen.h"
#include "src/builtins/builtins-promise-gen.h"
#include "src/builtins/builtins-promise.h"
#include "src/builtins/builtins-proxy-gen.h"
#include "src/builtins/builtins-regexp-gen.h"
#include "src/builtins/builtins-string-gen.h"
#include "src/builtins/builtins-typed-array-gen.h"
#include "src/builtins/builtins-utils-gen.h"
#include "src/builtins/builtins-wasm-gen.h"
#include "src/builtins/builtins.h"
#include "src/codegen/code-factory.h"
#include "src/heap/factory-inl.h"
#include "src/ic/binary-op-assembler.h"
#include "src/objects/arguments.h"
#include "src/objects/bigint.h"
#include "src/objects/elements-kind.h"
#include "src/objects/free-space.h"
#include "src/objects/js-break-iterator.h"
#include "src/objects/js-collator.h"
#include "src/objects/js-date-time-format.h"
#include "src/objects/js-display-names.h"
#include "src/objects/js-generator.h"
#include "src/objects/js-list-format.h"
#include "src/objects/js-locale.h"
#include "src/objects/js-number-format.h"
#include "src/objects/js-objects.h"
#include "src/objects/js-plural-rules.h"
#include "src/objects/js-promise.h"
#include "src/objects/js-regexp-string-iterator.h"
#include "src/objects/js-relative-time-format.h"
#include "src/objects/js-segment-iterator.h"
#include "src/objects/js-segmenter.h"
#include "src/objects/js-weak-refs.h"
#include "src/objects/objects.h"
#include "src/objects/ordered-hash-table.h"
#include "src/objects/property-array.h"
#include "src/objects/property-descriptor-object.h"
#include "src/objects/source-text-module.h"
#include "src/objects/stack-frame-info.h"
#include "src/objects/synthetic-module.h"
#include "src/objects/template-objects.h"
#include "src/torque/runtime-support.h"
#include "torque-generated/src/builtins/aggregate-error-tq-csa.h"
#include "torque-generated/src/builtins/array-copywithin-tq-csa.h"
#include "torque-generated/src/builtins/array-every-tq-csa.h"
#include "torque-generated/src/builtins/array-filter-tq-csa.h"
#include "torque-generated/src/builtins/array-find-tq-csa.h"
#include "torque-generated/src/builtins/array-findindex-tq-csa.h"
#include "torque-generated/src/builtins/array-foreach-tq-csa.h"
#include "torque-generated/src/builtins/array-from-tq-csa.h"
#include "torque-generated/src/builtins/array-isarray-tq-csa.h"
#include "torque-generated/src/builtins/array-join-tq-csa.h"
#include "torque-generated/src/builtins/array-lastindexof-tq-csa.h"
#include "torque-generated/src/builtins/array-map-tq-csa.h"
#include "torque-generated/src/builtins/array-of-tq-csa.h"
#include "torque-generated/src/builtins/array-reduce-right-tq-csa.h"
#include "torque-generated/src/builtins/array-reduce-tq-csa.h"
#include "torque-generated/src/builtins/array-reverse-tq-csa.h"
#include "torque-generated/src/builtins/array-shift-tq-csa.h"
#include "torque-generated/src/builtins/array-slice-tq-csa.h"
#include "torque-generated/src/builtins/array-some-tq-csa.h"
#include "torque-generated/src/builtins/array-splice-tq-csa.h"
#include "torque-generated/src/builtins/array-unshift-tq-csa.h"
#include "torque-generated/src/builtins/array-tq-csa.h"
#include "torque-generated/src/builtins/base-tq-csa.h"
#include "torque-generated/src/builtins/bigint-tq-csa.h"
#include "torque-generated/src/builtins/boolean-tq-csa.h"
#include "torque-generated/src/builtins/builtins-string-tq-csa.h"
#include "torque-generated/src/builtins/cast-tq-csa.h"
#include "torque-generated/src/builtins/collections-tq-csa.h"
#include "torque-generated/src/builtins/conversion-tq-csa.h"
#include "torque-generated/src/builtins/convert-tq-csa.h"
#include "torque-generated/src/builtins/console-tq-csa.h"
#include "torque-generated/src/builtins/data-view-tq-csa.h"
#include "torque-generated/src/builtins/finalization-registry-tq-csa.h"
#include "torque-generated/src/builtins/frames-tq-csa.h"
#include "torque-generated/src/builtins/frame-arguments-tq-csa.h"
#include "torque-generated/src/builtins/function-tq-csa.h"
#include "torque-generated/src/builtins/growable-fixed-array-tq-csa.h"
#include "torque-generated/src/builtins/ic-callable-tq-csa.h"
#include "torque-generated/src/builtins/ic-tq-csa.h"
#include "torque-generated/src/builtins/internal-coverage-tq-csa.h"
#include "torque-generated/src/builtins/internal-tq-csa.h"
#include "torque-generated/src/builtins/iterator-tq-csa.h"
#include "torque-generated/src/builtins/math-tq-csa.h"
#include "torque-generated/src/builtins/number-tq-csa.h"
#include "torque-generated/src/builtins/object-fromentries-tq-csa.h"
#include "torque-generated/src/builtins/object-tq-csa.h"
#include "torque-generated/src/builtins/promise-abstract-operations-tq-csa.h"
#include "torque-generated/src/builtins/promise-all-tq-csa.h"
#include "torque-generated/src/builtins/promise-all-element-closure-tq-csa.h"
#include "torque-generated/src/builtins/promise-any-tq-csa.h"
#include "torque-generated/src/builtins/promise-constructor-tq-csa.h"
#include "torque-generated/src/builtins/promise-finally-tq-csa.h"
#include "torque-generated/src/builtins/promise-misc-tq-csa.h"
#include "torque-generated/src/builtins/promise-race-tq-csa.h"
#include "torque-generated/src/builtins/promise-reaction-job-tq-csa.h"
#include "torque-generated/src/builtins/promise-resolve-tq-csa.h"
#include "torque-generated/src/builtins/promise-then-tq-csa.h"
#include "torque-generated/src/builtins/promise-jobs-tq-csa.h"
#include "torque-generated/src/builtins/proxy-constructor-tq-csa.h"
#include "torque-generated/src/builtins/proxy-delete-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-get-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-get-prototype-of-tq-csa.h"
#include "torque-generated/src/builtins/proxy-has-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-is-extensible-tq-csa.h"
#include "torque-generated/src/builtins/proxy-prevent-extensions-tq-csa.h"
#include "torque-generated/src/builtins/proxy-revocable-tq-csa.h"
#include "torque-generated/src/builtins/proxy-revoke-tq-csa.h"
#include "torque-generated/src/builtins/proxy-set-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-set-prototype-of-tq-csa.h"
#include "torque-generated/src/builtins/proxy-tq-csa.h"
#include "torque-generated/src/builtins/reflect-tq-csa.h"
#include "torque-generated/src/builtins/regexp-exec-tq-csa.h"
#include "torque-generated/src/builtins/regexp-match-all-tq-csa.h"
#include "torque-generated/src/builtins/regexp-match-tq-csa.h"
#include "torque-generated/src/builtins/regexp-replace-tq-csa.h"
#include "torque-generated/src/builtins/regexp-search-tq-csa.h"
#include "torque-generated/src/builtins/regexp-source-tq-csa.h"
#include "torque-generated/src/builtins/regexp-split-tq-csa.h"
#include "torque-generated/src/builtins/regexp-test-tq-csa.h"
#include "torque-generated/src/builtins/regexp-tq-csa.h"
#include "torque-generated/src/builtins/string-endswith-tq-csa.h"
#include "torque-generated/src/builtins/string-html-tq-csa.h"
#include "torque-generated/src/builtins/string-iterator-tq-csa.h"
#include "torque-generated/src/builtins/string-pad-tq-csa.h"
#include "torque-generated/src/builtins/string-repeat-tq-csa.h"
#include "torque-generated/src/builtins/string-replaceall-tq-csa.h"
#include "torque-generated/src/builtins/string-slice-tq-csa.h"
#include "torque-generated/src/builtins/string-startswith-tq-csa.h"
#include "torque-generated/src/builtins/string-substring-tq-csa.h"
#include "torque-generated/src/builtins/string-substr-tq-csa.h"
#include "torque-generated/src/builtins/symbol-tq-csa.h"
#include "torque-generated/src/builtins/torque-internal-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-createtypedarray-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-every-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-entries-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-filter-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-find-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-findindex-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-foreach-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-from-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-keys-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-of-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-reduce-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-reduceright-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-set-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-slice-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-some-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-sort-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-subarray-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-values-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-tq-csa.h"
#include "torque-generated/src/builtins/wasm-tq-csa.h"
#include "torque-generated/src/builtins/weak-ref-tq-csa.h"
#include "torque-generated/src/ic/handler-configuration-tq-csa.h"
#include "torque-generated/src/objects/allocation-site-tq-csa.h"
#include "torque-generated/src/objects/api-callbacks-tq-csa.h"
#include "torque-generated/src/objects/arguments-tq-csa.h"
#include "torque-generated/src/objects/cell-tq-csa.h"
#include "torque-generated/src/objects/code-tq-csa.h"
#include "torque-generated/src/objects/contexts-tq-csa.h"
#include "torque-generated/src/objects/data-handler-tq-csa.h"
#include "torque-generated/src/objects/debug-objects-tq-csa.h"
#include "torque-generated/src/objects/descriptor-array-tq-csa.h"
#include "torque-generated/src/objects/embedder-data-array-tq-csa.h"
#include "torque-generated/src/objects/feedback-cell-tq-csa.h"
#include "torque-generated/src/objects/feedback-vector-tq-csa.h"
#include "torque-generated/src/objects/fixed-array-tq-csa.h"
#include "torque-generated/src/objects/foreign-tq-csa.h"
#include "torque-generated/src/objects/free-space-tq-csa.h"
#include "torque-generated/src/objects/heap-number-tq-csa.h"
#include "torque-generated/src/objects/heap-object-tq-csa.h"
#include "torque-generated/src/objects/intl-objects-tq-csa.h"
#include "torque-generated/src/objects/js-array-buffer-tq-csa.h"
#include "torque-generated/src/objects/js-array-tq-csa.h"
#include "torque-generated/src/objects/js-collection-iterator-tq-csa.h"
#include "torque-generated/src/objects/js-collection-tq-csa.h"
#include "torque-generated/src/objects/js-generator-tq-csa.h"
#include "torque-generated/src/objects/js-objects-tq-csa.h"
#include "torque-generated/src/objects/js-promise-tq-csa.h"
#include "torque-generated/src/objects/js-proxy-tq-csa.h"
#include "torque-generated/src/objects/js-regexp-string-iterator-tq-csa.h"
#include "torque-generated/src/objects/js-regexp-tq-csa.h"
#include "torque-generated/src/objects/js-weak-refs-tq-csa.h"
#include "torque-generated/src/objects/literal-objects-tq-csa.h"
#include "torque-generated/src/objects/map-tq-csa.h"
#include "torque-generated/src/objects/microtask-tq-csa.h"
#include "torque-generated/src/objects/module-tq-csa.h"
#include "torque-generated/src/objects/name-tq-csa.h"
#include "torque-generated/src/objects/oddball-tq-csa.h"
#include "torque-generated/src/objects/ordered-hash-table-tq-csa.h"
#include "torque-generated/src/objects/primitive-heap-object-tq-csa.h"
#include "torque-generated/src/objects/promise-tq-csa.h"
#include "torque-generated/src/objects/property-array-tq-csa.h"
#include "torque-generated/src/objects/property-cell-tq-csa.h"
#include "torque-generated/src/objects/property-descriptor-object-tq-csa.h"
#include "torque-generated/src/objects/prototype-info-tq-csa.h"
#include "torque-generated/src/objects/regexp-match-info-tq-csa.h"
#include "torque-generated/src/objects/scope-info-tq-csa.h"
#include "torque-generated/src/objects/script-tq-csa.h"
#include "torque-generated/src/objects/shared-function-info-tq-csa.h"
#include "torque-generated/src/objects/source-text-module-tq-csa.h"
#include "torque-generated/src/objects/stack-frame-info-tq-csa.h"
#include "torque-generated/src/objects/string-tq-csa.h"
#include "torque-generated/src/objects/struct-tq-csa.h"
#include "torque-generated/src/objects/synthetic-module-tq-csa.h"
#include "torque-generated/src/objects/template-objects-tq-csa.h"
#include "torque-generated/src/objects/template-tq-csa.h"
#include "torque-generated/src/wasm/wasm-objects-tq-csa.h"
#include "torque-generated/test/torque/test-torque-tq-csa.h"
#include "torque-generated/third_party/v8/builtins/array-sort-tq-csa.h"

namespace v8 {
namespace internal {

TNode<JSArray> HandleSimpleArgumentsSlice_0(compiler::CodeAssemblerState* state_, TNode<NativeContext> p_context, TNode<JSArgumentsObject> p_args, TNode<Smi> p_start, TNode<Smi> p_count, compiler::CodeAssemblerLabel* label_Bailout) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Smi> tmp0;
  TNode<BoolT> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 11);
    tmp0 = FromConstexpr_Smi_constexpr_int31_0(state_, FixedArray::kMaxRegularLength);
    tmp1 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{p_count}, TNode<Smi>{tmp0});
    ca_.Branch(tmp1, &block3, std::vector<Node*>{}, &block4, std::vector<Node*>{});
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.Goto(&block1);
  }

  TNode<Smi> tmp2;
  TNode<IntPtrT> tmp3;
  TNode<FixedArrayBase> tmp4;
  TNode<FixedArray> tmp5;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 13);
    tmp2 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{p_start}, TNode<Smi>{p_count});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 15);
    tmp3 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp4 = CodeStubAssembler(state_).LoadReference<FixedArrayBase>(CodeStubAssembler::Reference{p_args, tmp3});
    compiler::CodeAssemblerLabel label6(&ca_);
    tmp5 = Cast_FixedArray_0(state_, TNode<HeapObject>{tmp4}, &label6);
    ca_.Goto(&block5);
    if (label6.is_used()) {
      ca_.Bind(&label6);
      ca_.Goto(&block6);
    }
  }

  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.Goto(&block1);
  }

  TNode<IntPtrT> tmp7;
  TNode<Smi> tmp8;
  TNode<BoolT> tmp9;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 16);
    tmp7 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp8 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp5, tmp7});
    tmp9 = CodeStubAssembler(state_).SmiAbove(TNode<Smi>{tmp2}, TNode<Smi>{tmp8});
    ca_.Branch(tmp9, &block7, std::vector<Node*>{}, &block8, std::vector<Node*>{});
  }

  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.Goto(&block1);
  }

  TNode<Map> tmp10;
  TNode<JSArray> tmp11;
  TNode<IntPtrT> tmp12;
  TNode<FixedArrayBase> tmp13;
  TNode<FixedArray> tmp14;
  if (block8.is_used()) {
    ca_.Bind(&block8);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 19);
    tmp10 = CodeStubAssembler(state_).LoadJSArrayElementsMap(ElementsKind::HOLEY_ELEMENTS, TNode<NativeContext>{p_context});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 21);
    tmp11 = CodeStubAssembler(state_).AllocateJSArray(ElementsKind::HOLEY_ELEMENTS, TNode<Map>{tmp10}, TNode<Smi>{p_count}, TNode<Smi>{p_count});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 23);
    tmp12 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp13 = CodeStubAssembler(state_).LoadReference<FixedArrayBase>(CodeStubAssembler::Reference{tmp11, tmp12});
    compiler::CodeAssemblerLabel label15(&ca_);
    tmp14 = Cast_FixedArray_0(state_, TNode<HeapObject>{tmp13}, &label15);
    ca_.Goto(&block9);
    if (label15.is_used()) {
      ca_.Bind(&label15);
      ca_.Goto(&block10);
    }
  }

  if (block10.is_used()) {
    ca_.Bind(&block10);
    ca_.Goto(&block1);
  }

  TNode<IntPtrT> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<IntPtrT> tmp18;
  if (block9.is_used()) {
    ca_.Bind(&block9);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 26);
    tmp16 = Convert_intptr_Smi_0(state_, TNode<Smi>{p_start});
    tmp17 = Convert_intptr_Smi_0(state_, TNode<Smi>{p_count});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 24);
    tmp18 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    CodeStubAssembler(state_).CopyElements(ElementsKind::PACKED_ELEMENTS, TNode<FixedArrayBase>{tmp14}, TNode<IntPtrT>{tmp18}, TNode<FixedArrayBase>{tmp5}, TNode<IntPtrT>{tmp16}, TNode<IntPtrT>{tmp17});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 6);
    ca_.Goto(&block11);
  }

  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.Goto(label_Bailout);
  }

    ca_.Bind(&block11);
  return TNode<JSArray>{tmp11};
}

TNode<JSArray> HandleFastAliasedSloppyArgumentsSlice_0(compiler::CodeAssemblerState* state_, TNode<NativeContext> p_context, TNode<JSArgumentsObject> p_args, TNode<Smi> p_start, TNode<Smi> p_count, compiler::CodeAssemblerLabel* label_Bailout) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi> block19(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi> block20(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block22(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block30(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block31(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block23(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi> block37(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi> block38(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Object> block24(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block40(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Smi> tmp0;
  TNode<BoolT> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 35);
    tmp0 = FromConstexpr_Smi_constexpr_int31_0(state_, FixedArray::kMaxRegularLength);
    tmp1 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{p_count}, TNode<Smi>{tmp0});
    ca_.Branch(tmp1, &block3, std::vector<Node*>{}, &block4, std::vector<Node*>{});
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.Goto(&block1);
  }

  TNode<IntPtrT> tmp2;
  TNode<FixedArrayBase> tmp3;
  TNode<SloppyArgumentsElements> tmp4;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 38);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp3 = CodeStubAssembler(state_).LoadReference<FixedArrayBase>(CodeStubAssembler::Reference{p_args, tmp2});
    compiler::CodeAssemblerLabel label5(&ca_);
    tmp4 = Cast_SloppyArgumentsElements_0(state_, TNode<HeapObject>{tmp3}, &label5);
    ca_.Goto(&block5);
    if (label5.is_used()) {
      ca_.Bind(&label5);
      ca_.Goto(&block6);
    }
  }

  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.Goto(&block1);
  }

  TNode<IntPtrT> tmp6;
  TNode<Smi> tmp7;
  TNode<Smi> tmp8;
  TNode<IntPtrT> tmp9;
  TNode<FixedArray> tmp10;
  TNode<FixedArray> tmp11;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 39);
    tmp6 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp7 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp4, tmp6});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 43);
    tmp8 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{p_start}, TNode<Smi>{p_count});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 45);
    tmp9 = FromConstexpr_intptr_constexpr_int31_0(state_, 12);
    tmp10 = CodeStubAssembler(state_).LoadReference<FixedArray>(CodeStubAssembler::Reference{tmp4, tmp9});
    compiler::CodeAssemblerLabel label12(&ca_);
    tmp11 = Cast_FixedArray_0(state_, TNode<HeapObject>{tmp10}, &label12);
    ca_.Goto(&block7);
    if (label12.is_used()) {
      ca_.Bind(&label12);
      ca_.Goto(&block8);
    }
  }

  if (block8.is_used()) {
    ca_.Bind(&block8);
    ca_.Goto(&block1);
  }

  TNode<IntPtrT> tmp13;
  TNode<Smi> tmp14;
  TNode<BoolT> tmp15;
  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 47);
    tmp13 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp14 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp11, tmp13});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 48);
    tmp15 = CodeStubAssembler(state_).SmiAbove(TNode<Smi>{tmp8}, TNode<Smi>{tmp14});
    ca_.Branch(tmp15, &block9, std::vector<Node*>{}, &block10, std::vector<Node*>{});
  }

  if (block9.is_used()) {
    ca_.Bind(&block9);
    ca_.Goto(&block1);
  }

  TNode<IntPtrT> tmp16;
  TNode<Context> tmp17;
  TNode<Map> tmp18;
  TNode<JSArray> tmp19;
  TNode<Smi> tmp20;
  TNode<IntPtrT> tmp21;
  TNode<FixedArrayBase> tmp22;
  TNode<FixedArray> tmp23;
  TNode<Smi> tmp24;
  if (block10.is_used()) {
    ca_.Bind(&block10);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 50);
    tmp16 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp17 = CodeStubAssembler(state_).LoadReference<Context>(CodeStubAssembler::Reference{tmp4, tmp16});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 53);
    tmp18 = CodeStubAssembler(state_).LoadJSArrayElementsMap(ElementsKind::HOLEY_ELEMENTS, TNode<NativeContext>{p_context});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 55);
    tmp19 = CodeStubAssembler(state_).AllocateJSArray(ElementsKind::HOLEY_ELEMENTS, TNode<Map>{tmp18}, TNode<Smi>{p_count}, TNode<Smi>{p_count});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 57);
    tmp20 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 58);
    tmp21 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp22 = CodeStubAssembler(state_).LoadReference<FixedArrayBase>(CodeStubAssembler::Reference{tmp19, tmp21});
    tmp23 = UnsafeCast_FixedArray_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp22});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 59);
    tmp24 = CodeStubAssembler(state_).SmiMin(TNode<Smi>{tmp7}, TNode<Smi>{tmp8});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 62);
    ca_.Goto(&block13, tmp20, p_start);
  }

  TNode<Smi> phi_bb13_12;
  TNode<Smi> phi_bb13_15;
  TNode<BoolT> tmp25;
  if (block13.is_used()) {
    ca_.Bind(&block13, &phi_bb13_12, &phi_bb13_15);
    tmp25 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{phi_bb13_15}, TNode<Smi>{tmp24});
    ca_.Branch(tmp25, &block11, std::vector<Node*>{phi_bb13_12, phi_bb13_15}, &block12, std::vector<Node*>{phi_bb13_12, phi_bb13_15});
  }

  TNode<Smi> phi_bb11_12;
  TNode<Smi> phi_bb11_15;
  TNode<IntPtrT> tmp26;
  TNode<IntPtrT> tmp27;
  TNode<IntPtrT> tmp28;
  TNode<Smi> tmp29;
  TNode<IntPtrT> tmp30;
  TNode<IntPtrT> tmp31;
  TNode<UintPtrT> tmp32;
  TNode<UintPtrT> tmp33;
  TNode<BoolT> tmp34;
  if (block11.is_used()) {
    ca_.Bind(&block11, &phi_bb11_12, &phi_bb11_15);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 63);
    tmp26 = FromConstexpr_intptr_constexpr_int31_0(state_, 16);
    tmp27 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp28 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/arguments.tq", 83);
    tmp29 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp4, tmp28});
    tmp30 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp29});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp31 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb11_15});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp32 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp31});
    tmp33 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp30});
    tmp34 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp32}, TNode<UintPtrT>{tmp33});
    ca_.Branch(tmp34, &block19, std::vector<Node*>{phi_bb11_12, phi_bb11_15, phi_bb11_15, phi_bb11_15}, &block20, std::vector<Node*>{phi_bb11_12, phi_bb11_15, phi_bb11_15, phi_bb11_15});
  }

  TNode<Smi> phi_bb19_12;
  TNode<Smi> phi_bb19_15;
  TNode<Smi> phi_bb19_20;
  TNode<Smi> phi_bb19_21;
  TNode<IntPtrT> tmp35;
  TNode<IntPtrT> tmp36;
  TNode<IntPtrT> tmp37;
  TNode<HeapObject> tmp38;
  TNode<IntPtrT> tmp39;
  TNode<Object> tmp40;
  TNode<Oddball> tmp41;
  TNode<BoolT> tmp42;
  if (block19.is_used()) {
    ca_.Bind(&block19, &phi_bb19_12, &phi_bb19_15, &phi_bb19_20, &phi_bb19_21);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp35 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp36 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp31}, TNode<IntPtrT>{tmp35});
    tmp37 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp26}, TNode<IntPtrT>{tmp36});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp38, tmp39) = NewReference_Smi_OR_TheHole_0(state_, TNode<HeapObject>{tmp4}, TNode<IntPtrT>{tmp37}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 63);
    tmp40 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp38, tmp39});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 65);
    tmp41 = TheHole_0(state_);
    tmp42 = CodeStubAssembler(state_).TaggedNotEqual(TNode<Object>{tmp40}, TNode<HeapObject>{tmp41});
    ca_.Branch(tmp42, &block22, std::vector<Node*>{phi_bb19_12, phi_bb19_15}, &block23, std::vector<Node*>{phi_bb19_12, phi_bb19_15});
  }

  TNode<Smi> phi_bb20_12;
  TNode<Smi> phi_bb20_15;
  TNode<Smi> phi_bb20_20;
  TNode<Smi> phi_bb20_21;
  if (block20.is_used()) {
    ca_.Bind(&block20, &phi_bb20_12, &phi_bb20_15, &phi_bb20_20, &phi_bb20_21);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb22_12;
  TNode<Smi> phi_bb22_15;
  TNode<IntPtrT> tmp43;
  TNode<IntPtrT> tmp44;
  TNode<IntPtrT> tmp45;
  TNode<Smi> tmp46;
  TNode<IntPtrT> tmp47;
  TNode<Smi> tmp48;
  TNode<IntPtrT> tmp49;
  TNode<UintPtrT> tmp50;
  TNode<UintPtrT> tmp51;
  TNode<BoolT> tmp52;
  if (block22.is_used()) {
    ca_.Bind(&block22, &phi_bb22_12, &phi_bb22_15);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 65);
    tmp43 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp44 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp45 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp46 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp17, tmp45});
    tmp47 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp46});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 65);
    tmp48 = UnsafeCast_Smi_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp40});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp49 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp48});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp50 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp49});
    tmp51 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp47});
    tmp52 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp50}, TNode<UintPtrT>{tmp51});
    ca_.Branch(tmp52, &block30, std::vector<Node*>{phi_bb22_12, phi_bb22_15}, &block31, std::vector<Node*>{phi_bb22_12, phi_bb22_15});
  }

  TNode<Smi> phi_bb30_12;
  TNode<Smi> phi_bb30_15;
  TNode<IntPtrT> tmp53;
  TNode<IntPtrT> tmp54;
  TNode<IntPtrT> tmp55;
  TNode<HeapObject> tmp56;
  TNode<IntPtrT> tmp57;
  TNode<Object> tmp58;
  if (block30.is_used()) {
    ca_.Bind(&block30, &phi_bb30_12, &phi_bb30_15);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp53 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp54 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp49}, TNode<IntPtrT>{tmp53});
    tmp55 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp43}, TNode<IntPtrT>{tmp54});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp56, tmp57) = NewReference_Object_0(state_, TNode<HeapObject>{tmp17}, TNode<IntPtrT>{tmp55}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 65);
    tmp58 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp56, tmp57});
    ca_.Goto(&block24, phi_bb30_12, phi_bb30_15, tmp58);
  }

  TNode<Smi> phi_bb31_12;
  TNode<Smi> phi_bb31_15;
  if (block31.is_used()) {
    ca_.Bind(&block31, &phi_bb31_12, &phi_bb31_15);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb23_12;
  TNode<Smi> phi_bb23_15;
  TNode<IntPtrT> tmp59;
  TNode<IntPtrT> tmp60;
  TNode<IntPtrT> tmp61;
  TNode<Smi> tmp62;
  TNode<IntPtrT> tmp63;
  TNode<IntPtrT> tmp64;
  TNode<UintPtrT> tmp65;
  TNode<UintPtrT> tmp66;
  TNode<BoolT> tmp67;
  if (block23.is_used()) {
    ca_.Bind(&block23, &phi_bb23_12, &phi_bb23_15);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 66);
    tmp59 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp60 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp61 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp62 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp11, tmp61});
    tmp63 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp62});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp64 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb23_15});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp65 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp64});
    tmp66 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp63});
    tmp67 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp65}, TNode<UintPtrT>{tmp66});
    ca_.Branch(tmp67, &block37, std::vector<Node*>{phi_bb23_12, phi_bb23_15, phi_bb23_15, phi_bb23_15}, &block38, std::vector<Node*>{phi_bb23_12, phi_bb23_15, phi_bb23_15, phi_bb23_15});
  }

  TNode<Smi> phi_bb37_12;
  TNode<Smi> phi_bb37_15;
  TNode<Smi> phi_bb37_21;
  TNode<Smi> phi_bb37_22;
  TNode<IntPtrT> tmp68;
  TNode<IntPtrT> tmp69;
  TNode<IntPtrT> tmp70;
  TNode<HeapObject> tmp71;
  TNode<IntPtrT> tmp72;
  TNode<Object> tmp73;
  if (block37.is_used()) {
    ca_.Bind(&block37, &phi_bb37_12, &phi_bb37_15, &phi_bb37_21, &phi_bb37_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp68 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp69 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp64}, TNode<IntPtrT>{tmp68});
    tmp70 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp59}, TNode<IntPtrT>{tmp69});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp71, tmp72) = NewReference_Object_0(state_, TNode<HeapObject>{tmp11}, TNode<IntPtrT>{tmp70}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 66);
    tmp73 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp71, tmp72});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 65);
    ca_.Goto(&block24, phi_bb37_12, phi_bb37_15, tmp73);
  }

  TNode<Smi> phi_bb38_12;
  TNode<Smi> phi_bb38_15;
  TNode<Smi> phi_bb38_21;
  TNode<Smi> phi_bb38_22;
  if (block38.is_used()) {
    ca_.Bind(&block38, &phi_bb38_12, &phi_bb38_15, &phi_bb38_21, &phi_bb38_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb24_12;
  TNode<Smi> phi_bb24_15;
  TNode<Object> phi_bb24_17;
  TNode<Object> tmp74;
  TNode<Smi> tmp75;
  TNode<Smi> tmp76;
  TNode<Smi> tmp77;
  TNode<Smi> tmp78;
  if (block24.is_used()) {
    ca_.Bind(&block24, &phi_bb24_12, &phi_bb24_15, &phi_bb24_17);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 64);
    tmp74 = UnsafeCast_JSReceiver_OR_Smi_OR_HeapNumber_OR_BigInt_OR_String_OR_Symbol_OR_True_OR_False_OR_Null_OR_Undefined_OR_TheHole_0(state_, TNode<Context>{p_context}, TNode<Object>{phi_bb24_17});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 72);
    tmp75 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp76 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb24_12}, TNode<Smi>{tmp75});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 71);
    CodeStubAssembler(state_).StoreFixedArrayElement(TNode<FixedArray>{tmp23}, TNode<Smi>{phi_bb24_12}, TNode<Object>{tmp74}, UNSAFE_SKIP_WRITE_BARRIER);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 62);
    tmp77 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp78 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb24_15}, TNode<Smi>{tmp77});
    ca_.Goto(&block13, tmp76, tmp78);
  }

  TNode<Smi> phi_bb12_12;
  TNode<Smi> phi_bb12_15;
  TNode<Smi> tmp79;
  TNode<Smi> tmp80;
  TNode<Smi> tmp81;
  TNode<IntPtrT> tmp82;
  TNode<IntPtrT> tmp83;
  TNode<IntPtrT> tmp84;
  if (block12.is_used()) {
    ca_.Bind(&block12, &phi_bb12_12, &phi_bb12_15);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 77);
    tmp79 = CodeStubAssembler(state_).SmiMax(TNode<Smi>{tmp7}, TNode<Smi>{p_start});
    tmp80 = CodeStubAssembler(state_).SmiMin(TNode<Smi>{tmp79}, TNode<Smi>{tmp8});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 78);
    tmp81 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp8}, TNode<Smi>{tmp80});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 80);
    tmp82 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb12_12});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 81);
    tmp83 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp80});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 82);
    tmp84 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp81});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 79);
    CodeStubAssembler(state_).CopyElements(ElementsKind::PACKED_ELEMENTS, TNode<FixedArrayBase>{tmp23}, TNode<IntPtrT>{tmp82}, TNode<FixedArrayBase>{tmp11}, TNode<IntPtrT>{tmp83}, TNode<IntPtrT>{tmp84});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 30);
    ca_.Goto(&block40);
  }

  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.Goto(label_Bailout);
  }

    ca_.Bind(&block40);
  return TNode<JSArray>{tmp19};
}

TNode<JSArray> HandleFastSlice_0(compiler::CodeAssemblerState* state_, TNode<NativeContext> p_context, TNode<Object> p_o, TNode<Number> p_startNumber, TNode<Number> p_countNumber, compiler::CodeAssemblerLabel* label_Bailout) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block14(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block15(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block16(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block20(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block19(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block24(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block23(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block25(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block29(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block28(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block26(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block30(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block31(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<JSArgumentsObject> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<JSArgumentsObject, JSArgumentsObject> block33(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<JSArgumentsObject, JSArgumentsObject> block32(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<JSArray> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<JSArray> block34(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Smi> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 90);
    compiler::CodeAssemblerLabel label1(&ca_);
    tmp0 = Cast_Smi_0(state_, TNode<Object>{p_startNumber}, &label1);
    ca_.Goto(&block3);
    if (label1.is_used()) {
      ca_.Bind(&label1);
      ca_.Goto(&block4);
    }
  }

  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.Goto(&block1);
  }

  TNode<Smi> tmp2;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 91);
    compiler::CodeAssemblerLabel label3(&ca_);
    tmp2 = Cast_Smi_0(state_, TNode<Object>{p_countNumber}, &label3);
    ca_.Goto(&block5);
    if (label3.is_used()) {
      ca_.Bind(&label3);
      ca_.Goto(&block6);
    }
  }

  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.Goto(&block1);
  }

  TNode<Smi> tmp4;
  TNode<BoolT> tmp5;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 92);
    tmp4 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp5 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{tmp0}, TNode<Smi>{tmp4});
    ca_.Branch(tmp5, &block7, std::vector<Node*>{}, &block8, std::vector<Node*>{});
  }

  if (block8.is_used()) {
    ca_.Bind(&block8);
    CodeStubAssembler(state_).FailAssert("Torque assert 'start >= 0' failed", "src/builtins/array-slice.tq", 92);
  }

  TNode<JSArray> tmp6;
  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 96);
    compiler::CodeAssemblerLabel label7(&ca_);
    tmp6 = Cast_FastJSArrayForCopy_1(state_, TNode<Context>{p_context}, TNode<Object>{p_o}, &label7);
    ca_.Goto(&block13);
    if (label7.is_used()) {
      ca_.Bind(&label7);
      ca_.Goto(&block14);
    }
  }

  TNode<JSArgumentsObject> tmp8;
  if (block14.is_used()) {
    ca_.Bind(&block14);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 106);
    compiler::CodeAssemblerLabel label9(&ca_);
    tmp8 = Cast_JSStrictArgumentsObject_1(state_, TNode<Context>{p_context}, TNode<Object>{ca_.UncheckedCast<Object>(p_o)}, &label9);
    ca_.Goto(&block19);
    if (label9.is_used()) {
      ca_.Bind(&label9);
      ca_.Goto(&block20);
    }
  }

  TNode<Smi> tmp10;
  TNode<Smi> tmp11;
  TNode<BoolT> tmp12;
  if (block13.is_used()) {
    ca_.Bind(&block13);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 103);
    tmp10 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp0}, TNode<Smi>{tmp2});
    tmp11 = CodeStubAssembler(state_).LoadFastJSArrayLength(TNode<JSArray>{tmp6});
    tmp12 = CodeStubAssembler(state_).SmiAbove(TNode<Smi>{tmp10}, TNode<Smi>{tmp11});
    ca_.Branch(tmp12, &block15, std::vector<Node*>{}, &block16, std::vector<Node*>{});
  }

  if (block15.is_used()) {
    ca_.Bind(&block15);
    ca_.Goto(&block1);
  }

  TNode<JSArray> tmp13;
  if (block16.is_used()) {
    ca_.Bind(&block16);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 104);
    tmp13 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kExtractFastJSArray, p_context, tmp6, tmp0, tmp2));
    ca_.Goto(&block2, tmp13);
  }

  TNode<JSArgumentsObject> tmp14;
  if (block20.is_used()) {
    ca_.Bind(&block20);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 109);
    compiler::CodeAssemblerLabel label15(&ca_);
    tmp14 = Cast_JSSloppyArgumentsObject_1(state_, TNode<Context>{p_context}, TNode<Object>{ca_.UncheckedCast<Object>(p_o)}, &label15);
    ca_.Goto(&block23);
    if (label15.is_used()) {
      ca_.Bind(&label15);
      ca_.Goto(&block24);
    }
  }

  if (block19.is_used()) {
    ca_.Bind(&block19);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 107);
    ca_.Goto(&block10, tmp8);
  }

  if (block24.is_used()) {
    ca_.Bind(&block24);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 120);
    ca_.Goto(&block1);
  }

  TNode<IntPtrT> tmp16;
  TNode<Map> tmp17;
  TNode<BoolT> tmp18;
  if (block23.is_used()) {
    ca_.Bind(&block23);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 110);
    tmp16 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp17 = CodeStubAssembler(state_).LoadReference<Map>(CodeStubAssembler::Reference{tmp14, tmp16});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 111);
    tmp18 = CodeStubAssembler(state_).IsFastAliasedArgumentsMap(TNode<Context>{p_context}, TNode<Map>{tmp17});
    ca_.Branch(tmp18, &block25, std::vector<Node*>{}, &block26, std::vector<Node*>{});
  }

  TNode<JSArray> tmp19;
  if (block25.is_used()) {
    ca_.Bind(&block25);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 112);
    compiler::CodeAssemblerLabel label20(&ca_);
    tmp19 = HandleFastAliasedSloppyArgumentsSlice_0(state_, TNode<NativeContext>{p_context}, TNode<JSArgumentsObject>{tmp14}, TNode<Smi>{tmp0}, TNode<Smi>{tmp2}, &label20);
    ca_.Goto(&block28);
    if (label20.is_used()) {
      ca_.Bind(&label20);
      ca_.Goto(&block29);
    }
  }

  if (block29.is_used()) {
    ca_.Bind(&block29);
    ca_.Goto(&block1);
  }

  if (block28.is_used()) {
    ca_.Bind(&block28);
    ca_.Goto(&block2, tmp19);
  }

  TNode<BoolT> tmp21;
  if (block26.is_used()) {
    ca_.Bind(&block26);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 114);
    tmp21 = CodeStubAssembler(state_).IsSloppyArgumentsMap(TNode<Context>{p_context}, TNode<Map>{tmp17});
    ca_.Branch(tmp21, &block30, std::vector<Node*>{}, &block31, std::vector<Node*>{});
  }

  if (block30.is_used()) {
    ca_.Bind(&block30);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 115);
    ca_.Goto(&block10, tmp14);
  }

  if (block31.is_used()) {
    ca_.Bind(&block31);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 117);
    ca_.Goto(&block1);
  }

  TNode<JSArgumentsObject> phi_bb10_6;
  TNode<JSArray> tmp22;
  if (block10.is_used()) {
    ca_.Bind(&block10, &phi_bb10_6);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 124);
    compiler::CodeAssemblerLabel label23(&ca_);
    tmp22 = HandleSimpleArgumentsSlice_0(state_, TNode<NativeContext>{p_context}, TNode<JSArgumentsObject>{phi_bb10_6}, TNode<Smi>{tmp0}, TNode<Smi>{tmp2}, &label23);
    ca_.Goto(&block32, phi_bb10_6, phi_bb10_6);
    if (label23.is_used()) {
      ca_.Bind(&label23);
      ca_.Goto(&block33, phi_bb10_6, phi_bb10_6);
    }
  }

  TNode<JSArgumentsObject> phi_bb33_6;
  TNode<JSArgumentsObject> phi_bb33_8;
  if (block33.is_used()) {
    ca_.Bind(&block33, &phi_bb33_6, &phi_bb33_8);
    ca_.Goto(&block1);
  }

  TNode<JSArgumentsObject> phi_bb32_6;
  TNode<JSArgumentsObject> phi_bb32_8;
  if (block32.is_used()) {
    ca_.Bind(&block32, &phi_bb32_6, &phi_bb32_8);
    ca_.Goto(&block2, tmp22);
  }

  TNode<JSArray> phi_bb2_4;
  if (block2.is_used()) {
    ca_.Bind(&block2, &phi_bb2_4);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 86);
    ca_.Goto(&block34, phi_bb2_4);
  }

  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.Goto(label_Bailout);
  }

  TNode<JSArray> phi_bb34_4;
    ca_.Bind(&block34, &phi_bb34_4);
  return TNode<JSArray>{phi_bb34_4};
}

TF_BUILTIN(ArrayPrototypeSlice, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  Node* argc = Parameter(Descriptor::kJSActualArgumentsCount);
  TNode<IntPtrT> arguments_length(ChangeInt32ToIntPtr(UncheckedCast<Int32T>(argc)));
  TNode<RawPtrT> arguments_frame = UncheckedCast<RawPtrT>(LoadFramePointer());
  TorqueStructArguments torque_arguments(GetFrameArguments(arguments_frame, arguments_length));
  CodeStubArguments arguments(this, torque_arguments);
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = arguments.GetReceiver();
USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Number> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Number> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block15(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block16(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Number> block17(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block20(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block19(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block22(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block21(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block24(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block23(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block26(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block25(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block28(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block27(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block30(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block29(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block34(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block33(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Number, Number> block37(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Number, Number> block35(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Number, Number, Number> block38(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Number, Number, Number> block39(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Number, Number> block36(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<BoolT> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 134);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp1 = CodeStubAssembler(state_).WordEqual(TNode<IntPtrT>{torque_arguments.length}, TNode<IntPtrT>{tmp0});
    ca_.Branch(tmp1, &block1, std::vector<Node*>{}, &block2, std::vector<Node*>{});
  }

  TNode<JSArray> tmp2;
  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 136);
    compiler::CodeAssemblerLabel label3(&ca_);
    tmp2 = Cast_FastJSArrayForCopy_1(state_, TNode<Context>{parameter0}, TNode<Object>{parameter1}, &label3);
    ca_.Goto(&block5);
    if (label3.is_used()) {
      ca_.Bind(&label3);
      ca_.Goto(&block6);
    }
  }

  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 134);
    ca_.Goto(&block2);
  }

  TNode<JSArray> tmp4;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 137);
    tmp4 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kCloneFastJSArray, parameter0, tmp2));
    arguments.PopAndReturn(tmp4);
  }

  TNode<JSReceiver> tmp5;
  TNode<Number> tmp6;
  TNode<IntPtrT> tmp7;
  TNode<Object> tmp8;
  TNode<Number> tmp9;
  TNode<Number> tmp10;
  TNode<BoolT> tmp11;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 145);
    tmp5 = CodeStubAssembler(state_).ToObject_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 148);
    tmp6 = GetLengthProperty_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp5});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 151);
    tmp7 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp8 = CodeStubAssembler(state_).GetArgumentValue(TorqueStructArguments{TNode<RawPtrT>{torque_arguments.frame}, TNode<RawPtrT>{torque_arguments.base}, TNode<IntPtrT>{torque_arguments.length}}, TNode<IntPtrT>{tmp7});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 152);
    tmp9 = ToInteger_Inline_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp8});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 156);
    tmp10 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp11 = NumberIsLessThan_0(state_, TNode<Number>{tmp9}, TNode<Number>{tmp10});
    ca_.Branch(tmp11, &block7, std::vector<Node*>{}, &block8, std::vector<Node*>{});
  }

  TNode<Number> tmp12;
  TNode<Number> tmp13;
  TNode<Number> tmp14;
  if (block7.is_used()) {
    ca_.Bind(&block7);
    tmp12 = CodeStubAssembler(state_).NumberAdd(TNode<Number>{tmp6}, TNode<Number>{tmp9});
    tmp13 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp14 = Max_0(state_, TNode<Number>{tmp12}, TNode<Number>{tmp13});
    ca_.Goto(&block9, tmp14);
  }

  TNode<Number> tmp15;
  if (block8.is_used()) {
    ca_.Bind(&block8);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 157);
    tmp15 = Min_0(state_, TNode<Number>{tmp9}, TNode<Number>{tmp6});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 156);
    ca_.Goto(&block9, tmp15);
  }

  TNode<Number> phi_bb9_9;
  TNode<IntPtrT> tmp16;
  TNode<Object> tmp17;
  TNode<Oddball> tmp18;
  TNode<BoolT> tmp19;
  if (block9.is_used()) {
    ca_.Bind(&block9, &phi_bb9_9);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 161);
    tmp16 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp17 = CodeStubAssembler(state_).GetArgumentValue(TorqueStructArguments{TNode<RawPtrT>{torque_arguments.frame}, TNode<RawPtrT>{torque_arguments.base}, TNode<IntPtrT>{torque_arguments.length}}, TNode<IntPtrT>{tmp16});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 162);
    tmp18 = Undefined_0(state_);
    tmp19 = CodeStubAssembler(state_).TaggedEqual(TNode<Object>{tmp17}, TNode<HeapObject>{tmp18});
    ca_.Branch(tmp19, &block11, std::vector<Node*>{}, &block12, std::vector<Node*>{});
  }

  if (block11.is_used()) {
    ca_.Bind(&block11);
    ca_.Goto(&block13, tmp6);
  }

  TNode<Number> tmp20;
  if (block12.is_used()) {
    ca_.Bind(&block12);
    tmp20 = ToInteger_Inline_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp17});
    ca_.Goto(&block13, tmp20);
  }

  TNode<Number> phi_bb13_11;
  TNode<Number> tmp21;
  TNode<BoolT> tmp22;
  if (block13.is_used()) {
    ca_.Bind(&block13, &phi_bb13_11);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 167);
    tmp21 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp22 = NumberIsLessThan_0(state_, TNode<Number>{phi_bb13_11}, TNode<Number>{tmp21});
    ca_.Branch(tmp22, &block15, std::vector<Node*>{}, &block16, std::vector<Node*>{});
  }

  TNode<Number> tmp23;
  TNode<Number> tmp24;
  TNode<Number> tmp25;
  if (block15.is_used()) {
    ca_.Bind(&block15);
    tmp23 = CodeStubAssembler(state_).NumberAdd(TNode<Number>{tmp6}, TNode<Number>{phi_bb13_11});
    tmp24 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp25 = Max_0(state_, TNode<Number>{tmp23}, TNode<Number>{tmp24});
    ca_.Goto(&block17, tmp25);
  }

  TNode<Number> tmp26;
  if (block16.is_used()) {
    ca_.Bind(&block16);
    tmp26 = Min_0(state_, TNode<Number>{phi_bb13_11}, TNode<Number>{tmp6});
    ca_.Goto(&block17, tmp26);
  }

  TNode<Number> phi_bb17_12;
  TNode<Number> tmp27;
  TNode<Number> tmp28;
  TNode<Number> tmp29;
  TNode<Number> tmp30;
  TNode<BoolT> tmp31;
  if (block17.is_used()) {
    ca_.Bind(&block17, &phi_bb17_12);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 170);
    tmp27 = CodeStubAssembler(state_).NumberSub(TNode<Number>{phi_bb17_12}, TNode<Number>{phi_bb9_9});
    tmp28 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp29 = Max_0(state_, TNode<Number>{tmp27}, TNode<Number>{tmp28});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 172);
    tmp30 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp31 = NumberIsLessThanOrEqual_0(state_, TNode<Number>{tmp30}, TNode<Number>{phi_bb9_9});
    ca_.Branch(tmp31, &block19, std::vector<Node*>{}, &block20, std::vector<Node*>{});
  }

  if (block20.is_used()) {
    ca_.Bind(&block20);
    CodeStubAssembler(state_).FailAssert("Torque assert '0 <= k' failed", "src/builtins/array-slice.tq", 172);
  }

  TNode<BoolT> tmp32;
  if (block19.is_used()) {
    ca_.Bind(&block19);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 173);
    tmp32 = NumberIsLessThanOrEqual_0(state_, TNode<Number>{phi_bb9_9}, TNode<Number>{tmp6});
    ca_.Branch(tmp32, &block21, std::vector<Node*>{}, &block22, std::vector<Node*>{});
  }

  if (block22.is_used()) {
    ca_.Bind(&block22);
    CodeStubAssembler(state_).FailAssert("Torque assert 'k <= len' failed", "src/builtins/array-slice.tq", 173);
  }

  TNode<Number> tmp33;
  TNode<BoolT> tmp34;
  if (block21.is_used()) {
    ca_.Bind(&block21);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 174);
    tmp33 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp34 = NumberIsLessThanOrEqual_0(state_, TNode<Number>{tmp33}, TNode<Number>{phi_bb17_12});
    ca_.Branch(tmp34, &block23, std::vector<Node*>{}, &block24, std::vector<Node*>{});
  }

  if (block24.is_used()) {
    ca_.Bind(&block24);
    CodeStubAssembler(state_).FailAssert("Torque assert '0 <= final' failed", "src/builtins/array-slice.tq", 174);
  }

  TNode<BoolT> tmp35;
  if (block23.is_used()) {
    ca_.Bind(&block23);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 175);
    tmp35 = NumberIsLessThanOrEqual_0(state_, TNode<Number>{phi_bb17_12}, TNode<Number>{tmp6});
    ca_.Branch(tmp35, &block25, std::vector<Node*>{}, &block26, std::vector<Node*>{});
  }

  if (block26.is_used()) {
    ca_.Bind(&block26);
    CodeStubAssembler(state_).FailAssert("Torque assert 'final <= len' failed", "src/builtins/array-slice.tq", 175);
  }

  TNode<Number> tmp36;
  TNode<BoolT> tmp37;
  if (block25.is_used()) {
    ca_.Bind(&block25);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 176);
    tmp36 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp37 = NumberIsLessThanOrEqual_0(state_, TNode<Number>{tmp36}, TNode<Number>{tmp29});
    ca_.Branch(tmp37, &block27, std::vector<Node*>{}, &block28, std::vector<Node*>{});
  }

  if (block28.is_used()) {
    ca_.Bind(&block28);
    CodeStubAssembler(state_).FailAssert("Torque assert '0 <= count' failed", "src/builtins/array-slice.tq", 176);
  }

  TNode<BoolT> tmp38;
  if (block27.is_used()) {
    ca_.Bind(&block27);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 177);
    tmp38 = NumberIsLessThanOrEqual_0(state_, TNode<Number>{tmp29}, TNode<Number>{tmp6});
    ca_.Branch(tmp38, &block29, std::vector<Node*>{}, &block30, std::vector<Node*>{});
  }

  if (block30.is_used()) {
    ca_.Bind(&block30);
    CodeStubAssembler(state_).FailAssert("Torque assert 'count <= len' failed", "src/builtins/array-slice.tq", 177);
  }

  TNode<JSArray> tmp39;
  if (block29.is_used()) {
    ca_.Bind(&block29);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 180);
    compiler::CodeAssemblerLabel label40(&ca_);
    tmp39 = HandleFastSlice_0(state_, TNode<NativeContext>{parameter0}, TNode<Object>{tmp5}, TNode<Number>{phi_bb9_9}, TNode<Number>{tmp29}, &label40);
    ca_.Goto(&block33);
    if (label40.is_used()) {
      ca_.Bind(&label40);
      ca_.Goto(&block34);
    }
  }

  TNode<JSReceiver> tmp41;
  TNode<Number> tmp42;
  if (block34.is_used()) {
    ca_.Bind(&block34);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 185);
    tmp41 = CodeStubAssembler(state_).ArraySpeciesCreate(TNode<Context>{parameter0}, TNode<Object>{tmp5}, TNode<Number>{tmp29});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 188);
    tmp42 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 191);
    ca_.Goto(&block37, phi_bb9_9, tmp42);
  }

  if (block33.is_used()) {
    ca_.Bind(&block33);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 180);
    arguments.PopAndReturn(tmp39);
  }

  TNode<Number> phi_bb37_9;
  TNode<Number> phi_bb37_15;
  TNode<BoolT> tmp43;
  if (block37.is_used()) {
    ca_.Bind(&block37, &phi_bb37_9, &phi_bb37_15);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 191);
    tmp43 = NumberIsLessThan_0(state_, TNode<Number>{phi_bb37_9}, TNode<Number>{phi_bb17_12});
    ca_.Branch(tmp43, &block35, std::vector<Node*>{phi_bb37_9, phi_bb37_15}, &block36, std::vector<Node*>{phi_bb37_9, phi_bb37_15});
  }

  TNode<Number> phi_bb35_9;
  TNode<Number> phi_bb35_15;
  TNode<Oddball> tmp44;
  TNode<Oddball> tmp45;
  TNode<BoolT> tmp46;
  if (block35.is_used()) {
    ca_.Bind(&block35, &phi_bb35_9, &phi_bb35_15);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 196);
    tmp44 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kHasProperty, parameter0, tmp5, phi_bb35_9));
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 199);
    tmp45 = True_0(state_);
    tmp46 = CodeStubAssembler(state_).TaggedEqual(TNode<HeapObject>{tmp44}, TNode<HeapObject>{tmp45});
    ca_.Branch(tmp46, &block38, std::vector<Node*>{phi_bb35_9, phi_bb35_15, phi_bb35_9}, &block39, std::vector<Node*>{phi_bb35_9, phi_bb35_15, phi_bb35_9});
  }

  TNode<Number> phi_bb38_9;
  TNode<Number> phi_bb38_15;
  TNode<Number> phi_bb38_16;
  TNode<Object> tmp47;
  TNode<Object> tmp48;
  if (block38.is_used()) {
    ca_.Bind(&block38, &phi_bb38_9, &phi_bb38_15, &phi_bb38_16);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 201);
    tmp47 = CodeStubAssembler(state_).GetProperty(TNode<Context>{parameter0}, TNode<Object>{tmp5}, TNode<Object>{phi_bb38_16});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 204);
    tmp48 = CodeStubAssembler(state_).CallBuiltin(Builtins::kFastCreateDataProperty, parameter0, tmp41, phi_bb38_15, tmp47);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 199);
    ca_.Goto(&block39, phi_bb38_9, phi_bb38_15, phi_bb38_16);
  }

  TNode<Number> phi_bb39_9;
  TNode<Number> phi_bb39_15;
  TNode<Number> phi_bb39_16;
  TNode<Number> tmp49;
  TNode<Number> tmp50;
  TNode<Number> tmp51;
  TNode<Number> tmp52;
  if (block39.is_used()) {
    ca_.Bind(&block39, &phi_bb39_9, &phi_bb39_15, &phi_bb39_16);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 208);
    tmp49 = FromConstexpr_Number_constexpr_int31_0(state_, 1);
    tmp50 = CodeStubAssembler(state_).NumberAdd(TNode<Number>{phi_bb39_9}, TNode<Number>{tmp49});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 211);
    tmp51 = FromConstexpr_Number_constexpr_int31_0(state_, 1);
    tmp52 = CodeStubAssembler(state_).NumberAdd(TNode<Number>{phi_bb39_15}, TNode<Number>{tmp51});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 191);
    ca_.Goto(&block37, tmp50, tmp52);
  }

  TNode<Number> phi_bb36_9;
  TNode<Number> phi_bb36_15;
  TNode<String> tmp53;
  TNode<Object> tmp54;
  if (block36.is_used()) {
    ca_.Bind(&block36, &phi_bb36_9, &phi_bb36_15);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 215);
    tmp53 = kLengthString_0(state_);
    tmp54 = CodeStubAssembler(state_).CallBuiltin(Builtins::kSetProperty, parameter0, tmp41, tmp53, phi_bb36_15);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 218);
    arguments.PopAndReturn(tmp41);
  }
}

TNode<Object> UnsafeCast_JSReceiver_OR_Smi_OR_HeapNumber_OR_BigInt_OR_String_OR_Symbol_OR_True_OR_False_OR_Null_OR_Undefined_OR_TheHole_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<Object> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<BoolT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/cast.tq", 622);
    tmp0 = Is_JSReceiver_OR_Smi_OR_HeapNumber_OR_BigInt_OR_String_OR_Symbol_OR_True_OR_False_OR_Null_OR_Undefined_OR_TheHole_Object_0(state_, TNode<Context>{p_context}, TNode<Object>{p_o});
    ca_.Branch(tmp0, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    CodeStubAssembler(state_).FailAssert("Torque assert 'Is<A>(o)' failed", "src/builtins/cast.tq", 622);
  }

  TNode<Object> tmp1;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../src/builtins/cast.tq", 623);
    tmp1 = (TNode<Object>{p_o});
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 64);
    ca_.Goto(&block4);
  }

    ca_.Bind(&block4);
  return TNode<Object>{tmp1};
}

TNode<JSArray> Cast_FastJSArrayForCopy_1(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<Object> p_o, compiler::CodeAssemblerLabel* label_CastError) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<HeapObject> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/cast.tq", 157);
    compiler::CodeAssemblerLabel label1(&ca_);
    tmp0 = CodeStubAssembler(state_).TaggedToHeapObject(TNode<Object>{p_o}, &label1);
    ca_.Goto(&block3);
    if (label1.is_used()) {
      ca_.Bind(&label1);
      ca_.Goto(&block4);
    }
  }

  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.Goto(&block1);
  }

  TNode<JSArray> tmp2;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    compiler::CodeAssemblerLabel label3(&ca_);
    tmp2 = Cast_FastJSArrayForCopy_0(state_, TNode<Context>{p_context}, TNode<HeapObject>{tmp0}, &label3);
    ca_.Goto(&block5);
    if (label3.is_used()) {
      ca_.Bind(&label3);
      ca_.Goto(&block6);
    }
  }

  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.Goto(&block1);
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 96);
    ca_.Goto(&block7);
  }

  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.Goto(label_CastError);
  }

    ca_.Bind(&block7);
  return TNode<JSArray>{tmp2};
}

TNode<JSArgumentsObject> Cast_JSStrictArgumentsObject_1(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<Object> p_o, compiler::CodeAssemblerLabel* label_CastError) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<HeapObject> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/cast.tq", 157);
    compiler::CodeAssemblerLabel label1(&ca_);
    tmp0 = CodeStubAssembler(state_).TaggedToHeapObject(TNode<Object>{p_o}, &label1);
    ca_.Goto(&block3);
    if (label1.is_used()) {
      ca_.Bind(&label1);
      ca_.Goto(&block4);
    }
  }

  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.Goto(&block1);
  }

  TNode<JSArgumentsObject> tmp2;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    compiler::CodeAssemblerLabel label3(&ca_);
    tmp2 = Cast_JSStrictArgumentsObject_0(state_, TNode<Context>{p_context}, TNode<HeapObject>{tmp0}, &label3);
    ca_.Goto(&block5);
    if (label3.is_used()) {
      ca_.Bind(&label3);
      ca_.Goto(&block6);
    }
  }

  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.Goto(&block1);
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 106);
    ca_.Goto(&block7);
  }

  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.Goto(label_CastError);
  }

    ca_.Bind(&block7);
  return TNode<JSArgumentsObject>{tmp2};
}

TNode<JSArgumentsObject> Cast_JSSloppyArgumentsObject_1(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<Object> p_o, compiler::CodeAssemblerLabel* label_CastError) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<HeapObject> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/cast.tq", 157);
    compiler::CodeAssemblerLabel label1(&ca_);
    tmp0 = CodeStubAssembler(state_).TaggedToHeapObject(TNode<Object>{p_o}, &label1);
    ca_.Goto(&block3);
    if (label1.is_used()) {
      ca_.Bind(&label1);
      ca_.Goto(&block4);
    }
  }

  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.Goto(&block1);
  }

  TNode<JSArgumentsObject> tmp2;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    compiler::CodeAssemblerLabel label3(&ca_);
    tmp2 = Cast_JSSloppyArgumentsObject_0(state_, TNode<Context>{p_context}, TNode<HeapObject>{tmp0}, &label3);
    ca_.Goto(&block5);
    if (label3.is_used()) {
      ca_.Bind(&label3);
      ca_.Goto(&block6);
    }
  }

  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.Goto(&block1);
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/array-slice.tq", 109);
    ca_.Goto(&block7);
  }

  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.Goto(label_CastError);
  }

    ca_.Bind(&block7);
  return TNode<JSArgumentsObject>{tmp2};
}

}  // namespace internal
}  // namespace v8

