#include "src/builtins/builtins-array-gen.h"
#include "src/builtins/builtins-bigint-gen.h"
#include "src/builtins/builtins-collections-gen.h"
#include "src/builtins/builtins-constructor-gen.h"
#include "src/builtins/builtins-data-view-gen.h"
#include "src/builtins/builtins-iterator-gen.h"
#include "src/builtins/builtins-promise-gen.h"
#include "src/builtins/builtins-promise.h"
#include "src/builtins/builtins-proxy-gen.h"
#include "src/builtins/builtins-regexp-gen.h"
#include "src/builtins/builtins-string-gen.h"
#include "src/builtins/builtins-typed-array-gen.h"
#include "src/builtins/builtins-utils-gen.h"
#include "src/builtins/builtins-wasm-gen.h"
#include "src/builtins/builtins.h"
#include "src/codegen/code-factory.h"
#include "src/heap/factory-inl.h"
#include "src/ic/binary-op-assembler.h"
#include "src/objects/arguments.h"
#include "src/objects/bigint.h"
#include "src/objects/elements-kind.h"
#include "src/objects/free-space.h"
#include "src/objects/js-break-iterator.h"
#include "src/objects/js-collator.h"
#include "src/objects/js-date-time-format.h"
#include "src/objects/js-display-names.h"
#include "src/objects/js-generator.h"
#include "src/objects/js-list-format.h"
#include "src/objects/js-locale.h"
#include "src/objects/js-number-format.h"
#include "src/objects/js-objects.h"
#include "src/objects/js-plural-rules.h"
#include "src/objects/js-promise.h"
#include "src/objects/js-regexp-string-iterator.h"
#include "src/objects/js-relative-time-format.h"
#include "src/objects/js-segment-iterator.h"
#include "src/objects/js-segmenter.h"
#include "src/objects/js-weak-refs.h"
#include "src/objects/objects.h"
#include "src/objects/ordered-hash-table.h"
#include "src/objects/property-array.h"
#include "src/objects/property-descriptor-object.h"
#include "src/objects/source-text-module.h"
#include "src/objects/stack-frame-info.h"
#include "src/objects/synthetic-module.h"
#include "src/objects/template-objects.h"
#include "src/torque/runtime-support.h"
#include "torque-generated/src/builtins/aggregate-error-tq-csa.h"
#include "torque-generated/src/builtins/array-copywithin-tq-csa.h"
#include "torque-generated/src/builtins/array-every-tq-csa.h"
#include "torque-generated/src/builtins/array-filter-tq-csa.h"
#include "torque-generated/src/builtins/array-find-tq-csa.h"
#include "torque-generated/src/builtins/array-findindex-tq-csa.h"
#include "torque-generated/src/builtins/array-foreach-tq-csa.h"
#include "torque-generated/src/builtins/array-from-tq-csa.h"
#include "torque-generated/src/builtins/array-isarray-tq-csa.h"
#include "torque-generated/src/builtins/array-join-tq-csa.h"
#include "torque-generated/src/builtins/array-lastindexof-tq-csa.h"
#include "torque-generated/src/builtins/array-map-tq-csa.h"
#include "torque-generated/src/builtins/array-of-tq-csa.h"
#include "torque-generated/src/builtins/array-reduce-right-tq-csa.h"
#include "torque-generated/src/builtins/array-reduce-tq-csa.h"
#include "torque-generated/src/builtins/array-reverse-tq-csa.h"
#include "torque-generated/src/builtins/array-shift-tq-csa.h"
#include "torque-generated/src/builtins/array-slice-tq-csa.h"
#include "torque-generated/src/builtins/array-some-tq-csa.h"
#include "torque-generated/src/builtins/array-splice-tq-csa.h"
#include "torque-generated/src/builtins/array-unshift-tq-csa.h"
#include "torque-generated/src/builtins/array-tq-csa.h"
#include "torque-generated/src/builtins/base-tq-csa.h"
#include "torque-generated/src/builtins/bigint-tq-csa.h"
#include "torque-generated/src/builtins/boolean-tq-csa.h"
#include "torque-generated/src/builtins/builtins-string-tq-csa.h"
#include "torque-generated/src/builtins/cast-tq-csa.h"
#include "torque-generated/src/builtins/collections-tq-csa.h"
#include "torque-generated/src/builtins/conversion-tq-csa.h"
#include "torque-generated/src/builtins/convert-tq-csa.h"
#include "torque-generated/src/builtins/console-tq-csa.h"
#include "torque-generated/src/builtins/data-view-tq-csa.h"
#include "torque-generated/src/builtins/finalization-registry-tq-csa.h"
#include "torque-generated/src/builtins/frames-tq-csa.h"
#include "torque-generated/src/builtins/frame-arguments-tq-csa.h"
#include "torque-generated/src/builtins/function-tq-csa.h"
#include "torque-generated/src/builtins/growable-fixed-array-tq-csa.h"
#include "torque-generated/src/builtins/ic-callable-tq-csa.h"
#include "torque-generated/src/builtins/ic-tq-csa.h"
#include "torque-generated/src/builtins/internal-coverage-tq-csa.h"
#include "torque-generated/src/builtins/internal-tq-csa.h"
#include "torque-generated/src/builtins/iterator-tq-csa.h"
#include "torque-generated/src/builtins/math-tq-csa.h"
#include "torque-generated/src/builtins/number-tq-csa.h"
#include "torque-generated/src/builtins/object-fromentries-tq-csa.h"
#include "torque-generated/src/builtins/object-tq-csa.h"
#include "torque-generated/src/builtins/promise-abstract-operations-tq-csa.h"
#include "torque-generated/src/builtins/promise-all-tq-csa.h"
#include "torque-generated/src/builtins/promise-all-element-closure-tq-csa.h"
#include "torque-generated/src/builtins/promise-any-tq-csa.h"
#include "torque-generated/src/builtins/promise-constructor-tq-csa.h"
#include "torque-generated/src/builtins/promise-finally-tq-csa.h"
#include "torque-generated/src/builtins/promise-misc-tq-csa.h"
#include "torque-generated/src/builtins/promise-race-tq-csa.h"
#include "torque-generated/src/builtins/promise-reaction-job-tq-csa.h"
#include "torque-generated/src/builtins/promise-resolve-tq-csa.h"
#include "torque-generated/src/builtins/promise-then-tq-csa.h"
#include "torque-generated/src/builtins/promise-jobs-tq-csa.h"
#include "torque-generated/src/builtins/proxy-constructor-tq-csa.h"
#include "torque-generated/src/builtins/proxy-delete-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-get-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-get-prototype-of-tq-csa.h"
#include "torque-generated/src/builtins/proxy-has-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-is-extensible-tq-csa.h"
#include "torque-generated/src/builtins/proxy-prevent-extensions-tq-csa.h"
#include "torque-generated/src/builtins/proxy-revocable-tq-csa.h"
#include "torque-generated/src/builtins/proxy-revoke-tq-csa.h"
#include "torque-generated/src/builtins/proxy-set-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-set-prototype-of-tq-csa.h"
#include "torque-generated/src/builtins/proxy-tq-csa.h"
#include "torque-generated/src/builtins/reflect-tq-csa.h"
#include "torque-generated/src/builtins/regexp-exec-tq-csa.h"
#include "torque-generated/src/builtins/regexp-match-all-tq-csa.h"
#include "torque-generated/src/builtins/regexp-match-tq-csa.h"
#include "torque-generated/src/builtins/regexp-replace-tq-csa.h"
#include "torque-generated/src/builtins/regexp-search-tq-csa.h"
#include "torque-generated/src/builtins/regexp-source-tq-csa.h"
#include "torque-generated/src/builtins/regexp-split-tq-csa.h"
#include "torque-generated/src/builtins/regexp-test-tq-csa.h"
#include "torque-generated/src/builtins/regexp-tq-csa.h"
#include "torque-generated/src/builtins/string-endswith-tq-csa.h"
#include "torque-generated/src/builtins/string-html-tq-csa.h"
#include "torque-generated/src/builtins/string-iterator-tq-csa.h"
#include "torque-generated/src/builtins/string-pad-tq-csa.h"
#include "torque-generated/src/builtins/string-repeat-tq-csa.h"
#include "torque-generated/src/builtins/string-replaceall-tq-csa.h"
#include "torque-generated/src/builtins/string-slice-tq-csa.h"
#include "torque-generated/src/builtins/string-startswith-tq-csa.h"
#include "torque-generated/src/builtins/string-substring-tq-csa.h"
#include "torque-generated/src/builtins/string-substr-tq-csa.h"
#include "torque-generated/src/builtins/symbol-tq-csa.h"
#include "torque-generated/src/builtins/torque-internal-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-createtypedarray-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-every-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-entries-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-filter-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-find-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-findindex-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-foreach-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-from-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-keys-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-of-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-reduce-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-reduceright-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-set-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-slice-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-some-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-sort-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-subarray-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-values-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-tq-csa.h"
#include "torque-generated/src/builtins/wasm-tq-csa.h"
#include "torque-generated/src/builtins/weak-ref-tq-csa.h"
#include "torque-generated/src/ic/handler-configuration-tq-csa.h"
#include "torque-generated/src/objects/allocation-site-tq-csa.h"
#include "torque-generated/src/objects/api-callbacks-tq-csa.h"
#include "torque-generated/src/objects/arguments-tq-csa.h"
#include "torque-generated/src/objects/cell-tq-csa.h"
#include "torque-generated/src/objects/code-tq-csa.h"
#include "torque-generated/src/objects/contexts-tq-csa.h"
#include "torque-generated/src/objects/data-handler-tq-csa.h"
#include "torque-generated/src/objects/debug-objects-tq-csa.h"
#include "torque-generated/src/objects/descriptor-array-tq-csa.h"
#include "torque-generated/src/objects/embedder-data-array-tq-csa.h"
#include "torque-generated/src/objects/feedback-cell-tq-csa.h"
#include "torque-generated/src/objects/feedback-vector-tq-csa.h"
#include "torque-generated/src/objects/fixed-array-tq-csa.h"
#include "torque-generated/src/objects/foreign-tq-csa.h"
#include "torque-generated/src/objects/free-space-tq-csa.h"
#include "torque-generated/src/objects/heap-number-tq-csa.h"
#include "torque-generated/src/objects/heap-object-tq-csa.h"
#include "torque-generated/src/objects/intl-objects-tq-csa.h"
#include "torque-generated/src/objects/js-array-buffer-tq-csa.h"
#include "torque-generated/src/objects/js-array-tq-csa.h"
#include "torque-generated/src/objects/js-collection-iterator-tq-csa.h"
#include "torque-generated/src/objects/js-collection-tq-csa.h"
#include "torque-generated/src/objects/js-generator-tq-csa.h"
#include "torque-generated/src/objects/js-objects-tq-csa.h"
#include "torque-generated/src/objects/js-promise-tq-csa.h"
#include "torque-generated/src/objects/js-proxy-tq-csa.h"
#include "torque-generated/src/objects/js-regexp-string-iterator-tq-csa.h"
#include "torque-generated/src/objects/js-regexp-tq-csa.h"
#include "torque-generated/src/objects/js-weak-refs-tq-csa.h"
#include "torque-generated/src/objects/literal-objects-tq-csa.h"
#include "torque-generated/src/objects/map-tq-csa.h"
#include "torque-generated/src/objects/microtask-tq-csa.h"
#include "torque-generated/src/objects/module-tq-csa.h"
#include "torque-generated/src/objects/name-tq-csa.h"
#include "torque-generated/src/objects/oddball-tq-csa.h"
#include "torque-generated/src/objects/ordered-hash-table-tq-csa.h"
#include "torque-generated/src/objects/primitive-heap-object-tq-csa.h"
#include "torque-generated/src/objects/promise-tq-csa.h"
#include "torque-generated/src/objects/property-array-tq-csa.h"
#include "torque-generated/src/objects/property-cell-tq-csa.h"
#include "torque-generated/src/objects/property-descriptor-object-tq-csa.h"
#include "torque-generated/src/objects/prototype-info-tq-csa.h"
#include "torque-generated/src/objects/regexp-match-info-tq-csa.h"
#include "torque-generated/src/objects/scope-info-tq-csa.h"
#include "torque-generated/src/objects/script-tq-csa.h"
#include "torque-generated/src/objects/shared-function-info-tq-csa.h"
#include "torque-generated/src/objects/source-text-module-tq-csa.h"
#include "torque-generated/src/objects/stack-frame-info-tq-csa.h"
#include "torque-generated/src/objects/string-tq-csa.h"
#include "torque-generated/src/objects/struct-tq-csa.h"
#include "torque-generated/src/objects/synthetic-module-tq-csa.h"
#include "torque-generated/src/objects/template-objects-tq-csa.h"
#include "torque-generated/src/objects/template-tq-csa.h"
#include "torque-generated/src/wasm/wasm-objects-tq-csa.h"
#include "torque-generated/test/torque/test-torque-tq-csa.h"
#include "torque-generated/third_party/v8/builtins/array-sort-tq-csa.h"

namespace v8 {
namespace internal {

void ReduceToSmiOrFloat64_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<Object> p_x, compiler::CodeAssemblerLabel* label_SmiResult, compiler::TypedCodeAssemblerVariable<Smi>* label_SmiResult_parameter_0, compiler::CodeAssemblerLabel* label_Float64Result, compiler::TypedCodeAssemblerVariable<Float64T>* label_Float64Result_parameter_0) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object, Object, Object> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object, Object, Object> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object, Object> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object, Object> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 11);
    ca_.Goto(&block5, p_x);
  }

  TNode<Object> phi_bb5_2;
  TNode<BoolT> tmp0;
  if (block5.is_used()) {
    ca_.Bind(&block5, &phi_bb5_2);
    tmp0 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    ca_.Branch(tmp0, &block3, std::vector<Node*>{phi_bb5_2}, &block4, std::vector<Node*>{phi_bb5_2});
  }

  TNode<Object> phi_bb3_2;
  TNode<Smi> tmp1;
  if (block3.is_used()) {
    ca_.Bind(&block3, &phi_bb3_2);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 13);
    compiler::CodeAssemblerLabel label2(&ca_);
    tmp1 = Cast_Smi_0(state_, TNode<Object>{phi_bb3_2}, &label2);
    ca_.Goto(&block8, phi_bb3_2, phi_bb3_2, phi_bb3_2);
    if (label2.is_used()) {
      ca_.Bind(&label2);
      ca_.Goto(&block9, phi_bb3_2, phi_bb3_2, phi_bb3_2);
    }
  }

  TNode<Object> phi_bb9_2;
  TNode<Object> phi_bb9_3;
  TNode<Object> phi_bb9_4;
  TNode<HeapNumber> tmp3;
  if (block9.is_used()) {
    ca_.Bind(&block9, &phi_bb9_2, &phi_bb9_3, &phi_bb9_4);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 16);
    compiler::CodeAssemblerLabel label4(&ca_);
    tmp3 = Cast_HeapNumber_0(state_, TNode<HeapObject>{ca_.UncheckedCast<HeapObject>(phi_bb9_3)}, &label4);
    ca_.Goto(&block12, phi_bb9_2, phi_bb9_3);
    if (label4.is_used()) {
      ca_.Bind(&label4);
      ca_.Goto(&block13, phi_bb9_2, phi_bb9_3);
    }
  }

  TNode<Object> phi_bb8_2;
  TNode<Object> phi_bb8_3;
  TNode<Object> phi_bb8_4;
  if (block8.is_used()) {
    ca_.Bind(&block8, &phi_bb8_2, &phi_bb8_3, &phi_bb8_4);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 7);
    *label_SmiResult_parameter_0 = tmp1;
    ca_.Goto(label_SmiResult);
  }

  TNode<Object> phi_bb13_2;
  TNode<Object> phi_bb13_3;
  TNode<Number> tmp5;
  if (block13.is_used()) {
    ca_.Bind(&block13, &phi_bb13_2, &phi_bb13_3);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 20);
    tmp5 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kNonNumberToNumber, p_context, ca_.UncheckedCast<HeapObject>(phi_bb13_3)));
    ca_.SetSourcePosition("../../src/builtins/math.tq", 11);
    ca_.Goto(&block5, tmp5);
  }

  TNode<Object> phi_bb12_2;
  TNode<Object> phi_bb12_3;
  TNode<Float64T> tmp6;
  if (block12.is_used()) {
    ca_.Bind(&block12, &phi_bb12_2, &phi_bb12_3);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 17);
    tmp6 = Convert_float64_HeapNumber_0(state_, TNode<HeapNumber>{tmp3});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 7);
    *label_Float64Result_parameter_0 = tmp6;
    ca_.Goto(label_Float64Result);
  }

  TNode<Object> phi_bb4_2;
  if (block4.is_used()) {
    ca_.Bind(&block4, &phi_bb4_2);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 24);
    VerifiedUnreachable_0(state_);
  }
}

TF_BUILTIN(MathAbs, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block14(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block15(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block18(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block17(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

    compiler::TypedCodeAssemblerVariable<Smi> tmp1(&ca_);
    compiler::TypedCodeAssemblerVariable<Float64T> tmp3(&ca_);
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 39);
    compiler::CodeAssemblerLabel label0(&ca_);
    compiler::CodeAssemblerLabel label2(&ca_);
    ReduceToSmiOrFloat64_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter1}, &label0, &tmp1, &label2, &tmp3);
    if (label0.is_used()) {
      ca_.Bind(&label0);
      ca_.Goto(&block5);
    }
    if (label2.is_used()) {
      ca_.Bind(&label2);
      ca_.Goto(&block6);
    }
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 42);
    if (((CodeStubAssembler(state_).IsIntPtrAbsWithOverflowSupported()))) {
      ca_.Goto(&block9);
    } else {
      ca_.Goto(&block10);
    }
  }

  TNode<Float64T> tmp4;
  TNode<Number> tmp5;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 58);
    tmp4 = CodeStubAssembler(state_).Float64Abs(TNode<Float64T>{tmp3.value()});
    tmp5 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp4});
    CodeStubAssembler(state_).Return(tmp5);
  }

  TNode<Smi> tmp6;
  if (block9.is_used()) {
    ca_.Bind(&block9);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 43);
    compiler::CodeAssemblerLabel label7(&ca_);
    tmp6 = CodeStubAssembler(state_).TrySmiAbs(TNode<Smi>{tmp1.value()}, &label7);
    ca_.Goto(&block12);
    if (label7.is_used()) {
      ca_.Bind(&label7);
      ca_.Goto(&block13);
    }
  }

  if (block13.is_used()) {
    ca_.Bind(&block13);
    ca_.Goto(&block8);
  }

  if (block12.is_used()) {
    ca_.Bind(&block12);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 45);
    CodeStubAssembler(state_).Return(tmp6);
  }

  TNode<Smi> tmp8;
  TNode<BoolT> tmp9;
  if (block10.is_used()) {
    ca_.Bind(&block10);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 47);
    tmp8 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp9 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{tmp8}, TNode<Smi>{tmp1.value()});
    ca_.Branch(tmp9, &block14, std::vector<Node*>{}, &block15, std::vector<Node*>{});
  }

  if (block14.is_used()) {
    ca_.Bind(&block14);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 48);
    CodeStubAssembler(state_).Return(tmp1.value());
  }

  TNode<Smi> tmp10;
  TNode<Smi> tmp11;
  if (block15.is_used()) {
    ca_.Bind(&block15);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 50);
    tmp10 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    compiler::CodeAssemblerLabel label12(&ca_);
    tmp11 = CodeStubAssembler(state_).TrySmiSub(TNode<Smi>{tmp10}, TNode<Smi>{tmp1.value()}, &label12);
    ca_.Goto(&block17);
    if (label12.is_used()) {
      ca_.Bind(&label12);
      ca_.Goto(&block18);
    }
  }

  if (block18.is_used()) {
    ca_.Bind(&block18);
    ca_.Goto(&block8);
  }

  if (block17.is_used()) {
    ca_.Bind(&block17);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 51);
    CodeStubAssembler(state_).Return(tmp11);
  }

  TNode<Number> tmp13;
  if (block8.is_used()) {
    ca_.Bind(&block8);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 55);
    tmp13 = CodeStubAssembler(state_).NumberConstant(0.0 - kSmiMinValue);
    CodeStubAssembler(state_).Return(tmp13);
  }
}

TF_BUILTIN(MathCeil, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

    compiler::TypedCodeAssemblerVariable<Smi> tmp1(&ca_);
    compiler::TypedCodeAssemblerVariable<Float64T> tmp3(&ca_);
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 67);
    compiler::CodeAssemblerLabel label0(&ca_);
    compiler::CodeAssemblerLabel label2(&ca_);
    ReduceToSmiOrFloat64_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter1}, &label0, &tmp1, &label2, &tmp3);
    if (label0.is_used()) {
      ca_.Bind(&label0);
      ca_.Goto(&block5);
    }
    if (label2.is_used()) {
      ca_.Bind(&label2);
      ca_.Goto(&block6);
    }
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 69);
    CodeStubAssembler(state_).Return(tmp1.value());
  }

  TNode<Float64T> tmp4;
  TNode<Number> tmp5;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 71);
    tmp4 = CodeStubAssembler(state_).Float64Ceil(TNode<Float64T>{tmp3.value()});
    tmp5 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp4});
    CodeStubAssembler(state_).Return(tmp5);
  }
}

TF_BUILTIN(MathFloor, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

    compiler::TypedCodeAssemblerVariable<Smi> tmp1(&ca_);
    compiler::TypedCodeAssemblerVariable<Float64T> tmp3(&ca_);
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 80);
    compiler::CodeAssemblerLabel label0(&ca_);
    compiler::CodeAssemblerLabel label2(&ca_);
    ReduceToSmiOrFloat64_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter1}, &label0, &tmp1, &label2, &tmp3);
    if (label0.is_used()) {
      ca_.Bind(&label0);
      ca_.Goto(&block5);
    }
    if (label2.is_used()) {
      ca_.Bind(&label2);
      ca_.Goto(&block6);
    }
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 82);
    CodeStubAssembler(state_).Return(tmp1.value());
  }

  TNode<Float64T> tmp4;
  TNode<Number> tmp5;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 84);
    tmp4 = CodeStubAssembler(state_).Float64Floor(TNode<Float64T>{tmp3.value()});
    tmp5 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp4});
    CodeStubAssembler(state_).Return(tmp5);
  }
}

TF_BUILTIN(MathRound, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

    compiler::TypedCodeAssemblerVariable<Smi> tmp1(&ca_);
    compiler::TypedCodeAssemblerVariable<Float64T> tmp3(&ca_);
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 93);
    compiler::CodeAssemblerLabel label0(&ca_);
    compiler::CodeAssemblerLabel label2(&ca_);
    ReduceToSmiOrFloat64_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter1}, &label0, &tmp1, &label2, &tmp3);
    if (label0.is_used()) {
      ca_.Bind(&label0);
      ca_.Goto(&block5);
    }
    if (label2.is_used()) {
      ca_.Bind(&label2);
      ca_.Goto(&block6);
    }
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 95);
    CodeStubAssembler(state_).Return(tmp1.value());
  }

  TNode<Float64T> tmp4;
  TNode<Number> tmp5;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 97);
    tmp4 = CodeStubAssembler(state_).Float64Round(TNode<Float64T>{tmp3.value()});
    tmp5 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp4});
    CodeStubAssembler(state_).Return(tmp5);
  }
}

TF_BUILTIN(MathTrunc, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

    compiler::TypedCodeAssemblerVariable<Smi> tmp1(&ca_);
    compiler::TypedCodeAssemblerVariable<Float64T> tmp3(&ca_);
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 106);
    compiler::CodeAssemblerLabel label0(&ca_);
    compiler::CodeAssemblerLabel label2(&ca_);
    ReduceToSmiOrFloat64_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter1}, &label0, &tmp1, &label2, &tmp3);
    if (label0.is_used()) {
      ca_.Bind(&label0);
      ca_.Goto(&block5);
    }
    if (label2.is_used()) {
      ca_.Bind(&label2);
      ca_.Goto(&block6);
    }
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 108);
    CodeStubAssembler(state_).Return(tmp1.value());
  }

  TNode<Float64T> tmp4;
  TNode<Number> tmp5;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 110);
    tmp4 = CodeStubAssembler(state_).Float64Trunc(TNode<Float64T>{tmp3.value()});
    tmp5 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp4});
    CodeStubAssembler(state_).Return(tmp5);
  }
}

TNode<Number> MathPowImpl_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<Object> p_base, TNode<Object> p_exponent) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Float64T> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 121);
    tmp0 = CodeStubAssembler(state_).TruncateTaggedToFloat64(TNode<Context>{p_context}, TNode<Object>{p_base});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 122);
    tmp1 = CodeStubAssembler(state_).TruncateTaggedToFloat64(TNode<Context>{p_context}, TNode<Object>{p_exponent});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 123);
    tmp2 = CodeStubAssembler(state_).Float64Pow(TNode<Float64T>{tmp0}, TNode<Float64T>{tmp1});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 124);
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 118);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<Number>{tmp3};
}

TF_BUILTIN(MathPow, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kBase));
  USE(parameter1);
  TNode<Object> parameter2 = UncheckedCast<Object>(Parameter(Descriptor::kExponent));
  USE(parameter2);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 130);
    tmp0 = MathPowImpl_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter1}, TNode<Object>{parameter2});
    CodeStubAssembler(state_).Return(tmp0);
  }
}

TF_BUILTIN(MathMax, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  Node* argc = Parameter(Descriptor::kJSActualArgumentsCount);
  TNode<IntPtrT> arguments_length(ChangeInt32ToIntPtr(UncheckedCast<Int32T>(argc)));
  TNode<RawPtrT> arguments_frame = UncheckedCast<RawPtrT>(LoadFramePointer());
  TorqueStructArguments torque_arguments(GetFrameArguments(arguments_frame, arguments_length));
  CodeStubArguments arguments(this, torque_arguments);
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Float64T, IntPtrT> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Float64T, IntPtrT> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Float64T, IntPtrT> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Float64T> tmp0;
  TNode<IntPtrT> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 137);
    tmp0 = FromConstexpr_float64_constexpr_float64_0(state_, -V8_INFINITY);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 139);
    tmp1 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    ca_.Goto(&block3, tmp0, tmp1);
  }

  TNode<Float64T> phi_bb3_4;
  TNode<IntPtrT> phi_bb3_6;
  TNode<BoolT> tmp2;
  if (block3.is_used()) {
    ca_.Bind(&block3, &phi_bb3_4, &phi_bb3_6);
    tmp2 = CodeStubAssembler(state_).IntPtrLessThan(TNode<IntPtrT>{phi_bb3_6}, TNode<IntPtrT>{torque_arguments.length});
    ca_.Branch(tmp2, &block1, std::vector<Node*>{phi_bb3_4, phi_bb3_6}, &block2, std::vector<Node*>{phi_bb3_4, phi_bb3_6});
  }

  TNode<Float64T> phi_bb1_4;
  TNode<IntPtrT> phi_bb1_6;
  TNode<Object> tmp3;
  TNode<Float64T> tmp4;
  TNode<Float64T> tmp5;
  TNode<IntPtrT> tmp6;
  TNode<IntPtrT> tmp7;
  if (block1.is_used()) {
    ca_.Bind(&block1, &phi_bb1_4, &phi_bb1_6);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 140);
    tmp3 = CodeStubAssembler(state_).GetArgumentValue(TorqueStructArguments{TNode<RawPtrT>{torque_arguments.frame}, TNode<RawPtrT>{torque_arguments.base}, TNode<IntPtrT>{torque_arguments.length}}, TNode<IntPtrT>{phi_bb1_6});
    tmp4 = CodeStubAssembler(state_).TruncateTaggedToFloat64(TNode<Context>{parameter0}, TNode<Object>{tmp3});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 141);
    tmp5 = CodeStubAssembler(state_).Float64Max(TNode<Float64T>{phi_bb1_4}, TNode<Float64T>{tmp4});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 139);
    tmp6 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp7 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{phi_bb1_6}, TNode<IntPtrT>{tmp6});
    ca_.Goto(&block3, tmp5, tmp7);
  }

  TNode<Float64T> phi_bb2_4;
  TNode<IntPtrT> phi_bb2_6;
  TNode<Number> tmp8;
  if (block2.is_used()) {
    ca_.Bind(&block2, &phi_bb2_4, &phi_bb2_6);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 143);
    tmp8 = Convert_Number_float64_0(state_, TNode<Float64T>{phi_bb2_4});
    arguments.PopAndReturn(tmp8);
  }
}

TF_BUILTIN(MathMin, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  Node* argc = Parameter(Descriptor::kJSActualArgumentsCount);
  TNode<IntPtrT> arguments_length(ChangeInt32ToIntPtr(UncheckedCast<Int32T>(argc)));
  TNode<RawPtrT> arguments_frame = UncheckedCast<RawPtrT>(LoadFramePointer());
  TorqueStructArguments torque_arguments(GetFrameArguments(arguments_frame, arguments_length));
  CodeStubArguments arguments(this, torque_arguments);
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Float64T, IntPtrT> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Float64T, IntPtrT> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Float64T, IntPtrT> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Float64T> tmp0;
  TNode<IntPtrT> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 150);
    tmp0 = FromConstexpr_float64_constexpr_float64_0(state_, V8_INFINITY);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 152);
    tmp1 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    ca_.Goto(&block3, tmp0, tmp1);
  }

  TNode<Float64T> phi_bb3_4;
  TNode<IntPtrT> phi_bb3_6;
  TNode<BoolT> tmp2;
  if (block3.is_used()) {
    ca_.Bind(&block3, &phi_bb3_4, &phi_bb3_6);
    tmp2 = CodeStubAssembler(state_).IntPtrLessThan(TNode<IntPtrT>{phi_bb3_6}, TNode<IntPtrT>{torque_arguments.length});
    ca_.Branch(tmp2, &block1, std::vector<Node*>{phi_bb3_4, phi_bb3_6}, &block2, std::vector<Node*>{phi_bb3_4, phi_bb3_6});
  }

  TNode<Float64T> phi_bb1_4;
  TNode<IntPtrT> phi_bb1_6;
  TNode<Object> tmp3;
  TNode<Float64T> tmp4;
  TNode<Float64T> tmp5;
  TNode<IntPtrT> tmp6;
  TNode<IntPtrT> tmp7;
  if (block1.is_used()) {
    ca_.Bind(&block1, &phi_bb1_4, &phi_bb1_6);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 153);
    tmp3 = CodeStubAssembler(state_).GetArgumentValue(TorqueStructArguments{TNode<RawPtrT>{torque_arguments.frame}, TNode<RawPtrT>{torque_arguments.base}, TNode<IntPtrT>{torque_arguments.length}}, TNode<IntPtrT>{phi_bb1_6});
    tmp4 = CodeStubAssembler(state_).TruncateTaggedToFloat64(TNode<Context>{parameter0}, TNode<Object>{tmp3});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 154);
    tmp5 = CodeStubAssembler(state_).Float64Min(TNode<Float64T>{phi_bb1_4}, TNode<Float64T>{tmp4});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 152);
    tmp6 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp7 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{phi_bb1_6}, TNode<IntPtrT>{tmp6});
    ca_.Goto(&block3, tmp5, tmp7);
  }

  TNode<Float64T> phi_bb2_4;
  TNode<IntPtrT> phi_bb2_6;
  TNode<Number> tmp8;
  if (block2.is_used()) {
    ca_.Bind(&block2, &phi_bb2_4, &phi_bb2_6);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 156);
    tmp8 = Convert_Number_float64_0(state_, TNode<Float64T>{phi_bb2_4});
    arguments.PopAndReturn(tmp8);
  }
}

TF_BUILTIN(MathAcos, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 164);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 165);
    tmp2 = CodeStubAssembler(state_).Float64Acos(TNode<Float64T>{tmp1});
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathAcosh, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 173);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 174);
    tmp2 = CodeStubAssembler(state_).Float64Acosh(TNode<Float64T>{tmp1});
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathAsin, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 182);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 183);
    tmp2 = CodeStubAssembler(state_).Float64Asin(TNode<Float64T>{tmp1});
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathAsinh, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 191);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 192);
    tmp2 = CodeStubAssembler(state_).Float64Asinh(TNode<Float64T>{tmp1});
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathAtan, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 200);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 201);
    tmp2 = CodeStubAssembler(state_).Float64Atan(TNode<Float64T>{tmp1});
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathAtan2, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kY));
  USE(parameter1);
  TNode<Object> parameter2 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter2);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Number> tmp2;
  TNode<Float64T> tmp3;
  TNode<Float64T> tmp4;
  TNode<Number> tmp5;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 209);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 210);
    tmp2 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter2});
    tmp3 = Convert_float64_Number_0(state_, TNode<Number>{tmp2});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 211);
    tmp4 = CodeStubAssembler(state_).Float64Atan2(TNode<Float64T>{tmp1}, TNode<Float64T>{tmp3});
    tmp5 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp4});
    CodeStubAssembler(state_).Return(tmp5);
  }
}

TF_BUILTIN(MathAtanh, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 219);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 220);
    tmp2 = CodeStubAssembler(state_).Float64Atanh(TNode<Float64T>{tmp1});
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathCbrt, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 228);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 229);
    tmp2 = CodeStubAssembler(state_).Float64Cbrt(TNode<Float64T>{tmp1});
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathClz32, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Int32T> tmp1;
  TNode<Int32T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 237);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_int32_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 238);
    tmp2 = CodeStubAssembler(state_).Word32Clz(TNode<Int32T>{tmp1});
    tmp3 = Convert_Number_int32_0(state_, TNode<Int32T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathCos, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 246);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 247);
    tmp2 = CodeStubAssembler(state_).Float64Cos(TNode<Float64T>{tmp1});
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathCosh, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 255);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 256);
    tmp2 = CodeStubAssembler(state_).Float64Cosh(TNode<Float64T>{tmp1});
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathExp, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 264);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 265);
    tmp2 = CodeStubAssembler(state_).Float64Exp(TNode<Float64T>{tmp1});
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathExpm1, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 273);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 274);
    tmp2 = CodeStubAssembler(state_).Float64Expm1(TNode<Float64T>{tmp1});
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathFround, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float32T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 280);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float32_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 281);
    tmp2 = Convert_float64_float32_0(state_, TNode<Float32T>{tmp1});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 282);
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathImul, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  TNode<Object> parameter2 = UncheckedCast<Object>(Parameter(Descriptor::kY));
  USE(parameter2);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Int32T> tmp1;
  TNode<Number> tmp2;
  TNode<Int32T> tmp3;
  TNode<Int32T> tmp4;
  TNode<Number> tmp5;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 288);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_int32_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 289);
    tmp2 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter2});
    tmp3 = Convert_int32_Number_0(state_, TNode<Number>{tmp2});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 290);
    tmp4 = CodeStubAssembler(state_).Int32Mul(TNode<Int32T>{tmp1}, TNode<Int32T>{tmp3});
    tmp5 = Convert_Number_int32_0(state_, TNode<Int32T>{tmp4});
    CodeStubAssembler(state_).Return(tmp5);
  }
}

TF_BUILTIN(MathLog, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 298);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 299);
    tmp2 = CodeStubAssembler(state_).Float64Log(TNode<Float64T>{tmp1});
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathLog1p, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 307);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 308);
    tmp2 = CodeStubAssembler(state_).Float64Log1p(TNode<Float64T>{tmp1});
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathLog10, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 316);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 317);
    tmp2 = CodeStubAssembler(state_).Float64Log10(TNode<Float64T>{tmp1});
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathLog2, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 325);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 326);
    tmp2 = CodeStubAssembler(state_).Float64Log2(TNode<Float64T>{tmp1});
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathSin, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 334);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 335);
    tmp2 = CodeStubAssembler(state_).Float64Sin(TNode<Float64T>{tmp1});
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathSign, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<BoolT> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 341);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 342);
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 344);
    tmp2 = FromConstexpr_float64_constexpr_int31_0(state_, 0);
    tmp3 = CodeStubAssembler(state_).Float64LessThan(TNode<Float64T>{tmp1}, TNode<Float64T>{tmp2});
    ca_.Branch(tmp3, &block1, std::vector<Node*>{}, &block2, std::vector<Node*>{});
  }

  TNode<Number> tmp4;
  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 345);
    tmp4 = FromConstexpr_Number_constexpr_int31_0(state_, -1);
    CodeStubAssembler(state_).Return(tmp4);
  }

  TNode<Float64T> tmp5;
  TNode<BoolT> tmp6;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 346);
    tmp5 = FromConstexpr_float64_constexpr_int31_0(state_, 0);
    tmp6 = CodeStubAssembler(state_).Float64GreaterThan(TNode<Float64T>{tmp1}, TNode<Float64T>{tmp5});
    ca_.Branch(tmp6, &block4, std::vector<Node*>{}, &block5, std::vector<Node*>{});
  }

  TNode<Number> tmp7;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 347);
    tmp7 = FromConstexpr_Number_constexpr_int31_0(state_, 1);
    CodeStubAssembler(state_).Return(tmp7);
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 349);
    CodeStubAssembler(state_).Return(tmp0);
  }
}

TF_BUILTIN(MathSinh, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 358);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 359);
    tmp2 = CodeStubAssembler(state_).Float64Sinh(TNode<Float64T>{tmp1});
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathSqrt, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 367);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 368);
    tmp2 = CodeStubAssembler(state_).Float64Sqrt(TNode<Float64T>{tmp1});
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathTan, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 376);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 377);
    tmp2 = CodeStubAssembler(state_).Float64Tan(TNode<Float64T>{tmp1});
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathTanh, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kX));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Number> tmp0;
  TNode<Float64T> tmp1;
  TNode<Float64T> tmp2;
  TNode<Number> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 385);
    tmp0 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter1});
    tmp1 = Convert_float64_Number_0(state_, TNode<Number>{tmp0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 386);
    tmp2 = CodeStubAssembler(state_).Float64Tanh(TNode<Float64T>{tmp1});
    tmp3 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp2});
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(MathHypot, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  Node* argc = Parameter(Descriptor::kJSActualArgumentsCount);
  TNode<IntPtrT> arguments_length(ChangeInt32ToIntPtr(UncheckedCast<Int32T>(argc)));
  TNode<RawPtrT> arguments_frame = UncheckedCast<RawPtrT>(LoadFramePointer());
  TorqueStructArguments torque_arguments(GetFrameArguments(arguments_frame, arguments_length));
  CodeStubArguments arguments(this, torque_arguments);
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = arguments.GetReceiver();
USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T, IntPtrT> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T, IntPtrT> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T, IntPtrT> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T, IntPtrT> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T, IntPtrT, IntPtrT, IntPtrT, IntPtrT, IntPtrT> block14(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T, IntPtrT, IntPtrT, IntPtrT, IntPtrT, IntPtrT> block15(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T, IntPtrT> block17(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T, IntPtrT> block18(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T, IntPtrT> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T, IntPtrT> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T> block19(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T> block20(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T> block22(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T> block23(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T> block25(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T> block26(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T> block28(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T> block27(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T, Float64T, Float64T, IntPtrT> block31(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T, Float64T, Float64T, IntPtrT> block29(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T, Float64T, Float64T, IntPtrT, IntPtrT, IntPtrT, IntPtrT, IntPtrT> block37(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T, Float64T, Float64T, IntPtrT, IntPtrT, IntPtrT, IntPtrT, IntPtrT> block38(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T, Float64T, Float64T, IntPtrT, IntPtrT, IntPtrT> block42(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T, Float64T, Float64T, IntPtrT, IntPtrT, IntPtrT> block41(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT, Float64T, Float64T, Float64T, IntPtrT> block30(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<BoolT> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 394);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp1 = CodeStubAssembler(state_).WordEqual(TNode<IntPtrT>{torque_arguments.length}, TNode<IntPtrT>{tmp0});
    ca_.Branch(tmp1, &block1, std::vector<Node*>{}, &block2, std::vector<Node*>{});
  }

  TNode<Number> tmp2;
  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 395);
    tmp2 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    arguments.PopAndReturn(tmp2);
  }

  TNode<FixedDoubleArray> tmp3;
  TNode<BoolT> tmp4;
  TNode<Float64T> tmp5;
  TNode<IntPtrT> tmp6;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 397);
    tmp3 = CodeStubAssembler(state_).AllocateZeroedFixedDoubleArray(TNode<IntPtrT>{torque_arguments.length});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 398);
    tmp4 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 399);
    tmp5 = FromConstexpr_float64_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 400);
    tmp6 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    ca_.Goto(&block5, tmp4, tmp5, tmp6);
  }

  TNode<BoolT> phi_bb5_7;
  TNode<Float64T> phi_bb5_8;
  TNode<IntPtrT> phi_bb5_9;
  TNode<BoolT> tmp7;
  if (block5.is_used()) {
    ca_.Bind(&block5, &phi_bb5_7, &phi_bb5_8, &phi_bb5_9);
    tmp7 = CodeStubAssembler(state_).IntPtrLessThan(TNode<IntPtrT>{phi_bb5_9}, TNode<IntPtrT>{torque_arguments.length});
    ca_.Branch(tmp7, &block3, std::vector<Node*>{phi_bb5_7, phi_bb5_8, phi_bb5_9}, &block4, std::vector<Node*>{phi_bb5_7, phi_bb5_8, phi_bb5_9});
  }

  TNode<BoolT> phi_bb3_7;
  TNode<Float64T> phi_bb3_8;
  TNode<IntPtrT> phi_bb3_9;
  TNode<Object> tmp8;
  TNode<Number> tmp9;
  TNode<Float64T> tmp10;
  TNode<BoolT> tmp11;
  if (block3.is_used()) {
    ca_.Bind(&block3, &phi_bb3_7, &phi_bb3_8, &phi_bb3_9);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 401);
    tmp8 = CodeStubAssembler(state_).GetArgumentValue(TorqueStructArguments{TNode<RawPtrT>{torque_arguments.frame}, TNode<RawPtrT>{torque_arguments.base}, TNode<IntPtrT>{torque_arguments.length}}, TNode<IntPtrT>{phi_bb3_9});
    tmp9 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{tmp8});
    tmp10 = Convert_float64_Number_0(state_, TNode<Number>{tmp9});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 402);
    tmp11 = Float64IsNaN_0(state_, TNode<Float64T>{tmp10});
    ca_.Branch(tmp11, &block7, std::vector<Node*>{phi_bb3_7, phi_bb3_8, phi_bb3_9}, &block8, std::vector<Node*>{phi_bb3_7, phi_bb3_8, phi_bb3_9});
  }

  TNode<BoolT> phi_bb7_7;
  TNode<Float64T> phi_bb7_8;
  TNode<IntPtrT> phi_bb7_9;
  TNode<BoolT> tmp12;
  if (block7.is_used()) {
    ca_.Bind(&block7, &phi_bb7_7, &phi_bb7_8, &phi_bb7_9);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 403);
    tmp12 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 402);
    ca_.Goto(&block9, tmp12, phi_bb7_8, phi_bb7_9);
  }

  TNode<BoolT> phi_bb8_7;
  TNode<Float64T> phi_bb8_8;
  TNode<IntPtrT> phi_bb8_9;
  TNode<Float64T> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<IntPtrT> tmp15;
  TNode<IntPtrT> tmp16;
  TNode<Smi> tmp17;
  TNode<IntPtrT> tmp18;
  TNode<UintPtrT> tmp19;
  TNode<UintPtrT> tmp20;
  TNode<BoolT> tmp21;
  if (block8.is_used()) {
    ca_.Bind(&block8, &phi_bb8_7, &phi_bb8_8, &phi_bb8_9);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 405);
    tmp13 = CodeStubAssembler(state_).Float64Abs(TNode<Float64T>{tmp10});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 406);
    tmp14 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp15 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp16 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 22);
    tmp17 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp3, tmp16});
    tmp18 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp17});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp19 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{phi_bb8_9});
    tmp20 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp18});
    tmp21 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp19}, TNode<UintPtrT>{tmp20});
    ca_.Branch(tmp21, &block14, std::vector<Node*>{phi_bb8_7, phi_bb8_8, phi_bb8_9, phi_bb8_9, phi_bb8_9, phi_bb8_9, phi_bb8_9}, &block15, std::vector<Node*>{phi_bb8_7, phi_bb8_8, phi_bb8_9, phi_bb8_9, phi_bb8_9, phi_bb8_9, phi_bb8_9});
  }

  TNode<BoolT> phi_bb14_7;
  TNode<Float64T> phi_bb14_8;
  TNode<IntPtrT> phi_bb14_9;
  TNode<IntPtrT> phi_bb14_16;
  TNode<IntPtrT> phi_bb14_17;
  TNode<IntPtrT> phi_bb14_21;
  TNode<IntPtrT> phi_bb14_22;
  TNode<IntPtrT> tmp22;
  TNode<IntPtrT> tmp23;
  TNode<IntPtrT> tmp24;
  TNode<HeapObject> tmp25;
  TNode<IntPtrT> tmp26;
  TNode<BoolT> tmp27;
  TNode<Float64T> tmp28;
  TNode<BoolT> tmp29;
  if (block14.is_used()) {
    ca_.Bind(&block14, &phi_bb14_7, &phi_bb14_8, &phi_bb14_9, &phi_bb14_16, &phi_bb14_17, &phi_bb14_21, &phi_bb14_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp22 = FromConstexpr_intptr_constexpr_int31_0(state_, kDoubleSize);
    tmp23 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{phi_bb14_22}, TNode<IntPtrT>{tmp22});
    tmp24 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp14}, TNode<IntPtrT>{tmp23});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp25, tmp26) = NewReference_float64_or_hole_0(state_, TNode<HeapObject>{tmp3}, TNode<IntPtrT>{tmp24}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/math.tq", 406);
    std::tie(tmp27, tmp28) = Convert_float64_or_hole_float64_0(state_, TNode<Float64T>{tmp13}).Flatten();
    StoreFloat64OrHole_0(state_, TorqueStructReference_float64_or_hole_0{TNode<HeapObject>{tmp25}, TNode<IntPtrT>{tmp26}, TorqueStructUnsafe_0{}}, TorqueStructfloat64_or_hole_0{TNode<BoolT>{tmp27}, TNode<Float64T>{tmp28}});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 407);
    tmp29 = CodeStubAssembler(state_).Float64GreaterThan(TNode<Float64T>{tmp13}, TNode<Float64T>{phi_bb14_8});
    ca_.Branch(tmp29, &block17, std::vector<Node*>{phi_bb14_7, phi_bb14_8, phi_bb14_9}, &block18, std::vector<Node*>{phi_bb14_7, phi_bb14_8, phi_bb14_9});
  }

  TNode<BoolT> phi_bb15_7;
  TNode<Float64T> phi_bb15_8;
  TNode<IntPtrT> phi_bb15_9;
  TNode<IntPtrT> phi_bb15_16;
  TNode<IntPtrT> phi_bb15_17;
  TNode<IntPtrT> phi_bb15_21;
  TNode<IntPtrT> phi_bb15_22;
  if (block15.is_used()) {
    ca_.Bind(&block15, &phi_bb15_7, &phi_bb15_8, &phi_bb15_9, &phi_bb15_16, &phi_bb15_17, &phi_bb15_21, &phi_bb15_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<BoolT> phi_bb17_7;
  TNode<Float64T> phi_bb17_8;
  TNode<IntPtrT> phi_bb17_9;
  if (block17.is_used()) {
    ca_.Bind(&block17, &phi_bb17_7, &phi_bb17_8, &phi_bb17_9);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 407);
    ca_.Goto(&block18, phi_bb17_7, tmp13, phi_bb17_9);
  }

  TNode<BoolT> phi_bb18_7;
  TNode<Float64T> phi_bb18_8;
  TNode<IntPtrT> phi_bb18_9;
  if (block18.is_used()) {
    ca_.Bind(&block18, &phi_bb18_7, &phi_bb18_8, &phi_bb18_9);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 402);
    ca_.Goto(&block9, phi_bb18_7, phi_bb18_8, phi_bb18_9);
  }

  TNode<BoolT> phi_bb9_7;
  TNode<Float64T> phi_bb9_8;
  TNode<IntPtrT> phi_bb9_9;
  TNode<IntPtrT> tmp30;
  TNode<IntPtrT> tmp31;
  if (block9.is_used()) {
    ca_.Bind(&block9, &phi_bb9_7, &phi_bb9_8, &phi_bb9_9);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 400);
    tmp30 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp31 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{phi_bb9_9}, TNode<IntPtrT>{tmp30});
    ca_.Goto(&block5, phi_bb9_7, phi_bb9_8, tmp31);
  }

  TNode<BoolT> phi_bb4_7;
  TNode<Float64T> phi_bb4_8;
  TNode<IntPtrT> phi_bb4_9;
  TNode<Float64T> tmp32;
  TNode<BoolT> tmp33;
  if (block4.is_used()) {
    ca_.Bind(&block4, &phi_bb4_7, &phi_bb4_8, &phi_bb4_9);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 412);
    tmp32 = FromConstexpr_float64_constexpr_float64_0(state_, V8_INFINITY);
    tmp33 = CodeStubAssembler(state_).Float64Equal(TNode<Float64T>{phi_bb4_8}, TNode<Float64T>{tmp32});
    ca_.Branch(tmp33, &block19, std::vector<Node*>{phi_bb4_7, phi_bb4_8}, &block20, std::vector<Node*>{phi_bb4_7, phi_bb4_8});
  }

  TNode<BoolT> phi_bb19_7;
  TNode<Float64T> phi_bb19_8;
  TNode<Number> tmp34;
  if (block19.is_used()) {
    ca_.Bind(&block19, &phi_bb19_7, &phi_bb19_8);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 413);
    tmp34 = FromConstexpr_Number_constexpr_float64_0(state_, V8_INFINITY);
    arguments.PopAndReturn(tmp34);
  }

  TNode<BoolT> phi_bb20_7;
  TNode<Float64T> phi_bb20_8;
  if (block20.is_used()) {
    ca_.Bind(&block20, &phi_bb20_7, &phi_bb20_8);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 414);
    ca_.Branch(phi_bb20_7, &block22, std::vector<Node*>{phi_bb20_7, phi_bb20_8}, &block23, std::vector<Node*>{phi_bb20_7, phi_bb20_8});
  }

  TNode<BoolT> phi_bb22_7;
  TNode<Float64T> phi_bb22_8;
  TNode<HeapNumber> tmp35;
  if (block22.is_used()) {
    ca_.Bind(&block22, &phi_bb22_7, &phi_bb22_8);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 415);
    tmp35 = kNaN_0(state_);
    arguments.PopAndReturn(tmp35);
  }

  TNode<BoolT> phi_bb23_7;
  TNode<Float64T> phi_bb23_8;
  TNode<Float64T> tmp36;
  TNode<BoolT> tmp37;
  if (block23.is_used()) {
    ca_.Bind(&block23, &phi_bb23_7, &phi_bb23_8);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 416);
    tmp36 = FromConstexpr_float64_constexpr_int31_0(state_, 0);
    tmp37 = CodeStubAssembler(state_).Float64Equal(TNode<Float64T>{phi_bb23_8}, TNode<Float64T>{tmp36});
    ca_.Branch(tmp37, &block25, std::vector<Node*>{phi_bb23_7, phi_bb23_8}, &block26, std::vector<Node*>{phi_bb23_7, phi_bb23_8});
  }

  TNode<BoolT> phi_bb25_7;
  TNode<Float64T> phi_bb25_8;
  TNode<Number> tmp38;
  if (block25.is_used()) {
    ca_.Bind(&block25, &phi_bb25_7, &phi_bb25_8);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 417);
    tmp38 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    arguments.PopAndReturn(tmp38);
  }

  TNode<BoolT> phi_bb26_7;
  TNode<Float64T> phi_bb26_8;
  TNode<Float64T> tmp39;
  TNode<BoolT> tmp40;
  if (block26.is_used()) {
    ca_.Bind(&block26, &phi_bb26_7, &phi_bb26_8);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 419);
    tmp39 = FromConstexpr_float64_constexpr_int31_0(state_, 0);
    tmp40 = CodeStubAssembler(state_).Float64GreaterThan(TNode<Float64T>{phi_bb26_8}, TNode<Float64T>{tmp39});
    ca_.Branch(tmp40, &block27, std::vector<Node*>{phi_bb26_7, phi_bb26_8}, &block28, std::vector<Node*>{phi_bb26_7, phi_bb26_8});
  }

  TNode<BoolT> phi_bb28_7;
  TNode<Float64T> phi_bb28_8;
  if (block28.is_used()) {
    ca_.Bind(&block28, &phi_bb28_7, &phi_bb28_8);
    CodeStubAssembler(state_).FailAssert("Torque assert 'max > 0' failed", "src/builtins/math.tq", 419);
  }

  TNode<BoolT> phi_bb27_7;
  TNode<Float64T> phi_bb27_8;
  TNode<Float64T> tmp41;
  TNode<Float64T> tmp42;
  TNode<IntPtrT> tmp43;
  if (block27.is_used()) {
    ca_.Bind(&block27, &phi_bb27_7, &phi_bb27_8);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 423);
    tmp41 = FromConstexpr_float64_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 424);
    tmp42 = FromConstexpr_float64_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 425);
    tmp43 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    ca_.Goto(&block31, phi_bb27_7, phi_bb27_8, tmp41, tmp42, tmp43);
  }

  TNode<BoolT> phi_bb31_7;
  TNode<Float64T> phi_bb31_8;
  TNode<Float64T> phi_bb31_9;
  TNode<Float64T> phi_bb31_10;
  TNode<IntPtrT> phi_bb31_11;
  TNode<BoolT> tmp44;
  if (block31.is_used()) {
    ca_.Bind(&block31, &phi_bb31_7, &phi_bb31_8, &phi_bb31_9, &phi_bb31_10, &phi_bb31_11);
    tmp44 = CodeStubAssembler(state_).IntPtrLessThan(TNode<IntPtrT>{phi_bb31_11}, TNode<IntPtrT>{torque_arguments.length});
    ca_.Branch(tmp44, &block29, std::vector<Node*>{phi_bb31_7, phi_bb31_8, phi_bb31_9, phi_bb31_10, phi_bb31_11}, &block30, std::vector<Node*>{phi_bb31_7, phi_bb31_8, phi_bb31_9, phi_bb31_10, phi_bb31_11});
  }

  TNode<BoolT> phi_bb29_7;
  TNode<Float64T> phi_bb29_8;
  TNode<Float64T> phi_bb29_9;
  TNode<Float64T> phi_bb29_10;
  TNode<IntPtrT> phi_bb29_11;
  TNode<IntPtrT> tmp45;
  TNode<IntPtrT> tmp46;
  TNode<IntPtrT> tmp47;
  TNode<Smi> tmp48;
  TNode<IntPtrT> tmp49;
  TNode<UintPtrT> tmp50;
  TNode<UintPtrT> tmp51;
  TNode<BoolT> tmp52;
  if (block29.is_used()) {
    ca_.Bind(&block29, &phi_bb29_7, &phi_bb29_8, &phi_bb29_9, &phi_bb29_10, &phi_bb29_11);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 426);
    tmp45 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp46 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp47 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 22);
    tmp48 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp3, tmp47});
    tmp49 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp48});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp50 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{phi_bb29_11});
    tmp51 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp49});
    tmp52 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp50}, TNode<UintPtrT>{tmp51});
    ca_.Branch(tmp52, &block37, std::vector<Node*>{phi_bb29_7, phi_bb29_8, phi_bb29_9, phi_bb29_10, phi_bb29_11, phi_bb29_11, phi_bb29_11, phi_bb29_11, phi_bb29_11}, &block38, std::vector<Node*>{phi_bb29_7, phi_bb29_8, phi_bb29_9, phi_bb29_10, phi_bb29_11, phi_bb29_11, phi_bb29_11, phi_bb29_11, phi_bb29_11});
  }

  TNode<BoolT> phi_bb37_7;
  TNode<Float64T> phi_bb37_8;
  TNode<Float64T> phi_bb37_9;
  TNode<Float64T> phi_bb37_10;
  TNode<IntPtrT> phi_bb37_11;
  TNode<IntPtrT> phi_bb37_16;
  TNode<IntPtrT> phi_bb37_17;
  TNode<IntPtrT> phi_bb37_21;
  TNode<IntPtrT> phi_bb37_22;
  TNode<IntPtrT> tmp53;
  TNode<IntPtrT> tmp54;
  TNode<IntPtrT> tmp55;
  TNode<HeapObject> tmp56;
  TNode<IntPtrT> tmp57;
  TNode<BoolT> tmp58;
  TNode<Float64T> tmp59;
  TNode<BoolT> tmp60;
  if (block37.is_used()) {
    ca_.Bind(&block37, &phi_bb37_7, &phi_bb37_8, &phi_bb37_9, &phi_bb37_10, &phi_bb37_11, &phi_bb37_16, &phi_bb37_17, &phi_bb37_21, &phi_bb37_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp53 = FromConstexpr_intptr_constexpr_int31_0(state_, kDoubleSize);
    tmp54 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{phi_bb37_22}, TNode<IntPtrT>{tmp53});
    tmp55 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp45}, TNode<IntPtrT>{tmp54});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp56, tmp57) = NewReference_float64_or_hole_0(state_, TNode<HeapObject>{tmp3}, TNode<IntPtrT>{tmp55}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/math.tq", 426);
    std::tie(tmp58, tmp59) = LoadFloat64OrHole_0(state_, TorqueStructReference_float64_or_hole_0{TNode<HeapObject>{tmp56}, TNode<IntPtrT>{tmp57}, TorqueStructUnsafe_0{}}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/base.tq", 126);
    tmp60 = CodeStubAssembler(state_).Word32BinaryNot(TNode<BoolT>{tmp58});
    ca_.Branch(tmp60, &block41, std::vector<Node*>{phi_bb37_7, phi_bb37_8, phi_bb37_9, phi_bb37_10, phi_bb37_11, phi_bb37_16, phi_bb37_17}, &block42, std::vector<Node*>{phi_bb37_7, phi_bb37_8, phi_bb37_9, phi_bb37_10, phi_bb37_11, phi_bb37_16, phi_bb37_17});
  }

  TNode<BoolT> phi_bb38_7;
  TNode<Float64T> phi_bb38_8;
  TNode<Float64T> phi_bb38_9;
  TNode<Float64T> phi_bb38_10;
  TNode<IntPtrT> phi_bb38_11;
  TNode<IntPtrT> phi_bb38_16;
  TNode<IntPtrT> phi_bb38_17;
  TNode<IntPtrT> phi_bb38_21;
  TNode<IntPtrT> phi_bb38_22;
  if (block38.is_used()) {
    ca_.Bind(&block38, &phi_bb38_7, &phi_bb38_8, &phi_bb38_9, &phi_bb38_10, &phi_bb38_11, &phi_bb38_16, &phi_bb38_17, &phi_bb38_21, &phi_bb38_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<BoolT> phi_bb42_7;
  TNode<Float64T> phi_bb42_8;
  TNode<Float64T> phi_bb42_9;
  TNode<Float64T> phi_bb42_10;
  TNode<IntPtrT> phi_bb42_11;
  TNode<IntPtrT> phi_bb42_16;
  TNode<IntPtrT> phi_bb42_17;
  if (block42.is_used()) {
    ca_.Bind(&block42, &phi_bb42_7, &phi_bb42_8, &phi_bb42_9, &phi_bb42_10, &phi_bb42_11, &phi_bb42_16, &phi_bb42_17);
    ca_.SetSourcePosition("../../src/builtins/base.tq", 126);
    CodeStubAssembler(state_).FailAssert("Torque assert '!this.is_hole' failed", "src/builtins/base.tq", 126);
  }

  TNode<BoolT> phi_bb41_7;
  TNode<Float64T> phi_bb41_8;
  TNode<Float64T> phi_bb41_9;
  TNode<Float64T> phi_bb41_10;
  TNode<IntPtrT> phi_bb41_11;
  TNode<IntPtrT> phi_bb41_16;
  TNode<IntPtrT> phi_bb41_17;
  TNode<Float64T> tmp61;
  TNode<Float64T> tmp62;
  TNode<Float64T> tmp63;
  TNode<Float64T> tmp64;
  TNode<Float64T> tmp65;
  TNode<Float64T> tmp66;
  TNode<IntPtrT> tmp67;
  TNode<IntPtrT> tmp68;
  if (block41.is_used()) {
    ca_.Bind(&block41, &phi_bb41_7, &phi_bb41_8, &phi_bb41_9, &phi_bb41_10, &phi_bb41_11, &phi_bb41_16, &phi_bb41_17);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 426);
    tmp61 = CodeStubAssembler(state_).Float64Div(TNode<Float64T>{tmp59}, TNode<Float64T>{phi_bb41_8});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 427);
    tmp62 = CodeStubAssembler(state_).Float64Mul(TNode<Float64T>{tmp61}, TNode<Float64T>{tmp61});
    tmp63 = CodeStubAssembler(state_).Float64Sub(TNode<Float64T>{tmp62}, TNode<Float64T>{phi_bb41_10});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 428);
    tmp64 = CodeStubAssembler(state_).Float64Add(TNode<Float64T>{phi_bb41_9}, TNode<Float64T>{tmp63});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 429);
    tmp65 = CodeStubAssembler(state_).Float64Sub(TNode<Float64T>{tmp64}, TNode<Float64T>{phi_bb41_9});
    tmp66 = CodeStubAssembler(state_).Float64Sub(TNode<Float64T>{tmp65}, TNode<Float64T>{tmp63});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 425);
    tmp67 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp68 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{phi_bb41_11}, TNode<IntPtrT>{tmp67});
    ca_.Goto(&block31, phi_bb41_7, phi_bb41_8, tmp64, tmp66, tmp68);
  }

  TNode<BoolT> phi_bb30_7;
  TNode<Float64T> phi_bb30_8;
  TNode<Float64T> phi_bb30_9;
  TNode<Float64T> phi_bb30_10;
  TNode<IntPtrT> phi_bb30_11;
  TNode<Float64T> tmp69;
  TNode<Float64T> tmp70;
  TNode<Number> tmp71;
  if (block30.is_used()) {
    ca_.Bind(&block30, &phi_bb30_7, &phi_bb30_8, &phi_bb30_9, &phi_bb30_10, &phi_bb30_11);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 432);
    tmp69 = CodeStubAssembler(state_).Float64Sqrt(TNode<Float64T>{phi_bb30_9});
    tmp70 = CodeStubAssembler(state_).Float64Mul(TNode<Float64T>{tmp69}, TNode<Float64T>{phi_bb30_8});
    tmp71 = Convert_Number_float64_0(state_, TNode<Float64T>{tmp70});
    arguments.PopAndReturn(tmp71);
  }
}

TF_BUILTIN(MathRandom, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kReceiver));
USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block18(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block19(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block27(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block28(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block31(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block30(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block36(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block37(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block41(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block40(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<IntPtrT> tmp1;
  TNode<IntPtrT> tmp2;
  TNode<Smi> tmp3;
  TNode<IntPtrT> tmp4;
  TNode<IntPtrT> tmp5;
  TNode<UintPtrT> tmp6;
  TNode<UintPtrT> tmp7;
  TNode<BoolT> tmp8;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 441);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp1 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp3 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter0, tmp2});
    tmp4 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp3});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 441);
    tmp5 = FromConstexpr_intptr_constexpr_intptr_0(state_, Context::Field::MATH_RANDOM_INDEX_INDEX);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp6 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp5});
    tmp7 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp4});
    tmp8 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp6}, TNode<UintPtrT>{tmp7});
    ca_.Branch(tmp8, &block7, std::vector<Node*>{}, &block8, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp9;
  TNode<IntPtrT> tmp10;
  TNode<IntPtrT> tmp11;
  TNode<HeapObject> tmp12;
  TNode<IntPtrT> tmp13;
  TNode<Object> tmp14;
  TNode<Smi> tmp15;
  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp9 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp10 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp5}, TNode<IntPtrT>{tmp9});
    tmp11 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp0}, TNode<IntPtrT>{tmp10});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp12, tmp13) = NewReference_Object_0(state_, TNode<HeapObject>{parameter0}, TNode<IntPtrT>{tmp11}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/math.tq", 441);
    tmp14 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp12, tmp13});
    compiler::CodeAssemblerLabel label16(&ca_);
    tmp15 = Cast_Smi_0(state_, TNode<Object>{tmp14}, &label16);
    ca_.Goto(&block10);
    if (label16.is_used()) {
      ca_.Bind(&label16);
      ca_.Goto(&block11);
    }
  }

  if (block8.is_used()) {
    ca_.Bind(&block8);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  if (block11.is_used()) {
    ca_.Bind(&block11);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 442);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/math.tq:442:17");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> tmp17;
  TNode<BoolT> tmp18;
  if (block10.is_used()) {
    ca_.Bind(&block10);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 443);
    tmp17 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp18 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp15}, TNode<Smi>{tmp17});
    ca_.Branch(tmp18, &block12, std::vector<Node*>{}, &block13, std::vector<Node*>{tmp15});
  }

  TNode<Smi> tmp19;
  if (block12.is_used()) {
    ca_.Bind(&block12);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 445);
    tmp19 = CodeStubAssembler(state_).RefillMathRandom(TNode<NativeContext>{parameter0});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 443);
    ca_.Goto(&block13, tmp19);
  }

  TNode<Smi> phi_bb13_2;
  TNode<Smi> tmp20;
  TNode<Smi> tmp21;
  TNode<IntPtrT> tmp22;
  TNode<IntPtrT> tmp23;
  TNode<IntPtrT> tmp24;
  TNode<Smi> tmp25;
  TNode<IntPtrT> tmp26;
  TNode<IntPtrT> tmp27;
  TNode<UintPtrT> tmp28;
  TNode<UintPtrT> tmp29;
  TNode<BoolT> tmp30;
  if (block13.is_used()) {
    ca_.Bind(&block13, &phi_bb13_2);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 447);
    tmp20 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp21 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb13_2}, TNode<Smi>{tmp20});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 448);
    tmp22 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp23 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp24 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp25 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter0, tmp24});
    tmp26 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp25});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 448);
    tmp27 = FromConstexpr_intptr_constexpr_intptr_0(state_, Context::Field::MATH_RANDOM_INDEX_INDEX);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp28 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp27});
    tmp29 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp26});
    tmp30 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp28}, TNode<UintPtrT>{tmp29});
    ca_.Branch(tmp30, &block18, std::vector<Node*>{}, &block19, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp31;
  TNode<IntPtrT> tmp32;
  TNode<IntPtrT> tmp33;
  TNode<HeapObject> tmp34;
  TNode<IntPtrT> tmp35;
  TNode<IntPtrT> tmp36;
  TNode<IntPtrT> tmp37;
  TNode<IntPtrT> tmp38;
  TNode<Smi> tmp39;
  TNode<IntPtrT> tmp40;
  TNode<IntPtrT> tmp41;
  TNode<UintPtrT> tmp42;
  TNode<UintPtrT> tmp43;
  TNode<BoolT> tmp44;
  if (block18.is_used()) {
    ca_.Bind(&block18);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp31 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp32 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp27}, TNode<IntPtrT>{tmp31});
    tmp33 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp22}, TNode<IntPtrT>{tmp32});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp34, tmp35) = NewReference_Object_0(state_, TNode<HeapObject>{parameter0}, TNode<IntPtrT>{tmp33}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/math.tq", 448);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp34, tmp35}, tmp21);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 451);
    tmp36 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp37 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp38 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp39 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter0, tmp38});
    tmp40 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp39});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 451);
    tmp41 = FromConstexpr_intptr_constexpr_intptr_0(state_, Context::Field::MATH_RANDOM_CACHE_INDEX);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp42 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp41});
    tmp43 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp40});
    tmp44 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp42}, TNode<UintPtrT>{tmp43});
    ca_.Branch(tmp44, &block27, std::vector<Node*>{}, &block28, std::vector<Node*>{});
  }

  if (block19.is_used()) {
    ca_.Bind(&block19);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp45;
  TNode<IntPtrT> tmp46;
  TNode<IntPtrT> tmp47;
  TNode<HeapObject> tmp48;
  TNode<IntPtrT> tmp49;
  TNode<Object> tmp50;
  TNode<FixedDoubleArray> tmp51;
  if (block27.is_used()) {
    ca_.Bind(&block27);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp45 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp46 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp41}, TNode<IntPtrT>{tmp45});
    tmp47 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp36}, TNode<IntPtrT>{tmp46});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp48, tmp49) = NewReference_Object_0(state_, TNode<HeapObject>{parameter0}, TNode<IntPtrT>{tmp47}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/math.tq", 451);
    tmp50 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp48, tmp49});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 450);
    compiler::CodeAssemblerLabel label52(&ca_);
    tmp51 = Cast_FixedDoubleArray_1(state_, TNode<Context>{parameter0}, TNode<Object>{tmp50}, &label52);
    ca_.Goto(&block30);
    if (label52.is_used()) {
      ca_.Bind(&label52);
      ca_.Goto(&block31);
    }
  }

  if (block28.is_used()) {
    ca_.Bind(&block28);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  if (block31.is_used()) {
    ca_.Bind(&block31);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 452);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/math.tq:452:17");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp53;
  TNode<IntPtrT> tmp54;
  TNode<IntPtrT> tmp55;
  TNode<Smi> tmp56;
  TNode<IntPtrT> tmp57;
  TNode<IntPtrT> tmp58;
  TNode<UintPtrT> tmp59;
  TNode<UintPtrT> tmp60;
  TNode<BoolT> tmp61;
  if (block30.is_used()) {
    ca_.Bind(&block30);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 454);
    tmp53 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp54 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp55 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 22);
    tmp56 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp51, tmp55});
    tmp57 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp56});
    ca_.SetSourcePosition("../../src/builtins/math.tq", 454);
    tmp58 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp21});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp59 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp58});
    tmp60 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp57});
    tmp61 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp59}, TNode<UintPtrT>{tmp60});
    ca_.Branch(tmp61, &block36, std::vector<Node*>{}, &block37, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp62;
  TNode<IntPtrT> tmp63;
  TNode<IntPtrT> tmp64;
  TNode<HeapObject> tmp65;
  TNode<IntPtrT> tmp66;
  TNode<BoolT> tmp67;
  TNode<Float64T> tmp68;
  TNode<BoolT> tmp69;
  if (block36.is_used()) {
    ca_.Bind(&block36);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp62 = FromConstexpr_intptr_constexpr_int31_0(state_, kDoubleSize);
    tmp63 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp58}, TNode<IntPtrT>{tmp62});
    tmp64 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp53}, TNode<IntPtrT>{tmp63});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp65, tmp66) = NewReference_float64_or_hole_0(state_, TNode<HeapObject>{tmp51}, TNode<IntPtrT>{tmp64}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/math.tq", 454);
    std::tie(tmp67, tmp68) = LoadFloat64OrHole_0(state_, TorqueStructReference_float64_or_hole_0{TNode<HeapObject>{tmp65}, TNode<IntPtrT>{tmp66}, TorqueStructUnsafe_0{}}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/base.tq", 126);
    tmp69 = CodeStubAssembler(state_).Word32BinaryNot(TNode<BoolT>{tmp67});
    ca_.Branch(tmp69, &block40, std::vector<Node*>{}, &block41, std::vector<Node*>{});
  }

  if (block37.is_used()) {
    ca_.Bind(&block37);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  if (block41.is_used()) {
    ca_.Bind(&block41);
    ca_.SetSourcePosition("../../src/builtins/base.tq", 126);
    CodeStubAssembler(state_).FailAssert("Torque assert '!this.is_hole' failed", "src/builtins/base.tq", 126);
  }

  TNode<HeapNumber> tmp70;
  if (block40.is_used()) {
    ca_.Bind(&block40);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 455);
    tmp70 = CodeStubAssembler(state_).AllocateHeapNumberWithValue(TNode<Float64T>{tmp68});
    CodeStubAssembler(state_).Return(tmp70);
  }
}

TNode<FixedDoubleArray> Cast_FixedDoubleArray_1(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<Object> p_o, compiler::CodeAssemblerLabel* label_CastError) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<HeapObject> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/cast.tq", 157);
    compiler::CodeAssemblerLabel label1(&ca_);
    tmp0 = CodeStubAssembler(state_).TaggedToHeapObject(TNode<Object>{p_o}, &label1);
    ca_.Goto(&block3);
    if (label1.is_used()) {
      ca_.Bind(&label1);
      ca_.Goto(&block4);
    }
  }

  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.Goto(&block1);
  }

  TNode<FixedDoubleArray> tmp2;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    compiler::CodeAssemblerLabel label3(&ca_);
    tmp2 = Cast_FixedDoubleArray_0(state_, TNode<HeapObject>{tmp0}, &label3);
    ca_.Goto(&block5);
    if (label3.is_used()) {
      ca_.Bind(&label3);
      ca_.Goto(&block6);
    }
  }

  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.Goto(&block1);
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/math.tq", 450);
    ca_.Goto(&block7);
  }

  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.Goto(label_CastError);
  }

    ca_.Bind(&block7);
  return TNode<FixedDoubleArray>{tmp2};
}

}  // namespace internal
}  // namespace v8

