#include "src/builtins/builtins-array-gen.h"
#include "src/builtins/builtins-bigint-gen.h"
#include "src/builtins/builtins-collections-gen.h"
#include "src/builtins/builtins-constructor-gen.h"
#include "src/builtins/builtins-data-view-gen.h"
#include "src/builtins/builtins-iterator-gen.h"
#include "src/builtins/builtins-promise-gen.h"
#include "src/builtins/builtins-promise.h"
#include "src/builtins/builtins-proxy-gen.h"
#include "src/builtins/builtins-regexp-gen.h"
#include "src/builtins/builtins-string-gen.h"
#include "src/builtins/builtins-typed-array-gen.h"
#include "src/builtins/builtins-utils-gen.h"
#include "src/builtins/builtins-wasm-gen.h"
#include "src/builtins/builtins.h"
#include "src/codegen/code-factory.h"
#include "src/heap/factory-inl.h"
#include "src/ic/binary-op-assembler.h"
#include "src/objects/arguments.h"
#include "src/objects/bigint.h"
#include "src/objects/elements-kind.h"
#include "src/objects/free-space.h"
#include "src/objects/js-break-iterator.h"
#include "src/objects/js-collator.h"
#include "src/objects/js-date-time-format.h"
#include "src/objects/js-display-names.h"
#include "src/objects/js-generator.h"
#include "src/objects/js-list-format.h"
#include "src/objects/js-locale.h"
#include "src/objects/js-number-format.h"
#include "src/objects/js-objects.h"
#include "src/objects/js-plural-rules.h"
#include "src/objects/js-promise.h"
#include "src/objects/js-regexp-string-iterator.h"
#include "src/objects/js-relative-time-format.h"
#include "src/objects/js-segment-iterator.h"
#include "src/objects/js-segmenter.h"
#include "src/objects/js-weak-refs.h"
#include "src/objects/objects.h"
#include "src/objects/ordered-hash-table.h"
#include "src/objects/property-array.h"
#include "src/objects/property-descriptor-object.h"
#include "src/objects/source-text-module.h"
#include "src/objects/stack-frame-info.h"
#include "src/objects/synthetic-module.h"
#include "src/objects/template-objects.h"
#include "src/torque/runtime-support.h"
#include "torque-generated/src/builtins/aggregate-error-tq-csa.h"
#include "torque-generated/src/builtins/array-copywithin-tq-csa.h"
#include "torque-generated/src/builtins/array-every-tq-csa.h"
#include "torque-generated/src/builtins/array-filter-tq-csa.h"
#include "torque-generated/src/builtins/array-find-tq-csa.h"
#include "torque-generated/src/builtins/array-findindex-tq-csa.h"
#include "torque-generated/src/builtins/array-foreach-tq-csa.h"
#include "torque-generated/src/builtins/array-from-tq-csa.h"
#include "torque-generated/src/builtins/array-isarray-tq-csa.h"
#include "torque-generated/src/builtins/array-join-tq-csa.h"
#include "torque-generated/src/builtins/array-lastindexof-tq-csa.h"
#include "torque-generated/src/builtins/array-map-tq-csa.h"
#include "torque-generated/src/builtins/array-of-tq-csa.h"
#include "torque-generated/src/builtins/array-reduce-right-tq-csa.h"
#include "torque-generated/src/builtins/array-reduce-tq-csa.h"
#include "torque-generated/src/builtins/array-reverse-tq-csa.h"
#include "torque-generated/src/builtins/array-shift-tq-csa.h"
#include "torque-generated/src/builtins/array-slice-tq-csa.h"
#include "torque-generated/src/builtins/array-some-tq-csa.h"
#include "torque-generated/src/builtins/array-splice-tq-csa.h"
#include "torque-generated/src/builtins/array-unshift-tq-csa.h"
#include "torque-generated/src/builtins/array-tq-csa.h"
#include "torque-generated/src/builtins/base-tq-csa.h"
#include "torque-generated/src/builtins/bigint-tq-csa.h"
#include "torque-generated/src/builtins/boolean-tq-csa.h"
#include "torque-generated/src/builtins/builtins-string-tq-csa.h"
#include "torque-generated/src/builtins/cast-tq-csa.h"
#include "torque-generated/src/builtins/collections-tq-csa.h"
#include "torque-generated/src/builtins/conversion-tq-csa.h"
#include "torque-generated/src/builtins/convert-tq-csa.h"
#include "torque-generated/src/builtins/console-tq-csa.h"
#include "torque-generated/src/builtins/data-view-tq-csa.h"
#include "torque-generated/src/builtins/finalization-registry-tq-csa.h"
#include "torque-generated/src/builtins/frames-tq-csa.h"
#include "torque-generated/src/builtins/frame-arguments-tq-csa.h"
#include "torque-generated/src/builtins/function-tq-csa.h"
#include "torque-generated/src/builtins/growable-fixed-array-tq-csa.h"
#include "torque-generated/src/builtins/ic-callable-tq-csa.h"
#include "torque-generated/src/builtins/ic-tq-csa.h"
#include "torque-generated/src/builtins/internal-coverage-tq-csa.h"
#include "torque-generated/src/builtins/internal-tq-csa.h"
#include "torque-generated/src/builtins/iterator-tq-csa.h"
#include "torque-generated/src/builtins/math-tq-csa.h"
#include "torque-generated/src/builtins/number-tq-csa.h"
#include "torque-generated/src/builtins/object-fromentries-tq-csa.h"
#include "torque-generated/src/builtins/object-tq-csa.h"
#include "torque-generated/src/builtins/promise-abstract-operations-tq-csa.h"
#include "torque-generated/src/builtins/promise-all-tq-csa.h"
#include "torque-generated/src/builtins/promise-all-element-closure-tq-csa.h"
#include "torque-generated/src/builtins/promise-any-tq-csa.h"
#include "torque-generated/src/builtins/promise-constructor-tq-csa.h"
#include "torque-generated/src/builtins/promise-finally-tq-csa.h"
#include "torque-generated/src/builtins/promise-misc-tq-csa.h"
#include "torque-generated/src/builtins/promise-race-tq-csa.h"
#include "torque-generated/src/builtins/promise-reaction-job-tq-csa.h"
#include "torque-generated/src/builtins/promise-resolve-tq-csa.h"
#include "torque-generated/src/builtins/promise-then-tq-csa.h"
#include "torque-generated/src/builtins/promise-jobs-tq-csa.h"
#include "torque-generated/src/builtins/proxy-constructor-tq-csa.h"
#include "torque-generated/src/builtins/proxy-delete-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-get-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-get-prototype-of-tq-csa.h"
#include "torque-generated/src/builtins/proxy-has-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-is-extensible-tq-csa.h"
#include "torque-generated/src/builtins/proxy-prevent-extensions-tq-csa.h"
#include "torque-generated/src/builtins/proxy-revocable-tq-csa.h"
#include "torque-generated/src/builtins/proxy-revoke-tq-csa.h"
#include "torque-generated/src/builtins/proxy-set-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-set-prototype-of-tq-csa.h"
#include "torque-generated/src/builtins/proxy-tq-csa.h"
#include "torque-generated/src/builtins/reflect-tq-csa.h"
#include "torque-generated/src/builtins/regexp-exec-tq-csa.h"
#include "torque-generated/src/builtins/regexp-match-all-tq-csa.h"
#include "torque-generated/src/builtins/regexp-match-tq-csa.h"
#include "torque-generated/src/builtins/regexp-replace-tq-csa.h"
#include "torque-generated/src/builtins/regexp-search-tq-csa.h"
#include "torque-generated/src/builtins/regexp-source-tq-csa.h"
#include "torque-generated/src/builtins/regexp-split-tq-csa.h"
#include "torque-generated/src/builtins/regexp-test-tq-csa.h"
#include "torque-generated/src/builtins/regexp-tq-csa.h"
#include "torque-generated/src/builtins/string-endswith-tq-csa.h"
#include "torque-generated/src/builtins/string-html-tq-csa.h"
#include "torque-generated/src/builtins/string-iterator-tq-csa.h"
#include "torque-generated/src/builtins/string-pad-tq-csa.h"
#include "torque-generated/src/builtins/string-repeat-tq-csa.h"
#include "torque-generated/src/builtins/string-replaceall-tq-csa.h"
#include "torque-generated/src/builtins/string-slice-tq-csa.h"
#include "torque-generated/src/builtins/string-startswith-tq-csa.h"
#include "torque-generated/src/builtins/string-substring-tq-csa.h"
#include "torque-generated/src/builtins/string-substr-tq-csa.h"
#include "torque-generated/src/builtins/symbol-tq-csa.h"
#include "torque-generated/src/builtins/torque-internal-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-createtypedarray-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-every-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-entries-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-filter-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-find-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-findindex-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-foreach-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-from-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-keys-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-of-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-reduce-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-reduceright-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-set-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-slice-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-some-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-sort-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-subarray-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-values-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-tq-csa.h"
#include "torque-generated/src/builtins/wasm-tq-csa.h"
#include "torque-generated/src/builtins/weak-ref-tq-csa.h"
#include "torque-generated/src/ic/handler-configuration-tq-csa.h"
#include "torque-generated/src/objects/allocation-site-tq-csa.h"
#include "torque-generated/src/objects/api-callbacks-tq-csa.h"
#include "torque-generated/src/objects/arguments-tq-csa.h"
#include "torque-generated/src/objects/cell-tq-csa.h"
#include "torque-generated/src/objects/code-tq-csa.h"
#include "torque-generated/src/objects/contexts-tq-csa.h"
#include "torque-generated/src/objects/data-handler-tq-csa.h"
#include "torque-generated/src/objects/debug-objects-tq-csa.h"
#include "torque-generated/src/objects/descriptor-array-tq-csa.h"
#include "torque-generated/src/objects/embedder-data-array-tq-csa.h"
#include "torque-generated/src/objects/feedback-cell-tq-csa.h"
#include "torque-generated/src/objects/feedback-vector-tq-csa.h"
#include "torque-generated/src/objects/fixed-array-tq-csa.h"
#include "torque-generated/src/objects/foreign-tq-csa.h"
#include "torque-generated/src/objects/free-space-tq-csa.h"
#include "torque-generated/src/objects/heap-number-tq-csa.h"
#include "torque-generated/src/objects/heap-object-tq-csa.h"
#include "torque-generated/src/objects/intl-objects-tq-csa.h"
#include "torque-generated/src/objects/js-array-buffer-tq-csa.h"
#include "torque-generated/src/objects/js-array-tq-csa.h"
#include "torque-generated/src/objects/js-collection-iterator-tq-csa.h"
#include "torque-generated/src/objects/js-collection-tq-csa.h"
#include "torque-generated/src/objects/js-generator-tq-csa.h"
#include "torque-generated/src/objects/js-objects-tq-csa.h"
#include "torque-generated/src/objects/js-promise-tq-csa.h"
#include "torque-generated/src/objects/js-proxy-tq-csa.h"
#include "torque-generated/src/objects/js-regexp-string-iterator-tq-csa.h"
#include "torque-generated/src/objects/js-regexp-tq-csa.h"
#include "torque-generated/src/objects/js-weak-refs-tq-csa.h"
#include "torque-generated/src/objects/literal-objects-tq-csa.h"
#include "torque-generated/src/objects/map-tq-csa.h"
#include "torque-generated/src/objects/microtask-tq-csa.h"
#include "torque-generated/src/objects/module-tq-csa.h"
#include "torque-generated/src/objects/name-tq-csa.h"
#include "torque-generated/src/objects/oddball-tq-csa.h"
#include "torque-generated/src/objects/ordered-hash-table-tq-csa.h"
#include "torque-generated/src/objects/primitive-heap-object-tq-csa.h"
#include "torque-generated/src/objects/promise-tq-csa.h"
#include "torque-generated/src/objects/property-array-tq-csa.h"
#include "torque-generated/src/objects/property-cell-tq-csa.h"
#include "torque-generated/src/objects/property-descriptor-object-tq-csa.h"
#include "torque-generated/src/objects/prototype-info-tq-csa.h"
#include "torque-generated/src/objects/regexp-match-info-tq-csa.h"
#include "torque-generated/src/objects/scope-info-tq-csa.h"
#include "torque-generated/src/objects/script-tq-csa.h"
#include "torque-generated/src/objects/shared-function-info-tq-csa.h"
#include "torque-generated/src/objects/source-text-module-tq-csa.h"
#include "torque-generated/src/objects/stack-frame-info-tq-csa.h"
#include "torque-generated/src/objects/string-tq-csa.h"
#include "torque-generated/src/objects/struct-tq-csa.h"
#include "torque-generated/src/objects/synthetic-module-tq-csa.h"
#include "torque-generated/src/objects/template-objects-tq-csa.h"
#include "torque-generated/src/objects/template-tq-csa.h"
#include "torque-generated/src/wasm/wasm-objects-tq-csa.h"
#include "torque-generated/test/torque/test-torque-tq-csa.h"
#include "torque-generated/third_party/v8/builtins/array-sort-tq-csa.h"

namespace v8 {
namespace internal {

TNode<Context> CreatePromiseAllResolveElementContext_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<PromiseCapability> p_capability, TNode<NativeContext> p_nativeContext) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block14(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block20(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block21(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block23(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Context> tmp0;
  TNode<IntPtrT> tmp1;
  TNode<IntPtrT> tmp2;
  TNode<IntPtrT> tmp3;
  TNode<Smi> tmp4;
  TNode<IntPtrT> tmp5;
  TNode<IntPtrT> tmp6;
  TNode<UintPtrT> tmp7;
  TNode<UintPtrT> tmp8;
  TNode<BoolT> tmp9;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 21);
    tmp0 = CodeStubAssembler(state_).AllocateSyntheticFunctionContext(TNode<NativeContext>{p_nativeContext}, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementLength);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 24);
    tmp1 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp3 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp4 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp0, tmp3});
    tmp5 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp4});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp6 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp7 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp6});
    tmp8 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp5});
    tmp9 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp7}, TNode<UintPtrT>{tmp8});
    ca_.Branch(tmp9, &block6, std::vector<Node*>{}, &block7, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp10;
  TNode<IntPtrT> tmp11;
  TNode<IntPtrT> tmp12;
  TNode<HeapObject> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<Smi> tmp15;
  TNode<IntPtrT> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<IntPtrT> tmp18;
  TNode<Smi> tmp19;
  TNode<IntPtrT> tmp20;
  TNode<IntPtrT> tmp21;
  TNode<UintPtrT> tmp22;
  TNode<UintPtrT> tmp23;
  TNode<BoolT> tmp24;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp10 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp11 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp6}, TNode<IntPtrT>{tmp10});
    tmp12 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp1}, TNode<IntPtrT>{tmp11});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp13, tmp14) = NewReference_Object_0(state_, TNode<HeapObject>{tmp0}, TNode<IntPtrT>{tmp12}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 26);
    tmp15 = CodeStubAssembler(state_).SmiConstant(1);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 24);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp13, tmp14}, tmp15);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 27);
    tmp16 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp17 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp18 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp19 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp0, tmp18});
    tmp20 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp19});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp21 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementCapabilitySlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp22 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp21});
    tmp23 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp20});
    tmp24 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp22}, TNode<UintPtrT>{tmp23});
    ca_.Branch(tmp24, &block13, std::vector<Node*>{}, &block14, std::vector<Node*>{});
  }

  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp25;
  TNode<IntPtrT> tmp26;
  TNode<IntPtrT> tmp27;
  TNode<HeapObject> tmp28;
  TNode<IntPtrT> tmp29;
  TNode<IntPtrT> tmp30;
  TNode<IntPtrT> tmp31;
  TNode<IntPtrT> tmp32;
  TNode<Smi> tmp33;
  TNode<IntPtrT> tmp34;
  TNode<IntPtrT> tmp35;
  TNode<UintPtrT> tmp36;
  TNode<UintPtrT> tmp37;
  TNode<BoolT> tmp38;
  if (block13.is_used()) {
    ca_.Bind(&block13);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp25 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp26 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp21}, TNode<IntPtrT>{tmp25});
    tmp27 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp16}, TNode<IntPtrT>{tmp26});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp28, tmp29) = NewReference_Object_0(state_, TNode<HeapObject>{tmp0}, TNode<IntPtrT>{tmp27}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 27);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp28, tmp29}, p_capability);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 30);
    tmp30 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp31 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp32 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp33 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp0, tmp32});
    tmp34 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp33});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp35 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementValuesSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp36 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp35});
    tmp37 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp34});
    tmp38 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp36}, TNode<UintPtrT>{tmp37});
    ca_.Branch(tmp38, &block20, std::vector<Node*>{}, &block21, std::vector<Node*>{});
  }

  if (block14.is_used()) {
    ca_.Bind(&block14);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp39;
  TNode<IntPtrT> tmp40;
  TNode<IntPtrT> tmp41;
  TNode<HeapObject> tmp42;
  TNode<IntPtrT> tmp43;
  TNode<FixedArray> tmp44;
  if (block20.is_used()) {
    ca_.Bind(&block20);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp39 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp40 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp35}, TNode<IntPtrT>{tmp39});
    tmp41 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp30}, TNode<IntPtrT>{tmp40});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp42, tmp43) = NewReference_Object_0(state_, TNode<HeapObject>{tmp0}, TNode<IntPtrT>{tmp41}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 32);
    tmp44 = kEmptyFixedArray_0(state_);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 30);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp42, tmp43}, tmp44);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 19);
    ca_.Goto(&block23);
  }

  if (block21.is_used()) {
    ca_.Bind(&block21);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

    ca_.Bind(&block23);
  return TNode<Context>{tmp0};
}

TNode<JSFunction> CreatePromiseAllResolveElementFunction_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<Context> p_resolveElementContext, TNode<Smi> p_index, TNode<NativeContext> p_nativeContext, TNode<SharedFunctionInfo> p_resolveFunction) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block14(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block15(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Smi> tmp0;
  TNode<BoolT> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 39);
    tmp0 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp1 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{p_index}, TNode<Smi>{tmp0});
    ca_.Branch(tmp1, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    CodeStubAssembler(state_).FailAssert("Torque assert 'index > 0' failed", "src/builtins/promise-all.tq", 39);
  }

  TNode<Smi> tmp2;
  TNode<BoolT> tmp3;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 40);
    tmp2 = FromConstexpr_Smi_constexpr_int31_0(state_, PropertyArray::HashField::kMax);
    tmp3 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{p_index}, TNode<Smi>{tmp2});
    ca_.Branch(tmp3, &block4, std::vector<Node*>{}, &block5, std::vector<Node*>{});
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    CodeStubAssembler(state_).FailAssert("Torque assert 'index < kPropertyArrayHashFieldMax' failed", "src/builtins/promise-all.tq", 40);
  }

  TNode<IntPtrT> tmp4;
  TNode<IntPtrT> tmp5;
  TNode<IntPtrT> tmp6;
  TNode<Smi> tmp7;
  TNode<IntPtrT> tmp8;
  TNode<IntPtrT> tmp9;
  TNode<UintPtrT> tmp10;
  TNode<UintPtrT> tmp11;
  TNode<BoolT> tmp12;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 43);
    tmp4 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp5 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp6 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp7 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_nativeContext, tmp6});
    tmp8 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp7});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 43);
    tmp9 = FromConstexpr_intptr_constexpr_intptr_0(state_, Context::Field::STRICT_FUNCTION_WITHOUT_PROTOTYPE_MAP_INDEX);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp10 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp9});
    tmp11 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp8});
    tmp12 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp10}, TNode<UintPtrT>{tmp11});
    ca_.Branch(tmp12, &block10, std::vector<Node*>{}, &block11, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<IntPtrT> tmp15;
  TNode<HeapObject> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<Object> tmp18;
  TNode<Map> tmp19;
  TNode<JSFunction> tmp20;
  TNode<BoolT> tmp21;
  if (block10.is_used()) {
    ca_.Bind(&block10);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp13 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp14 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp9}, TNode<IntPtrT>{tmp13});
    tmp15 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp4}, TNode<IntPtrT>{tmp14});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp16, tmp17) = NewReference_Object_0(state_, TNode<HeapObject>{p_nativeContext}, TNode<IntPtrT>{tmp15}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 43);
    tmp18 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp16, tmp17});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 42);
    tmp19 = UnsafeCast_Map_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp18});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 45);
    tmp20 = CodeStubAssembler(state_).AllocateFunctionWithMapAndContext(TNode<Map>{tmp19}, TNode<SharedFunctionInfo>{p_resolveFunction}, TNode<Context>{p_resolveElementContext});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 48);
    tmp21 = FromConstexpr_bool_constexpr_bool_0(state_, (CodeStubAssembler(state_).ConstexprInt31Equal(PropertyArray::kNoHashSentinel, 0)));
    ca_.Branch(tmp21, &block13, std::vector<Node*>{}, &block14, std::vector<Node*>{});
  }

  if (block11.is_used()) {
    ca_.Bind(&block11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  if (block14.is_used()) {
    ca_.Bind(&block14);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 48);
    CodeStubAssembler(state_).FailAssert("Torque assert 'kPropertyArrayNoHashSentinel == 0' failed", "src/builtins/promise-all.tq", 48);
  }

  TNode<IntPtrT> tmp22;
  if (block13.is_used()) {
    ca_.Bind(&block13);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 49);
    tmp22 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp20, tmp22}, p_index);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 36);
    ca_.Goto(&block15);
  }

    ca_.Bind(&block15);
  return TNode<JSFunction>{tmp20};
}

TNode<Context> CreatePromiseResolvingFunctionsContext_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<JSPromise> p_promise, TNode<Object> p_debugEvent, TNode<NativeContext> p_nativeContext) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block14(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block20(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block21(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block23(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Context> tmp0;
  TNode<IntPtrT> tmp1;
  TNode<IntPtrT> tmp2;
  TNode<IntPtrT> tmp3;
  TNode<Smi> tmp4;
  TNode<IntPtrT> tmp5;
  TNode<IntPtrT> tmp6;
  TNode<UintPtrT> tmp7;
  TNode<UintPtrT> tmp8;
  TNode<BoolT> tmp9;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 57);
    tmp0 = CodeStubAssembler(state_).AllocateSyntheticFunctionContext(TNode<NativeContext>{p_nativeContext}, PromiseBuiltins::kPromiseContextLength);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 59);
    tmp1 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp3 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp4 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp0, tmp3});
    tmp5 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp4});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 59);
    tmp6 = FromConstexpr_intptr_constexpr_intptr_0(state_, PromiseBuiltins::kPromiseSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp7 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp6});
    tmp8 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp5});
    tmp9 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp7}, TNode<UintPtrT>{tmp8});
    ca_.Branch(tmp9, &block6, std::vector<Node*>{}, &block7, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp10;
  TNode<IntPtrT> tmp11;
  TNode<IntPtrT> tmp12;
  TNode<HeapObject> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<IntPtrT> tmp15;
  TNode<IntPtrT> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<Smi> tmp18;
  TNode<IntPtrT> tmp19;
  TNode<IntPtrT> tmp20;
  TNode<UintPtrT> tmp21;
  TNode<UintPtrT> tmp22;
  TNode<BoolT> tmp23;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp10 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp11 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp6}, TNode<IntPtrT>{tmp10});
    tmp12 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp1}, TNode<IntPtrT>{tmp11});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp13, tmp14) = NewReference_Object_0(state_, TNode<HeapObject>{tmp0}, TNode<IntPtrT>{tmp12}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 59);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp13, tmp14}, p_promise);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 60);
    tmp15 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp16 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp17 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp18 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp0, tmp17});
    tmp19 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp18});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 60);
    tmp20 = FromConstexpr_intptr_constexpr_intptr_0(state_, PromiseBuiltins::kAlreadyResolvedSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp21 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp20});
    tmp22 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp19});
    tmp23 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp21}, TNode<UintPtrT>{tmp22});
    ca_.Branch(tmp23, &block13, std::vector<Node*>{}, &block14, std::vector<Node*>{});
  }

  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp24;
  TNode<IntPtrT> tmp25;
  TNode<IntPtrT> tmp26;
  TNode<HeapObject> tmp27;
  TNode<IntPtrT> tmp28;
  TNode<Oddball> tmp29;
  TNode<IntPtrT> tmp30;
  TNode<IntPtrT> tmp31;
  TNode<IntPtrT> tmp32;
  TNode<Smi> tmp33;
  TNode<IntPtrT> tmp34;
  TNode<IntPtrT> tmp35;
  TNode<UintPtrT> tmp36;
  TNode<UintPtrT> tmp37;
  TNode<BoolT> tmp38;
  if (block13.is_used()) {
    ca_.Bind(&block13);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp24 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp25 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp20}, TNode<IntPtrT>{tmp24});
    tmp26 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp15}, TNode<IntPtrT>{tmp25});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp27, tmp28) = NewReference_Object_0(state_, TNode<HeapObject>{tmp0}, TNode<IntPtrT>{tmp26}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 60);
    tmp29 = False_0(state_);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp27, tmp28}, tmp29);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 61);
    tmp30 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp31 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp32 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp33 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp0, tmp32});
    tmp34 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp33});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 61);
    tmp35 = FromConstexpr_intptr_constexpr_intptr_0(state_, PromiseBuiltins::kDebugEventSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp36 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp35});
    tmp37 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp34});
    tmp38 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp36}, TNode<UintPtrT>{tmp37});
    ca_.Branch(tmp38, &block20, std::vector<Node*>{}, &block21, std::vector<Node*>{});
  }

  if (block14.is_used()) {
    ca_.Bind(&block14);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp39;
  TNode<IntPtrT> tmp40;
  TNode<IntPtrT> tmp41;
  TNode<HeapObject> tmp42;
  TNode<IntPtrT> tmp43;
  if (block20.is_used()) {
    ca_.Bind(&block20);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp39 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp40 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp35}, TNode<IntPtrT>{tmp39});
    tmp41 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp30}, TNode<IntPtrT>{tmp40});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp42, tmp43) = NewReference_Object_0(state_, TNode<HeapObject>{tmp0}, TNode<IntPtrT>{tmp41}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 61);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp42, tmp43}, p_debugEvent);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 53);
    ca_.Goto(&block23);
  }

  if (block21.is_used()) {
    ca_.Bind(&block21);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

    ca_.Bind(&block23);
  return TNode<Context>{tmp0};
}

TNode<BoolT> IsPromiseThenLookupChainIntact_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<NativeContext> p_nativeContext, TNode<Map> p_receiverMap) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block15(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<BoolT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 67);
    tmp0 = IsForceSlowPath_0(state_);
    ca_.Branch(tmp0, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  TNode<BoolT> tmp1;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    tmp1 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block1, tmp1);
  }

  TNode<BoolT> tmp2;
  TNode<BoolT> tmp3;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 68);
    tmp2 = CodeStubAssembler(state_).IsJSPromiseMap(TNode<Map>{p_receiverMap});
    tmp3 = CodeStubAssembler(state_).Word32BinaryNot(TNode<BoolT>{tmp2});
    ca_.Branch(tmp3, &block4, std::vector<Node*>{}, &block5, std::vector<Node*>{});
  }

  TNode<BoolT> tmp4;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    tmp4 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block1, tmp4);
  }

  TNode<IntPtrT> tmp5;
  TNode<HeapObject> tmp6;
  TNode<IntPtrT> tmp7;
  TNode<IntPtrT> tmp8;
  TNode<IntPtrT> tmp9;
  TNode<Smi> tmp10;
  TNode<IntPtrT> tmp11;
  TNode<IntPtrT> tmp12;
  TNode<UintPtrT> tmp13;
  TNode<UintPtrT> tmp14;
  TNode<BoolT> tmp15;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 69);
    tmp5 = FromConstexpr_intptr_constexpr_int31_0(state_, 16);
    tmp6 = CodeStubAssembler(state_).LoadReference<HeapObject>(CodeStubAssembler::Reference{p_receiverMap, tmp5});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 70);
    tmp7 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp8 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp9 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp10 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_nativeContext, tmp9});
    tmp11 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp10});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 70);
    tmp12 = FromConstexpr_intptr_constexpr_intptr_0(state_, Context::Field::PROMISE_PROTOTYPE_INDEX);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp13 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp12});
    tmp14 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp11});
    tmp15 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp13}, TNode<UintPtrT>{tmp14});
    ca_.Branch(tmp15, &block12, std::vector<Node*>{}, &block13, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<IntPtrT> tmp18;
  TNode<HeapObject> tmp19;
  TNode<IntPtrT> tmp20;
  TNode<Object> tmp21;
  TNode<BoolT> tmp22;
  if (block12.is_used()) {
    ca_.Bind(&block12);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp16 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp17 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp12}, TNode<IntPtrT>{tmp16});
    tmp18 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp7}, TNode<IntPtrT>{tmp17});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp19, tmp20) = NewReference_Object_0(state_, TNode<HeapObject>{p_nativeContext}, TNode<IntPtrT>{tmp18}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 70);
    tmp21 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp19, tmp20});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 69);
    tmp22 = CodeStubAssembler(state_).TaggedNotEqual(TNode<HeapObject>{tmp6}, TNode<Object>{tmp21});
    ca_.Branch(tmp22, &block6, std::vector<Node*>{}, &block7, std::vector<Node*>{});
  }

  if (block13.is_used()) {
    ca_.Bind(&block13);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<BoolT> tmp23;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 71);
    tmp23 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block1, tmp23);
  }

  TNode<BoolT> tmp24;
  TNode<BoolT> tmp25;
  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 72);
    tmp24 = CodeStubAssembler(state_).IsPromiseThenProtectorCellInvalid();
    tmp25 = CodeStubAssembler(state_).Word32BinaryNot(TNode<BoolT>{tmp24});
    ca_.Goto(&block1, tmp25);
  }

  TNode<BoolT> phi_bb1_3;
  if (block1.is_used()) {
    ca_.Bind(&block1, &phi_bb1_3);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 65);
    ca_.Goto(&block15, phi_bb1_3);
  }

  TNode<BoolT> phi_bb15_3;
    ca_.Bind(&block15, &phi_bb15_3);
  return TNode<BoolT>{phi_bb15_3};
}

TF_BUILTIN(PromiseAll, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kReceiver));
USE(parameter1);
  TNode<Object> parameter2 = UncheckedCast<Object>(Parameter(Descriptor::kIterable));
  USE(parameter2);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Object> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 357);
    tmp0 = GeneratePromiseAll_PromiseAllResolveElementFunctor_PromiseAllRejectElementFunctor_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter1}, TNode<Object>{parameter2}, TorqueStructPromiseAllResolveElementFunctor_0{}, TorqueStructPromiseAllRejectElementFunctor_0{});
    CodeStubAssembler(state_).Return(tmp0);
  }
}

TF_BUILTIN(PromiseAllSettled, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kReceiver));
USE(parameter1);
  TNode<Object> parameter2 = UncheckedCast<Object>(Parameter(Descriptor::kIterable));
  USE(parameter2);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Object> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 366);
    tmp0 = GeneratePromiseAll_PromiseAllSettledResolveElementFunctor_PromiseAllSettledRejectElementFunctor_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter1}, TNode<Object>{parameter2}, TorqueStructPromiseAllSettledResolveElementFunctor_0{}, TorqueStructPromiseAllSettledRejectElementFunctor_0{});
    CodeStubAssembler(state_).Return(tmp0);
  }
}

TNode<Object> GeneratePromiseAll_PromiseAllResolveElementFunctor_PromiseAllRejectElementFunctor_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<Object> p_receiver, TNode<Object> p_iterable, TorqueStructPromiseAllResolveElementFunctor_0 p_createResolveElementFunctor, TorqueStructPromiseAllRejectElementFunctor_0 p_createRejectElementFunctor) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block12(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block16(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block15(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block14(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block11(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block9(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block17(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<NativeContext> tmp0;
  TNode<JSReceiver> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 309);
    tmp0 = CodeStubAssembler(state_).LoadNativeContext(TNode<Context>{p_context});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 312);
    compiler::CodeAssemblerLabel label2(&ca_);
    tmp1 = Cast_JSReceiver_1(state_, TNode<Context>{p_context}, TNode<Object>{p_receiver}, &label2);
    ca_.Goto(&block4);
    if (label2.is_used()) {
      ca_.Bind(&label2);
      ca_.Goto(&block5);
    }
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 313);
    CodeStubAssembler(state_).ThrowTypeError(TNode<Context>{p_context}, MessageTemplate::kCalledOnNonObject, "Promise.all");
  }

  TNode<Oddball> tmp3;
  TNode<PromiseCapability> tmp4;
  TNode<BoolT> tmp5;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 318);
    tmp3 = False_0(state_);
    tmp4 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kNewPromiseCapability, p_context, tmp1, tmp3));
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 321);
    tmp5 = Is_Constructor_JSReceiver_0(state_, TNode<Context>{p_context}, TNode<JSReceiver>{tmp1});
    ca_.Branch(tmp5, &block6, std::vector<Node*>{}, &block7, std::vector<Node*>{});
  }

  if (block7.is_used()) {
    ca_.Bind(&block7);
    CodeStubAssembler(state_).FailAssert("Torque assert 'Is<Constructor>(receiver)' failed", "src/builtins/promise-all.tq", 321);
  }

  TNode<JSReceiver> tmp6;
  TNode<Object> tmp7;
      TNode<Object> tmp9;
  TNode<JSReceiver> tmp10;
  TNode<Object> tmp11;
      TNode<Object> tmp13;
  TNode<Object> tmp14;
    compiler::TypedCodeAssemblerVariable<Object> tmp16(&ca_);
      TNode<Object> tmp18;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 322);
    tmp6 = UnsafeCast_Constructor_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp1});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 328);
    compiler::CodeAssemblerExceptionHandlerLabel catch8__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch8__label);
    tmp7 = GetPromiseResolve_0(state_, TNode<Context>{p_context}, TNode<NativeContext>{tmp0}, TNode<JSReceiver>{tmp6});
    }
    if (catch8__label.is_used()) {
      compiler::CodeAssemblerLabel catch8_skip(&ca_);
      ca_.Goto(&catch8_skip);
      ca_.Bind(&catch8__label, &tmp9);
      ca_.Goto(&block12);
      ca_.Bind(&catch8_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 332);
    compiler::CodeAssemblerExceptionHandlerLabel catch12__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch12__label);
    std::tie(tmp10, tmp11) = IteratorBuiltinsAssembler(state_).GetIterator(TNode<Context>{p_context}, TNode<Object>{p_iterable}).Flatten();
    }
    if (catch12__label.is_used()) {
      compiler::CodeAssemblerLabel catch12_skip(&ca_);
      ca_.Goto(&catch12_skip);
      ca_.Bind(&catch12__label, &tmp13);
      ca_.Goto(&block13);
      ca_.Bind(&catch12_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 339);
    compiler::CodeAssemblerLabel label15(&ca_);
    compiler::CodeAssemblerExceptionHandlerLabel catch17__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch17__label);
    tmp14 = PerformPromiseAll_PromiseAllResolveElementFunctor_PromiseAllRejectElementFunctor_0(state_, TNode<Context>{p_context}, TNode<NativeContext>{tmp0}, TorqueStructIteratorRecord{TNode<JSReceiver>{tmp10}, TNode<Object>{tmp11}}, TNode<JSReceiver>{tmp6}, TNode<PromiseCapability>{tmp4}, TNode<Object>{tmp7}, TorqueStructPromiseAllResolveElementFunctor_0{}, TorqueStructPromiseAllRejectElementFunctor_0{}, &label15, &tmp16);
    }
    if (catch17__label.is_used()) {
      compiler::CodeAssemblerLabel catch17_skip(&ca_);
      ca_.Goto(&catch17_skip);
      ca_.Bind(&catch17__label, &tmp18);
      ca_.Goto(&block16);
      ca_.Bind(&catch17_skip);
    }
    ca_.Goto(&block14);
    if (label15.is_used()) {
      ca_.Bind(&label15);
      ca_.Goto(&block15);
    }
  }

  if (block12.is_used()) {
    ca_.Bind(&block12);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 328);
    ca_.Goto(&block11, tmp9);
  }

  if (block13.is_used()) {
    ca_.Bind(&block13);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 332);
    ca_.Goto(&block11, tmp13);
  }

  if (block16.is_used()) {
    ca_.Bind(&block16);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 339);
    ca_.Goto(&block11, tmp18);
  }

  if (block15.is_used()) {
    ca_.Bind(&block15);
    ca_.Goto(&block9, tmp16.value());
  }

  if (block14.is_used()) {
    ca_.Bind(&block14);
    ca_.Goto(&block1, tmp14);
  }

  TNode<Object> phi_bb11_7;
  if (block11.is_used()) {
    ca_.Bind(&block11, &phi_bb11_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 344);
    ca_.Goto(&block9, phi_bb11_7);
  }

  TNode<Object> phi_bb9_7;
  TNode<Object> tmp19;
  TNode<IntPtrT> tmp20;
  TNode<Object> tmp21;
  TNode<Object> tmp22;
  TNode<Oddball> tmp23;
  TNode<Object> tmp24;
  TNode<IntPtrT> tmp25;
  TNode<HeapObject> tmp26;
  if (block9.is_used()) {
    ca_.Bind(&block9, &phi_bb9_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 347);
    tmp19 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{phi_bb9_7});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 348);
    tmp20 = FromConstexpr_intptr_constexpr_int31_0(state_, 12);
    tmp21 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp4, tmp20});
    tmp22 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp21});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 349);
    tmp23 = Undefined_0(state_);
    tmp24 = CodeStubAssembler(state_).Call(TNode<Context>{p_context}, TNode<Object>{tmp22}, TNode<Object>{tmp23}, TNode<Object>{tmp19});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 350);
    tmp25 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp26 = CodeStubAssembler(state_).LoadReference<HeapObject>(CodeStubAssembler::Reference{tmp4, tmp25});
    ca_.Goto(&block1, tmp26);
  }

  TNode<Object> phi_bb1_3;
  if (block1.is_used()) {
    ca_.Bind(&block1, &phi_bb1_3);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 357);
    ca_.Goto(&block17);
  }

    ca_.Bind(&block17);
  return TNode<Object>{phi_bb1_3};
}

TNode<Object> GeneratePromiseAll_PromiseAllSettledResolveElementFunctor_PromiseAllSettledRejectElementFunctor_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<Object> p_receiver, TNode<Object> p_iterable, TorqueStructPromiseAllSettledResolveElementFunctor_0 p_createResolveElementFunctor, TorqueStructPromiseAllSettledRejectElementFunctor_0 p_createRejectElementFunctor) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block12(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block16(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block15(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block14(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block11(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block9(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block17(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<NativeContext> tmp0;
  TNode<JSReceiver> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 309);
    tmp0 = CodeStubAssembler(state_).LoadNativeContext(TNode<Context>{p_context});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 312);
    compiler::CodeAssemblerLabel label2(&ca_);
    tmp1 = Cast_JSReceiver_1(state_, TNode<Context>{p_context}, TNode<Object>{p_receiver}, &label2);
    ca_.Goto(&block4);
    if (label2.is_used()) {
      ca_.Bind(&label2);
      ca_.Goto(&block5);
    }
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 313);
    CodeStubAssembler(state_).ThrowTypeError(TNode<Context>{p_context}, MessageTemplate::kCalledOnNonObject, "Promise.all");
  }

  TNode<Oddball> tmp3;
  TNode<PromiseCapability> tmp4;
  TNode<BoolT> tmp5;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 318);
    tmp3 = False_0(state_);
    tmp4 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kNewPromiseCapability, p_context, tmp1, tmp3));
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 321);
    tmp5 = Is_Constructor_JSReceiver_0(state_, TNode<Context>{p_context}, TNode<JSReceiver>{tmp1});
    ca_.Branch(tmp5, &block6, std::vector<Node*>{}, &block7, std::vector<Node*>{});
  }

  if (block7.is_used()) {
    ca_.Bind(&block7);
    CodeStubAssembler(state_).FailAssert("Torque assert 'Is<Constructor>(receiver)' failed", "src/builtins/promise-all.tq", 321);
  }

  TNode<JSReceiver> tmp6;
  TNode<Object> tmp7;
      TNode<Object> tmp9;
  TNode<JSReceiver> tmp10;
  TNode<Object> tmp11;
      TNode<Object> tmp13;
  TNode<Object> tmp14;
    compiler::TypedCodeAssemblerVariable<Object> tmp16(&ca_);
      TNode<Object> tmp18;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 322);
    tmp6 = UnsafeCast_Constructor_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp1});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 328);
    compiler::CodeAssemblerExceptionHandlerLabel catch8__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch8__label);
    tmp7 = GetPromiseResolve_0(state_, TNode<Context>{p_context}, TNode<NativeContext>{tmp0}, TNode<JSReceiver>{tmp6});
    }
    if (catch8__label.is_used()) {
      compiler::CodeAssemblerLabel catch8_skip(&ca_);
      ca_.Goto(&catch8_skip);
      ca_.Bind(&catch8__label, &tmp9);
      ca_.Goto(&block12);
      ca_.Bind(&catch8_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 332);
    compiler::CodeAssemblerExceptionHandlerLabel catch12__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch12__label);
    std::tie(tmp10, tmp11) = IteratorBuiltinsAssembler(state_).GetIterator(TNode<Context>{p_context}, TNode<Object>{p_iterable}).Flatten();
    }
    if (catch12__label.is_used()) {
      compiler::CodeAssemblerLabel catch12_skip(&ca_);
      ca_.Goto(&catch12_skip);
      ca_.Bind(&catch12__label, &tmp13);
      ca_.Goto(&block13);
      ca_.Bind(&catch12_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 339);
    compiler::CodeAssemblerLabel label15(&ca_);
    compiler::CodeAssemblerExceptionHandlerLabel catch17__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch17__label);
    tmp14 = PerformPromiseAll_PromiseAllSettledResolveElementFunctor_PromiseAllSettledRejectElementFunctor_0(state_, TNode<Context>{p_context}, TNode<NativeContext>{tmp0}, TorqueStructIteratorRecord{TNode<JSReceiver>{tmp10}, TNode<Object>{tmp11}}, TNode<JSReceiver>{tmp6}, TNode<PromiseCapability>{tmp4}, TNode<Object>{tmp7}, TorqueStructPromiseAllSettledResolveElementFunctor_0{}, TorqueStructPromiseAllSettledRejectElementFunctor_0{}, &label15, &tmp16);
    }
    if (catch17__label.is_used()) {
      compiler::CodeAssemblerLabel catch17_skip(&ca_);
      ca_.Goto(&catch17_skip);
      ca_.Bind(&catch17__label, &tmp18);
      ca_.Goto(&block16);
      ca_.Bind(&catch17_skip);
    }
    ca_.Goto(&block14);
    if (label15.is_used()) {
      ca_.Bind(&label15);
      ca_.Goto(&block15);
    }
  }

  if (block12.is_used()) {
    ca_.Bind(&block12);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 328);
    ca_.Goto(&block11, tmp9);
  }

  if (block13.is_used()) {
    ca_.Bind(&block13);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 332);
    ca_.Goto(&block11, tmp13);
  }

  if (block16.is_used()) {
    ca_.Bind(&block16);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 339);
    ca_.Goto(&block11, tmp18);
  }

  if (block15.is_used()) {
    ca_.Bind(&block15);
    ca_.Goto(&block9, tmp16.value());
  }

  if (block14.is_used()) {
    ca_.Bind(&block14);
    ca_.Goto(&block1, tmp14);
  }

  TNode<Object> phi_bb11_7;
  if (block11.is_used()) {
    ca_.Bind(&block11, &phi_bb11_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 344);
    ca_.Goto(&block9, phi_bb11_7);
  }

  TNode<Object> phi_bb9_7;
  TNode<Object> tmp19;
  TNode<IntPtrT> tmp20;
  TNode<Object> tmp21;
  TNode<Object> tmp22;
  TNode<Oddball> tmp23;
  TNode<Object> tmp24;
  TNode<IntPtrT> tmp25;
  TNode<HeapObject> tmp26;
  if (block9.is_used()) {
    ca_.Bind(&block9, &phi_bb9_7);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 347);
    tmp19 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{phi_bb9_7});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 348);
    tmp20 = FromConstexpr_intptr_constexpr_int31_0(state_, 12);
    tmp21 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp4, tmp20});
    tmp22 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp21});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 349);
    tmp23 = Undefined_0(state_);
    tmp24 = CodeStubAssembler(state_).Call(TNode<Context>{p_context}, TNode<Object>{tmp22}, TNode<Object>{tmp23}, TNode<Object>{tmp19});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 350);
    tmp25 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp26 = CodeStubAssembler(state_).LoadReference<HeapObject>(CodeStubAssembler::Reference{tmp4, tmp25});
    ca_.Goto(&block1, tmp26);
  }

  TNode<Object> phi_bb1_3;
  if (block1.is_used()) {
    ca_.Bind(&block1, &phi_bb1_3);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 366);
    ca_.Goto(&block17);
  }

    ca_.Bind(&block17);
  return TNode<Object>{phi_bb1_3};
}

TNode<Object> PerformPromiseAll_PromiseAllResolveElementFunctor_PromiseAllRejectElementFunctor_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<NativeContext> p_nativeContext, TorqueStructIteratorRecord p_iter, TNode<JSReceiver> p_constructor, TNode<PromiseCapability> p_capability, TNode<Object> p_promiseResolveFunction, TorqueStructPromiseAllResolveElementFunctor_0 p_createResolveElementFunctor, TorqueStructPromiseAllRejectElementFunctor_0 p_createRejectElementFunctor, compiler::CodeAssemblerLabel* label_Reject, compiler::TypedCodeAssemblerVariable<Object>* label_Reject_parameter_0) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block9(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block12(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block17(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block18(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block20(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block23(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block24(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block21(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block31(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block30(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block29(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block32(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Object> block26(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block35(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block36(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block33(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block37(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block38(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block34(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block39(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block40(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block41(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block46(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block47(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block49(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block50(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block51(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block52(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block57(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block58(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block60(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block61(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block66(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block67(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block70(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block68(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block71(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, BoolT> block69(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block72(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block75(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block73(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block76(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, BoolT> block74(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block77(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block80(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block78(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block81(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, BoolT> block79(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block82(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block85(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block83(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block86(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block87(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block88(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block89(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, BoolT> block84(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block64(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block91(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block92(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block93(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block96(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block97(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block100(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block98(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block101(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, BoolT> block99(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block94(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block102(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block95(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block65(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block103(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block104(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block90(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block105(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block106(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block22(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Object> block8(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block111(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block112(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block118(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block119(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block121(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block128(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block129(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block131(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block137(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block138(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, FixedArray> block132(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block122(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block141(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block140(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block146(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block147(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block153(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block154(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block123(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block156(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<HeapObject> tmp1;
  TNode<IntPtrT> tmp2;
  TNode<Object> tmp3;
  TNode<IntPtrT> tmp4;
  TNode<Object> tmp5;
  TNode<BoolT> tmp6;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 120);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp1 = CodeStubAssembler(state_).LoadReference<HeapObject>(CodeStubAssembler::Reference{p_capability, tmp0});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 121);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp3 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{p_capability, tmp2});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 122);
    tmp4 = FromConstexpr_intptr_constexpr_int31_0(state_, 12);
    tmp5 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{p_capability, tmp4});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 126);
    tmp6 = CodeStubAssembler(state_).IsDebugActive();
    ca_.Branch(tmp6, &block3, std::vector<Node*>{}, &block4, std::vector<Node*>{});
  }

  TNode<Symbol> tmp7;
  TNode<Oddball> tmp8;
  TNode<Object> tmp9;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 127);
    tmp7 = kPromiseForwardingHandlerSymbol_0(state_);
    tmp8 = True_0(state_);
    tmp9 = CodeStubAssembler(state_).SetPropertyStrict(TNode<Context>{p_context}, TNode<Object>{tmp5}, TNode<Object>{tmp7}, TNode<Object>{tmp8});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 126);
    ca_.Goto(&block4);
  }

  TNode<Context> tmp10;
  TNode<Smi> tmp11;
  TNode<IntPtrT> tmp12;
      TNode<Object> tmp14;
  TNode<IntPtrT> tmp15;
      TNode<Object> tmp17;
  TNode<IntPtrT> tmp18;
      TNode<Object> tmp20;
  TNode<Smi> tmp21;
  TNode<IntPtrT> tmp22;
  TNode<IntPtrT> tmp23;
      TNode<Object> tmp25;
  TNode<UintPtrT> tmp26;
  TNode<UintPtrT> tmp27;
  TNode<BoolT> tmp28;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 131);
    tmp10 = CreatePromiseAllResolveElementContext_0(state_, TNode<Context>{p_context}, TNode<PromiseCapability>{p_capability}, TNode<NativeContext>{p_nativeContext});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 133);
    tmp11 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 137);
    compiler::CodeAssemblerExceptionHandlerLabel catch13__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch13__label);
    tmp12 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    }
    if (catch13__label.is_used()) {
      compiler::CodeAssemblerLabel catch13_skip(&ca_);
      ca_.Goto(&catch13_skip);
      ca_.Bind(&catch13__label, &tmp14);
      ca_.Goto(&block9);
      ca_.Bind(&catch13_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch16__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch16__label);
    tmp15 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    }
    if (catch16__label.is_used()) {
      compiler::CodeAssemblerLabel catch16_skip(&ca_);
      ca_.Goto(&catch16_skip);
      ca_.Bind(&catch16__label, &tmp17);
      ca_.Goto(&block10);
      ca_.Bind(&catch16_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch19__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch19__label);
    tmp18 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    }
    if (catch19__label.is_used()) {
      compiler::CodeAssemblerLabel catch19_skip(&ca_);
      ca_.Goto(&catch19_skip);
      ca_.Bind(&catch19__label, &tmp20);
      ca_.Goto(&block11);
      ca_.Bind(&catch19_skip);
    }
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp21 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_nativeContext, tmp18});
    tmp22 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp21});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 137);
    compiler::CodeAssemblerExceptionHandlerLabel catch24__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch24__label);
    tmp23 = FromConstexpr_intptr_constexpr_intptr_0(state_, Context::Field::ITERATOR_RESULT_MAP_INDEX);
    }
    if (catch24__label.is_used()) {
      compiler::CodeAssemblerLabel catch24_skip(&ca_);
      ca_.Goto(&catch24_skip);
      ca_.Bind(&catch24__label, &tmp25);
      ca_.Goto(&block12);
      ca_.Bind(&catch24_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp26 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp23});
    tmp27 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp22});
    tmp28 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp26}, TNode<UintPtrT>{tmp27});
    ca_.Branch(tmp28, &block17, std::vector<Node*>{}, &block18, std::vector<Node*>{});
  }

  if (block9.is_used()) {
    ca_.Bind(&block9);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 137);
    ca_.Goto(&block8, tmp11, tmp14);
  }

  if (block10.is_used()) {
    ca_.Bind(&block10);
    ca_.Goto(&block8, tmp11, tmp17);
  }

  if (block11.is_used()) {
    ca_.Bind(&block11);
    ca_.Goto(&block8, tmp11, tmp20);
  }

  if (block12.is_used()) {
    ca_.Bind(&block12);
    ca_.Goto(&block8, tmp11, tmp25);
  }

  TNode<IntPtrT> tmp29;
  TNode<IntPtrT> tmp30;
  TNode<IntPtrT> tmp31;
  TNode<HeapObject> tmp32;
  TNode<IntPtrT> tmp33;
  TNode<Object> tmp34;
  TNode<Map> tmp35;
      TNode<Object> tmp37;
  if (block17.is_used()) {
    ca_.Bind(&block17);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp29 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp30 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp23}, TNode<IntPtrT>{tmp29});
    tmp31 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp12}, TNode<IntPtrT>{tmp30});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp32, tmp33) = NewReference_Object_0(state_, TNode<HeapObject>{p_nativeContext}, TNode<IntPtrT>{tmp31}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 137);
    tmp34 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp32, tmp33});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 136);
    compiler::CodeAssemblerExceptionHandlerLabel catch36__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch36__label);
    tmp35 = UnsafeCast_Map_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp34});
    }
    if (catch36__label.is_used()) {
      compiler::CodeAssemblerLabel catch36_skip(&ca_);
      ca_.Goto(&catch36_skip);
      ca_.Bind(&catch36__label, &tmp37);
      ca_.Goto(&block20);
      ca_.Bind(&catch36_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 138);
    ca_.Goto(&block23, tmp11);
  }

  if (block18.is_used()) {
    ca_.Bind(&block18);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  if (block20.is_used()) {
    ca_.Bind(&block20);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 136);
    ca_.Goto(&block8, tmp11, tmp37);
  }

  TNode<Smi> phi_bb23_11;
  TNode<BoolT> tmp38;
      TNode<Object> tmp40;
  if (block23.is_used()) {
    ca_.Bind(&block23, &phi_bb23_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 138);
    compiler::CodeAssemblerExceptionHandlerLabel catch39__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch39__label);
    tmp38 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    }
    if (catch39__label.is_used()) {
      compiler::CodeAssemblerLabel catch39_skip(&ca_);
      ca_.Goto(&catch39_skip);
      ca_.Bind(&catch39__label, &tmp40);
      ca_.Goto(&block24, phi_bb23_11);
      ca_.Bind(&catch39_skip);
    }
    ca_.Branch(tmp38, &block21, std::vector<Node*>{phi_bb23_11}, &block22, std::vector<Node*>{phi_bb23_11});
  }

  TNode<Smi> phi_bb24_11;
  if (block24.is_used()) {
    ca_.Bind(&block24, &phi_bb24_11);
    ca_.Goto(&block8, phi_bb24_11, tmp40);
  }

  TNode<Smi> phi_bb21_11;
  TNode<JSReceiver> tmp41;
      TNode<Object> tmp44;
  if (block21.is_used()) {
    ca_.Bind(&block21, &phi_bb21_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 144);
    compiler::CodeAssemblerLabel label42(&ca_);
    compiler::CodeAssemblerExceptionHandlerLabel catch43__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch43__label);
    tmp41 = IteratorBuiltinsAssembler(state_).IteratorStep(TNode<Context>{p_context}, TorqueStructIteratorRecord{TNode<JSReceiver>{p_iter.object}, TNode<Object>{p_iter.next}}, TNode<Map>{tmp35}, &label42);
    }
    if (catch43__label.is_used()) {
      compiler::CodeAssemblerLabel catch43_skip(&ca_);
      ca_.Goto(&catch43_skip);
      ca_.Bind(&catch43__label, &tmp44);
      ca_.Goto(&block31, phi_bb21_11);
      ca_.Bind(&catch43_skip);
    }
    ca_.Goto(&block29, phi_bb21_11);
    if (label42.is_used()) {
      ca_.Bind(&label42);
      ca_.Goto(&block30, phi_bb21_11);
    }
  }

  TNode<Smi> phi_bb31_11;
  if (block31.is_used()) {
    ca_.Bind(&block31, &phi_bb31_11);
    ca_.Goto(&block26, phi_bb31_11, tmp44);
  }

  TNode<Smi> phi_bb30_11;
  if (block30.is_used()) {
    ca_.Bind(&block30, &phi_bb30_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 135);
    ca_.Goto(&block5, phi_bb30_11);
  }

  TNode<Smi> phi_bb29_11;
  TNode<Object> tmp45;
      TNode<Object> tmp47;
  TNode<Smi> tmp48;
      TNode<Object> tmp50;
  TNode<BoolT> tmp51;
      TNode<Object> tmp53;
  if (block29.is_used()) {
    ca_.Bind(&block29, &phi_bb29_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 151);
    compiler::CodeAssemblerExceptionHandlerLabel catch46__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch46__label);
    tmp45 = IteratorBuiltinsAssembler(state_).IteratorValue(TNode<Context>{p_context}, TNode<JSReceiver>{tmp41}, TNode<Map>{tmp35});
    }
    if (catch46__label.is_used()) {
      compiler::CodeAssemblerLabel catch46_skip(&ca_);
      ca_.Goto(&catch46_skip);
      ca_.Bind(&catch46__label, &tmp47);
      ca_.Goto(&block32, phi_bb29_11);
      ca_.Bind(&catch46_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 157);
    compiler::CodeAssemblerExceptionHandlerLabel catch49__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch49__label);
    tmp48 = FromConstexpr_Smi_constexpr_int31_0(state_, PropertyArray::HashField::kMax);
    }
    if (catch49__label.is_used()) {
      compiler::CodeAssemblerLabel catch49_skip(&ca_);
      ca_.Goto(&catch49_skip);
      ca_.Bind(&catch49__label, &tmp50);
      ca_.Goto(&block35, phi_bb29_11, phi_bb29_11, phi_bb29_11);
      ca_.Bind(&catch49_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch52__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch52__label);
    tmp51 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{phi_bb29_11}, TNode<Smi>{tmp48});
    }
    if (catch52__label.is_used()) {
      compiler::CodeAssemblerLabel catch52_skip(&ca_);
      ca_.Goto(&catch52_skip);
      ca_.Bind(&catch52__label, &tmp53);
      ca_.Goto(&block36, phi_bb29_11, phi_bb29_11);
      ca_.Bind(&catch52_skip);
    }
    ca_.Branch(tmp51, &block33, std::vector<Node*>{phi_bb29_11}, &block34, std::vector<Node*>{phi_bb29_11});
  }

  TNode<Smi> phi_bb32_11;
  if (block32.is_used()) {
    ca_.Bind(&block32, &phi_bb32_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 151);
    ca_.Goto(&block26, phi_bb32_11, tmp47);
  }

  TNode<Smi> phi_bb26_11;
  TNode<Object> phi_bb26_14;
  if (block26.is_used()) {
    ca_.Bind(&block26, &phi_bb26_11, &phi_bb26_14);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 153);
    ca_.Goto(&block1, phi_bb26_14);
  }

  TNode<Smi> phi_bb35_11;
  TNode<Smi> phi_bb35_14;
  TNode<Smi> phi_bb35_15;
  if (block35.is_used()) {
    ca_.Bind(&block35, &phi_bb35_11, &phi_bb35_14, &phi_bb35_15);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 157);
    ca_.Goto(&block8, phi_bb35_11, tmp50);
  }

  TNode<Smi> phi_bb36_11;
  TNode<Smi> phi_bb36_14;
  if (block36.is_used()) {
    ca_.Bind(&block36, &phi_bb36_11, &phi_bb36_14);
    ca_.Goto(&block8, phi_bb36_11, tmp53);
  }

  TNode<Smi> phi_bb33_11;
  TNode<Object> tmp54;
      TNode<Object> tmp56;
      TNode<Object> tmp58;
  if (block33.is_used()) {
    ca_.Bind(&block33, &phi_bb33_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 166);
    compiler::CodeAssemblerExceptionHandlerLabel catch55__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch55__label);
    tmp54 = FromConstexpr_Object_constexpr_string_0(state_, "all");
    }
    if (catch55__label.is_used()) {
      compiler::CodeAssemblerLabel catch55_skip(&ca_);
      ca_.Goto(&catch55_skip);
      ca_.Bind(&catch55__label, &tmp56);
      ca_.Goto(&block37, phi_bb33_11);
      ca_.Bind(&catch55_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch57__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch57__label);
    CodeStubAssembler(state_).ThrowRangeError(TNode<Context>{p_context}, MessageTemplate::kTooManyElementsInPromiseCombinator, TNode<Object>{tmp54});
    }
    if (catch57__label.is_used()) {
      compiler::CodeAssemblerLabel catch57_skip(&ca_);
      ca_.Bind(&catch57__label, &tmp58);
      ca_.Goto(&block38, phi_bb33_11);
    }
  }

  TNode<Smi> phi_bb37_11;
  if (block37.is_used()) {
    ca_.Bind(&block37, &phi_bb37_11);
    ca_.Goto(&block8, phi_bb37_11, tmp56);
  }

  TNode<Smi> phi_bb38_11;
  if (block38.is_used()) {
    ca_.Bind(&block38, &phi_bb38_11);
    ca_.Goto(&block8, phi_bb38_11, tmp58);
  }

  TNode<Smi> phi_bb34_11;
  TNode<IntPtrT> tmp59;
      TNode<Object> tmp61;
  TNode<IntPtrT> tmp62;
      TNode<Object> tmp64;
  TNode<IntPtrT> tmp65;
      TNode<Object> tmp67;
  TNode<Smi> tmp68;
  TNode<IntPtrT> tmp69;
  TNode<IntPtrT> tmp70;
  TNode<UintPtrT> tmp71;
  TNode<UintPtrT> tmp72;
  TNode<BoolT> tmp73;
  if (block34.is_used()) {
    ca_.Bind(&block34, &phi_bb34_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 173);
    compiler::CodeAssemblerExceptionHandlerLabel catch60__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch60__label);
    tmp59 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    }
    if (catch60__label.is_used()) {
      compiler::CodeAssemblerLabel catch60_skip(&ca_);
      ca_.Goto(&catch60_skip);
      ca_.Bind(&catch60__label, &tmp61);
      ca_.Goto(&block39, phi_bb34_11);
      ca_.Bind(&catch60_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch63__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch63__label);
    tmp62 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    }
    if (catch63__label.is_used()) {
      compiler::CodeAssemblerLabel catch63_skip(&ca_);
      ca_.Goto(&catch63_skip);
      ca_.Bind(&catch63__label, &tmp64);
      ca_.Goto(&block40, phi_bb34_11);
      ca_.Bind(&catch63_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch66__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch66__label);
    tmp65 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    }
    if (catch66__label.is_used()) {
      compiler::CodeAssemblerLabel catch66_skip(&ca_);
      ca_.Goto(&catch66_skip);
      ca_.Bind(&catch66__label, &tmp67);
      ca_.Goto(&block41, phi_bb34_11);
      ca_.Bind(&catch66_skip);
    }
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp68 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp10, tmp65});
    tmp69 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp68});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp70 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp71 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp70});
    tmp72 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp69});
    tmp73 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp71}, TNode<UintPtrT>{tmp72});
    ca_.Branch(tmp73, &block46, std::vector<Node*>{phi_bb34_11}, &block47, std::vector<Node*>{phi_bb34_11});
  }

  TNode<Smi> phi_bb39_11;
  if (block39.is_used()) {
    ca_.Bind(&block39, &phi_bb39_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 173);
    ca_.Goto(&block8, phi_bb39_11, tmp61);
  }

  TNode<Smi> phi_bb40_11;
  if (block40.is_used()) {
    ca_.Bind(&block40, &phi_bb40_11);
    ca_.Goto(&block8, phi_bb40_11, tmp64);
  }

  TNode<Smi> phi_bb41_11;
  if (block41.is_used()) {
    ca_.Bind(&block41, &phi_bb41_11);
    ca_.Goto(&block8, phi_bb41_11, tmp67);
  }

  TNode<Smi> phi_bb46_11;
  TNode<IntPtrT> tmp74;
  TNode<IntPtrT> tmp75;
  TNode<IntPtrT> tmp76;
  TNode<HeapObject> tmp77;
  TNode<IntPtrT> tmp78;
  TNode<Object> tmp79;
  TNode<Smi> tmp80;
      TNode<Object> tmp82;
  TNode<IntPtrT> tmp83;
      TNode<Object> tmp85;
  TNode<IntPtrT> tmp86;
      TNode<Object> tmp88;
  TNode<IntPtrT> tmp89;
      TNode<Object> tmp91;
  TNode<Smi> tmp92;
  TNode<IntPtrT> tmp93;
  TNode<IntPtrT> tmp94;
  TNode<UintPtrT> tmp95;
  TNode<UintPtrT> tmp96;
  TNode<BoolT> tmp97;
  if (block46.is_used()) {
    ca_.Bind(&block46, &phi_bb46_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp74 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp75 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp70}, TNode<IntPtrT>{tmp74});
    tmp76 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp59}, TNode<IntPtrT>{tmp75});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp77, tmp78) = NewReference_Object_0(state_, TNode<HeapObject>{tmp10}, TNode<IntPtrT>{tmp76}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 173);
    tmp79 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp77, tmp78});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 172);
    compiler::CodeAssemblerExceptionHandlerLabel catch81__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch81__label);
    tmp80 = UnsafeCast_Smi_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp79});
    }
    if (catch81__label.is_used()) {
      compiler::CodeAssemblerLabel catch81_skip(&ca_);
      ca_.Goto(&catch81_skip);
      ca_.Bind(&catch81__label, &tmp82);
      ca_.Goto(&block49, phi_bb46_11);
      ca_.Bind(&catch81_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 176);
    compiler::CodeAssemblerExceptionHandlerLabel catch84__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch84__label);
    tmp83 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    }
    if (catch84__label.is_used()) {
      compiler::CodeAssemblerLabel catch84_skip(&ca_);
      ca_.Goto(&catch84_skip);
      ca_.Bind(&catch84__label, &tmp85);
      ca_.Goto(&block50, phi_bb46_11);
      ca_.Bind(&catch84_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch87__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch87__label);
    tmp86 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    }
    if (catch87__label.is_used()) {
      compiler::CodeAssemblerLabel catch87_skip(&ca_);
      ca_.Goto(&catch87_skip);
      ca_.Bind(&catch87__label, &tmp88);
      ca_.Goto(&block51, phi_bb46_11);
      ca_.Bind(&catch87_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch90__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch90__label);
    tmp89 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    }
    if (catch90__label.is_used()) {
      compiler::CodeAssemblerLabel catch90_skip(&ca_);
      ca_.Goto(&catch90_skip);
      ca_.Bind(&catch90__label, &tmp91);
      ca_.Goto(&block52, phi_bb46_11);
      ca_.Bind(&catch90_skip);
    }
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp92 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp10, tmp89});
    tmp93 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp92});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp94 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp95 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp94});
    tmp96 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp93});
    tmp97 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp95}, TNode<UintPtrT>{tmp96});
    ca_.Branch(tmp97, &block57, std::vector<Node*>{phi_bb46_11}, &block58, std::vector<Node*>{phi_bb46_11});
  }

  TNode<Smi> phi_bb47_11;
  if (block47.is_used()) {
    ca_.Bind(&block47, &phi_bb47_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb49_11;
  if (block49.is_used()) {
    ca_.Bind(&block49, &phi_bb49_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 172);
    ca_.Goto(&block8, phi_bb49_11, tmp82);
  }

  TNode<Smi> phi_bb50_11;
  if (block50.is_used()) {
    ca_.Bind(&block50, &phi_bb50_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 176);
    ca_.Goto(&block8, phi_bb50_11, tmp85);
  }

  TNode<Smi> phi_bb51_11;
  if (block51.is_used()) {
    ca_.Bind(&block51, &phi_bb51_11);
    ca_.Goto(&block8, phi_bb51_11, tmp88);
  }

  TNode<Smi> phi_bb52_11;
  if (block52.is_used()) {
    ca_.Bind(&block52, &phi_bb52_11);
    ca_.Goto(&block8, phi_bb52_11, tmp91);
  }

  TNode<Smi> phi_bb57_11;
  TNode<IntPtrT> tmp98;
  TNode<IntPtrT> tmp99;
  TNode<IntPtrT> tmp100;
  TNode<HeapObject> tmp101;
  TNode<IntPtrT> tmp102;
  TNode<Smi> tmp103;
      TNode<Object> tmp105;
  TNode<Smi> tmp106;
      TNode<Object> tmp108;
  TNode<SharedFunctionInfo> tmp109;
  TNode<JSFunction> tmp110;
  TNode<IntPtrT> tmp111;
  TNode<Object> tmp112;
  TNode<JSReceiver> tmp113;
  TNode<Oddball> tmp114;
  TNode<BoolT> tmp115;
      TNode<Object> tmp117;
  if (block57.is_used()) {
    ca_.Bind(&block57, &phi_bb57_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp98 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp99 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp94}, TNode<IntPtrT>{tmp98});
    tmp100 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp83}, TNode<IntPtrT>{tmp99});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp101, tmp102) = NewReference_Object_0(state_, TNode<HeapObject>{tmp10}, TNode<IntPtrT>{tmp100}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 179);
    compiler::CodeAssemblerExceptionHandlerLabel catch104__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch104__label);
    tmp103 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    }
    if (catch104__label.is_used()) {
      compiler::CodeAssemblerLabel catch104_skip(&ca_);
      ca_.Goto(&catch104_skip);
      ca_.Bind(&catch104__label, &tmp105);
      ca_.Goto(&block60, phi_bb57_11);
      ca_.Bind(&catch104_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch107__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch107__label);
    tmp106 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp80}, TNode<Smi>{tmp103});
    }
    if (catch107__label.is_used()) {
      compiler::CodeAssemblerLabel catch107_skip(&ca_);
      ca_.Goto(&catch107_skip);
      ca_.Bind(&catch107__label, &tmp108);
      ca_.Goto(&block61, phi_bb57_11);
      ca_.Bind(&catch107_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 176);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp101, tmp102}, tmp106);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 81);
    tmp109 = CodeStubAssembler(state_).PromiseAllResolveElementSharedFunConstant();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 79);
    tmp110 = CreatePromiseAllResolveElementFunction_0(state_, TNode<Context>{p_context}, TNode<Context>{tmp10}, TNode<Smi>{phi_bb57_11}, TNode<NativeContext>{p_nativeContext}, TNode<SharedFunctionInfo>{tmp109});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 89);
    tmp111 = FromConstexpr_intptr_constexpr_int31_0(state_, 12);
    tmp112 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{p_capability, tmp111});
    tmp113 = UnsafeCast_Callable_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp112});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    tmp114 = Undefined_0(state_);
    compiler::CodeAssemblerExceptionHandlerLabel catch116__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch116__label);
    tmp115 = CodeStubAssembler(state_).TaggedNotEqual(TNode<Object>{p_promiseResolveFunction}, TNode<HeapObject>{tmp114});
    }
    if (catch116__label.is_used()) {
      compiler::CodeAssemblerLabel catch116_skip(&ca_);
      ca_.Goto(&catch116_skip);
      ca_.Bind(&catch116__label, &tmp117);
      ca_.Goto(&block66, phi_bb57_11);
      ca_.Bind(&catch116_skip);
    }
    ca_.Branch(tmp115, &block67, std::vector<Node*>{phi_bb57_11}, &block68, std::vector<Node*>{phi_bb57_11});
  }

  TNode<Smi> phi_bb58_11;
  if (block58.is_used()) {
    ca_.Bind(&block58, &phi_bb58_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb60_11;
  if (block60.is_used()) {
    ca_.Bind(&block60, &phi_bb60_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 179);
    ca_.Goto(&block8, phi_bb60_11, tmp105);
  }

  TNode<Smi> phi_bb61_11;
  if (block61.is_used()) {
    ca_.Bind(&block61, &phi_bb61_11);
    ca_.Goto(&block8, phi_bb61_11, tmp108);
  }

  TNode<Smi> phi_bb66_11;
  if (block66.is_used()) {
    ca_.Bind(&block66, &phi_bb66_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Goto(&block8, phi_bb66_11, tmp117);
  }

  TNode<Smi> phi_bb67_11;
  TNode<BoolT> tmp118;
      TNode<Object> tmp120;
  if (block67.is_used()) {
    ca_.Bind(&block67, &phi_bb67_11);
    compiler::CodeAssemblerExceptionHandlerLabel catch119__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch119__label);
    tmp118 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    }
    if (catch119__label.is_used()) {
      compiler::CodeAssemblerLabel catch119_skip(&ca_);
      ca_.Goto(&catch119_skip);
      ca_.Bind(&catch119__label, &tmp120);
      ca_.Goto(&block70, phi_bb67_11);
      ca_.Bind(&catch119_skip);
    }
    ca_.Goto(&block69, phi_bb67_11, tmp118);
  }

  TNode<Smi> phi_bb70_11;
  if (block70.is_used()) {
    ca_.Bind(&block70, &phi_bb70_11);
    ca_.Goto(&block8, phi_bb70_11, tmp120);
  }

  TNode<Smi> phi_bb68_11;
  TNode<BoolT> tmp121;
      TNode<Object> tmp123;
  if (block68.is_used()) {
    ca_.Bind(&block68, &phi_bb68_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 216);
    compiler::CodeAssemblerExceptionHandlerLabel catch122__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch122__label);
    tmp121 = PromiseBuiltinsAssembler(state_).IsPromiseHookEnabledOrDebugIsActiveOrHasAsyncEventDelegate();
    }
    if (catch122__label.is_used()) {
      compiler::CodeAssemblerLabel catch122_skip(&ca_);
      ca_.Goto(&catch122_skip);
      ca_.Bind(&catch122__label, &tmp123);
      ca_.Goto(&block71, phi_bb68_11);
      ca_.Bind(&catch122_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Goto(&block69, phi_bb68_11, tmp121);
  }

  TNode<Smi> phi_bb71_11;
  if (block71.is_used()) {
    ca_.Bind(&block71, &phi_bb71_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 216);
    ca_.Goto(&block8, phi_bb71_11, tmp123);
  }

  TNode<Smi> phi_bb69_11;
  TNode<BoolT> phi_bb69_18;
  if (block69.is_used()) {
    ca_.Bind(&block69, &phi_bb69_11, &phi_bb69_18);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Branch(phi_bb69_18, &block72, std::vector<Node*>{phi_bb69_11}, &block73, std::vector<Node*>{phi_bb69_11});
  }

  TNode<Smi> phi_bb72_11;
  TNode<BoolT> tmp124;
      TNode<Object> tmp126;
  if (block72.is_used()) {
    ca_.Bind(&block72, &phi_bb72_11);
    compiler::CodeAssemblerExceptionHandlerLabel catch125__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch125__label);
    tmp124 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    }
    if (catch125__label.is_used()) {
      compiler::CodeAssemblerLabel catch125_skip(&ca_);
      ca_.Goto(&catch125_skip);
      ca_.Bind(&catch125__label, &tmp126);
      ca_.Goto(&block75, phi_bb72_11);
      ca_.Bind(&catch125_skip);
    }
    ca_.Goto(&block74, phi_bb72_11, tmp124);
  }

  TNode<Smi> phi_bb75_11;
  if (block75.is_used()) {
    ca_.Bind(&block75, &phi_bb75_11);
    ca_.Goto(&block8, phi_bb75_11, tmp126);
  }

  TNode<Smi> phi_bb73_11;
  TNode<BoolT> tmp127;
      TNode<Object> tmp129;
  if (block73.is_used()) {
    ca_.Bind(&block73, &phi_bb73_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 217);
    compiler::CodeAssemblerExceptionHandlerLabel catch128__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch128__label);
    tmp127 = CodeStubAssembler(state_).IsPromiseSpeciesProtectorCellInvalid();
    }
    if (catch128__label.is_used()) {
      compiler::CodeAssemblerLabel catch128_skip(&ca_);
      ca_.Goto(&catch128_skip);
      ca_.Bind(&catch128__label, &tmp129);
      ca_.Goto(&block76, phi_bb73_11);
      ca_.Bind(&catch128_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Goto(&block74, phi_bb73_11, tmp127);
  }

  TNode<Smi> phi_bb76_11;
  if (block76.is_used()) {
    ca_.Bind(&block76, &phi_bb76_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 217);
    ca_.Goto(&block8, phi_bb76_11, tmp129);
  }

  TNode<Smi> phi_bb74_11;
  TNode<BoolT> phi_bb74_18;
  if (block74.is_used()) {
    ca_.Bind(&block74, &phi_bb74_11, &phi_bb74_18);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Branch(phi_bb74_18, &block77, std::vector<Node*>{phi_bb74_11}, &block78, std::vector<Node*>{phi_bb74_11});
  }

  TNode<Smi> phi_bb77_11;
  TNode<BoolT> tmp130;
      TNode<Object> tmp132;
  if (block77.is_used()) {
    ca_.Bind(&block77, &phi_bb77_11);
    compiler::CodeAssemblerExceptionHandlerLabel catch131__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch131__label);
    tmp130 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    }
    if (catch131__label.is_used()) {
      compiler::CodeAssemblerLabel catch131_skip(&ca_);
      ca_.Goto(&catch131_skip);
      ca_.Bind(&catch131__label, &tmp132);
      ca_.Goto(&block80, phi_bb77_11);
      ca_.Bind(&catch131_skip);
    }
    ca_.Goto(&block79, phi_bb77_11, tmp130);
  }

  TNode<Smi> phi_bb80_11;
  if (block80.is_used()) {
    ca_.Bind(&block80, &phi_bb80_11);
    ca_.Goto(&block8, phi_bb80_11, tmp132);
  }

  TNode<Smi> phi_bb78_11;
  TNode<BoolT> tmp133;
      TNode<Object> tmp135;
  if (block78.is_used()) {
    ca_.Bind(&block78, &phi_bb78_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 217);
    compiler::CodeAssemblerExceptionHandlerLabel catch134__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch134__label);
    tmp133 = Is_Smi_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp45});
    }
    if (catch134__label.is_used()) {
      compiler::CodeAssemblerLabel catch134_skip(&ca_);
      ca_.Goto(&catch134_skip);
      ca_.Bind(&catch134__label, &tmp135);
      ca_.Goto(&block81, phi_bb78_11);
      ca_.Bind(&catch134_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Goto(&block79, phi_bb78_11, tmp133);
  }

  TNode<Smi> phi_bb81_11;
  if (block81.is_used()) {
    ca_.Bind(&block81, &phi_bb81_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 217);
    ca_.Goto(&block8, phi_bb81_11, tmp135);
  }

  TNode<Smi> phi_bb79_11;
  TNode<BoolT> phi_bb79_18;
  if (block79.is_used()) {
    ca_.Bind(&block79, &phi_bb79_11, &phi_bb79_18);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Branch(phi_bb79_18, &block82, std::vector<Node*>{phi_bb79_11}, &block83, std::vector<Node*>{phi_bb79_11});
  }

  TNode<Smi> phi_bb82_11;
  TNode<BoolT> tmp136;
      TNode<Object> tmp138;
  if (block82.is_used()) {
    ca_.Bind(&block82, &phi_bb82_11);
    compiler::CodeAssemblerExceptionHandlerLabel catch137__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch137__label);
    tmp136 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    }
    if (catch137__label.is_used()) {
      compiler::CodeAssemblerLabel catch137_skip(&ca_);
      ca_.Goto(&catch137_skip);
      ca_.Bind(&catch137__label, &tmp138);
      ca_.Goto(&block85, phi_bb82_11);
      ca_.Bind(&catch137_skip);
    }
    ca_.Goto(&block84, phi_bb82_11, tmp136);
  }

  TNode<Smi> phi_bb85_11;
  if (block85.is_used()) {
    ca_.Bind(&block85, &phi_bb85_11);
    ca_.Goto(&block8, phi_bb85_11, tmp138);
  }

  TNode<Smi> phi_bb83_11;
  TNode<HeapObject> tmp139;
      TNode<Object> tmp141;
  TNode<IntPtrT> tmp142;
      TNode<Object> tmp144;
  TNode<Map> tmp145;
  TNode<BoolT> tmp146;
      TNode<Object> tmp148;
  TNode<BoolT> tmp149;
      TNode<Object> tmp151;
  if (block83.is_used()) {
    ca_.Bind(&block83, &phi_bb83_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 219);
    compiler::CodeAssemblerExceptionHandlerLabel catch140__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch140__label);
    tmp139 = UnsafeCast_HeapObject_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp45});
    }
    if (catch140__label.is_used()) {
      compiler::CodeAssemblerLabel catch140_skip(&ca_);
      ca_.Goto(&catch140_skip);
      ca_.Bind(&catch140__label, &tmp141);
      ca_.Goto(&block86, phi_bb83_11);
      ca_.Bind(&catch140_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch143__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch143__label);
    tmp142 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    }
    if (catch143__label.is_used()) {
      compiler::CodeAssemblerLabel catch143_skip(&ca_);
      ca_.Goto(&catch143_skip);
      ca_.Bind(&catch143__label, &tmp144);
      ca_.Goto(&block87, phi_bb83_11);
      ca_.Bind(&catch143_skip);
    }
    tmp145 = CodeStubAssembler(state_).LoadReference<Map>(CodeStubAssembler::Reference{tmp139, tmp142});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 218);
    compiler::CodeAssemblerExceptionHandlerLabel catch147__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch147__label);
    tmp146 = IsPromiseThenLookupChainIntact_0(state_, TNode<Context>{p_context}, TNode<NativeContext>{p_nativeContext}, TNode<Map>{tmp145});
    }
    if (catch147__label.is_used()) {
      compiler::CodeAssemblerLabel catch147_skip(&ca_);
      ca_.Goto(&catch147_skip);
      ca_.Bind(&catch147__label, &tmp148);
      ca_.Goto(&block88, phi_bb83_11);
      ca_.Bind(&catch147_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch150__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch150__label);
    tmp149 = CodeStubAssembler(state_).Word32BinaryNot(TNode<BoolT>{tmp146});
    }
    if (catch150__label.is_used()) {
      compiler::CodeAssemblerLabel catch150_skip(&ca_);
      ca_.Goto(&catch150_skip);
      ca_.Bind(&catch150__label, &tmp151);
      ca_.Goto(&block89, phi_bb83_11);
      ca_.Bind(&catch150_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Goto(&block84, phi_bb83_11, tmp149);
  }

  TNode<Smi> phi_bb86_11;
  if (block86.is_used()) {
    ca_.Bind(&block86, &phi_bb86_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 219);
    ca_.Goto(&block8, phi_bb86_11, tmp141);
  }

  TNode<Smi> phi_bb87_11;
  if (block87.is_used()) {
    ca_.Bind(&block87, &phi_bb87_11);
    ca_.Goto(&block8, phi_bb87_11, tmp144);
  }

  TNode<Smi> phi_bb88_11;
  if (block88.is_used()) {
    ca_.Bind(&block88, &phi_bb88_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 218);
    ca_.Goto(&block8, phi_bb88_11, tmp148);
  }

  TNode<Smi> phi_bb89_11;
  if (block89.is_used()) {
    ca_.Bind(&block89, &phi_bb89_11);
    ca_.Goto(&block8, phi_bb89_11, tmp151);
  }

  TNode<Smi> phi_bb84_11;
  TNode<BoolT> phi_bb84_18;
  if (block84.is_used()) {
    ca_.Bind(&block84, &phi_bb84_11, &phi_bb84_18);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Branch(phi_bb84_18, &block64, std::vector<Node*>{phi_bb84_11}, &block65, std::vector<Node*>{phi_bb84_11});
  }

  TNode<Smi> phi_bb64_11;
  TNode<Object> tmp152;
      TNode<Object> tmp154;
  TNode<String> tmp155;
  TNode<Object> tmp156;
      TNode<Object> tmp158;
  TNode<Object> tmp159;
      TNode<Object> tmp161;
  TNode<BoolT> tmp162;
      TNode<Object> tmp164;
  if (block64.is_used()) {
    ca_.Bind(&block64, &phi_bb64_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 223);
    compiler::CodeAssemblerExceptionHandlerLabel catch153__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch153__label);
    tmp152 = CallResolve_0(state_, TNode<Context>{p_context}, TNode<JSReceiver>{p_constructor}, TNode<Object>{p_promiseResolveFunction}, TNode<Object>{tmp45});
    }
    if (catch153__label.is_used()) {
      compiler::CodeAssemblerLabel catch153_skip(&ca_);
      ca_.Goto(&catch153_skip);
      ca_.Bind(&catch153__label, &tmp154);
      ca_.Goto(&block91, phi_bb64_11);
      ca_.Bind(&catch153_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 227);
    tmp155 = kThenString_0(state_);
    compiler::CodeAssemblerExceptionHandlerLabel catch157__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch157__label);
    tmp156 = CodeStubAssembler(state_).GetProperty(TNode<Context>{p_context}, TNode<Object>{tmp152}, TNode<Object>{tmp155});
    }
    if (catch157__label.is_used()) {
      compiler::CodeAssemblerLabel catch157_skip(&ca_);
      ca_.Goto(&catch157_skip);
      ca_.Bind(&catch157__label, &tmp158);
      ca_.Goto(&block92, phi_bb64_11);
      ca_.Bind(&catch157_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 228);
    compiler::CodeAssemblerExceptionHandlerLabel catch160__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch160__label);
    tmp159 = CodeStubAssembler(state_).Call(TNode<Context>{p_nativeContext}, TNode<Object>{tmp156}, TNode<Object>{tmp152}, TNode<Object>{tmp110}, TNode<Object>{tmp113});
    }
    if (catch160__label.is_used()) {
      compiler::CodeAssemblerLabel catch160_skip(&ca_);
      ca_.Goto(&catch160_skip);
      ca_.Bind(&catch160__label, &tmp161);
      ca_.Goto(&block93, phi_bb64_11);
      ca_.Bind(&catch160_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 234);
    compiler::CodeAssemblerExceptionHandlerLabel catch163__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch163__label);
    tmp162 = CodeStubAssembler(state_).IsDebugActive();
    }
    if (catch163__label.is_used()) {
      compiler::CodeAssemblerLabel catch163_skip(&ca_);
      ca_.Goto(&catch163_skip);
      ca_.Bind(&catch163__label, &tmp164);
      ca_.Goto(&block96, phi_bb64_11);
      ca_.Bind(&catch163_skip);
    }
    ca_.Branch(tmp162, &block97, std::vector<Node*>{phi_bb64_11}, &block98, std::vector<Node*>{phi_bb64_11});
  }

  TNode<Smi> phi_bb91_11;
  if (block91.is_used()) {
    ca_.Bind(&block91, &phi_bb91_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 223);
    ca_.Goto(&block8, phi_bb91_11, tmp154);
  }

  TNode<Smi> phi_bb92_11;
  if (block92.is_used()) {
    ca_.Bind(&block92, &phi_bb92_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 227);
    ca_.Goto(&block8, phi_bb92_11, tmp158);
  }

  TNode<Smi> phi_bb93_11;
  if (block93.is_used()) {
    ca_.Bind(&block93, &phi_bb93_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 228);
    ca_.Goto(&block8, phi_bb93_11, tmp161);
  }

  TNode<Smi> phi_bb96_11;
  if (block96.is_used()) {
    ca_.Bind(&block96, &phi_bb96_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 234);
    ca_.Goto(&block8, phi_bb96_11, tmp164);
  }

  TNode<Smi> phi_bb97_11;
  TNode<BoolT> tmp165;
      TNode<Object> tmp167;
  if (block97.is_used()) {
    ca_.Bind(&block97, &phi_bb97_11);
    compiler::CodeAssemblerExceptionHandlerLabel catch166__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch166__label);
    tmp165 = Is_JSPromise_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp159});
    }
    if (catch166__label.is_used()) {
      compiler::CodeAssemblerLabel catch166_skip(&ca_);
      ca_.Goto(&catch166_skip);
      ca_.Bind(&catch166__label, &tmp167);
      ca_.Goto(&block100, phi_bb97_11);
      ca_.Bind(&catch166_skip);
    }
    ca_.Goto(&block99, phi_bb97_11, tmp165);
  }

  TNode<Smi> phi_bb100_11;
  if (block100.is_used()) {
    ca_.Bind(&block100, &phi_bb100_11);
    ca_.Goto(&block8, phi_bb100_11, tmp167);
  }

  TNode<Smi> phi_bb98_11;
  TNode<BoolT> tmp168;
      TNode<Object> tmp170;
  if (block98.is_used()) {
    ca_.Bind(&block98, &phi_bb98_11);
    compiler::CodeAssemblerExceptionHandlerLabel catch169__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch169__label);
    tmp168 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    }
    if (catch169__label.is_used()) {
      compiler::CodeAssemblerLabel catch169_skip(&ca_);
      ca_.Goto(&catch169_skip);
      ca_.Bind(&catch169__label, &tmp170);
      ca_.Goto(&block101, phi_bb98_11);
      ca_.Bind(&catch169_skip);
    }
    ca_.Goto(&block99, phi_bb98_11, tmp168);
  }

  TNode<Smi> phi_bb101_11;
  if (block101.is_used()) {
    ca_.Bind(&block101, &phi_bb101_11);
    ca_.Goto(&block8, phi_bb101_11, tmp170);
  }

  TNode<Smi> phi_bb99_11;
  TNode<BoolT> phi_bb99_21;
  if (block99.is_used()) {
    ca_.Bind(&block99, &phi_bb99_11, &phi_bb99_21);
    ca_.Branch(phi_bb99_21, &block94, std::vector<Node*>{phi_bb99_11}, &block95, std::vector<Node*>{phi_bb99_11});
  }

  TNode<Smi> phi_bb94_11;
  TNode<Symbol> tmp171;
  TNode<Object> tmp172;
      TNode<Object> tmp174;
  if (block94.is_used()) {
    ca_.Bind(&block94, &phi_bb94_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 236);
    tmp171 = kPromiseHandledBySymbol_0(state_);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 235);
    compiler::CodeAssemblerExceptionHandlerLabel catch173__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch173__label);
    tmp172 = CodeStubAssembler(state_).SetPropertyStrict(TNode<Context>{p_context}, TNode<Object>{tmp159}, TNode<Object>{tmp171}, TNode<Object>{tmp1});
    }
    if (catch173__label.is_used()) {
      compiler::CodeAssemblerLabel catch173_skip(&ca_);
      ca_.Goto(&catch173_skip);
      ca_.Bind(&catch173__label, &tmp174);
      ca_.Goto(&block102, phi_bb94_11);
      ca_.Bind(&catch173_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 234);
    ca_.Goto(&block95, phi_bb94_11);
  }

  TNode<Smi> phi_bb102_11;
  if (block102.is_used()) {
    ca_.Bind(&block102, &phi_bb102_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 235);
    ca_.Goto(&block8, phi_bb102_11, tmp174);
  }

  TNode<Smi> phi_bb95_11;
  if (block95.is_used()) {
    ca_.Bind(&block95, &phi_bb95_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Goto(&block90, phi_bb95_11);
  }

  TNode<Smi> phi_bb65_11;
  TNode<JSPromise> tmp175;
      TNode<Object> tmp177;
  TNode<Oddball> tmp178;
      TNode<Object> tmp180;
  if (block65.is_used()) {
    ca_.Bind(&block65, &phi_bb65_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 240);
    compiler::CodeAssemblerExceptionHandlerLabel catch176__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch176__label);
    tmp175 = UnsafeCast_JSPromise_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp45});
    }
    if (catch176__label.is_used()) {
      compiler::CodeAssemblerLabel catch176_skip(&ca_);
      ca_.Goto(&catch176_skip);
      ca_.Bind(&catch176__label, &tmp177);
      ca_.Goto(&block103, phi_bb65_11);
      ca_.Bind(&catch176_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 241);
    tmp178 = Undefined_0(state_);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 239);
    compiler::CodeAssemblerExceptionHandlerLabel catch179__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch179__label);
    PerformPromiseThenImpl_0(state_, TNode<Context>{p_context}, TNode<JSPromise>{tmp175}, TNode<HeapObject>{tmp110}, TNode<HeapObject>{tmp113}, TNode<HeapObject>{tmp178});
    }
    if (catch179__label.is_used()) {
      compiler::CodeAssemblerLabel catch179_skip(&ca_);
      ca_.Goto(&catch179_skip);
      ca_.Bind(&catch179__label, &tmp180);
      ca_.Goto(&block104, phi_bb65_11);
      ca_.Bind(&catch179_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Goto(&block90, phi_bb65_11);
  }

  TNode<Smi> phi_bb103_11;
  if (block103.is_used()) {
    ca_.Bind(&block103, &phi_bb103_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 240);
    ca_.Goto(&block8, phi_bb103_11, tmp177);
  }

  TNode<Smi> phi_bb104_11;
  if (block104.is_used()) {
    ca_.Bind(&block104, &phi_bb104_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 239);
    ca_.Goto(&block8, phi_bb104_11, tmp180);
  }

  TNode<Smi> phi_bb90_11;
  TNode<Smi> tmp181;
      TNode<Object> tmp183;
  TNode<Smi> tmp184;
      TNode<Object> tmp186;
  if (block90.is_used()) {
    ca_.Bind(&block90, &phi_bb90_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 245);
    compiler::CodeAssemblerExceptionHandlerLabel catch182__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch182__label);
    tmp181 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    }
    if (catch182__label.is_used()) {
      compiler::CodeAssemblerLabel catch182_skip(&ca_);
      ca_.Goto(&catch182_skip);
      ca_.Bind(&catch182__label, &tmp183);
      ca_.Goto(&block105, phi_bb90_11, phi_bb90_11, phi_bb90_11);
      ca_.Bind(&catch182_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch185__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch185__label);
    tmp184 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb90_11}, TNode<Smi>{tmp181});
    }
    if (catch185__label.is_used()) {
      compiler::CodeAssemblerLabel catch185_skip(&ca_);
      ca_.Goto(&catch185_skip);
      ca_.Bind(&catch185__label, &tmp186);
      ca_.Goto(&block106, phi_bb90_11, phi_bb90_11);
      ca_.Bind(&catch185_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 138);
    ca_.Goto(&block23, tmp184);
  }

  TNode<Smi> phi_bb105_11;
  TNode<Smi> phi_bb105_17;
  TNode<Smi> phi_bb105_18;
  if (block105.is_used()) {
    ca_.Bind(&block105, &phi_bb105_11, &phi_bb105_17, &phi_bb105_18);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 245);
    ca_.Goto(&block8, phi_bb105_11, tmp183);
  }

  TNode<Smi> phi_bb106_11;
  TNode<Smi> phi_bb106_17;
  if (block106.is_used()) {
    ca_.Bind(&block106, &phi_bb106_11, &phi_bb106_17);
    ca_.Goto(&block8, phi_bb106_11, tmp186);
  }

  TNode<Smi> phi_bb22_11;
  if (block22.is_used()) {
    ca_.Bind(&block22, &phi_bb22_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 250);
    ca_.Goto(&block5, phi_bb22_11);
  }

  TNode<Smi> phi_bb8_11;
  TNode<Object> phi_bb8_12;
  if (block8.is_used()) {
    ca_.Bind(&block8, &phi_bb8_11, &phi_bb8_12);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 248);
    IteratorCloseOnException_0(state_, TNode<Context>{p_context}, TorqueStructIteratorRecord{TNode<JSReceiver>{p_iter.object}, TNode<Object>{p_iter.next}});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 249);
    ca_.Goto(&block1, phi_bb8_12);
  }

  TNode<Smi> phi_bb5_11;
  TNode<IntPtrT> tmp187;
  TNode<IntPtrT> tmp188;
  TNode<IntPtrT> tmp189;
  TNode<Smi> tmp190;
  TNode<IntPtrT> tmp191;
  TNode<IntPtrT> tmp192;
  TNode<UintPtrT> tmp193;
  TNode<UintPtrT> tmp194;
  TNode<BoolT> tmp195;
  if (block5.is_used()) {
    ca_.Bind(&block5, &phi_bb5_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 256);
    tmp187 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp188 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp189 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp190 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp10, tmp189});
    tmp191 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp190});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp192 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp193 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp192});
    tmp194 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp191});
    tmp195 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp193}, TNode<UintPtrT>{tmp194});
    ca_.Branch(tmp195, &block111, std::vector<Node*>{phi_bb5_11}, &block112, std::vector<Node*>{phi_bb5_11});
  }

  TNode<Smi> phi_bb111_11;
  TNode<IntPtrT> tmp196;
  TNode<IntPtrT> tmp197;
  TNode<IntPtrT> tmp198;
  TNode<HeapObject> tmp199;
  TNode<IntPtrT> tmp200;
  TNode<Object> tmp201;
  TNode<Smi> tmp202;
  TNode<Smi> tmp203;
  TNode<Smi> tmp204;
  TNode<IntPtrT> tmp205;
  TNode<IntPtrT> tmp206;
  TNode<IntPtrT> tmp207;
  TNode<Smi> tmp208;
  TNode<IntPtrT> tmp209;
  TNode<IntPtrT> tmp210;
  TNode<UintPtrT> tmp211;
  TNode<UintPtrT> tmp212;
  TNode<BoolT> tmp213;
  if (block111.is_used()) {
    ca_.Bind(&block111, &phi_bb111_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp196 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp197 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp192}, TNode<IntPtrT>{tmp196});
    tmp198 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp187}, TNode<IntPtrT>{tmp197});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp199, tmp200) = NewReference_Object_0(state_, TNode<HeapObject>{tmp10}, TNode<IntPtrT>{tmp198}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 256);
    tmp201 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp199, tmp200});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 255);
    tmp202 = UnsafeCast_Smi_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp201});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 259);
    tmp203 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp204 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp202}, TNode<Smi>{tmp203});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 260);
    tmp205 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp206 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp207 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp208 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp10, tmp207});
    tmp209 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp208});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp210 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp211 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp210});
    tmp212 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp209});
    tmp213 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp211}, TNode<UintPtrT>{tmp212});
    ca_.Branch(tmp213, &block118, std::vector<Node*>{phi_bb111_11}, &block119, std::vector<Node*>{phi_bb111_11});
  }

  TNode<Smi> phi_bb112_11;
  if (block112.is_used()) {
    ca_.Bind(&block112, &phi_bb112_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb118_11;
  TNode<IntPtrT> tmp214;
  TNode<IntPtrT> tmp215;
  TNode<IntPtrT> tmp216;
  TNode<HeapObject> tmp217;
  TNode<IntPtrT> tmp218;
  TNode<Smi> tmp219;
  TNode<BoolT> tmp220;
  if (block118.is_used()) {
    ca_.Bind(&block118, &phi_bb118_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp214 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp215 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp210}, TNode<IntPtrT>{tmp214});
    tmp216 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp205}, TNode<IntPtrT>{tmp215});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp217, tmp218) = NewReference_Object_0(state_, TNode<HeapObject>{tmp10}, TNode<IntPtrT>{tmp216}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 260);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp217, tmp218}, tmp204);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 263);
    tmp219 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp220 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{tmp204}, TNode<Smi>{tmp219});
    ca_.Branch(tmp220, &block121, std::vector<Node*>{phi_bb118_11}, &block122, std::vector<Node*>{phi_bb118_11});
  }

  TNode<Smi> phi_bb119_11;
  if (block119.is_used()) {
    ca_.Bind(&block119, &phi_bb119_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb121_11;
  TNode<IntPtrT> tmp221;
  TNode<IntPtrT> tmp222;
  TNode<IntPtrT> tmp223;
  TNode<Smi> tmp224;
  TNode<IntPtrT> tmp225;
  TNode<IntPtrT> tmp226;
  TNode<UintPtrT> tmp227;
  TNode<UintPtrT> tmp228;
  TNode<BoolT> tmp229;
  if (block121.is_used()) {
    ca_.Bind(&block121, &phi_bb121_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 268);
    tmp221 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp222 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp223 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp224 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp10, tmp223});
    tmp225 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp224});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp226 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementValuesSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp227 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp226});
    tmp228 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp225});
    tmp229 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp227}, TNode<UintPtrT>{tmp228});
    ca_.Branch(tmp229, &block128, std::vector<Node*>{phi_bb121_11}, &block129, std::vector<Node*>{phi_bb121_11});
  }

  TNode<Smi> phi_bb128_11;
  TNode<IntPtrT> tmp230;
  TNode<IntPtrT> tmp231;
  TNode<IntPtrT> tmp232;
  TNode<HeapObject> tmp233;
  TNode<IntPtrT> tmp234;
  TNode<Object> tmp235;
  TNode<FixedArray> tmp236;
  TNode<IntPtrT> tmp237;
  TNode<IntPtrT> tmp238;
  TNode<IntPtrT> tmp239;
  TNode<IntPtrT> tmp240;
  TNode<BoolT> tmp241;
  if (block128.is_used()) {
    ca_.Bind(&block128, &phi_bb128_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp230 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp231 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp226}, TNode<IntPtrT>{tmp230});
    tmp232 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp221}, TNode<IntPtrT>{tmp231});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp233, tmp234) = NewReference_Object_0(state_, TNode<HeapObject>{tmp10}, TNode<IntPtrT>{tmp232}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 268);
    tmp235 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp233, tmp234});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 267);
    tmp236 = UnsafeCast_FixedArray_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp235});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 273);
    tmp237 = CodeStubAssembler(state_).SmiUntag(TNode<Smi>{phi_bb128_11});
    tmp238 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp239 = CodeStubAssembler(state_).IntPtrSub(TNode<IntPtrT>{tmp237}, TNode<IntPtrT>{tmp238});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 275);
    tmp240 = CodeStubAssembler(state_).LoadAndUntagFixedArrayBaseLength(TNode<FixedArrayBase>{tmp236});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 276);
    tmp241 = CodeStubAssembler(state_).IntPtrLessThan(TNode<IntPtrT>{tmp240}, TNode<IntPtrT>{tmp239});
    ca_.Branch(tmp241, &block131, std::vector<Node*>{phi_bb128_11}, &block132, std::vector<Node*>{phi_bb128_11, tmp236});
  }

  TNode<Smi> phi_bb129_11;
  if (block129.is_used()) {
    ca_.Bind(&block129, &phi_bb129_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb131_11;
  TNode<IntPtrT> tmp242;
  TNode<FixedArray> tmp243;
  TNode<IntPtrT> tmp244;
  TNode<IntPtrT> tmp245;
  TNode<IntPtrT> tmp246;
  TNode<Smi> tmp247;
  TNode<IntPtrT> tmp248;
  TNode<IntPtrT> tmp249;
  TNode<UintPtrT> tmp250;
  TNode<UintPtrT> tmp251;
  TNode<BoolT> tmp252;
  if (block131.is_used()) {
    ca_.Bind(&block131, &phi_bb131_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 277);
    tmp242 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp243 = ExtractFixedArray_0(state_, TNode<FixedArray>{tmp236}, TNode<IntPtrT>{tmp242}, TNode<IntPtrT>{tmp240}, TNode<IntPtrT>{tmp239});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 278);
    tmp244 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp245 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp246 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp247 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp10, tmp246});
    tmp248 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp247});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp249 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementValuesSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp250 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp249});
    tmp251 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp248});
    tmp252 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp250}, TNode<UintPtrT>{tmp251});
    ca_.Branch(tmp252, &block137, std::vector<Node*>{phi_bb131_11}, &block138, std::vector<Node*>{phi_bb131_11});
  }

  TNode<Smi> phi_bb137_11;
  TNode<IntPtrT> tmp253;
  TNode<IntPtrT> tmp254;
  TNode<IntPtrT> tmp255;
  TNode<HeapObject> tmp256;
  TNode<IntPtrT> tmp257;
  if (block137.is_used()) {
    ca_.Bind(&block137, &phi_bb137_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp253 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp254 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp249}, TNode<IntPtrT>{tmp253});
    tmp255 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp244}, TNode<IntPtrT>{tmp254});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp256, tmp257) = NewReference_Object_0(state_, TNode<HeapObject>{tmp10}, TNode<IntPtrT>{tmp255}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 278);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp256, tmp257}, tmp243);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 276);
    ca_.Goto(&block132, phi_bb137_11, tmp243);
  }

  TNode<Smi> phi_bb138_11;
  if (block138.is_used()) {
    ca_.Bind(&block138, &phi_bb138_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb132_11;
  TNode<FixedArray> phi_bb132_13;
  if (block132.is_used()) {
    ca_.Bind(&block132, &phi_bb132_11, &phi_bb132_13);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 263);
    ca_.Goto(&block123, phi_bb132_11);
  }

  TNode<Smi> phi_bb122_11;
  TNode<Smi> tmp258;
  TNode<BoolT> tmp259;
  if (block122.is_used()) {
    ca_.Bind(&block122, &phi_bb122_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 284);
    tmp258 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp259 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp204}, TNode<Smi>{tmp258});
    ca_.Branch(tmp259, &block140, std::vector<Node*>{phi_bb122_11}, &block141, std::vector<Node*>{phi_bb122_11});
  }

  TNode<Smi> phi_bb141_11;
  if (block141.is_used()) {
    ca_.Bind(&block141, &phi_bb141_11);
    CodeStubAssembler(state_).FailAssert("Torque assert 'remainingElementsCount == 0' failed", "src/builtins/promise-all.tq", 284);
  }

  TNode<Smi> phi_bb140_11;
  TNode<IntPtrT> tmp260;
  TNode<IntPtrT> tmp261;
  TNode<IntPtrT> tmp262;
  TNode<Smi> tmp263;
  TNode<IntPtrT> tmp264;
  TNode<IntPtrT> tmp265;
  TNode<UintPtrT> tmp266;
  TNode<UintPtrT> tmp267;
  TNode<BoolT> tmp268;
  if (block140.is_used()) {
    ca_.Bind(&block140, &phi_bb140_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 291);
    tmp260 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp261 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp262 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp263 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp10, tmp262});
    tmp264 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp263});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp265 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementValuesSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp266 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp265});
    tmp267 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp264});
    tmp268 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp266}, TNode<UintPtrT>{tmp267});
    ca_.Branch(tmp268, &block146, std::vector<Node*>{phi_bb140_11}, &block147, std::vector<Node*>{phi_bb140_11});
  }

  TNode<Smi> phi_bb146_11;
  TNode<IntPtrT> tmp269;
  TNode<IntPtrT> tmp270;
  TNode<IntPtrT> tmp271;
  TNode<HeapObject> tmp272;
  TNode<IntPtrT> tmp273;
  TNode<Object> tmp274;
  TNode<FixedArray> tmp275;
  TNode<IntPtrT> tmp276;
  TNode<IntPtrT> tmp277;
  TNode<IntPtrT> tmp278;
  TNode<Smi> tmp279;
  TNode<IntPtrT> tmp280;
  TNode<IntPtrT> tmp281;
  TNode<UintPtrT> tmp282;
  TNode<UintPtrT> tmp283;
  TNode<BoolT> tmp284;
  if (block146.is_used()) {
    ca_.Bind(&block146, &phi_bb146_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp269 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp270 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp265}, TNode<IntPtrT>{tmp269});
    tmp271 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp260}, TNode<IntPtrT>{tmp270});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp272, tmp273) = NewReference_Object_0(state_, TNode<HeapObject>{tmp10}, TNode<IntPtrT>{tmp271}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 291);
    tmp274 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp272, tmp273});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 290);
    tmp275 = UnsafeCast_FixedArray_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp274});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 295);
    tmp276 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp277 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp278 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp279 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_nativeContext, tmp278});
    tmp280 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp279});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 295);
    tmp281 = FromConstexpr_intptr_constexpr_intptr_0(state_, Context::Field::JS_ARRAY_PACKED_ELEMENTS_MAP_INDEX);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp282 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp281});
    tmp283 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp280});
    tmp284 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp282}, TNode<UintPtrT>{tmp283});
    ca_.Branch(tmp284, &block153, std::vector<Node*>{phi_bb146_11}, &block154, std::vector<Node*>{phi_bb146_11});
  }

  TNode<Smi> phi_bb147_11;
  if (block147.is_used()) {
    ca_.Bind(&block147, &phi_bb147_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb153_11;
  TNode<IntPtrT> tmp285;
  TNode<IntPtrT> tmp286;
  TNode<IntPtrT> tmp287;
  TNode<HeapObject> tmp288;
  TNode<IntPtrT> tmp289;
  TNode<Object> tmp290;
  TNode<Map> tmp291;
  TNode<JSArray> tmp292;
  TNode<Object> tmp293;
  TNode<Oddball> tmp294;
  TNode<Object> tmp295;
  if (block153.is_used()) {
    ca_.Bind(&block153, &phi_bb153_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp285 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp286 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp281}, TNode<IntPtrT>{tmp285});
    tmp287 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp276}, TNode<IntPtrT>{tmp286});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp288, tmp289) = NewReference_Object_0(state_, TNode<HeapObject>{p_nativeContext}, TNode<IntPtrT>{tmp287}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 295);
    tmp290 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp288, tmp289});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 294);
    tmp291 = UnsafeCast_Map_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp290});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 297);
    tmp292 = NewJSArray_0(state_, TNode<Context>{p_context}, TNode<Map>{tmp291}, TNode<FixedArrayBase>{tmp275});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 298);
    tmp293 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp3});
    tmp294 = Undefined_0(state_);
    tmp295 = CodeStubAssembler(state_).Call(TNode<Context>{p_nativeContext}, TNode<Object>{tmp293}, TNode<Object>{tmp294}, TNode<Object>{tmp292});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 263);
    ca_.Goto(&block123, phi_bb153_11);
  }

  TNode<Smi> phi_bb154_11;
  if (block154.is_used()) {
    ca_.Bind(&block154, &phi_bb154_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb123_11;
  if (block123.is_used()) {
    ca_.Bind(&block123, &phi_bb123_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 339);
    ca_.Goto(&block156);
  }

  TNode<Object> phi_bb1_0;
  if (block1.is_used()) {
    ca_.Bind(&block1, &phi_bb1_0);
    *label_Reject_parameter_0 = phi_bb1_0;
    ca_.Goto(label_Reject);
  }

    ca_.Bind(&block156);
  return TNode<Object>{tmp1};
}

TNode<Object> PerformPromiseAll_PromiseAllSettledResolveElementFunctor_PromiseAllSettledRejectElementFunctor_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<NativeContext> p_nativeContext, TorqueStructIteratorRecord p_iter, TNode<JSReceiver> p_constructor, TNode<PromiseCapability> p_capability, TNode<Object> p_promiseResolveFunction, TorqueStructPromiseAllSettledResolveElementFunctor_0 p_createResolveElementFunctor, TorqueStructPromiseAllSettledRejectElementFunctor_0 p_createRejectElementFunctor, compiler::CodeAssemblerLabel* label_Reject, compiler::TypedCodeAssemblerVariable<Object>* label_Reject_parameter_0) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block9(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block12(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block17(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block18(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block20(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block23(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block24(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block21(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block31(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block30(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block29(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block32(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Object> block26(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block35(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block36(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block33(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block37(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block38(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block34(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block39(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block40(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block41(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block46(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block47(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block49(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block50(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block51(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block52(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block57(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block58(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block60(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block61(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block66(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block67(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block70(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block68(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block71(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, BoolT> block69(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block72(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block75(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block73(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block76(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, BoolT> block74(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block77(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block80(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block78(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block81(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, BoolT> block79(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block82(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block85(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block83(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block86(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block87(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block88(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block89(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, BoolT> block84(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block64(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block91(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block92(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block93(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block96(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block97(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block100(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block98(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block101(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, BoolT> block99(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block94(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block102(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block95(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block65(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block103(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block104(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block90(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block105(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block106(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block22(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Object> block8(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block111(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block112(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block118(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block119(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block121(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block128(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block129(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block131(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block137(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block138(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, FixedArray> block132(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block122(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block141(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block140(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block146(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block147(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block153(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block154(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block123(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block156(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<HeapObject> tmp1;
  TNode<IntPtrT> tmp2;
  TNode<Object> tmp3;
  TNode<IntPtrT> tmp4;
  TNode<Object> tmp5;
  TNode<BoolT> tmp6;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 120);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp1 = CodeStubAssembler(state_).LoadReference<HeapObject>(CodeStubAssembler::Reference{p_capability, tmp0});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 121);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp3 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{p_capability, tmp2});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 122);
    tmp4 = FromConstexpr_intptr_constexpr_int31_0(state_, 12);
    tmp5 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{p_capability, tmp4});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 126);
    tmp6 = CodeStubAssembler(state_).IsDebugActive();
    ca_.Branch(tmp6, &block3, std::vector<Node*>{}, &block4, std::vector<Node*>{});
  }

  TNode<Symbol> tmp7;
  TNode<Oddball> tmp8;
  TNode<Object> tmp9;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 127);
    tmp7 = kPromiseForwardingHandlerSymbol_0(state_);
    tmp8 = True_0(state_);
    tmp9 = CodeStubAssembler(state_).SetPropertyStrict(TNode<Context>{p_context}, TNode<Object>{tmp5}, TNode<Object>{tmp7}, TNode<Object>{tmp8});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 126);
    ca_.Goto(&block4);
  }

  TNode<Context> tmp10;
  TNode<Smi> tmp11;
  TNode<IntPtrT> tmp12;
      TNode<Object> tmp14;
  TNode<IntPtrT> tmp15;
      TNode<Object> tmp17;
  TNode<IntPtrT> tmp18;
      TNode<Object> tmp20;
  TNode<Smi> tmp21;
  TNode<IntPtrT> tmp22;
  TNode<IntPtrT> tmp23;
      TNode<Object> tmp25;
  TNode<UintPtrT> tmp26;
  TNode<UintPtrT> tmp27;
  TNode<BoolT> tmp28;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 131);
    tmp10 = CreatePromiseAllResolveElementContext_0(state_, TNode<Context>{p_context}, TNode<PromiseCapability>{p_capability}, TNode<NativeContext>{p_nativeContext});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 133);
    tmp11 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 137);
    compiler::CodeAssemblerExceptionHandlerLabel catch13__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch13__label);
    tmp12 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    }
    if (catch13__label.is_used()) {
      compiler::CodeAssemblerLabel catch13_skip(&ca_);
      ca_.Goto(&catch13_skip);
      ca_.Bind(&catch13__label, &tmp14);
      ca_.Goto(&block9);
      ca_.Bind(&catch13_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch16__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch16__label);
    tmp15 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    }
    if (catch16__label.is_used()) {
      compiler::CodeAssemblerLabel catch16_skip(&ca_);
      ca_.Goto(&catch16_skip);
      ca_.Bind(&catch16__label, &tmp17);
      ca_.Goto(&block10);
      ca_.Bind(&catch16_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch19__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch19__label);
    tmp18 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    }
    if (catch19__label.is_used()) {
      compiler::CodeAssemblerLabel catch19_skip(&ca_);
      ca_.Goto(&catch19_skip);
      ca_.Bind(&catch19__label, &tmp20);
      ca_.Goto(&block11);
      ca_.Bind(&catch19_skip);
    }
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp21 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_nativeContext, tmp18});
    tmp22 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp21});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 137);
    compiler::CodeAssemblerExceptionHandlerLabel catch24__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch24__label);
    tmp23 = FromConstexpr_intptr_constexpr_intptr_0(state_, Context::Field::ITERATOR_RESULT_MAP_INDEX);
    }
    if (catch24__label.is_used()) {
      compiler::CodeAssemblerLabel catch24_skip(&ca_);
      ca_.Goto(&catch24_skip);
      ca_.Bind(&catch24__label, &tmp25);
      ca_.Goto(&block12);
      ca_.Bind(&catch24_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp26 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp23});
    tmp27 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp22});
    tmp28 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp26}, TNode<UintPtrT>{tmp27});
    ca_.Branch(tmp28, &block17, std::vector<Node*>{}, &block18, std::vector<Node*>{});
  }

  if (block9.is_used()) {
    ca_.Bind(&block9);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 137);
    ca_.Goto(&block8, tmp11, tmp14);
  }

  if (block10.is_used()) {
    ca_.Bind(&block10);
    ca_.Goto(&block8, tmp11, tmp17);
  }

  if (block11.is_used()) {
    ca_.Bind(&block11);
    ca_.Goto(&block8, tmp11, tmp20);
  }

  if (block12.is_used()) {
    ca_.Bind(&block12);
    ca_.Goto(&block8, tmp11, tmp25);
  }

  TNode<IntPtrT> tmp29;
  TNode<IntPtrT> tmp30;
  TNode<IntPtrT> tmp31;
  TNode<HeapObject> tmp32;
  TNode<IntPtrT> tmp33;
  TNode<Object> tmp34;
  TNode<Map> tmp35;
      TNode<Object> tmp37;
  if (block17.is_used()) {
    ca_.Bind(&block17);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp29 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp30 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp23}, TNode<IntPtrT>{tmp29});
    tmp31 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp12}, TNode<IntPtrT>{tmp30});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp32, tmp33) = NewReference_Object_0(state_, TNode<HeapObject>{p_nativeContext}, TNode<IntPtrT>{tmp31}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 137);
    tmp34 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp32, tmp33});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 136);
    compiler::CodeAssemblerExceptionHandlerLabel catch36__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch36__label);
    tmp35 = UnsafeCast_Map_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp34});
    }
    if (catch36__label.is_used()) {
      compiler::CodeAssemblerLabel catch36_skip(&ca_);
      ca_.Goto(&catch36_skip);
      ca_.Bind(&catch36__label, &tmp37);
      ca_.Goto(&block20);
      ca_.Bind(&catch36_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 138);
    ca_.Goto(&block23, tmp11);
  }

  if (block18.is_used()) {
    ca_.Bind(&block18);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  if (block20.is_used()) {
    ca_.Bind(&block20);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 136);
    ca_.Goto(&block8, tmp11, tmp37);
  }

  TNode<Smi> phi_bb23_11;
  TNode<BoolT> tmp38;
      TNode<Object> tmp40;
  if (block23.is_used()) {
    ca_.Bind(&block23, &phi_bb23_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 138);
    compiler::CodeAssemblerExceptionHandlerLabel catch39__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch39__label);
    tmp38 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    }
    if (catch39__label.is_used()) {
      compiler::CodeAssemblerLabel catch39_skip(&ca_);
      ca_.Goto(&catch39_skip);
      ca_.Bind(&catch39__label, &tmp40);
      ca_.Goto(&block24, phi_bb23_11);
      ca_.Bind(&catch39_skip);
    }
    ca_.Branch(tmp38, &block21, std::vector<Node*>{phi_bb23_11}, &block22, std::vector<Node*>{phi_bb23_11});
  }

  TNode<Smi> phi_bb24_11;
  if (block24.is_used()) {
    ca_.Bind(&block24, &phi_bb24_11);
    ca_.Goto(&block8, phi_bb24_11, tmp40);
  }

  TNode<Smi> phi_bb21_11;
  TNode<JSReceiver> tmp41;
      TNode<Object> tmp44;
  if (block21.is_used()) {
    ca_.Bind(&block21, &phi_bb21_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 144);
    compiler::CodeAssemblerLabel label42(&ca_);
    compiler::CodeAssemblerExceptionHandlerLabel catch43__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch43__label);
    tmp41 = IteratorBuiltinsAssembler(state_).IteratorStep(TNode<Context>{p_context}, TorqueStructIteratorRecord{TNode<JSReceiver>{p_iter.object}, TNode<Object>{p_iter.next}}, TNode<Map>{tmp35}, &label42);
    }
    if (catch43__label.is_used()) {
      compiler::CodeAssemblerLabel catch43_skip(&ca_);
      ca_.Goto(&catch43_skip);
      ca_.Bind(&catch43__label, &tmp44);
      ca_.Goto(&block31, phi_bb21_11);
      ca_.Bind(&catch43_skip);
    }
    ca_.Goto(&block29, phi_bb21_11);
    if (label42.is_used()) {
      ca_.Bind(&label42);
      ca_.Goto(&block30, phi_bb21_11);
    }
  }

  TNode<Smi> phi_bb31_11;
  if (block31.is_used()) {
    ca_.Bind(&block31, &phi_bb31_11);
    ca_.Goto(&block26, phi_bb31_11, tmp44);
  }

  TNode<Smi> phi_bb30_11;
  if (block30.is_used()) {
    ca_.Bind(&block30, &phi_bb30_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 135);
    ca_.Goto(&block5, phi_bb30_11);
  }

  TNode<Smi> phi_bb29_11;
  TNode<Object> tmp45;
      TNode<Object> tmp47;
  TNode<Smi> tmp48;
      TNode<Object> tmp50;
  TNode<BoolT> tmp51;
      TNode<Object> tmp53;
  if (block29.is_used()) {
    ca_.Bind(&block29, &phi_bb29_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 151);
    compiler::CodeAssemblerExceptionHandlerLabel catch46__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch46__label);
    tmp45 = IteratorBuiltinsAssembler(state_).IteratorValue(TNode<Context>{p_context}, TNode<JSReceiver>{tmp41}, TNode<Map>{tmp35});
    }
    if (catch46__label.is_used()) {
      compiler::CodeAssemblerLabel catch46_skip(&ca_);
      ca_.Goto(&catch46_skip);
      ca_.Bind(&catch46__label, &tmp47);
      ca_.Goto(&block32, phi_bb29_11);
      ca_.Bind(&catch46_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 157);
    compiler::CodeAssemblerExceptionHandlerLabel catch49__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch49__label);
    tmp48 = FromConstexpr_Smi_constexpr_int31_0(state_, PropertyArray::HashField::kMax);
    }
    if (catch49__label.is_used()) {
      compiler::CodeAssemblerLabel catch49_skip(&ca_);
      ca_.Goto(&catch49_skip);
      ca_.Bind(&catch49__label, &tmp50);
      ca_.Goto(&block35, phi_bb29_11, phi_bb29_11, phi_bb29_11);
      ca_.Bind(&catch49_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch52__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch52__label);
    tmp51 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{phi_bb29_11}, TNode<Smi>{tmp48});
    }
    if (catch52__label.is_used()) {
      compiler::CodeAssemblerLabel catch52_skip(&ca_);
      ca_.Goto(&catch52_skip);
      ca_.Bind(&catch52__label, &tmp53);
      ca_.Goto(&block36, phi_bb29_11, phi_bb29_11);
      ca_.Bind(&catch52_skip);
    }
    ca_.Branch(tmp51, &block33, std::vector<Node*>{phi_bb29_11}, &block34, std::vector<Node*>{phi_bb29_11});
  }

  TNode<Smi> phi_bb32_11;
  if (block32.is_used()) {
    ca_.Bind(&block32, &phi_bb32_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 151);
    ca_.Goto(&block26, phi_bb32_11, tmp47);
  }

  TNode<Smi> phi_bb26_11;
  TNode<Object> phi_bb26_14;
  if (block26.is_used()) {
    ca_.Bind(&block26, &phi_bb26_11, &phi_bb26_14);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 153);
    ca_.Goto(&block1, phi_bb26_14);
  }

  TNode<Smi> phi_bb35_11;
  TNode<Smi> phi_bb35_14;
  TNode<Smi> phi_bb35_15;
  if (block35.is_used()) {
    ca_.Bind(&block35, &phi_bb35_11, &phi_bb35_14, &phi_bb35_15);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 157);
    ca_.Goto(&block8, phi_bb35_11, tmp50);
  }

  TNode<Smi> phi_bb36_11;
  TNode<Smi> phi_bb36_14;
  if (block36.is_used()) {
    ca_.Bind(&block36, &phi_bb36_11, &phi_bb36_14);
    ca_.Goto(&block8, phi_bb36_11, tmp53);
  }

  TNode<Smi> phi_bb33_11;
  TNode<Object> tmp54;
      TNode<Object> tmp56;
      TNode<Object> tmp58;
  if (block33.is_used()) {
    ca_.Bind(&block33, &phi_bb33_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 166);
    compiler::CodeAssemblerExceptionHandlerLabel catch55__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch55__label);
    tmp54 = FromConstexpr_Object_constexpr_string_0(state_, "all");
    }
    if (catch55__label.is_used()) {
      compiler::CodeAssemblerLabel catch55_skip(&ca_);
      ca_.Goto(&catch55_skip);
      ca_.Bind(&catch55__label, &tmp56);
      ca_.Goto(&block37, phi_bb33_11);
      ca_.Bind(&catch55_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch57__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch57__label);
    CodeStubAssembler(state_).ThrowRangeError(TNode<Context>{p_context}, MessageTemplate::kTooManyElementsInPromiseCombinator, TNode<Object>{tmp54});
    }
    if (catch57__label.is_used()) {
      compiler::CodeAssemblerLabel catch57_skip(&ca_);
      ca_.Bind(&catch57__label, &tmp58);
      ca_.Goto(&block38, phi_bb33_11);
    }
  }

  TNode<Smi> phi_bb37_11;
  if (block37.is_used()) {
    ca_.Bind(&block37, &phi_bb37_11);
    ca_.Goto(&block8, phi_bb37_11, tmp56);
  }

  TNode<Smi> phi_bb38_11;
  if (block38.is_used()) {
    ca_.Bind(&block38, &phi_bb38_11);
    ca_.Goto(&block8, phi_bb38_11, tmp58);
  }

  TNode<Smi> phi_bb34_11;
  TNode<IntPtrT> tmp59;
      TNode<Object> tmp61;
  TNode<IntPtrT> tmp62;
      TNode<Object> tmp64;
  TNode<IntPtrT> tmp65;
      TNode<Object> tmp67;
  TNode<Smi> tmp68;
  TNode<IntPtrT> tmp69;
  TNode<IntPtrT> tmp70;
  TNode<UintPtrT> tmp71;
  TNode<UintPtrT> tmp72;
  TNode<BoolT> tmp73;
  if (block34.is_used()) {
    ca_.Bind(&block34, &phi_bb34_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 173);
    compiler::CodeAssemblerExceptionHandlerLabel catch60__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch60__label);
    tmp59 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    }
    if (catch60__label.is_used()) {
      compiler::CodeAssemblerLabel catch60_skip(&ca_);
      ca_.Goto(&catch60_skip);
      ca_.Bind(&catch60__label, &tmp61);
      ca_.Goto(&block39, phi_bb34_11);
      ca_.Bind(&catch60_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch63__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch63__label);
    tmp62 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    }
    if (catch63__label.is_used()) {
      compiler::CodeAssemblerLabel catch63_skip(&ca_);
      ca_.Goto(&catch63_skip);
      ca_.Bind(&catch63__label, &tmp64);
      ca_.Goto(&block40, phi_bb34_11);
      ca_.Bind(&catch63_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch66__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch66__label);
    tmp65 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    }
    if (catch66__label.is_used()) {
      compiler::CodeAssemblerLabel catch66_skip(&ca_);
      ca_.Goto(&catch66_skip);
      ca_.Bind(&catch66__label, &tmp67);
      ca_.Goto(&block41, phi_bb34_11);
      ca_.Bind(&catch66_skip);
    }
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp68 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp10, tmp65});
    tmp69 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp68});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp70 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp71 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp70});
    tmp72 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp69});
    tmp73 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp71}, TNode<UintPtrT>{tmp72});
    ca_.Branch(tmp73, &block46, std::vector<Node*>{phi_bb34_11}, &block47, std::vector<Node*>{phi_bb34_11});
  }

  TNode<Smi> phi_bb39_11;
  if (block39.is_used()) {
    ca_.Bind(&block39, &phi_bb39_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 173);
    ca_.Goto(&block8, phi_bb39_11, tmp61);
  }

  TNode<Smi> phi_bb40_11;
  if (block40.is_used()) {
    ca_.Bind(&block40, &phi_bb40_11);
    ca_.Goto(&block8, phi_bb40_11, tmp64);
  }

  TNode<Smi> phi_bb41_11;
  if (block41.is_used()) {
    ca_.Bind(&block41, &phi_bb41_11);
    ca_.Goto(&block8, phi_bb41_11, tmp67);
  }

  TNode<Smi> phi_bb46_11;
  TNode<IntPtrT> tmp74;
  TNode<IntPtrT> tmp75;
  TNode<IntPtrT> tmp76;
  TNode<HeapObject> tmp77;
  TNode<IntPtrT> tmp78;
  TNode<Object> tmp79;
  TNode<Smi> tmp80;
      TNode<Object> tmp82;
  TNode<IntPtrT> tmp83;
      TNode<Object> tmp85;
  TNode<IntPtrT> tmp86;
      TNode<Object> tmp88;
  TNode<IntPtrT> tmp89;
      TNode<Object> tmp91;
  TNode<Smi> tmp92;
  TNode<IntPtrT> tmp93;
  TNode<IntPtrT> tmp94;
  TNode<UintPtrT> tmp95;
  TNode<UintPtrT> tmp96;
  TNode<BoolT> tmp97;
  if (block46.is_used()) {
    ca_.Bind(&block46, &phi_bb46_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp74 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp75 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp70}, TNode<IntPtrT>{tmp74});
    tmp76 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp59}, TNode<IntPtrT>{tmp75});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp77, tmp78) = NewReference_Object_0(state_, TNode<HeapObject>{tmp10}, TNode<IntPtrT>{tmp76}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 173);
    tmp79 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp77, tmp78});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 172);
    compiler::CodeAssemblerExceptionHandlerLabel catch81__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch81__label);
    tmp80 = UnsafeCast_Smi_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp79});
    }
    if (catch81__label.is_used()) {
      compiler::CodeAssemblerLabel catch81_skip(&ca_);
      ca_.Goto(&catch81_skip);
      ca_.Bind(&catch81__label, &tmp82);
      ca_.Goto(&block49, phi_bb46_11);
      ca_.Bind(&catch81_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 176);
    compiler::CodeAssemblerExceptionHandlerLabel catch84__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch84__label);
    tmp83 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    }
    if (catch84__label.is_used()) {
      compiler::CodeAssemblerLabel catch84_skip(&ca_);
      ca_.Goto(&catch84_skip);
      ca_.Bind(&catch84__label, &tmp85);
      ca_.Goto(&block50, phi_bb46_11);
      ca_.Bind(&catch84_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch87__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch87__label);
    tmp86 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    }
    if (catch87__label.is_used()) {
      compiler::CodeAssemblerLabel catch87_skip(&ca_);
      ca_.Goto(&catch87_skip);
      ca_.Bind(&catch87__label, &tmp88);
      ca_.Goto(&block51, phi_bb46_11);
      ca_.Bind(&catch87_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch90__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch90__label);
    tmp89 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    }
    if (catch90__label.is_used()) {
      compiler::CodeAssemblerLabel catch90_skip(&ca_);
      ca_.Goto(&catch90_skip);
      ca_.Bind(&catch90__label, &tmp91);
      ca_.Goto(&block52, phi_bb46_11);
      ca_.Bind(&catch90_skip);
    }
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp92 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp10, tmp89});
    tmp93 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp92});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp94 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp95 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp94});
    tmp96 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp93});
    tmp97 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp95}, TNode<UintPtrT>{tmp96});
    ca_.Branch(tmp97, &block57, std::vector<Node*>{phi_bb46_11}, &block58, std::vector<Node*>{phi_bb46_11});
  }

  TNode<Smi> phi_bb47_11;
  if (block47.is_used()) {
    ca_.Bind(&block47, &phi_bb47_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb49_11;
  if (block49.is_used()) {
    ca_.Bind(&block49, &phi_bb49_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 172);
    ca_.Goto(&block8, phi_bb49_11, tmp82);
  }

  TNode<Smi> phi_bb50_11;
  if (block50.is_used()) {
    ca_.Bind(&block50, &phi_bb50_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 176);
    ca_.Goto(&block8, phi_bb50_11, tmp85);
  }

  TNode<Smi> phi_bb51_11;
  if (block51.is_used()) {
    ca_.Bind(&block51, &phi_bb51_11);
    ca_.Goto(&block8, phi_bb51_11, tmp88);
  }

  TNode<Smi> phi_bb52_11;
  if (block52.is_used()) {
    ca_.Bind(&block52, &phi_bb52_11);
    ca_.Goto(&block8, phi_bb52_11, tmp91);
  }

  TNode<Smi> phi_bb57_11;
  TNode<IntPtrT> tmp98;
  TNode<IntPtrT> tmp99;
  TNode<IntPtrT> tmp100;
  TNode<HeapObject> tmp101;
  TNode<IntPtrT> tmp102;
  TNode<Smi> tmp103;
      TNode<Object> tmp105;
  TNode<Smi> tmp106;
      TNode<Object> tmp108;
  TNode<SharedFunctionInfo> tmp109;
  TNode<JSFunction> tmp110;
  TNode<SharedFunctionInfo> tmp111;
  TNode<JSFunction> tmp112;
  TNode<Oddball> tmp113;
  TNode<BoolT> tmp114;
      TNode<Object> tmp116;
  if (block57.is_used()) {
    ca_.Bind(&block57, &phi_bb57_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp98 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp99 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp94}, TNode<IntPtrT>{tmp98});
    tmp100 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp83}, TNode<IntPtrT>{tmp99});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp101, tmp102) = NewReference_Object_0(state_, TNode<HeapObject>{tmp10}, TNode<IntPtrT>{tmp100}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 179);
    compiler::CodeAssemblerExceptionHandlerLabel catch104__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch104__label);
    tmp103 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    }
    if (catch104__label.is_used()) {
      compiler::CodeAssemblerLabel catch104_skip(&ca_);
      ca_.Goto(&catch104_skip);
      ca_.Bind(&catch104__label, &tmp105);
      ca_.Goto(&block60, phi_bb57_11);
      ca_.Bind(&catch104_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch107__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch107__label);
    tmp106 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp80}, TNode<Smi>{tmp103});
    }
    if (catch107__label.is_used()) {
      compiler::CodeAssemblerLabel catch107_skip(&ca_);
      ca_.Goto(&catch107_skip);
      ca_.Bind(&catch107__label, &tmp108);
      ca_.Goto(&block61, phi_bb57_11);
      ca_.Bind(&catch107_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 176);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp101, tmp102}, tmp106);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 99);
    tmp109 = CodeStubAssembler(state_).PromiseAllSettledResolveElementSharedFunConstant();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 97);
    tmp110 = CreatePromiseAllResolveElementFunction_0(state_, TNode<Context>{p_context}, TNode<Context>{tmp10}, TNode<Smi>{phi_bb57_11}, TNode<NativeContext>{p_nativeContext}, TNode<SharedFunctionInfo>{tmp109});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 109);
    tmp111 = CodeStubAssembler(state_).PromiseAllSettledRejectElementSharedFunConstant();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 107);
    tmp112 = CreatePromiseAllResolveElementFunction_0(state_, TNode<Context>{p_context}, TNode<Context>{tmp10}, TNode<Smi>{phi_bb57_11}, TNode<NativeContext>{p_nativeContext}, TNode<SharedFunctionInfo>{tmp111});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    tmp113 = Undefined_0(state_);
    compiler::CodeAssemblerExceptionHandlerLabel catch115__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch115__label);
    tmp114 = CodeStubAssembler(state_).TaggedNotEqual(TNode<Object>{p_promiseResolveFunction}, TNode<HeapObject>{tmp113});
    }
    if (catch115__label.is_used()) {
      compiler::CodeAssemblerLabel catch115_skip(&ca_);
      ca_.Goto(&catch115_skip);
      ca_.Bind(&catch115__label, &tmp116);
      ca_.Goto(&block66, phi_bb57_11);
      ca_.Bind(&catch115_skip);
    }
    ca_.Branch(tmp114, &block67, std::vector<Node*>{phi_bb57_11}, &block68, std::vector<Node*>{phi_bb57_11});
  }

  TNode<Smi> phi_bb58_11;
  if (block58.is_used()) {
    ca_.Bind(&block58, &phi_bb58_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb60_11;
  if (block60.is_used()) {
    ca_.Bind(&block60, &phi_bb60_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 179);
    ca_.Goto(&block8, phi_bb60_11, tmp105);
  }

  TNode<Smi> phi_bb61_11;
  if (block61.is_used()) {
    ca_.Bind(&block61, &phi_bb61_11);
    ca_.Goto(&block8, phi_bb61_11, tmp108);
  }

  TNode<Smi> phi_bb66_11;
  if (block66.is_used()) {
    ca_.Bind(&block66, &phi_bb66_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Goto(&block8, phi_bb66_11, tmp116);
  }

  TNode<Smi> phi_bb67_11;
  TNode<BoolT> tmp117;
      TNode<Object> tmp119;
  if (block67.is_used()) {
    ca_.Bind(&block67, &phi_bb67_11);
    compiler::CodeAssemblerExceptionHandlerLabel catch118__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch118__label);
    tmp117 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    }
    if (catch118__label.is_used()) {
      compiler::CodeAssemblerLabel catch118_skip(&ca_);
      ca_.Goto(&catch118_skip);
      ca_.Bind(&catch118__label, &tmp119);
      ca_.Goto(&block70, phi_bb67_11);
      ca_.Bind(&catch118_skip);
    }
    ca_.Goto(&block69, phi_bb67_11, tmp117);
  }

  TNode<Smi> phi_bb70_11;
  if (block70.is_used()) {
    ca_.Bind(&block70, &phi_bb70_11);
    ca_.Goto(&block8, phi_bb70_11, tmp119);
  }

  TNode<Smi> phi_bb68_11;
  TNode<BoolT> tmp120;
      TNode<Object> tmp122;
  if (block68.is_used()) {
    ca_.Bind(&block68, &phi_bb68_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 216);
    compiler::CodeAssemblerExceptionHandlerLabel catch121__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch121__label);
    tmp120 = PromiseBuiltinsAssembler(state_).IsPromiseHookEnabledOrDebugIsActiveOrHasAsyncEventDelegate();
    }
    if (catch121__label.is_used()) {
      compiler::CodeAssemblerLabel catch121_skip(&ca_);
      ca_.Goto(&catch121_skip);
      ca_.Bind(&catch121__label, &tmp122);
      ca_.Goto(&block71, phi_bb68_11);
      ca_.Bind(&catch121_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Goto(&block69, phi_bb68_11, tmp120);
  }

  TNode<Smi> phi_bb71_11;
  if (block71.is_used()) {
    ca_.Bind(&block71, &phi_bb71_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 216);
    ca_.Goto(&block8, phi_bb71_11, tmp122);
  }

  TNode<Smi> phi_bb69_11;
  TNode<BoolT> phi_bb69_18;
  if (block69.is_used()) {
    ca_.Bind(&block69, &phi_bb69_11, &phi_bb69_18);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Branch(phi_bb69_18, &block72, std::vector<Node*>{phi_bb69_11}, &block73, std::vector<Node*>{phi_bb69_11});
  }

  TNode<Smi> phi_bb72_11;
  TNode<BoolT> tmp123;
      TNode<Object> tmp125;
  if (block72.is_used()) {
    ca_.Bind(&block72, &phi_bb72_11);
    compiler::CodeAssemblerExceptionHandlerLabel catch124__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch124__label);
    tmp123 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    }
    if (catch124__label.is_used()) {
      compiler::CodeAssemblerLabel catch124_skip(&ca_);
      ca_.Goto(&catch124_skip);
      ca_.Bind(&catch124__label, &tmp125);
      ca_.Goto(&block75, phi_bb72_11);
      ca_.Bind(&catch124_skip);
    }
    ca_.Goto(&block74, phi_bb72_11, tmp123);
  }

  TNode<Smi> phi_bb75_11;
  if (block75.is_used()) {
    ca_.Bind(&block75, &phi_bb75_11);
    ca_.Goto(&block8, phi_bb75_11, tmp125);
  }

  TNode<Smi> phi_bb73_11;
  TNode<BoolT> tmp126;
      TNode<Object> tmp128;
  if (block73.is_used()) {
    ca_.Bind(&block73, &phi_bb73_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 217);
    compiler::CodeAssemblerExceptionHandlerLabel catch127__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch127__label);
    tmp126 = CodeStubAssembler(state_).IsPromiseSpeciesProtectorCellInvalid();
    }
    if (catch127__label.is_used()) {
      compiler::CodeAssemblerLabel catch127_skip(&ca_);
      ca_.Goto(&catch127_skip);
      ca_.Bind(&catch127__label, &tmp128);
      ca_.Goto(&block76, phi_bb73_11);
      ca_.Bind(&catch127_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Goto(&block74, phi_bb73_11, tmp126);
  }

  TNode<Smi> phi_bb76_11;
  if (block76.is_used()) {
    ca_.Bind(&block76, &phi_bb76_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 217);
    ca_.Goto(&block8, phi_bb76_11, tmp128);
  }

  TNode<Smi> phi_bb74_11;
  TNode<BoolT> phi_bb74_18;
  if (block74.is_used()) {
    ca_.Bind(&block74, &phi_bb74_11, &phi_bb74_18);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Branch(phi_bb74_18, &block77, std::vector<Node*>{phi_bb74_11}, &block78, std::vector<Node*>{phi_bb74_11});
  }

  TNode<Smi> phi_bb77_11;
  TNode<BoolT> tmp129;
      TNode<Object> tmp131;
  if (block77.is_used()) {
    ca_.Bind(&block77, &phi_bb77_11);
    compiler::CodeAssemblerExceptionHandlerLabel catch130__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch130__label);
    tmp129 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    }
    if (catch130__label.is_used()) {
      compiler::CodeAssemblerLabel catch130_skip(&ca_);
      ca_.Goto(&catch130_skip);
      ca_.Bind(&catch130__label, &tmp131);
      ca_.Goto(&block80, phi_bb77_11);
      ca_.Bind(&catch130_skip);
    }
    ca_.Goto(&block79, phi_bb77_11, tmp129);
  }

  TNode<Smi> phi_bb80_11;
  if (block80.is_used()) {
    ca_.Bind(&block80, &phi_bb80_11);
    ca_.Goto(&block8, phi_bb80_11, tmp131);
  }

  TNode<Smi> phi_bb78_11;
  TNode<BoolT> tmp132;
      TNode<Object> tmp134;
  if (block78.is_used()) {
    ca_.Bind(&block78, &phi_bb78_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 217);
    compiler::CodeAssemblerExceptionHandlerLabel catch133__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch133__label);
    tmp132 = Is_Smi_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp45});
    }
    if (catch133__label.is_used()) {
      compiler::CodeAssemblerLabel catch133_skip(&ca_);
      ca_.Goto(&catch133_skip);
      ca_.Bind(&catch133__label, &tmp134);
      ca_.Goto(&block81, phi_bb78_11);
      ca_.Bind(&catch133_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Goto(&block79, phi_bb78_11, tmp132);
  }

  TNode<Smi> phi_bb81_11;
  if (block81.is_used()) {
    ca_.Bind(&block81, &phi_bb81_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 217);
    ca_.Goto(&block8, phi_bb81_11, tmp134);
  }

  TNode<Smi> phi_bb79_11;
  TNode<BoolT> phi_bb79_18;
  if (block79.is_used()) {
    ca_.Bind(&block79, &phi_bb79_11, &phi_bb79_18);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Branch(phi_bb79_18, &block82, std::vector<Node*>{phi_bb79_11}, &block83, std::vector<Node*>{phi_bb79_11});
  }

  TNode<Smi> phi_bb82_11;
  TNode<BoolT> tmp135;
      TNode<Object> tmp137;
  if (block82.is_used()) {
    ca_.Bind(&block82, &phi_bb82_11);
    compiler::CodeAssemblerExceptionHandlerLabel catch136__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch136__label);
    tmp135 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    }
    if (catch136__label.is_used()) {
      compiler::CodeAssemblerLabel catch136_skip(&ca_);
      ca_.Goto(&catch136_skip);
      ca_.Bind(&catch136__label, &tmp137);
      ca_.Goto(&block85, phi_bb82_11);
      ca_.Bind(&catch136_skip);
    }
    ca_.Goto(&block84, phi_bb82_11, tmp135);
  }

  TNode<Smi> phi_bb85_11;
  if (block85.is_used()) {
    ca_.Bind(&block85, &phi_bb85_11);
    ca_.Goto(&block8, phi_bb85_11, tmp137);
  }

  TNode<Smi> phi_bb83_11;
  TNode<HeapObject> tmp138;
      TNode<Object> tmp140;
  TNode<IntPtrT> tmp141;
      TNode<Object> tmp143;
  TNode<Map> tmp144;
  TNode<BoolT> tmp145;
      TNode<Object> tmp147;
  TNode<BoolT> tmp148;
      TNode<Object> tmp150;
  if (block83.is_used()) {
    ca_.Bind(&block83, &phi_bb83_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 219);
    compiler::CodeAssemblerExceptionHandlerLabel catch139__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch139__label);
    tmp138 = UnsafeCast_HeapObject_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp45});
    }
    if (catch139__label.is_used()) {
      compiler::CodeAssemblerLabel catch139_skip(&ca_);
      ca_.Goto(&catch139_skip);
      ca_.Bind(&catch139__label, &tmp140);
      ca_.Goto(&block86, phi_bb83_11);
      ca_.Bind(&catch139_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch142__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch142__label);
    tmp141 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    }
    if (catch142__label.is_used()) {
      compiler::CodeAssemblerLabel catch142_skip(&ca_);
      ca_.Goto(&catch142_skip);
      ca_.Bind(&catch142__label, &tmp143);
      ca_.Goto(&block87, phi_bb83_11);
      ca_.Bind(&catch142_skip);
    }
    tmp144 = CodeStubAssembler(state_).LoadReference<Map>(CodeStubAssembler::Reference{tmp138, tmp141});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 218);
    compiler::CodeAssemblerExceptionHandlerLabel catch146__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch146__label);
    tmp145 = IsPromiseThenLookupChainIntact_0(state_, TNode<Context>{p_context}, TNode<NativeContext>{p_nativeContext}, TNode<Map>{tmp144});
    }
    if (catch146__label.is_used()) {
      compiler::CodeAssemblerLabel catch146_skip(&ca_);
      ca_.Goto(&catch146_skip);
      ca_.Bind(&catch146__label, &tmp147);
      ca_.Goto(&block88, phi_bb83_11);
      ca_.Bind(&catch146_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch149__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch149__label);
    tmp148 = CodeStubAssembler(state_).Word32BinaryNot(TNode<BoolT>{tmp145});
    }
    if (catch149__label.is_used()) {
      compiler::CodeAssemblerLabel catch149_skip(&ca_);
      ca_.Goto(&catch149_skip);
      ca_.Bind(&catch149__label, &tmp150);
      ca_.Goto(&block89, phi_bb83_11);
      ca_.Bind(&catch149_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Goto(&block84, phi_bb83_11, tmp148);
  }

  TNode<Smi> phi_bb86_11;
  if (block86.is_used()) {
    ca_.Bind(&block86, &phi_bb86_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 219);
    ca_.Goto(&block8, phi_bb86_11, tmp140);
  }

  TNode<Smi> phi_bb87_11;
  if (block87.is_used()) {
    ca_.Bind(&block87, &phi_bb87_11);
    ca_.Goto(&block8, phi_bb87_11, tmp143);
  }

  TNode<Smi> phi_bb88_11;
  if (block88.is_used()) {
    ca_.Bind(&block88, &phi_bb88_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 218);
    ca_.Goto(&block8, phi_bb88_11, tmp147);
  }

  TNode<Smi> phi_bb89_11;
  if (block89.is_used()) {
    ca_.Bind(&block89, &phi_bb89_11);
    ca_.Goto(&block8, phi_bb89_11, tmp150);
  }

  TNode<Smi> phi_bb84_11;
  TNode<BoolT> phi_bb84_18;
  if (block84.is_used()) {
    ca_.Bind(&block84, &phi_bb84_11, &phi_bb84_18);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Branch(phi_bb84_18, &block64, std::vector<Node*>{phi_bb84_11}, &block65, std::vector<Node*>{phi_bb84_11});
  }

  TNode<Smi> phi_bb64_11;
  TNode<Object> tmp151;
      TNode<Object> tmp153;
  TNode<String> tmp154;
  TNode<Object> tmp155;
      TNode<Object> tmp157;
  TNode<Object> tmp158;
      TNode<Object> tmp160;
  TNode<BoolT> tmp161;
      TNode<Object> tmp163;
  if (block64.is_used()) {
    ca_.Bind(&block64, &phi_bb64_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 223);
    compiler::CodeAssemblerExceptionHandlerLabel catch152__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch152__label);
    tmp151 = CallResolve_0(state_, TNode<Context>{p_context}, TNode<JSReceiver>{p_constructor}, TNode<Object>{p_promiseResolveFunction}, TNode<Object>{tmp45});
    }
    if (catch152__label.is_used()) {
      compiler::CodeAssemblerLabel catch152_skip(&ca_);
      ca_.Goto(&catch152_skip);
      ca_.Bind(&catch152__label, &tmp153);
      ca_.Goto(&block91, phi_bb64_11);
      ca_.Bind(&catch152_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 227);
    tmp154 = kThenString_0(state_);
    compiler::CodeAssemblerExceptionHandlerLabel catch156__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch156__label);
    tmp155 = CodeStubAssembler(state_).GetProperty(TNode<Context>{p_context}, TNode<Object>{tmp151}, TNode<Object>{tmp154});
    }
    if (catch156__label.is_used()) {
      compiler::CodeAssemblerLabel catch156_skip(&ca_);
      ca_.Goto(&catch156_skip);
      ca_.Bind(&catch156__label, &tmp157);
      ca_.Goto(&block92, phi_bb64_11);
      ca_.Bind(&catch156_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 228);
    compiler::CodeAssemblerExceptionHandlerLabel catch159__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch159__label);
    tmp158 = CodeStubAssembler(state_).Call(TNode<Context>{p_nativeContext}, TNode<Object>{tmp155}, TNode<Object>{tmp151}, TNode<Object>{tmp110}, TNode<Object>{tmp112});
    }
    if (catch159__label.is_used()) {
      compiler::CodeAssemblerLabel catch159_skip(&ca_);
      ca_.Goto(&catch159_skip);
      ca_.Bind(&catch159__label, &tmp160);
      ca_.Goto(&block93, phi_bb64_11);
      ca_.Bind(&catch159_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 234);
    compiler::CodeAssemblerExceptionHandlerLabel catch162__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch162__label);
    tmp161 = CodeStubAssembler(state_).IsDebugActive();
    }
    if (catch162__label.is_used()) {
      compiler::CodeAssemblerLabel catch162_skip(&ca_);
      ca_.Goto(&catch162_skip);
      ca_.Bind(&catch162__label, &tmp163);
      ca_.Goto(&block96, phi_bb64_11);
      ca_.Bind(&catch162_skip);
    }
    ca_.Branch(tmp161, &block97, std::vector<Node*>{phi_bb64_11}, &block98, std::vector<Node*>{phi_bb64_11});
  }

  TNode<Smi> phi_bb91_11;
  if (block91.is_used()) {
    ca_.Bind(&block91, &phi_bb91_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 223);
    ca_.Goto(&block8, phi_bb91_11, tmp153);
  }

  TNode<Smi> phi_bb92_11;
  if (block92.is_used()) {
    ca_.Bind(&block92, &phi_bb92_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 227);
    ca_.Goto(&block8, phi_bb92_11, tmp157);
  }

  TNode<Smi> phi_bb93_11;
  if (block93.is_used()) {
    ca_.Bind(&block93, &phi_bb93_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 228);
    ca_.Goto(&block8, phi_bb93_11, tmp160);
  }

  TNode<Smi> phi_bb96_11;
  if (block96.is_used()) {
    ca_.Bind(&block96, &phi_bb96_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 234);
    ca_.Goto(&block8, phi_bb96_11, tmp163);
  }

  TNode<Smi> phi_bb97_11;
  TNode<BoolT> tmp164;
      TNode<Object> tmp166;
  if (block97.is_used()) {
    ca_.Bind(&block97, &phi_bb97_11);
    compiler::CodeAssemblerExceptionHandlerLabel catch165__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch165__label);
    tmp164 = Is_JSPromise_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp158});
    }
    if (catch165__label.is_used()) {
      compiler::CodeAssemblerLabel catch165_skip(&ca_);
      ca_.Goto(&catch165_skip);
      ca_.Bind(&catch165__label, &tmp166);
      ca_.Goto(&block100, phi_bb97_11);
      ca_.Bind(&catch165_skip);
    }
    ca_.Goto(&block99, phi_bb97_11, tmp164);
  }

  TNode<Smi> phi_bb100_11;
  if (block100.is_used()) {
    ca_.Bind(&block100, &phi_bb100_11);
    ca_.Goto(&block8, phi_bb100_11, tmp166);
  }

  TNode<Smi> phi_bb98_11;
  TNode<BoolT> tmp167;
      TNode<Object> tmp169;
  if (block98.is_used()) {
    ca_.Bind(&block98, &phi_bb98_11);
    compiler::CodeAssemblerExceptionHandlerLabel catch168__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch168__label);
    tmp167 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    }
    if (catch168__label.is_used()) {
      compiler::CodeAssemblerLabel catch168_skip(&ca_);
      ca_.Goto(&catch168_skip);
      ca_.Bind(&catch168__label, &tmp169);
      ca_.Goto(&block101, phi_bb98_11);
      ca_.Bind(&catch168_skip);
    }
    ca_.Goto(&block99, phi_bb98_11, tmp167);
  }

  TNode<Smi> phi_bb101_11;
  if (block101.is_used()) {
    ca_.Bind(&block101, &phi_bb101_11);
    ca_.Goto(&block8, phi_bb101_11, tmp169);
  }

  TNode<Smi> phi_bb99_11;
  TNode<BoolT> phi_bb99_21;
  if (block99.is_used()) {
    ca_.Bind(&block99, &phi_bb99_11, &phi_bb99_21);
    ca_.Branch(phi_bb99_21, &block94, std::vector<Node*>{phi_bb99_11}, &block95, std::vector<Node*>{phi_bb99_11});
  }

  TNode<Smi> phi_bb94_11;
  TNode<Symbol> tmp170;
  TNode<Object> tmp171;
      TNode<Object> tmp173;
  if (block94.is_used()) {
    ca_.Bind(&block94, &phi_bb94_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 236);
    tmp170 = kPromiseHandledBySymbol_0(state_);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 235);
    compiler::CodeAssemblerExceptionHandlerLabel catch172__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch172__label);
    tmp171 = CodeStubAssembler(state_).SetPropertyStrict(TNode<Context>{p_context}, TNode<Object>{tmp158}, TNode<Object>{tmp170}, TNode<Object>{tmp1});
    }
    if (catch172__label.is_used()) {
      compiler::CodeAssemblerLabel catch172_skip(&ca_);
      ca_.Goto(&catch172_skip);
      ca_.Bind(&catch172__label, &tmp173);
      ca_.Goto(&block102, phi_bb94_11);
      ca_.Bind(&catch172_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 234);
    ca_.Goto(&block95, phi_bb94_11);
  }

  TNode<Smi> phi_bb102_11;
  if (block102.is_used()) {
    ca_.Bind(&block102, &phi_bb102_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 235);
    ca_.Goto(&block8, phi_bb102_11, tmp173);
  }

  TNode<Smi> phi_bb95_11;
  if (block95.is_used()) {
    ca_.Bind(&block95, &phi_bb95_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Goto(&block90, phi_bb95_11);
  }

  TNode<Smi> phi_bb65_11;
  TNode<JSPromise> tmp174;
      TNode<Object> tmp176;
  TNode<Oddball> tmp177;
      TNode<Object> tmp179;
  if (block65.is_used()) {
    ca_.Bind(&block65, &phi_bb65_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 240);
    compiler::CodeAssemblerExceptionHandlerLabel catch175__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch175__label);
    tmp174 = UnsafeCast_JSPromise_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp45});
    }
    if (catch175__label.is_used()) {
      compiler::CodeAssemblerLabel catch175_skip(&ca_);
      ca_.Goto(&catch175_skip);
      ca_.Bind(&catch175__label, &tmp176);
      ca_.Goto(&block103, phi_bb65_11);
      ca_.Bind(&catch175_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 241);
    tmp177 = Undefined_0(state_);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 239);
    compiler::CodeAssemblerExceptionHandlerLabel catch178__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch178__label);
    PerformPromiseThenImpl_0(state_, TNode<Context>{p_context}, TNode<JSPromise>{tmp174}, TNode<HeapObject>{tmp110}, TNode<HeapObject>{tmp112}, TNode<HeapObject>{tmp177});
    }
    if (catch178__label.is_used()) {
      compiler::CodeAssemblerLabel catch178_skip(&ca_);
      ca_.Goto(&catch178_skip);
      ca_.Bind(&catch178__label, &tmp179);
      ca_.Goto(&block104, phi_bb65_11);
      ca_.Bind(&catch178_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 215);
    ca_.Goto(&block90, phi_bb65_11);
  }

  TNode<Smi> phi_bb103_11;
  if (block103.is_used()) {
    ca_.Bind(&block103, &phi_bb103_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 240);
    ca_.Goto(&block8, phi_bb103_11, tmp176);
  }

  TNode<Smi> phi_bb104_11;
  if (block104.is_used()) {
    ca_.Bind(&block104, &phi_bb104_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 239);
    ca_.Goto(&block8, phi_bb104_11, tmp179);
  }

  TNode<Smi> phi_bb90_11;
  TNode<Smi> tmp180;
      TNode<Object> tmp182;
  TNode<Smi> tmp183;
      TNode<Object> tmp185;
  if (block90.is_used()) {
    ca_.Bind(&block90, &phi_bb90_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 245);
    compiler::CodeAssemblerExceptionHandlerLabel catch181__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch181__label);
    tmp180 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    }
    if (catch181__label.is_used()) {
      compiler::CodeAssemblerLabel catch181_skip(&ca_);
      ca_.Goto(&catch181_skip);
      ca_.Bind(&catch181__label, &tmp182);
      ca_.Goto(&block105, phi_bb90_11, phi_bb90_11, phi_bb90_11);
      ca_.Bind(&catch181_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch184__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch184__label);
    tmp183 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb90_11}, TNode<Smi>{tmp180});
    }
    if (catch184__label.is_used()) {
      compiler::CodeAssemblerLabel catch184_skip(&ca_);
      ca_.Goto(&catch184_skip);
      ca_.Bind(&catch184__label, &tmp185);
      ca_.Goto(&block106, phi_bb90_11, phi_bb90_11);
      ca_.Bind(&catch184_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 138);
    ca_.Goto(&block23, tmp183);
  }

  TNode<Smi> phi_bb105_11;
  TNode<Smi> phi_bb105_17;
  TNode<Smi> phi_bb105_18;
  if (block105.is_used()) {
    ca_.Bind(&block105, &phi_bb105_11, &phi_bb105_17, &phi_bb105_18);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 245);
    ca_.Goto(&block8, phi_bb105_11, tmp182);
  }

  TNode<Smi> phi_bb106_11;
  TNode<Smi> phi_bb106_17;
  if (block106.is_used()) {
    ca_.Bind(&block106, &phi_bb106_11, &phi_bb106_17);
    ca_.Goto(&block8, phi_bb106_11, tmp185);
  }

  TNode<Smi> phi_bb22_11;
  if (block22.is_used()) {
    ca_.Bind(&block22, &phi_bb22_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 250);
    ca_.Goto(&block5, phi_bb22_11);
  }

  TNode<Smi> phi_bb8_11;
  TNode<Object> phi_bb8_12;
  if (block8.is_used()) {
    ca_.Bind(&block8, &phi_bb8_11, &phi_bb8_12);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 248);
    IteratorCloseOnException_0(state_, TNode<Context>{p_context}, TorqueStructIteratorRecord{TNode<JSReceiver>{p_iter.object}, TNode<Object>{p_iter.next}});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 249);
    ca_.Goto(&block1, phi_bb8_12);
  }

  TNode<Smi> phi_bb5_11;
  TNode<IntPtrT> tmp186;
  TNode<IntPtrT> tmp187;
  TNode<IntPtrT> tmp188;
  TNode<Smi> tmp189;
  TNode<IntPtrT> tmp190;
  TNode<IntPtrT> tmp191;
  TNode<UintPtrT> tmp192;
  TNode<UintPtrT> tmp193;
  TNode<BoolT> tmp194;
  if (block5.is_used()) {
    ca_.Bind(&block5, &phi_bb5_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 256);
    tmp186 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp187 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp188 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp189 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp10, tmp188});
    tmp190 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp189});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp191 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp192 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp191});
    tmp193 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp190});
    tmp194 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp192}, TNode<UintPtrT>{tmp193});
    ca_.Branch(tmp194, &block111, std::vector<Node*>{phi_bb5_11}, &block112, std::vector<Node*>{phi_bb5_11});
  }

  TNode<Smi> phi_bb111_11;
  TNode<IntPtrT> tmp195;
  TNode<IntPtrT> tmp196;
  TNode<IntPtrT> tmp197;
  TNode<HeapObject> tmp198;
  TNode<IntPtrT> tmp199;
  TNode<Object> tmp200;
  TNode<Smi> tmp201;
  TNode<Smi> tmp202;
  TNode<Smi> tmp203;
  TNode<IntPtrT> tmp204;
  TNode<IntPtrT> tmp205;
  TNode<IntPtrT> tmp206;
  TNode<Smi> tmp207;
  TNode<IntPtrT> tmp208;
  TNode<IntPtrT> tmp209;
  TNode<UintPtrT> tmp210;
  TNode<UintPtrT> tmp211;
  TNode<BoolT> tmp212;
  if (block111.is_used()) {
    ca_.Bind(&block111, &phi_bb111_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp195 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp196 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp191}, TNode<IntPtrT>{tmp195});
    tmp197 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp186}, TNode<IntPtrT>{tmp196});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp198, tmp199) = NewReference_Object_0(state_, TNode<HeapObject>{tmp10}, TNode<IntPtrT>{tmp197}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 256);
    tmp200 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp198, tmp199});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 255);
    tmp201 = UnsafeCast_Smi_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp200});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 259);
    tmp202 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp203 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp201}, TNode<Smi>{tmp202});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 260);
    tmp204 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp205 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp206 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp207 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp10, tmp206});
    tmp208 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp207});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp209 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp210 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp209});
    tmp211 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp208});
    tmp212 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp210}, TNode<UintPtrT>{tmp211});
    ca_.Branch(tmp212, &block118, std::vector<Node*>{phi_bb111_11}, &block119, std::vector<Node*>{phi_bb111_11});
  }

  TNode<Smi> phi_bb112_11;
  if (block112.is_used()) {
    ca_.Bind(&block112, &phi_bb112_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb118_11;
  TNode<IntPtrT> tmp213;
  TNode<IntPtrT> tmp214;
  TNode<IntPtrT> tmp215;
  TNode<HeapObject> tmp216;
  TNode<IntPtrT> tmp217;
  TNode<Smi> tmp218;
  TNode<BoolT> tmp219;
  if (block118.is_used()) {
    ca_.Bind(&block118, &phi_bb118_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp213 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp214 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp209}, TNode<IntPtrT>{tmp213});
    tmp215 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp204}, TNode<IntPtrT>{tmp214});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp216, tmp217) = NewReference_Object_0(state_, TNode<HeapObject>{tmp10}, TNode<IntPtrT>{tmp215}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 260);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp216, tmp217}, tmp203);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 263);
    tmp218 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp219 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{tmp203}, TNode<Smi>{tmp218});
    ca_.Branch(tmp219, &block121, std::vector<Node*>{phi_bb118_11}, &block122, std::vector<Node*>{phi_bb118_11});
  }

  TNode<Smi> phi_bb119_11;
  if (block119.is_used()) {
    ca_.Bind(&block119, &phi_bb119_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb121_11;
  TNode<IntPtrT> tmp220;
  TNode<IntPtrT> tmp221;
  TNode<IntPtrT> tmp222;
  TNode<Smi> tmp223;
  TNode<IntPtrT> tmp224;
  TNode<IntPtrT> tmp225;
  TNode<UintPtrT> tmp226;
  TNode<UintPtrT> tmp227;
  TNode<BoolT> tmp228;
  if (block121.is_used()) {
    ca_.Bind(&block121, &phi_bb121_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 268);
    tmp220 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp221 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp222 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp223 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp10, tmp222});
    tmp224 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp223});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp225 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementValuesSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp226 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp225});
    tmp227 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp224});
    tmp228 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp226}, TNode<UintPtrT>{tmp227});
    ca_.Branch(tmp228, &block128, std::vector<Node*>{phi_bb121_11}, &block129, std::vector<Node*>{phi_bb121_11});
  }

  TNode<Smi> phi_bb128_11;
  TNode<IntPtrT> tmp229;
  TNode<IntPtrT> tmp230;
  TNode<IntPtrT> tmp231;
  TNode<HeapObject> tmp232;
  TNode<IntPtrT> tmp233;
  TNode<Object> tmp234;
  TNode<FixedArray> tmp235;
  TNode<IntPtrT> tmp236;
  TNode<IntPtrT> tmp237;
  TNode<IntPtrT> tmp238;
  TNode<IntPtrT> tmp239;
  TNode<BoolT> tmp240;
  if (block128.is_used()) {
    ca_.Bind(&block128, &phi_bb128_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp229 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp230 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp225}, TNode<IntPtrT>{tmp229});
    tmp231 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp220}, TNode<IntPtrT>{tmp230});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp232, tmp233) = NewReference_Object_0(state_, TNode<HeapObject>{tmp10}, TNode<IntPtrT>{tmp231}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 268);
    tmp234 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp232, tmp233});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 267);
    tmp235 = UnsafeCast_FixedArray_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp234});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 273);
    tmp236 = CodeStubAssembler(state_).SmiUntag(TNode<Smi>{phi_bb128_11});
    tmp237 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp238 = CodeStubAssembler(state_).IntPtrSub(TNode<IntPtrT>{tmp236}, TNode<IntPtrT>{tmp237});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 275);
    tmp239 = CodeStubAssembler(state_).LoadAndUntagFixedArrayBaseLength(TNode<FixedArrayBase>{tmp235});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 276);
    tmp240 = CodeStubAssembler(state_).IntPtrLessThan(TNode<IntPtrT>{tmp239}, TNode<IntPtrT>{tmp238});
    ca_.Branch(tmp240, &block131, std::vector<Node*>{phi_bb128_11}, &block132, std::vector<Node*>{phi_bb128_11, tmp235});
  }

  TNode<Smi> phi_bb129_11;
  if (block129.is_used()) {
    ca_.Bind(&block129, &phi_bb129_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb131_11;
  TNode<IntPtrT> tmp241;
  TNode<FixedArray> tmp242;
  TNode<IntPtrT> tmp243;
  TNode<IntPtrT> tmp244;
  TNode<IntPtrT> tmp245;
  TNode<Smi> tmp246;
  TNode<IntPtrT> tmp247;
  TNode<IntPtrT> tmp248;
  TNode<UintPtrT> tmp249;
  TNode<UintPtrT> tmp250;
  TNode<BoolT> tmp251;
  if (block131.is_used()) {
    ca_.Bind(&block131, &phi_bb131_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 277);
    tmp241 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp242 = ExtractFixedArray_0(state_, TNode<FixedArray>{tmp235}, TNode<IntPtrT>{tmp241}, TNode<IntPtrT>{tmp239}, TNode<IntPtrT>{tmp238});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 278);
    tmp243 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp244 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp245 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp246 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp10, tmp245});
    tmp247 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp246});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp248 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementValuesSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp249 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp248});
    tmp250 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp247});
    tmp251 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp249}, TNode<UintPtrT>{tmp250});
    ca_.Branch(tmp251, &block137, std::vector<Node*>{phi_bb131_11}, &block138, std::vector<Node*>{phi_bb131_11});
  }

  TNode<Smi> phi_bb137_11;
  TNode<IntPtrT> tmp252;
  TNode<IntPtrT> tmp253;
  TNode<IntPtrT> tmp254;
  TNode<HeapObject> tmp255;
  TNode<IntPtrT> tmp256;
  if (block137.is_used()) {
    ca_.Bind(&block137, &phi_bb137_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp252 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp253 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp248}, TNode<IntPtrT>{tmp252});
    tmp254 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp243}, TNode<IntPtrT>{tmp253});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp255, tmp256) = NewReference_Object_0(state_, TNode<HeapObject>{tmp10}, TNode<IntPtrT>{tmp254}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 278);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp255, tmp256}, tmp242);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 276);
    ca_.Goto(&block132, phi_bb137_11, tmp242);
  }

  TNode<Smi> phi_bb138_11;
  if (block138.is_used()) {
    ca_.Bind(&block138, &phi_bb138_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb132_11;
  TNode<FixedArray> phi_bb132_13;
  if (block132.is_used()) {
    ca_.Bind(&block132, &phi_bb132_11, &phi_bb132_13);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 263);
    ca_.Goto(&block123, phi_bb132_11);
  }

  TNode<Smi> phi_bb122_11;
  TNode<Smi> tmp257;
  TNode<BoolT> tmp258;
  if (block122.is_used()) {
    ca_.Bind(&block122, &phi_bb122_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 284);
    tmp257 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp258 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp203}, TNode<Smi>{tmp257});
    ca_.Branch(tmp258, &block140, std::vector<Node*>{phi_bb122_11}, &block141, std::vector<Node*>{phi_bb122_11});
  }

  TNode<Smi> phi_bb141_11;
  if (block141.is_used()) {
    ca_.Bind(&block141, &phi_bb141_11);
    CodeStubAssembler(state_).FailAssert("Torque assert 'remainingElementsCount == 0' failed", "src/builtins/promise-all.tq", 284);
  }

  TNode<Smi> phi_bb140_11;
  TNode<IntPtrT> tmp259;
  TNode<IntPtrT> tmp260;
  TNode<IntPtrT> tmp261;
  TNode<Smi> tmp262;
  TNode<IntPtrT> tmp263;
  TNode<IntPtrT> tmp264;
  TNode<UintPtrT> tmp265;
  TNode<UintPtrT> tmp266;
  TNode<BoolT> tmp267;
  if (block140.is_used()) {
    ca_.Bind(&block140, &phi_bb140_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 291);
    tmp259 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp260 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp261 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp262 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp10, tmp261});
    tmp263 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp262});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp264 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAllResolveElementContextSlots::kPromiseAllResolveElementValuesSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp265 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp264});
    tmp266 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp263});
    tmp267 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp265}, TNode<UintPtrT>{tmp266});
    ca_.Branch(tmp267, &block146, std::vector<Node*>{phi_bb140_11}, &block147, std::vector<Node*>{phi_bb140_11});
  }

  TNode<Smi> phi_bb146_11;
  TNode<IntPtrT> tmp268;
  TNode<IntPtrT> tmp269;
  TNode<IntPtrT> tmp270;
  TNode<HeapObject> tmp271;
  TNode<IntPtrT> tmp272;
  TNode<Object> tmp273;
  TNode<FixedArray> tmp274;
  TNode<IntPtrT> tmp275;
  TNode<IntPtrT> tmp276;
  TNode<IntPtrT> tmp277;
  TNode<Smi> tmp278;
  TNode<IntPtrT> tmp279;
  TNode<IntPtrT> tmp280;
  TNode<UintPtrT> tmp281;
  TNode<UintPtrT> tmp282;
  TNode<BoolT> tmp283;
  if (block146.is_used()) {
    ca_.Bind(&block146, &phi_bb146_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp268 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp269 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp264}, TNode<IntPtrT>{tmp268});
    tmp270 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp259}, TNode<IntPtrT>{tmp269});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp271, tmp272) = NewReference_Object_0(state_, TNode<HeapObject>{tmp10}, TNode<IntPtrT>{tmp270}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 291);
    tmp273 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp271, tmp272});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 290);
    tmp274 = UnsafeCast_FixedArray_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp273});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 295);
    tmp275 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp276 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp277 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp278 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_nativeContext, tmp277});
    tmp279 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp278});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 295);
    tmp280 = FromConstexpr_intptr_constexpr_intptr_0(state_, Context::Field::JS_ARRAY_PACKED_ELEMENTS_MAP_INDEX);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp281 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp280});
    tmp282 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp279});
    tmp283 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp281}, TNode<UintPtrT>{tmp282});
    ca_.Branch(tmp283, &block153, std::vector<Node*>{phi_bb146_11}, &block154, std::vector<Node*>{phi_bb146_11});
  }

  TNode<Smi> phi_bb147_11;
  if (block147.is_used()) {
    ca_.Bind(&block147, &phi_bb147_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb153_11;
  TNode<IntPtrT> tmp284;
  TNode<IntPtrT> tmp285;
  TNode<IntPtrT> tmp286;
  TNode<HeapObject> tmp287;
  TNode<IntPtrT> tmp288;
  TNode<Object> tmp289;
  TNode<Map> tmp290;
  TNode<JSArray> tmp291;
  TNode<Object> tmp292;
  TNode<Oddball> tmp293;
  TNode<Object> tmp294;
  if (block153.is_used()) {
    ca_.Bind(&block153, &phi_bb153_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp284 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp285 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp280}, TNode<IntPtrT>{tmp284});
    tmp286 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp275}, TNode<IntPtrT>{tmp285});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp287, tmp288) = NewReference_Object_0(state_, TNode<HeapObject>{p_nativeContext}, TNode<IntPtrT>{tmp286}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 295);
    tmp289 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp287, tmp288});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 294);
    tmp290 = UnsafeCast_Map_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp289});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 297);
    tmp291 = NewJSArray_0(state_, TNode<Context>{p_context}, TNode<Map>{tmp290}, TNode<FixedArrayBase>{tmp274});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 298);
    tmp292 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp3});
    tmp293 = Undefined_0(state_);
    tmp294 = CodeStubAssembler(state_).Call(TNode<Context>{p_nativeContext}, TNode<Object>{tmp292}, TNode<Object>{tmp293}, TNode<Object>{tmp291});
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 263);
    ca_.Goto(&block123, phi_bb153_11);
  }

  TNode<Smi> phi_bb154_11;
  if (block154.is_used()) {
    ca_.Bind(&block154, &phi_bb154_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb123_11;
  if (block123.is_used()) {
    ca_.Bind(&block123, &phi_bb123_11);
    ca_.SetSourcePosition("../../src/builtins/promise-all.tq", 339);
    ca_.Goto(&block156);
  }

  TNode<Object> phi_bb1_0;
  if (block1.is_used()) {
    ca_.Bind(&block1, &phi_bb1_0);
    *label_Reject_parameter_0 = phi_bb1_0;
    ca_.Goto(label_Reject);
  }

    ca_.Bind(&block156);
  return TNode<Object>{tmp1};
}

}  // namespace internal
}  // namespace v8

