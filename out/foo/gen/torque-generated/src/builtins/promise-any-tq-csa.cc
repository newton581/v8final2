#include "src/builtins/builtins-array-gen.h"
#include "src/builtins/builtins-bigint-gen.h"
#include "src/builtins/builtins-collections-gen.h"
#include "src/builtins/builtins-constructor-gen.h"
#include "src/builtins/builtins-data-view-gen.h"
#include "src/builtins/builtins-iterator-gen.h"
#include "src/builtins/builtins-promise-gen.h"
#include "src/builtins/builtins-promise.h"
#include "src/builtins/builtins-proxy-gen.h"
#include "src/builtins/builtins-regexp-gen.h"
#include "src/builtins/builtins-string-gen.h"
#include "src/builtins/builtins-typed-array-gen.h"
#include "src/builtins/builtins-utils-gen.h"
#include "src/builtins/builtins-wasm-gen.h"
#include "src/builtins/builtins.h"
#include "src/codegen/code-factory.h"
#include "src/heap/factory-inl.h"
#include "src/ic/binary-op-assembler.h"
#include "src/objects/arguments.h"
#include "src/objects/bigint.h"
#include "src/objects/elements-kind.h"
#include "src/objects/free-space.h"
#include "src/objects/js-break-iterator.h"
#include "src/objects/js-collator.h"
#include "src/objects/js-date-time-format.h"
#include "src/objects/js-display-names.h"
#include "src/objects/js-generator.h"
#include "src/objects/js-list-format.h"
#include "src/objects/js-locale.h"
#include "src/objects/js-number-format.h"
#include "src/objects/js-objects.h"
#include "src/objects/js-plural-rules.h"
#include "src/objects/js-promise.h"
#include "src/objects/js-regexp-string-iterator.h"
#include "src/objects/js-relative-time-format.h"
#include "src/objects/js-segment-iterator.h"
#include "src/objects/js-segmenter.h"
#include "src/objects/js-weak-refs.h"
#include "src/objects/objects.h"
#include "src/objects/ordered-hash-table.h"
#include "src/objects/property-array.h"
#include "src/objects/property-descriptor-object.h"
#include "src/objects/source-text-module.h"
#include "src/objects/stack-frame-info.h"
#include "src/objects/synthetic-module.h"
#include "src/objects/template-objects.h"
#include "src/torque/runtime-support.h"
#include "torque-generated/src/builtins/aggregate-error-tq-csa.h"
#include "torque-generated/src/builtins/array-copywithin-tq-csa.h"
#include "torque-generated/src/builtins/array-every-tq-csa.h"
#include "torque-generated/src/builtins/array-filter-tq-csa.h"
#include "torque-generated/src/builtins/array-find-tq-csa.h"
#include "torque-generated/src/builtins/array-findindex-tq-csa.h"
#include "torque-generated/src/builtins/array-foreach-tq-csa.h"
#include "torque-generated/src/builtins/array-from-tq-csa.h"
#include "torque-generated/src/builtins/array-isarray-tq-csa.h"
#include "torque-generated/src/builtins/array-join-tq-csa.h"
#include "torque-generated/src/builtins/array-lastindexof-tq-csa.h"
#include "torque-generated/src/builtins/array-map-tq-csa.h"
#include "torque-generated/src/builtins/array-of-tq-csa.h"
#include "torque-generated/src/builtins/array-reduce-right-tq-csa.h"
#include "torque-generated/src/builtins/array-reduce-tq-csa.h"
#include "torque-generated/src/builtins/array-reverse-tq-csa.h"
#include "torque-generated/src/builtins/array-shift-tq-csa.h"
#include "torque-generated/src/builtins/array-slice-tq-csa.h"
#include "torque-generated/src/builtins/array-some-tq-csa.h"
#include "torque-generated/src/builtins/array-splice-tq-csa.h"
#include "torque-generated/src/builtins/array-unshift-tq-csa.h"
#include "torque-generated/src/builtins/array-tq-csa.h"
#include "torque-generated/src/builtins/base-tq-csa.h"
#include "torque-generated/src/builtins/bigint-tq-csa.h"
#include "torque-generated/src/builtins/boolean-tq-csa.h"
#include "torque-generated/src/builtins/builtins-string-tq-csa.h"
#include "torque-generated/src/builtins/cast-tq-csa.h"
#include "torque-generated/src/builtins/collections-tq-csa.h"
#include "torque-generated/src/builtins/conversion-tq-csa.h"
#include "torque-generated/src/builtins/convert-tq-csa.h"
#include "torque-generated/src/builtins/console-tq-csa.h"
#include "torque-generated/src/builtins/data-view-tq-csa.h"
#include "torque-generated/src/builtins/finalization-registry-tq-csa.h"
#include "torque-generated/src/builtins/frames-tq-csa.h"
#include "torque-generated/src/builtins/frame-arguments-tq-csa.h"
#include "torque-generated/src/builtins/function-tq-csa.h"
#include "torque-generated/src/builtins/growable-fixed-array-tq-csa.h"
#include "torque-generated/src/builtins/ic-callable-tq-csa.h"
#include "torque-generated/src/builtins/ic-tq-csa.h"
#include "torque-generated/src/builtins/internal-coverage-tq-csa.h"
#include "torque-generated/src/builtins/internal-tq-csa.h"
#include "torque-generated/src/builtins/iterator-tq-csa.h"
#include "torque-generated/src/builtins/math-tq-csa.h"
#include "torque-generated/src/builtins/number-tq-csa.h"
#include "torque-generated/src/builtins/object-fromentries-tq-csa.h"
#include "torque-generated/src/builtins/object-tq-csa.h"
#include "torque-generated/src/builtins/promise-abstract-operations-tq-csa.h"
#include "torque-generated/src/builtins/promise-all-tq-csa.h"
#include "torque-generated/src/builtins/promise-all-element-closure-tq-csa.h"
#include "torque-generated/src/builtins/promise-any-tq-csa.h"
#include "torque-generated/src/builtins/promise-constructor-tq-csa.h"
#include "torque-generated/src/builtins/promise-finally-tq-csa.h"
#include "torque-generated/src/builtins/promise-misc-tq-csa.h"
#include "torque-generated/src/builtins/promise-race-tq-csa.h"
#include "torque-generated/src/builtins/promise-reaction-job-tq-csa.h"
#include "torque-generated/src/builtins/promise-resolve-tq-csa.h"
#include "torque-generated/src/builtins/promise-then-tq-csa.h"
#include "torque-generated/src/builtins/promise-jobs-tq-csa.h"
#include "torque-generated/src/builtins/proxy-constructor-tq-csa.h"
#include "torque-generated/src/builtins/proxy-delete-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-get-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-get-prototype-of-tq-csa.h"
#include "torque-generated/src/builtins/proxy-has-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-is-extensible-tq-csa.h"
#include "torque-generated/src/builtins/proxy-prevent-extensions-tq-csa.h"
#include "torque-generated/src/builtins/proxy-revocable-tq-csa.h"
#include "torque-generated/src/builtins/proxy-revoke-tq-csa.h"
#include "torque-generated/src/builtins/proxy-set-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-set-prototype-of-tq-csa.h"
#include "torque-generated/src/builtins/proxy-tq-csa.h"
#include "torque-generated/src/builtins/reflect-tq-csa.h"
#include "torque-generated/src/builtins/regexp-exec-tq-csa.h"
#include "torque-generated/src/builtins/regexp-match-all-tq-csa.h"
#include "torque-generated/src/builtins/regexp-match-tq-csa.h"
#include "torque-generated/src/builtins/regexp-replace-tq-csa.h"
#include "torque-generated/src/builtins/regexp-search-tq-csa.h"
#include "torque-generated/src/builtins/regexp-source-tq-csa.h"
#include "torque-generated/src/builtins/regexp-split-tq-csa.h"
#include "torque-generated/src/builtins/regexp-test-tq-csa.h"
#include "torque-generated/src/builtins/regexp-tq-csa.h"
#include "torque-generated/src/builtins/string-endswith-tq-csa.h"
#include "torque-generated/src/builtins/string-html-tq-csa.h"
#include "torque-generated/src/builtins/string-iterator-tq-csa.h"
#include "torque-generated/src/builtins/string-pad-tq-csa.h"
#include "torque-generated/src/builtins/string-repeat-tq-csa.h"
#include "torque-generated/src/builtins/string-replaceall-tq-csa.h"
#include "torque-generated/src/builtins/string-slice-tq-csa.h"
#include "torque-generated/src/builtins/string-startswith-tq-csa.h"
#include "torque-generated/src/builtins/string-substring-tq-csa.h"
#include "torque-generated/src/builtins/string-substr-tq-csa.h"
#include "torque-generated/src/builtins/symbol-tq-csa.h"
#include "torque-generated/src/builtins/torque-internal-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-createtypedarray-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-every-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-entries-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-filter-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-find-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-findindex-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-foreach-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-from-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-keys-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-of-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-reduce-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-reduceright-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-set-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-slice-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-some-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-sort-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-subarray-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-values-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-tq-csa.h"
#include "torque-generated/src/builtins/wasm-tq-csa.h"
#include "torque-generated/src/builtins/weak-ref-tq-csa.h"
#include "torque-generated/src/ic/handler-configuration-tq-csa.h"
#include "torque-generated/src/objects/allocation-site-tq-csa.h"
#include "torque-generated/src/objects/api-callbacks-tq-csa.h"
#include "torque-generated/src/objects/arguments-tq-csa.h"
#include "torque-generated/src/objects/cell-tq-csa.h"
#include "torque-generated/src/objects/code-tq-csa.h"
#include "torque-generated/src/objects/contexts-tq-csa.h"
#include "torque-generated/src/objects/data-handler-tq-csa.h"
#include "torque-generated/src/objects/debug-objects-tq-csa.h"
#include "torque-generated/src/objects/descriptor-array-tq-csa.h"
#include "torque-generated/src/objects/embedder-data-array-tq-csa.h"
#include "torque-generated/src/objects/feedback-cell-tq-csa.h"
#include "torque-generated/src/objects/feedback-vector-tq-csa.h"
#include "torque-generated/src/objects/fixed-array-tq-csa.h"
#include "torque-generated/src/objects/foreign-tq-csa.h"
#include "torque-generated/src/objects/free-space-tq-csa.h"
#include "torque-generated/src/objects/heap-number-tq-csa.h"
#include "torque-generated/src/objects/heap-object-tq-csa.h"
#include "torque-generated/src/objects/intl-objects-tq-csa.h"
#include "torque-generated/src/objects/js-array-buffer-tq-csa.h"
#include "torque-generated/src/objects/js-array-tq-csa.h"
#include "torque-generated/src/objects/js-collection-iterator-tq-csa.h"
#include "torque-generated/src/objects/js-collection-tq-csa.h"
#include "torque-generated/src/objects/js-generator-tq-csa.h"
#include "torque-generated/src/objects/js-objects-tq-csa.h"
#include "torque-generated/src/objects/js-promise-tq-csa.h"
#include "torque-generated/src/objects/js-proxy-tq-csa.h"
#include "torque-generated/src/objects/js-regexp-string-iterator-tq-csa.h"
#include "torque-generated/src/objects/js-regexp-tq-csa.h"
#include "torque-generated/src/objects/js-weak-refs-tq-csa.h"
#include "torque-generated/src/objects/literal-objects-tq-csa.h"
#include "torque-generated/src/objects/map-tq-csa.h"
#include "torque-generated/src/objects/microtask-tq-csa.h"
#include "torque-generated/src/objects/module-tq-csa.h"
#include "torque-generated/src/objects/name-tq-csa.h"
#include "torque-generated/src/objects/oddball-tq-csa.h"
#include "torque-generated/src/objects/ordered-hash-table-tq-csa.h"
#include "torque-generated/src/objects/primitive-heap-object-tq-csa.h"
#include "torque-generated/src/objects/promise-tq-csa.h"
#include "torque-generated/src/objects/property-array-tq-csa.h"
#include "torque-generated/src/objects/property-cell-tq-csa.h"
#include "torque-generated/src/objects/property-descriptor-object-tq-csa.h"
#include "torque-generated/src/objects/prototype-info-tq-csa.h"
#include "torque-generated/src/objects/regexp-match-info-tq-csa.h"
#include "torque-generated/src/objects/scope-info-tq-csa.h"
#include "torque-generated/src/objects/script-tq-csa.h"
#include "torque-generated/src/objects/shared-function-info-tq-csa.h"
#include "torque-generated/src/objects/source-text-module-tq-csa.h"
#include "torque-generated/src/objects/stack-frame-info-tq-csa.h"
#include "torque-generated/src/objects/string-tq-csa.h"
#include "torque-generated/src/objects/struct-tq-csa.h"
#include "torque-generated/src/objects/synthetic-module-tq-csa.h"
#include "torque-generated/src/objects/template-objects-tq-csa.h"
#include "torque-generated/src/objects/template-tq-csa.h"
#include "torque-generated/src/wasm/wasm-objects-tq-csa.h"
#include "torque-generated/test/torque/test-torque-tq-csa.h"
#include "torque-generated/third_party/v8/builtins/array-sort-tq-csa.h"

namespace v8 {
namespace internal {

TNode<Int32T> FromConstexpr_PromiseAnyRejectElementContextSlots_constexpr_kPromiseAnyRejectElementRemainingSlot_0(compiler::CodeAssemblerState* state_, PromiseBuiltins::PromiseAnyRejectElementContextSlots p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Int32T> tmp0;
  TNode<Int32T> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 8);
    tmp0 = ca_.Int32Constant(CastToUnderlyingTypeIfEnum(p_o));
    tmp1 = (TNode<Int32T>{tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<Int32T>{tmp1};
}

TNode<Int32T> FromConstexpr_PromiseAnyRejectElementContextSlots_constexpr_kPromiseAnyRejectElementCapabilitySlot_0(compiler::CodeAssemblerState* state_, PromiseBuiltins::PromiseAnyRejectElementContextSlots p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Int32T> tmp0;
  TNode<Int32T> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 8);
    tmp0 = ca_.Int32Constant(CastToUnderlyingTypeIfEnum(p_o));
    tmp1 = (TNode<Int32T>{tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<Int32T>{tmp1};
}

TNode<Int32T> FromConstexpr_PromiseAnyRejectElementContextSlots_constexpr_kPromiseAnyRejectElementErrorsSlot_0(compiler::CodeAssemblerState* state_, PromiseBuiltins::PromiseAnyRejectElementContextSlots p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Int32T> tmp0;
  TNode<Int32T> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 8);
    tmp0 = ca_.Int32Constant(CastToUnderlyingTypeIfEnum(p_o));
    tmp1 = (TNode<Int32T>{tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<Int32T>{tmp1};
}

TNode<Int32T> FromConstexpr_PromiseAnyRejectElementContextSlots_constexpr_kPromiseAnyRejectElementLength_0(compiler::CodeAssemblerState* state_, PromiseBuiltins::PromiseAnyRejectElementContextSlots p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Int32T> tmp0;
  TNode<Int32T> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 8);
    tmp0 = ca_.Int32Constant(CastToUnderlyingTypeIfEnum(p_o));
    tmp1 = (TNode<Int32T>{tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<Int32T>{tmp1};
}

TNode<Context> CreatePromiseAnyRejectElementContext_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<PromiseCapability> p_capability, TNode<NativeContext> p_nativeContext) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block14(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block20(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block21(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block23(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Context> tmp0;
  TNode<IntPtrT> tmp1;
  TNode<IntPtrT> tmp2;
  TNode<IntPtrT> tmp3;
  TNode<Smi> tmp4;
  TNode<IntPtrT> tmp5;
  TNode<IntPtrT> tmp6;
  TNode<UintPtrT> tmp7;
  TNode<UintPtrT> tmp8;
  TNode<BoolT> tmp9;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 31);
    tmp0 = CodeStubAssembler(state_).AllocateSyntheticFunctionContext(TNode<NativeContext>{p_nativeContext}, PromiseBuiltins::PromiseAnyRejectElementContextSlots::kPromiseAnyRejectElementLength);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 34);
    tmp1 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp3 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp4 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp0, tmp3});
    tmp5 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp4});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp6 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAnyRejectElementContextSlots::kPromiseAnyRejectElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp7 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp6});
    tmp8 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp5});
    tmp9 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp7}, TNode<UintPtrT>{tmp8});
    ca_.Branch(tmp9, &block6, std::vector<Node*>{}, &block7, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp10;
  TNode<IntPtrT> tmp11;
  TNode<IntPtrT> tmp12;
  TNode<HeapObject> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<Smi> tmp15;
  TNode<IntPtrT> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<IntPtrT> tmp18;
  TNode<Smi> tmp19;
  TNode<IntPtrT> tmp20;
  TNode<IntPtrT> tmp21;
  TNode<UintPtrT> tmp22;
  TNode<UintPtrT> tmp23;
  TNode<BoolT> tmp24;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp10 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp11 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp6}, TNode<IntPtrT>{tmp10});
    tmp12 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp1}, TNode<IntPtrT>{tmp11});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp13, tmp14) = NewReference_Object_0(state_, TNode<HeapObject>{tmp0}, TNode<IntPtrT>{tmp12}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 36);
    tmp15 = CodeStubAssembler(state_).SmiConstant(1);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 34);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp13, tmp14}, tmp15);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 37);
    tmp16 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp17 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp18 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp19 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp0, tmp18});
    tmp20 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp19});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp21 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAnyRejectElementContextSlots::kPromiseAnyRejectElementCapabilitySlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp22 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp21});
    tmp23 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp20});
    tmp24 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp22}, TNode<UintPtrT>{tmp23});
    ca_.Branch(tmp24, &block13, std::vector<Node*>{}, &block14, std::vector<Node*>{});
  }

  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp25;
  TNode<IntPtrT> tmp26;
  TNode<IntPtrT> tmp27;
  TNode<HeapObject> tmp28;
  TNode<IntPtrT> tmp29;
  TNode<IntPtrT> tmp30;
  TNode<IntPtrT> tmp31;
  TNode<IntPtrT> tmp32;
  TNode<Smi> tmp33;
  TNode<IntPtrT> tmp34;
  TNode<IntPtrT> tmp35;
  TNode<UintPtrT> tmp36;
  TNode<UintPtrT> tmp37;
  TNode<BoolT> tmp38;
  if (block13.is_used()) {
    ca_.Bind(&block13);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp25 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp26 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp21}, TNode<IntPtrT>{tmp25});
    tmp27 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp16}, TNode<IntPtrT>{tmp26});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp28, tmp29) = NewReference_Object_0(state_, TNode<HeapObject>{tmp0}, TNode<IntPtrT>{tmp27}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 37);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp28, tmp29}, p_capability);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 40);
    tmp30 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp31 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp32 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp33 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp0, tmp32});
    tmp34 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp33});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp35 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAnyRejectElementContextSlots::kPromiseAnyRejectElementErrorsSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp36 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp35});
    tmp37 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp34});
    tmp38 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp36}, TNode<UintPtrT>{tmp37});
    ca_.Branch(tmp38, &block20, std::vector<Node*>{}, &block21, std::vector<Node*>{});
  }

  if (block14.is_used()) {
    ca_.Bind(&block14);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp39;
  TNode<IntPtrT> tmp40;
  TNode<IntPtrT> tmp41;
  TNode<HeapObject> tmp42;
  TNode<IntPtrT> tmp43;
  TNode<FixedArray> tmp44;
  if (block20.is_used()) {
    ca_.Bind(&block20);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp39 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp40 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp35}, TNode<IntPtrT>{tmp39});
    tmp41 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp30}, TNode<IntPtrT>{tmp40});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp42, tmp43) = NewReference_Object_0(state_, TNode<HeapObject>{tmp0}, TNode<IntPtrT>{tmp41}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 42);
    tmp44 = kEmptyFixedArray_0(state_);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 40);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp42, tmp43}, tmp44);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 28);
    ca_.Goto(&block23);
  }

  if (block21.is_used()) {
    ca_.Bind(&block21);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

    ca_.Bind(&block23);
  return TNode<Context>{tmp0};
}

TNode<JSFunction> CreatePromiseAnyRejectElementFunction_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<Context> p_rejectElementContext, TNode<Smi> p_index, TNode<NativeContext> p_nativeContext) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block14(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block15(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Smi> tmp0;
  TNode<BoolT> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 49);
    tmp0 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp1 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{p_index}, TNode<Smi>{tmp0});
    ca_.Branch(tmp1, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    CodeStubAssembler(state_).FailAssert("Torque assert 'index > 0' failed", "src/builtins/promise-any.tq", 49);
  }

  TNode<Smi> tmp2;
  TNode<BoolT> tmp3;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 50);
    tmp2 = FromConstexpr_Smi_constexpr_int31_0(state_, PropertyArray::HashField::kMax);
    tmp3 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{p_index}, TNode<Smi>{tmp2});
    ca_.Branch(tmp3, &block4, std::vector<Node*>{}, &block5, std::vector<Node*>{});
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    CodeStubAssembler(state_).FailAssert("Torque assert 'index < kPropertyArrayHashFieldMax' failed", "src/builtins/promise-any.tq", 50);
  }

  TNode<IntPtrT> tmp4;
  TNode<IntPtrT> tmp5;
  TNode<IntPtrT> tmp6;
  TNode<Smi> tmp7;
  TNode<IntPtrT> tmp8;
  TNode<IntPtrT> tmp9;
  TNode<UintPtrT> tmp10;
  TNode<UintPtrT> tmp11;
  TNode<BoolT> tmp12;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 52);
    tmp4 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp5 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp6 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp7 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_nativeContext, tmp6});
    tmp8 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp7});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 52);
    tmp9 = FromConstexpr_intptr_constexpr_intptr_0(state_, Context::Field::STRICT_FUNCTION_WITHOUT_PROTOTYPE_MAP_INDEX);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp10 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp9});
    tmp11 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp8});
    tmp12 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp10}, TNode<UintPtrT>{tmp11});
    ca_.Branch(tmp12, &block10, std::vector<Node*>{}, &block11, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<IntPtrT> tmp15;
  TNode<HeapObject> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<Object> tmp18;
  TNode<Map> tmp19;
  TNode<SharedFunctionInfo> tmp20;
  TNode<JSFunction> tmp21;
  TNode<BoolT> tmp22;
  if (block10.is_used()) {
    ca_.Bind(&block10);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp13 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp14 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp9}, TNode<IntPtrT>{tmp13});
    tmp15 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp4}, TNode<IntPtrT>{tmp14});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp16, tmp17) = NewReference_Object_0(state_, TNode<HeapObject>{p_nativeContext}, TNode<IntPtrT>{tmp15}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 52);
    tmp18 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp16, tmp17});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 51);
    tmp19 = UnsafeCast_Map_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp18});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 54);
    tmp20 = CodeStubAssembler(state_).PromiseAnyRejectElementSharedFunConstant();
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 56);
    tmp21 = CodeStubAssembler(state_).AllocateFunctionWithMapAndContext(TNode<Map>{tmp19}, TNode<SharedFunctionInfo>{tmp20}, TNode<Context>{p_rejectElementContext});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 57);
    tmp22 = FromConstexpr_bool_constexpr_bool_0(state_, (CodeStubAssembler(state_).ConstexprInt31Equal(PropertyArray::kNoHashSentinel, 0)));
    ca_.Branch(tmp22, &block13, std::vector<Node*>{}, &block14, std::vector<Node*>{});
  }

  if (block11.is_used()) {
    ca_.Bind(&block11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  if (block14.is_used()) {
    ca_.Bind(&block14);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 57);
    CodeStubAssembler(state_).FailAssert("Torque assert 'kPropertyArrayNoHashSentinel == 0' failed", "src/builtins/promise-any.tq", 57);
  }

  TNode<IntPtrT> tmp23;
  if (block13.is_used()) {
    ca_.Bind(&block13);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 58);
    tmp23 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp21, tmp23}, p_index);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 46);
    ca_.Goto(&block15);
  }

    ca_.Bind(&block15);
  return TNode<JSFunction>{tmp21};
}

TF_BUILTIN(PromiseAnyRejectElementClosure, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kReceiver));
USE(parameter1);
  TNode<JSFunction> parameter2 = UncheckedCast<JSFunction>(Parameter(Descriptor::kJSTarget));
USE(parameter2);
  TNode<Object> parameter3 = UncheckedCast<Object>(Parameter(Descriptor::kValue));
  USE(parameter3);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block12(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block17(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block18(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block24(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block25(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block27(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block33(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block34(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block28(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, FixedArray, FixedArray, HeapObject> block40(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, FixedArray, FixedArray, HeapObject> block41(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block47(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block48(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block50(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block56(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block57(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block51(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<BoolT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 78);
    tmp0 = IsNativeContext_0(state_, TNode<HeapObject>{parameter0});
    ca_.Branch(tmp0, &block1, std::vector<Node*>{}, &block2, std::vector<Node*>{});
  }

  TNode<Oddball> tmp1;
  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 79);
    tmp1 = Undefined_0(state_);
    CodeStubAssembler(state_).Return(tmp1);
  }

  TNode<IntPtrT> tmp2;
  TNode<Smi> tmp3;
  TNode<Smi> tmp4;
  TNode<BoolT> tmp5;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 83);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp3 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter0, tmp2});
    tmp4 = FromConstexpr_Smi_constexpr_int31_0(state_, PromiseBuiltins::PromiseAnyRejectElementContextSlots::kPromiseAnyRejectElementLength);
    tmp5 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp3}, TNode<Smi>{tmp4});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 82);
    ca_.Branch(tmp5, &block3, std::vector<Node*>{}, &block4, std::vector<Node*>{});
  }

  if (block4.is_used()) {
    ca_.Bind(&block4);
    CodeStubAssembler(state_).FailAssert("Torque assert 'context.length == PromiseAnyRejectElementContextSlots::kPromiseAnyRejectElementLength' failed", "src/builtins/promise-any.tq", 82);
  }

  TNode<NativeContext> tmp6;
  TNode<IntPtrT> tmp7;
  TNode<BoolT> tmp8;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 87);
    tmp6 = CodeStubAssembler(state_).LoadNativeContext(TNode<Context>{parameter0});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 88);
    tmp7 = FromConstexpr_intptr_constexpr_int31_0(state_, 16);
    CodeStubAssembler(state_).StoreReference<Context>(CodeStubAssembler::Reference{parameter2, tmp7}, tmp6);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 91);
    tmp8 = FromConstexpr_bool_constexpr_bool_0(state_, (CodeStubAssembler(state_).ConstexprInt31Equal(PropertyArray::kNoHashSentinel, 0)));
    ca_.Branch(tmp8, &block5, std::vector<Node*>{}, &block6, std::vector<Node*>{});
  }

  if (block6.is_used()) {
    ca_.Bind(&block6);
    CodeStubAssembler(state_).FailAssert("Torque assert 'kPropertyArrayNoHashSentinel == 0' failed", "src/builtins/promise-any.tq", 91);
  }

  TNode<IntPtrT> tmp9;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 92);
    compiler::CodeAssemblerLabel label10(&ca_);
    tmp9 = CodeStubAssembler(state_).LoadJSReceiverIdentityHash(TNode<Object>{parameter2}, &label10);
    ca_.Goto(&block9);
    if (label10.is_used()) {
      ca_.Bind(&label10);
      ca_.Goto(&block10);
    }
  }

  if (block10.is_used()) {
    ca_.Bind(&block10);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/promise-any.tq:92:69");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp11;
  TNode<BoolT> tmp12;
  if (block9.is_used()) {
    ca_.Bind(&block9);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 93);
    tmp11 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp12 = CodeStubAssembler(state_).IntPtrGreaterThan(TNode<IntPtrT>{tmp9}, TNode<IntPtrT>{tmp11});
    ca_.Branch(tmp12, &block11, std::vector<Node*>{}, &block12, std::vector<Node*>{});
  }

  if (block12.is_used()) {
    ca_.Bind(&block12);
    CodeStubAssembler(state_).FailAssert("Torque assert 'identityHash > 0' failed", "src/builtins/promise-any.tq", 93);
  }

  TNode<IntPtrT> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<IntPtrT> tmp15;
  TNode<IntPtrT> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<Smi> tmp18;
  TNode<IntPtrT> tmp19;
  TNode<IntPtrT> tmp20;
  TNode<UintPtrT> tmp21;
  TNode<UintPtrT> tmp22;
  TNode<BoolT> tmp23;
  if (block11.is_used()) {
    ca_.Bind(&block11);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 94);
    tmp13 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp14 = CodeStubAssembler(state_).IntPtrSub(TNode<IntPtrT>{tmp9}, TNode<IntPtrT>{tmp13});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 98);
    tmp15 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp16 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp17 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp18 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter0, tmp17});
    tmp19 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp18});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp20 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAnyRejectElementContextSlots::kPromiseAnyRejectElementErrorsSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp21 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp20});
    tmp22 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp19});
    tmp23 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp21}, TNode<UintPtrT>{tmp22});
    ca_.Branch(tmp23, &block17, std::vector<Node*>{}, &block18, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp24;
  TNode<IntPtrT> tmp25;
  TNode<IntPtrT> tmp26;
  TNode<HeapObject> tmp27;
  TNode<IntPtrT> tmp28;
  TNode<Object> tmp29;
  TNode<FixedArray> tmp30;
  TNode<IntPtrT> tmp31;
  TNode<IntPtrT> tmp32;
  TNode<IntPtrT> tmp33;
  TNode<Smi> tmp34;
  TNode<IntPtrT> tmp35;
  TNode<IntPtrT> tmp36;
  TNode<UintPtrT> tmp37;
  TNode<UintPtrT> tmp38;
  TNode<BoolT> tmp39;
  if (block17.is_used()) {
    ca_.Bind(&block17);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp24 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp25 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp20}, TNode<IntPtrT>{tmp24});
    tmp26 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp15}, TNode<IntPtrT>{tmp25});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp27, tmp28) = NewReference_Object_0(state_, TNode<HeapObject>{parameter0}, TNode<IntPtrT>{tmp26}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 98);
    tmp29 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp27, tmp28});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 97);
    tmp30 = UnsafeCast_FixedArray_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp29});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 105);
    tmp31 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp32 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp33 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp34 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter0, tmp33});
    tmp35 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp34});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp36 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAnyRejectElementContextSlots::kPromiseAnyRejectElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp37 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp36});
    tmp38 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp35});
    tmp39 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp37}, TNode<UintPtrT>{tmp38});
    ca_.Branch(tmp39, &block24, std::vector<Node*>{}, &block25, std::vector<Node*>{});
  }

  if (block18.is_used()) {
    ca_.Bind(&block18);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp40;
  TNode<IntPtrT> tmp41;
  TNode<IntPtrT> tmp42;
  TNode<HeapObject> tmp43;
  TNode<IntPtrT> tmp44;
  TNode<Object> tmp45;
  TNode<Smi> tmp46;
  TNode<IntPtrT> tmp47;
  TNode<IntPtrT> tmp48;
  TNode<IntPtrT> tmp49;
  TNode<IntPtrT> tmp50;
  TNode<IntPtrT> tmp51;
  TNode<BoolT> tmp52;
  if (block24.is_used()) {
    ca_.Bind(&block24);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp40 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp41 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp36}, TNode<IntPtrT>{tmp40});
    tmp42 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp31}, TNode<IntPtrT>{tmp41});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp43, tmp44) = NewReference_Object_0(state_, TNode<HeapObject>{parameter0}, TNode<IntPtrT>{tmp42}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 105);
    tmp45 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp43, tmp44});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 104);
    tmp46 = UnsafeCast_Smi_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp45});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 109);
    tmp47 = CodeStubAssembler(state_).SmiUntag(TNode<Smi>{tmp46});
    tmp48 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp49 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp14}, TNode<IntPtrT>{tmp48});
    tmp50 = CodeStubAssembler(state_).IntPtrMax(TNode<IntPtrT>{tmp47}, TNode<IntPtrT>{tmp49});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 110);
    tmp51 = CodeStubAssembler(state_).LoadAndUntagFixedArrayBaseLength(TNode<FixedArrayBase>{tmp30});
    tmp52 = CodeStubAssembler(state_).IntPtrGreaterThan(TNode<IntPtrT>{tmp50}, TNode<IntPtrT>{tmp51});
    ca_.Branch(tmp52, &block27, std::vector<Node*>{}, &block28, std::vector<Node*>{tmp30});
  }

  if (block25.is_used()) {
    ca_.Bind(&block25);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp53;
  TNode<IntPtrT> tmp54;
  TNode<FixedArray> tmp55;
  TNode<IntPtrT> tmp56;
  TNode<IntPtrT> tmp57;
  TNode<IntPtrT> tmp58;
  TNode<Smi> tmp59;
  TNode<IntPtrT> tmp60;
  TNode<IntPtrT> tmp61;
  TNode<UintPtrT> tmp62;
  TNode<UintPtrT> tmp63;
  TNode<BoolT> tmp64;
  if (block27.is_used()) {
    ca_.Bind(&block27);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 111);
    tmp53 = CodeStubAssembler(state_).LoadAndUntagFixedArrayBaseLength(TNode<FixedArrayBase>{tmp30});
    tmp54 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp55 = ExtractFixedArray_0(state_, TNode<FixedArray>{tmp30}, TNode<IntPtrT>{tmp54}, TNode<IntPtrT>{tmp53}, TNode<IntPtrT>{tmp50});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 112);
    tmp56 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp57 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp58 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp59 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter0, tmp58});
    tmp60 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp59});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp61 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAnyRejectElementContextSlots::kPromiseAnyRejectElementErrorsSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp62 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp61});
    tmp63 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp60});
    tmp64 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp62}, TNode<UintPtrT>{tmp63});
    ca_.Branch(tmp64, &block33, std::vector<Node*>{}, &block34, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp65;
  TNode<IntPtrT> tmp66;
  TNode<IntPtrT> tmp67;
  TNode<HeapObject> tmp68;
  TNode<IntPtrT> tmp69;
  if (block33.is_used()) {
    ca_.Bind(&block33);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp65 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp66 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp61}, TNode<IntPtrT>{tmp65});
    tmp67 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp56}, TNode<IntPtrT>{tmp66});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp68, tmp69) = NewReference_Object_0(state_, TNode<HeapObject>{parameter0}, TNode<IntPtrT>{tmp67}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 112);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp68, tmp69}, tmp55);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 110);
    ca_.Goto(&block28, tmp55);
  }

  if (block34.is_used()) {
    ca_.Bind(&block34);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb28_7;
  TNode<IntPtrT> tmp70;
  TNode<IntPtrT> tmp71;
  TNode<IntPtrT> tmp72;
  TNode<Smi> tmp73;
  TNode<IntPtrT> tmp74;
  TNode<UintPtrT> tmp75;
  TNode<UintPtrT> tmp76;
  TNode<BoolT> tmp77;
  if (block28.is_used()) {
    ca_.Bind(&block28, &phi_bb28_7);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 115);
    tmp70 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp71 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp72 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp73 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{phi_bb28_7, tmp72});
    tmp74 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp73});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp75 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp14});
    tmp76 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp74});
    tmp77 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp75}, TNode<UintPtrT>{tmp76});
    ca_.Branch(tmp77, &block40, std::vector<Node*>{phi_bb28_7, phi_bb28_7, phi_bb28_7, phi_bb28_7}, &block41, std::vector<Node*>{phi_bb28_7, phi_bb28_7, phi_bb28_7, phi_bb28_7});
  }

  TNode<FixedArray> phi_bb40_7;
  TNode<FixedArray> phi_bb40_10;
  TNode<FixedArray> phi_bb40_11;
  TNode<HeapObject> phi_bb40_16;
  TNode<IntPtrT> tmp78;
  TNode<IntPtrT> tmp79;
  TNode<IntPtrT> tmp80;
  TNode<HeapObject> tmp81;
  TNode<IntPtrT> tmp82;
  TNode<Smi> tmp83;
  TNode<Smi> tmp84;
  TNode<IntPtrT> tmp85;
  TNode<IntPtrT> tmp86;
  TNode<IntPtrT> tmp87;
  TNode<Smi> tmp88;
  TNode<IntPtrT> tmp89;
  TNode<IntPtrT> tmp90;
  TNode<UintPtrT> tmp91;
  TNode<UintPtrT> tmp92;
  TNode<BoolT> tmp93;
  if (block40.is_used()) {
    ca_.Bind(&block40, &phi_bb40_7, &phi_bb40_10, &phi_bb40_11, &phi_bb40_16);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp78 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp79 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp14}, TNode<IntPtrT>{tmp78});
    tmp80 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp70}, TNode<IntPtrT>{tmp79});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp81, tmp82) = NewReference_Object_0(state_, TNode<HeapObject>{phi_bb40_16}, TNode<IntPtrT>{tmp80}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 115);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp81, tmp82}, parameter3);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 119);
    tmp83 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp84 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp46}, TNode<Smi>{tmp83});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 120);
    tmp85 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp86 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp87 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp88 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter0, tmp87});
    tmp89 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp88});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp90 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAnyRejectElementContextSlots::kPromiseAnyRejectElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp91 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp90});
    tmp92 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp89});
    tmp93 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp91}, TNode<UintPtrT>{tmp92});
    ca_.Branch(tmp93, &block47, std::vector<Node*>{phi_bb40_7}, &block48, std::vector<Node*>{phi_bb40_7});
  }

  TNode<FixedArray> phi_bb41_7;
  TNode<FixedArray> phi_bb41_10;
  TNode<FixedArray> phi_bb41_11;
  TNode<HeapObject> phi_bb41_16;
  if (block41.is_used()) {
    ca_.Bind(&block41, &phi_bb41_7, &phi_bb41_10, &phi_bb41_11, &phi_bb41_16);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb47_7;
  TNode<IntPtrT> tmp94;
  TNode<IntPtrT> tmp95;
  TNode<IntPtrT> tmp96;
  TNode<HeapObject> tmp97;
  TNode<IntPtrT> tmp98;
  TNode<Smi> tmp99;
  TNode<BoolT> tmp100;
  if (block47.is_used()) {
    ca_.Bind(&block47, &phi_bb47_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp94 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp95 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp90}, TNode<IntPtrT>{tmp94});
    tmp96 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp85}, TNode<IntPtrT>{tmp95});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp97, tmp98) = NewReference_Object_0(state_, TNode<HeapObject>{parameter0}, TNode<IntPtrT>{tmp96}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 120);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp97, tmp98}, tmp84);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 125);
    tmp99 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp100 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp84}, TNode<Smi>{tmp99});
    ca_.Branch(tmp100, &block50, std::vector<Node*>{phi_bb47_7}, &block51, std::vector<Node*>{phi_bb47_7});
  }

  TNode<FixedArray> phi_bb48_7;
  if (block48.is_used()) {
    ca_.Bind(&block48, &phi_bb48_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb50_7;
  TNode<JSObject> tmp101;
  TNode<IntPtrT> tmp102;
  TNode<IntPtrT> tmp103;
  TNode<IntPtrT> tmp104;
  TNode<Smi> tmp105;
  TNode<IntPtrT> tmp106;
  TNode<IntPtrT> tmp107;
  TNode<UintPtrT> tmp108;
  TNode<UintPtrT> tmp109;
  TNode<BoolT> tmp110;
  if (block50.is_used()) {
    ca_.Bind(&block50, &phi_bb50_7);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 129);
    tmp101 = ConstructAggregateError_0(state_, TNode<Context>{parameter0}, TNode<FixedArray>{phi_bb50_7});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 132);
    tmp102 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp103 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp104 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp105 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter0, tmp104});
    tmp106 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp105});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp107 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAnyRejectElementContextSlots::kPromiseAnyRejectElementCapabilitySlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp108 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp107});
    tmp109 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp106});
    tmp110 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp108}, TNode<UintPtrT>{tmp109});
    ca_.Branch(tmp110, &block56, std::vector<Node*>{phi_bb50_7}, &block57, std::vector<Node*>{phi_bb50_7});
  }

  TNode<FixedArray> phi_bb56_7;
  TNode<IntPtrT> tmp111;
  TNode<IntPtrT> tmp112;
  TNode<IntPtrT> tmp113;
  TNode<HeapObject> tmp114;
  TNode<IntPtrT> tmp115;
  TNode<Object> tmp116;
  TNode<PromiseCapability> tmp117;
  TNode<IntPtrT> tmp118;
  TNode<Object> tmp119;
  TNode<JSReceiver> tmp120;
  TNode<Oddball> tmp121;
  TNode<Object> tmp122;
  if (block56.is_used()) {
    ca_.Bind(&block56, &phi_bb56_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp111 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp112 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp107}, TNode<IntPtrT>{tmp111});
    tmp113 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp102}, TNode<IntPtrT>{tmp112});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp114, tmp115) = NewReference_Object_0(state_, TNode<HeapObject>{parameter0}, TNode<IntPtrT>{tmp113}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 132);
    tmp116 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp114, tmp115});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 131);
    tmp117 = UnsafeCast_PromiseCapability_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp116});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 134);
    tmp118 = FromConstexpr_intptr_constexpr_int31_0(state_, 12);
    tmp119 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp117, tmp118});
    tmp120 = UnsafeCast_Callable_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp119});
    tmp121 = Undefined_0(state_);
    tmp122 = CodeStubAssembler(state_).Call(TNode<Context>{parameter0}, TNode<Object>{tmp120}, TNode<Object>{tmp121}, TNode<Object>{tmp101});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 125);
    ca_.Goto(&block51, phi_bb56_7);
  }

  TNode<FixedArray> phi_bb57_7;
  if (block57.is_used()) {
    ca_.Bind(&block57, &phi_bb57_7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb51_7;
  TNode<Oddball> tmp123;
  if (block51.is_used()) {
    ca_.Bind(&block51, &phi_bb51_7);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 138);
    tmp123 = Undefined_0(state_);
    CodeStubAssembler(state_).Return(tmp123);
  }
}

TNode<Object> PerformPromiseAny_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<NativeContext> p_nativeContext, TorqueStructIteratorRecord p_iteratorRecord, TNode<JSReceiver> p_constructor, TNode<PromiseCapability> p_resultCapability, TNode<Object> p_promiseResolveFunction, compiler::CodeAssemblerLabel* label_Reject, compiler::TypedCodeAssemblerVariable<Object>* label_Reject_parameter_0) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block9(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block15(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block16(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block18(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block21(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block22(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block19(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block29(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block28(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block27(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block30(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Object> block24(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block33(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block34(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block31(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block35(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block36(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block32(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block37(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block38(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block39(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block40(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block41(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block46(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block47(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block49(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block50(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block51(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block52(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block57(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block58(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block60(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block61(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block62(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block63(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block64(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block65(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block66(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block67(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block70(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block71(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block74(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block72(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block75(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block73(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block68(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block76(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block77(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block78(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block69(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block20(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Object> block6(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block83(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block84(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block90(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block91(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block93(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block99(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block100(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block94(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block102(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Context> tmp0;
  TNode<Smi> tmp1;
  TNode<IntPtrT> tmp2;
      TNode<Object> tmp4;
  TNode<IntPtrT> tmp5;
      TNode<Object> tmp7;
  TNode<IntPtrT> tmp8;
      TNode<Object> tmp10;
  TNode<Smi> tmp11;
  TNode<IntPtrT> tmp12;
  TNode<IntPtrT> tmp13;
      TNode<Object> tmp15;
  TNode<UintPtrT> tmp16;
  TNode<UintPtrT> tmp17;
  TNode<BoolT> tmp18;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 154);
    tmp0 = CreatePromiseAnyRejectElementContext_0(state_, TNode<Context>{p_context}, TNode<PromiseCapability>{p_resultCapability}, TNode<NativeContext>{p_nativeContext});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 158);
    tmp1 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 162);
    compiler::CodeAssemblerExceptionHandlerLabel catch3__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch3__label);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    }
    if (catch3__label.is_used()) {
      compiler::CodeAssemblerLabel catch3_skip(&ca_);
      ca_.Goto(&catch3_skip);
      ca_.Bind(&catch3__label, &tmp4);
      ca_.Goto(&block7);
      ca_.Bind(&catch3_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch6__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch6__label);
    tmp5 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    }
    if (catch6__label.is_used()) {
      compiler::CodeAssemblerLabel catch6_skip(&ca_);
      ca_.Goto(&catch6_skip);
      ca_.Bind(&catch6__label, &tmp7);
      ca_.Goto(&block8);
      ca_.Bind(&catch6_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch9__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch9__label);
    tmp8 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    }
    if (catch9__label.is_used()) {
      compiler::CodeAssemblerLabel catch9_skip(&ca_);
      ca_.Goto(&catch9_skip);
      ca_.Bind(&catch9__label, &tmp10);
      ca_.Goto(&block9);
      ca_.Bind(&catch9_skip);
    }
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp11 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_nativeContext, tmp8});
    tmp12 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp11});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 162);
    compiler::CodeAssemblerExceptionHandlerLabel catch14__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch14__label);
    tmp13 = FromConstexpr_intptr_constexpr_intptr_0(state_, Context::Field::ITERATOR_RESULT_MAP_INDEX);
    }
    if (catch14__label.is_used()) {
      compiler::CodeAssemblerLabel catch14_skip(&ca_);
      ca_.Goto(&catch14_skip);
      ca_.Bind(&catch14__label, &tmp15);
      ca_.Goto(&block10);
      ca_.Bind(&catch14_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp16 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp13});
    tmp17 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp12});
    tmp18 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp16}, TNode<UintPtrT>{tmp17});
    ca_.Branch(tmp18, &block15, std::vector<Node*>{}, &block16, std::vector<Node*>{});
  }

  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 162);
    ca_.Goto(&block6, tmp1, tmp4);
  }

  if (block8.is_used()) {
    ca_.Bind(&block8);
    ca_.Goto(&block6, tmp1, tmp7);
  }

  if (block9.is_used()) {
    ca_.Bind(&block9);
    ca_.Goto(&block6, tmp1, tmp10);
  }

  if (block10.is_used()) {
    ca_.Bind(&block10);
    ca_.Goto(&block6, tmp1, tmp15);
  }

  TNode<IntPtrT> tmp19;
  TNode<IntPtrT> tmp20;
  TNode<IntPtrT> tmp21;
  TNode<HeapObject> tmp22;
  TNode<IntPtrT> tmp23;
  TNode<Object> tmp24;
  TNode<Map> tmp25;
      TNode<Object> tmp27;
  if (block15.is_used()) {
    ca_.Bind(&block15);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp19 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp20 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp13}, TNode<IntPtrT>{tmp19});
    tmp21 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp2}, TNode<IntPtrT>{tmp20});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp22, tmp23) = NewReference_Object_0(state_, TNode<HeapObject>{p_nativeContext}, TNode<IntPtrT>{tmp21}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 162);
    tmp24 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp22, tmp23});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 161);
    compiler::CodeAssemblerExceptionHandlerLabel catch26__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch26__label);
    tmp25 = UnsafeCast_Map_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp24});
    }
    if (catch26__label.is_used()) {
      compiler::CodeAssemblerLabel catch26_skip(&ca_);
      ca_.Goto(&catch26_skip);
      ca_.Bind(&catch26__label, &tmp27);
      ca_.Goto(&block18);
      ca_.Bind(&catch26_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 164);
    ca_.Goto(&block21, tmp1);
  }

  if (block16.is_used()) {
    ca_.Bind(&block16);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  if (block18.is_used()) {
    ca_.Bind(&block18);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 161);
    ca_.Goto(&block6, tmp1, tmp27);
  }

  TNode<Smi> phi_bb21_8;
  TNode<BoolT> tmp28;
      TNode<Object> tmp30;
  if (block21.is_used()) {
    ca_.Bind(&block21, &phi_bb21_8);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 164);
    compiler::CodeAssemblerExceptionHandlerLabel catch29__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch29__label);
    tmp28 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    }
    if (catch29__label.is_used()) {
      compiler::CodeAssemblerLabel catch29_skip(&ca_);
      ca_.Goto(&catch29_skip);
      ca_.Bind(&catch29__label, &tmp30);
      ca_.Goto(&block22, phi_bb21_8);
      ca_.Bind(&catch29_skip);
    }
    ca_.Branch(tmp28, &block19, std::vector<Node*>{phi_bb21_8}, &block20, std::vector<Node*>{phi_bb21_8});
  }

  TNode<Smi> phi_bb22_8;
  if (block22.is_used()) {
    ca_.Bind(&block22, &phi_bb22_8);
    ca_.Goto(&block6, phi_bb22_8, tmp30);
  }

  TNode<Smi> phi_bb19_8;
  TNode<JSReceiver> tmp31;
      TNode<Object> tmp34;
  if (block19.is_used()) {
    ca_.Bind(&block19, &phi_bb19_8);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 175);
    compiler::CodeAssemblerLabel label32(&ca_);
    compiler::CodeAssemblerExceptionHandlerLabel catch33__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch33__label);
    tmp31 = IteratorBuiltinsAssembler(state_).IteratorStep(TNode<Context>{p_context}, TorqueStructIteratorRecord{TNode<JSReceiver>{p_iteratorRecord.object}, TNode<Object>{p_iteratorRecord.next}}, TNode<Map>{tmp25}, &label32);
    }
    if (catch33__label.is_used()) {
      compiler::CodeAssemblerLabel catch33_skip(&ca_);
      ca_.Goto(&catch33_skip);
      ca_.Bind(&catch33__label, &tmp34);
      ca_.Goto(&block29, phi_bb19_8);
      ca_.Bind(&catch33_skip);
    }
    ca_.Goto(&block27, phi_bb19_8);
    if (label32.is_used()) {
      ca_.Bind(&label32);
      ca_.Goto(&block28, phi_bb19_8);
    }
  }

  TNode<Smi> phi_bb29_8;
  if (block29.is_used()) {
    ca_.Bind(&block29, &phi_bb29_8);
    ca_.Goto(&block24, phi_bb29_8, tmp34);
  }

  TNode<Smi> phi_bb28_8;
  if (block28.is_used()) {
    ca_.Bind(&block28, &phi_bb28_8);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 160);
    ca_.Goto(&block3, phi_bb28_8);
  }

  TNode<Smi> phi_bb27_8;
  TNode<Object> tmp35;
      TNode<Object> tmp37;
  TNode<Smi> tmp38;
      TNode<Object> tmp40;
  TNode<BoolT> tmp41;
      TNode<Object> tmp43;
  if (block27.is_used()) {
    ca_.Bind(&block27, &phi_bb27_8);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 183);
    compiler::CodeAssemblerExceptionHandlerLabel catch36__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch36__label);
    tmp35 = IteratorBuiltinsAssembler(state_).IteratorValue(TNode<Context>{p_context}, TNode<JSReceiver>{tmp31}, TNode<Map>{tmp25});
    }
    if (catch36__label.is_used()) {
      compiler::CodeAssemblerLabel catch36_skip(&ca_);
      ca_.Goto(&catch36_skip);
      ca_.Bind(&catch36__label, &tmp37);
      ca_.Goto(&block30, phi_bb27_8);
      ca_.Bind(&catch36_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 190);
    compiler::CodeAssemblerExceptionHandlerLabel catch39__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch39__label);
    tmp38 = FromConstexpr_Smi_constexpr_int31_0(state_, PropertyArray::HashField::kMax);
    }
    if (catch39__label.is_used()) {
      compiler::CodeAssemblerLabel catch39_skip(&ca_);
      ca_.Goto(&catch39_skip);
      ca_.Bind(&catch39__label, &tmp40);
      ca_.Goto(&block33, phi_bb27_8, phi_bb27_8, phi_bb27_8);
      ca_.Bind(&catch39_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch42__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch42__label);
    tmp41 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{phi_bb27_8}, TNode<Smi>{tmp38});
    }
    if (catch42__label.is_used()) {
      compiler::CodeAssemblerLabel catch42_skip(&ca_);
      ca_.Goto(&catch42_skip);
      ca_.Bind(&catch42__label, &tmp43);
      ca_.Goto(&block34, phi_bb27_8, phi_bb27_8);
      ca_.Bind(&catch42_skip);
    }
    ca_.Branch(tmp41, &block31, std::vector<Node*>{phi_bb27_8}, &block32, std::vector<Node*>{phi_bb27_8});
  }

  TNode<Smi> phi_bb30_8;
  if (block30.is_used()) {
    ca_.Bind(&block30, &phi_bb30_8);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 183);
    ca_.Goto(&block24, phi_bb30_8, tmp37);
  }

  TNode<Smi> phi_bb24_8;
  TNode<Object> phi_bb24_11;
  if (block24.is_used()) {
    ca_.Bind(&block24, &phi_bb24_8, &phi_bb24_11);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 185);
    ca_.Goto(&block1, phi_bb24_11);
  }

  TNode<Smi> phi_bb33_8;
  TNode<Smi> phi_bb33_11;
  TNode<Smi> phi_bb33_12;
  if (block33.is_used()) {
    ca_.Bind(&block33, &phi_bb33_8, &phi_bb33_11, &phi_bb33_12);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 190);
    ca_.Goto(&block6, phi_bb33_8, tmp40);
  }

  TNode<Smi> phi_bb34_8;
  TNode<Smi> phi_bb34_11;
  if (block34.is_used()) {
    ca_.Bind(&block34, &phi_bb34_8, &phi_bb34_11);
    ca_.Goto(&block6, phi_bb34_8, tmp43);
  }

  TNode<Smi> phi_bb31_8;
  TNode<Object> tmp44;
      TNode<Object> tmp46;
      TNode<Object> tmp48;
  if (block31.is_used()) {
    ca_.Bind(&block31, &phi_bb31_8);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 200);
    compiler::CodeAssemblerExceptionHandlerLabel catch45__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch45__label);
    tmp44 = FromConstexpr_Object_constexpr_string_0(state_, "any");
    }
    if (catch45__label.is_used()) {
      compiler::CodeAssemblerLabel catch45_skip(&ca_);
      ca_.Goto(&catch45_skip);
      ca_.Bind(&catch45__label, &tmp46);
      ca_.Goto(&block35, phi_bb31_8);
      ca_.Bind(&catch45_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch47__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch47__label);
    CodeStubAssembler(state_).ThrowRangeError(TNode<Context>{p_context}, MessageTemplate::kTooManyElementsInPromiseCombinator, TNode<Object>{tmp44});
    }
    if (catch47__label.is_used()) {
      compiler::CodeAssemblerLabel catch47_skip(&ca_);
      ca_.Bind(&catch47__label, &tmp48);
      ca_.Goto(&block36, phi_bb31_8);
    }
  }

  TNode<Smi> phi_bb35_8;
  if (block35.is_used()) {
    ca_.Bind(&block35, &phi_bb35_8);
    ca_.Goto(&block6, phi_bb35_8, tmp46);
  }

  TNode<Smi> phi_bb36_8;
  if (block36.is_used()) {
    ca_.Bind(&block36, &phi_bb36_8);
    ca_.Goto(&block6, phi_bb36_8, tmp48);
  }

  TNode<Smi> phi_bb32_8;
  TNode<Object> tmp49;
      TNode<Object> tmp51;
  TNode<JSFunction> tmp52;
      TNode<Object> tmp54;
  TNode<IntPtrT> tmp55;
      TNode<Object> tmp57;
  TNode<IntPtrT> tmp58;
      TNode<Object> tmp60;
  TNode<IntPtrT> tmp61;
      TNode<Object> tmp63;
  TNode<Smi> tmp64;
  TNode<IntPtrT> tmp65;
  TNode<IntPtrT> tmp66;
  TNode<UintPtrT> tmp67;
  TNode<UintPtrT> tmp68;
  TNode<BoolT> tmp69;
  if (block32.is_used()) {
    ca_.Bind(&block32, &phi_bb32_8);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 210);
    compiler::CodeAssemblerExceptionHandlerLabel catch50__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch50__label);
    tmp49 = CallResolve_0(state_, TNode<Context>{p_context}, TNode<JSReceiver>{p_constructor}, TNode<Object>{p_promiseResolveFunction}, TNode<Object>{tmp35});
    }
    if (catch50__label.is_used()) {
      compiler::CodeAssemblerLabel catch50_skip(&ca_);
      ca_.Goto(&catch50_skip);
      ca_.Bind(&catch50__label, &tmp51);
      ca_.Goto(&block37, phi_bb32_8);
      ca_.Bind(&catch50_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 230);
    compiler::CodeAssemblerExceptionHandlerLabel catch53__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch53__label);
    tmp52 = CreatePromiseAnyRejectElementFunction_0(state_, TNode<Context>{p_context}, TNode<Context>{tmp0}, TNode<Smi>{phi_bb32_8}, TNode<NativeContext>{p_nativeContext});
    }
    if (catch53__label.is_used()) {
      compiler::CodeAssemblerLabel catch53_skip(&ca_);
      ca_.Goto(&catch53_skip);
      ca_.Bind(&catch53__label, &tmp54);
      ca_.Goto(&block38, phi_bb32_8, phi_bb32_8);
      ca_.Bind(&catch53_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 235);
    compiler::CodeAssemblerExceptionHandlerLabel catch56__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch56__label);
    tmp55 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    }
    if (catch56__label.is_used()) {
      compiler::CodeAssemblerLabel catch56_skip(&ca_);
      ca_.Goto(&catch56_skip);
      ca_.Bind(&catch56__label, &tmp57);
      ca_.Goto(&block39, phi_bb32_8);
      ca_.Bind(&catch56_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch59__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch59__label);
    tmp58 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    }
    if (catch59__label.is_used()) {
      compiler::CodeAssemblerLabel catch59_skip(&ca_);
      ca_.Goto(&catch59_skip);
      ca_.Bind(&catch59__label, &tmp60);
      ca_.Goto(&block40, phi_bb32_8);
      ca_.Bind(&catch59_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch62__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch62__label);
    tmp61 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    }
    if (catch62__label.is_used()) {
      compiler::CodeAssemblerLabel catch62_skip(&ca_);
      ca_.Goto(&catch62_skip);
      ca_.Bind(&catch62__label, &tmp63);
      ca_.Goto(&block41, phi_bb32_8);
      ca_.Bind(&catch62_skip);
    }
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp64 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp0, tmp61});
    tmp65 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp64});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp66 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAnyRejectElementContextSlots::kPromiseAnyRejectElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp67 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp66});
    tmp68 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp65});
    tmp69 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp67}, TNode<UintPtrT>{tmp68});
    ca_.Branch(tmp69, &block46, std::vector<Node*>{phi_bb32_8}, &block47, std::vector<Node*>{phi_bb32_8});
  }

  TNode<Smi> phi_bb37_8;
  if (block37.is_used()) {
    ca_.Bind(&block37, &phi_bb37_8);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 210);
    ca_.Goto(&block6, phi_bb37_8, tmp51);
  }

  TNode<Smi> phi_bb38_8;
  TNode<Smi> phi_bb38_13;
  if (block38.is_used()) {
    ca_.Bind(&block38, &phi_bb38_8, &phi_bb38_13);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 230);
    ca_.Goto(&block6, phi_bb38_8, tmp54);
  }

  TNode<Smi> phi_bb39_8;
  if (block39.is_used()) {
    ca_.Bind(&block39, &phi_bb39_8);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 235);
    ca_.Goto(&block6, phi_bb39_8, tmp57);
  }

  TNode<Smi> phi_bb40_8;
  if (block40.is_used()) {
    ca_.Bind(&block40, &phi_bb40_8);
    ca_.Goto(&block6, phi_bb40_8, tmp60);
  }

  TNode<Smi> phi_bb41_8;
  if (block41.is_used()) {
    ca_.Bind(&block41, &phi_bb41_8);
    ca_.Goto(&block6, phi_bb41_8, tmp63);
  }

  TNode<Smi> phi_bb46_8;
  TNode<IntPtrT> tmp70;
  TNode<IntPtrT> tmp71;
  TNode<IntPtrT> tmp72;
  TNode<HeapObject> tmp73;
  TNode<IntPtrT> tmp74;
  TNode<Object> tmp75;
  TNode<Smi> tmp76;
      TNode<Object> tmp78;
  TNode<IntPtrT> tmp79;
      TNode<Object> tmp81;
  TNode<IntPtrT> tmp82;
      TNode<Object> tmp84;
  TNode<IntPtrT> tmp85;
      TNode<Object> tmp87;
  TNode<Smi> tmp88;
  TNode<IntPtrT> tmp89;
  TNode<IntPtrT> tmp90;
  TNode<UintPtrT> tmp91;
  TNode<UintPtrT> tmp92;
  TNode<BoolT> tmp93;
  if (block46.is_used()) {
    ca_.Bind(&block46, &phi_bb46_8);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp70 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp71 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp66}, TNode<IntPtrT>{tmp70});
    tmp72 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp55}, TNode<IntPtrT>{tmp71});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp73, tmp74) = NewReference_Object_0(state_, TNode<HeapObject>{tmp0}, TNode<IntPtrT>{tmp72}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 235);
    tmp75 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp73, tmp74});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 234);
    compiler::CodeAssemblerExceptionHandlerLabel catch77__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch77__label);
    tmp76 = UnsafeCast_Smi_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp75});
    }
    if (catch77__label.is_used()) {
      compiler::CodeAssemblerLabel catch77_skip(&ca_);
      ca_.Goto(&catch77_skip);
      ca_.Bind(&catch77__label, &tmp78);
      ca_.Goto(&block49, phi_bb46_8);
      ca_.Bind(&catch77_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 238);
    compiler::CodeAssemblerExceptionHandlerLabel catch80__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch80__label);
    tmp79 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    }
    if (catch80__label.is_used()) {
      compiler::CodeAssemblerLabel catch80_skip(&ca_);
      ca_.Goto(&catch80_skip);
      ca_.Bind(&catch80__label, &tmp81);
      ca_.Goto(&block50, phi_bb46_8);
      ca_.Bind(&catch80_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch83__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch83__label);
    tmp82 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    }
    if (catch83__label.is_used()) {
      compiler::CodeAssemblerLabel catch83_skip(&ca_);
      ca_.Goto(&catch83_skip);
      ca_.Bind(&catch83__label, &tmp84);
      ca_.Goto(&block51, phi_bb46_8);
      ca_.Bind(&catch83_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch86__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch86__label);
    tmp85 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    }
    if (catch86__label.is_used()) {
      compiler::CodeAssemblerLabel catch86_skip(&ca_);
      ca_.Goto(&catch86_skip);
      ca_.Bind(&catch86__label, &tmp87);
      ca_.Goto(&block52, phi_bb46_8);
      ca_.Bind(&catch86_skip);
    }
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp88 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp0, tmp85});
    tmp89 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp88});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp90 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAnyRejectElementContextSlots::kPromiseAnyRejectElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp91 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp90});
    tmp92 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp89});
    tmp93 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp91}, TNode<UintPtrT>{tmp92});
    ca_.Branch(tmp93, &block57, std::vector<Node*>{phi_bb46_8}, &block58, std::vector<Node*>{phi_bb46_8});
  }

  TNode<Smi> phi_bb47_8;
  if (block47.is_used()) {
    ca_.Bind(&block47, &phi_bb47_8);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb49_8;
  if (block49.is_used()) {
    ca_.Bind(&block49, &phi_bb49_8);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 234);
    ca_.Goto(&block6, phi_bb49_8, tmp78);
  }

  TNode<Smi> phi_bb50_8;
  if (block50.is_used()) {
    ca_.Bind(&block50, &phi_bb50_8);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 238);
    ca_.Goto(&block6, phi_bb50_8, tmp81);
  }

  TNode<Smi> phi_bb51_8;
  if (block51.is_used()) {
    ca_.Bind(&block51, &phi_bb51_8);
    ca_.Goto(&block6, phi_bb51_8, tmp84);
  }

  TNode<Smi> phi_bb52_8;
  if (block52.is_used()) {
    ca_.Bind(&block52, &phi_bb52_8);
    ca_.Goto(&block6, phi_bb52_8, tmp87);
  }

  TNode<Smi> phi_bb57_8;
  TNode<IntPtrT> tmp94;
  TNode<IntPtrT> tmp95;
  TNode<IntPtrT> tmp96;
  TNode<HeapObject> tmp97;
  TNode<IntPtrT> tmp98;
  TNode<Smi> tmp99;
      TNode<Object> tmp101;
  TNode<Smi> tmp102;
      TNode<Object> tmp104;
  TNode<String> tmp105;
  TNode<Object> tmp106;
      TNode<Object> tmp108;
  TNode<IntPtrT> tmp109;
      TNode<Object> tmp111;
  TNode<Object> tmp112;
  TNode<Object> tmp113;
      TNode<Object> tmp115;
  TNode<Object> tmp116;
      TNode<Object> tmp118;
  TNode<Smi> tmp119;
      TNode<Object> tmp121;
  TNode<Smi> tmp122;
      TNode<Object> tmp124;
  TNode<BoolT> tmp125;
      TNode<Object> tmp127;
  if (block57.is_used()) {
    ca_.Bind(&block57, &phi_bb57_8);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp94 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp95 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp90}, TNode<IntPtrT>{tmp94});
    tmp96 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp79}, TNode<IntPtrT>{tmp95});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp97, tmp98) = NewReference_Object_0(state_, TNode<HeapObject>{tmp0}, TNode<IntPtrT>{tmp96}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 240);
    compiler::CodeAssemblerExceptionHandlerLabel catch100__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch100__label);
    tmp99 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    }
    if (catch100__label.is_used()) {
      compiler::CodeAssemblerLabel catch100_skip(&ca_);
      ca_.Goto(&catch100_skip);
      ca_.Bind(&catch100__label, &tmp101);
      ca_.Goto(&block60, phi_bb57_8);
      ca_.Bind(&catch100_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch103__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch103__label);
    tmp102 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp76}, TNode<Smi>{tmp99});
    }
    if (catch103__label.is_used()) {
      compiler::CodeAssemblerLabel catch103_skip(&ca_);
      ca_.Goto(&catch103_skip);
      ca_.Bind(&catch103__label, &tmp104);
      ca_.Goto(&block61, phi_bb57_8);
      ca_.Bind(&catch103_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 238);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp97, tmp98}, tmp102);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 246);
    tmp105 = kThenString_0(state_);
    compiler::CodeAssemblerExceptionHandlerLabel catch107__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch107__label);
    tmp106 = CodeStubAssembler(state_).GetProperty(TNode<Context>{p_context}, TNode<Object>{tmp49}, TNode<Object>{tmp105});
    }
    if (catch107__label.is_used()) {
      compiler::CodeAssemblerLabel catch107_skip(&ca_);
      ca_.Goto(&catch107_skip);
      ca_.Bind(&catch107__label, &tmp108);
      ca_.Goto(&block62, phi_bb57_8);
      ca_.Bind(&catch107_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 249);
    compiler::CodeAssemblerExceptionHandlerLabel catch110__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch110__label);
    tmp109 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    }
    if (catch110__label.is_used()) {
      compiler::CodeAssemblerLabel catch110_skip(&ca_);
      ca_.Goto(&catch110_skip);
      ca_.Bind(&catch110__label, &tmp111);
      ca_.Goto(&block63, phi_bb57_8);
      ca_.Bind(&catch110_skip);
    }
    tmp112 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{p_resultCapability, tmp109});
    compiler::CodeAssemblerExceptionHandlerLabel catch114__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch114__label);
    tmp113 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp112});
    }
    if (catch114__label.is_used()) {
      compiler::CodeAssemblerLabel catch114_skip(&ca_);
      ca_.Goto(&catch114_skip);
      ca_.Bind(&catch114__label, &tmp115);
      ca_.Goto(&block64, phi_bb57_8);
      ca_.Bind(&catch114_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 247);
    compiler::CodeAssemblerExceptionHandlerLabel catch117__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch117__label);
    tmp116 = CodeStubAssembler(state_).Call(TNode<Context>{p_context}, TNode<Object>{tmp106}, TNode<Object>{tmp49}, TNode<Object>{tmp113}, TNode<Object>{tmp52});
    }
    if (catch117__label.is_used()) {
      compiler::CodeAssemblerLabel catch117_skip(&ca_);
      ca_.Goto(&catch117_skip);
      ca_.Bind(&catch117__label, &tmp118);
      ca_.Goto(&block65, phi_bb57_8);
      ca_.Bind(&catch117_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 252);
    compiler::CodeAssemblerExceptionHandlerLabel catch120__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch120__label);
    tmp119 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    }
    if (catch120__label.is_used()) {
      compiler::CodeAssemblerLabel catch120_skip(&ca_);
      ca_.Goto(&catch120_skip);
      ca_.Bind(&catch120__label, &tmp121);
      ca_.Goto(&block66, phi_bb57_8, phi_bb57_8, phi_bb57_8);
      ca_.Bind(&catch120_skip);
    }
    compiler::CodeAssemblerExceptionHandlerLabel catch123__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch123__label);
    tmp122 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb57_8}, TNode<Smi>{tmp119});
    }
    if (catch123__label.is_used()) {
      compiler::CodeAssemblerLabel catch123_skip(&ca_);
      ca_.Goto(&catch123_skip);
      ca_.Bind(&catch123__label, &tmp124);
      ca_.Goto(&block67, phi_bb57_8, phi_bb57_8);
      ca_.Bind(&catch123_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 256);
    compiler::CodeAssemblerExceptionHandlerLabel catch126__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch126__label);
    tmp125 = CodeStubAssembler(state_).IsDebugActive();
    }
    if (catch126__label.is_used()) {
      compiler::CodeAssemblerLabel catch126_skip(&ca_);
      ca_.Goto(&catch126_skip);
      ca_.Bind(&catch126__label, &tmp127);
      ca_.Goto(&block70);
      ca_.Bind(&catch126_skip);
    }
    ca_.Branch(tmp125, &block71, std::vector<Node*>{}, &block72, std::vector<Node*>{});
  }

  TNode<Smi> phi_bb58_8;
  if (block58.is_used()) {
    ca_.Bind(&block58, &phi_bb58_8);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb60_8;
  if (block60.is_used()) {
    ca_.Bind(&block60, &phi_bb60_8);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 240);
    ca_.Goto(&block6, phi_bb60_8, tmp101);
  }

  TNode<Smi> phi_bb61_8;
  if (block61.is_used()) {
    ca_.Bind(&block61, &phi_bb61_8);
    ca_.Goto(&block6, phi_bb61_8, tmp104);
  }

  TNode<Smi> phi_bb62_8;
  if (block62.is_used()) {
    ca_.Bind(&block62, &phi_bb62_8);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 246);
    ca_.Goto(&block6, phi_bb62_8, tmp108);
  }

  TNode<Smi> phi_bb63_8;
  if (block63.is_used()) {
    ca_.Bind(&block63, &phi_bb63_8);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 249);
    ca_.Goto(&block6, phi_bb63_8, tmp111);
  }

  TNode<Smi> phi_bb64_8;
  if (block64.is_used()) {
    ca_.Bind(&block64, &phi_bb64_8);
    ca_.Goto(&block6, phi_bb64_8, tmp115);
  }

  TNode<Smi> phi_bb65_8;
  if (block65.is_used()) {
    ca_.Bind(&block65, &phi_bb65_8);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 247);
    ca_.Goto(&block6, phi_bb65_8, tmp118);
  }

  TNode<Smi> phi_bb66_8;
  TNode<Smi> phi_bb66_16;
  TNode<Smi> phi_bb66_17;
  if (block66.is_used()) {
    ca_.Bind(&block66, &phi_bb66_8, &phi_bb66_16, &phi_bb66_17);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 252);
    ca_.Goto(&block6, phi_bb66_8, tmp121);
  }

  TNode<Smi> phi_bb67_8;
  TNode<Smi> phi_bb67_16;
  if (block67.is_used()) {
    ca_.Bind(&block67, &phi_bb67_8, &phi_bb67_16);
    ca_.Goto(&block6, phi_bb67_8, tmp124);
  }

  if (block70.is_used()) {
    ca_.Bind(&block70);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 256);
    ca_.Goto(&block6, tmp122, tmp127);
  }

  TNode<BoolT> tmp128;
      TNode<Object> tmp130;
  if (block71.is_used()) {
    ca_.Bind(&block71);
    compiler::CodeAssemblerExceptionHandlerLabel catch129__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch129__label);
    tmp128 = Is_JSPromise_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp116});
    }
    if (catch129__label.is_used()) {
      compiler::CodeAssemblerLabel catch129_skip(&ca_);
      ca_.Goto(&catch129_skip);
      ca_.Bind(&catch129__label, &tmp130);
      ca_.Goto(&block74);
      ca_.Bind(&catch129_skip);
    }
    ca_.Goto(&block73, tmp128);
  }

  if (block74.is_used()) {
    ca_.Bind(&block74);
    ca_.Goto(&block6, tmp122, tmp130);
  }

  TNode<BoolT> tmp131;
      TNode<Object> tmp133;
  if (block72.is_used()) {
    ca_.Bind(&block72);
    compiler::CodeAssemblerExceptionHandlerLabel catch132__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch132__label);
    tmp131 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    }
    if (catch132__label.is_used()) {
      compiler::CodeAssemblerLabel catch132_skip(&ca_);
      ca_.Goto(&catch132_skip);
      ca_.Bind(&catch132__label, &tmp133);
      ca_.Goto(&block75);
      ca_.Bind(&catch132_skip);
    }
    ca_.Goto(&block73, tmp131);
  }

  if (block75.is_used()) {
    ca_.Bind(&block75);
    ca_.Goto(&block6, tmp122, tmp133);
  }

  TNode<BoolT> phi_bb73_17;
  if (block73.is_used()) {
    ca_.Bind(&block73, &phi_bb73_17);
    ca_.Branch(phi_bb73_17, &block68, std::vector<Node*>{}, &block69, std::vector<Node*>{});
  }

  TNode<Symbol> tmp134;
  TNode<IntPtrT> tmp135;
      TNode<Object> tmp137;
  TNode<HeapObject> tmp138;
  TNode<Object> tmp139;
      TNode<Object> tmp141;
  TNode<Symbol> tmp142;
  TNode<Oddball> tmp143;
  TNode<Object> tmp144;
      TNode<Object> tmp146;
  if (block68.is_used()) {
    ca_.Bind(&block68);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 258);
    tmp134 = kPromiseHandledBySymbol_0(state_);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 259);
    compiler::CodeAssemblerExceptionHandlerLabel catch136__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch136__label);
    tmp135 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    }
    if (catch136__label.is_used()) {
      compiler::CodeAssemblerLabel catch136_skip(&ca_);
      ca_.Goto(&catch136_skip);
      ca_.Bind(&catch136__label, &tmp137);
      ca_.Goto(&block76);
      ca_.Bind(&catch136_skip);
    }
    tmp138 = CodeStubAssembler(state_).LoadReference<HeapObject>(CodeStubAssembler::Reference{p_resultCapability, tmp135});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 257);
    compiler::CodeAssemblerExceptionHandlerLabel catch140__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch140__label);
    tmp139 = CodeStubAssembler(state_).SetPropertyStrict(TNode<Context>{p_context}, TNode<Object>{tmp116}, TNode<Object>{tmp134}, TNode<Object>{tmp138});
    }
    if (catch140__label.is_used()) {
      compiler::CodeAssemblerLabel catch140_skip(&ca_);
      ca_.Goto(&catch140_skip);
      ca_.Bind(&catch140__label, &tmp141);
      ca_.Goto(&block77);
      ca_.Bind(&catch140_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 261);
    tmp142 = kPromiseForwardingHandlerSymbol_0(state_);
    tmp143 = True_0(state_);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 260);
    compiler::CodeAssemblerExceptionHandlerLabel catch145__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch145__label);
    tmp144 = CodeStubAssembler(state_).SetPropertyStrict(TNode<Context>{p_context}, TNode<Object>{tmp52}, TNode<Object>{tmp142}, TNode<Object>{tmp143});
    }
    if (catch145__label.is_used()) {
      compiler::CodeAssemblerLabel catch145_skip(&ca_);
      ca_.Goto(&catch145_skip);
      ca_.Bind(&catch145__label, &tmp146);
      ca_.Goto(&block78);
      ca_.Bind(&catch145_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 256);
    ca_.Goto(&block69);
  }

  if (block76.is_used()) {
    ca_.Bind(&block76);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 259);
    ca_.Goto(&block6, tmp122, tmp137);
  }

  if (block77.is_used()) {
    ca_.Bind(&block77);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 257);
    ca_.Goto(&block6, tmp122, tmp141);
  }

  if (block78.is_used()) {
    ca_.Bind(&block78);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 260);
    ca_.Goto(&block6, tmp122, tmp146);
  }

  if (block69.is_used()) {
    ca_.Bind(&block69);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 164);
    ca_.Goto(&block21, tmp122);
  }

  TNode<Smi> phi_bb20_8;
  if (block20.is_used()) {
    ca_.Bind(&block20, &phi_bb20_8);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 267);
    ca_.Goto(&block3, phi_bb20_8);
  }

  TNode<Smi> phi_bb6_8;
  TNode<Object> phi_bb6_9;
  if (block6.is_used()) {
    ca_.Bind(&block6, &phi_bb6_8, &phi_bb6_9);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 265);
    IteratorCloseOnException_0(state_, TNode<Context>{p_context}, TorqueStructIteratorRecord{TNode<JSReceiver>{p_iteratorRecord.object}, TNode<Object>{p_iteratorRecord.next}});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 266);
    ca_.Goto(&block1, phi_bb6_9);
  }

  TNode<Smi> phi_bb3_8;
  TNode<IntPtrT> tmp147;
  TNode<IntPtrT> tmp148;
  TNode<IntPtrT> tmp149;
  TNode<Smi> tmp150;
  TNode<IntPtrT> tmp151;
  TNode<IntPtrT> tmp152;
  TNode<UintPtrT> tmp153;
  TNode<UintPtrT> tmp154;
  TNode<BoolT> tmp155;
  if (block3.is_used()) {
    ca_.Bind(&block3, &phi_bb3_8);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 274);
    tmp147 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp148 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp149 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp150 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp0, tmp149});
    tmp151 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp150});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp152 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAnyRejectElementContextSlots::kPromiseAnyRejectElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp153 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp152});
    tmp154 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp151});
    tmp155 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp153}, TNode<UintPtrT>{tmp154});
    ca_.Branch(tmp155, &block83, std::vector<Node*>{phi_bb3_8}, &block84, std::vector<Node*>{phi_bb3_8});
  }

  TNode<Smi> phi_bb83_8;
  TNode<IntPtrT> tmp156;
  TNode<IntPtrT> tmp157;
  TNode<IntPtrT> tmp158;
  TNode<HeapObject> tmp159;
  TNode<IntPtrT> tmp160;
  TNode<Object> tmp161;
  TNode<Smi> tmp162;
  TNode<Smi> tmp163;
  TNode<Smi> tmp164;
  TNode<IntPtrT> tmp165;
  TNode<IntPtrT> tmp166;
  TNode<IntPtrT> tmp167;
  TNode<Smi> tmp168;
  TNode<IntPtrT> tmp169;
  TNode<IntPtrT> tmp170;
  TNode<UintPtrT> tmp171;
  TNode<UintPtrT> tmp172;
  TNode<BoolT> tmp173;
  if (block83.is_used()) {
    ca_.Bind(&block83, &phi_bb83_8);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp156 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp157 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp152}, TNode<IntPtrT>{tmp156});
    tmp158 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp147}, TNode<IntPtrT>{tmp157});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp159, tmp160) = NewReference_Object_0(state_, TNode<HeapObject>{tmp0}, TNode<IntPtrT>{tmp158}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 274);
    tmp161 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp159, tmp160});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 273);
    tmp162 = UnsafeCast_Smi_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp161});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 276);
    tmp163 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp164 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp162}, TNode<Smi>{tmp163});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 277);
    tmp165 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp166 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp167 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp168 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp0, tmp167});
    tmp169 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp168});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp170 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAnyRejectElementContextSlots::kPromiseAnyRejectElementRemainingSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp171 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp170});
    tmp172 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp169});
    tmp173 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp171}, TNode<UintPtrT>{tmp172});
    ca_.Branch(tmp173, &block90, std::vector<Node*>{phi_bb83_8}, &block91, std::vector<Node*>{phi_bb83_8});
  }

  TNode<Smi> phi_bb84_8;
  if (block84.is_used()) {
    ca_.Bind(&block84, &phi_bb84_8);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb90_8;
  TNode<IntPtrT> tmp174;
  TNode<IntPtrT> tmp175;
  TNode<IntPtrT> tmp176;
  TNode<HeapObject> tmp177;
  TNode<IntPtrT> tmp178;
  TNode<Smi> tmp179;
  TNode<BoolT> tmp180;
  if (block90.is_used()) {
    ca_.Bind(&block90, &phi_bb90_8);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp174 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp175 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp170}, TNode<IntPtrT>{tmp174});
    tmp176 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp165}, TNode<IntPtrT>{tmp175});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp177, tmp178) = NewReference_Object_0(state_, TNode<HeapObject>{tmp0}, TNode<IntPtrT>{tmp176}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 277);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp177, tmp178}, tmp164);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 282);
    tmp179 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp180 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp164}, TNode<Smi>{tmp179});
    ca_.Branch(tmp180, &block93, std::vector<Node*>{phi_bb90_8}, &block94, std::vector<Node*>{phi_bb90_8});
  }

  TNode<Smi> phi_bb91_8;
  if (block91.is_used()) {
    ca_.Bind(&block91, &phi_bb91_8);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb93_8;
  TNode<IntPtrT> tmp181;
  TNode<IntPtrT> tmp182;
  TNode<IntPtrT> tmp183;
  TNode<Smi> tmp184;
  TNode<IntPtrT> tmp185;
  TNode<IntPtrT> tmp186;
  TNode<UintPtrT> tmp187;
  TNode<UintPtrT> tmp188;
  TNode<BoolT> tmp189;
  if (block93.is_used()) {
    ca_.Bind(&block93, &phi_bb93_8);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 289);
    tmp181 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp182 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp183 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/contexts.tq", 11);
    tmp184 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp0, tmp183});
    tmp185 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp184});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 49);
    tmp186 = Convert_intptr_constexpr_int31_0(state_, PromiseBuiltins::PromiseAnyRejectElementContextSlots::kPromiseAnyRejectElementErrorsSlot);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp187 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp186});
    tmp188 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp185});
    tmp189 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp187}, TNode<UintPtrT>{tmp188});
    ca_.Branch(tmp189, &block99, std::vector<Node*>{phi_bb93_8}, &block100, std::vector<Node*>{phi_bb93_8});
  }

  TNode<Smi> phi_bb99_8;
  TNode<IntPtrT> tmp190;
  TNode<IntPtrT> tmp191;
  TNode<IntPtrT> tmp192;
  TNode<HeapObject> tmp193;
  TNode<IntPtrT> tmp194;
  TNode<Object> tmp195;
  TNode<FixedArray> tmp196;
  TNode<JSObject> tmp197;
  if (block99.is_used()) {
    ca_.Bind(&block99, &phi_bb99_8);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp190 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp191 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp186}, TNode<IntPtrT>{tmp190});
    tmp192 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp181}, TNode<IntPtrT>{tmp191});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp193, tmp194) = NewReference_Object_0(state_, TNode<HeapObject>{tmp0}, TNode<IntPtrT>{tmp192}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 289);
    tmp195 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp193, tmp194});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 288);
    tmp196 = UnsafeCast_FixedArray_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp195});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 293);
    tmp197 = ConstructAggregateError_0(state_, TNode<Context>{p_context}, TNode<FixedArray>{tmp196});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 295);
    ca_.Goto(&block1, tmp197);
  }

  TNode<Smi> phi_bb100_8;
  if (block100.is_used()) {
    ca_.Bind(&block100, &phi_bb100_8);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 50);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:50:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb94_8;
  TNode<IntPtrT> tmp198;
  TNode<HeapObject> tmp199;
  if (block94.is_used()) {
    ca_.Bind(&block94, &phi_bb94_8);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 298);
    tmp198 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp199 = CodeStubAssembler(state_).LoadReference<HeapObject>(CodeStubAssembler::Reference{p_resultCapability, tmp198});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 141);
    ca_.Goto(&block102);
  }

  TNode<Object> phi_bb1_0;
  if (block1.is_used()) {
    ca_.Bind(&block1, &phi_bb1_0);
    *label_Reject_parameter_0 = phi_bb1_0;
    ca_.Goto(label_Reject);
  }

    ca_.Bind(&block102);
  return TNode<Object>{tmp199};
}

TF_BUILTIN(PromiseAny, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::kReceiver));
USE(parameter1);
  TNode<Object> parameter2 = UncheckedCast<Object>(Parameter(Descriptor::kIterable));
  USE(parameter2);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block12(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block15(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block14(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block10(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Object> block8(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block17(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block16(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<NativeContext> tmp0;
  TNode<JSReceiver> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 305);
    tmp0 = CodeStubAssembler(state_).LoadNativeContext(TNode<Context>{parameter0});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 308);
    compiler::CodeAssemblerLabel label2(&ca_);
    tmp1 = Cast_JSReceiver_1(state_, TNode<Context>{parameter0}, TNode<Object>{parameter1}, &label2);
    ca_.Goto(&block3);
    if (label2.is_used()) {
      ca_.Bind(&label2);
      ca_.Goto(&block4);
    }
  }

  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 309);
    CodeStubAssembler(state_).ThrowTypeError(TNode<Context>{parameter0}, MessageTemplate::kCalledOnNonObject, "Promise.any");
  }

  TNode<Oddball> tmp3;
  TNode<PromiseCapability> tmp4;
  TNode<BoolT> tmp5;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 312);
    tmp3 = False_0(state_);
    tmp4 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kNewPromiseCapability, parameter0, tmp1, tmp3));
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 315);
    tmp5 = Is_Constructor_JSReceiver_0(state_, TNode<Context>{parameter0}, TNode<JSReceiver>{tmp1});
    ca_.Branch(tmp5, &block5, std::vector<Node*>{}, &block6, std::vector<Node*>{});
  }

  if (block6.is_used()) {
    ca_.Bind(&block6);
    CodeStubAssembler(state_).FailAssert("Torque assert 'Is<Constructor>(receiver)' failed", "src/builtins/promise-any.tq", 315);
  }

  TNode<JSReceiver> tmp6;
  TNode<Object> tmp7;
      TNode<Object> tmp9;
  TNode<JSReceiver> tmp10;
  TNode<Object> tmp11;
      TNode<Object> tmp13;
  TNode<Object> tmp14;
    compiler::TypedCodeAssemblerVariable<Object> tmp16(&ca_);
      TNode<Object> tmp18;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 316);
    tmp6 = UnsafeCast_Constructor_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp1});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 323);
    compiler::CodeAssemblerExceptionHandlerLabel catch8__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch8__label);
    tmp7 = GetPromiseResolve_0(state_, TNode<Context>{parameter0}, TNode<NativeContext>{tmp0}, TNode<JSReceiver>{tmp6});
    }
    if (catch8__label.is_used()) {
      compiler::CodeAssemblerLabel catch8_skip(&ca_);
      ca_.Goto(&catch8_skip);
      ca_.Bind(&catch8__label, &tmp9);
      ca_.Goto(&block11);
      ca_.Bind(&catch8_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 329);
    compiler::CodeAssemblerExceptionHandlerLabel catch12__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch12__label);
    std::tie(tmp10, tmp11) = IteratorBuiltinsAssembler(state_).GetIterator(TNode<Context>{parameter0}, TNode<Object>{parameter2}).Flatten();
    }
    if (catch12__label.is_used()) {
      compiler::CodeAssemblerLabel catch12_skip(&ca_);
      ca_.Goto(&catch12_skip);
      ca_.Bind(&catch12__label, &tmp13);
      ca_.Goto(&block12);
      ca_.Bind(&catch12_skip);
    }
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 344);
    compiler::CodeAssemblerLabel label15(&ca_);
    compiler::CodeAssemblerExceptionHandlerLabel catch17__label(&ca_, compiler::CodeAssemblerLabel::kDeferred);
    { compiler::ScopedExceptionHandler s(&ca_, &catch17__label);
    tmp14 = PerformPromiseAny_0(state_, TNode<Context>{parameter0}, TNode<NativeContext>{tmp0}, TorqueStructIteratorRecord{TNode<JSReceiver>{tmp10}, TNode<Object>{tmp11}}, TNode<JSReceiver>{tmp6}, TNode<PromiseCapability>{tmp4}, TNode<Object>{tmp7}, &label15, &tmp16);
    }
    if (catch17__label.is_used()) {
      compiler::CodeAssemblerLabel catch17_skip(&ca_);
      ca_.Goto(&catch17_skip);
      ca_.Bind(&catch17__label, &tmp18);
      ca_.Goto(&block15);
      ca_.Bind(&catch17_skip);
    }
    ca_.Goto(&block13);
    if (label15.is_used()) {
      ca_.Bind(&label15);
      ca_.Goto(&block14);
    }
  }

  if (block11.is_used()) {
    ca_.Bind(&block11);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 323);
    ca_.Goto(&block10, tmp9);
  }

  if (block12.is_used()) {
    ca_.Bind(&block12);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 329);
    ca_.Goto(&block10, tmp13);
  }

  if (block15.is_used()) {
    ca_.Bind(&block15);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 344);
    ca_.Goto(&block10, tmp18);
  }

  if (block14.is_used()) {
    ca_.Bind(&block14);
    ca_.Goto(&block8, tmp16.value());
  }

  if (block13.is_used()) {
    ca_.Bind(&block13);
    CodeStubAssembler(state_).Return(tmp14);
  }

  TNode<Object> phi_bb10_7;
  if (block10.is_used()) {
    ca_.Bind(&block10, &phi_bb10_7);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 349);
    ca_.Goto(&block8, phi_bb10_7);
  }

  TNode<Object> phi_bb8_7;
  TNode<Oddball> tmp19;
  TNode<BoolT> tmp20;
  if (block8.is_used()) {
    ca_.Bind(&block8, &phi_bb8_7);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 352);
    tmp19 = TheHole_0(state_);
    tmp20 = CodeStubAssembler(state_).TaggedNotEqual(TNode<Object>{phi_bb8_7}, TNode<HeapObject>{tmp19});
    ca_.Branch(tmp20, &block16, std::vector<Node*>{}, &block17, std::vector<Node*>{});
  }

  if (block17.is_used()) {
    ca_.Bind(&block17);
    CodeStubAssembler(state_).FailAssert("Torque assert 'e != TheHole' failed", "src/builtins/promise-any.tq", 352);
  }

  TNode<IntPtrT> tmp21;
  TNode<Object> tmp22;
  TNode<JSReceiver> tmp23;
  TNode<Oddball> tmp24;
  TNode<Object> tmp25;
  TNode<Object> tmp26;
  TNode<IntPtrT> tmp27;
  TNode<HeapObject> tmp28;
  if (block16.is_used()) {
    ca_.Bind(&block16);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 354);
    tmp21 = FromConstexpr_intptr_constexpr_int31_0(state_, 12);
    tmp22 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp4, tmp21});
    tmp23 = UnsafeCast_Callable_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp22});
    tmp24 = Undefined_0(state_);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 355);
    tmp25 = UnsafeCast_JSAny_0(state_, TNode<Context>{parameter0}, TNode<Object>{phi_bb8_7});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 353);
    tmp26 = CodeStubAssembler(state_).Call(TNode<Context>{parameter0}, TNode<Object>{tmp23}, TNode<Object>{tmp24}, TNode<Object>{tmp25});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 356);
    tmp27 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp28 = CodeStubAssembler(state_).LoadReference<HeapObject>(CodeStubAssembler::Reference{tmp4, tmp27});
    CodeStubAssembler(state_).Return(tmp28);
  }
}

TNode<JSObject> ConstructAggregateError_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<FixedArray> p_errors) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Smi> tmp0;
  TNode<JSObject> tmp1;
  TNode<JSArray> tmp2;
  TNode<String> tmp3;
  TNode<Smi> tmp4;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 363);
    tmp0 = CodeStubAssembler(state_).SmiConstant(MessageTemplate::kAllPromisesRejected);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 362);
    tmp1 = TORQUE_CAST(CodeStubAssembler(state_).CallRuntime(Runtime::kConstructInternalAggregateErrorHelper, p_context, tmp0)); 
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 364);
    tmp2 = CreateJSArrayWithElements_0(state_, TNode<Context>{p_context}, TNode<FixedArray>{p_errors});
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 366);
    tmp3 = CodeStubAssembler(state_).ErrorsStringConstant();
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 367);
    tmp4 = CodeStubAssembler(state_).SmiConstant(PropertyAttributes::DONT_ENUM);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 365);
    CodeStubAssembler(state_).CallRuntime(Runtime::kSetOwnPropertyIgnoreAttributes, p_context, tmp1, tmp3, tmp2, tmp4);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 360);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<JSObject>{tmp1};
}

TNode<BoolT> Is_JSPromise_JSAny_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<Object> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<JSPromise> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/cast.tq", 616);
    compiler::CodeAssemblerLabel label1(&ca_);
    tmp0 = Cast_JSPromise_1(state_, TNode<Context>{p_context}, TNode<Object>{p_o}, &label1);
    ca_.Goto(&block4);
    if (label1.is_used()) {
      ca_.Bind(&label1);
      ca_.Goto(&block5);
    }
  }

  TNode<BoolT> tmp2;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    tmp2 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block1, tmp2);
  }

  TNode<BoolT> tmp3;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../src/builtins/cast.tq", 617);
    tmp3 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    ca_.Goto(&block1, tmp3);
  }

  TNode<BoolT> phi_bb1_2;
  if (block1.is_used()) {
    ca_.Bind(&block1, &phi_bb1_2);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 256);
    ca_.Goto(&block6);
  }

    ca_.Bind(&block6);
  return TNode<BoolT>{phi_bb1_2};
}

TNode<BoolT> Is_Constructor_JSReceiver_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<JSReceiver> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<JSReceiver> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/cast.tq", 616);
    compiler::CodeAssemblerLabel label1(&ca_);
    tmp0 = Cast_Constructor_0(state_, TNode<HeapObject>{p_o}, &label1);
    ca_.Goto(&block4);
    if (label1.is_used()) {
      ca_.Bind(&label1);
      ca_.Goto(&block5);
    }
  }

  TNode<BoolT> tmp2;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    tmp2 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block1, tmp2);
  }

  TNode<BoolT> tmp3;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../src/builtins/cast.tq", 617);
    tmp3 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    ca_.Goto(&block1, tmp3);
  }

  TNode<BoolT> phi_bb1_2;
  if (block1.is_used()) {
    ca_.Bind(&block1, &phi_bb1_2);
    ca_.SetSourcePosition("../../src/builtins/promise-any.tq", 315);
    ca_.Goto(&block6);
  }

    ca_.Bind(&block6);
  return TNode<BoolT>{phi_bb1_2};
}

}  // namespace internal
}  // namespace v8

