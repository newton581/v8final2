#ifndef V8_GEN_TORQUE_GENERATED_SRC_OBJECTS_FOREIGN_TQ_H_
#define V8_GEN_TORQUE_GENERATED_SRC_OBJECTS_FOREIGN_TQ_H_

#include "src/builtins/builtins-promise.h"
#include "src/compiler/code-assembler.h"
#include "src/codegen/code-stub-assembler.h"
#include "src/utils/utils.h"
#include "torque-generated/field-offsets-tq.h"
#include "torque-generated/csa-types-tq.h"

namespace v8 {
namespace internal {

TNode<Foreign> Cast_Foreign_0(compiler::CodeAssemblerState* state_, TNode<HeapObject> p_obj, compiler::CodeAssemblerLabel* label_CastError);
TNode<ExternalPointerT> LoadForeignForeignAddress_0(compiler::CodeAssemblerState* state_, TNode<Foreign> p_o);
void StoreForeignForeignAddress_0(compiler::CodeAssemblerState* state_, TNode<Foreign> p_o, TNode<ExternalPointerT> p_v);
TNode<Foreign> DownCastForTorqueClass_Foreign_0(compiler::CodeAssemblerState* state_, TNode<HeapObject> p_o, compiler::CodeAssemblerLabel* label_CastError);
}  // namespace internal
}  // namespace v8

#endif  // V8_GEN_TORQUE_GENERATED_SRC_OBJECTS_FOREIGN_TQ_H_
