#include "src/builtins/builtins-array-gen.h"
#include "src/builtins/builtins-bigint-gen.h"
#include "src/builtins/builtins-collections-gen.h"
#include "src/builtins/builtins-constructor-gen.h"
#include "src/builtins/builtins-data-view-gen.h"
#include "src/builtins/builtins-iterator-gen.h"
#include "src/builtins/builtins-promise-gen.h"
#include "src/builtins/builtins-promise.h"
#include "src/builtins/builtins-proxy-gen.h"
#include "src/builtins/builtins-regexp-gen.h"
#include "src/builtins/builtins-string-gen.h"
#include "src/builtins/builtins-typed-array-gen.h"
#include "src/builtins/builtins-utils-gen.h"
#include "src/builtins/builtins-wasm-gen.h"
#include "src/builtins/builtins.h"
#include "src/codegen/code-factory.h"
#include "src/heap/factory-inl.h"
#include "src/ic/binary-op-assembler.h"
#include "src/objects/arguments.h"
#include "src/objects/bigint.h"
#include "src/objects/elements-kind.h"
#include "src/objects/free-space.h"
#include "src/objects/js-break-iterator.h"
#include "src/objects/js-collator.h"
#include "src/objects/js-date-time-format.h"
#include "src/objects/js-display-names.h"
#include "src/objects/js-generator.h"
#include "src/objects/js-list-format.h"
#include "src/objects/js-locale.h"
#include "src/objects/js-number-format.h"
#include "src/objects/js-objects.h"
#include "src/objects/js-plural-rules.h"
#include "src/objects/js-promise.h"
#include "src/objects/js-regexp-string-iterator.h"
#include "src/objects/js-relative-time-format.h"
#include "src/objects/js-segment-iterator.h"
#include "src/objects/js-segmenter.h"
#include "src/objects/js-weak-refs.h"
#include "src/objects/objects.h"
#include "src/objects/ordered-hash-table.h"
#include "src/objects/property-array.h"
#include "src/objects/property-descriptor-object.h"
#include "src/objects/source-text-module.h"
#include "src/objects/stack-frame-info.h"
#include "src/objects/synthetic-module.h"
#include "src/objects/template-objects.h"
#include "src/torque/runtime-support.h"
#include "torque-generated/src/builtins/aggregate-error-tq-csa.h"
#include "torque-generated/src/builtins/array-copywithin-tq-csa.h"
#include "torque-generated/src/builtins/array-every-tq-csa.h"
#include "torque-generated/src/builtins/array-filter-tq-csa.h"
#include "torque-generated/src/builtins/array-find-tq-csa.h"
#include "torque-generated/src/builtins/array-findindex-tq-csa.h"
#include "torque-generated/src/builtins/array-foreach-tq-csa.h"
#include "torque-generated/src/builtins/array-from-tq-csa.h"
#include "torque-generated/src/builtins/array-isarray-tq-csa.h"
#include "torque-generated/src/builtins/array-join-tq-csa.h"
#include "torque-generated/src/builtins/array-lastindexof-tq-csa.h"
#include "torque-generated/src/builtins/array-map-tq-csa.h"
#include "torque-generated/src/builtins/array-of-tq-csa.h"
#include "torque-generated/src/builtins/array-reduce-right-tq-csa.h"
#include "torque-generated/src/builtins/array-reduce-tq-csa.h"
#include "torque-generated/src/builtins/array-reverse-tq-csa.h"
#include "torque-generated/src/builtins/array-shift-tq-csa.h"
#include "torque-generated/src/builtins/array-slice-tq-csa.h"
#include "torque-generated/src/builtins/array-some-tq-csa.h"
#include "torque-generated/src/builtins/array-splice-tq-csa.h"
#include "torque-generated/src/builtins/array-unshift-tq-csa.h"
#include "torque-generated/src/builtins/array-tq-csa.h"
#include "torque-generated/src/builtins/base-tq-csa.h"
#include "torque-generated/src/builtins/bigint-tq-csa.h"
#include "torque-generated/src/builtins/boolean-tq-csa.h"
#include "torque-generated/src/builtins/builtins-string-tq-csa.h"
#include "torque-generated/src/builtins/cast-tq-csa.h"
#include "torque-generated/src/builtins/collections-tq-csa.h"
#include "torque-generated/src/builtins/conversion-tq-csa.h"
#include "torque-generated/src/builtins/convert-tq-csa.h"
#include "torque-generated/src/builtins/console-tq-csa.h"
#include "torque-generated/src/builtins/data-view-tq-csa.h"
#include "torque-generated/src/builtins/finalization-registry-tq-csa.h"
#include "torque-generated/src/builtins/frames-tq-csa.h"
#include "torque-generated/src/builtins/frame-arguments-tq-csa.h"
#include "torque-generated/src/builtins/function-tq-csa.h"
#include "torque-generated/src/builtins/growable-fixed-array-tq-csa.h"
#include "torque-generated/src/builtins/ic-callable-tq-csa.h"
#include "torque-generated/src/builtins/ic-tq-csa.h"
#include "torque-generated/src/builtins/internal-coverage-tq-csa.h"
#include "torque-generated/src/builtins/internal-tq-csa.h"
#include "torque-generated/src/builtins/iterator-tq-csa.h"
#include "torque-generated/src/builtins/math-tq-csa.h"
#include "torque-generated/src/builtins/number-tq-csa.h"
#include "torque-generated/src/builtins/object-fromentries-tq-csa.h"
#include "torque-generated/src/builtins/object-tq-csa.h"
#include "torque-generated/src/builtins/promise-abstract-operations-tq-csa.h"
#include "torque-generated/src/builtins/promise-all-tq-csa.h"
#include "torque-generated/src/builtins/promise-all-element-closure-tq-csa.h"
#include "torque-generated/src/builtins/promise-any-tq-csa.h"
#include "torque-generated/src/builtins/promise-constructor-tq-csa.h"
#include "torque-generated/src/builtins/promise-finally-tq-csa.h"
#include "torque-generated/src/builtins/promise-misc-tq-csa.h"
#include "torque-generated/src/builtins/promise-race-tq-csa.h"
#include "torque-generated/src/builtins/promise-reaction-job-tq-csa.h"
#include "torque-generated/src/builtins/promise-resolve-tq-csa.h"
#include "torque-generated/src/builtins/promise-then-tq-csa.h"
#include "torque-generated/src/builtins/promise-jobs-tq-csa.h"
#include "torque-generated/src/builtins/proxy-constructor-tq-csa.h"
#include "torque-generated/src/builtins/proxy-delete-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-get-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-get-prototype-of-tq-csa.h"
#include "torque-generated/src/builtins/proxy-has-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-is-extensible-tq-csa.h"
#include "torque-generated/src/builtins/proxy-prevent-extensions-tq-csa.h"
#include "torque-generated/src/builtins/proxy-revocable-tq-csa.h"
#include "torque-generated/src/builtins/proxy-revoke-tq-csa.h"
#include "torque-generated/src/builtins/proxy-set-property-tq-csa.h"
#include "torque-generated/src/builtins/proxy-set-prototype-of-tq-csa.h"
#include "torque-generated/src/builtins/proxy-tq-csa.h"
#include "torque-generated/src/builtins/reflect-tq-csa.h"
#include "torque-generated/src/builtins/regexp-exec-tq-csa.h"
#include "torque-generated/src/builtins/regexp-match-all-tq-csa.h"
#include "torque-generated/src/builtins/regexp-match-tq-csa.h"
#include "torque-generated/src/builtins/regexp-replace-tq-csa.h"
#include "torque-generated/src/builtins/regexp-search-tq-csa.h"
#include "torque-generated/src/builtins/regexp-source-tq-csa.h"
#include "torque-generated/src/builtins/regexp-split-tq-csa.h"
#include "torque-generated/src/builtins/regexp-test-tq-csa.h"
#include "torque-generated/src/builtins/regexp-tq-csa.h"
#include "torque-generated/src/builtins/string-endswith-tq-csa.h"
#include "torque-generated/src/builtins/string-html-tq-csa.h"
#include "torque-generated/src/builtins/string-iterator-tq-csa.h"
#include "torque-generated/src/builtins/string-pad-tq-csa.h"
#include "torque-generated/src/builtins/string-repeat-tq-csa.h"
#include "torque-generated/src/builtins/string-replaceall-tq-csa.h"
#include "torque-generated/src/builtins/string-slice-tq-csa.h"
#include "torque-generated/src/builtins/string-startswith-tq-csa.h"
#include "torque-generated/src/builtins/string-substring-tq-csa.h"
#include "torque-generated/src/builtins/string-substr-tq-csa.h"
#include "torque-generated/src/builtins/symbol-tq-csa.h"
#include "torque-generated/src/builtins/torque-internal-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-createtypedarray-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-every-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-entries-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-filter-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-find-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-findindex-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-foreach-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-from-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-keys-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-of-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-reduce-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-reduceright-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-set-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-slice-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-some-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-sort-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-subarray-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-values-tq-csa.h"
#include "torque-generated/src/builtins/typed-array-tq-csa.h"
#include "torque-generated/src/builtins/wasm-tq-csa.h"
#include "torque-generated/src/builtins/weak-ref-tq-csa.h"
#include "torque-generated/src/ic/handler-configuration-tq-csa.h"
#include "torque-generated/src/objects/allocation-site-tq-csa.h"
#include "torque-generated/src/objects/api-callbacks-tq-csa.h"
#include "torque-generated/src/objects/arguments-tq-csa.h"
#include "torque-generated/src/objects/cell-tq-csa.h"
#include "torque-generated/src/objects/code-tq-csa.h"
#include "torque-generated/src/objects/contexts-tq-csa.h"
#include "torque-generated/src/objects/data-handler-tq-csa.h"
#include "torque-generated/src/objects/debug-objects-tq-csa.h"
#include "torque-generated/src/objects/descriptor-array-tq-csa.h"
#include "torque-generated/src/objects/embedder-data-array-tq-csa.h"
#include "torque-generated/src/objects/feedback-cell-tq-csa.h"
#include "torque-generated/src/objects/feedback-vector-tq-csa.h"
#include "torque-generated/src/objects/fixed-array-tq-csa.h"
#include "torque-generated/src/objects/foreign-tq-csa.h"
#include "torque-generated/src/objects/free-space-tq-csa.h"
#include "torque-generated/src/objects/heap-number-tq-csa.h"
#include "torque-generated/src/objects/heap-object-tq-csa.h"
#include "torque-generated/src/objects/intl-objects-tq-csa.h"
#include "torque-generated/src/objects/js-array-buffer-tq-csa.h"
#include "torque-generated/src/objects/js-array-tq-csa.h"
#include "torque-generated/src/objects/js-collection-iterator-tq-csa.h"
#include "torque-generated/src/objects/js-collection-tq-csa.h"
#include "torque-generated/src/objects/js-generator-tq-csa.h"
#include "torque-generated/src/objects/js-objects-tq-csa.h"
#include "torque-generated/src/objects/js-promise-tq-csa.h"
#include "torque-generated/src/objects/js-proxy-tq-csa.h"
#include "torque-generated/src/objects/js-regexp-string-iterator-tq-csa.h"
#include "torque-generated/src/objects/js-regexp-tq-csa.h"
#include "torque-generated/src/objects/js-weak-refs-tq-csa.h"
#include "torque-generated/src/objects/literal-objects-tq-csa.h"
#include "torque-generated/src/objects/map-tq-csa.h"
#include "torque-generated/src/objects/microtask-tq-csa.h"
#include "torque-generated/src/objects/module-tq-csa.h"
#include "torque-generated/src/objects/name-tq-csa.h"
#include "torque-generated/src/objects/oddball-tq-csa.h"
#include "torque-generated/src/objects/ordered-hash-table-tq-csa.h"
#include "torque-generated/src/objects/primitive-heap-object-tq-csa.h"
#include "torque-generated/src/objects/promise-tq-csa.h"
#include "torque-generated/src/objects/property-array-tq-csa.h"
#include "torque-generated/src/objects/property-cell-tq-csa.h"
#include "torque-generated/src/objects/property-descriptor-object-tq-csa.h"
#include "torque-generated/src/objects/prototype-info-tq-csa.h"
#include "torque-generated/src/objects/regexp-match-info-tq-csa.h"
#include "torque-generated/src/objects/scope-info-tq-csa.h"
#include "torque-generated/src/objects/script-tq-csa.h"
#include "torque-generated/src/objects/shared-function-info-tq-csa.h"
#include "torque-generated/src/objects/source-text-module-tq-csa.h"
#include "torque-generated/src/objects/stack-frame-info-tq-csa.h"
#include "torque-generated/src/objects/string-tq-csa.h"
#include "torque-generated/src/objects/struct-tq-csa.h"
#include "torque-generated/src/objects/synthetic-module-tq-csa.h"
#include "torque-generated/src/objects/template-objects-tq-csa.h"
#include "torque-generated/src/objects/template-tq-csa.h"
#include "torque-generated/src/wasm/wasm-objects-tq-csa.h"
#include "torque-generated/test/torque/test-torque-tq-csa.h"
#include "torque-generated/third_party/v8/builtins/array-sort-tq-csa.h"

namespace v8 {
namespace internal {

TNode<SortState> Cast_SortState_0(compiler::CodeAssemblerState* state_, TNode<HeapObject> p_obj, compiler::CodeAssemblerLabel* label_CastError) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<SortState> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 17);
    compiler::CodeAssemblerLabel label1(&ca_);
    tmp0 = DownCastForTorqueClass_SortState_0(state_, TNode<HeapObject>{p_obj}, &label1);
    ca_.Goto(&block3);
    if (label1.is_used()) {
      ca_.Bind(&label1);
      ca_.Goto(&block4);
    }
  }

  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.Goto(label_CastError);
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.Goto(&block5);
  }

    ca_.Bind(&block5);
  return TNode<SortState>{tmp0};
}

TNode<IntPtrT> CalculateWorkArrayLength_0(compiler::CodeAssemblerState* state_, TNode<JSReceiver> p_receiver, TNode<Number> p_initialReceiverLength) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<UintPtrT> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<UintPtrT> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<UintPtrT> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<UintPtrT> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<UintPtrT> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<UintPtrT, IntPtrT> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<UintPtrT, IntPtrT> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<IntPtrT> block14(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<UintPtrT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 116);
    compiler::CodeAssemblerLabel label1(&ca_);
    tmp0 = ChangeSafeIntegerNumberToUintPtr_0(state_, TNode<Number>{p_initialReceiverLength}, &label1);
    ca_.Goto(&block4);
    if (label1.is_used()) {
      ca_.Bind(&label1);
      ca_.Goto(&block5);
    }
  }

  TNode<UintPtrT> tmp2;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 122);
    tmp2 = FromConstexpr_uintptr_constexpr_uintptr_0(state_, kSmiMaxValue);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 114);
    ca_.Goto(&block2, tmp2);
  }

  TNode<UintPtrT> tmp3;
  TNode<BoolT> tmp4;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 118);
    tmp3 = FromConstexpr_uintptr_constexpr_uintptr_0(state_, kSmiMaxValue);
    tmp4 = CodeStubAssembler(state_).UintPtrGreaterThan(TNode<UintPtrT>{tmp0}, TNode<UintPtrT>{tmp3});
    ca_.Branch(tmp4, &block6, std::vector<Node*>{}, &block7, std::vector<Node*>{tmp0});
  }

  TNode<UintPtrT> tmp5;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 119);
    tmp5 = FromConstexpr_uintptr_constexpr_uintptr_0(state_, kSmiMaxValue);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 118);
    ca_.Goto(&block7, tmp5);
  }

  TNode<UintPtrT> phi_bb7_2;
  if (block7.is_used()) {
    ca_.Bind(&block7, &phi_bb7_2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 121);
    ca_.Goto(&block2, phi_bb7_2);
  }

  TNode<UintPtrT> phi_bb2_2;
  TNode<IntPtrT> tmp6;
  TNode<JSObject> tmp7;
  if (block2.is_used()) {
    ca_.Bind(&block2, &phi_bb2_2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 125);
    tmp6 = Convert_intptr_uintptr_0(state_, TNode<UintPtrT>{phi_bb2_2});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 127);
    compiler::CodeAssemblerLabel label8(&ca_);
    tmp7 = Cast_JSObject_0(state_, TNode<HeapObject>{p_receiver}, &label8);
    ca_.Goto(&block10, phi_bb2_2);
    if (label8.is_used()) {
      ca_.Bind(&label8);
      ca_.Goto(&block11, phi_bb2_2);
    }
  }

  TNode<UintPtrT> phi_bb11_2;
  if (block11.is_used()) {
    ca_.Bind(&block11, &phi_bb11_2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 126);
    ca_.Goto(&block8, phi_bb11_2, tmp6);
  }

  TNode<UintPtrT> phi_bb10_2;
  TNode<IntPtrT> tmp9;
  TNode<FixedArrayBase> tmp10;
  TNode<IntPtrT> tmp11;
  TNode<Smi> tmp12;
  TNode<IntPtrT> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<BoolT> tmp15;
  if (block10.is_used()) {
    ca_.Bind(&block10, &phi_bb10_2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 128);
    tmp9 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp10 = CodeStubAssembler(state_).LoadReference<FixedArrayBase>(CodeStubAssembler::Reference{tmp7, tmp9});
    tmp11 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp12 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp10, tmp11});
    tmp13 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp12});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 133);
    tmp14 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp15 = CodeStubAssembler(state_).WordNotEqual(TNode<IntPtrT>{tmp13}, TNode<IntPtrT>{tmp14});
    ca_.Branch(tmp15, &block12, std::vector<Node*>{phi_bb10_2}, &block13, std::vector<Node*>{phi_bb10_2, tmp6});
  }

  TNode<UintPtrT> phi_bb12_2;
  TNode<IntPtrT> tmp16;
  if (block12.is_used()) {
    ca_.Bind(&block12, &phi_bb12_2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 134);
    tmp16 = CodeStubAssembler(state_).IntPtrMin(TNode<IntPtrT>{tmp6}, TNode<IntPtrT>{tmp13});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 133);
    ca_.Goto(&block13, phi_bb12_2, tmp16);
  }

  TNode<UintPtrT> phi_bb13_2;
  TNode<IntPtrT> phi_bb13_3;
  if (block13.is_used()) {
    ca_.Bind(&block13, &phi_bb13_2, &phi_bb13_3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 136);
    ca_.Goto(&block8, phi_bb13_2, phi_bb13_3);
  }

  TNode<UintPtrT> phi_bb8_2;
  TNode<IntPtrT> phi_bb8_3;
  if (block8.is_used()) {
    ca_.Bind(&block8, &phi_bb8_2, &phi_bb8_3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 109);
    ca_.Goto(&block14, phi_bb8_3);
  }

  TNode<IntPtrT> phi_bb14_2;
    ca_.Bind(&block14, &phi_bb14_2);
  return TNode<IntPtrT>{phi_bb14_2};
}

TNode<SortState> NewSortState_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<JSReceiver> p_receiver, TNode<HeapObject> p_comparefn, TNode<Number> p_initialReceiverLength) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BuiltinPtr> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block14(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BuiltinPtr, BuiltinPtr, BuiltinPtr, BuiltinPtr> block15(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BuiltinPtr, BuiltinPtr, BuiltinPtr, BuiltinPtr> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BuiltinPtr, BuiltinPtr, BuiltinPtr, BuiltinPtr> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block16(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Oddball> tmp0;
  TNode<BoolT> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 145);
    tmp0 = Undefined_0(state_);
    tmp1 = CodeStubAssembler(state_).TaggedNotEqual(TNode<HeapObject>{p_comparefn}, TNode<HeapObject>{tmp0});
    ca_.Branch(tmp1, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.Goto(&block4, ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kSortCompareUserFn)));
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.Goto(&block4, ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kSortCompareDefault)));
  }

  TNode<BuiltinPtr> phi_bb4_4;
  TNode<IntPtrT> tmp2;
  TNode<Map> tmp3;
  TNode<JSArray> tmp4;
  if (block4.is_used()) {
    ca_.Bind(&block4, &phi_bb4_4);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 146);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp3 = CodeStubAssembler(state_).LoadReference<Map>(CodeStubAssembler::Reference{p_receiver, tmp2});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 153);
    compiler::CodeAssemblerLabel label5(&ca_);
    tmp4 = Cast_FastJSArray_0(state_, TNode<Context>{p_context}, TNode<HeapObject>{p_receiver}, &label5);
    ca_.Goto(&block8);
    if (label5.is_used()) {
      ca_.Bind(&label5);
      ca_.Goto(&block9);
    }
  }

  if (block9.is_used()) {
    ca_.Bind(&block9);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 152);
    ca_.Goto(&block6, ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kLoad_GenericElementsAccessor_0)), ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kStore_GenericElementsAccessor_0)), ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kDelete_GenericElementsAccessor_0)), ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kCanUseSameAccessor_GenericElementsAccessor_0)));
  }

  TNode<Int32T> tmp6;
  TNode<BoolT> tmp7;
  if (block8.is_used()) {
    ca_.Bind(&block8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 156);
    EnsureWriteableFastElements_0(state_, TNode<Context>{p_context}, TNode<JSArray>{tmp4});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 158);
    tmp6 = CodeStubAssembler(state_).LoadMapElementsKind(TNode<Map>{tmp3});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 159);
    tmp7 = CodeStubAssembler(state_).IsDoubleElementsKind(TNode<Int32T>{tmp6});
    ca_.Branch(tmp7, &block10, std::vector<Node*>{}, &block11, std::vector<Node*>{});
  }

  if (block10.is_used()) {
    ca_.Bind(&block10);
    ca_.Goto(&block12, ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kLoad_FastDoubleElements_0)), ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kStore_FastDoubleElements_0)), ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kDelete_FastDoubleElements_0)), ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kCanUseSameAccessor_FastDoubleElements_0)));
  }

  TNode<BoolT> tmp8;
  if (block11.is_used()) {
    ca_.Bind(&block11);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 164);
    tmp8 = CodeStubAssembler(state_).IsFastSmiElementsKind(TNode<Int32T>{tmp6});
    ca_.Branch(tmp8, &block13, std::vector<Node*>{}, &block14, std::vector<Node*>{});
  }

  if (block13.is_used()) {
    ca_.Bind(&block13);
    ca_.Goto(&block15, ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kLoad_FastSmiElements_0)), ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kStore_FastSmiElements_0)), ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kDelete_FastSmiElements_0)), ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kCanUseSameAccessor_FastSmiElements_0)));
  }

  if (block14.is_used()) {
    ca_.Bind(&block14);
    ca_.Goto(&block15, ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kLoad_FastObjectElements_0)), ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kStore_FastObjectElements_0)), ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kDelete_FastObjectElements_0)), ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kCanUseSameAccessor_FastObjectElements_0)));
  }

  TNode<BuiltinPtr> phi_bb15_6;
  TNode<BuiltinPtr> phi_bb15_7;
  TNode<BuiltinPtr> phi_bb15_8;
  TNode<BuiltinPtr> phi_bb15_9;
  if (block15.is_used()) {
    ca_.Bind(&block15, &phi_bb15_6, &phi_bb15_7, &phi_bb15_8, &phi_bb15_9);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 159);
    ca_.Goto(&block12, phi_bb15_6, phi_bb15_7, phi_bb15_8, phi_bb15_9);
  }

  TNode<BuiltinPtr> phi_bb12_6;
  TNode<BuiltinPtr> phi_bb12_7;
  TNode<BuiltinPtr> phi_bb12_8;
  TNode<BuiltinPtr> phi_bb12_9;
  if (block12.is_used()) {
    ca_.Bind(&block12, &phi_bb12_6, &phi_bb12_7, &phi_bb12_8, &phi_bb12_9);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 175);
    ca_.Goto(&block6, phi_bb12_6, phi_bb12_7, phi_bb12_8, phi_bb12_9);
  }

  TNode<BuiltinPtr> phi_bb6_6;
  TNode<BuiltinPtr> phi_bb6_7;
  TNode<BuiltinPtr> phi_bb6_8;
  TNode<BuiltinPtr> phi_bb6_9;
  TNode<IntPtrT> tmp9;
  TNode<IntPtrT> tmp10;
  TNode<FixedArray> tmp11;
  TNode<FixedArray> tmp12;
  TNode<FixedArray> tmp13;
  TNode<Map> tmp14;
  TNode<BoolT> tmp15;
  TNode<IntPtrT> tmp16;
  TNode<HeapObject> tmp17;
  TNode<IntPtrT> tmp18;
  TNode<IntPtrT> tmp19;
  TNode<IntPtrT> tmp20;
  TNode<IntPtrT> tmp21;
  TNode<IntPtrT> tmp22;
  TNode<IntPtrT> tmp23;
  TNode<IntPtrT> tmp24;
  TNode<IntPtrT> tmp25;
  TNode<IntPtrT> tmp26;
  TNode<IntPtrT> tmp27;
  TNode<IntPtrT> tmp28;
  TNode<Smi> tmp29;
  TNode<IntPtrT> tmp30;
  TNode<Smi> tmp31;
  TNode<IntPtrT> tmp32;
  TNode<IntPtrT> tmp33;
  TNode<IntPtrT> tmp34;
  TNode<IntPtrT> tmp35;
  TNode<Smi> tmp36;
  TNode<IntPtrT> tmp37;
  TNode<Smi> tmp38;
  TNode<SortState> tmp39;
  if (block6.is_used()) {
    ca_.Bind(&block6, &phi_bb6_6, &phi_bb6_7, &phi_bb6_8, &phi_bb6_9);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 183);
    tmp9 = CalculateWorkArrayLength_0(state_, TNode<JSReceiver>{p_receiver}, TNode<Number>{p_initialReceiverLength});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 197);
    tmp10 = Convert_intptr_constexpr_int31_0(state_, kMaxMergePending_0(state_));
    tmp11 = CodeStubAssembler(state_).AllocateZeroedFixedArray(TNode<IntPtrT>{tmp10});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 198);
    tmp12 = CodeStubAssembler(state_).AllocateZeroedFixedArray(TNode<IntPtrT>{tmp9});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 199);
    tmp13 = kEmptyFixedArray_0(state_);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 185);
    tmp14 = CodeStubAssembler(state_).GetInstanceTypeMap(SORT_STATE_TYPE);
    tmp15 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    tmp16 = FromConstexpr_intptr_constexpr_int31_0(state_, 68);
    tmp17 = AllocateFromNew_0(state_, TNode<IntPtrT>{tmp16}, TNode<Map>{tmp14}, TNode<BoolT>{tmp15});
    tmp18 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    CodeStubAssembler(state_).StoreReference<Map>(CodeStubAssembler::Reference{tmp17, tmp18}, tmp14);
    tmp19 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    CodeStubAssembler(state_).StoreReference<JSReceiver>(CodeStubAssembler::Reference{tmp17, tmp19}, p_receiver);
    tmp20 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    CodeStubAssembler(state_).StoreReference<Map>(CodeStubAssembler::Reference{tmp17, tmp20}, tmp3);
    tmp21 = FromConstexpr_intptr_constexpr_int31_0(state_, 12);
    CodeStubAssembler(state_).StoreReference<Number>(CodeStubAssembler::Reference{tmp17, tmp21}, p_initialReceiverLength);
    tmp22 = FromConstexpr_intptr_constexpr_int31_0(state_, 16);
    CodeStubAssembler(state_).StoreReference<HeapObject>(CodeStubAssembler::Reference{tmp17, tmp22}, p_comparefn);
    tmp23 = FromConstexpr_intptr_constexpr_int31_0(state_, 20);
    CodeStubAssembler(state_).StoreReference<BuiltinPtr>(CodeStubAssembler::Reference{tmp17, tmp23}, phi_bb4_4);
    tmp24 = FromConstexpr_intptr_constexpr_int31_0(state_, 24);
    CodeStubAssembler(state_).StoreReference<BuiltinPtr>(CodeStubAssembler::Reference{tmp17, tmp24}, phi_bb6_6);
    tmp25 = FromConstexpr_intptr_constexpr_int31_0(state_, 28);
    CodeStubAssembler(state_).StoreReference<BuiltinPtr>(CodeStubAssembler::Reference{tmp17, tmp25}, phi_bb6_7);
    tmp26 = FromConstexpr_intptr_constexpr_int31_0(state_, 32);
    CodeStubAssembler(state_).StoreReference<BuiltinPtr>(CodeStubAssembler::Reference{tmp17, tmp26}, phi_bb6_8);
    tmp27 = FromConstexpr_intptr_constexpr_int31_0(state_, 36);
    CodeStubAssembler(state_).StoreReference<BuiltinPtr>(CodeStubAssembler::Reference{tmp17, tmp27}, phi_bb6_9);
    tmp28 = FromConstexpr_intptr_constexpr_int31_0(state_, 40);
    tmp29 = FromConstexpr_Smi_constexpr_int31_0(state_, kMinGallopWins_0(state_));
    CodeStubAssembler(state_).StoreReference<Smi>(CodeStubAssembler::Reference{tmp17, tmp28}, tmp29);
    tmp30 = FromConstexpr_intptr_constexpr_int31_0(state_, 44);
    tmp31 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    CodeStubAssembler(state_).StoreReference<Smi>(CodeStubAssembler::Reference{tmp17, tmp30}, tmp31);
    tmp32 = FromConstexpr_intptr_constexpr_int31_0(state_, 48);
    CodeStubAssembler(state_).StoreReference<FixedArray>(CodeStubAssembler::Reference{tmp17, tmp32}, tmp11);
    tmp33 = FromConstexpr_intptr_constexpr_int31_0(state_, 52);
    CodeStubAssembler(state_).StoreReference<FixedArray>(CodeStubAssembler::Reference{tmp17, tmp33}, tmp12);
    tmp34 = FromConstexpr_intptr_constexpr_int31_0(state_, 56);
    CodeStubAssembler(state_).StoreReference<FixedArray>(CodeStubAssembler::Reference{tmp17, tmp34}, tmp13);
    tmp35 = FromConstexpr_intptr_constexpr_int31_0(state_, 60);
    tmp36 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    CodeStubAssembler(state_).StoreReference<Smi>(CodeStubAssembler::Reference{tmp17, tmp35}, tmp36);
    tmp37 = FromConstexpr_intptr_constexpr_int31_0(state_, 64);
    tmp38 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    CodeStubAssembler(state_).StoreReference<Smi>(CodeStubAssembler::Reference{tmp17, tmp37}, tmp38);
    tmp39 = TORQUE_CAST(TNode<HeapObject>{tmp17});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 141);
    ca_.Goto(&block16);
  }

    ca_.Bind(&block16);
  return TNode<SortState>{tmp39};
}

TNode<Smi> kSuccess_0(compiler::CodeAssemblerState* state_) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 205);
  TNode<Smi> tmp0;
    tmp0 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
  return TNode<Smi>{tmp0};
}

int31_t kMaxMergePending_0(compiler::CodeAssemblerState* state_) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

    ca_.Bind(&block0);
  return 85;
}

int31_t kMinGallopWins_0(compiler::CodeAssemblerState* state_) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

    ca_.Bind(&block0);
  return 7;
}

TNode<Smi> kSortStateTempSize_0(compiler::CodeAssemblerState* state_) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 220);
  TNode<Smi> tmp0;
    tmp0 = FromConstexpr_Smi_constexpr_int31_0(state_, 32);
  return TNode<Smi>{tmp0};
}

TF_BUILTIN(Load_FastSmiElements_0, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<SortState> parameter1 = UncheckedCast<SortState>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<Smi> parameter2 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<JSReceiver> tmp1;
  TNode<JSObject> tmp2;
  TNode<IntPtrT> tmp3;
  TNode<FixedArrayBase> tmp4;
  TNode<FixedArray> tmp5;
  TNode<IntPtrT> tmp6;
  TNode<IntPtrT> tmp7;
  TNode<IntPtrT> tmp8;
  TNode<Smi> tmp9;
  TNode<IntPtrT> tmp10;
  TNode<IntPtrT> tmp11;
  TNode<UintPtrT> tmp12;
  TNode<UintPtrT> tmp13;
  TNode<BoolT> tmp14;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 244);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp1 = CodeStubAssembler(state_).LoadReference<JSReceiver>(CodeStubAssembler::Reference{parameter1, tmp0});
    tmp2 = UnsafeCast_JSObject_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp1});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 245);
    tmp3 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp4 = CodeStubAssembler(state_).LoadReference<FixedArrayBase>(CodeStubAssembler::Reference{tmp2, tmp3});
    tmp5 = UnsafeCast_FixedArray_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp4});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 246);
    tmp6 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp7 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp8 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp9 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp5, tmp8});
    tmp10 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp9});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp11 = Convert_intptr_Smi_0(state_, TNode<Smi>{parameter2});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp12 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp11});
    tmp13 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp10});
    tmp14 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp12}, TNode<UintPtrT>{tmp13});
    ca_.Branch(tmp14, &block5, std::vector<Node*>{}, &block6, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp15;
  TNode<IntPtrT> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<HeapObject> tmp18;
  TNode<IntPtrT> tmp19;
  TNode<Object> tmp20;
  TNode<Object> tmp21;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp15 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp16 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp11}, TNode<IntPtrT>{tmp15});
    tmp17 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp6}, TNode<IntPtrT>{tmp16});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp18, tmp19) = NewReference_Object_0(state_, TNode<HeapObject>{tmp5}, TNode<IntPtrT>{tmp17}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 246);
    tmp20 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp18, tmp19});
    tmp21 = UnsafeCast_JSReceiver_OR_Smi_OR_HeapNumber_OR_BigInt_OR_String_OR_Symbol_OR_True_OR_False_OR_Null_OR_Undefined_OR_TheHole_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp20});
    CodeStubAssembler(state_).Return(tmp21);
  }

  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }
}

TF_BUILTIN(Load_FastObjectElements_0, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<SortState> parameter1 = UncheckedCast<SortState>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<Smi> parameter2 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<JSReceiver> tmp1;
  TNode<JSObject> tmp2;
  TNode<IntPtrT> tmp3;
  TNode<FixedArrayBase> tmp4;
  TNode<FixedArray> tmp5;
  TNode<IntPtrT> tmp6;
  TNode<IntPtrT> tmp7;
  TNode<IntPtrT> tmp8;
  TNode<Smi> tmp9;
  TNode<IntPtrT> tmp10;
  TNode<IntPtrT> tmp11;
  TNode<UintPtrT> tmp12;
  TNode<UintPtrT> tmp13;
  TNode<BoolT> tmp14;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 251);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp1 = CodeStubAssembler(state_).LoadReference<JSReceiver>(CodeStubAssembler::Reference{parameter1, tmp0});
    tmp2 = UnsafeCast_JSObject_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp1});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 252);
    tmp3 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp4 = CodeStubAssembler(state_).LoadReference<FixedArrayBase>(CodeStubAssembler::Reference{tmp2, tmp3});
    tmp5 = UnsafeCast_FixedArray_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp4});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 253);
    tmp6 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp7 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp8 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp9 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp5, tmp8});
    tmp10 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp9});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp11 = Convert_intptr_Smi_0(state_, TNode<Smi>{parameter2});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp12 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp11});
    tmp13 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp10});
    tmp14 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp12}, TNode<UintPtrT>{tmp13});
    ca_.Branch(tmp14, &block5, std::vector<Node*>{}, &block6, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp15;
  TNode<IntPtrT> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<HeapObject> tmp18;
  TNode<IntPtrT> tmp19;
  TNode<Object> tmp20;
  TNode<Object> tmp21;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp15 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp16 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp11}, TNode<IntPtrT>{tmp15});
    tmp17 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp6}, TNode<IntPtrT>{tmp16});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp18, tmp19) = NewReference_Object_0(state_, TNode<HeapObject>{tmp5}, TNode<IntPtrT>{tmp17}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 253);
    tmp20 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp18, tmp19});
    tmp21 = UnsafeCast_JSReceiver_OR_Smi_OR_HeapNumber_OR_BigInt_OR_String_OR_Symbol_OR_True_OR_False_OR_Null_OR_Undefined_OR_TheHole_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp20});
    CodeStubAssembler(state_).Return(tmp21);
  }

  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }
}

TF_BUILTIN(Load_FastDoubleElements_0, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<SortState> parameter1 = UncheckedCast<SortState>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<Smi> parameter2 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<JSReceiver> tmp1;
  TNode<JSObject> tmp2;
  TNode<IntPtrT> tmp3;
  TNode<FixedArrayBase> tmp4;
  TNode<FixedDoubleArray> tmp5;
  TNode<IntPtrT> tmp6;
  TNode<IntPtrT> tmp7;
  TNode<IntPtrT> tmp8;
  TNode<Smi> tmp9;
  TNode<IntPtrT> tmp10;
  TNode<IntPtrT> tmp11;
  TNode<UintPtrT> tmp12;
  TNode<UintPtrT> tmp13;
  TNode<BoolT> tmp14;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 259);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp1 = CodeStubAssembler(state_).LoadReference<JSReceiver>(CodeStubAssembler::Reference{parameter1, tmp0});
    tmp2 = UnsafeCast_JSObject_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp1});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 260);
    tmp3 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp4 = CodeStubAssembler(state_).LoadReference<FixedArrayBase>(CodeStubAssembler::Reference{tmp2, tmp3});
    tmp5 = UnsafeCast_FixedDoubleArray_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp4});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 261);
    tmp6 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp7 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp8 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 22);
    tmp9 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp5, tmp8});
    tmp10 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp9});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp11 = Convert_intptr_Smi_0(state_, TNode<Smi>{parameter2});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp12 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp11});
    tmp13 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp10});
    tmp14 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp12}, TNode<UintPtrT>{tmp13});
    ca_.Branch(tmp14, &block7, std::vector<Node*>{}, &block8, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp15;
  TNode<IntPtrT> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<HeapObject> tmp18;
  TNode<IntPtrT> tmp19;
  TNode<BoolT> tmp20;
  TNode<Float64T> tmp21;
  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp15 = FromConstexpr_intptr_constexpr_int31_0(state_, kDoubleSize);
    tmp16 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp11}, TNode<IntPtrT>{tmp15});
    tmp17 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp6}, TNode<IntPtrT>{tmp16});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp18, tmp19) = NewReference_float64_or_hole_0(state_, TNode<HeapObject>{tmp5}, TNode<IntPtrT>{tmp17}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 261);
    std::tie(tmp20, tmp21) = LoadFloat64OrHole_0(state_, TorqueStructReference_float64_or_hole_0{TNode<HeapObject>{tmp18}, TNode<IntPtrT>{tmp19}, TorqueStructUnsafe_0{}}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/base.tq", 120);
    ca_.Branch(tmp20, &block11, std::vector<Node*>{}, &block12, std::vector<Node*>{});
  }

  if (block8.is_used()) {
    ca_.Bind(&block8);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Oddball> tmp22;
  if (block11.is_used()) {
    ca_.Bind(&block11);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 264);
    tmp22 = TheHole_0(state_);
    CodeStubAssembler(state_).Return(tmp22);
  }

  TNode<HeapNumber> tmp23;
  if (block12.is_used()) {
    ca_.Bind(&block12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 262);
    tmp23 = CodeStubAssembler(state_).AllocateHeapNumberWithValue(TNode<Float64T>{tmp21});
    CodeStubAssembler(state_).Return(tmp23);
  }
}

TF_BUILTIN(Store_FastSmiElements_0, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<SortState> parameter1 = UncheckedCast<SortState>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<Smi> parameter2 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  TNode<Object> parameter3 = UncheckedCast<Object>(Parameter(Descriptor::ParameterIndex<2>()));
  USE(parameter3);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<JSReceiver> tmp1;
  TNode<JSObject> tmp2;
  TNode<IntPtrT> tmp3;
  TNode<FixedArrayBase> tmp4;
  TNode<FixedArray> tmp5;
  TNode<Smi> tmp6;
  TNode<Smi> tmp7;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 276);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp1 = CodeStubAssembler(state_).LoadReference<JSReceiver>(CodeStubAssembler::Reference{parameter1, tmp0});
    tmp2 = UnsafeCast_JSObject_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp1});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 277);
    tmp3 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp4 = CodeStubAssembler(state_).LoadReference<FixedArrayBase>(CodeStubAssembler::Reference{tmp2, tmp3});
    tmp5 = UnsafeCast_FixedArray_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp4});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 278);
    tmp6 = UnsafeCast_Smi_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter3});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 279);
    CodeStubAssembler(state_).StoreFixedArrayElement(TNode<FixedArray>{tmp5}, TNode<Smi>{parameter2}, TNode<Smi>{tmp6}, SKIP_WRITE_BARRIER);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 280);
    tmp7 = kSuccess_0(state_);
    CodeStubAssembler(state_).Return(tmp7);
  }
}

TF_BUILTIN(Store_FastObjectElements_0, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<SortState> parameter1 = UncheckedCast<SortState>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<Smi> parameter2 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  TNode<Object> parameter3 = UncheckedCast<Object>(Parameter(Descriptor::ParameterIndex<2>()));
  USE(parameter3);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<JSReceiver> tmp1;
  TNode<JSObject> tmp2;
  TNode<IntPtrT> tmp3;
  TNode<FixedArrayBase> tmp4;
  TNode<FixedArray> tmp5;
  TNode<IntPtrT> tmp6;
  TNode<IntPtrT> tmp7;
  TNode<IntPtrT> tmp8;
  TNode<Smi> tmp9;
  TNode<IntPtrT> tmp10;
  TNode<IntPtrT> tmp11;
  TNode<UintPtrT> tmp12;
  TNode<UintPtrT> tmp13;
  TNode<BoolT> tmp14;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 285);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp1 = CodeStubAssembler(state_).LoadReference<JSReceiver>(CodeStubAssembler::Reference{parameter1, tmp0});
    tmp2 = UnsafeCast_JSObject_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp1});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 286);
    tmp3 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp4 = CodeStubAssembler(state_).LoadReference<FixedArrayBase>(CodeStubAssembler::Reference{tmp2, tmp3});
    tmp5 = UnsafeCast_FixedArray_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp4});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 287);
    tmp6 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp7 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp8 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp9 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp5, tmp8});
    tmp10 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp9});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp11 = Convert_intptr_Smi_0(state_, TNode<Smi>{parameter2});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp12 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp11});
    tmp13 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp10});
    tmp14 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp12}, TNode<UintPtrT>{tmp13});
    ca_.Branch(tmp14, &block5, std::vector<Node*>{}, &block6, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp15;
  TNode<IntPtrT> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<HeapObject> tmp18;
  TNode<IntPtrT> tmp19;
  TNode<Smi> tmp20;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp15 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp16 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp11}, TNode<IntPtrT>{tmp15});
    tmp17 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp6}, TNode<IntPtrT>{tmp16});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp18, tmp19) = NewReference_Object_0(state_, TNode<HeapObject>{tmp5}, TNode<IntPtrT>{tmp17}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 287);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp18, tmp19}, parameter3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 288);
    tmp20 = kSuccess_0(state_);
    CodeStubAssembler(state_).Return(tmp20);
  }

  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }
}

TF_BUILTIN(Store_FastDoubleElements_0, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<SortState> parameter1 = UncheckedCast<SortState>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<Smi> parameter2 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  TNode<Object> parameter3 = UncheckedCast<Object>(Parameter(Descriptor::ParameterIndex<2>()));
  USE(parameter3);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<JSReceiver> tmp1;
  TNode<JSObject> tmp2;
  TNode<IntPtrT> tmp3;
  TNode<FixedArrayBase> tmp4;
  TNode<FixedDoubleArray> tmp5;
  TNode<HeapNumber> tmp6;
  TNode<Float64T> tmp7;
  TNode<Smi> tmp8;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 293);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp1 = CodeStubAssembler(state_).LoadReference<JSReceiver>(CodeStubAssembler::Reference{parameter1, tmp0});
    tmp2 = UnsafeCast_JSObject_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp1});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 294);
    tmp3 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp4 = CodeStubAssembler(state_).LoadReference<FixedArrayBase>(CodeStubAssembler::Reference{tmp2, tmp3});
    tmp5 = UnsafeCast_FixedDoubleArray_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp4});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 295);
    tmp6 = UnsafeCast_HeapNumber_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter3});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 296);
    tmp7 = Convert_float64_HeapNumber_0(state_, TNode<HeapNumber>{tmp6});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 297);
    CodeStubAssembler(state_).StoreFixedDoubleArrayElementSmi(TNode<FixedDoubleArray>{tmp5}, TNode<Smi>{parameter2}, TNode<Float64T>{tmp7});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 298);
    tmp8 = kSuccess_0(state_);
    CodeStubAssembler(state_).Return(tmp8);
  }
}

TF_BUILTIN(Delete_FastSmiElements_0, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<SortState> parameter1 = UncheckedCast<SortState>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<Smi> parameter2 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<JSReceiver> tmp1;
  TNode<IntPtrT> tmp2;
  TNode<Map> tmp3;
  TNode<Int32T> tmp4;
  TNode<BoolT> tmp5;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 310);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp1 = CodeStubAssembler(state_).LoadReference<JSReceiver>(CodeStubAssembler::Reference{parameter1, tmp0});
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp3 = CodeStubAssembler(state_).LoadReference<Map>(CodeStubAssembler::Reference{tmp1, tmp2});
    tmp4 = CodeStubAssembler(state_).LoadMapElementsKind(TNode<Map>{tmp3});
    tmp5 = CodeStubAssembler(state_).IsHoleyFastElementsKind(TNode<Int32T>{tmp4});
    ca_.Branch(tmp5, &block1, std::vector<Node*>{}, &block2, std::vector<Node*>{});
  }

  if (block2.is_used()) {
    ca_.Bind(&block2);
    CodeStubAssembler(state_).FailAssert("Torque assert 'IsHoleyFastElementsKind(sortState.receiver.map.elements_kind)' failed", "third_party/v8/builtins/array-sort.tq", 310);
  }

  TNode<IntPtrT> tmp6;
  TNode<JSReceiver> tmp7;
  TNode<JSObject> tmp8;
  TNode<IntPtrT> tmp9;
  TNode<FixedArrayBase> tmp10;
  TNode<FixedArray> tmp11;
  TNode<IntPtrT> tmp12;
  TNode<IntPtrT> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<Smi> tmp15;
  TNode<IntPtrT> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<UintPtrT> tmp18;
  TNode<UintPtrT> tmp19;
  TNode<BoolT> tmp20;
  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 312);
    tmp6 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp7 = CodeStubAssembler(state_).LoadReference<JSReceiver>(CodeStubAssembler::Reference{parameter1, tmp6});
    tmp8 = UnsafeCast_JSObject_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp7});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 313);
    tmp9 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp10 = CodeStubAssembler(state_).LoadReference<FixedArrayBase>(CodeStubAssembler::Reference{tmp8, tmp9});
    tmp11 = UnsafeCast_FixedArray_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp10});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 314);
    tmp12 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp13 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp14 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp15 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp11, tmp14});
    tmp16 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp15});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp17 = Convert_intptr_Smi_0(state_, TNode<Smi>{parameter2});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp18 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp17});
    tmp19 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp16});
    tmp20 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp18}, TNode<UintPtrT>{tmp19});
    ca_.Branch(tmp20, &block7, std::vector<Node*>{}, &block8, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp21;
  TNode<IntPtrT> tmp22;
  TNode<IntPtrT> tmp23;
  TNode<HeapObject> tmp24;
  TNode<IntPtrT> tmp25;
  TNode<Oddball> tmp26;
  TNode<Smi> tmp27;
  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp21 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp22 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp17}, TNode<IntPtrT>{tmp21});
    tmp23 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp12}, TNode<IntPtrT>{tmp22});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp24, tmp25) = NewReference_Object_0(state_, TNode<HeapObject>{tmp11}, TNode<IntPtrT>{tmp23}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 314);
    tmp26 = TheHole_0(state_);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp24, tmp25}, tmp26);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 315);
    tmp27 = kSuccess_0(state_);
    CodeStubAssembler(state_).Return(tmp27);
  }

  if (block8.is_used()) {
    ca_.Bind(&block8);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }
}

TF_BUILTIN(Delete_FastObjectElements_0, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<SortState> parameter1 = UncheckedCast<SortState>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<Smi> parameter2 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<JSReceiver> tmp1;
  TNode<IntPtrT> tmp2;
  TNode<Map> tmp3;
  TNode<Int32T> tmp4;
  TNode<BoolT> tmp5;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 320);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp1 = CodeStubAssembler(state_).LoadReference<JSReceiver>(CodeStubAssembler::Reference{parameter1, tmp0});
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp3 = CodeStubAssembler(state_).LoadReference<Map>(CodeStubAssembler::Reference{tmp1, tmp2});
    tmp4 = CodeStubAssembler(state_).LoadMapElementsKind(TNode<Map>{tmp3});
    tmp5 = CodeStubAssembler(state_).IsHoleyFastElementsKind(TNode<Int32T>{tmp4});
    ca_.Branch(tmp5, &block1, std::vector<Node*>{}, &block2, std::vector<Node*>{});
  }

  if (block2.is_used()) {
    ca_.Bind(&block2);
    CodeStubAssembler(state_).FailAssert("Torque assert 'IsHoleyFastElementsKind(sortState.receiver.map.elements_kind)' failed", "third_party/v8/builtins/array-sort.tq", 320);
  }

  TNode<IntPtrT> tmp6;
  TNode<JSReceiver> tmp7;
  TNode<JSObject> tmp8;
  TNode<IntPtrT> tmp9;
  TNode<FixedArrayBase> tmp10;
  TNode<FixedArray> tmp11;
  TNode<IntPtrT> tmp12;
  TNode<IntPtrT> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<Smi> tmp15;
  TNode<IntPtrT> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<UintPtrT> tmp18;
  TNode<UintPtrT> tmp19;
  TNode<BoolT> tmp20;
  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 322);
    tmp6 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp7 = CodeStubAssembler(state_).LoadReference<JSReceiver>(CodeStubAssembler::Reference{parameter1, tmp6});
    tmp8 = UnsafeCast_JSObject_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp7});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 323);
    tmp9 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp10 = CodeStubAssembler(state_).LoadReference<FixedArrayBase>(CodeStubAssembler::Reference{tmp8, tmp9});
    tmp11 = UnsafeCast_FixedArray_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp10});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 324);
    tmp12 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp13 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp14 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp15 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp11, tmp14});
    tmp16 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp15});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp17 = Convert_intptr_Smi_0(state_, TNode<Smi>{parameter2});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp18 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp17});
    tmp19 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp16});
    tmp20 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp18}, TNode<UintPtrT>{tmp19});
    ca_.Branch(tmp20, &block7, std::vector<Node*>{}, &block8, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp21;
  TNode<IntPtrT> tmp22;
  TNode<IntPtrT> tmp23;
  TNode<HeapObject> tmp24;
  TNode<IntPtrT> tmp25;
  TNode<Oddball> tmp26;
  TNode<Smi> tmp27;
  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp21 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp22 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp17}, TNode<IntPtrT>{tmp21});
    tmp23 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp12}, TNode<IntPtrT>{tmp22});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp24, tmp25) = NewReference_Object_0(state_, TNode<HeapObject>{tmp11}, TNode<IntPtrT>{tmp23}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 324);
    tmp26 = TheHole_0(state_);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp24, tmp25}, tmp26);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 325);
    tmp27 = kSuccess_0(state_);
    CodeStubAssembler(state_).Return(tmp27);
  }

  if (block8.is_used()) {
    ca_.Bind(&block8);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }
}

TF_BUILTIN(Delete_FastDoubleElements_0, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<SortState> parameter1 = UncheckedCast<SortState>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<Smi> parameter2 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<JSReceiver> tmp1;
  TNode<IntPtrT> tmp2;
  TNode<Map> tmp3;
  TNode<Int32T> tmp4;
  TNode<BoolT> tmp5;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 330);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp1 = CodeStubAssembler(state_).LoadReference<JSReceiver>(CodeStubAssembler::Reference{parameter1, tmp0});
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp3 = CodeStubAssembler(state_).LoadReference<Map>(CodeStubAssembler::Reference{tmp1, tmp2});
    tmp4 = CodeStubAssembler(state_).LoadMapElementsKind(TNode<Map>{tmp3});
    tmp5 = CodeStubAssembler(state_).IsHoleyFastElementsKind(TNode<Int32T>{tmp4});
    ca_.Branch(tmp5, &block1, std::vector<Node*>{}, &block2, std::vector<Node*>{});
  }

  if (block2.is_used()) {
    ca_.Bind(&block2);
    CodeStubAssembler(state_).FailAssert("Torque assert 'IsHoleyFastElementsKind(sortState.receiver.map.elements_kind)' failed", "third_party/v8/builtins/array-sort.tq", 330);
  }

  TNode<IntPtrT> tmp6;
  TNode<JSReceiver> tmp7;
  TNode<JSObject> tmp8;
  TNode<IntPtrT> tmp9;
  TNode<FixedArrayBase> tmp10;
  TNode<FixedDoubleArray> tmp11;
  TNode<IntPtrT> tmp12;
  TNode<IntPtrT> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<Smi> tmp15;
  TNode<IntPtrT> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<UintPtrT> tmp18;
  TNode<UintPtrT> tmp19;
  TNode<BoolT> tmp20;
  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 332);
    tmp6 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp7 = CodeStubAssembler(state_).LoadReference<JSReceiver>(CodeStubAssembler::Reference{parameter1, tmp6});
    tmp8 = UnsafeCast_JSObject_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp7});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 333);
    tmp9 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp10 = CodeStubAssembler(state_).LoadReference<FixedArrayBase>(CodeStubAssembler::Reference{tmp8, tmp9});
    tmp11 = UnsafeCast_FixedDoubleArray_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp10});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 334);
    tmp12 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp13 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp14 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 22);
    tmp15 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp11, tmp14});
    tmp16 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp15});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp17 = Convert_intptr_Smi_0(state_, TNode<Smi>{parameter2});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp18 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp17});
    tmp19 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp16});
    tmp20 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp18}, TNode<UintPtrT>{tmp19});
    ca_.Branch(tmp20, &block7, std::vector<Node*>{}, &block8, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp21;
  TNode<IntPtrT> tmp22;
  TNode<IntPtrT> tmp23;
  TNode<HeapObject> tmp24;
  TNode<IntPtrT> tmp25;
  TNode<BoolT> tmp26;
  TNode<Float64T> tmp27;
  TNode<Smi> tmp28;
  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp21 = FromConstexpr_intptr_constexpr_int31_0(state_, kDoubleSize);
    tmp22 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp17}, TNode<IntPtrT>{tmp21});
    tmp23 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp12}, TNode<IntPtrT>{tmp22});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp24, tmp25) = NewReference_float64_or_hole_0(state_, TNode<HeapObject>{tmp11}, TNode<IntPtrT>{tmp23}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 334);
    std::tie(tmp26, tmp27) = kDoubleHole_0(state_).Flatten();
    StoreFloat64OrHole_0(state_, TorqueStructReference_float64_or_hole_0{TNode<HeapObject>{tmp24}, TNode<IntPtrT>{tmp25}, TorqueStructUnsafe_0{}}, TorqueStructfloat64_or_hole_0{TNode<BoolT>{tmp26}, TNode<Float64T>{tmp27}});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 335);
    tmp28 = kSuccess_0(state_);
    CodeStubAssembler(state_).Return(tmp28);
  }

  if (block8.is_used()) {
    ca_.Bind(&block8);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }
}

TF_BUILTIN(SortCompareDefault, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<Object> parameter2 = UncheckedCast<Object>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  TNode<Object> parameter3 = UncheckedCast<Object>(Parameter(Descriptor::ParameterIndex<2>()));
  USE(parameter3);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Oddball> tmp0;
  TNode<BoolT> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 340);
    tmp0 = Undefined_0(state_);
    tmp1 = CodeStubAssembler(state_).TaggedEqual(TNode<Object>{parameter1}, TNode<HeapObject>{tmp0});
    ca_.Branch(tmp1, &block1, std::vector<Node*>{}, &block2, std::vector<Node*>{});
  }

  if (block2.is_used()) {
    ca_.Bind(&block2);
    CodeStubAssembler(state_).FailAssert("Torque assert 'comparefn == Undefined' failed", "third_party/v8/builtins/array-sort.tq", 340);
  }

  TNode<BoolT> tmp2;
  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 342);
    tmp2 = CodeStubAssembler(state_).TaggedIsSmi(TNode<Object>{parameter2});
    ca_.Branch(tmp2, &block5, std::vector<Node*>{}, &block6, std::vector<Node*>{});
  }

  TNode<BoolT> tmp3;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    tmp3 = CodeStubAssembler(state_).TaggedIsSmi(TNode<Object>{parameter3});
    ca_.Goto(&block7, tmp3);
  }

  TNode<BoolT> tmp4;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    tmp4 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block7, tmp4);
  }

  TNode<BoolT> phi_bb7_5;
  if (block7.is_used()) {
    ca_.Bind(&block7, &phi_bb7_5);
    ca_.Branch(phi_bb7_5, &block3, std::vector<Node*>{}, &block4, std::vector<Node*>{});
  }

  TNode<Smi> tmp5;
  TNode<Smi> tmp6;
  TNode<Smi> tmp7;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 343);
    tmp5 = UnsafeCast_Smi_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter2});
    tmp6 = UnsafeCast_Smi_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter3});
    tmp7 = CodeStubAssembler(state_).SmiLexicographicCompare(TNode<Smi>{tmp5}, TNode<Smi>{tmp6});
    CodeStubAssembler(state_).Return(tmp7);
  }

  TNode<String> tmp8;
  TNode<String> tmp9;
  TNode<Oddball> tmp10;
  TNode<Oddball> tmp11;
  TNode<BoolT> tmp12;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 347);
    tmp8 = CodeStubAssembler(state_).ToString_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter2});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 350);
    tmp9 = CodeStubAssembler(state_).ToString_Inline(TNode<Context>{parameter0}, TNode<Object>{parameter3});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 355);
    tmp10 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kStringLessThan, parameter0, tmp8, tmp9));
    tmp11 = True_0(state_);
    tmp12 = CodeStubAssembler(state_).TaggedEqual(TNode<HeapObject>{tmp10}, TNode<HeapObject>{tmp11});
    ca_.Branch(tmp12, &block8, std::vector<Node*>{}, &block9, std::vector<Node*>{});
  }

  TNode<Number> tmp13;
  if (block8.is_used()) {
    ca_.Bind(&block8);
    tmp13 = FromConstexpr_Number_constexpr_int31_0(state_, -1);
    CodeStubAssembler(state_).Return(tmp13);
  }

  TNode<Oddball> tmp14;
  TNode<Oddball> tmp15;
  TNode<BoolT> tmp16;
  if (block9.is_used()) {
    ca_.Bind(&block9);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 360);
    tmp14 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kStringLessThan, parameter0, tmp9, tmp8));
    tmp15 = True_0(state_);
    tmp16 = CodeStubAssembler(state_).TaggedEqual(TNode<HeapObject>{tmp14}, TNode<HeapObject>{tmp15});
    ca_.Branch(tmp16, &block10, std::vector<Node*>{}, &block11, std::vector<Node*>{});
  }

  TNode<Number> tmp17;
  if (block10.is_used()) {
    ca_.Bind(&block10);
    tmp17 = FromConstexpr_Number_constexpr_int31_0(state_, 1);
    CodeStubAssembler(state_).Return(tmp17);
  }

  TNode<Number> tmp18;
  if (block11.is_used()) {
    ca_.Bind(&block11);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 363);
    tmp18 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    CodeStubAssembler(state_).Return(tmp18);
  }
}

TF_BUILTIN(SortCompareUserFn, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = UncheckedCast<Object>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<Object> parameter2 = UncheckedCast<Object>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  TNode<Object> parameter3 = UncheckedCast<Object>(Parameter(Descriptor::ParameterIndex<2>()));
  USE(parameter3);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Oddball> tmp0;
  TNode<BoolT> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 368);
    tmp0 = Undefined_0(state_);
    tmp1 = CodeStubAssembler(state_).TaggedNotEqual(TNode<Object>{parameter1}, TNode<HeapObject>{tmp0});
    ca_.Branch(tmp1, &block1, std::vector<Node*>{}, &block2, std::vector<Node*>{});
  }

  if (block2.is_used()) {
    ca_.Bind(&block2);
    CodeStubAssembler(state_).FailAssert("Torque assert 'comparefn != Undefined' failed", "third_party/v8/builtins/array-sort.tq", 368);
  }

  TNode<JSReceiver> tmp2;
  TNode<Oddball> tmp3;
  TNode<Object> tmp4;
  TNode<Number> tmp5;
  TNode<BoolT> tmp6;
  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 369);
    tmp2 = UnsafeCast_Callable_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter1});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 372);
    tmp3 = Undefined_0(state_);
    tmp4 = CodeStubAssembler(state_).Call(TNode<Context>{parameter0}, TNode<Object>{tmp2}, TNode<Object>{tmp3}, TNode<Object>{parameter2}, TNode<Object>{parameter3});
    tmp5 = CodeStubAssembler(state_).ToNumber_Inline(TNode<Context>{parameter0}, TNode<Object>{tmp4});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 375);
    tmp6 = NumberIsNaN_0(state_, TNode<Number>{tmp5});
    ca_.Branch(tmp6, &block3, std::vector<Node*>{}, &block4, std::vector<Node*>{});
  }

  TNode<Number> tmp7;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    tmp7 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    CodeStubAssembler(state_).Return(tmp7);
  }

  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 378);
    CodeStubAssembler(state_).Return(tmp5);
  }
}

TF_BUILTIN(CanUseSameAccessor_GenericElementsAccessor_0, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<JSReceiver> parameter1 = UncheckedCast<JSReceiver>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<Map> parameter2 = UncheckedCast<Map>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  TNode<Number> parameter3 = UncheckedCast<Number>(Parameter(Descriptor::ParameterIndex<2>()));
  USE(parameter3);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Oddball> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 397);
    tmp0 = True_0(state_);
    CodeStubAssembler(state_).Return(tmp0);
  }
}

TNode<Smi> GetPendingRunsSize_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<SortState> p_sortState) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<Smi> tmp1;
  TNode<Smi> tmp2;
  TNode<BoolT> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 403);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 44);
    tmp1 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_sortState, tmp0});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 404);
    tmp2 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp3 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{tmp1}, TNode<Smi>{tmp2});
    ca_.Branch(tmp3, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    CodeStubAssembler(state_).FailAssert("Torque assert 'stackSize >= 0' failed", "third_party/v8/builtins/array-sort.tq", 404);
  }

  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 402);
    ca_.Goto(&block4);
  }

    ca_.Bind(&block4);
  return TNode<Smi>{tmp1};
}

TNode<Smi> GetPendingRunBase_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<FixedArray> p_pendingRuns, TNode<Smi> p_run) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<IntPtrT> tmp1;
  TNode<IntPtrT> tmp2;
  TNode<Smi> tmp3;
  TNode<IntPtrT> tmp4;
  TNode<Smi> tmp5;
  TNode<IntPtrT> tmp6;
  TNode<UintPtrT> tmp7;
  TNode<UintPtrT> tmp8;
  TNode<BoolT> tmp9;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 410);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp1 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp3 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_pendingRuns, tmp2});
    tmp4 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp3});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 410);
    tmp5 = CodeStubAssembler(state_).SmiShl(TNode<Smi>{p_run}, 1);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp6 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp5});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp7 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp6});
    tmp8 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp4});
    tmp9 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp7}, TNode<UintPtrT>{tmp8});
    ca_.Branch(tmp9, &block6, std::vector<Node*>{}, &block7, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp10;
  TNode<IntPtrT> tmp11;
  TNode<IntPtrT> tmp12;
  TNode<HeapObject> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<Object> tmp15;
  TNode<Smi> tmp16;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp10 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp11 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp6}, TNode<IntPtrT>{tmp10});
    tmp12 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp0}, TNode<IntPtrT>{tmp11});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp13, tmp14) = NewReference_Object_0(state_, TNode<HeapObject>{p_pendingRuns}, TNode<IntPtrT>{tmp12}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 410);
    tmp15 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp13, tmp14});
    tmp16 = UnsafeCast_Smi_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp15});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 408);
    ca_.Goto(&block9);
  }

  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

    ca_.Bind(&block9);
  return TNode<Smi>{tmp16};
}

void SetPendingRunBase_0(compiler::CodeAssemblerState* state_, TNode<FixedArray> p_pendingRuns, TNode<Smi> p_run, TNode<Smi> p_value) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<IntPtrT> tmp1;
  TNode<IntPtrT> tmp2;
  TNode<Smi> tmp3;
  TNode<IntPtrT> tmp4;
  TNode<Smi> tmp5;
  TNode<IntPtrT> tmp6;
  TNode<UintPtrT> tmp7;
  TNode<UintPtrT> tmp8;
  TNode<BoolT> tmp9;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 414);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp1 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp3 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_pendingRuns, tmp2});
    tmp4 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp3});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 414);
    tmp5 = CodeStubAssembler(state_).SmiShl(TNode<Smi>{p_run}, 1);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp6 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp5});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp7 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp6});
    tmp8 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp4});
    tmp9 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp7}, TNode<UintPtrT>{tmp8});
    ca_.Branch(tmp9, &block6, std::vector<Node*>{}, &block7, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp10;
  TNode<IntPtrT> tmp11;
  TNode<IntPtrT> tmp12;
  TNode<HeapObject> tmp13;
  TNode<IntPtrT> tmp14;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp10 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp11 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp6}, TNode<IntPtrT>{tmp10});
    tmp12 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp0}, TNode<IntPtrT>{tmp11});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp13, tmp14) = NewReference_Object_0(state_, TNode<HeapObject>{p_pendingRuns}, TNode<IntPtrT>{tmp12}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 414);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp13, tmp14}, p_value);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 413);
    ca_.Goto(&block9);
  }

  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

    ca_.Bind(&block9);
}

TNode<Smi> GetPendingRunLength_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<FixedArray> p_pendingRuns, TNode<Smi> p_run) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<IntPtrT> tmp1;
  TNode<IntPtrT> tmp2;
  TNode<Smi> tmp3;
  TNode<IntPtrT> tmp4;
  TNode<Smi> tmp5;
  TNode<Smi> tmp6;
  TNode<Smi> tmp7;
  TNode<IntPtrT> tmp8;
  TNode<UintPtrT> tmp9;
  TNode<UintPtrT> tmp10;
  TNode<BoolT> tmp11;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 419);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp1 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp3 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_pendingRuns, tmp2});
    tmp4 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp3});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 419);
    tmp5 = CodeStubAssembler(state_).SmiShl(TNode<Smi>{p_run}, 1);
    tmp6 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp7 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp5}, TNode<Smi>{tmp6});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp8 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp7});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp9 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp8});
    tmp10 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp4});
    tmp11 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp9}, TNode<UintPtrT>{tmp10});
    ca_.Branch(tmp11, &block6, std::vector<Node*>{}, &block7, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp12;
  TNode<IntPtrT> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<HeapObject> tmp15;
  TNode<IntPtrT> tmp16;
  TNode<Object> tmp17;
  TNode<Smi> tmp18;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp12 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp13 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp8}, TNode<IntPtrT>{tmp12});
    tmp14 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp0}, TNode<IntPtrT>{tmp13});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp15, tmp16) = NewReference_Object_0(state_, TNode<HeapObject>{p_pendingRuns}, TNode<IntPtrT>{tmp14}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 419);
    tmp17 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp15, tmp16});
    tmp18 = UnsafeCast_Smi_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp17});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 417);
    ca_.Goto(&block9);
  }

  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

    ca_.Bind(&block9);
  return TNode<Smi>{tmp18};
}

void SetPendingRunLength_0(compiler::CodeAssemblerState* state_, TNode<FixedArray> p_pendingRuns, TNode<Smi> p_run, TNode<Smi> p_value) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<IntPtrT> tmp1;
  TNode<IntPtrT> tmp2;
  TNode<Smi> tmp3;
  TNode<IntPtrT> tmp4;
  TNode<Smi> tmp5;
  TNode<Smi> tmp6;
  TNode<Smi> tmp7;
  TNode<IntPtrT> tmp8;
  TNode<UintPtrT> tmp9;
  TNode<UintPtrT> tmp10;
  TNode<BoolT> tmp11;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 423);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp1 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp3 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_pendingRuns, tmp2});
    tmp4 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp3});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 423);
    tmp5 = CodeStubAssembler(state_).SmiShl(TNode<Smi>{p_run}, 1);
    tmp6 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp7 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp5}, TNode<Smi>{tmp6});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp8 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp7});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp9 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp8});
    tmp10 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp4});
    tmp11 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp9}, TNode<UintPtrT>{tmp10});
    ca_.Branch(tmp11, &block6, std::vector<Node*>{}, &block7, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp12;
  TNode<IntPtrT> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<HeapObject> tmp15;
  TNode<IntPtrT> tmp16;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp12 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp13 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp8}, TNode<IntPtrT>{tmp12});
    tmp14 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp0}, TNode<IntPtrT>{tmp13});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp15, tmp16) = NewReference_Object_0(state_, TNode<HeapObject>{p_pendingRuns}, TNode<IntPtrT>{tmp14}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 423);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp15, tmp16}, p_value);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 422);
    ca_.Goto(&block9);
  }

  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

    ca_.Bind(&block9);
}

void PushRun_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<SortState> p_sortState, TNode<Smi> p_base, TNode<Smi> p_length) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Smi> tmp0;
  TNode<Smi> tmp1;
  TNode<BoolT> tmp2;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 428);
    tmp0 = GetPendingRunsSize_0(state_, TNode<Context>{p_context}, TNode<SortState>{p_sortState});
    tmp1 = FromConstexpr_Smi_constexpr_int31_0(state_, kMaxMergePending_0(state_));
    tmp2 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{tmp0}, TNode<Smi>{tmp1});
    ca_.Branch(tmp2, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    CodeStubAssembler(state_).FailAssert("Torque assert 'GetPendingRunsSize(sortState) < kMaxMergePending' failed", "third_party/v8/builtins/array-sort.tq", 428);
  }

  TNode<Smi> tmp3;
  TNode<IntPtrT> tmp4;
  TNode<FixedArray> tmp5;
  TNode<IntPtrT> tmp6;
  TNode<Smi> tmp7;
  TNode<Smi> tmp8;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 430);
    tmp3 = GetPendingRunsSize_0(state_, TNode<Context>{p_context}, TNode<SortState>{p_sortState});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 431);
    tmp4 = FromConstexpr_intptr_constexpr_int31_0(state_, 48);
    tmp5 = CodeStubAssembler(state_).LoadReference<FixedArray>(CodeStubAssembler::Reference{p_sortState, tmp4});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 433);
    SetPendingRunBase_0(state_, TNode<FixedArray>{tmp5}, TNode<Smi>{tmp3}, TNode<Smi>{p_base});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 434);
    SetPendingRunLength_0(state_, TNode<FixedArray>{tmp5}, TNode<Smi>{tmp3}, TNode<Smi>{p_length});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 436);
    tmp6 = FromConstexpr_intptr_constexpr_int31_0(state_, 44);
    tmp7 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp8 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp3}, TNode<Smi>{tmp7});
    CodeStubAssembler(state_).StoreReference<Smi>(CodeStubAssembler::Reference{p_sortState, tmp6}, tmp8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 426);
    ca_.Goto(&block4);
  }

    ca_.Bind(&block4);
}

TNode<FixedArray> GetTempArray_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<SortState> p_sortState, TNode<Smi> p_requestedSize) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Smi> tmp0;
  TNode<Smi> tmp1;
  TNode<IntPtrT> tmp2;
  TNode<FixedArray> tmp3;
  TNode<IntPtrT> tmp4;
  TNode<Smi> tmp5;
  TNode<BoolT> tmp6;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 443);
    tmp0 = kSortStateTempSize_0(state_);
    tmp1 = CodeStubAssembler(state_).SmiMax(TNode<Smi>{tmp0}, TNode<Smi>{p_requestedSize});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 445);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 56);
    tmp3 = CodeStubAssembler(state_).LoadReference<FixedArray>(CodeStubAssembler::Reference{p_sortState, tmp2});
    tmp4 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp5 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp3, tmp4});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 446);
    tmp6 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{tmp5}, TNode<Smi>{tmp1});
    ca_.Branch(tmp6, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp7;
  TNode<FixedArray> tmp8;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 447);
    tmp7 = FromConstexpr_intptr_constexpr_int31_0(state_, 56);
    tmp8 = CodeStubAssembler(state_).LoadReference<FixedArray>(CodeStubAssembler::Reference{p_sortState, tmp7});
    ca_.Goto(&block1, tmp8);
  }

  TNode<IntPtrT> tmp9;
  TNode<FixedArray> tmp10;
  TNode<IntPtrT> tmp11;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 451);
    tmp9 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp1});
    tmp10 = CodeStubAssembler(state_).AllocateZeroedFixedArray(TNode<IntPtrT>{tmp9});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 453);
    tmp11 = FromConstexpr_intptr_constexpr_int31_0(state_, 56);
    CodeStubAssembler(state_).StoreReference<FixedArray>(CodeStubAssembler::Reference{p_sortState, tmp11}, tmp10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 454);
    ca_.Goto(&block1, tmp10);
  }

  TNode<FixedArray> phi_bb1_3;
  if (block1.is_used()) {
    ca_.Bind(&block1, &phi_bb1_3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 441);
    ca_.Goto(&block4);
  }

    ca_.Bind(&block4);
  return TNode<FixedArray>{phi_bb1_3};
}

TF_BUILTIN(Copy, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<FixedArray> parameter1 = UncheckedCast<FixedArray>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<Smi> parameter2 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  TNode<FixedArray> parameter3 = UncheckedCast<FixedArray>(Parameter(Descriptor::ParameterIndex<2>()));
  USE(parameter3);
  TNode<Smi> parameter4 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<3>()));
  USE(parameter4);
  TNode<Smi> parameter5 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<4>()));
  USE(parameter5);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block14(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block19(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block20(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi> block26(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi> block27(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block31(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block29(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block36(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block37(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi> block43(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi> block44(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block30(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Smi> tmp0;
  TNode<BoolT> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 461);
    tmp0 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp1 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{parameter2}, TNode<Smi>{tmp0});
    ca_.Branch(tmp1, &block1, std::vector<Node*>{}, &block2, std::vector<Node*>{});
  }

  if (block2.is_used()) {
    ca_.Bind(&block2);
    CodeStubAssembler(state_).FailAssert("Torque assert 'srcPos >= 0' failed", "third_party/v8/builtins/array-sort.tq", 461);
  }

  TNode<Smi> tmp2;
  TNode<BoolT> tmp3;
  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 462);
    tmp2 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp3 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{parameter4}, TNode<Smi>{tmp2});
    ca_.Branch(tmp3, &block3, std::vector<Node*>{}, &block4, std::vector<Node*>{});
  }

  if (block4.is_used()) {
    ca_.Bind(&block4);
    CodeStubAssembler(state_).FailAssert("Torque assert 'dstPos >= 0' failed", "third_party/v8/builtins/array-sort.tq", 462);
  }

  TNode<IntPtrT> tmp4;
  TNode<Smi> tmp5;
  TNode<Smi> tmp6;
  TNode<BoolT> tmp7;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 463);
    tmp4 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp5 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter1, tmp4});
    tmp6 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp5}, TNode<Smi>{parameter5});
    tmp7 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{parameter2}, TNode<Smi>{tmp6});
    ca_.Branch(tmp7, &block5, std::vector<Node*>{}, &block6, std::vector<Node*>{});
  }

  if (block6.is_used()) {
    ca_.Bind(&block6);
    CodeStubAssembler(state_).FailAssert("Torque assert 'srcPos <= source.length - length' failed", "third_party/v8/builtins/array-sort.tq", 463);
  }

  TNode<IntPtrT> tmp8;
  TNode<Smi> tmp9;
  TNode<Smi> tmp10;
  TNode<BoolT> tmp11;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 464);
    tmp8 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp9 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter3, tmp8});
    tmp10 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp9}, TNode<Smi>{parameter5});
    tmp11 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{parameter4}, TNode<Smi>{tmp10});
    ca_.Branch(tmp11, &block7, std::vector<Node*>{}, &block8, std::vector<Node*>{});
  }

  if (block8.is_used()) {
    ca_.Bind(&block8);
    CodeStubAssembler(state_).FailAssert("Torque assert 'dstPos <= target.length - length' failed", "third_party/v8/builtins/array-sort.tq", 464);
  }

  TNode<BoolT> tmp12;
  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 472);
    tmp12 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{parameter2}, TNode<Smi>{parameter4});
    ca_.Branch(tmp12, &block9, std::vector<Node*>{}, &block10, std::vector<Node*>{});
  }

  TNode<Smi> tmp13;
  TNode<Smi> tmp14;
  TNode<Smi> tmp15;
  TNode<Smi> tmp16;
  TNode<Smi> tmp17;
  TNode<Smi> tmp18;
  if (block9.is_used()) {
    ca_.Bind(&block9);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 473);
    tmp13 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{parameter2}, TNode<Smi>{parameter5});
    tmp14 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp15 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp13}, TNode<Smi>{tmp14});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 474);
    tmp16 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{parameter4}, TNode<Smi>{parameter5});
    tmp17 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp18 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp16}, TNode<Smi>{tmp17});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 475);
    ca_.Goto(&block14, tmp15, tmp18);
  }

  TNode<Smi> phi_bb14_6;
  TNode<Smi> phi_bb14_7;
  TNode<BoolT> tmp19;
  if (block14.is_used()) {
    ca_.Bind(&block14, &phi_bb14_6, &phi_bb14_7);
    tmp19 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{phi_bb14_6}, TNode<Smi>{parameter2});
    ca_.Branch(tmp19, &block12, std::vector<Node*>{phi_bb14_6, phi_bb14_7}, &block13, std::vector<Node*>{phi_bb14_6, phi_bb14_7});
  }

  TNode<Smi> phi_bb12_6;
  TNode<Smi> phi_bb12_7;
  TNode<IntPtrT> tmp20;
  TNode<IntPtrT> tmp21;
  TNode<IntPtrT> tmp22;
  TNode<Smi> tmp23;
  TNode<IntPtrT> tmp24;
  TNode<Smi> tmp25;
  TNode<Smi> tmp26;
  TNode<IntPtrT> tmp27;
  TNode<UintPtrT> tmp28;
  TNode<UintPtrT> tmp29;
  TNode<BoolT> tmp30;
  if (block12.is_used()) {
    ca_.Bind(&block12, &phi_bb12_6, &phi_bb12_7);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 476);
    tmp20 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp21 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp22 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp23 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter3, tmp22});
    tmp24 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp23});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 476);
    tmp25 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp26 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb12_7}, TNode<Smi>{tmp25});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp27 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb12_7});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp28 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp27});
    tmp29 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp24});
    tmp30 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp28}, TNode<UintPtrT>{tmp29});
    ca_.Branch(tmp30, &block19, std::vector<Node*>{phi_bb12_6, phi_bb12_7, phi_bb12_7}, &block20, std::vector<Node*>{phi_bb12_6, phi_bb12_7, phi_bb12_7});
  }

  TNode<Smi> phi_bb19_6;
  TNode<Smi> phi_bb19_12;
  TNode<Smi> phi_bb19_13;
  TNode<IntPtrT> tmp31;
  TNode<IntPtrT> tmp32;
  TNode<IntPtrT> tmp33;
  TNode<HeapObject> tmp34;
  TNode<IntPtrT> tmp35;
  TNode<IntPtrT> tmp36;
  TNode<IntPtrT> tmp37;
  TNode<IntPtrT> tmp38;
  TNode<Smi> tmp39;
  TNode<IntPtrT> tmp40;
  TNode<Smi> tmp41;
  TNode<Smi> tmp42;
  TNode<IntPtrT> tmp43;
  TNode<UintPtrT> tmp44;
  TNode<UintPtrT> tmp45;
  TNode<BoolT> tmp46;
  if (block19.is_used()) {
    ca_.Bind(&block19, &phi_bb19_6, &phi_bb19_12, &phi_bb19_13);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp31 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp32 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp27}, TNode<IntPtrT>{tmp31});
    tmp33 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp20}, TNode<IntPtrT>{tmp32});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp34, tmp35) = NewReference_Object_0(state_, TNode<HeapObject>{parameter3}, TNode<IntPtrT>{tmp33}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 476);
    tmp36 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp37 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp38 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp39 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter1, tmp38});
    tmp40 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp39});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 476);
    tmp41 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp42 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb19_6}, TNode<Smi>{tmp41});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp43 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb19_6});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp44 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp43});
    tmp45 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp40});
    tmp46 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp44}, TNode<UintPtrT>{tmp45});
    ca_.Branch(tmp46, &block26, std::vector<Node*>{phi_bb19_12, phi_bb19_13, phi_bb19_6, phi_bb19_6}, &block27, std::vector<Node*>{phi_bb19_12, phi_bb19_13, phi_bb19_6, phi_bb19_6});
  }

  TNode<Smi> phi_bb20_6;
  TNode<Smi> phi_bb20_12;
  TNode<Smi> phi_bb20_13;
  if (block20.is_used()) {
    ca_.Bind(&block20, &phi_bb20_6, &phi_bb20_12, &phi_bb20_13);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb26_12;
  TNode<Smi> phi_bb26_13;
  TNode<Smi> phi_bb26_20;
  TNode<Smi> phi_bb26_21;
  TNode<IntPtrT> tmp47;
  TNode<IntPtrT> tmp48;
  TNode<IntPtrT> tmp49;
  TNode<HeapObject> tmp50;
  TNode<IntPtrT> tmp51;
  TNode<Object> tmp52;
  if (block26.is_used()) {
    ca_.Bind(&block26, &phi_bb26_12, &phi_bb26_13, &phi_bb26_20, &phi_bb26_21);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp47 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp48 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp43}, TNode<IntPtrT>{tmp47});
    tmp49 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp36}, TNode<IntPtrT>{tmp48});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp50, tmp51) = NewReference_Object_0(state_, TNode<HeapObject>{parameter1}, TNode<IntPtrT>{tmp49}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 476);
    tmp52 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp50, tmp51});
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp34, tmp35}, tmp52);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 475);
    ca_.Goto(&block14, tmp42, tmp26);
  }

  TNode<Smi> phi_bb27_12;
  TNode<Smi> phi_bb27_13;
  TNode<Smi> phi_bb27_20;
  TNode<Smi> phi_bb27_21;
  if (block27.is_used()) {
    ca_.Bind(&block27, &phi_bb27_12, &phi_bb27_13, &phi_bb27_20, &phi_bb27_21);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb13_6;
  TNode<Smi> phi_bb13_7;
  if (block13.is_used()) {
    ca_.Bind(&block13, &phi_bb13_6, &phi_bb13_7);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 472);
    ca_.Goto(&block11);
  }

  TNode<Smi> tmp53;
  if (block10.is_used()) {
    ca_.Bind(&block10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 481);
    tmp53 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{parameter2}, TNode<Smi>{parameter5});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 483);
    ca_.Goto(&block31, parameter2, parameter4);
  }

  TNode<Smi> phi_bb31_6;
  TNode<Smi> phi_bb31_7;
  TNode<BoolT> tmp54;
  if (block31.is_used()) {
    ca_.Bind(&block31, &phi_bb31_6, &phi_bb31_7);
    tmp54 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{phi_bb31_6}, TNode<Smi>{tmp53});
    ca_.Branch(tmp54, &block29, std::vector<Node*>{phi_bb31_6, phi_bb31_7}, &block30, std::vector<Node*>{phi_bb31_6, phi_bb31_7});
  }

  TNode<Smi> phi_bb29_6;
  TNode<Smi> phi_bb29_7;
  TNode<IntPtrT> tmp55;
  TNode<IntPtrT> tmp56;
  TNode<IntPtrT> tmp57;
  TNode<Smi> tmp58;
  TNode<IntPtrT> tmp59;
  TNode<Smi> tmp60;
  TNode<Smi> tmp61;
  TNode<IntPtrT> tmp62;
  TNode<UintPtrT> tmp63;
  TNode<UintPtrT> tmp64;
  TNode<BoolT> tmp65;
  if (block29.is_used()) {
    ca_.Bind(&block29, &phi_bb29_6, &phi_bb29_7);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 484);
    tmp55 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp56 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp57 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp58 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter3, tmp57});
    tmp59 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp58});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 484);
    tmp60 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp61 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb29_7}, TNode<Smi>{tmp60});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp62 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb29_7});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp63 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp62});
    tmp64 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp59});
    tmp65 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp63}, TNode<UintPtrT>{tmp64});
    ca_.Branch(tmp65, &block36, std::vector<Node*>{phi_bb29_6, phi_bb29_7, phi_bb29_7}, &block37, std::vector<Node*>{phi_bb29_6, phi_bb29_7, phi_bb29_7});
  }

  TNode<Smi> phi_bb36_6;
  TNode<Smi> phi_bb36_13;
  TNode<Smi> phi_bb36_14;
  TNode<IntPtrT> tmp66;
  TNode<IntPtrT> tmp67;
  TNode<IntPtrT> tmp68;
  TNode<HeapObject> tmp69;
  TNode<IntPtrT> tmp70;
  TNode<IntPtrT> tmp71;
  TNode<IntPtrT> tmp72;
  TNode<IntPtrT> tmp73;
  TNode<Smi> tmp74;
  TNode<IntPtrT> tmp75;
  TNode<Smi> tmp76;
  TNode<Smi> tmp77;
  TNode<IntPtrT> tmp78;
  TNode<UintPtrT> tmp79;
  TNode<UintPtrT> tmp80;
  TNode<BoolT> tmp81;
  if (block36.is_used()) {
    ca_.Bind(&block36, &phi_bb36_6, &phi_bb36_13, &phi_bb36_14);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp66 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp67 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp62}, TNode<IntPtrT>{tmp66});
    tmp68 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp55}, TNode<IntPtrT>{tmp67});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp69, tmp70) = NewReference_Object_0(state_, TNode<HeapObject>{parameter3}, TNode<IntPtrT>{tmp68}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 484);
    tmp71 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp72 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp73 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp74 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter1, tmp73});
    tmp75 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp74});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 484);
    tmp76 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp77 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb36_6}, TNode<Smi>{tmp76});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp78 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb36_6});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp79 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp78});
    tmp80 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp75});
    tmp81 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp79}, TNode<UintPtrT>{tmp80});
    ca_.Branch(tmp81, &block43, std::vector<Node*>{phi_bb36_13, phi_bb36_14, phi_bb36_6, phi_bb36_6}, &block44, std::vector<Node*>{phi_bb36_13, phi_bb36_14, phi_bb36_6, phi_bb36_6});
  }

  TNode<Smi> phi_bb37_6;
  TNode<Smi> phi_bb37_13;
  TNode<Smi> phi_bb37_14;
  if (block37.is_used()) {
    ca_.Bind(&block37, &phi_bb37_6, &phi_bb37_13, &phi_bb37_14);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb43_13;
  TNode<Smi> phi_bb43_14;
  TNode<Smi> phi_bb43_21;
  TNode<Smi> phi_bb43_22;
  TNode<IntPtrT> tmp82;
  TNode<IntPtrT> tmp83;
  TNode<IntPtrT> tmp84;
  TNode<HeapObject> tmp85;
  TNode<IntPtrT> tmp86;
  TNode<Object> tmp87;
  if (block43.is_used()) {
    ca_.Bind(&block43, &phi_bb43_13, &phi_bb43_14, &phi_bb43_21, &phi_bb43_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp82 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp83 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp78}, TNode<IntPtrT>{tmp82});
    tmp84 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp71}, TNode<IntPtrT>{tmp83});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp85, tmp86) = NewReference_Object_0(state_, TNode<HeapObject>{parameter1}, TNode<IntPtrT>{tmp84}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 484);
    tmp87 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp85, tmp86});
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp69, tmp70}, tmp87);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 483);
    ca_.Goto(&block31, tmp77, tmp61);
  }

  TNode<Smi> phi_bb44_13;
  TNode<Smi> phi_bb44_14;
  TNode<Smi> phi_bb44_21;
  TNode<Smi> phi_bb44_22;
  if (block44.is_used()) {
    ca_.Bind(&block44, &phi_bb44_13, &phi_bb44_14, &phi_bb44_21, &phi_bb44_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb30_6;
  TNode<Smi> phi_bb30_7;
  if (block30.is_used()) {
    ca_.Bind(&block30, &phi_bb30_6, &phi_bb30_7);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 472);
    ca_.Goto(&block11);
  }

  TNode<Smi> tmp88;
  if (block11.is_used()) {
    ca_.Bind(&block11);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 487);
    tmp88 = kSuccess_0(state_);
    CodeStubAssembler(state_).Return(tmp88);
  }
}

void BinaryInsertionSort_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<SortState> p_sortState, TNode<Smi> p_low, TNode<Smi> p_startArg, TNode<Smi> p_high) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi> block19(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi> block20(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block23(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block22(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block26(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block24(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block31(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block32(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block34(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block35(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block36(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block25(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block38(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block37(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi> block41(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi> block39(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi> block47(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi> block48(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi> block54(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi> block55(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi> block40(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block61(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block62(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block64(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<BoolT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 502);
    tmp0 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{p_low}, TNode<Smi>{p_startArg});
    ca_.Branch(tmp0, &block4, std::vector<Node*>{}, &block5, std::vector<Node*>{});
  }

  TNode<BoolT> tmp1;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    tmp1 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{p_startArg}, TNode<Smi>{p_high});
    ca_.Goto(&block6, tmp1);
  }

  TNode<BoolT> tmp2;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    tmp2 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block6, tmp2);
  }

  TNode<BoolT> phi_bb6_6;
  if (block6.is_used()) {
    ca_.Bind(&block6, &phi_bb6_6);
    ca_.Branch(phi_bb6_6, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    CodeStubAssembler(state_).FailAssert("Torque assert 'low <= startArg && startArg <= high' failed", "third_party/v8/builtins/array-sort.tq", 502);
  }

  TNode<IntPtrT> tmp3;
  TNode<FixedArray> tmp4;
  TNode<BoolT> tmp5;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 504);
    tmp3 = FromConstexpr_intptr_constexpr_int31_0(state_, 52);
    tmp4 = CodeStubAssembler(state_).LoadReference<FixedArray>(CodeStubAssembler::Reference{p_sortState, tmp3});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 506);
    tmp5 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{p_low}, TNode<Smi>{p_startArg});
    ca_.Branch(tmp5, &block7, std::vector<Node*>{}, &block8, std::vector<Node*>{});
  }

  TNode<Smi> tmp6;
  TNode<Smi> tmp7;
  if (block7.is_used()) {
    ca_.Bind(&block7);
    tmp6 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp7 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{p_startArg}, TNode<Smi>{tmp6});
    ca_.Goto(&block9, tmp7);
  }

  if (block8.is_used()) {
    ca_.Bind(&block8);
    ca_.Goto(&block9, p_startArg);
  }

  TNode<Smi> phi_bb9_6;
  if (block9.is_used()) {
    ca_.Bind(&block9, &phi_bb9_6);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 508);
    ca_.Goto(&block13, phi_bb9_6);
  }

  TNode<Smi> phi_bb13_6;
  TNode<BoolT> tmp8;
  if (block13.is_used()) {
    ca_.Bind(&block13, &phi_bb13_6);
    tmp8 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{phi_bb13_6}, TNode<Smi>{p_high});
    ca_.Branch(tmp8, &block11, std::vector<Node*>{phi_bb13_6}, &block12, std::vector<Node*>{phi_bb13_6});
  }

  TNode<Smi> phi_bb11_6;
  TNode<IntPtrT> tmp9;
  TNode<IntPtrT> tmp10;
  TNode<IntPtrT> tmp11;
  TNode<Smi> tmp12;
  TNode<IntPtrT> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<UintPtrT> tmp15;
  TNode<UintPtrT> tmp16;
  TNode<BoolT> tmp17;
  if (block11.is_used()) {
    ca_.Bind(&block11, &phi_bb11_6);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 513);
    tmp9 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp10 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp11 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp12 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp4, tmp11});
    tmp13 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp12});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp14 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb11_6});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp15 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp14});
    tmp16 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp13});
    tmp17 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp15}, TNode<UintPtrT>{tmp16});
    ca_.Branch(tmp17, &block19, std::vector<Node*>{phi_bb11_6, phi_bb11_6, phi_bb11_6, phi_bb11_6}, &block20, std::vector<Node*>{phi_bb11_6, phi_bb11_6, phi_bb11_6, phi_bb11_6});
  }

  TNode<Smi> phi_bb19_6;
  TNode<Smi> phi_bb19_8;
  TNode<Smi> phi_bb19_13;
  TNode<Smi> phi_bb19_14;
  TNode<IntPtrT> tmp18;
  TNode<IntPtrT> tmp19;
  TNode<IntPtrT> tmp20;
  TNode<HeapObject> tmp21;
  TNode<IntPtrT> tmp22;
  TNode<Object> tmp23;
  TNode<Object> tmp24;
  TNode<BoolT> tmp25;
  if (block19.is_used()) {
    ca_.Bind(&block19, &phi_bb19_6, &phi_bb19_8, &phi_bb19_13, &phi_bb19_14);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp18 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp19 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp14}, TNode<IntPtrT>{tmp18});
    tmp20 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp9}, TNode<IntPtrT>{tmp19});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp21, tmp22) = NewReference_Object_0(state_, TNode<HeapObject>{tmp4}, TNode<IntPtrT>{tmp20}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 513);
    tmp23 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp21, tmp22});
    tmp24 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp23});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 518);
    tmp25 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{p_low}, TNode<Smi>{phi_bb19_8});
    ca_.Branch(tmp25, &block22, std::vector<Node*>{phi_bb19_6, phi_bb19_8}, &block23, std::vector<Node*>{phi_bb19_6, phi_bb19_8});
  }

  TNode<Smi> phi_bb20_6;
  TNode<Smi> phi_bb20_8;
  TNode<Smi> phi_bb20_13;
  TNode<Smi> phi_bb20_14;
  if (block20.is_used()) {
    ca_.Bind(&block20, &phi_bb20_6, &phi_bb20_8, &phi_bb20_13, &phi_bb20_14);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb23_6;
  TNode<Smi> phi_bb23_8;
  if (block23.is_used()) {
    ca_.Bind(&block23, &phi_bb23_6, &phi_bb23_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 518);
    CodeStubAssembler(state_).FailAssert("Torque assert 'left < right' failed", "third_party/v8/builtins/array-sort.tq", 518);
  }

  TNode<Smi> phi_bb22_6;
  TNode<Smi> phi_bb22_8;
  if (block22.is_used()) {
    ca_.Bind(&block22, &phi_bb22_6, &phi_bb22_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 521);
    ca_.Goto(&block26, phi_bb22_6, p_low, phi_bb22_8);
  }

  TNode<Smi> phi_bb26_6;
  TNode<Smi> phi_bb26_7;
  TNode<Smi> phi_bb26_8;
  TNode<BoolT> tmp26;
  if (block26.is_used()) {
    ca_.Bind(&block26, &phi_bb26_6, &phi_bb26_7, &phi_bb26_8);
    tmp26 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{phi_bb26_7}, TNode<Smi>{phi_bb26_8});
    ca_.Branch(tmp26, &block24, std::vector<Node*>{phi_bb26_6, phi_bb26_7, phi_bb26_8}, &block25, std::vector<Node*>{phi_bb26_6, phi_bb26_7, phi_bb26_8});
  }

  TNode<Smi> phi_bb24_6;
  TNode<Smi> phi_bb24_7;
  TNode<Smi> phi_bb24_8;
  TNode<Smi> tmp27;
  TNode<Smi> tmp28;
  TNode<Smi> tmp29;
  TNode<IntPtrT> tmp30;
  TNode<IntPtrT> tmp31;
  TNode<IntPtrT> tmp32;
  TNode<Smi> tmp33;
  TNode<IntPtrT> tmp34;
  TNode<IntPtrT> tmp35;
  TNode<UintPtrT> tmp36;
  TNode<UintPtrT> tmp37;
  TNode<BoolT> tmp38;
  if (block24.is_used()) {
    ca_.Bind(&block24, &phi_bb24_6, &phi_bb24_7, &phi_bb24_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 522);
    tmp27 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb24_8}, TNode<Smi>{phi_bb24_7});
    tmp28 = CodeStubAssembler(state_).SmiSar(TNode<Smi>{tmp27}, 1);
    tmp29 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb24_7}, TNode<Smi>{tmp28});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 524);
    tmp30 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp31 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp32 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp33 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp4, tmp32});
    tmp34 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp33});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp35 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp29});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp36 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp35});
    tmp37 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp34});
    tmp38 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp36}, TNode<UintPtrT>{tmp37});
    ca_.Branch(tmp38, &block31, std::vector<Node*>{phi_bb24_6, phi_bb24_7, phi_bb24_8}, &block32, std::vector<Node*>{phi_bb24_6, phi_bb24_7, phi_bb24_8});
  }

  TNode<Smi> phi_bb31_6;
  TNode<Smi> phi_bb31_7;
  TNode<Smi> phi_bb31_8;
  TNode<IntPtrT> tmp39;
  TNode<IntPtrT> tmp40;
  TNode<IntPtrT> tmp41;
  TNode<HeapObject> tmp42;
  TNode<IntPtrT> tmp43;
  TNode<Object> tmp44;
  TNode<Object> tmp45;
  TNode<Number> tmp46;
  TNode<Number> tmp47;
  TNode<BoolT> tmp48;
  if (block31.is_used()) {
    ca_.Bind(&block31, &phi_bb31_6, &phi_bb31_7, &phi_bb31_8);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp39 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp40 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp35}, TNode<IntPtrT>{tmp39});
    tmp41 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp30}, TNode<IntPtrT>{tmp40});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp42, tmp43) = NewReference_Object_0(state_, TNode<HeapObject>{tmp4}, TNode<IntPtrT>{tmp41}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 524);
    tmp44 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp42, tmp43});
    tmp45 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp44});
    tmp46 = Method_SortState_Compare_0(state_, TNode<Context>{p_context}, TNode<SortState>{p_sortState}, TNode<Object>{tmp24}, TNode<Object>{tmp45});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 526);
    tmp47 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp48 = NumberIsLessThan_0(state_, TNode<Number>{tmp46}, TNode<Number>{tmp47});
    ca_.Branch(tmp48, &block34, std::vector<Node*>{phi_bb31_6, phi_bb31_7, phi_bb31_8}, &block35, std::vector<Node*>{phi_bb31_6, phi_bb31_7, phi_bb31_8});
  }

  TNode<Smi> phi_bb32_6;
  TNode<Smi> phi_bb32_7;
  TNode<Smi> phi_bb32_8;
  if (block32.is_used()) {
    ca_.Bind(&block32, &phi_bb32_6, &phi_bb32_7, &phi_bb32_8);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb34_6;
  TNode<Smi> phi_bb34_7;
  TNode<Smi> phi_bb34_8;
  if (block34.is_used()) {
    ca_.Bind(&block34, &phi_bb34_6, &phi_bb34_7, &phi_bb34_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 526);
    ca_.Goto(&block36, phi_bb34_6, phi_bb34_7, tmp29);
  }

  TNode<Smi> phi_bb35_6;
  TNode<Smi> phi_bb35_7;
  TNode<Smi> phi_bb35_8;
  TNode<Smi> tmp49;
  TNode<Smi> tmp50;
  if (block35.is_used()) {
    ca_.Bind(&block35, &phi_bb35_6, &phi_bb35_7, &phi_bb35_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 529);
    tmp49 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp50 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp29}, TNode<Smi>{tmp49});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 526);
    ca_.Goto(&block36, phi_bb35_6, tmp50, phi_bb35_8);
  }

  TNode<Smi> phi_bb36_6;
  TNode<Smi> phi_bb36_7;
  TNode<Smi> phi_bb36_8;
  if (block36.is_used()) {
    ca_.Bind(&block36, &phi_bb36_6, &phi_bb36_7, &phi_bb36_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 521);
    ca_.Goto(&block26, phi_bb36_6, phi_bb36_7, phi_bb36_8);
  }

  TNode<Smi> phi_bb25_6;
  TNode<Smi> phi_bb25_7;
  TNode<Smi> phi_bb25_8;
  TNode<BoolT> tmp51;
  if (block25.is_used()) {
    ca_.Bind(&block25, &phi_bb25_6, &phi_bb25_7, &phi_bb25_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 532);
    tmp51 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{phi_bb25_7}, TNode<Smi>{phi_bb25_8});
    ca_.Branch(tmp51, &block37, std::vector<Node*>{phi_bb25_6, phi_bb25_7, phi_bb25_8}, &block38, std::vector<Node*>{phi_bb25_6, phi_bb25_7, phi_bb25_8});
  }

  TNode<Smi> phi_bb38_6;
  TNode<Smi> phi_bb38_7;
  TNode<Smi> phi_bb38_8;
  if (block38.is_used()) {
    ca_.Bind(&block38, &phi_bb38_6, &phi_bb38_7, &phi_bb38_8);
    CodeStubAssembler(state_).FailAssert("Torque assert 'left == right' failed", "third_party/v8/builtins/array-sort.tq", 532);
  }

  TNode<Smi> phi_bb37_6;
  TNode<Smi> phi_bb37_7;
  TNode<Smi> phi_bb37_8;
  if (block37.is_used()) {
    ca_.Bind(&block37, &phi_bb37_6, &phi_bb37_7, &phi_bb37_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 541);
    ca_.Goto(&block41, phi_bb37_6, phi_bb37_7, phi_bb37_8, phi_bb37_6);
  }

  TNode<Smi> phi_bb41_6;
  TNode<Smi> phi_bb41_7;
  TNode<Smi> phi_bb41_8;
  TNode<Smi> phi_bb41_10;
  TNode<BoolT> tmp52;
  if (block41.is_used()) {
    ca_.Bind(&block41, &phi_bb41_6, &phi_bb41_7, &phi_bb41_8, &phi_bb41_10);
    tmp52 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{phi_bb41_10}, TNode<Smi>{phi_bb41_7});
    ca_.Branch(tmp52, &block39, std::vector<Node*>{phi_bb41_6, phi_bb41_7, phi_bb41_8, phi_bb41_10}, &block40, std::vector<Node*>{phi_bb41_6, phi_bb41_7, phi_bb41_8, phi_bb41_10});
  }

  TNode<Smi> phi_bb39_6;
  TNode<Smi> phi_bb39_7;
  TNode<Smi> phi_bb39_8;
  TNode<Smi> phi_bb39_10;
  TNode<IntPtrT> tmp53;
  TNode<IntPtrT> tmp54;
  TNode<IntPtrT> tmp55;
  TNode<Smi> tmp56;
  TNode<IntPtrT> tmp57;
  TNode<IntPtrT> tmp58;
  TNode<UintPtrT> tmp59;
  TNode<UintPtrT> tmp60;
  TNode<BoolT> tmp61;
  if (block39.is_used()) {
    ca_.Bind(&block39, &phi_bb39_6, &phi_bb39_7, &phi_bb39_8, &phi_bb39_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 542);
    tmp53 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp54 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp55 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp56 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp4, tmp55});
    tmp57 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp56});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp58 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb39_10});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp59 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp58});
    tmp60 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp57});
    tmp61 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp59}, TNode<UintPtrT>{tmp60});
    ca_.Branch(tmp61, &block47, std::vector<Node*>{phi_bb39_6, phi_bb39_7, phi_bb39_8, phi_bb39_10, phi_bb39_10, phi_bb39_10}, &block48, std::vector<Node*>{phi_bb39_6, phi_bb39_7, phi_bb39_8, phi_bb39_10, phi_bb39_10, phi_bb39_10});
  }

  TNode<Smi> phi_bb47_6;
  TNode<Smi> phi_bb47_7;
  TNode<Smi> phi_bb47_8;
  TNode<Smi> phi_bb47_10;
  TNode<Smi> phi_bb47_15;
  TNode<Smi> phi_bb47_16;
  TNode<IntPtrT> tmp62;
  TNode<IntPtrT> tmp63;
  TNode<IntPtrT> tmp64;
  TNode<HeapObject> tmp65;
  TNode<IntPtrT> tmp66;
  TNode<IntPtrT> tmp67;
  TNode<IntPtrT> tmp68;
  TNode<IntPtrT> tmp69;
  TNode<Smi> tmp70;
  TNode<IntPtrT> tmp71;
  TNode<Smi> tmp72;
  TNode<Smi> tmp73;
  TNode<IntPtrT> tmp74;
  TNode<UintPtrT> tmp75;
  TNode<UintPtrT> tmp76;
  TNode<BoolT> tmp77;
  if (block47.is_used()) {
    ca_.Bind(&block47, &phi_bb47_6, &phi_bb47_7, &phi_bb47_8, &phi_bb47_10, &phi_bb47_15, &phi_bb47_16);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp62 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp63 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp58}, TNode<IntPtrT>{tmp62});
    tmp64 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp53}, TNode<IntPtrT>{tmp63});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp65, tmp66) = NewReference_Object_0(state_, TNode<HeapObject>{tmp4}, TNode<IntPtrT>{tmp64}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 542);
    tmp67 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp68 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp69 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp70 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp4, tmp69});
    tmp71 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp70});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 542);
    tmp72 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp73 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb47_10}, TNode<Smi>{tmp72});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp74 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp73});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp75 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp74});
    tmp76 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp71});
    tmp77 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp75}, TNode<UintPtrT>{tmp76});
    ca_.Branch(tmp77, &block54, std::vector<Node*>{phi_bb47_6, phi_bb47_7, phi_bb47_8, phi_bb47_10, phi_bb47_15, phi_bb47_16}, &block55, std::vector<Node*>{phi_bb47_6, phi_bb47_7, phi_bb47_8, phi_bb47_10, phi_bb47_15, phi_bb47_16});
  }

  TNode<Smi> phi_bb48_6;
  TNode<Smi> phi_bb48_7;
  TNode<Smi> phi_bb48_8;
  TNode<Smi> phi_bb48_10;
  TNode<Smi> phi_bb48_15;
  TNode<Smi> phi_bb48_16;
  if (block48.is_used()) {
    ca_.Bind(&block48, &phi_bb48_6, &phi_bb48_7, &phi_bb48_8, &phi_bb48_10, &phi_bb48_15, &phi_bb48_16);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb54_6;
  TNode<Smi> phi_bb54_7;
  TNode<Smi> phi_bb54_8;
  TNode<Smi> phi_bb54_10;
  TNode<Smi> phi_bb54_15;
  TNode<Smi> phi_bb54_16;
  TNode<IntPtrT> tmp78;
  TNode<IntPtrT> tmp79;
  TNode<IntPtrT> tmp80;
  TNode<HeapObject> tmp81;
  TNode<IntPtrT> tmp82;
  TNode<Object> tmp83;
  TNode<Smi> tmp84;
  TNode<Smi> tmp85;
  if (block54.is_used()) {
    ca_.Bind(&block54, &phi_bb54_6, &phi_bb54_7, &phi_bb54_8, &phi_bb54_10, &phi_bb54_15, &phi_bb54_16);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp78 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp79 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp74}, TNode<IntPtrT>{tmp78});
    tmp80 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp67}, TNode<IntPtrT>{tmp79});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp81, tmp82) = NewReference_Object_0(state_, TNode<HeapObject>{tmp4}, TNode<IntPtrT>{tmp80}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 542);
    tmp83 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp81, tmp82});
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp65, tmp66}, tmp83);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 541);
    tmp84 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp85 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb54_10}, TNode<Smi>{tmp84});
    ca_.Goto(&block41, phi_bb54_6, phi_bb54_7, phi_bb54_8, tmp85);
  }

  TNode<Smi> phi_bb55_6;
  TNode<Smi> phi_bb55_7;
  TNode<Smi> phi_bb55_8;
  TNode<Smi> phi_bb55_10;
  TNode<Smi> phi_bb55_15;
  TNode<Smi> phi_bb55_16;
  if (block55.is_used()) {
    ca_.Bind(&block55, &phi_bb55_6, &phi_bb55_7, &phi_bb55_8, &phi_bb55_10, &phi_bb55_15, &phi_bb55_16);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb40_6;
  TNode<Smi> phi_bb40_7;
  TNode<Smi> phi_bb40_8;
  TNode<Smi> phi_bb40_10;
  TNode<IntPtrT> tmp86;
  TNode<IntPtrT> tmp87;
  TNode<IntPtrT> tmp88;
  TNode<Smi> tmp89;
  TNode<IntPtrT> tmp90;
  TNode<IntPtrT> tmp91;
  TNode<UintPtrT> tmp92;
  TNode<UintPtrT> tmp93;
  TNode<BoolT> tmp94;
  if (block40.is_used()) {
    ca_.Bind(&block40, &phi_bb40_6, &phi_bb40_7, &phi_bb40_8, &phi_bb40_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 544);
    tmp86 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp87 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp88 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp89 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp4, tmp88});
    tmp90 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp89});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp91 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb40_7});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp92 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp91});
    tmp93 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp90});
    tmp94 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp92}, TNode<UintPtrT>{tmp93});
    ca_.Branch(tmp94, &block61, std::vector<Node*>{phi_bb40_6, phi_bb40_7, phi_bb40_8, phi_bb40_7, phi_bb40_7}, &block62, std::vector<Node*>{phi_bb40_6, phi_bb40_7, phi_bb40_8, phi_bb40_7, phi_bb40_7});
  }

  TNode<Smi> phi_bb61_6;
  TNode<Smi> phi_bb61_7;
  TNode<Smi> phi_bb61_8;
  TNode<Smi> phi_bb61_14;
  TNode<Smi> phi_bb61_15;
  TNode<IntPtrT> tmp95;
  TNode<IntPtrT> tmp96;
  TNode<IntPtrT> tmp97;
  TNode<HeapObject> tmp98;
  TNode<IntPtrT> tmp99;
  TNode<Smi> tmp100;
  TNode<Smi> tmp101;
  if (block61.is_used()) {
    ca_.Bind(&block61, &phi_bb61_6, &phi_bb61_7, &phi_bb61_8, &phi_bb61_14, &phi_bb61_15);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp95 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp96 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp91}, TNode<IntPtrT>{tmp95});
    tmp97 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp86}, TNode<IntPtrT>{tmp96});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp98, tmp99) = NewReference_Object_0(state_, TNode<HeapObject>{tmp4}, TNode<IntPtrT>{tmp97}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 544);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp98, tmp99}, tmp24);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 508);
    tmp100 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp101 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb61_6}, TNode<Smi>{tmp100});
    ca_.Goto(&block13, tmp101);
  }

  TNode<Smi> phi_bb62_6;
  TNode<Smi> phi_bb62_7;
  TNode<Smi> phi_bb62_8;
  TNode<Smi> phi_bb62_14;
  TNode<Smi> phi_bb62_15;
  if (block62.is_used()) {
    ca_.Bind(&block62, &phi_bb62_6, &phi_bb62_7, &phi_bb62_8, &phi_bb62_14, &phi_bb62_15);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb12_6;
  if (block12.is_used()) {
    ca_.Bind(&block12, &phi_bb12_6);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 500);
    ca_.Goto(&block64);
  }

    ca_.Bind(&block64);
}

TNode<Smi> CountAndMakeRun_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<SortState> p_sortState, TNode<Smi> p_lowArg, TNode<Smi> p_high) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block17(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block18(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block20(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block21(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block22(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Number, Object, Smi> block26(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Number, Object, Smi> block24(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Number, Object, Smi, Smi, Smi> block32(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Number, Object, Smi, Smi, Smi> block33(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Object, Smi> block35(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Object, Smi> block38(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Object, Smi> block39(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Object, Smi> block36(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Object, Smi> block40(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Object, Smi> block41(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Object, Smi> block37(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Number, Object, Smi> block25(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Number, Object> block42(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Number, Object> block43(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block44(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<BoolT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 567);
    tmp0 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{p_lowArg}, TNode<Smi>{p_high});
    ca_.Branch(tmp0, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    CodeStubAssembler(state_).FailAssert("Torque assert 'lowArg < high' failed", "third_party/v8/builtins/array-sort.tq", 567);
  }

  TNode<IntPtrT> tmp1;
  TNode<FixedArray> tmp2;
  TNode<Smi> tmp3;
  TNode<Smi> tmp4;
  TNode<BoolT> tmp5;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 569);
    tmp1 = FromConstexpr_intptr_constexpr_int31_0(state_, 52);
    tmp2 = CodeStubAssembler(state_).LoadReference<FixedArray>(CodeStubAssembler::Reference{p_sortState, tmp1});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 571);
    tmp3 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp4 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{p_lowArg}, TNode<Smi>{tmp3});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 572);
    tmp5 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp4}, TNode<Smi>{p_high});
    ca_.Branch(tmp5, &block4, std::vector<Node*>{}, &block5, std::vector<Node*>{});
  }

  TNode<Smi> tmp6;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    tmp6 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    ca_.Goto(&block1, tmp6);
  }

  TNode<Smi> tmp7;
  TNode<IntPtrT> tmp8;
  TNode<IntPtrT> tmp9;
  TNode<IntPtrT> tmp10;
  TNode<Smi> tmp11;
  TNode<IntPtrT> tmp12;
  TNode<IntPtrT> tmp13;
  TNode<UintPtrT> tmp14;
  TNode<UintPtrT> tmp15;
  TNode<BoolT> tmp16;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 574);
    tmp7 = FromConstexpr_Smi_constexpr_int31_0(state_, 2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 576);
    tmp8 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp9 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp10 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp11 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp2, tmp10});
    tmp12 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp11});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp13 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp4});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp14 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp13});
    tmp15 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp12});
    tmp16 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp14}, TNode<UintPtrT>{tmp15});
    ca_.Branch(tmp16, &block10, std::vector<Node*>{}, &block11, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp17;
  TNode<IntPtrT> tmp18;
  TNode<IntPtrT> tmp19;
  TNode<HeapObject> tmp20;
  TNode<IntPtrT> tmp21;
  TNode<Object> tmp22;
  TNode<Object> tmp23;
  TNode<IntPtrT> tmp24;
  TNode<IntPtrT> tmp25;
  TNode<IntPtrT> tmp26;
  TNode<Smi> tmp27;
  TNode<IntPtrT> tmp28;
  TNode<Smi> tmp29;
  TNode<Smi> tmp30;
  TNode<IntPtrT> tmp31;
  TNode<UintPtrT> tmp32;
  TNode<UintPtrT> tmp33;
  TNode<BoolT> tmp34;
  if (block10.is_used()) {
    ca_.Bind(&block10);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp17 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp18 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp13}, TNode<IntPtrT>{tmp17});
    tmp19 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp8}, TNode<IntPtrT>{tmp18});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp20, tmp21) = NewReference_Object_0(state_, TNode<HeapObject>{tmp2}, TNode<IntPtrT>{tmp19}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 576);
    tmp22 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp20, tmp21});
    tmp23 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp22});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 577);
    tmp24 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp25 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp26 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp27 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp2, tmp26});
    tmp28 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp27});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 577);
    tmp29 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp30 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp4}, TNode<Smi>{tmp29});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp31 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp30});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp32 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp31});
    tmp33 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp28});
    tmp34 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp32}, TNode<UintPtrT>{tmp33});
    ca_.Branch(tmp34, &block17, std::vector<Node*>{}, &block18, std::vector<Node*>{});
  }

  if (block11.is_used()) {
    ca_.Bind(&block11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp35;
  TNode<IntPtrT> tmp36;
  TNode<IntPtrT> tmp37;
  TNode<HeapObject> tmp38;
  TNode<IntPtrT> tmp39;
  TNode<Object> tmp40;
  TNode<Object> tmp41;
  TNode<Number> tmp42;
  TNode<Number> tmp43;
  TNode<BoolT> tmp44;
  if (block17.is_used()) {
    ca_.Bind(&block17);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp35 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp36 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp31}, TNode<IntPtrT>{tmp35});
    tmp37 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp24}, TNode<IntPtrT>{tmp36});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp38, tmp39) = NewReference_Object_0(state_, TNode<HeapObject>{tmp2}, TNode<IntPtrT>{tmp37}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 577);
    tmp40 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp38, tmp39});
    tmp41 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp40});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 578);
    tmp42 = Method_SortState_Compare_0(state_, TNode<Context>{p_context}, TNode<SortState>{p_sortState}, TNode<Object>{tmp23}, TNode<Object>{tmp41});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 583);
    tmp43 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp44 = NumberIsLessThan_0(state_, TNode<Number>{tmp42}, TNode<Number>{tmp43});
    ca_.Branch(tmp44, &block20, std::vector<Node*>{}, &block21, std::vector<Node*>{});
  }

  if (block18.is_used()) {
    ca_.Bind(&block18);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<BoolT> tmp45;
  if (block20.is_used()) {
    ca_.Bind(&block20);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 583);
    tmp45 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    ca_.Goto(&block22, tmp45);
  }

  TNode<BoolT> tmp46;
  if (block21.is_used()) {
    ca_.Bind(&block21);
    tmp46 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block22, tmp46);
  }

  TNode<BoolT> phi_bb22_10;
  TNode<Smi> tmp47;
  TNode<Smi> tmp48;
  if (block22.is_used()) {
    ca_.Bind(&block22, &phi_bb22_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 586);
    tmp47 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp48 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp4}, TNode<Smi>{tmp47});
    ca_.Goto(&block26, tmp7, tmp42, tmp23, tmp48);
  }

  TNode<Smi> phi_bb26_6;
  TNode<Number> phi_bb26_9;
  TNode<Object> phi_bb26_11;
  TNode<Smi> phi_bb26_12;
  TNode<BoolT> tmp49;
  if (block26.is_used()) {
    ca_.Bind(&block26, &phi_bb26_6, &phi_bb26_9, &phi_bb26_11, &phi_bb26_12);
    tmp49 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{phi_bb26_12}, TNode<Smi>{p_high});
    ca_.Branch(tmp49, &block24, std::vector<Node*>{phi_bb26_6, phi_bb26_9, phi_bb26_11, phi_bb26_12}, &block25, std::vector<Node*>{phi_bb26_6, phi_bb26_9, phi_bb26_11, phi_bb26_12});
  }

  TNode<Smi> phi_bb24_6;
  TNode<Number> phi_bb24_9;
  TNode<Object> phi_bb24_11;
  TNode<Smi> phi_bb24_12;
  TNode<IntPtrT> tmp50;
  TNode<IntPtrT> tmp51;
  TNode<IntPtrT> tmp52;
  TNode<Smi> tmp53;
  TNode<IntPtrT> tmp54;
  TNode<IntPtrT> tmp55;
  TNode<UintPtrT> tmp56;
  TNode<UintPtrT> tmp57;
  TNode<BoolT> tmp58;
  if (block24.is_used()) {
    ca_.Bind(&block24, &phi_bb24_6, &phi_bb24_9, &phi_bb24_11, &phi_bb24_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 587);
    tmp50 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp51 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp52 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp53 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp2, tmp52});
    tmp54 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp53});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp55 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb24_12});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp56 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp55});
    tmp57 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp54});
    tmp58 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp56}, TNode<UintPtrT>{tmp57});
    ca_.Branch(tmp58, &block32, std::vector<Node*>{phi_bb24_6, phi_bb24_9, phi_bb24_11, phi_bb24_12, phi_bb24_12, phi_bb24_12}, &block33, std::vector<Node*>{phi_bb24_6, phi_bb24_9, phi_bb24_11, phi_bb24_12, phi_bb24_12, phi_bb24_12});
  }

  TNode<Smi> phi_bb32_6;
  TNode<Number> phi_bb32_9;
  TNode<Object> phi_bb32_11;
  TNode<Smi> phi_bb32_12;
  TNode<Smi> phi_bb32_17;
  TNode<Smi> phi_bb32_18;
  TNode<IntPtrT> tmp59;
  TNode<IntPtrT> tmp60;
  TNode<IntPtrT> tmp61;
  TNode<HeapObject> tmp62;
  TNode<IntPtrT> tmp63;
  TNode<Object> tmp64;
  TNode<Object> tmp65;
  TNode<Number> tmp66;
  if (block32.is_used()) {
    ca_.Bind(&block32, &phi_bb32_6, &phi_bb32_9, &phi_bb32_11, &phi_bb32_12, &phi_bb32_17, &phi_bb32_18);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp59 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp60 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp55}, TNode<IntPtrT>{tmp59});
    tmp61 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp50}, TNode<IntPtrT>{tmp60});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp62, tmp63) = NewReference_Object_0(state_, TNode<HeapObject>{tmp2}, TNode<IntPtrT>{tmp61}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 587);
    tmp64 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp62, tmp63});
    tmp65 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp64});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 588);
    tmp66 = Method_SortState_Compare_0(state_, TNode<Context>{p_context}, TNode<SortState>{p_sortState}, TNode<Object>{tmp65}, TNode<Object>{phi_bb32_11});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 590);
    ca_.Branch(phi_bb22_10, &block35, std::vector<Node*>{phi_bb32_6, phi_bb32_11, phi_bb32_12}, &block36, std::vector<Node*>{phi_bb32_6, phi_bb32_11, phi_bb32_12});
  }

  TNode<Smi> phi_bb33_6;
  TNode<Number> phi_bb33_9;
  TNode<Object> phi_bb33_11;
  TNode<Smi> phi_bb33_12;
  TNode<Smi> phi_bb33_17;
  TNode<Smi> phi_bb33_18;
  if (block33.is_used()) {
    ca_.Bind(&block33, &phi_bb33_6, &phi_bb33_9, &phi_bb33_11, &phi_bb33_12, &phi_bb33_17, &phi_bb33_18);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb35_6;
  TNode<Object> phi_bb35_11;
  TNode<Smi> phi_bb35_12;
  TNode<Number> tmp67;
  TNode<BoolT> tmp68;
  if (block35.is_used()) {
    ca_.Bind(&block35, &phi_bb35_6, &phi_bb35_11, &phi_bb35_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 591);
    tmp67 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp68 = NumberIsGreaterThanOrEqual_0(state_, TNode<Number>{tmp66}, TNode<Number>{tmp67});
    ca_.Branch(tmp68, &block38, std::vector<Node*>{phi_bb35_6, phi_bb35_11, phi_bb35_12}, &block39, std::vector<Node*>{phi_bb35_6, phi_bb35_11, phi_bb35_12});
  }

  TNode<Smi> phi_bb38_6;
  TNode<Object> phi_bb38_11;
  TNode<Smi> phi_bb38_12;
  if (block38.is_used()) {
    ca_.Bind(&block38, &phi_bb38_6, &phi_bb38_11, &phi_bb38_12);
    ca_.Goto(&block25, phi_bb38_6, tmp66, phi_bb38_11, phi_bb38_12);
  }

  TNode<Smi> phi_bb39_6;
  TNode<Object> phi_bb39_11;
  TNode<Smi> phi_bb39_12;
  if (block39.is_used()) {
    ca_.Bind(&block39, &phi_bb39_6, &phi_bb39_11, &phi_bb39_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 590);
    ca_.Goto(&block37, phi_bb39_6, phi_bb39_11, phi_bb39_12);
  }

  TNode<Smi> phi_bb36_6;
  TNode<Object> phi_bb36_11;
  TNode<Smi> phi_bb36_12;
  TNode<Number> tmp69;
  TNode<BoolT> tmp70;
  if (block36.is_used()) {
    ca_.Bind(&block36, &phi_bb36_6, &phi_bb36_11, &phi_bb36_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 593);
    tmp69 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp70 = NumberIsLessThan_0(state_, TNode<Number>{tmp66}, TNode<Number>{tmp69});
    ca_.Branch(tmp70, &block40, std::vector<Node*>{phi_bb36_6, phi_bb36_11, phi_bb36_12}, &block41, std::vector<Node*>{phi_bb36_6, phi_bb36_11, phi_bb36_12});
  }

  TNode<Smi> phi_bb40_6;
  TNode<Object> phi_bb40_11;
  TNode<Smi> phi_bb40_12;
  if (block40.is_used()) {
    ca_.Bind(&block40, &phi_bb40_6, &phi_bb40_11, &phi_bb40_12);
    ca_.Goto(&block25, phi_bb40_6, tmp66, phi_bb40_11, phi_bb40_12);
  }

  TNode<Smi> phi_bb41_6;
  TNode<Object> phi_bb41_11;
  TNode<Smi> phi_bb41_12;
  if (block41.is_used()) {
    ca_.Bind(&block41, &phi_bb41_6, &phi_bb41_11, &phi_bb41_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 590);
    ca_.Goto(&block37, phi_bb41_6, phi_bb41_11, phi_bb41_12);
  }

  TNode<Smi> phi_bb37_6;
  TNode<Object> phi_bb37_11;
  TNode<Smi> phi_bb37_12;
  TNode<Smi> tmp71;
  TNode<Smi> tmp72;
  TNode<Smi> tmp73;
  TNode<Smi> tmp74;
  if (block37.is_used()) {
    ca_.Bind(&block37, &phi_bb37_6, &phi_bb37_11, &phi_bb37_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 597);
    tmp71 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp72 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb37_6}, TNode<Smi>{tmp71});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 586);
    tmp73 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp74 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb37_12}, TNode<Smi>{tmp73});
    ca_.Goto(&block26, tmp72, tmp66, tmp65, tmp74);
  }

  TNode<Smi> phi_bb25_6;
  TNode<Number> phi_bb25_9;
  TNode<Object> phi_bb25_11;
  TNode<Smi> phi_bb25_12;
  if (block25.is_used()) {
    ca_.Bind(&block25, &phi_bb25_6, &phi_bb25_9, &phi_bb25_11, &phi_bb25_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 600);
    ca_.Branch(phi_bb22_10, &block42, std::vector<Node*>{phi_bb25_6, phi_bb25_9, phi_bb25_11}, &block43, std::vector<Node*>{phi_bb25_6, phi_bb25_9, phi_bb25_11});
  }

  TNode<Smi> phi_bb42_6;
  TNode<Number> phi_bb42_9;
  TNode<Object> phi_bb42_11;
  TNode<Smi> tmp75;
  if (block42.is_used()) {
    ca_.Bind(&block42, &phi_bb42_6, &phi_bb42_9, &phi_bb42_11);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 601);
    tmp75 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{p_lowArg}, TNode<Smi>{phi_bb42_6});
    ReverseRange_0(state_, TNode<FixedArray>{tmp2}, TNode<Smi>{p_lowArg}, TNode<Smi>{tmp75});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 600);
    ca_.Goto(&block43, phi_bb42_6, phi_bb42_9, phi_bb42_11);
  }

  TNode<Smi> phi_bb43_6;
  TNode<Number> phi_bb43_9;
  TNode<Object> phi_bb43_11;
  if (block43.is_used()) {
    ca_.Bind(&block43, &phi_bb43_6, &phi_bb43_9, &phi_bb43_11);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 604);
    ca_.Goto(&block1, phi_bb43_6);
  }

  TNode<Smi> phi_bb1_4;
  if (block1.is_used()) {
    ca_.Bind(&block1, &phi_bb1_4);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 565);
    ca_.Goto(&block44, phi_bb1_4);
  }

  TNode<Smi> phi_bb44_4;
    ca_.Bind(&block44, &phi_bb44_4);
  return TNode<Smi>{phi_bb44_4};
}

void ReverseRange_0(compiler::CodeAssemblerState* state_, TNode<FixedArray> p_array, TNode<Smi> p_from, TNode<Smi> p_to) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi> block16(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi> block17(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block23(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block24(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block30(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block31(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block33(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Smi> tmp0;
  TNode<Smi> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 609);
    tmp0 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp1 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{p_to}, TNode<Smi>{tmp0});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 611);
    ca_.Goto(&block4, p_from, tmp1);
  }

  TNode<Smi> phi_bb4_3;
  TNode<Smi> phi_bb4_4;
  TNode<BoolT> tmp2;
  if (block4.is_used()) {
    ca_.Bind(&block4, &phi_bb4_3, &phi_bb4_4);
    tmp2 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{phi_bb4_3}, TNode<Smi>{phi_bb4_4});
    ca_.Branch(tmp2, &block2, std::vector<Node*>{phi_bb4_3, phi_bb4_4}, &block3, std::vector<Node*>{phi_bb4_3, phi_bb4_4});
  }

  TNode<Smi> phi_bb2_3;
  TNode<Smi> phi_bb2_4;
  TNode<IntPtrT> tmp3;
  TNode<IntPtrT> tmp4;
  TNode<IntPtrT> tmp5;
  TNode<Smi> tmp6;
  TNode<IntPtrT> tmp7;
  TNode<IntPtrT> tmp8;
  TNode<UintPtrT> tmp9;
  TNode<UintPtrT> tmp10;
  TNode<BoolT> tmp11;
  if (block2.is_used()) {
    ca_.Bind(&block2, &phi_bb2_3, &phi_bb2_4);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 612);
    tmp3 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp4 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp5 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp6 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_array, tmp5});
    tmp7 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp6});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp8 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb2_3});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp9 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp8});
    tmp10 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp7});
    tmp11 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp9}, TNode<UintPtrT>{tmp10});
    ca_.Branch(tmp11, &block9, std::vector<Node*>{phi_bb2_3, phi_bb2_4, phi_bb2_3, phi_bb2_3}, &block10, std::vector<Node*>{phi_bb2_3, phi_bb2_4, phi_bb2_3, phi_bb2_3});
  }

  TNode<Smi> phi_bb9_3;
  TNode<Smi> phi_bb9_4;
  TNode<Smi> phi_bb9_9;
  TNode<Smi> phi_bb9_10;
  TNode<IntPtrT> tmp12;
  TNode<IntPtrT> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<HeapObject> tmp15;
  TNode<IntPtrT> tmp16;
  TNode<Object> tmp17;
  TNode<IntPtrT> tmp18;
  TNode<IntPtrT> tmp19;
  TNode<IntPtrT> tmp20;
  TNode<Smi> tmp21;
  TNode<IntPtrT> tmp22;
  TNode<IntPtrT> tmp23;
  TNode<UintPtrT> tmp24;
  TNode<UintPtrT> tmp25;
  TNode<BoolT> tmp26;
  if (block9.is_used()) {
    ca_.Bind(&block9, &phi_bb9_3, &phi_bb9_4, &phi_bb9_9, &phi_bb9_10);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp12 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp13 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp8}, TNode<IntPtrT>{tmp12});
    tmp14 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp3}, TNode<IntPtrT>{tmp13});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp15, tmp16) = NewReference_Object_0(state_, TNode<HeapObject>{p_array}, TNode<IntPtrT>{tmp14}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 612);
    tmp17 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp15, tmp16});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 613);
    tmp18 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp19 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp20 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp21 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_array, tmp20});
    tmp22 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp21});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp23 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb9_4});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp24 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp23});
    tmp25 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp22});
    tmp26 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp24}, TNode<UintPtrT>{tmp25});
    ca_.Branch(tmp26, &block16, std::vector<Node*>{phi_bb9_3, phi_bb9_4, phi_bb9_4, phi_bb9_4}, &block17, std::vector<Node*>{phi_bb9_3, phi_bb9_4, phi_bb9_4, phi_bb9_4});
  }

  TNode<Smi> phi_bb10_3;
  TNode<Smi> phi_bb10_4;
  TNode<Smi> phi_bb10_9;
  TNode<Smi> phi_bb10_10;
  if (block10.is_used()) {
    ca_.Bind(&block10, &phi_bb10_3, &phi_bb10_4, &phi_bb10_9, &phi_bb10_10);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb16_3;
  TNode<Smi> phi_bb16_4;
  TNode<Smi> phi_bb16_10;
  TNode<Smi> phi_bb16_11;
  TNode<IntPtrT> tmp27;
  TNode<IntPtrT> tmp28;
  TNode<IntPtrT> tmp29;
  TNode<HeapObject> tmp30;
  TNode<IntPtrT> tmp31;
  TNode<Object> tmp32;
  TNode<IntPtrT> tmp33;
  TNode<IntPtrT> tmp34;
  TNode<IntPtrT> tmp35;
  TNode<Smi> tmp36;
  TNode<IntPtrT> tmp37;
  TNode<Smi> tmp38;
  TNode<Smi> tmp39;
  TNode<IntPtrT> tmp40;
  TNode<UintPtrT> tmp41;
  TNode<UintPtrT> tmp42;
  TNode<BoolT> tmp43;
  if (block16.is_used()) {
    ca_.Bind(&block16, &phi_bb16_3, &phi_bb16_4, &phi_bb16_10, &phi_bb16_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp27 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp28 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp23}, TNode<IntPtrT>{tmp27});
    tmp29 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp18}, TNode<IntPtrT>{tmp28});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp30, tmp31) = NewReference_Object_0(state_, TNode<HeapObject>{p_array}, TNode<IntPtrT>{tmp29}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 613);
    tmp32 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp30, tmp31});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 614);
    tmp33 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp34 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp35 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp36 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_array, tmp35});
    tmp37 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp36});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 614);
    tmp38 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp39 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb16_3}, TNode<Smi>{tmp38});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp40 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb16_3});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp41 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp40});
    tmp42 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp37});
    tmp43 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp41}, TNode<UintPtrT>{tmp42});
    ca_.Branch(tmp43, &block23, std::vector<Node*>{phi_bb16_4, phi_bb16_3, phi_bb16_3}, &block24, std::vector<Node*>{phi_bb16_4, phi_bb16_3, phi_bb16_3});
  }

  TNode<Smi> phi_bb17_3;
  TNode<Smi> phi_bb17_4;
  TNode<Smi> phi_bb17_10;
  TNode<Smi> phi_bb17_11;
  if (block17.is_used()) {
    ca_.Bind(&block17, &phi_bb17_3, &phi_bb17_4, &phi_bb17_10, &phi_bb17_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb23_4;
  TNode<Smi> phi_bb23_11;
  TNode<Smi> phi_bb23_12;
  TNode<IntPtrT> tmp44;
  TNode<IntPtrT> tmp45;
  TNode<IntPtrT> tmp46;
  TNode<HeapObject> tmp47;
  TNode<IntPtrT> tmp48;
  TNode<IntPtrT> tmp49;
  TNode<IntPtrT> tmp50;
  TNode<IntPtrT> tmp51;
  TNode<Smi> tmp52;
  TNode<IntPtrT> tmp53;
  TNode<Smi> tmp54;
  TNode<Smi> tmp55;
  TNode<IntPtrT> tmp56;
  TNode<UintPtrT> tmp57;
  TNode<UintPtrT> tmp58;
  TNode<BoolT> tmp59;
  if (block23.is_used()) {
    ca_.Bind(&block23, &phi_bb23_4, &phi_bb23_11, &phi_bb23_12);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp44 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp45 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp40}, TNode<IntPtrT>{tmp44});
    tmp46 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp33}, TNode<IntPtrT>{tmp45});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp47, tmp48) = NewReference_Object_0(state_, TNode<HeapObject>{p_array}, TNode<IntPtrT>{tmp46}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 614);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp47, tmp48}, tmp32);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 615);
    tmp49 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp50 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp51 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp52 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_array, tmp51});
    tmp53 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp52});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 615);
    tmp54 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp55 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb23_4}, TNode<Smi>{tmp54});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp56 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb23_4});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp57 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp56});
    tmp58 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp53});
    tmp59 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp57}, TNode<UintPtrT>{tmp58});
    ca_.Branch(tmp59, &block30, std::vector<Node*>{phi_bb23_4, phi_bb23_4}, &block31, std::vector<Node*>{phi_bb23_4, phi_bb23_4});
  }

  TNode<Smi> phi_bb24_4;
  TNode<Smi> phi_bb24_11;
  TNode<Smi> phi_bb24_12;
  if (block24.is_used()) {
    ca_.Bind(&block24, &phi_bb24_4, &phi_bb24_11, &phi_bb24_12);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb30_11;
  TNode<Smi> phi_bb30_12;
  TNode<IntPtrT> tmp60;
  TNode<IntPtrT> tmp61;
  TNode<IntPtrT> tmp62;
  TNode<HeapObject> tmp63;
  TNode<IntPtrT> tmp64;
  if (block30.is_used()) {
    ca_.Bind(&block30, &phi_bb30_11, &phi_bb30_12);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp60 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp61 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp56}, TNode<IntPtrT>{tmp60});
    tmp62 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp49}, TNode<IntPtrT>{tmp61});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp63, tmp64) = NewReference_Object_0(state_, TNode<HeapObject>{p_array}, TNode<IntPtrT>{tmp62}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 615);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp63, tmp64}, tmp17);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 611);
    ca_.Goto(&block4, tmp39, tmp55);
  }

  TNode<Smi> phi_bb31_11;
  TNode<Smi> phi_bb31_12;
  if (block31.is_used()) {
    ca_.Bind(&block31, &phi_bb31_11, &phi_bb31_12);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb3_3;
  TNode<Smi> phi_bb3_4;
  if (block3.is_used()) {
    ca_.Bind(&block3, &phi_bb3_3, &phi_bb3_4);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 607);
    ca_.Goto(&block33);
  }

    ca_.Bind(&block33);
}

TF_BUILTIN(MergeAt, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<SortState> parameter1 = UncheckedCast<SortState>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<Smi> parameter2 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block14(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block16(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block15(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block17(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block18(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block23(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block24(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block27(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block26(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block28(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block29(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block31(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block30(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block36(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block37(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block40(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block39(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block41(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block42(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block43(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block44(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block45(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Smi> tmp0;
  TNode<Smi> tmp1;
  TNode<BoolT> tmp2;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 623);
    tmp0 = GetPendingRunsSize_0(state_, TNode<Context>{parameter0}, TNode<SortState>{parameter1});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 627);
    tmp1 = FromConstexpr_Smi_constexpr_int31_0(state_, 2);
    tmp2 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{tmp0}, TNode<Smi>{tmp1});
    ca_.Branch(tmp2, &block1, std::vector<Node*>{}, &block2, std::vector<Node*>{});
  }

  if (block2.is_used()) {
    ca_.Bind(&block2);
    CodeStubAssembler(state_).FailAssert("Torque assert 'stackSize >= 2' failed", "third_party/v8/builtins/array-sort.tq", 627);
  }

  TNode<Smi> tmp3;
  TNode<BoolT> tmp4;
  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 628);
    tmp3 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp4 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{parameter2}, TNode<Smi>{tmp3});
    ca_.Branch(tmp4, &block3, std::vector<Node*>{}, &block4, std::vector<Node*>{});
  }

  if (block4.is_used()) {
    ca_.Bind(&block4);
    CodeStubAssembler(state_).FailAssert("Torque assert 'i >= 0' failed", "third_party/v8/builtins/array-sort.tq", 628);
  }

  TNode<Smi> tmp5;
  TNode<Smi> tmp6;
  TNode<BoolT> tmp7;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 629);
    tmp5 = FromConstexpr_Smi_constexpr_int31_0(state_, 2);
    tmp6 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp0}, TNode<Smi>{tmp5});
    tmp7 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{parameter2}, TNode<Smi>{tmp6});
    ca_.Branch(tmp7, &block7, std::vector<Node*>{}, &block8, std::vector<Node*>{});
  }

  TNode<BoolT> tmp8;
  if (block7.is_used()) {
    ca_.Bind(&block7);
    tmp8 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    ca_.Goto(&block9, tmp8);
  }

  TNode<Smi> tmp9;
  TNode<Smi> tmp10;
  TNode<BoolT> tmp11;
  if (block8.is_used()) {
    ca_.Bind(&block8);
    tmp9 = FromConstexpr_Smi_constexpr_int31_0(state_, 3);
    tmp10 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp0}, TNode<Smi>{tmp9});
    tmp11 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{parameter2}, TNode<Smi>{tmp10});
    ca_.Goto(&block9, tmp11);
  }

  TNode<BoolT> phi_bb9_5;
  if (block9.is_used()) {
    ca_.Bind(&block9, &phi_bb9_5);
    ca_.Branch(phi_bb9_5, &block5, std::vector<Node*>{}, &block6, std::vector<Node*>{});
  }

  if (block6.is_used()) {
    ca_.Bind(&block6);
    CodeStubAssembler(state_).FailAssert("Torque assert 'i == stackSize - 2 || i == stackSize - 3' failed", "third_party/v8/builtins/array-sort.tq", 629);
  }

  TNode<IntPtrT> tmp12;
  TNode<FixedArray> tmp13;
  TNode<IntPtrT> tmp14;
  TNode<FixedArray> tmp15;
  TNode<Smi> tmp16;
  TNode<Smi> tmp17;
  TNode<Smi> tmp18;
  TNode<Smi> tmp19;
  TNode<Smi> tmp20;
  TNode<Smi> tmp21;
  TNode<Smi> tmp22;
  TNode<Smi> tmp23;
  TNode<Smi> tmp24;
  TNode<BoolT> tmp25;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 631);
    tmp12 = FromConstexpr_intptr_constexpr_int31_0(state_, 52);
    tmp13 = CodeStubAssembler(state_).LoadReference<FixedArray>(CodeStubAssembler::Reference{parameter1, tmp12});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 633);
    tmp14 = FromConstexpr_intptr_constexpr_int31_0(state_, 48);
    tmp15 = CodeStubAssembler(state_).LoadReference<FixedArray>(CodeStubAssembler::Reference{parameter1, tmp14});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 634);
    tmp16 = GetPendingRunBase_0(state_, TNode<Context>{parameter0}, TNode<FixedArray>{tmp15}, TNode<Smi>{parameter2});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 635);
    tmp17 = GetPendingRunLength_0(state_, TNode<Context>{parameter0}, TNode<FixedArray>{tmp15}, TNode<Smi>{parameter2});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 636);
    tmp18 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp19 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{parameter2}, TNode<Smi>{tmp18});
    tmp20 = GetPendingRunBase_0(state_, TNode<Context>{parameter0}, TNode<FixedArray>{tmp15}, TNode<Smi>{tmp19});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 637);
    tmp21 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp22 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{parameter2}, TNode<Smi>{tmp21});
    tmp23 = GetPendingRunLength_0(state_, TNode<Context>{parameter0}, TNode<FixedArray>{tmp15}, TNode<Smi>{tmp22});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 638);
    tmp24 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp25 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{tmp17}, TNode<Smi>{tmp24});
    ca_.Branch(tmp25, &block12, std::vector<Node*>{}, &block13, std::vector<Node*>{});
  }

  TNode<Smi> tmp26;
  TNode<BoolT> tmp27;
  if (block12.is_used()) {
    ca_.Bind(&block12);
    tmp26 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp27 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{tmp23}, TNode<Smi>{tmp26});
    ca_.Goto(&block14, tmp27);
  }

  TNode<BoolT> tmp28;
  if (block13.is_used()) {
    ca_.Bind(&block13);
    tmp28 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block14, tmp28);
  }

  TNode<BoolT> phi_bb14_11;
  if (block14.is_used()) {
    ca_.Bind(&block14, &phi_bb14_11);
    ca_.Branch(phi_bb14_11, &block10, std::vector<Node*>{}, &block11, std::vector<Node*>{});
  }

  if (block11.is_used()) {
    ca_.Bind(&block11);
    CodeStubAssembler(state_).FailAssert("Torque assert 'lengthA > 0 && lengthB > 0' failed", "third_party/v8/builtins/array-sort.tq", 638);
  }

  TNode<Smi> tmp29;
  TNode<BoolT> tmp30;
  if (block10.is_used()) {
    ca_.Bind(&block10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 639);
    tmp29 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp16}, TNode<Smi>{tmp17});
    tmp30 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp29}, TNode<Smi>{tmp20});
    ca_.Branch(tmp30, &block15, std::vector<Node*>{}, &block16, std::vector<Node*>{});
  }

  if (block16.is_used()) {
    ca_.Bind(&block16);
    CodeStubAssembler(state_).FailAssert("Torque assert 'baseA + lengthA == baseB' failed", "third_party/v8/builtins/array-sort.tq", 639);
  }

  TNode<Smi> tmp31;
  TNode<Smi> tmp32;
  TNode<Smi> tmp33;
  TNode<BoolT> tmp34;
  if (block15.is_used()) {
    ca_.Bind(&block15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 644);
    tmp31 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp17}, TNode<Smi>{tmp23});
    SetPendingRunLength_0(state_, TNode<FixedArray>{tmp15}, TNode<Smi>{parameter2}, TNode<Smi>{tmp31});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 645);
    tmp32 = FromConstexpr_Smi_constexpr_int31_0(state_, 3);
    tmp33 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp0}, TNode<Smi>{tmp32});
    tmp34 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{parameter2}, TNode<Smi>{tmp33});
    ca_.Branch(tmp34, &block17, std::vector<Node*>{}, &block18, std::vector<Node*>{});
  }

  TNode<Smi> tmp35;
  TNode<Smi> tmp36;
  TNode<Smi> tmp37;
  TNode<Smi> tmp38;
  TNode<Smi> tmp39;
  TNode<Smi> tmp40;
  TNode<Smi> tmp41;
  TNode<Smi> tmp42;
  TNode<Smi> tmp43;
  TNode<Smi> tmp44;
  if (block17.is_used()) {
    ca_.Bind(&block17);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 646);
    tmp35 = FromConstexpr_Smi_constexpr_int31_0(state_, 2);
    tmp36 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{parameter2}, TNode<Smi>{tmp35});
    tmp37 = GetPendingRunBase_0(state_, TNode<Context>{parameter0}, TNode<FixedArray>{tmp15}, TNode<Smi>{tmp36});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 647);
    tmp38 = FromConstexpr_Smi_constexpr_int31_0(state_, 2);
    tmp39 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{parameter2}, TNode<Smi>{tmp38});
    tmp40 = GetPendingRunLength_0(state_, TNode<Context>{parameter0}, TNode<FixedArray>{tmp15}, TNode<Smi>{tmp39});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 648);
    tmp41 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp42 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{parameter2}, TNode<Smi>{tmp41});
    SetPendingRunBase_0(state_, TNode<FixedArray>{tmp15}, TNode<Smi>{tmp42}, TNode<Smi>{tmp37});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 649);
    tmp43 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp44 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{parameter2}, TNode<Smi>{tmp43});
    SetPendingRunLength_0(state_, TNode<FixedArray>{tmp15}, TNode<Smi>{tmp44}, TNode<Smi>{tmp40});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 645);
    ca_.Goto(&block18);
  }

  TNode<IntPtrT> tmp45;
  TNode<Smi> tmp46;
  TNode<Smi> tmp47;
  TNode<IntPtrT> tmp48;
  TNode<IntPtrT> tmp49;
  TNode<IntPtrT> tmp50;
  TNode<Smi> tmp51;
  TNode<IntPtrT> tmp52;
  TNode<IntPtrT> tmp53;
  TNode<UintPtrT> tmp54;
  TNode<UintPtrT> tmp55;
  TNode<BoolT> tmp56;
  if (block18.is_used()) {
    ca_.Bind(&block18);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 651);
    tmp45 = FromConstexpr_intptr_constexpr_int31_0(state_, 44);
    tmp46 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp47 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp0}, TNode<Smi>{tmp46});
    CodeStubAssembler(state_).StoreReference<Smi>(CodeStubAssembler::Reference{parameter1, tmp45}, tmp47);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 655);
    tmp48 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp49 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp50 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp51 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp50});
    tmp52 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp51});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp53 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp20});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp54 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp53});
    tmp55 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp52});
    tmp56 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp54}, TNode<UintPtrT>{tmp55});
    ca_.Branch(tmp56, &block23, std::vector<Node*>{}, &block24, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp57;
  TNode<IntPtrT> tmp58;
  TNode<IntPtrT> tmp59;
  TNode<HeapObject> tmp60;
  TNode<IntPtrT> tmp61;
  TNode<Object> tmp62;
  TNode<Object> tmp63;
  TNode<Smi> tmp64;
  TNode<Smi> tmp65;
  TNode<Smi> tmp66;
  TNode<BoolT> tmp67;
  if (block23.is_used()) {
    ca_.Bind(&block23);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp57 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp58 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp53}, TNode<IntPtrT>{tmp57});
    tmp59 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp48}, TNode<IntPtrT>{tmp58});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp60, tmp61) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp59}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 655);
    tmp62 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp60, tmp61});
    tmp63 = UnsafeCast_JSAny_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp62});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 656);
    tmp64 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp65 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kGallopRight, parameter0, parameter1, tmp13, tmp63, tmp16, tmp17, tmp64));
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 657);
    tmp66 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp67 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{tmp65}, TNode<Smi>{tmp66});
    ca_.Branch(tmp67, &block26, std::vector<Node*>{}, &block27, std::vector<Node*>{});
  }

  if (block24.is_used()) {
    ca_.Bind(&block24);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  if (block27.is_used()) {
    ca_.Bind(&block27);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 657);
    CodeStubAssembler(state_).FailAssert("Torque assert 'k >= 0' failed", "third_party/v8/builtins/array-sort.tq", 657);
  }

  TNode<Smi> tmp68;
  TNode<Smi> tmp69;
  TNode<Smi> tmp70;
  TNode<BoolT> tmp71;
  if (block26.is_used()) {
    ca_.Bind(&block26);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 659);
    tmp68 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp16}, TNode<Smi>{tmp65});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 660);
    tmp69 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp17}, TNode<Smi>{tmp65});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 661);
    tmp70 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp71 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp69}, TNode<Smi>{tmp70});
    ca_.Branch(tmp71, &block28, std::vector<Node*>{}, &block29, std::vector<Node*>{});
  }

  TNode<Smi> tmp72;
  if (block28.is_used()) {
    ca_.Bind(&block28);
    tmp72 = kSuccess_0(state_);
    CodeStubAssembler(state_).Return(tmp72);
  }

  TNode<Smi> tmp73;
  TNode<BoolT> tmp74;
  if (block29.is_used()) {
    ca_.Bind(&block29);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 662);
    tmp73 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp74 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{tmp69}, TNode<Smi>{tmp73});
    ca_.Branch(tmp74, &block30, std::vector<Node*>{}, &block31, std::vector<Node*>{});
  }

  if (block31.is_used()) {
    ca_.Bind(&block31);
    CodeStubAssembler(state_).FailAssert("Torque assert 'lengthA > 0' failed", "third_party/v8/builtins/array-sort.tq", 662);
  }

  TNode<IntPtrT> tmp75;
  TNode<IntPtrT> tmp76;
  TNode<IntPtrT> tmp77;
  TNode<Smi> tmp78;
  TNode<IntPtrT> tmp79;
  TNode<Smi> tmp80;
  TNode<Smi> tmp81;
  TNode<Smi> tmp82;
  TNode<IntPtrT> tmp83;
  TNode<UintPtrT> tmp84;
  TNode<UintPtrT> tmp85;
  TNode<BoolT> tmp86;
  if (block30.is_used()) {
    ca_.Bind(&block30);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 666);
    tmp75 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp76 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp77 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp78 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp77});
    tmp79 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp78});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 666);
    tmp80 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp68}, TNode<Smi>{tmp69});
    tmp81 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp82 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp80}, TNode<Smi>{tmp81});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp83 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp82});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp84 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp83});
    tmp85 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp79});
    tmp86 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp84}, TNode<UintPtrT>{tmp85});
    ca_.Branch(tmp86, &block36, std::vector<Node*>{}, &block37, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp87;
  TNode<IntPtrT> tmp88;
  TNode<IntPtrT> tmp89;
  TNode<HeapObject> tmp90;
  TNode<IntPtrT> tmp91;
  TNode<Object> tmp92;
  TNode<Object> tmp93;
  TNode<Smi> tmp94;
  TNode<Smi> tmp95;
  TNode<Smi> tmp96;
  TNode<Smi> tmp97;
  TNode<BoolT> tmp98;
  if (block36.is_used()) {
    ca_.Bind(&block36);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp87 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp88 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp83}, TNode<IntPtrT>{tmp87});
    tmp89 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp75}, TNode<IntPtrT>{tmp88});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp90, tmp91) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp89}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 666);
    tmp92 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp90, tmp91});
    tmp93 = UnsafeCast_JSAny_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp92});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 667);
    tmp94 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp95 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp23}, TNode<Smi>{tmp94});
    tmp96 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kGallopLeft, parameter0, parameter1, tmp13, tmp93, tmp20, tmp23, tmp95));
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 668);
    tmp97 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp98 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{tmp96}, TNode<Smi>{tmp97});
    ca_.Branch(tmp98, &block39, std::vector<Node*>{}, &block40, std::vector<Node*>{});
  }

  if (block37.is_used()) {
    ca_.Bind(&block37);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  if (block40.is_used()) {
    ca_.Bind(&block40);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 668);
    CodeStubAssembler(state_).FailAssert("Torque assert 'lengthB >= 0' failed", "third_party/v8/builtins/array-sort.tq", 668);
  }

  TNode<Smi> tmp99;
  TNode<BoolT> tmp100;
  if (block39.is_used()) {
    ca_.Bind(&block39);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 669);
    tmp99 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp100 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp96}, TNode<Smi>{tmp99});
    ca_.Branch(tmp100, &block41, std::vector<Node*>{}, &block42, std::vector<Node*>{});
  }

  TNode<Smi> tmp101;
  if (block41.is_used()) {
    ca_.Bind(&block41);
    tmp101 = kSuccess_0(state_);
    CodeStubAssembler(state_).Return(tmp101);
  }

  TNode<BoolT> tmp102;
  if (block42.is_used()) {
    ca_.Bind(&block42);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 673);
    tmp102 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{tmp69}, TNode<Smi>{tmp96});
    ca_.Branch(tmp102, &block43, std::vector<Node*>{}, &block44, std::vector<Node*>{});
  }

  if (block43.is_used()) {
    ca_.Bind(&block43);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 674);
    MergeLow_0(state_, TNode<Context>{parameter0}, TNode<SortState>{parameter1}, TNode<Smi>{tmp68}, TNode<Smi>{tmp69}, TNode<Smi>{tmp20}, TNode<Smi>{tmp96});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 673);
    ca_.Goto(&block45);
  }

  if (block44.is_used()) {
    ca_.Bind(&block44);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 676);
    MergeHigh_0(state_, TNode<Context>{parameter0}, TNode<SortState>{parameter1}, TNode<Smi>{tmp68}, TNode<Smi>{tmp69}, TNode<Smi>{tmp20}, TNode<Smi>{tmp96});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 673);
    ca_.Goto(&block45);
  }

  TNode<Smi> tmp103;
  if (block45.is_used()) {
    ca_.Bind(&block45);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 678);
    tmp103 = kSuccess_0(state_);
    CodeStubAssembler(state_).Return(tmp103);
  }
}

TF_BUILTIN(GallopLeft, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<SortState> parameter1 = UncheckedCast<SortState>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<FixedArray> parameter2 = UncheckedCast<FixedArray>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  TNode<Object> parameter3 = UncheckedCast<Object>(Parameter(Descriptor::ParameterIndex<2>()));
  USE(parameter3);
  TNode<Smi> parameter4 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<3>()));
  USE(parameter4);
  TNode<Smi> parameter5 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<4>()));
  USE(parameter5);
  TNode<Smi> parameter6 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<5>()));
  USE(parameter6);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block15(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block16(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block18(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block23(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block21(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block28(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block29(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block31(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block32(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block33(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block34(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block22(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block35(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block36(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block19(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block38(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block37(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block41(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block39(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block46(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block47(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block49(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block50(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block51(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block52(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block40(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block53(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block54(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block20(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block57(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block58(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number, BoolT> block59(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block60(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block61(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number, BoolT> block62(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block56(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block55(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block65(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block63(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block70(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block71(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block73(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block74(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block75(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block64(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block77(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block76(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block80(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block81(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number, BoolT> block82(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block79(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block78(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Smi> tmp0;
  TNode<BoolT> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 701);
    tmp0 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp1 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{parameter5}, TNode<Smi>{tmp0});
    ca_.Branch(tmp1, &block3, std::vector<Node*>{}, &block4, std::vector<Node*>{});
  }

  TNode<Smi> tmp2;
  TNode<BoolT> tmp3;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    tmp2 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp3 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{parameter4}, TNode<Smi>{tmp2});
    ca_.Goto(&block5, tmp3);
  }

  TNode<BoolT> tmp4;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    tmp4 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block5, tmp4);
  }

  TNode<BoolT> phi_bb5_8;
  if (block5.is_used()) {
    ca_.Bind(&block5, &phi_bb5_8);
    ca_.Branch(phi_bb5_8, &block1, std::vector<Node*>{}, &block2, std::vector<Node*>{});
  }

  if (block2.is_used()) {
    ca_.Bind(&block2);
    CodeStubAssembler(state_).FailAssert("Torque assert 'length > 0 && base >= 0' failed", "third_party/v8/builtins/array-sort.tq", 701);
  }

  TNode<Smi> tmp5;
  TNode<BoolT> tmp6;
  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 702);
    tmp5 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp6 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{tmp5}, TNode<Smi>{parameter6});
    ca_.Branch(tmp6, &block8, std::vector<Node*>{}, &block9, std::vector<Node*>{});
  }

  TNode<BoolT> tmp7;
  if (block8.is_used()) {
    ca_.Bind(&block8);
    tmp7 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{parameter6}, TNode<Smi>{parameter5});
    ca_.Goto(&block10, tmp7);
  }

  TNode<BoolT> tmp8;
  if (block9.is_used()) {
    ca_.Bind(&block9);
    tmp8 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block10, tmp8);
  }

  TNode<BoolT> phi_bb10_8;
  if (block10.is_used()) {
    ca_.Bind(&block10, &phi_bb10_8);
    ca_.Branch(phi_bb10_8, &block6, std::vector<Node*>{}, &block7, std::vector<Node*>{});
  }

  if (block7.is_used()) {
    ca_.Bind(&block7);
    CodeStubAssembler(state_).FailAssert("Torque assert '0 <= hint && hint < length' failed", "third_party/v8/builtins/array-sort.tq", 702);
  }

  TNode<Smi> tmp9;
  TNode<Smi> tmp10;
  TNode<IntPtrT> tmp11;
  TNode<IntPtrT> tmp12;
  TNode<IntPtrT> tmp13;
  TNode<Smi> tmp14;
  TNode<IntPtrT> tmp15;
  TNode<Smi> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<UintPtrT> tmp18;
  TNode<UintPtrT> tmp19;
  TNode<BoolT> tmp20;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 704);
    tmp9 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 705);
    tmp10 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 707);
    tmp11 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp12 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp13 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp14 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter2, tmp13});
    tmp15 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp14});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 707);
    tmp16 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{parameter4}, TNode<Smi>{parameter6});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp17 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp16});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp18 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp17});
    tmp19 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp15});
    tmp20 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp18}, TNode<UintPtrT>{tmp19});
    ca_.Branch(tmp20, &block15, std::vector<Node*>{}, &block16, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp21;
  TNode<IntPtrT> tmp22;
  TNode<IntPtrT> tmp23;
  TNode<HeapObject> tmp24;
  TNode<IntPtrT> tmp25;
  TNode<Object> tmp26;
  TNode<Object> tmp27;
  TNode<Number> tmp28;
  TNode<Number> tmp29;
  TNode<BoolT> tmp30;
  if (block15.is_used()) {
    ca_.Bind(&block15);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp21 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp22 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp17}, TNode<IntPtrT>{tmp21});
    tmp23 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp11}, TNode<IntPtrT>{tmp22});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp24, tmp25) = NewReference_Object_0(state_, TNode<HeapObject>{parameter2}, TNode<IntPtrT>{tmp23}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 707);
    tmp26 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp24, tmp25});
    tmp27 = UnsafeCast_JSAny_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp26});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 708);
    tmp28 = Method_SortState_Compare_0(state_, TNode<Context>{parameter0}, TNode<SortState>{parameter1}, TNode<Object>{tmp27}, TNode<Object>{parameter3});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 710);
    tmp29 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp30 = NumberIsLessThan_0(state_, TNode<Number>{tmp28}, TNode<Number>{tmp29});
    ca_.Branch(tmp30, &block18, std::vector<Node*>{}, &block19, std::vector<Node*>{});
  }

  if (block16.is_used()) {
    ca_.Bind(&block16);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> tmp31;
  if (block18.is_used()) {
    ca_.Bind(&block18);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 715);
    tmp31 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{parameter5}, TNode<Smi>{parameter6});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 716);
    ca_.Goto(&block23, tmp9, tmp10, tmp28);
  }

  TNode<Smi> phi_bb23_7;
  TNode<Smi> phi_bb23_8;
  TNode<Number> phi_bb23_10;
  TNode<BoolT> tmp32;
  if (block23.is_used()) {
    ca_.Bind(&block23, &phi_bb23_7, &phi_bb23_8, &phi_bb23_10);
    tmp32 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{phi_bb23_8}, TNode<Smi>{tmp31});
    ca_.Branch(tmp32, &block21, std::vector<Node*>{phi_bb23_7, phi_bb23_8, phi_bb23_10}, &block22, std::vector<Node*>{phi_bb23_7, phi_bb23_8, phi_bb23_10});
  }

  TNode<Smi> phi_bb21_7;
  TNode<Smi> phi_bb21_8;
  TNode<Number> phi_bb21_10;
  TNode<IntPtrT> tmp33;
  TNode<IntPtrT> tmp34;
  TNode<IntPtrT> tmp35;
  TNode<Smi> tmp36;
  TNode<IntPtrT> tmp37;
  TNode<Smi> tmp38;
  TNode<Smi> tmp39;
  TNode<IntPtrT> tmp40;
  TNode<UintPtrT> tmp41;
  TNode<UintPtrT> tmp42;
  TNode<BoolT> tmp43;
  if (block21.is_used()) {
    ca_.Bind(&block21, &phi_bb21_7, &phi_bb21_8, &phi_bb21_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 718);
    tmp33 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp34 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp35 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp36 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter2, tmp35});
    tmp37 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp36});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 718);
    tmp38 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{parameter4}, TNode<Smi>{parameter6});
    tmp39 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp38}, TNode<Smi>{phi_bb21_8});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp40 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp39});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp41 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp40});
    tmp42 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp37});
    tmp43 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp41}, TNode<UintPtrT>{tmp42});
    ca_.Branch(tmp43, &block28, std::vector<Node*>{phi_bb21_7, phi_bb21_8, phi_bb21_10}, &block29, std::vector<Node*>{phi_bb21_7, phi_bb21_8, phi_bb21_10});
  }

  TNode<Smi> phi_bb28_7;
  TNode<Smi> phi_bb28_8;
  TNode<Number> phi_bb28_10;
  TNode<IntPtrT> tmp44;
  TNode<IntPtrT> tmp45;
  TNode<IntPtrT> tmp46;
  TNode<HeapObject> tmp47;
  TNode<IntPtrT> tmp48;
  TNode<Object> tmp49;
  TNode<Object> tmp50;
  TNode<Number> tmp51;
  TNode<Number> tmp52;
  TNode<BoolT> tmp53;
  if (block28.is_used()) {
    ca_.Bind(&block28, &phi_bb28_7, &phi_bb28_8, &phi_bb28_10);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp44 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp45 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp40}, TNode<IntPtrT>{tmp44});
    tmp46 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp33}, TNode<IntPtrT>{tmp45});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp47, tmp48) = NewReference_Object_0(state_, TNode<HeapObject>{parameter2}, TNode<IntPtrT>{tmp46}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 718);
    tmp49 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp47, tmp48});
    tmp50 = UnsafeCast_JSAny_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp49});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 719);
    tmp51 = Method_SortState_Compare_0(state_, TNode<Context>{parameter0}, TNode<SortState>{parameter1}, TNode<Object>{tmp50}, TNode<Object>{parameter3});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 722);
    tmp52 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp53 = NumberIsGreaterThanOrEqual_0(state_, TNode<Number>{tmp51}, TNode<Number>{tmp52});
    ca_.Branch(tmp53, &block31, std::vector<Node*>{phi_bb28_7, phi_bb28_8}, &block32, std::vector<Node*>{phi_bb28_7, phi_bb28_8});
  }

  TNode<Smi> phi_bb29_7;
  TNode<Smi> phi_bb29_8;
  TNode<Number> phi_bb29_10;
  if (block29.is_used()) {
    ca_.Bind(&block29, &phi_bb29_7, &phi_bb29_8, &phi_bb29_10);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb31_7;
  TNode<Smi> phi_bb31_8;
  if (block31.is_used()) {
    ca_.Bind(&block31, &phi_bb31_7, &phi_bb31_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 722);
    ca_.Goto(&block22, phi_bb31_7, phi_bb31_8, tmp51);
  }

  TNode<Smi> phi_bb32_7;
  TNode<Smi> phi_bb32_8;
  TNode<Smi> tmp54;
  TNode<Smi> tmp55;
  TNode<Smi> tmp56;
  TNode<Smi> tmp57;
  TNode<BoolT> tmp58;
  if (block32.is_used()) {
    ca_.Bind(&block32, &phi_bb32_7, &phi_bb32_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 725);
    tmp54 = CodeStubAssembler(state_).SmiShl(TNode<Smi>{phi_bb32_8}, 1);
    tmp55 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp56 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp54}, TNode<Smi>{tmp55});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 728);
    tmp57 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp58 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{tmp56}, TNode<Smi>{tmp57});
    ca_.Branch(tmp58, &block33, std::vector<Node*>{phi_bb32_8}, &block34, std::vector<Node*>{phi_bb32_8, tmp56});
  }

  TNode<Smi> phi_bb33_7;
  if (block33.is_used()) {
    ca_.Bind(&block33, &phi_bb33_7);
    ca_.Goto(&block34, phi_bb33_7, tmp31);
  }

  TNode<Smi> phi_bb34_7;
  TNode<Smi> phi_bb34_8;
  if (block34.is_used()) {
    ca_.Bind(&block34, &phi_bb34_7, &phi_bb34_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 716);
    ca_.Goto(&block23, phi_bb34_7, phi_bb34_8, tmp51);
  }

  TNode<Smi> phi_bb22_7;
  TNode<Smi> phi_bb22_8;
  TNode<Number> phi_bb22_10;
  TNode<BoolT> tmp59;
  if (block22.is_used()) {
    ca_.Bind(&block22, &phi_bb22_7, &phi_bb22_8, &phi_bb22_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 731);
    tmp59 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{phi_bb22_8}, TNode<Smi>{tmp31});
    ca_.Branch(tmp59, &block35, std::vector<Node*>{phi_bb22_7, phi_bb22_8, phi_bb22_10}, &block36, std::vector<Node*>{phi_bb22_7, phi_bb22_8, phi_bb22_10});
  }

  TNode<Smi> phi_bb35_7;
  TNode<Smi> phi_bb35_8;
  TNode<Number> phi_bb35_10;
  if (block35.is_used()) {
    ca_.Bind(&block35, &phi_bb35_7, &phi_bb35_8, &phi_bb35_10);
    ca_.Goto(&block36, phi_bb35_7, tmp31, phi_bb35_10);
  }

  TNode<Smi> phi_bb36_7;
  TNode<Smi> phi_bb36_8;
  TNode<Number> phi_bb36_10;
  TNode<Smi> tmp60;
  TNode<Smi> tmp61;
  if (block36.is_used()) {
    ca_.Bind(&block36, &phi_bb36_7, &phi_bb36_8, &phi_bb36_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 734);
    tmp60 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb36_7}, TNode<Smi>{parameter6});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 735);
    tmp61 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb36_8}, TNode<Smi>{parameter6});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 710);
    ca_.Goto(&block20, tmp60, tmp61, phi_bb36_10);
  }

  TNode<Number> tmp62;
  TNode<BoolT> tmp63;
  if (block19.is_used()) {
    ca_.Bind(&block19);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 739);
    tmp62 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp63 = NumberIsGreaterThanOrEqual_0(state_, TNode<Number>{tmp28}, TNode<Number>{tmp62});
    ca_.Branch(tmp63, &block37, std::vector<Node*>{}, &block38, std::vector<Node*>{});
  }

  if (block38.is_used()) {
    ca_.Bind(&block38);
    CodeStubAssembler(state_).FailAssert("Torque assert 'order >= 0' failed", "third_party/v8/builtins/array-sort.tq", 739);
  }

  TNode<Smi> tmp64;
  TNode<Smi> tmp65;
  if (block37.is_used()) {
    ca_.Bind(&block37);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 742);
    tmp64 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp65 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{parameter6}, TNode<Smi>{tmp64});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 743);
    ca_.Goto(&block41, tmp9, tmp10, tmp28);
  }

  TNode<Smi> phi_bb41_7;
  TNode<Smi> phi_bb41_8;
  TNode<Number> phi_bb41_10;
  TNode<BoolT> tmp66;
  if (block41.is_used()) {
    ca_.Bind(&block41, &phi_bb41_7, &phi_bb41_8, &phi_bb41_10);
    tmp66 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{phi_bb41_8}, TNode<Smi>{tmp65});
    ca_.Branch(tmp66, &block39, std::vector<Node*>{phi_bb41_7, phi_bb41_8, phi_bb41_10}, &block40, std::vector<Node*>{phi_bb41_7, phi_bb41_8, phi_bb41_10});
  }

  TNode<Smi> phi_bb39_7;
  TNode<Smi> phi_bb39_8;
  TNode<Number> phi_bb39_10;
  TNode<IntPtrT> tmp67;
  TNode<IntPtrT> tmp68;
  TNode<IntPtrT> tmp69;
  TNode<Smi> tmp70;
  TNode<IntPtrT> tmp71;
  TNode<Smi> tmp72;
  TNode<Smi> tmp73;
  TNode<IntPtrT> tmp74;
  TNode<UintPtrT> tmp75;
  TNode<UintPtrT> tmp76;
  TNode<BoolT> tmp77;
  if (block39.is_used()) {
    ca_.Bind(&block39, &phi_bb39_7, &phi_bb39_8, &phi_bb39_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 745);
    tmp67 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp68 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp69 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp70 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter2, tmp69});
    tmp71 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp70});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 745);
    tmp72 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{parameter4}, TNode<Smi>{parameter6});
    tmp73 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp72}, TNode<Smi>{phi_bb39_8});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp74 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp73});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp75 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp74});
    tmp76 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp71});
    tmp77 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp75}, TNode<UintPtrT>{tmp76});
    ca_.Branch(tmp77, &block46, std::vector<Node*>{phi_bb39_7, phi_bb39_8, phi_bb39_10}, &block47, std::vector<Node*>{phi_bb39_7, phi_bb39_8, phi_bb39_10});
  }

  TNode<Smi> phi_bb46_7;
  TNode<Smi> phi_bb46_8;
  TNode<Number> phi_bb46_10;
  TNode<IntPtrT> tmp78;
  TNode<IntPtrT> tmp79;
  TNode<IntPtrT> tmp80;
  TNode<HeapObject> tmp81;
  TNode<IntPtrT> tmp82;
  TNode<Object> tmp83;
  TNode<Object> tmp84;
  TNode<Number> tmp85;
  TNode<Number> tmp86;
  TNode<BoolT> tmp87;
  if (block46.is_used()) {
    ca_.Bind(&block46, &phi_bb46_7, &phi_bb46_8, &phi_bb46_10);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp78 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp79 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp74}, TNode<IntPtrT>{tmp78});
    tmp80 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp67}, TNode<IntPtrT>{tmp79});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp81, tmp82) = NewReference_Object_0(state_, TNode<HeapObject>{parameter2}, TNode<IntPtrT>{tmp80}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 745);
    tmp83 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp81, tmp82});
    tmp84 = UnsafeCast_JSAny_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp83});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 746);
    tmp85 = Method_SortState_Compare_0(state_, TNode<Context>{parameter0}, TNode<SortState>{parameter1}, TNode<Object>{tmp84}, TNode<Object>{parameter3});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 748);
    tmp86 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp87 = NumberIsLessThan_0(state_, TNode<Number>{tmp85}, TNode<Number>{tmp86});
    ca_.Branch(tmp87, &block49, std::vector<Node*>{phi_bb46_7, phi_bb46_8}, &block50, std::vector<Node*>{phi_bb46_7, phi_bb46_8});
  }

  TNode<Smi> phi_bb47_7;
  TNode<Smi> phi_bb47_8;
  TNode<Number> phi_bb47_10;
  if (block47.is_used()) {
    ca_.Bind(&block47, &phi_bb47_7, &phi_bb47_8, &phi_bb47_10);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb49_7;
  TNode<Smi> phi_bb49_8;
  if (block49.is_used()) {
    ca_.Bind(&block49, &phi_bb49_7, &phi_bb49_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 748);
    ca_.Goto(&block40, phi_bb49_7, phi_bb49_8, tmp85);
  }

  TNode<Smi> phi_bb50_7;
  TNode<Smi> phi_bb50_8;
  TNode<Smi> tmp88;
  TNode<Smi> tmp89;
  TNode<Smi> tmp90;
  TNode<Smi> tmp91;
  TNode<BoolT> tmp92;
  if (block50.is_used()) {
    ca_.Bind(&block50, &phi_bb50_7, &phi_bb50_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 751);
    tmp88 = CodeStubAssembler(state_).SmiShl(TNode<Smi>{phi_bb50_8}, 1);
    tmp89 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp90 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp88}, TNode<Smi>{tmp89});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 754);
    tmp91 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp92 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{tmp90}, TNode<Smi>{tmp91});
    ca_.Branch(tmp92, &block51, std::vector<Node*>{phi_bb50_8}, &block52, std::vector<Node*>{phi_bb50_8, tmp90});
  }

  TNode<Smi> phi_bb51_7;
  if (block51.is_used()) {
    ca_.Bind(&block51, &phi_bb51_7);
    ca_.Goto(&block52, phi_bb51_7, tmp65);
  }

  TNode<Smi> phi_bb52_7;
  TNode<Smi> phi_bb52_8;
  if (block52.is_used()) {
    ca_.Bind(&block52, &phi_bb52_7, &phi_bb52_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 743);
    ca_.Goto(&block41, phi_bb52_7, phi_bb52_8, tmp85);
  }

  TNode<Smi> phi_bb40_7;
  TNode<Smi> phi_bb40_8;
  TNode<Number> phi_bb40_10;
  TNode<BoolT> tmp93;
  if (block40.is_used()) {
    ca_.Bind(&block40, &phi_bb40_7, &phi_bb40_8, &phi_bb40_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 757);
    tmp93 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{phi_bb40_8}, TNode<Smi>{tmp65});
    ca_.Branch(tmp93, &block53, std::vector<Node*>{phi_bb40_7, phi_bb40_8, phi_bb40_10}, &block54, std::vector<Node*>{phi_bb40_7, phi_bb40_8, phi_bb40_10});
  }

  TNode<Smi> phi_bb53_7;
  TNode<Smi> phi_bb53_8;
  TNode<Number> phi_bb53_10;
  if (block53.is_used()) {
    ca_.Bind(&block53, &phi_bb53_7, &phi_bb53_8, &phi_bb53_10);
    ca_.Goto(&block54, phi_bb53_7, tmp65, phi_bb53_10);
  }

  TNode<Smi> phi_bb54_7;
  TNode<Smi> phi_bb54_8;
  TNode<Number> phi_bb54_10;
  TNode<Smi> tmp94;
  TNode<Smi> tmp95;
  if (block54.is_used()) {
    ca_.Bind(&block54, &phi_bb54_7, &phi_bb54_8, &phi_bb54_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 761);
    tmp94 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{parameter6}, TNode<Smi>{phi_bb54_8});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 762);
    tmp95 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{parameter6}, TNode<Smi>{phi_bb54_7});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 710);
    ca_.Goto(&block20, tmp94, tmp95, phi_bb54_10);
  }

  TNode<Smi> phi_bb20_7;
  TNode<Smi> phi_bb20_8;
  TNode<Number> phi_bb20_10;
  TNode<Smi> tmp96;
  TNode<BoolT> tmp97;
  if (block20.is_used()) {
    ca_.Bind(&block20, &phi_bb20_7, &phi_bb20_8, &phi_bb20_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 765);
    tmp96 = FromConstexpr_Smi_constexpr_int31_0(state_, -1);
    tmp97 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{tmp96}, TNode<Smi>{phi_bb20_7});
    ca_.Branch(tmp97, &block57, std::vector<Node*>{phi_bb20_7, phi_bb20_8, phi_bb20_10}, &block58, std::vector<Node*>{phi_bb20_7, phi_bb20_8, phi_bb20_10});
  }

  TNode<Smi> phi_bb57_7;
  TNode<Smi> phi_bb57_8;
  TNode<Number> phi_bb57_10;
  TNode<BoolT> tmp98;
  if (block57.is_used()) {
    ca_.Bind(&block57, &phi_bb57_7, &phi_bb57_8, &phi_bb57_10);
    tmp98 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{phi_bb57_7}, TNode<Smi>{phi_bb57_8});
    ca_.Goto(&block59, phi_bb57_7, phi_bb57_8, phi_bb57_10, tmp98);
  }

  TNode<Smi> phi_bb58_7;
  TNode<Smi> phi_bb58_8;
  TNode<Number> phi_bb58_10;
  TNode<BoolT> tmp99;
  if (block58.is_used()) {
    ca_.Bind(&block58, &phi_bb58_7, &phi_bb58_8, &phi_bb58_10);
    tmp99 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block59, phi_bb58_7, phi_bb58_8, phi_bb58_10, tmp99);
  }

  TNode<Smi> phi_bb59_7;
  TNode<Smi> phi_bb59_8;
  TNode<Number> phi_bb59_10;
  TNode<BoolT> phi_bb59_12;
  if (block59.is_used()) {
    ca_.Bind(&block59, &phi_bb59_7, &phi_bb59_8, &phi_bb59_10, &phi_bb59_12);
    ca_.Branch(phi_bb59_12, &block60, std::vector<Node*>{phi_bb59_7, phi_bb59_8, phi_bb59_10}, &block61, std::vector<Node*>{phi_bb59_7, phi_bb59_8, phi_bb59_10});
  }

  TNode<Smi> phi_bb60_7;
  TNode<Smi> phi_bb60_8;
  TNode<Number> phi_bb60_10;
  TNode<BoolT> tmp100;
  if (block60.is_used()) {
    ca_.Bind(&block60, &phi_bb60_7, &phi_bb60_8, &phi_bb60_10);
    tmp100 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{phi_bb60_8}, TNode<Smi>{parameter5});
    ca_.Goto(&block62, phi_bb60_7, phi_bb60_8, phi_bb60_10, tmp100);
  }

  TNode<Smi> phi_bb61_7;
  TNode<Smi> phi_bb61_8;
  TNode<Number> phi_bb61_10;
  TNode<BoolT> tmp101;
  if (block61.is_used()) {
    ca_.Bind(&block61, &phi_bb61_7, &phi_bb61_8, &phi_bb61_10);
    tmp101 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block62, phi_bb61_7, phi_bb61_8, phi_bb61_10, tmp101);
  }

  TNode<Smi> phi_bb62_7;
  TNode<Smi> phi_bb62_8;
  TNode<Number> phi_bb62_10;
  TNode<BoolT> phi_bb62_12;
  if (block62.is_used()) {
    ca_.Bind(&block62, &phi_bb62_7, &phi_bb62_8, &phi_bb62_10, &phi_bb62_12);
    ca_.Branch(phi_bb62_12, &block55, std::vector<Node*>{phi_bb62_7, phi_bb62_8, phi_bb62_10}, &block56, std::vector<Node*>{phi_bb62_7, phi_bb62_8, phi_bb62_10});
  }

  TNode<Smi> phi_bb56_7;
  TNode<Smi> phi_bb56_8;
  TNode<Number> phi_bb56_10;
  if (block56.is_used()) {
    ca_.Bind(&block56, &phi_bb56_7, &phi_bb56_8, &phi_bb56_10);
    CodeStubAssembler(state_).FailAssert("Torque assert '-1 <= lastOfs && lastOfs < offset && offset <= length' failed", "third_party/v8/builtins/array-sort.tq", 765);
  }

  TNode<Smi> phi_bb55_7;
  TNode<Smi> phi_bb55_8;
  TNode<Number> phi_bb55_10;
  TNode<Smi> tmp102;
  TNode<Smi> tmp103;
  if (block55.is_used()) {
    ca_.Bind(&block55, &phi_bb55_7, &phi_bb55_8, &phi_bb55_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 771);
    tmp102 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp103 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb55_7}, TNode<Smi>{tmp102});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 772);
    ca_.Goto(&block65, tmp103, phi_bb55_8, phi_bb55_10);
  }

  TNode<Smi> phi_bb65_7;
  TNode<Smi> phi_bb65_8;
  TNode<Number> phi_bb65_10;
  TNode<BoolT> tmp104;
  if (block65.is_used()) {
    ca_.Bind(&block65, &phi_bb65_7, &phi_bb65_8, &phi_bb65_10);
    tmp104 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{phi_bb65_7}, TNode<Smi>{phi_bb65_8});
    ca_.Branch(tmp104, &block63, std::vector<Node*>{phi_bb65_7, phi_bb65_8, phi_bb65_10}, &block64, std::vector<Node*>{phi_bb65_7, phi_bb65_8, phi_bb65_10});
  }

  TNode<Smi> phi_bb63_7;
  TNode<Smi> phi_bb63_8;
  TNode<Number> phi_bb63_10;
  TNode<Smi> tmp105;
  TNode<Smi> tmp106;
  TNode<Smi> tmp107;
  TNode<IntPtrT> tmp108;
  TNode<IntPtrT> tmp109;
  TNode<IntPtrT> tmp110;
  TNode<Smi> tmp111;
  TNode<IntPtrT> tmp112;
  TNode<Smi> tmp113;
  TNode<IntPtrT> tmp114;
  TNode<UintPtrT> tmp115;
  TNode<UintPtrT> tmp116;
  TNode<BoolT> tmp117;
  if (block63.is_used()) {
    ca_.Bind(&block63, &phi_bb63_7, &phi_bb63_8, &phi_bb63_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 773);
    tmp105 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb63_8}, TNode<Smi>{phi_bb63_7});
    tmp106 = CodeStubAssembler(state_).SmiSar(TNode<Smi>{tmp105}, 1);
    tmp107 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb63_7}, TNode<Smi>{tmp106});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 775);
    tmp108 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp109 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp110 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp111 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter2, tmp110});
    tmp112 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp111});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 775);
    tmp113 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{parameter4}, TNode<Smi>{tmp107});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp114 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp113});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp115 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp114});
    tmp116 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp112});
    tmp117 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp115}, TNode<UintPtrT>{tmp116});
    ca_.Branch(tmp117, &block70, std::vector<Node*>{phi_bb63_7, phi_bb63_8, phi_bb63_10}, &block71, std::vector<Node*>{phi_bb63_7, phi_bb63_8, phi_bb63_10});
  }

  TNode<Smi> phi_bb70_7;
  TNode<Smi> phi_bb70_8;
  TNode<Number> phi_bb70_10;
  TNode<IntPtrT> tmp118;
  TNode<IntPtrT> tmp119;
  TNode<IntPtrT> tmp120;
  TNode<HeapObject> tmp121;
  TNode<IntPtrT> tmp122;
  TNode<Object> tmp123;
  TNode<Object> tmp124;
  TNode<Number> tmp125;
  TNode<Number> tmp126;
  TNode<BoolT> tmp127;
  if (block70.is_used()) {
    ca_.Bind(&block70, &phi_bb70_7, &phi_bb70_8, &phi_bb70_10);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp118 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp119 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp114}, TNode<IntPtrT>{tmp118});
    tmp120 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp108}, TNode<IntPtrT>{tmp119});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp121, tmp122) = NewReference_Object_0(state_, TNode<HeapObject>{parameter2}, TNode<IntPtrT>{tmp120}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 775);
    tmp123 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp121, tmp122});
    tmp124 = UnsafeCast_JSAny_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp123});
    tmp125 = Method_SortState_Compare_0(state_, TNode<Context>{parameter0}, TNode<SortState>{parameter1}, TNode<Object>{tmp124}, TNode<Object>{parameter3});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 777);
    tmp126 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp127 = NumberIsLessThan_0(state_, TNode<Number>{tmp125}, TNode<Number>{tmp126});
    ca_.Branch(tmp127, &block73, std::vector<Node*>{phi_bb70_7, phi_bb70_8}, &block74, std::vector<Node*>{phi_bb70_7, phi_bb70_8});
  }

  TNode<Smi> phi_bb71_7;
  TNode<Smi> phi_bb71_8;
  TNode<Number> phi_bb71_10;
  if (block71.is_used()) {
    ca_.Bind(&block71, &phi_bb71_7, &phi_bb71_8, &phi_bb71_10);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb73_7;
  TNode<Smi> phi_bb73_8;
  TNode<Smi> tmp128;
  TNode<Smi> tmp129;
  if (block73.is_used()) {
    ca_.Bind(&block73, &phi_bb73_7, &phi_bb73_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 778);
    tmp128 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp129 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp107}, TNode<Smi>{tmp128});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 777);
    ca_.Goto(&block75, tmp129, phi_bb73_8);
  }

  TNode<Smi> phi_bb74_7;
  TNode<Smi> phi_bb74_8;
  if (block74.is_used()) {
    ca_.Bind(&block74, &phi_bb74_7, &phi_bb74_8);
    ca_.Goto(&block75, phi_bb74_7, tmp107);
  }

  TNode<Smi> phi_bb75_7;
  TNode<Smi> phi_bb75_8;
  if (block75.is_used()) {
    ca_.Bind(&block75, &phi_bb75_7, &phi_bb75_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 772);
    ca_.Goto(&block65, phi_bb75_7, phi_bb75_8, tmp125);
  }

  TNode<Smi> phi_bb64_7;
  TNode<Smi> phi_bb64_8;
  TNode<Number> phi_bb64_10;
  TNode<BoolT> tmp130;
  if (block64.is_used()) {
    ca_.Bind(&block64, &phi_bb64_7, &phi_bb64_8, &phi_bb64_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 784);
    tmp130 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{phi_bb64_7}, TNode<Smi>{phi_bb64_8});
    ca_.Branch(tmp130, &block76, std::vector<Node*>{phi_bb64_7, phi_bb64_8, phi_bb64_10}, &block77, std::vector<Node*>{phi_bb64_7, phi_bb64_8, phi_bb64_10});
  }

  TNode<Smi> phi_bb77_7;
  TNode<Smi> phi_bb77_8;
  TNode<Number> phi_bb77_10;
  if (block77.is_used()) {
    ca_.Bind(&block77, &phi_bb77_7, &phi_bb77_8, &phi_bb77_10);
    CodeStubAssembler(state_).FailAssert("Torque assert 'lastOfs == offset' failed", "third_party/v8/builtins/array-sort.tq", 784);
  }

  TNode<Smi> phi_bb76_7;
  TNode<Smi> phi_bb76_8;
  TNode<Number> phi_bb76_10;
  TNode<Smi> tmp131;
  TNode<BoolT> tmp132;
  if (block76.is_used()) {
    ca_.Bind(&block76, &phi_bb76_7, &phi_bb76_8, &phi_bb76_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 785);
    tmp131 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp132 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{tmp131}, TNode<Smi>{phi_bb76_8});
    ca_.Branch(tmp132, &block80, std::vector<Node*>{phi_bb76_7, phi_bb76_8, phi_bb76_10}, &block81, std::vector<Node*>{phi_bb76_7, phi_bb76_8, phi_bb76_10});
  }

  TNode<Smi> phi_bb80_7;
  TNode<Smi> phi_bb80_8;
  TNode<Number> phi_bb80_10;
  TNode<BoolT> tmp133;
  if (block80.is_used()) {
    ca_.Bind(&block80, &phi_bb80_7, &phi_bb80_8, &phi_bb80_10);
    tmp133 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{phi_bb80_8}, TNode<Smi>{parameter5});
    ca_.Goto(&block82, phi_bb80_7, phi_bb80_8, phi_bb80_10, tmp133);
  }

  TNode<Smi> phi_bb81_7;
  TNode<Smi> phi_bb81_8;
  TNode<Number> phi_bb81_10;
  TNode<BoolT> tmp134;
  if (block81.is_used()) {
    ca_.Bind(&block81, &phi_bb81_7, &phi_bb81_8, &phi_bb81_10);
    tmp134 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block82, phi_bb81_7, phi_bb81_8, phi_bb81_10, tmp134);
  }

  TNode<Smi> phi_bb82_7;
  TNode<Smi> phi_bb82_8;
  TNode<Number> phi_bb82_10;
  TNode<BoolT> phi_bb82_12;
  if (block82.is_used()) {
    ca_.Bind(&block82, &phi_bb82_7, &phi_bb82_8, &phi_bb82_10, &phi_bb82_12);
    ca_.Branch(phi_bb82_12, &block78, std::vector<Node*>{phi_bb82_7, phi_bb82_8, phi_bb82_10}, &block79, std::vector<Node*>{phi_bb82_7, phi_bb82_8, phi_bb82_10});
  }

  TNode<Smi> phi_bb79_7;
  TNode<Smi> phi_bb79_8;
  TNode<Number> phi_bb79_10;
  if (block79.is_used()) {
    ca_.Bind(&block79, &phi_bb79_7, &phi_bb79_8, &phi_bb79_10);
    CodeStubAssembler(state_).FailAssert("Torque assert '0 <= offset && offset <= length' failed", "third_party/v8/builtins/array-sort.tq", 785);
  }

  TNode<Smi> phi_bb78_7;
  TNode<Smi> phi_bb78_8;
  TNode<Number> phi_bb78_10;
  if (block78.is_used()) {
    ca_.Bind(&block78, &phi_bb78_7, &phi_bb78_8, &phi_bb78_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 786);
    CodeStubAssembler(state_).Return(phi_bb78_8);
  }
}

TF_BUILTIN(GallopRight, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<SortState> parameter1 = UncheckedCast<SortState>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<FixedArray> parameter2 = UncheckedCast<FixedArray>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  TNode<Object> parameter3 = UncheckedCast<Object>(Parameter(Descriptor::ParameterIndex<2>()));
  USE(parameter3);
  TNode<Smi> parameter4 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<3>()));
  USE(parameter4);
  TNode<Smi> parameter5 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<4>()));
  USE(parameter5);
  TNode<Smi> parameter6 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<5>()));
  USE(parameter6);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block15(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block16(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block18(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block23(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block21(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block28(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block29(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block31(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block32(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block33(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block34(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block22(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block35(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block36(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block19(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block39(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block37(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block44(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block45(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block47(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block48(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block49(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block50(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block38(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block51(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block52(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block20(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Number> block55(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Number> block56(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Number, BoolT> block57(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Number> block58(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Number> block59(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Number, BoolT> block60(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Number> block54(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Number> block53(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block63(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block61(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block68(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block69(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block71(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block72(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block73(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block62(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block75(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block74(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block78(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block79(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number, BoolT> block80(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block77(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Number> block76(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Smi> tmp0;
  TNode<BoolT> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 800);
    tmp0 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp1 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{parameter5}, TNode<Smi>{tmp0});
    ca_.Branch(tmp1, &block3, std::vector<Node*>{}, &block4, std::vector<Node*>{});
  }

  TNode<Smi> tmp2;
  TNode<BoolT> tmp3;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    tmp2 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp3 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{parameter4}, TNode<Smi>{tmp2});
    ca_.Goto(&block5, tmp3);
  }

  TNode<BoolT> tmp4;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    tmp4 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block5, tmp4);
  }

  TNode<BoolT> phi_bb5_8;
  if (block5.is_used()) {
    ca_.Bind(&block5, &phi_bb5_8);
    ca_.Branch(phi_bb5_8, &block1, std::vector<Node*>{}, &block2, std::vector<Node*>{});
  }

  if (block2.is_used()) {
    ca_.Bind(&block2);
    CodeStubAssembler(state_).FailAssert("Torque assert 'length > 0 && base >= 0' failed", "third_party/v8/builtins/array-sort.tq", 800);
  }

  TNode<Smi> tmp5;
  TNode<BoolT> tmp6;
  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 801);
    tmp5 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp6 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{tmp5}, TNode<Smi>{parameter6});
    ca_.Branch(tmp6, &block8, std::vector<Node*>{}, &block9, std::vector<Node*>{});
  }

  TNode<BoolT> tmp7;
  if (block8.is_used()) {
    ca_.Bind(&block8);
    tmp7 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{parameter6}, TNode<Smi>{parameter5});
    ca_.Goto(&block10, tmp7);
  }

  TNode<BoolT> tmp8;
  if (block9.is_used()) {
    ca_.Bind(&block9);
    tmp8 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block10, tmp8);
  }

  TNode<BoolT> phi_bb10_8;
  if (block10.is_used()) {
    ca_.Bind(&block10, &phi_bb10_8);
    ca_.Branch(phi_bb10_8, &block6, std::vector<Node*>{}, &block7, std::vector<Node*>{});
  }

  if (block7.is_used()) {
    ca_.Bind(&block7);
    CodeStubAssembler(state_).FailAssert("Torque assert '0 <= hint && hint < length' failed", "third_party/v8/builtins/array-sort.tq", 801);
  }

  TNode<Smi> tmp9;
  TNode<Smi> tmp10;
  TNode<IntPtrT> tmp11;
  TNode<IntPtrT> tmp12;
  TNode<IntPtrT> tmp13;
  TNode<Smi> tmp14;
  TNode<IntPtrT> tmp15;
  TNode<Smi> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<UintPtrT> tmp18;
  TNode<UintPtrT> tmp19;
  TNode<BoolT> tmp20;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 803);
    tmp9 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 804);
    tmp10 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 806);
    tmp11 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp12 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp13 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp14 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter2, tmp13});
    tmp15 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp14});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 806);
    tmp16 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{parameter4}, TNode<Smi>{parameter6});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp17 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp16});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp18 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp17});
    tmp19 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp15});
    tmp20 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp18}, TNode<UintPtrT>{tmp19});
    ca_.Branch(tmp20, &block15, std::vector<Node*>{}, &block16, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp21;
  TNode<IntPtrT> tmp22;
  TNode<IntPtrT> tmp23;
  TNode<HeapObject> tmp24;
  TNode<IntPtrT> tmp25;
  TNode<Object> tmp26;
  TNode<Object> tmp27;
  TNode<Number> tmp28;
  TNode<Number> tmp29;
  TNode<BoolT> tmp30;
  if (block15.is_used()) {
    ca_.Bind(&block15);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp21 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp22 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp17}, TNode<IntPtrT>{tmp21});
    tmp23 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp11}, TNode<IntPtrT>{tmp22});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp24, tmp25) = NewReference_Object_0(state_, TNode<HeapObject>{parameter2}, TNode<IntPtrT>{tmp23}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 806);
    tmp26 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp24, tmp25});
    tmp27 = UnsafeCast_JSAny_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp26});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 807);
    tmp28 = Method_SortState_Compare_0(state_, TNode<Context>{parameter0}, TNode<SortState>{parameter1}, TNode<Object>{parameter3}, TNode<Object>{tmp27});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 809);
    tmp29 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp30 = NumberIsLessThan_0(state_, TNode<Number>{tmp28}, TNode<Number>{tmp29});
    ca_.Branch(tmp30, &block18, std::vector<Node*>{}, &block19, std::vector<Node*>{});
  }

  if (block16.is_used()) {
    ca_.Bind(&block16);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> tmp31;
  TNode<Smi> tmp32;
  if (block18.is_used()) {
    ca_.Bind(&block18);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 814);
    tmp31 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp32 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{parameter6}, TNode<Smi>{tmp31});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 815);
    ca_.Goto(&block23, tmp9, tmp10, tmp28);
  }

  TNode<Smi> phi_bb23_7;
  TNode<Smi> phi_bb23_8;
  TNode<Number> phi_bb23_10;
  TNode<BoolT> tmp33;
  if (block23.is_used()) {
    ca_.Bind(&block23, &phi_bb23_7, &phi_bb23_8, &phi_bb23_10);
    tmp33 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{phi_bb23_8}, TNode<Smi>{tmp32});
    ca_.Branch(tmp33, &block21, std::vector<Node*>{phi_bb23_7, phi_bb23_8, phi_bb23_10}, &block22, std::vector<Node*>{phi_bb23_7, phi_bb23_8, phi_bb23_10});
  }

  TNode<Smi> phi_bb21_7;
  TNode<Smi> phi_bb21_8;
  TNode<Number> phi_bb21_10;
  TNode<IntPtrT> tmp34;
  TNode<IntPtrT> tmp35;
  TNode<IntPtrT> tmp36;
  TNode<Smi> tmp37;
  TNode<IntPtrT> tmp38;
  TNode<Smi> tmp39;
  TNode<Smi> tmp40;
  TNode<IntPtrT> tmp41;
  TNode<UintPtrT> tmp42;
  TNode<UintPtrT> tmp43;
  TNode<BoolT> tmp44;
  if (block21.is_used()) {
    ca_.Bind(&block21, &phi_bb21_7, &phi_bb21_8, &phi_bb21_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 817);
    tmp34 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp35 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp36 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp37 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter2, tmp36});
    tmp38 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp37});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 817);
    tmp39 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{parameter4}, TNode<Smi>{parameter6});
    tmp40 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp39}, TNode<Smi>{phi_bb21_8});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp41 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp40});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp42 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp41});
    tmp43 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp38});
    tmp44 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp42}, TNode<UintPtrT>{tmp43});
    ca_.Branch(tmp44, &block28, std::vector<Node*>{phi_bb21_7, phi_bb21_8, phi_bb21_10}, &block29, std::vector<Node*>{phi_bb21_7, phi_bb21_8, phi_bb21_10});
  }

  TNode<Smi> phi_bb28_7;
  TNode<Smi> phi_bb28_8;
  TNode<Number> phi_bb28_10;
  TNode<IntPtrT> tmp45;
  TNode<IntPtrT> tmp46;
  TNode<IntPtrT> tmp47;
  TNode<HeapObject> tmp48;
  TNode<IntPtrT> tmp49;
  TNode<Object> tmp50;
  TNode<Object> tmp51;
  TNode<Number> tmp52;
  TNode<Number> tmp53;
  TNode<BoolT> tmp54;
  if (block28.is_used()) {
    ca_.Bind(&block28, &phi_bb28_7, &phi_bb28_8, &phi_bb28_10);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp45 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp46 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp41}, TNode<IntPtrT>{tmp45});
    tmp47 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp34}, TNode<IntPtrT>{tmp46});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp48, tmp49) = NewReference_Object_0(state_, TNode<HeapObject>{parameter2}, TNode<IntPtrT>{tmp47}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 817);
    tmp50 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp48, tmp49});
    tmp51 = UnsafeCast_JSAny_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp50});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 818);
    tmp52 = Method_SortState_Compare_0(state_, TNode<Context>{parameter0}, TNode<SortState>{parameter1}, TNode<Object>{parameter3}, TNode<Object>{tmp51});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 820);
    tmp53 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp54 = NumberIsGreaterThanOrEqual_0(state_, TNode<Number>{tmp52}, TNode<Number>{tmp53});
    ca_.Branch(tmp54, &block31, std::vector<Node*>{phi_bb28_7, phi_bb28_8}, &block32, std::vector<Node*>{phi_bb28_7, phi_bb28_8});
  }

  TNode<Smi> phi_bb29_7;
  TNode<Smi> phi_bb29_8;
  TNode<Number> phi_bb29_10;
  if (block29.is_used()) {
    ca_.Bind(&block29, &phi_bb29_7, &phi_bb29_8, &phi_bb29_10);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb31_7;
  TNode<Smi> phi_bb31_8;
  if (block31.is_used()) {
    ca_.Bind(&block31, &phi_bb31_7, &phi_bb31_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 820);
    ca_.Goto(&block22, phi_bb31_7, phi_bb31_8, tmp52);
  }

  TNode<Smi> phi_bb32_7;
  TNode<Smi> phi_bb32_8;
  TNode<Smi> tmp55;
  TNode<Smi> tmp56;
  TNode<Smi> tmp57;
  TNode<Smi> tmp58;
  TNode<BoolT> tmp59;
  if (block32.is_used()) {
    ca_.Bind(&block32, &phi_bb32_7, &phi_bb32_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 823);
    tmp55 = CodeStubAssembler(state_).SmiShl(TNode<Smi>{phi_bb32_8}, 1);
    tmp56 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp57 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp55}, TNode<Smi>{tmp56});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 826);
    tmp58 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp59 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{tmp57}, TNode<Smi>{tmp58});
    ca_.Branch(tmp59, &block33, std::vector<Node*>{phi_bb32_8}, &block34, std::vector<Node*>{phi_bb32_8, tmp57});
  }

  TNode<Smi> phi_bb33_7;
  if (block33.is_used()) {
    ca_.Bind(&block33, &phi_bb33_7);
    ca_.Goto(&block34, phi_bb33_7, tmp32);
  }

  TNode<Smi> phi_bb34_7;
  TNode<Smi> phi_bb34_8;
  if (block34.is_used()) {
    ca_.Bind(&block34, &phi_bb34_7, &phi_bb34_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 815);
    ca_.Goto(&block23, phi_bb34_7, phi_bb34_8, tmp52);
  }

  TNode<Smi> phi_bb22_7;
  TNode<Smi> phi_bb22_8;
  TNode<Number> phi_bb22_10;
  TNode<BoolT> tmp60;
  if (block22.is_used()) {
    ca_.Bind(&block22, &phi_bb22_7, &phi_bb22_8, &phi_bb22_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 829);
    tmp60 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{phi_bb22_8}, TNode<Smi>{tmp32});
    ca_.Branch(tmp60, &block35, std::vector<Node*>{phi_bb22_7, phi_bb22_8, phi_bb22_10}, &block36, std::vector<Node*>{phi_bb22_7, phi_bb22_8, phi_bb22_10});
  }

  TNode<Smi> phi_bb35_7;
  TNode<Smi> phi_bb35_8;
  TNode<Number> phi_bb35_10;
  if (block35.is_used()) {
    ca_.Bind(&block35, &phi_bb35_7, &phi_bb35_8, &phi_bb35_10);
    ca_.Goto(&block36, phi_bb35_7, tmp32, phi_bb35_10);
  }

  TNode<Smi> phi_bb36_7;
  TNode<Smi> phi_bb36_8;
  TNode<Number> phi_bb36_10;
  TNode<Smi> tmp61;
  TNode<Smi> tmp62;
  if (block36.is_used()) {
    ca_.Bind(&block36, &phi_bb36_7, &phi_bb36_8, &phi_bb36_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 833);
    tmp61 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{parameter6}, TNode<Smi>{phi_bb36_8});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 834);
    tmp62 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{parameter6}, TNode<Smi>{phi_bb36_7});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 809);
    ca_.Goto(&block20, tmp61, tmp62, phi_bb36_10);
  }

  TNode<Smi> tmp63;
  if (block19.is_used()) {
    ca_.Bind(&block19);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 840);
    tmp63 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{parameter5}, TNode<Smi>{parameter6});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 841);
    ca_.Goto(&block39, tmp9, tmp10, tmp28);
  }

  TNode<Smi> phi_bb39_7;
  TNode<Smi> phi_bb39_8;
  TNode<Number> phi_bb39_10;
  TNode<BoolT> tmp64;
  if (block39.is_used()) {
    ca_.Bind(&block39, &phi_bb39_7, &phi_bb39_8, &phi_bb39_10);
    tmp64 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{phi_bb39_8}, TNode<Smi>{tmp63});
    ca_.Branch(tmp64, &block37, std::vector<Node*>{phi_bb39_7, phi_bb39_8, phi_bb39_10}, &block38, std::vector<Node*>{phi_bb39_7, phi_bb39_8, phi_bb39_10});
  }

  TNode<Smi> phi_bb37_7;
  TNode<Smi> phi_bb37_8;
  TNode<Number> phi_bb37_10;
  TNode<IntPtrT> tmp65;
  TNode<IntPtrT> tmp66;
  TNode<IntPtrT> tmp67;
  TNode<Smi> tmp68;
  TNode<IntPtrT> tmp69;
  TNode<Smi> tmp70;
  TNode<Smi> tmp71;
  TNode<IntPtrT> tmp72;
  TNode<UintPtrT> tmp73;
  TNode<UintPtrT> tmp74;
  TNode<BoolT> tmp75;
  if (block37.is_used()) {
    ca_.Bind(&block37, &phi_bb37_7, &phi_bb37_8, &phi_bb37_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 843);
    tmp65 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp66 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp67 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp68 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter2, tmp67});
    tmp69 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp68});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 843);
    tmp70 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{parameter4}, TNode<Smi>{parameter6});
    tmp71 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp70}, TNode<Smi>{phi_bb37_8});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp72 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp71});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp73 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp72});
    tmp74 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp69});
    tmp75 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp73}, TNode<UintPtrT>{tmp74});
    ca_.Branch(tmp75, &block44, std::vector<Node*>{phi_bb37_7, phi_bb37_8, phi_bb37_10}, &block45, std::vector<Node*>{phi_bb37_7, phi_bb37_8, phi_bb37_10});
  }

  TNode<Smi> phi_bb44_7;
  TNode<Smi> phi_bb44_8;
  TNode<Number> phi_bb44_10;
  TNode<IntPtrT> tmp76;
  TNode<IntPtrT> tmp77;
  TNode<IntPtrT> tmp78;
  TNode<HeapObject> tmp79;
  TNode<IntPtrT> tmp80;
  TNode<Object> tmp81;
  TNode<Object> tmp82;
  TNode<Number> tmp83;
  TNode<Number> tmp84;
  TNode<BoolT> tmp85;
  if (block44.is_used()) {
    ca_.Bind(&block44, &phi_bb44_7, &phi_bb44_8, &phi_bb44_10);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp76 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp77 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp72}, TNode<IntPtrT>{tmp76});
    tmp78 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp65}, TNode<IntPtrT>{tmp77});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp79, tmp80) = NewReference_Object_0(state_, TNode<HeapObject>{parameter2}, TNode<IntPtrT>{tmp78}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 843);
    tmp81 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp79, tmp80});
    tmp82 = UnsafeCast_JSAny_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp81});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 844);
    tmp83 = Method_SortState_Compare_0(state_, TNode<Context>{parameter0}, TNode<SortState>{parameter1}, TNode<Object>{parameter3}, TNode<Object>{tmp82});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 847);
    tmp84 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp85 = NumberIsLessThan_0(state_, TNode<Number>{tmp83}, TNode<Number>{tmp84});
    ca_.Branch(tmp85, &block47, std::vector<Node*>{phi_bb44_7, phi_bb44_8}, &block48, std::vector<Node*>{phi_bb44_7, phi_bb44_8});
  }

  TNode<Smi> phi_bb45_7;
  TNode<Smi> phi_bb45_8;
  TNode<Number> phi_bb45_10;
  if (block45.is_used()) {
    ca_.Bind(&block45, &phi_bb45_7, &phi_bb45_8, &phi_bb45_10);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb47_7;
  TNode<Smi> phi_bb47_8;
  if (block47.is_used()) {
    ca_.Bind(&block47, &phi_bb47_7, &phi_bb47_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 847);
    ca_.Goto(&block38, phi_bb47_7, phi_bb47_8, tmp83);
  }

  TNode<Smi> phi_bb48_7;
  TNode<Smi> phi_bb48_8;
  TNode<Smi> tmp86;
  TNode<Smi> tmp87;
  TNode<Smi> tmp88;
  TNode<Smi> tmp89;
  TNode<BoolT> tmp90;
  if (block48.is_used()) {
    ca_.Bind(&block48, &phi_bb48_7, &phi_bb48_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 850);
    tmp86 = CodeStubAssembler(state_).SmiShl(TNode<Smi>{phi_bb48_8}, 1);
    tmp87 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp88 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp86}, TNode<Smi>{tmp87});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 853);
    tmp89 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp90 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{tmp88}, TNode<Smi>{tmp89});
    ca_.Branch(tmp90, &block49, std::vector<Node*>{phi_bb48_8}, &block50, std::vector<Node*>{phi_bb48_8, tmp88});
  }

  TNode<Smi> phi_bb49_7;
  if (block49.is_used()) {
    ca_.Bind(&block49, &phi_bb49_7);
    ca_.Goto(&block50, phi_bb49_7, tmp63);
  }

  TNode<Smi> phi_bb50_7;
  TNode<Smi> phi_bb50_8;
  if (block50.is_used()) {
    ca_.Bind(&block50, &phi_bb50_7, &phi_bb50_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 841);
    ca_.Goto(&block39, phi_bb50_7, phi_bb50_8, tmp83);
  }

  TNode<Smi> phi_bb38_7;
  TNode<Smi> phi_bb38_8;
  TNode<Number> phi_bb38_10;
  TNode<BoolT> tmp91;
  if (block38.is_used()) {
    ca_.Bind(&block38, &phi_bb38_7, &phi_bb38_8, &phi_bb38_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 856);
    tmp91 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{phi_bb38_8}, TNode<Smi>{tmp63});
    ca_.Branch(tmp91, &block51, std::vector<Node*>{phi_bb38_7, phi_bb38_8, phi_bb38_10}, &block52, std::vector<Node*>{phi_bb38_7, phi_bb38_8, phi_bb38_10});
  }

  TNode<Smi> phi_bb51_7;
  TNode<Smi> phi_bb51_8;
  TNode<Number> phi_bb51_10;
  if (block51.is_used()) {
    ca_.Bind(&block51, &phi_bb51_7, &phi_bb51_8, &phi_bb51_10);
    ca_.Goto(&block52, phi_bb51_7, tmp63, phi_bb51_10);
  }

  TNode<Smi> phi_bb52_7;
  TNode<Smi> phi_bb52_8;
  TNode<Number> phi_bb52_10;
  TNode<Smi> tmp92;
  TNode<Smi> tmp93;
  if (block52.is_used()) {
    ca_.Bind(&block52, &phi_bb52_7, &phi_bb52_8, &phi_bb52_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 859);
    tmp92 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb52_7}, TNode<Smi>{parameter6});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 860);
    tmp93 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb52_8}, TNode<Smi>{parameter6});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 809);
    ca_.Goto(&block20, tmp92, tmp93, phi_bb52_10);
  }

  TNode<Smi> phi_bb20_7;
  TNode<Smi> phi_bb20_8;
  TNode<Number> phi_bb20_10;
  TNode<Smi> tmp94;
  TNode<BoolT> tmp95;
  if (block20.is_used()) {
    ca_.Bind(&block20, &phi_bb20_7, &phi_bb20_8, &phi_bb20_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 862);
    tmp94 = FromConstexpr_Smi_constexpr_int31_0(state_, -1);
    tmp95 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{tmp94}, TNode<Smi>{phi_bb20_7});
    ca_.Branch(tmp95, &block55, std::vector<Node*>{phi_bb20_10}, &block56, std::vector<Node*>{phi_bb20_10});
  }

  TNode<Number> phi_bb55_10;
  TNode<BoolT> tmp96;
  if (block55.is_used()) {
    ca_.Bind(&block55, &phi_bb55_10);
    tmp96 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{phi_bb20_7}, TNode<Smi>{phi_bb20_8});
    ca_.Goto(&block57, phi_bb55_10, tmp96);
  }

  TNode<Number> phi_bb56_10;
  TNode<BoolT> tmp97;
  if (block56.is_used()) {
    ca_.Bind(&block56, &phi_bb56_10);
    tmp97 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block57, phi_bb56_10, tmp97);
  }

  TNode<Number> phi_bb57_10;
  TNode<BoolT> phi_bb57_12;
  if (block57.is_used()) {
    ca_.Bind(&block57, &phi_bb57_10, &phi_bb57_12);
    ca_.Branch(phi_bb57_12, &block58, std::vector<Node*>{phi_bb57_10}, &block59, std::vector<Node*>{phi_bb57_10});
  }

  TNode<Number> phi_bb58_10;
  TNode<BoolT> tmp98;
  if (block58.is_used()) {
    ca_.Bind(&block58, &phi_bb58_10);
    tmp98 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{phi_bb20_8}, TNode<Smi>{parameter5});
    ca_.Goto(&block60, phi_bb58_10, tmp98);
  }

  TNode<Number> phi_bb59_10;
  TNode<BoolT> tmp99;
  if (block59.is_used()) {
    ca_.Bind(&block59, &phi_bb59_10);
    tmp99 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block60, phi_bb59_10, tmp99);
  }

  TNode<Number> phi_bb60_10;
  TNode<BoolT> phi_bb60_12;
  if (block60.is_used()) {
    ca_.Bind(&block60, &phi_bb60_10, &phi_bb60_12);
    ca_.Branch(phi_bb60_12, &block53, std::vector<Node*>{phi_bb60_10}, &block54, std::vector<Node*>{phi_bb60_10});
  }

  TNode<Number> phi_bb54_10;
  if (block54.is_used()) {
    ca_.Bind(&block54, &phi_bb54_10);
    CodeStubAssembler(state_).FailAssert("Torque assert '-1 <= lastOfs && lastOfs < offset && offset <= length' failed", "third_party/v8/builtins/array-sort.tq", 862);
  }

  TNode<Number> phi_bb53_10;
  TNode<Smi> tmp100;
  TNode<Smi> tmp101;
  if (block53.is_used()) {
    ca_.Bind(&block53, &phi_bb53_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 868);
    tmp100 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp101 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb20_7}, TNode<Smi>{tmp100});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 869);
    ca_.Goto(&block63, tmp101, phi_bb20_8, phi_bb53_10);
  }

  TNode<Smi> phi_bb63_7;
  TNode<Smi> phi_bb63_8;
  TNode<Number> phi_bb63_10;
  TNode<BoolT> tmp102;
  if (block63.is_used()) {
    ca_.Bind(&block63, &phi_bb63_7, &phi_bb63_8, &phi_bb63_10);
    tmp102 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{phi_bb63_7}, TNode<Smi>{phi_bb63_8});
    ca_.Branch(tmp102, &block61, std::vector<Node*>{phi_bb63_7, phi_bb63_8, phi_bb63_10}, &block62, std::vector<Node*>{phi_bb63_7, phi_bb63_8, phi_bb63_10});
  }

  TNode<Smi> phi_bb61_7;
  TNode<Smi> phi_bb61_8;
  TNode<Number> phi_bb61_10;
  TNode<Smi> tmp103;
  TNode<Smi> tmp104;
  TNode<Smi> tmp105;
  TNode<IntPtrT> tmp106;
  TNode<IntPtrT> tmp107;
  TNode<IntPtrT> tmp108;
  TNode<Smi> tmp109;
  TNode<IntPtrT> tmp110;
  TNode<Smi> tmp111;
  TNode<IntPtrT> tmp112;
  TNode<UintPtrT> tmp113;
  TNode<UintPtrT> tmp114;
  TNode<BoolT> tmp115;
  if (block61.is_used()) {
    ca_.Bind(&block61, &phi_bb61_7, &phi_bb61_8, &phi_bb61_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 870);
    tmp103 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb61_8}, TNode<Smi>{phi_bb61_7});
    tmp104 = CodeStubAssembler(state_).SmiSar(TNode<Smi>{tmp103}, 1);
    tmp105 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb61_7}, TNode<Smi>{tmp104});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 872);
    tmp106 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp107 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp108 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp109 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{parameter2, tmp108});
    tmp110 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp109});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 872);
    tmp111 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{parameter4}, TNode<Smi>{tmp105});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp112 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp111});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp113 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp112});
    tmp114 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp110});
    tmp115 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp113}, TNode<UintPtrT>{tmp114});
    ca_.Branch(tmp115, &block68, std::vector<Node*>{phi_bb61_7, phi_bb61_8, phi_bb61_10}, &block69, std::vector<Node*>{phi_bb61_7, phi_bb61_8, phi_bb61_10});
  }

  TNode<Smi> phi_bb68_7;
  TNode<Smi> phi_bb68_8;
  TNode<Number> phi_bb68_10;
  TNode<IntPtrT> tmp116;
  TNode<IntPtrT> tmp117;
  TNode<IntPtrT> tmp118;
  TNode<HeapObject> tmp119;
  TNode<IntPtrT> tmp120;
  TNode<Object> tmp121;
  TNode<Object> tmp122;
  TNode<Number> tmp123;
  TNode<Number> tmp124;
  TNode<BoolT> tmp125;
  if (block68.is_used()) {
    ca_.Bind(&block68, &phi_bb68_7, &phi_bb68_8, &phi_bb68_10);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp116 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp117 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp112}, TNode<IntPtrT>{tmp116});
    tmp118 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp106}, TNode<IntPtrT>{tmp117});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp119, tmp120) = NewReference_Object_0(state_, TNode<HeapObject>{parameter2}, TNode<IntPtrT>{tmp118}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 872);
    tmp121 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp119, tmp120});
    tmp122 = UnsafeCast_JSAny_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp121});
    tmp123 = Method_SortState_Compare_0(state_, TNode<Context>{parameter0}, TNode<SortState>{parameter1}, TNode<Object>{parameter3}, TNode<Object>{tmp122});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 874);
    tmp124 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp125 = NumberIsLessThan_0(state_, TNode<Number>{tmp123}, TNode<Number>{tmp124});
    ca_.Branch(tmp125, &block71, std::vector<Node*>{phi_bb68_7, phi_bb68_8}, &block72, std::vector<Node*>{phi_bb68_7, phi_bb68_8});
  }

  TNode<Smi> phi_bb69_7;
  TNode<Smi> phi_bb69_8;
  TNode<Number> phi_bb69_10;
  if (block69.is_used()) {
    ca_.Bind(&block69, &phi_bb69_7, &phi_bb69_8, &phi_bb69_10);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb71_7;
  TNode<Smi> phi_bb71_8;
  if (block71.is_used()) {
    ca_.Bind(&block71, &phi_bb71_7, &phi_bb71_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 874);
    ca_.Goto(&block73, phi_bb71_7, tmp105);
  }

  TNode<Smi> phi_bb72_7;
  TNode<Smi> phi_bb72_8;
  TNode<Smi> tmp126;
  TNode<Smi> tmp127;
  if (block72.is_used()) {
    ca_.Bind(&block72, &phi_bb72_7, &phi_bb72_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 877);
    tmp126 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp127 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp105}, TNode<Smi>{tmp126});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 874);
    ca_.Goto(&block73, tmp127, phi_bb72_8);
  }

  TNode<Smi> phi_bb73_7;
  TNode<Smi> phi_bb73_8;
  if (block73.is_used()) {
    ca_.Bind(&block73, &phi_bb73_7, &phi_bb73_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 869);
    ca_.Goto(&block63, phi_bb73_7, phi_bb73_8, tmp123);
  }

  TNode<Smi> phi_bb62_7;
  TNode<Smi> phi_bb62_8;
  TNode<Number> phi_bb62_10;
  TNode<BoolT> tmp128;
  if (block62.is_used()) {
    ca_.Bind(&block62, &phi_bb62_7, &phi_bb62_8, &phi_bb62_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 881);
    tmp128 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{phi_bb62_7}, TNode<Smi>{phi_bb62_8});
    ca_.Branch(tmp128, &block74, std::vector<Node*>{phi_bb62_7, phi_bb62_8, phi_bb62_10}, &block75, std::vector<Node*>{phi_bb62_7, phi_bb62_8, phi_bb62_10});
  }

  TNode<Smi> phi_bb75_7;
  TNode<Smi> phi_bb75_8;
  TNode<Number> phi_bb75_10;
  if (block75.is_used()) {
    ca_.Bind(&block75, &phi_bb75_7, &phi_bb75_8, &phi_bb75_10);
    CodeStubAssembler(state_).FailAssert("Torque assert 'lastOfs == offset' failed", "third_party/v8/builtins/array-sort.tq", 881);
  }

  TNode<Smi> phi_bb74_7;
  TNode<Smi> phi_bb74_8;
  TNode<Number> phi_bb74_10;
  TNode<Smi> tmp129;
  TNode<BoolT> tmp130;
  if (block74.is_used()) {
    ca_.Bind(&block74, &phi_bb74_7, &phi_bb74_8, &phi_bb74_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 882);
    tmp129 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp130 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{tmp129}, TNode<Smi>{phi_bb74_8});
    ca_.Branch(tmp130, &block78, std::vector<Node*>{phi_bb74_7, phi_bb74_8, phi_bb74_10}, &block79, std::vector<Node*>{phi_bb74_7, phi_bb74_8, phi_bb74_10});
  }

  TNode<Smi> phi_bb78_7;
  TNode<Smi> phi_bb78_8;
  TNode<Number> phi_bb78_10;
  TNode<BoolT> tmp131;
  if (block78.is_used()) {
    ca_.Bind(&block78, &phi_bb78_7, &phi_bb78_8, &phi_bb78_10);
    tmp131 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{phi_bb78_8}, TNode<Smi>{parameter5});
    ca_.Goto(&block80, phi_bb78_7, phi_bb78_8, phi_bb78_10, tmp131);
  }

  TNode<Smi> phi_bb79_7;
  TNode<Smi> phi_bb79_8;
  TNode<Number> phi_bb79_10;
  TNode<BoolT> tmp132;
  if (block79.is_used()) {
    ca_.Bind(&block79, &phi_bb79_7, &phi_bb79_8, &phi_bb79_10);
    tmp132 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block80, phi_bb79_7, phi_bb79_8, phi_bb79_10, tmp132);
  }

  TNode<Smi> phi_bb80_7;
  TNode<Smi> phi_bb80_8;
  TNode<Number> phi_bb80_10;
  TNode<BoolT> phi_bb80_12;
  if (block80.is_used()) {
    ca_.Bind(&block80, &phi_bb80_7, &phi_bb80_8, &phi_bb80_10, &phi_bb80_12);
    ca_.Branch(phi_bb80_12, &block76, std::vector<Node*>{phi_bb80_7, phi_bb80_8, phi_bb80_10}, &block77, std::vector<Node*>{phi_bb80_7, phi_bb80_8, phi_bb80_10});
  }

  TNode<Smi> phi_bb77_7;
  TNode<Smi> phi_bb77_8;
  TNode<Number> phi_bb77_10;
  if (block77.is_used()) {
    ca_.Bind(&block77, &phi_bb77_7, &phi_bb77_8, &phi_bb77_10);
    CodeStubAssembler(state_).FailAssert("Torque assert '0 <= offset && offset <= length' failed", "third_party/v8/builtins/array-sort.tq", 882);
  }

  TNode<Smi> phi_bb76_7;
  TNode<Smi> phi_bb76_8;
  TNode<Number> phi_bb76_10;
  if (block76.is_used()) {
    ca_.Bind(&block76, &phi_bb76_7, &phi_bb76_8, &phi_bb76_10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 883);
    CodeStubAssembler(state_).Return(phi_bb76_8);
  }
}

void MergeLow_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<SortState> p_sortState, TNode<Smi> p_baseA, TNode<Smi> p_lengthAArg, TNode<Smi> p_baseB, TNode<Smi> p_lengthBArg) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block18(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block19(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block25(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block26(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block32(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block33(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block34(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block35(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi> block38(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi> block36(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block41(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block39(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block44(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block45(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT> block46(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block43(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block42(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block51(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block52(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block58(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block59(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block61(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block68(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block69(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block75(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block76(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block78(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block79(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block80(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block81(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block62(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block86(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block87(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block93(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block94(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block96(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block97(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block98(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block99(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block63(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block40(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT> block102(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT> block103(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT> block104(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT, BoolT> block105(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT> block106(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT> block107(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT, BoolT> block108(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT> block100(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block111(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block112(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT> block113(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block110(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block109(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block118(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block119(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi> block122(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi> block121(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi> block123(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block125(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block126(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block127(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block128(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi> block124(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi> block133(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi> block134(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block140(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block141(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block143(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block144(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block149(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block150(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block153(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block152(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block154(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block156(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block157(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block155(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi> block162(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi> block163(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi> block169(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi> block170(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block172(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block173(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT> block101(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi> block37(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block31(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block174(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block175(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block30(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block29(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block178(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block179(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, BoolT> block180(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block177(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block176(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block185(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block186(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi> block192(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi> block193(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block28(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block195(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Smi> tmp0;
  TNode<BoolT> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 894);
    tmp0 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp1 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{tmp0}, TNode<Smi>{p_lengthAArg});
    ca_.Branch(tmp1, &block4, std::vector<Node*>{}, &block5, std::vector<Node*>{});
  }

  TNode<Smi> tmp2;
  TNode<BoolT> tmp3;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    tmp2 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp3 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{tmp2}, TNode<Smi>{p_lengthBArg});
    ca_.Goto(&block6, tmp3);
  }

  TNode<BoolT> tmp4;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    tmp4 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block6, tmp4);
  }

  TNode<BoolT> phi_bb6_7;
  if (block6.is_used()) {
    ca_.Bind(&block6, &phi_bb6_7);
    ca_.Branch(phi_bb6_7, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    CodeStubAssembler(state_).FailAssert("Torque assert '0 < lengthAArg && 0 < lengthBArg' failed", "third_party/v8/builtins/array-sort.tq", 894);
  }

  TNode<Smi> tmp5;
  TNode<BoolT> tmp6;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 895);
    tmp5 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp6 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{tmp5}, TNode<Smi>{p_baseA});
    ca_.Branch(tmp6, &block9, std::vector<Node*>{}, &block10, std::vector<Node*>{});
  }

  TNode<Smi> tmp7;
  TNode<BoolT> tmp8;
  if (block9.is_used()) {
    ca_.Bind(&block9);
    tmp7 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp8 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{tmp7}, TNode<Smi>{p_baseB});
    ca_.Goto(&block11, tmp8);
  }

  TNode<BoolT> tmp9;
  if (block10.is_used()) {
    ca_.Bind(&block10);
    tmp9 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block11, tmp9);
  }

  TNode<BoolT> phi_bb11_7;
  if (block11.is_used()) {
    ca_.Bind(&block11, &phi_bb11_7);
    ca_.Branch(phi_bb11_7, &block7, std::vector<Node*>{}, &block8, std::vector<Node*>{});
  }

  if (block8.is_used()) {
    ca_.Bind(&block8);
    CodeStubAssembler(state_).FailAssert("Torque assert '0 <= baseA && 0 < baseB' failed", "third_party/v8/builtins/array-sort.tq", 895);
  }

  TNode<Smi> tmp10;
  TNode<BoolT> tmp11;
  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 896);
    tmp10 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{p_baseA}, TNode<Smi>{p_lengthAArg});
    tmp11 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp10}, TNode<Smi>{p_baseB});
    ca_.Branch(tmp11, &block12, std::vector<Node*>{}, &block13, std::vector<Node*>{});
  }

  if (block13.is_used()) {
    ca_.Bind(&block13);
    CodeStubAssembler(state_).FailAssert("Torque assert 'baseA + lengthAArg == baseB' failed", "third_party/v8/builtins/array-sort.tq", 896);
  }

  TNode<IntPtrT> tmp12;
  TNode<FixedArray> tmp13;
  TNode<FixedArray> tmp14;
  TNode<Smi> tmp15;
  TNode<Object> tmp16;
  TNode<Smi> tmp17;
  TNode<IntPtrT> tmp18;
  TNode<IntPtrT> tmp19;
  TNode<IntPtrT> tmp20;
  TNode<Smi> tmp21;
  TNode<IntPtrT> tmp22;
  TNode<Smi> tmp23;
  TNode<Smi> tmp24;
  TNode<IntPtrT> tmp25;
  TNode<UintPtrT> tmp26;
  TNode<UintPtrT> tmp27;
  TNode<BoolT> tmp28;
  if (block12.is_used()) {
    ca_.Bind(&block12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 901);
    tmp12 = FromConstexpr_intptr_constexpr_int31_0(state_, 52);
    tmp13 = CodeStubAssembler(state_).LoadReference<FixedArray>(CodeStubAssembler::Reference{p_sortState, tmp12});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 902);
    tmp14 = GetTempArray_0(state_, TNode<Context>{p_context}, TNode<SortState>{p_sortState}, TNode<Smi>{p_lengthAArg});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 903);
    tmp15 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp16 = CodeStubAssembler(state_).CallBuiltin(Builtins::kCopy, p_context, tmp13, p_baseA, tmp14, tmp15, p_lengthAArg);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 906);
    tmp17 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 909);
    tmp18 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp19 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp20 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp21 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp20});
    tmp22 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp21});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 909);
    tmp23 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp24 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{p_baseA}, TNode<Smi>{tmp23});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp25 = Convert_intptr_Smi_0(state_, TNode<Smi>{p_baseA});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp26 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp25});
    tmp27 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp22});
    tmp28 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp26}, TNode<UintPtrT>{tmp27});
    ca_.Branch(tmp28, &block18, std::vector<Node*>{}, &block19, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp29;
  TNode<IntPtrT> tmp30;
  TNode<IntPtrT> tmp31;
  TNode<HeapObject> tmp32;
  TNode<IntPtrT> tmp33;
  TNode<IntPtrT> tmp34;
  TNode<IntPtrT> tmp35;
  TNode<IntPtrT> tmp36;
  TNode<Smi> tmp37;
  TNode<IntPtrT> tmp38;
  TNode<Smi> tmp39;
  TNode<Smi> tmp40;
  TNode<IntPtrT> tmp41;
  TNode<UintPtrT> tmp42;
  TNode<UintPtrT> tmp43;
  TNode<BoolT> tmp44;
  if (block18.is_used()) {
    ca_.Bind(&block18);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp29 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp30 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp25}, TNode<IntPtrT>{tmp29});
    tmp31 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp18}, TNode<IntPtrT>{tmp30});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp32, tmp33) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp31}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 909);
    tmp34 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp35 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp36 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp37 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp36});
    tmp38 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp37});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 909);
    tmp39 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp40 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{p_baseB}, TNode<Smi>{tmp39});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp41 = Convert_intptr_Smi_0(state_, TNode<Smi>{p_baseB});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp42 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp41});
    tmp43 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp38});
    tmp44 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp42}, TNode<UintPtrT>{tmp43});
    ca_.Branch(tmp44, &block25, std::vector<Node*>{}, &block26, std::vector<Node*>{});
  }

  if (block19.is_used()) {
    ca_.Bind(&block19);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp45;
  TNode<IntPtrT> tmp46;
  TNode<IntPtrT> tmp47;
  TNode<HeapObject> tmp48;
  TNode<IntPtrT> tmp49;
  TNode<Object> tmp50;
  TNode<Smi> tmp51;
  TNode<Smi> tmp52;
  TNode<Smi> tmp53;
  TNode<BoolT> tmp54;
  if (block25.is_used()) {
    ca_.Bind(&block25);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp45 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp46 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp41}, TNode<IntPtrT>{tmp45});
    tmp47 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp34}, TNode<IntPtrT>{tmp46});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp48, tmp49) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp47}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 909);
    tmp50 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp48, tmp49});
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp32, tmp33}, tmp50);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 912);
    tmp51 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp52 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{p_lengthBArg}, TNode<Smi>{tmp51});
    tmp53 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp54 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp52}, TNode<Smi>{tmp53});
    ca_.Branch(tmp54, &block32, std::vector<Node*>{}, &block33, std::vector<Node*>{});
  }

  if (block26.is_used()) {
    ca_.Bind(&block26);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  if (block32.is_used()) {
    ca_.Bind(&block32);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 912);
    ca_.Goto(&block31, p_lengthAArg, tmp52, tmp24, tmp17, tmp40);
  }

  TNode<Smi> tmp55;
  TNode<BoolT> tmp56;
  if (block33.is_used()) {
    ca_.Bind(&block33);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 913);
    tmp55 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp56 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{p_lengthAArg}, TNode<Smi>{tmp55});
    ca_.Branch(tmp56, &block34, std::vector<Node*>{}, &block35, std::vector<Node*>{});
  }

  if (block34.is_used()) {
    ca_.Bind(&block34);
    ca_.Goto(&block29, p_lengthAArg, tmp52, tmp24, tmp17, tmp40);
  }

  TNode<IntPtrT> tmp57;
  TNode<Smi> tmp58;
  if (block35.is_used()) {
    ca_.Bind(&block35);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 915);
    tmp57 = FromConstexpr_intptr_constexpr_int31_0(state_, 40);
    tmp58 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_sortState, tmp57});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 918);
    ca_.Goto(&block38, p_lengthAArg, tmp52, tmp24, tmp17, tmp40, tmp58);
  }

  TNode<Smi> phi_bb38_6;
  TNode<Smi> phi_bb38_7;
  TNode<Smi> phi_bb38_10;
  TNode<Smi> phi_bb38_11;
  TNode<Smi> phi_bb38_12;
  TNode<Smi> phi_bb38_13;
  TNode<BoolT> tmp59;
  if (block38.is_used()) {
    ca_.Bind(&block38, &phi_bb38_6, &phi_bb38_7, &phi_bb38_10, &phi_bb38_11, &phi_bb38_12, &phi_bb38_13);
    tmp59 = CodeStubAssembler(state_).Int32TrueConstant();
    ca_.Branch(tmp59, &block36, std::vector<Node*>{phi_bb38_6, phi_bb38_7, phi_bb38_10, phi_bb38_11, phi_bb38_12, phi_bb38_13}, &block37, std::vector<Node*>{phi_bb38_6, phi_bb38_7, phi_bb38_10, phi_bb38_11, phi_bb38_12, phi_bb38_13});
  }

  TNode<Smi> phi_bb36_6;
  TNode<Smi> phi_bb36_7;
  TNode<Smi> phi_bb36_10;
  TNode<Smi> phi_bb36_11;
  TNode<Smi> phi_bb36_12;
  TNode<Smi> phi_bb36_13;
  TNode<Smi> tmp60;
  TNode<Smi> tmp61;
  if (block36.is_used()) {
    ca_.Bind(&block36, &phi_bb36_6, &phi_bb36_7, &phi_bb36_10, &phi_bb36_11, &phi_bb36_12, &phi_bb36_13);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 919);
    tmp60 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 920);
    tmp61 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 926);
    ca_.Goto(&block41, phi_bb36_6, phi_bb36_7, phi_bb36_10, phi_bb36_11, phi_bb36_12, phi_bb36_13, tmp60, tmp61);
  }

  TNode<Smi> phi_bb41_6;
  TNode<Smi> phi_bb41_7;
  TNode<Smi> phi_bb41_10;
  TNode<Smi> phi_bb41_11;
  TNode<Smi> phi_bb41_12;
  TNode<Smi> phi_bb41_13;
  TNode<Smi> phi_bb41_14;
  TNode<Smi> phi_bb41_15;
  TNode<BoolT> tmp62;
  if (block41.is_used()) {
    ca_.Bind(&block41, &phi_bb41_6, &phi_bb41_7, &phi_bb41_10, &phi_bb41_11, &phi_bb41_12, &phi_bb41_13, &phi_bb41_14, &phi_bb41_15);
    tmp62 = CodeStubAssembler(state_).Int32TrueConstant();
    ca_.Branch(tmp62, &block39, std::vector<Node*>{phi_bb41_6, phi_bb41_7, phi_bb41_10, phi_bb41_11, phi_bb41_12, phi_bb41_13, phi_bb41_14, phi_bb41_15}, &block40, std::vector<Node*>{phi_bb41_6, phi_bb41_7, phi_bb41_10, phi_bb41_11, phi_bb41_12, phi_bb41_13, phi_bb41_14, phi_bb41_15});
  }

  TNode<Smi> phi_bb39_6;
  TNode<Smi> phi_bb39_7;
  TNode<Smi> phi_bb39_10;
  TNode<Smi> phi_bb39_11;
  TNode<Smi> phi_bb39_12;
  TNode<Smi> phi_bb39_13;
  TNode<Smi> phi_bb39_14;
  TNode<Smi> phi_bb39_15;
  TNode<Smi> tmp63;
  TNode<BoolT> tmp64;
  if (block39.is_used()) {
    ca_.Bind(&block39, &phi_bb39_6, &phi_bb39_7, &phi_bb39_10, &phi_bb39_11, &phi_bb39_12, &phi_bb39_13, &phi_bb39_14, &phi_bb39_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 927);
    tmp63 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp64 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{phi_bb39_6}, TNode<Smi>{tmp63});
    ca_.Branch(tmp64, &block44, std::vector<Node*>{phi_bb39_6, phi_bb39_7, phi_bb39_10, phi_bb39_11, phi_bb39_12, phi_bb39_13, phi_bb39_14, phi_bb39_15}, &block45, std::vector<Node*>{phi_bb39_6, phi_bb39_7, phi_bb39_10, phi_bb39_11, phi_bb39_12, phi_bb39_13, phi_bb39_14, phi_bb39_15});
  }

  TNode<Smi> phi_bb44_6;
  TNode<Smi> phi_bb44_7;
  TNode<Smi> phi_bb44_10;
  TNode<Smi> phi_bb44_11;
  TNode<Smi> phi_bb44_12;
  TNode<Smi> phi_bb44_13;
  TNode<Smi> phi_bb44_14;
  TNode<Smi> phi_bb44_15;
  TNode<Smi> tmp65;
  TNode<BoolT> tmp66;
  if (block44.is_used()) {
    ca_.Bind(&block44, &phi_bb44_6, &phi_bb44_7, &phi_bb44_10, &phi_bb44_11, &phi_bb44_12, &phi_bb44_13, &phi_bb44_14, &phi_bb44_15);
    tmp65 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp66 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{phi_bb44_7}, TNode<Smi>{tmp65});
    ca_.Goto(&block46, phi_bb44_6, phi_bb44_7, phi_bb44_10, phi_bb44_11, phi_bb44_12, phi_bb44_13, phi_bb44_14, phi_bb44_15, tmp66);
  }

  TNode<Smi> phi_bb45_6;
  TNode<Smi> phi_bb45_7;
  TNode<Smi> phi_bb45_10;
  TNode<Smi> phi_bb45_11;
  TNode<Smi> phi_bb45_12;
  TNode<Smi> phi_bb45_13;
  TNode<Smi> phi_bb45_14;
  TNode<Smi> phi_bb45_15;
  TNode<BoolT> tmp67;
  if (block45.is_used()) {
    ca_.Bind(&block45, &phi_bb45_6, &phi_bb45_7, &phi_bb45_10, &phi_bb45_11, &phi_bb45_12, &phi_bb45_13, &phi_bb45_14, &phi_bb45_15);
    tmp67 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block46, phi_bb45_6, phi_bb45_7, phi_bb45_10, phi_bb45_11, phi_bb45_12, phi_bb45_13, phi_bb45_14, phi_bb45_15, tmp67);
  }

  TNode<Smi> phi_bb46_6;
  TNode<Smi> phi_bb46_7;
  TNode<Smi> phi_bb46_10;
  TNode<Smi> phi_bb46_11;
  TNode<Smi> phi_bb46_12;
  TNode<Smi> phi_bb46_13;
  TNode<Smi> phi_bb46_14;
  TNode<Smi> phi_bb46_15;
  TNode<BoolT> phi_bb46_17;
  if (block46.is_used()) {
    ca_.Bind(&block46, &phi_bb46_6, &phi_bb46_7, &phi_bb46_10, &phi_bb46_11, &phi_bb46_12, &phi_bb46_13, &phi_bb46_14, &phi_bb46_15, &phi_bb46_17);
    ca_.Branch(phi_bb46_17, &block42, std::vector<Node*>{phi_bb46_6, phi_bb46_7, phi_bb46_10, phi_bb46_11, phi_bb46_12, phi_bb46_13, phi_bb46_14, phi_bb46_15}, &block43, std::vector<Node*>{phi_bb46_6, phi_bb46_7, phi_bb46_10, phi_bb46_11, phi_bb46_12, phi_bb46_13, phi_bb46_14, phi_bb46_15});
  }

  TNode<Smi> phi_bb43_6;
  TNode<Smi> phi_bb43_7;
  TNode<Smi> phi_bb43_10;
  TNode<Smi> phi_bb43_11;
  TNode<Smi> phi_bb43_12;
  TNode<Smi> phi_bb43_13;
  TNode<Smi> phi_bb43_14;
  TNode<Smi> phi_bb43_15;
  if (block43.is_used()) {
    ca_.Bind(&block43, &phi_bb43_6, &phi_bb43_7, &phi_bb43_10, &phi_bb43_11, &phi_bb43_12, &phi_bb43_13, &phi_bb43_14, &phi_bb43_15);
    CodeStubAssembler(state_).FailAssert("Torque assert 'lengthA > 1 && lengthB > 0' failed", "third_party/v8/builtins/array-sort.tq", 927);
  }

  TNode<Smi> phi_bb42_6;
  TNode<Smi> phi_bb42_7;
  TNode<Smi> phi_bb42_10;
  TNode<Smi> phi_bb42_11;
  TNode<Smi> phi_bb42_12;
  TNode<Smi> phi_bb42_13;
  TNode<Smi> phi_bb42_14;
  TNode<Smi> phi_bb42_15;
  TNode<IntPtrT> tmp68;
  TNode<IntPtrT> tmp69;
  TNode<IntPtrT> tmp70;
  TNode<Smi> tmp71;
  TNode<IntPtrT> tmp72;
  TNode<IntPtrT> tmp73;
  TNode<UintPtrT> tmp74;
  TNode<UintPtrT> tmp75;
  TNode<BoolT> tmp76;
  if (block42.is_used()) {
    ca_.Bind(&block42, &phi_bb42_6, &phi_bb42_7, &phi_bb42_10, &phi_bb42_11, &phi_bb42_12, &phi_bb42_13, &phi_bb42_14, &phi_bb42_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 930);
    tmp68 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp69 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp70 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp71 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp70});
    tmp72 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp71});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp73 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb42_12});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp74 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp73});
    tmp75 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp72});
    tmp76 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp74}, TNode<UintPtrT>{tmp75});
    ca_.Branch(tmp76, &block51, std::vector<Node*>{phi_bb42_6, phi_bb42_7, phi_bb42_10, phi_bb42_11, phi_bb42_12, phi_bb42_13, phi_bb42_14, phi_bb42_15, phi_bb42_12, phi_bb42_12}, &block52, std::vector<Node*>{phi_bb42_6, phi_bb42_7, phi_bb42_10, phi_bb42_11, phi_bb42_12, phi_bb42_13, phi_bb42_14, phi_bb42_15, phi_bb42_12, phi_bb42_12});
  }

  TNode<Smi> phi_bb51_6;
  TNode<Smi> phi_bb51_7;
  TNode<Smi> phi_bb51_10;
  TNode<Smi> phi_bb51_11;
  TNode<Smi> phi_bb51_12;
  TNode<Smi> phi_bb51_13;
  TNode<Smi> phi_bb51_14;
  TNode<Smi> phi_bb51_15;
  TNode<Smi> phi_bb51_21;
  TNode<Smi> phi_bb51_22;
  TNode<IntPtrT> tmp77;
  TNode<IntPtrT> tmp78;
  TNode<IntPtrT> tmp79;
  TNode<HeapObject> tmp80;
  TNode<IntPtrT> tmp81;
  TNode<Object> tmp82;
  TNode<Object> tmp83;
  TNode<IntPtrT> tmp84;
  TNode<IntPtrT> tmp85;
  TNode<IntPtrT> tmp86;
  TNode<Smi> tmp87;
  TNode<IntPtrT> tmp88;
  TNode<IntPtrT> tmp89;
  TNode<UintPtrT> tmp90;
  TNode<UintPtrT> tmp91;
  TNode<BoolT> tmp92;
  if (block51.is_used()) {
    ca_.Bind(&block51, &phi_bb51_6, &phi_bb51_7, &phi_bb51_10, &phi_bb51_11, &phi_bb51_12, &phi_bb51_13, &phi_bb51_14, &phi_bb51_15, &phi_bb51_21, &phi_bb51_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp77 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp78 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp73}, TNode<IntPtrT>{tmp77});
    tmp79 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp68}, TNode<IntPtrT>{tmp78});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp80, tmp81) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp79}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 930);
    tmp82 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp80, tmp81});
    tmp83 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp82});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 931);
    tmp84 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp85 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp86 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp87 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp14, tmp86});
    tmp88 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp87});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp89 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb51_11});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp90 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp89});
    tmp91 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp88});
    tmp92 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp90}, TNode<UintPtrT>{tmp91});
    ca_.Branch(tmp92, &block58, std::vector<Node*>{phi_bb51_6, phi_bb51_7, phi_bb51_10, phi_bb51_11, phi_bb51_12, phi_bb51_13, phi_bb51_14, phi_bb51_15, phi_bb51_11, phi_bb51_11}, &block59, std::vector<Node*>{phi_bb51_6, phi_bb51_7, phi_bb51_10, phi_bb51_11, phi_bb51_12, phi_bb51_13, phi_bb51_14, phi_bb51_15, phi_bb51_11, phi_bb51_11});
  }

  TNode<Smi> phi_bb52_6;
  TNode<Smi> phi_bb52_7;
  TNode<Smi> phi_bb52_10;
  TNode<Smi> phi_bb52_11;
  TNode<Smi> phi_bb52_12;
  TNode<Smi> phi_bb52_13;
  TNode<Smi> phi_bb52_14;
  TNode<Smi> phi_bb52_15;
  TNode<Smi> phi_bb52_21;
  TNode<Smi> phi_bb52_22;
  if (block52.is_used()) {
    ca_.Bind(&block52, &phi_bb52_6, &phi_bb52_7, &phi_bb52_10, &phi_bb52_11, &phi_bb52_12, &phi_bb52_13, &phi_bb52_14, &phi_bb52_15, &phi_bb52_21, &phi_bb52_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb58_6;
  TNode<Smi> phi_bb58_7;
  TNode<Smi> phi_bb58_10;
  TNode<Smi> phi_bb58_11;
  TNode<Smi> phi_bb58_12;
  TNode<Smi> phi_bb58_13;
  TNode<Smi> phi_bb58_14;
  TNode<Smi> phi_bb58_15;
  TNode<Smi> phi_bb58_22;
  TNode<Smi> phi_bb58_23;
  TNode<IntPtrT> tmp93;
  TNode<IntPtrT> tmp94;
  TNode<IntPtrT> tmp95;
  TNode<HeapObject> tmp96;
  TNode<IntPtrT> tmp97;
  TNode<Object> tmp98;
  TNode<Object> tmp99;
  TNode<Number> tmp100;
  TNode<Number> tmp101;
  TNode<BoolT> tmp102;
  if (block58.is_used()) {
    ca_.Bind(&block58, &phi_bb58_6, &phi_bb58_7, &phi_bb58_10, &phi_bb58_11, &phi_bb58_12, &phi_bb58_13, &phi_bb58_14, &phi_bb58_15, &phi_bb58_22, &phi_bb58_23);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp93 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp94 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp89}, TNode<IntPtrT>{tmp93});
    tmp95 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp84}, TNode<IntPtrT>{tmp94});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp96, tmp97) = NewReference_Object_0(state_, TNode<HeapObject>{tmp14}, TNode<IntPtrT>{tmp95}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 931);
    tmp98 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp96, tmp97});
    tmp99 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp98});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 929);
    tmp100 = Method_SortState_Compare_0(state_, TNode<Context>{p_context}, TNode<SortState>{p_sortState}, TNode<Object>{tmp83}, TNode<Object>{tmp99});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 933);
    tmp101 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp102 = NumberIsLessThan_0(state_, TNode<Number>{tmp100}, TNode<Number>{tmp101});
    ca_.Branch(tmp102, &block61, std::vector<Node*>{phi_bb58_6, phi_bb58_7, phi_bb58_10, phi_bb58_11, phi_bb58_12, phi_bb58_13, phi_bb58_14, phi_bb58_15}, &block62, std::vector<Node*>{phi_bb58_6, phi_bb58_7, phi_bb58_10, phi_bb58_11, phi_bb58_12, phi_bb58_13, phi_bb58_14, phi_bb58_15});
  }

  TNode<Smi> phi_bb59_6;
  TNode<Smi> phi_bb59_7;
  TNode<Smi> phi_bb59_10;
  TNode<Smi> phi_bb59_11;
  TNode<Smi> phi_bb59_12;
  TNode<Smi> phi_bb59_13;
  TNode<Smi> phi_bb59_14;
  TNode<Smi> phi_bb59_15;
  TNode<Smi> phi_bb59_22;
  TNode<Smi> phi_bb59_23;
  if (block59.is_used()) {
    ca_.Bind(&block59, &phi_bb59_6, &phi_bb59_7, &phi_bb59_10, &phi_bb59_11, &phi_bb59_12, &phi_bb59_13, &phi_bb59_14, &phi_bb59_15, &phi_bb59_22, &phi_bb59_23);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb61_6;
  TNode<Smi> phi_bb61_7;
  TNode<Smi> phi_bb61_10;
  TNode<Smi> phi_bb61_11;
  TNode<Smi> phi_bb61_12;
  TNode<Smi> phi_bb61_13;
  TNode<Smi> phi_bb61_14;
  TNode<Smi> phi_bb61_15;
  TNode<IntPtrT> tmp103;
  TNode<IntPtrT> tmp104;
  TNode<IntPtrT> tmp105;
  TNode<Smi> tmp106;
  TNode<IntPtrT> tmp107;
  TNode<Smi> tmp108;
  TNode<Smi> tmp109;
  TNode<IntPtrT> tmp110;
  TNode<UintPtrT> tmp111;
  TNode<UintPtrT> tmp112;
  TNode<BoolT> tmp113;
  if (block61.is_used()) {
    ca_.Bind(&block61, &phi_bb61_6, &phi_bb61_7, &phi_bb61_10, &phi_bb61_11, &phi_bb61_12, &phi_bb61_13, &phi_bb61_14, &phi_bb61_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 934);
    tmp103 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp104 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp105 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp106 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp105});
    tmp107 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp106});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 934);
    tmp108 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp109 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb61_10}, TNode<Smi>{tmp108});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp110 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb61_10});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp111 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp110});
    tmp112 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp107});
    tmp113 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp111}, TNode<UintPtrT>{tmp112});
    ca_.Branch(tmp113, &block68, std::vector<Node*>{phi_bb61_6, phi_bb61_7, phi_bb61_11, phi_bb61_12, phi_bb61_13, phi_bb61_14, phi_bb61_15, phi_bb61_10, phi_bb61_10}, &block69, std::vector<Node*>{phi_bb61_6, phi_bb61_7, phi_bb61_11, phi_bb61_12, phi_bb61_13, phi_bb61_14, phi_bb61_15, phi_bb61_10, phi_bb61_10});
  }

  TNode<Smi> phi_bb68_6;
  TNode<Smi> phi_bb68_7;
  TNode<Smi> phi_bb68_11;
  TNode<Smi> phi_bb68_12;
  TNode<Smi> phi_bb68_13;
  TNode<Smi> phi_bb68_14;
  TNode<Smi> phi_bb68_15;
  TNode<Smi> phi_bb68_21;
  TNode<Smi> phi_bb68_22;
  TNode<IntPtrT> tmp114;
  TNode<IntPtrT> tmp115;
  TNode<IntPtrT> tmp116;
  TNode<HeapObject> tmp117;
  TNode<IntPtrT> tmp118;
  TNode<IntPtrT> tmp119;
  TNode<IntPtrT> tmp120;
  TNode<IntPtrT> tmp121;
  TNode<Smi> tmp122;
  TNode<IntPtrT> tmp123;
  TNode<Smi> tmp124;
  TNode<Smi> tmp125;
  TNode<IntPtrT> tmp126;
  TNode<UintPtrT> tmp127;
  TNode<UintPtrT> tmp128;
  TNode<BoolT> tmp129;
  if (block68.is_used()) {
    ca_.Bind(&block68, &phi_bb68_6, &phi_bb68_7, &phi_bb68_11, &phi_bb68_12, &phi_bb68_13, &phi_bb68_14, &phi_bb68_15, &phi_bb68_21, &phi_bb68_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp114 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp115 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp110}, TNode<IntPtrT>{tmp114});
    tmp116 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp103}, TNode<IntPtrT>{tmp115});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp117, tmp118) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp116}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 934);
    tmp119 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp120 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp121 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp122 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp121});
    tmp123 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp122});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 934);
    tmp124 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp125 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb68_12}, TNode<Smi>{tmp124});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp126 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb68_12});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp127 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp126});
    tmp128 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp123});
    tmp129 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp127}, TNode<UintPtrT>{tmp128});
    ca_.Branch(tmp129, &block75, std::vector<Node*>{phi_bb68_6, phi_bb68_7, phi_bb68_11, phi_bb68_13, phi_bb68_14, phi_bb68_15, phi_bb68_21, phi_bb68_22, phi_bb68_12, phi_bb68_12}, &block76, std::vector<Node*>{phi_bb68_6, phi_bb68_7, phi_bb68_11, phi_bb68_13, phi_bb68_14, phi_bb68_15, phi_bb68_21, phi_bb68_22, phi_bb68_12, phi_bb68_12});
  }

  TNode<Smi> phi_bb69_6;
  TNode<Smi> phi_bb69_7;
  TNode<Smi> phi_bb69_11;
  TNode<Smi> phi_bb69_12;
  TNode<Smi> phi_bb69_13;
  TNode<Smi> phi_bb69_14;
  TNode<Smi> phi_bb69_15;
  TNode<Smi> phi_bb69_21;
  TNode<Smi> phi_bb69_22;
  if (block69.is_used()) {
    ca_.Bind(&block69, &phi_bb69_6, &phi_bb69_7, &phi_bb69_11, &phi_bb69_12, &phi_bb69_13, &phi_bb69_14, &phi_bb69_15, &phi_bb69_21, &phi_bb69_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb75_6;
  TNode<Smi> phi_bb75_7;
  TNode<Smi> phi_bb75_11;
  TNode<Smi> phi_bb75_13;
  TNode<Smi> phi_bb75_14;
  TNode<Smi> phi_bb75_15;
  TNode<Smi> phi_bb75_21;
  TNode<Smi> phi_bb75_22;
  TNode<Smi> phi_bb75_29;
  TNode<Smi> phi_bb75_30;
  TNode<IntPtrT> tmp130;
  TNode<IntPtrT> tmp131;
  TNode<IntPtrT> tmp132;
  TNode<HeapObject> tmp133;
  TNode<IntPtrT> tmp134;
  TNode<Object> tmp135;
  TNode<Smi> tmp136;
  TNode<Smi> tmp137;
  TNode<Smi> tmp138;
  TNode<Smi> tmp139;
  TNode<Smi> tmp140;
  TNode<Smi> tmp141;
  TNode<BoolT> tmp142;
  if (block75.is_used()) {
    ca_.Bind(&block75, &phi_bb75_6, &phi_bb75_7, &phi_bb75_11, &phi_bb75_13, &phi_bb75_14, &phi_bb75_15, &phi_bb75_21, &phi_bb75_22, &phi_bb75_29, &phi_bb75_30);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp130 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp131 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp126}, TNode<IntPtrT>{tmp130});
    tmp132 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp119}, TNode<IntPtrT>{tmp131});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp133, tmp134) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp132}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 934);
    tmp135 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp133, tmp134});
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp117, tmp118}, tmp135);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 936);
    tmp136 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp137 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb75_15}, TNode<Smi>{tmp136});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 937);
    tmp138 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp139 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb75_7}, TNode<Smi>{tmp138});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 938);
    tmp140 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 940);
    tmp141 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp142 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp139}, TNode<Smi>{tmp141});
    ca_.Branch(tmp142, &block78, std::vector<Node*>{phi_bb75_6, phi_bb75_11, phi_bb75_13}, &block79, std::vector<Node*>{phi_bb75_6, phi_bb75_11, phi_bb75_13});
  }

  TNode<Smi> phi_bb76_6;
  TNode<Smi> phi_bb76_7;
  TNode<Smi> phi_bb76_11;
  TNode<Smi> phi_bb76_13;
  TNode<Smi> phi_bb76_14;
  TNode<Smi> phi_bb76_15;
  TNode<Smi> phi_bb76_21;
  TNode<Smi> phi_bb76_22;
  TNode<Smi> phi_bb76_29;
  TNode<Smi> phi_bb76_30;
  if (block76.is_used()) {
    ca_.Bind(&block76, &phi_bb76_6, &phi_bb76_7, &phi_bb76_11, &phi_bb76_13, &phi_bb76_14, &phi_bb76_15, &phi_bb76_21, &phi_bb76_22, &phi_bb76_29, &phi_bb76_30);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb78_6;
  TNode<Smi> phi_bb78_11;
  TNode<Smi> phi_bb78_13;
  if (block78.is_used()) {
    ca_.Bind(&block78, &phi_bb78_6, &phi_bb78_11, &phi_bb78_13);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 940);
    ca_.Goto(&block31, phi_bb78_6, tmp139, tmp109, phi_bb78_11, tmp125);
  }

  TNode<Smi> phi_bb79_6;
  TNode<Smi> phi_bb79_11;
  TNode<Smi> phi_bb79_13;
  TNode<BoolT> tmp143;
  if (block79.is_used()) {
    ca_.Bind(&block79, &phi_bb79_6, &phi_bb79_11, &phi_bb79_13);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 941);
    tmp143 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{tmp137}, TNode<Smi>{phi_bb79_13});
    ca_.Branch(tmp143, &block80, std::vector<Node*>{phi_bb79_6, phi_bb79_11, phi_bb79_13}, &block81, std::vector<Node*>{phi_bb79_6, phi_bb79_11, phi_bb79_13});
  }

  TNode<Smi> phi_bb80_6;
  TNode<Smi> phi_bb80_11;
  TNode<Smi> phi_bb80_13;
  if (block80.is_used()) {
    ca_.Bind(&block80, &phi_bb80_6, &phi_bb80_11, &phi_bb80_13);
    ca_.Goto(&block40, phi_bb80_6, tmp139, tmp109, phi_bb80_11, tmp125, phi_bb80_13, tmp140, tmp137);
  }

  TNode<Smi> phi_bb81_6;
  TNode<Smi> phi_bb81_11;
  TNode<Smi> phi_bb81_13;
  if (block81.is_used()) {
    ca_.Bind(&block81, &phi_bb81_6, &phi_bb81_11, &phi_bb81_13);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 933);
    ca_.Goto(&block63, phi_bb81_6, tmp139, tmp109, phi_bb81_11, tmp125, phi_bb81_13, tmp140, tmp137);
  }

  TNode<Smi> phi_bb62_6;
  TNode<Smi> phi_bb62_7;
  TNode<Smi> phi_bb62_10;
  TNode<Smi> phi_bb62_11;
  TNode<Smi> phi_bb62_12;
  TNode<Smi> phi_bb62_13;
  TNode<Smi> phi_bb62_14;
  TNode<Smi> phi_bb62_15;
  TNode<IntPtrT> tmp144;
  TNode<IntPtrT> tmp145;
  TNode<IntPtrT> tmp146;
  TNode<Smi> tmp147;
  TNode<IntPtrT> tmp148;
  TNode<Smi> tmp149;
  TNode<Smi> tmp150;
  TNode<IntPtrT> tmp151;
  TNode<UintPtrT> tmp152;
  TNode<UintPtrT> tmp153;
  TNode<BoolT> tmp154;
  if (block62.is_used()) {
    ca_.Bind(&block62, &phi_bb62_6, &phi_bb62_7, &phi_bb62_10, &phi_bb62_11, &phi_bb62_12, &phi_bb62_13, &phi_bb62_14, &phi_bb62_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 943);
    tmp144 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp145 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp146 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp147 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp146});
    tmp148 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp147});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 943);
    tmp149 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp150 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb62_10}, TNode<Smi>{tmp149});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp151 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb62_10});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp152 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp151});
    tmp153 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp148});
    tmp154 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp152}, TNode<UintPtrT>{tmp153});
    ca_.Branch(tmp154, &block86, std::vector<Node*>{phi_bb62_6, phi_bb62_7, phi_bb62_11, phi_bb62_12, phi_bb62_13, phi_bb62_14, phi_bb62_15, phi_bb62_10, phi_bb62_10}, &block87, std::vector<Node*>{phi_bb62_6, phi_bb62_7, phi_bb62_11, phi_bb62_12, phi_bb62_13, phi_bb62_14, phi_bb62_15, phi_bb62_10, phi_bb62_10});
  }

  TNode<Smi> phi_bb86_6;
  TNode<Smi> phi_bb86_7;
  TNode<Smi> phi_bb86_11;
  TNode<Smi> phi_bb86_12;
  TNode<Smi> phi_bb86_13;
  TNode<Smi> phi_bb86_14;
  TNode<Smi> phi_bb86_15;
  TNode<Smi> phi_bb86_21;
  TNode<Smi> phi_bb86_22;
  TNode<IntPtrT> tmp155;
  TNode<IntPtrT> tmp156;
  TNode<IntPtrT> tmp157;
  TNode<HeapObject> tmp158;
  TNode<IntPtrT> tmp159;
  TNode<IntPtrT> tmp160;
  TNode<IntPtrT> tmp161;
  TNode<IntPtrT> tmp162;
  TNode<Smi> tmp163;
  TNode<IntPtrT> tmp164;
  TNode<Smi> tmp165;
  TNode<Smi> tmp166;
  TNode<IntPtrT> tmp167;
  TNode<UintPtrT> tmp168;
  TNode<UintPtrT> tmp169;
  TNode<BoolT> tmp170;
  if (block86.is_used()) {
    ca_.Bind(&block86, &phi_bb86_6, &phi_bb86_7, &phi_bb86_11, &phi_bb86_12, &phi_bb86_13, &phi_bb86_14, &phi_bb86_15, &phi_bb86_21, &phi_bb86_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp155 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp156 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp151}, TNode<IntPtrT>{tmp155});
    tmp157 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp144}, TNode<IntPtrT>{tmp156});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp158, tmp159) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp157}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 943);
    tmp160 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp161 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp162 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp163 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp14, tmp162});
    tmp164 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp163});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 943);
    tmp165 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp166 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb86_11}, TNode<Smi>{tmp165});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp167 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb86_11});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp168 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp167});
    tmp169 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp164});
    tmp170 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp168}, TNode<UintPtrT>{tmp169});
    ca_.Branch(tmp170, &block93, std::vector<Node*>{phi_bb86_6, phi_bb86_7, phi_bb86_12, phi_bb86_13, phi_bb86_14, phi_bb86_15, phi_bb86_21, phi_bb86_22, phi_bb86_11, phi_bb86_11}, &block94, std::vector<Node*>{phi_bb86_6, phi_bb86_7, phi_bb86_12, phi_bb86_13, phi_bb86_14, phi_bb86_15, phi_bb86_21, phi_bb86_22, phi_bb86_11, phi_bb86_11});
  }

  TNode<Smi> phi_bb87_6;
  TNode<Smi> phi_bb87_7;
  TNode<Smi> phi_bb87_11;
  TNode<Smi> phi_bb87_12;
  TNode<Smi> phi_bb87_13;
  TNode<Smi> phi_bb87_14;
  TNode<Smi> phi_bb87_15;
  TNode<Smi> phi_bb87_21;
  TNode<Smi> phi_bb87_22;
  if (block87.is_used()) {
    ca_.Bind(&block87, &phi_bb87_6, &phi_bb87_7, &phi_bb87_11, &phi_bb87_12, &phi_bb87_13, &phi_bb87_14, &phi_bb87_15, &phi_bb87_21, &phi_bb87_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb93_6;
  TNode<Smi> phi_bb93_7;
  TNode<Smi> phi_bb93_12;
  TNode<Smi> phi_bb93_13;
  TNode<Smi> phi_bb93_14;
  TNode<Smi> phi_bb93_15;
  TNode<Smi> phi_bb93_21;
  TNode<Smi> phi_bb93_22;
  TNode<Smi> phi_bb93_29;
  TNode<Smi> phi_bb93_30;
  TNode<IntPtrT> tmp171;
  TNode<IntPtrT> tmp172;
  TNode<IntPtrT> tmp173;
  TNode<HeapObject> tmp174;
  TNode<IntPtrT> tmp175;
  TNode<Object> tmp176;
  TNode<Smi> tmp177;
  TNode<Smi> tmp178;
  TNode<Smi> tmp179;
  TNode<Smi> tmp180;
  TNode<Smi> tmp181;
  TNode<Smi> tmp182;
  TNode<BoolT> tmp183;
  if (block93.is_used()) {
    ca_.Bind(&block93, &phi_bb93_6, &phi_bb93_7, &phi_bb93_12, &phi_bb93_13, &phi_bb93_14, &phi_bb93_15, &phi_bb93_21, &phi_bb93_22, &phi_bb93_29, &phi_bb93_30);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp171 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp172 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp167}, TNode<IntPtrT>{tmp171});
    tmp173 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp160}, TNode<IntPtrT>{tmp172});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp174, tmp175) = NewReference_Object_0(state_, TNode<HeapObject>{tmp14}, TNode<IntPtrT>{tmp173}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 943);
    tmp176 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp174, tmp175});
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp158, tmp159}, tmp176);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 945);
    tmp177 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp178 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb93_14}, TNode<Smi>{tmp177});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 946);
    tmp179 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp180 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb93_6}, TNode<Smi>{tmp179});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 947);
    tmp181 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 949);
    tmp182 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp183 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp180}, TNode<Smi>{tmp182});
    ca_.Branch(tmp183, &block96, std::vector<Node*>{phi_bb93_7, phi_bb93_12, phi_bb93_13}, &block97, std::vector<Node*>{phi_bb93_7, phi_bb93_12, phi_bb93_13});
  }

  TNode<Smi> phi_bb94_6;
  TNode<Smi> phi_bb94_7;
  TNode<Smi> phi_bb94_12;
  TNode<Smi> phi_bb94_13;
  TNode<Smi> phi_bb94_14;
  TNode<Smi> phi_bb94_15;
  TNode<Smi> phi_bb94_21;
  TNode<Smi> phi_bb94_22;
  TNode<Smi> phi_bb94_29;
  TNode<Smi> phi_bb94_30;
  if (block94.is_used()) {
    ca_.Bind(&block94, &phi_bb94_6, &phi_bb94_7, &phi_bb94_12, &phi_bb94_13, &phi_bb94_14, &phi_bb94_15, &phi_bb94_21, &phi_bb94_22, &phi_bb94_29, &phi_bb94_30);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb96_7;
  TNode<Smi> phi_bb96_12;
  TNode<Smi> phi_bb96_13;
  if (block96.is_used()) {
    ca_.Bind(&block96, &phi_bb96_7, &phi_bb96_12, &phi_bb96_13);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 949);
    ca_.Goto(&block29, tmp180, phi_bb96_7, tmp150, tmp166, phi_bb96_12);
  }

  TNode<Smi> phi_bb97_7;
  TNode<Smi> phi_bb97_12;
  TNode<Smi> phi_bb97_13;
  TNode<BoolT> tmp184;
  if (block97.is_used()) {
    ca_.Bind(&block97, &phi_bb97_7, &phi_bb97_12, &phi_bb97_13);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 950);
    tmp184 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{tmp178}, TNode<Smi>{phi_bb97_13});
    ca_.Branch(tmp184, &block98, std::vector<Node*>{phi_bb97_7, phi_bb97_12, phi_bb97_13}, &block99, std::vector<Node*>{phi_bb97_7, phi_bb97_12, phi_bb97_13});
  }

  TNode<Smi> phi_bb98_7;
  TNode<Smi> phi_bb98_12;
  TNode<Smi> phi_bb98_13;
  if (block98.is_used()) {
    ca_.Bind(&block98, &phi_bb98_7, &phi_bb98_12, &phi_bb98_13);
    ca_.Goto(&block40, tmp180, phi_bb98_7, tmp150, tmp166, phi_bb98_12, phi_bb98_13, tmp178, tmp181);
  }

  TNode<Smi> phi_bb99_7;
  TNode<Smi> phi_bb99_12;
  TNode<Smi> phi_bb99_13;
  if (block99.is_used()) {
    ca_.Bind(&block99, &phi_bb99_7, &phi_bb99_12, &phi_bb99_13);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 933);
    ca_.Goto(&block63, tmp180, phi_bb99_7, tmp150, tmp166, phi_bb99_12, phi_bb99_13, tmp178, tmp181);
  }

  TNode<Smi> phi_bb63_6;
  TNode<Smi> phi_bb63_7;
  TNode<Smi> phi_bb63_10;
  TNode<Smi> phi_bb63_11;
  TNode<Smi> phi_bb63_12;
  TNode<Smi> phi_bb63_13;
  TNode<Smi> phi_bb63_14;
  TNode<Smi> phi_bb63_15;
  if (block63.is_used()) {
    ca_.Bind(&block63, &phi_bb63_6, &phi_bb63_7, &phi_bb63_10, &phi_bb63_11, &phi_bb63_12, &phi_bb63_13, &phi_bb63_14, &phi_bb63_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 926);
    ca_.Goto(&block41, phi_bb63_6, phi_bb63_7, phi_bb63_10, phi_bb63_11, phi_bb63_12, phi_bb63_13, phi_bb63_14, phi_bb63_15);
  }

  TNode<Smi> phi_bb40_6;
  TNode<Smi> phi_bb40_7;
  TNode<Smi> phi_bb40_10;
  TNode<Smi> phi_bb40_11;
  TNode<Smi> phi_bb40_12;
  TNode<Smi> phi_bb40_13;
  TNode<Smi> phi_bb40_14;
  TNode<Smi> phi_bb40_15;
  TNode<Smi> tmp185;
  TNode<Smi> tmp186;
  TNode<BoolT> tmp187;
  if (block40.is_used()) {
    ca_.Bind(&block40, &phi_bb40_6, &phi_bb40_7, &phi_bb40_10, &phi_bb40_11, &phi_bb40_12, &phi_bb40_13, &phi_bb40_14, &phi_bb40_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 957);
    tmp185 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp186 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb40_13}, TNode<Smi>{tmp185});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 958);
    tmp187 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 959);
    ca_.Goto(&block102, phi_bb40_6, phi_bb40_7, phi_bb40_10, phi_bb40_11, phi_bb40_12, tmp186, phi_bb40_14, phi_bb40_15, tmp187);
  }

  TNode<Smi> phi_bb102_6;
  TNode<Smi> phi_bb102_7;
  TNode<Smi> phi_bb102_10;
  TNode<Smi> phi_bb102_11;
  TNode<Smi> phi_bb102_12;
  TNode<Smi> phi_bb102_13;
  TNode<Smi> phi_bb102_14;
  TNode<Smi> phi_bb102_15;
  TNode<BoolT> phi_bb102_16;
  TNode<Smi> tmp188;
  TNode<BoolT> tmp189;
  if (block102.is_used()) {
    ca_.Bind(&block102, &phi_bb102_6, &phi_bb102_7, &phi_bb102_10, &phi_bb102_11, &phi_bb102_12, &phi_bb102_13, &phi_bb102_14, &phi_bb102_15, &phi_bb102_16);
    tmp188 = FromConstexpr_Smi_constexpr_int31_0(state_, kMinGallopWins_0(state_));
    tmp189 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{phi_bb102_14}, TNode<Smi>{tmp188});
    ca_.Branch(tmp189, &block103, std::vector<Node*>{phi_bb102_6, phi_bb102_7, phi_bb102_10, phi_bb102_11, phi_bb102_12, phi_bb102_13, phi_bb102_14, phi_bb102_15, phi_bb102_16}, &block104, std::vector<Node*>{phi_bb102_6, phi_bb102_7, phi_bb102_10, phi_bb102_11, phi_bb102_12, phi_bb102_13, phi_bb102_14, phi_bb102_15, phi_bb102_16});
  }

  TNode<Smi> phi_bb103_6;
  TNode<Smi> phi_bb103_7;
  TNode<Smi> phi_bb103_10;
  TNode<Smi> phi_bb103_11;
  TNode<Smi> phi_bb103_12;
  TNode<Smi> phi_bb103_13;
  TNode<Smi> phi_bb103_14;
  TNode<Smi> phi_bb103_15;
  TNode<BoolT> phi_bb103_16;
  TNode<BoolT> tmp190;
  if (block103.is_used()) {
    ca_.Bind(&block103, &phi_bb103_6, &phi_bb103_7, &phi_bb103_10, &phi_bb103_11, &phi_bb103_12, &phi_bb103_13, &phi_bb103_14, &phi_bb103_15, &phi_bb103_16);
    tmp190 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    ca_.Goto(&block105, phi_bb103_6, phi_bb103_7, phi_bb103_10, phi_bb103_11, phi_bb103_12, phi_bb103_13, phi_bb103_14, phi_bb103_15, phi_bb103_16, tmp190);
  }

  TNode<Smi> phi_bb104_6;
  TNode<Smi> phi_bb104_7;
  TNode<Smi> phi_bb104_10;
  TNode<Smi> phi_bb104_11;
  TNode<Smi> phi_bb104_12;
  TNode<Smi> phi_bb104_13;
  TNode<Smi> phi_bb104_14;
  TNode<Smi> phi_bb104_15;
  TNode<BoolT> phi_bb104_16;
  TNode<Smi> tmp191;
  TNode<BoolT> tmp192;
  if (block104.is_used()) {
    ca_.Bind(&block104, &phi_bb104_6, &phi_bb104_7, &phi_bb104_10, &phi_bb104_11, &phi_bb104_12, &phi_bb104_13, &phi_bb104_14, &phi_bb104_15, &phi_bb104_16);
    tmp191 = FromConstexpr_Smi_constexpr_int31_0(state_, kMinGallopWins_0(state_));
    tmp192 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{phi_bb104_15}, TNode<Smi>{tmp191});
    ca_.Goto(&block105, phi_bb104_6, phi_bb104_7, phi_bb104_10, phi_bb104_11, phi_bb104_12, phi_bb104_13, phi_bb104_14, phi_bb104_15, phi_bb104_16, tmp192);
  }

  TNode<Smi> phi_bb105_6;
  TNode<Smi> phi_bb105_7;
  TNode<Smi> phi_bb105_10;
  TNode<Smi> phi_bb105_11;
  TNode<Smi> phi_bb105_12;
  TNode<Smi> phi_bb105_13;
  TNode<Smi> phi_bb105_14;
  TNode<Smi> phi_bb105_15;
  TNode<BoolT> phi_bb105_16;
  TNode<BoolT> phi_bb105_18;
  if (block105.is_used()) {
    ca_.Bind(&block105, &phi_bb105_6, &phi_bb105_7, &phi_bb105_10, &phi_bb105_11, &phi_bb105_12, &phi_bb105_13, &phi_bb105_14, &phi_bb105_15, &phi_bb105_16, &phi_bb105_18);
    ca_.Branch(phi_bb105_18, &block106, std::vector<Node*>{phi_bb105_6, phi_bb105_7, phi_bb105_10, phi_bb105_11, phi_bb105_12, phi_bb105_13, phi_bb105_14, phi_bb105_15, phi_bb105_16}, &block107, std::vector<Node*>{phi_bb105_6, phi_bb105_7, phi_bb105_10, phi_bb105_11, phi_bb105_12, phi_bb105_13, phi_bb105_14, phi_bb105_15, phi_bb105_16});
  }

  TNode<Smi> phi_bb106_6;
  TNode<Smi> phi_bb106_7;
  TNode<Smi> phi_bb106_10;
  TNode<Smi> phi_bb106_11;
  TNode<Smi> phi_bb106_12;
  TNode<Smi> phi_bb106_13;
  TNode<Smi> phi_bb106_14;
  TNode<Smi> phi_bb106_15;
  TNode<BoolT> phi_bb106_16;
  TNode<BoolT> tmp193;
  if (block106.is_used()) {
    ca_.Bind(&block106, &phi_bb106_6, &phi_bb106_7, &phi_bb106_10, &phi_bb106_11, &phi_bb106_12, &phi_bb106_13, &phi_bb106_14, &phi_bb106_15, &phi_bb106_16);
    tmp193 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    ca_.Goto(&block108, phi_bb106_6, phi_bb106_7, phi_bb106_10, phi_bb106_11, phi_bb106_12, phi_bb106_13, phi_bb106_14, phi_bb106_15, phi_bb106_16, tmp193);
  }

  TNode<Smi> phi_bb107_6;
  TNode<Smi> phi_bb107_7;
  TNode<Smi> phi_bb107_10;
  TNode<Smi> phi_bb107_11;
  TNode<Smi> phi_bb107_12;
  TNode<Smi> phi_bb107_13;
  TNode<Smi> phi_bb107_14;
  TNode<Smi> phi_bb107_15;
  TNode<BoolT> phi_bb107_16;
  if (block107.is_used()) {
    ca_.Bind(&block107, &phi_bb107_6, &phi_bb107_7, &phi_bb107_10, &phi_bb107_11, &phi_bb107_12, &phi_bb107_13, &phi_bb107_14, &phi_bb107_15, &phi_bb107_16);
    ca_.Goto(&block108, phi_bb107_6, phi_bb107_7, phi_bb107_10, phi_bb107_11, phi_bb107_12, phi_bb107_13, phi_bb107_14, phi_bb107_15, phi_bb107_16, phi_bb107_16);
  }

  TNode<Smi> phi_bb108_6;
  TNode<Smi> phi_bb108_7;
  TNode<Smi> phi_bb108_10;
  TNode<Smi> phi_bb108_11;
  TNode<Smi> phi_bb108_12;
  TNode<Smi> phi_bb108_13;
  TNode<Smi> phi_bb108_14;
  TNode<Smi> phi_bb108_15;
  TNode<BoolT> phi_bb108_16;
  TNode<BoolT> phi_bb108_18;
  if (block108.is_used()) {
    ca_.Bind(&block108, &phi_bb108_6, &phi_bb108_7, &phi_bb108_10, &phi_bb108_11, &phi_bb108_12, &phi_bb108_13, &phi_bb108_14, &phi_bb108_15, &phi_bb108_16, &phi_bb108_18);
    ca_.Branch(phi_bb108_18, &block100, std::vector<Node*>{phi_bb108_6, phi_bb108_7, phi_bb108_10, phi_bb108_11, phi_bb108_12, phi_bb108_13, phi_bb108_14, phi_bb108_15, phi_bb108_16}, &block101, std::vector<Node*>{phi_bb108_6, phi_bb108_7, phi_bb108_10, phi_bb108_11, phi_bb108_12, phi_bb108_13, phi_bb108_14, phi_bb108_15, phi_bb108_16});
  }

  TNode<Smi> phi_bb100_6;
  TNode<Smi> phi_bb100_7;
  TNode<Smi> phi_bb100_10;
  TNode<Smi> phi_bb100_11;
  TNode<Smi> phi_bb100_12;
  TNode<Smi> phi_bb100_13;
  TNode<Smi> phi_bb100_14;
  TNode<Smi> phi_bb100_15;
  TNode<BoolT> phi_bb100_16;
  TNode<BoolT> tmp194;
  TNode<Smi> tmp195;
  TNode<BoolT> tmp196;
  if (block100.is_used()) {
    ca_.Bind(&block100, &phi_bb100_6, &phi_bb100_7, &phi_bb100_10, &phi_bb100_11, &phi_bb100_12, &phi_bb100_13, &phi_bb100_14, &phi_bb100_15, &phi_bb100_16);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 961);
    tmp194 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 962);
    tmp195 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp196 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{phi_bb100_6}, TNode<Smi>{tmp195});
    ca_.Branch(tmp196, &block111, std::vector<Node*>{phi_bb100_6, phi_bb100_7, phi_bb100_10, phi_bb100_11, phi_bb100_12, phi_bb100_13, phi_bb100_14, phi_bb100_15}, &block112, std::vector<Node*>{phi_bb100_6, phi_bb100_7, phi_bb100_10, phi_bb100_11, phi_bb100_12, phi_bb100_13, phi_bb100_14, phi_bb100_15});
  }

  TNode<Smi> phi_bb111_6;
  TNode<Smi> phi_bb111_7;
  TNode<Smi> phi_bb111_10;
  TNode<Smi> phi_bb111_11;
  TNode<Smi> phi_bb111_12;
  TNode<Smi> phi_bb111_13;
  TNode<Smi> phi_bb111_14;
  TNode<Smi> phi_bb111_15;
  TNode<Smi> tmp197;
  TNode<BoolT> tmp198;
  if (block111.is_used()) {
    ca_.Bind(&block111, &phi_bb111_6, &phi_bb111_7, &phi_bb111_10, &phi_bb111_11, &phi_bb111_12, &phi_bb111_13, &phi_bb111_14, &phi_bb111_15);
    tmp197 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp198 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{phi_bb111_7}, TNode<Smi>{tmp197});
    ca_.Goto(&block113, phi_bb111_6, phi_bb111_7, phi_bb111_10, phi_bb111_11, phi_bb111_12, phi_bb111_13, phi_bb111_14, phi_bb111_15, tmp198);
  }

  TNode<Smi> phi_bb112_6;
  TNode<Smi> phi_bb112_7;
  TNode<Smi> phi_bb112_10;
  TNode<Smi> phi_bb112_11;
  TNode<Smi> phi_bb112_12;
  TNode<Smi> phi_bb112_13;
  TNode<Smi> phi_bb112_14;
  TNode<Smi> phi_bb112_15;
  TNode<BoolT> tmp199;
  if (block112.is_used()) {
    ca_.Bind(&block112, &phi_bb112_6, &phi_bb112_7, &phi_bb112_10, &phi_bb112_11, &phi_bb112_12, &phi_bb112_13, &phi_bb112_14, &phi_bb112_15);
    tmp199 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block113, phi_bb112_6, phi_bb112_7, phi_bb112_10, phi_bb112_11, phi_bb112_12, phi_bb112_13, phi_bb112_14, phi_bb112_15, tmp199);
  }

  TNode<Smi> phi_bb113_6;
  TNode<Smi> phi_bb113_7;
  TNode<Smi> phi_bb113_10;
  TNode<Smi> phi_bb113_11;
  TNode<Smi> phi_bb113_12;
  TNode<Smi> phi_bb113_13;
  TNode<Smi> phi_bb113_14;
  TNode<Smi> phi_bb113_15;
  TNode<BoolT> phi_bb113_18;
  if (block113.is_used()) {
    ca_.Bind(&block113, &phi_bb113_6, &phi_bb113_7, &phi_bb113_10, &phi_bb113_11, &phi_bb113_12, &phi_bb113_13, &phi_bb113_14, &phi_bb113_15, &phi_bb113_18);
    ca_.Branch(phi_bb113_18, &block109, std::vector<Node*>{phi_bb113_6, phi_bb113_7, phi_bb113_10, phi_bb113_11, phi_bb113_12, phi_bb113_13, phi_bb113_14, phi_bb113_15}, &block110, std::vector<Node*>{phi_bb113_6, phi_bb113_7, phi_bb113_10, phi_bb113_11, phi_bb113_12, phi_bb113_13, phi_bb113_14, phi_bb113_15});
  }

  TNode<Smi> phi_bb110_6;
  TNode<Smi> phi_bb110_7;
  TNode<Smi> phi_bb110_10;
  TNode<Smi> phi_bb110_11;
  TNode<Smi> phi_bb110_12;
  TNode<Smi> phi_bb110_13;
  TNode<Smi> phi_bb110_14;
  TNode<Smi> phi_bb110_15;
  if (block110.is_used()) {
    ca_.Bind(&block110, &phi_bb110_6, &phi_bb110_7, &phi_bb110_10, &phi_bb110_11, &phi_bb110_12, &phi_bb110_13, &phi_bb110_14, &phi_bb110_15);
    CodeStubAssembler(state_).FailAssert("Torque assert 'lengthA > 1 && lengthB > 0' failed", "third_party/v8/builtins/array-sort.tq", 962);
  }

  TNode<Smi> phi_bb109_6;
  TNode<Smi> phi_bb109_7;
  TNode<Smi> phi_bb109_10;
  TNode<Smi> phi_bb109_11;
  TNode<Smi> phi_bb109_12;
  TNode<Smi> phi_bb109_13;
  TNode<Smi> phi_bb109_14;
  TNode<Smi> phi_bb109_15;
  TNode<Smi> tmp200;
  TNode<Smi> tmp201;
  TNode<Smi> tmp202;
  TNode<Smi> tmp203;
  TNode<IntPtrT> tmp204;
  TNode<IntPtrT> tmp205;
  TNode<IntPtrT> tmp206;
  TNode<IntPtrT> tmp207;
  TNode<Smi> tmp208;
  TNode<IntPtrT> tmp209;
  TNode<IntPtrT> tmp210;
  TNode<UintPtrT> tmp211;
  TNode<UintPtrT> tmp212;
  TNode<BoolT> tmp213;
  if (block109.is_used()) {
    ca_.Bind(&block109, &phi_bb109_6, &phi_bb109_7, &phi_bb109_10, &phi_bb109_11, &phi_bb109_12, &phi_bb109_13, &phi_bb109_14, &phi_bb109_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 964);
    tmp200 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp201 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb109_13}, TNode<Smi>{tmp200});
    tmp202 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp203 = CodeStubAssembler(state_).SmiMax(TNode<Smi>{tmp202}, TNode<Smi>{tmp201});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 965);
    tmp204 = FromConstexpr_intptr_constexpr_int31_0(state_, 40);
    CodeStubAssembler(state_).StoreReference<Smi>(CodeStubAssembler::Reference{p_sortState, tmp204}, tmp203);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 968);
    tmp205 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp206 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp207 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp208 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp207});
    tmp209 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp208});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp210 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb109_12});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp211 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp210});
    tmp212 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp209});
    tmp213 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp211}, TNode<UintPtrT>{tmp212});
    ca_.Branch(tmp213, &block118, std::vector<Node*>{phi_bb109_6, phi_bb109_7, phi_bb109_10, phi_bb109_11, phi_bb109_12, phi_bb109_14, phi_bb109_15, phi_bb109_12, phi_bb109_12}, &block119, std::vector<Node*>{phi_bb109_6, phi_bb109_7, phi_bb109_10, phi_bb109_11, phi_bb109_12, phi_bb109_14, phi_bb109_15, phi_bb109_12, phi_bb109_12});
  }

  TNode<Smi> phi_bb118_6;
  TNode<Smi> phi_bb118_7;
  TNode<Smi> phi_bb118_10;
  TNode<Smi> phi_bb118_11;
  TNode<Smi> phi_bb118_12;
  TNode<Smi> phi_bb118_14;
  TNode<Smi> phi_bb118_15;
  TNode<Smi> phi_bb118_22;
  TNode<Smi> phi_bb118_23;
  TNode<IntPtrT> tmp214;
  TNode<IntPtrT> tmp215;
  TNode<IntPtrT> tmp216;
  TNode<HeapObject> tmp217;
  TNode<IntPtrT> tmp218;
  TNode<Object> tmp219;
  TNode<Object> tmp220;
  TNode<Smi> tmp221;
  TNode<Smi> tmp222;
  TNode<Smi> tmp223;
  TNode<BoolT> tmp224;
  if (block118.is_used()) {
    ca_.Bind(&block118, &phi_bb118_6, &phi_bb118_7, &phi_bb118_10, &phi_bb118_11, &phi_bb118_12, &phi_bb118_14, &phi_bb118_15, &phi_bb118_22, &phi_bb118_23);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp214 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp215 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp210}, TNode<IntPtrT>{tmp214});
    tmp216 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp205}, TNode<IntPtrT>{tmp215});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp217, tmp218) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp216}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 968);
    tmp219 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp217, tmp218});
    tmp220 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp219});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 967);
    tmp221 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp222 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kGallopRight, p_context, p_sortState, tmp14, tmp220, phi_bb118_11, phi_bb118_6, tmp221));
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 970);
    tmp223 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp224 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{tmp222}, TNode<Smi>{tmp223});
    ca_.Branch(tmp224, &block121, std::vector<Node*>{phi_bb118_6, phi_bb118_7, phi_bb118_10, phi_bb118_11, phi_bb118_12, phi_bb118_15}, &block122, std::vector<Node*>{phi_bb118_6, phi_bb118_7, phi_bb118_10, phi_bb118_11, phi_bb118_12, phi_bb118_15});
  }

  TNode<Smi> phi_bb119_6;
  TNode<Smi> phi_bb119_7;
  TNode<Smi> phi_bb119_10;
  TNode<Smi> phi_bb119_11;
  TNode<Smi> phi_bb119_12;
  TNode<Smi> phi_bb119_14;
  TNode<Smi> phi_bb119_15;
  TNode<Smi> phi_bb119_22;
  TNode<Smi> phi_bb119_23;
  if (block119.is_used()) {
    ca_.Bind(&block119, &phi_bb119_6, &phi_bb119_7, &phi_bb119_10, &phi_bb119_11, &phi_bb119_12, &phi_bb119_14, &phi_bb119_15, &phi_bb119_22, &phi_bb119_23);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb122_6;
  TNode<Smi> phi_bb122_7;
  TNode<Smi> phi_bb122_10;
  TNode<Smi> phi_bb122_11;
  TNode<Smi> phi_bb122_12;
  TNode<Smi> phi_bb122_15;
  if (block122.is_used()) {
    ca_.Bind(&block122, &phi_bb122_6, &phi_bb122_7, &phi_bb122_10, &phi_bb122_11, &phi_bb122_12, &phi_bb122_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 970);
    CodeStubAssembler(state_).FailAssert("Torque assert 'nofWinsA >= 0' failed", "third_party/v8/builtins/array-sort.tq", 970);
  }

  TNode<Smi> phi_bb121_6;
  TNode<Smi> phi_bb121_7;
  TNode<Smi> phi_bb121_10;
  TNode<Smi> phi_bb121_11;
  TNode<Smi> phi_bb121_12;
  TNode<Smi> phi_bb121_15;
  TNode<Smi> tmp225;
  TNode<BoolT> tmp226;
  if (block121.is_used()) {
    ca_.Bind(&block121, &phi_bb121_6, &phi_bb121_7, &phi_bb121_10, &phi_bb121_11, &phi_bb121_12, &phi_bb121_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 972);
    tmp225 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp226 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{tmp222}, TNode<Smi>{tmp225});
    ca_.Branch(tmp226, &block123, std::vector<Node*>{phi_bb121_6, phi_bb121_7, phi_bb121_10, phi_bb121_11, phi_bb121_12, phi_bb121_15}, &block124, std::vector<Node*>{phi_bb121_6, phi_bb121_7, phi_bb121_10, phi_bb121_11, phi_bb121_12, phi_bb121_15});
  }

  TNode<Smi> phi_bb123_6;
  TNode<Smi> phi_bb123_7;
  TNode<Smi> phi_bb123_10;
  TNode<Smi> phi_bb123_11;
  TNode<Smi> phi_bb123_12;
  TNode<Smi> phi_bb123_15;
  TNode<Object> tmp227;
  TNode<Smi> tmp228;
  TNode<Smi> tmp229;
  TNode<Smi> tmp230;
  TNode<Smi> tmp231;
  TNode<BoolT> tmp232;
  if (block123.is_used()) {
    ca_.Bind(&block123, &phi_bb123_6, &phi_bb123_7, &phi_bb123_10, &phi_bb123_11, &phi_bb123_12, &phi_bb123_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 973);
    tmp227 = CodeStubAssembler(state_).CallBuiltin(Builtins::kCopy, p_context, tmp14, phi_bb123_11, tmp13, phi_bb123_10, tmp222);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 974);
    tmp228 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb123_10}, TNode<Smi>{tmp222});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 975);
    tmp229 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb123_11}, TNode<Smi>{tmp222});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 976);
    tmp230 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb123_6}, TNode<Smi>{tmp222});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 978);
    tmp231 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp232 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp230}, TNode<Smi>{tmp231});
    ca_.Branch(tmp232, &block125, std::vector<Node*>{phi_bb123_7, phi_bb123_12, phi_bb123_15}, &block126, std::vector<Node*>{phi_bb123_7, phi_bb123_12, phi_bb123_15});
  }

  TNode<Smi> phi_bb125_7;
  TNode<Smi> phi_bb125_12;
  TNode<Smi> phi_bb125_15;
  if (block125.is_used()) {
    ca_.Bind(&block125, &phi_bb125_7, &phi_bb125_12, &phi_bb125_15);
    ca_.Goto(&block29, tmp230, phi_bb125_7, tmp228, tmp229, phi_bb125_12);
  }

  TNode<Smi> phi_bb126_7;
  TNode<Smi> phi_bb126_12;
  TNode<Smi> phi_bb126_15;
  TNode<Smi> tmp233;
  TNode<BoolT> tmp234;
  if (block126.is_used()) {
    ca_.Bind(&block126, &phi_bb126_7, &phi_bb126_12, &phi_bb126_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 982);
    tmp233 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp234 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp230}, TNode<Smi>{tmp233});
    ca_.Branch(tmp234, &block127, std::vector<Node*>{phi_bb126_7, phi_bb126_12, phi_bb126_15}, &block128, std::vector<Node*>{phi_bb126_7, phi_bb126_12, phi_bb126_15});
  }

  TNode<Smi> phi_bb127_7;
  TNode<Smi> phi_bb127_12;
  TNode<Smi> phi_bb127_15;
  if (block127.is_used()) {
    ca_.Bind(&block127, &phi_bb127_7, &phi_bb127_12, &phi_bb127_15);
    ca_.Goto(&block31, tmp230, phi_bb127_7, tmp228, tmp229, phi_bb127_12);
  }

  TNode<Smi> phi_bb128_7;
  TNode<Smi> phi_bb128_12;
  TNode<Smi> phi_bb128_15;
  if (block128.is_used()) {
    ca_.Bind(&block128, &phi_bb128_7, &phi_bb128_12, &phi_bb128_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 972);
    ca_.Goto(&block124, tmp230, phi_bb128_7, tmp228, tmp229, phi_bb128_12, phi_bb128_15);
  }

  TNode<Smi> phi_bb124_6;
  TNode<Smi> phi_bb124_7;
  TNode<Smi> phi_bb124_10;
  TNode<Smi> phi_bb124_11;
  TNode<Smi> phi_bb124_12;
  TNode<Smi> phi_bb124_15;
  TNode<IntPtrT> tmp235;
  TNode<IntPtrT> tmp236;
  TNode<IntPtrT> tmp237;
  TNode<Smi> tmp238;
  TNode<IntPtrT> tmp239;
  TNode<Smi> tmp240;
  TNode<Smi> tmp241;
  TNode<IntPtrT> tmp242;
  TNode<UintPtrT> tmp243;
  TNode<UintPtrT> tmp244;
  TNode<BoolT> tmp245;
  if (block124.is_used()) {
    ca_.Bind(&block124, &phi_bb124_6, &phi_bb124_7, &phi_bb124_10, &phi_bb124_11, &phi_bb124_12, &phi_bb124_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 984);
    tmp235 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp236 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp237 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp238 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp237});
    tmp239 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp238});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 984);
    tmp240 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp241 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb124_10}, TNode<Smi>{tmp240});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp242 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb124_10});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp243 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp242});
    tmp244 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp239});
    tmp245 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp243}, TNode<UintPtrT>{tmp244});
    ca_.Branch(tmp245, &block133, std::vector<Node*>{phi_bb124_6, phi_bb124_7, phi_bb124_11, phi_bb124_12, phi_bb124_15, phi_bb124_10, phi_bb124_10}, &block134, std::vector<Node*>{phi_bb124_6, phi_bb124_7, phi_bb124_11, phi_bb124_12, phi_bb124_15, phi_bb124_10, phi_bb124_10});
  }

  TNode<Smi> phi_bb133_6;
  TNode<Smi> phi_bb133_7;
  TNode<Smi> phi_bb133_11;
  TNode<Smi> phi_bb133_12;
  TNode<Smi> phi_bb133_15;
  TNode<Smi> phi_bb133_21;
  TNode<Smi> phi_bb133_22;
  TNode<IntPtrT> tmp246;
  TNode<IntPtrT> tmp247;
  TNode<IntPtrT> tmp248;
  TNode<HeapObject> tmp249;
  TNode<IntPtrT> tmp250;
  TNode<IntPtrT> tmp251;
  TNode<IntPtrT> tmp252;
  TNode<IntPtrT> tmp253;
  TNode<Smi> tmp254;
  TNode<IntPtrT> tmp255;
  TNode<Smi> tmp256;
  TNode<Smi> tmp257;
  TNode<IntPtrT> tmp258;
  TNode<UintPtrT> tmp259;
  TNode<UintPtrT> tmp260;
  TNode<BoolT> tmp261;
  if (block133.is_used()) {
    ca_.Bind(&block133, &phi_bb133_6, &phi_bb133_7, &phi_bb133_11, &phi_bb133_12, &phi_bb133_15, &phi_bb133_21, &phi_bb133_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp246 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp247 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp242}, TNode<IntPtrT>{tmp246});
    tmp248 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp235}, TNode<IntPtrT>{tmp247});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp249, tmp250) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp248}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 984);
    tmp251 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp252 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp253 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp254 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp253});
    tmp255 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp254});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 984);
    tmp256 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp257 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb133_12}, TNode<Smi>{tmp256});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp258 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb133_12});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp259 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp258});
    tmp260 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp255});
    tmp261 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp259}, TNode<UintPtrT>{tmp260});
    ca_.Branch(tmp261, &block140, std::vector<Node*>{phi_bb133_6, phi_bb133_7, phi_bb133_11, phi_bb133_15, phi_bb133_21, phi_bb133_22, phi_bb133_12, phi_bb133_12}, &block141, std::vector<Node*>{phi_bb133_6, phi_bb133_7, phi_bb133_11, phi_bb133_15, phi_bb133_21, phi_bb133_22, phi_bb133_12, phi_bb133_12});
  }

  TNode<Smi> phi_bb134_6;
  TNode<Smi> phi_bb134_7;
  TNode<Smi> phi_bb134_11;
  TNode<Smi> phi_bb134_12;
  TNode<Smi> phi_bb134_15;
  TNode<Smi> phi_bb134_21;
  TNode<Smi> phi_bb134_22;
  if (block134.is_used()) {
    ca_.Bind(&block134, &phi_bb134_6, &phi_bb134_7, &phi_bb134_11, &phi_bb134_12, &phi_bb134_15, &phi_bb134_21, &phi_bb134_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb140_6;
  TNode<Smi> phi_bb140_7;
  TNode<Smi> phi_bb140_11;
  TNode<Smi> phi_bb140_15;
  TNode<Smi> phi_bb140_21;
  TNode<Smi> phi_bb140_22;
  TNode<Smi> phi_bb140_29;
  TNode<Smi> phi_bb140_30;
  TNode<IntPtrT> tmp262;
  TNode<IntPtrT> tmp263;
  TNode<IntPtrT> tmp264;
  TNode<HeapObject> tmp265;
  TNode<IntPtrT> tmp266;
  TNode<Object> tmp267;
  TNode<Smi> tmp268;
  TNode<Smi> tmp269;
  TNode<Smi> tmp270;
  TNode<BoolT> tmp271;
  if (block140.is_used()) {
    ca_.Bind(&block140, &phi_bb140_6, &phi_bb140_7, &phi_bb140_11, &phi_bb140_15, &phi_bb140_21, &phi_bb140_22, &phi_bb140_29, &phi_bb140_30);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp262 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp263 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp258}, TNode<IntPtrT>{tmp262});
    tmp264 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp251}, TNode<IntPtrT>{tmp263});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp265, tmp266) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp264}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 984);
    tmp267 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp265, tmp266});
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp249, tmp250}, tmp267);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 985);
    tmp268 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp269 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb140_7}, TNode<Smi>{tmp268});
    tmp270 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp271 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp269}, TNode<Smi>{tmp270});
    ca_.Branch(tmp271, &block143, std::vector<Node*>{phi_bb140_6, phi_bb140_11, phi_bb140_15}, &block144, std::vector<Node*>{phi_bb140_6, phi_bb140_11, phi_bb140_15});
  }

  TNode<Smi> phi_bb141_6;
  TNode<Smi> phi_bb141_7;
  TNode<Smi> phi_bb141_11;
  TNode<Smi> phi_bb141_15;
  TNode<Smi> phi_bb141_21;
  TNode<Smi> phi_bb141_22;
  TNode<Smi> phi_bb141_29;
  TNode<Smi> phi_bb141_30;
  if (block141.is_used()) {
    ca_.Bind(&block141, &phi_bb141_6, &phi_bb141_7, &phi_bb141_11, &phi_bb141_15, &phi_bb141_21, &phi_bb141_22, &phi_bb141_29, &phi_bb141_30);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb143_6;
  TNode<Smi> phi_bb143_11;
  TNode<Smi> phi_bb143_15;
  if (block143.is_used()) {
    ca_.Bind(&block143, &phi_bb143_6, &phi_bb143_11, &phi_bb143_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 985);
    ca_.Goto(&block31, phi_bb143_6, tmp269, tmp241, phi_bb143_11, tmp257);
  }

  TNode<Smi> phi_bb144_6;
  TNode<Smi> phi_bb144_11;
  TNode<Smi> phi_bb144_15;
  TNode<IntPtrT> tmp272;
  TNode<IntPtrT> tmp273;
  TNode<IntPtrT> tmp274;
  TNode<Smi> tmp275;
  TNode<IntPtrT> tmp276;
  TNode<IntPtrT> tmp277;
  TNode<UintPtrT> tmp278;
  TNode<UintPtrT> tmp279;
  TNode<BoolT> tmp280;
  if (block144.is_used()) {
    ca_.Bind(&block144, &phi_bb144_6, &phi_bb144_11, &phi_bb144_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 988);
    tmp272 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp273 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp274 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp275 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp14, tmp274});
    tmp276 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp275});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp277 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb144_11});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp278 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp277});
    tmp279 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp276});
    tmp280 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp278}, TNode<UintPtrT>{tmp279});
    ca_.Branch(tmp280, &block149, std::vector<Node*>{phi_bb144_6, phi_bb144_11, phi_bb144_15, phi_bb144_11, phi_bb144_11}, &block150, std::vector<Node*>{phi_bb144_6, phi_bb144_11, phi_bb144_15, phi_bb144_11, phi_bb144_11});
  }

  TNode<Smi> phi_bb149_6;
  TNode<Smi> phi_bb149_11;
  TNode<Smi> phi_bb149_15;
  TNode<Smi> phi_bb149_22;
  TNode<Smi> phi_bb149_23;
  TNode<IntPtrT> tmp281;
  TNode<IntPtrT> tmp282;
  TNode<IntPtrT> tmp283;
  TNode<HeapObject> tmp284;
  TNode<IntPtrT> tmp285;
  TNode<Object> tmp286;
  TNode<Object> tmp287;
  TNode<Smi> tmp288;
  TNode<Smi> tmp289;
  TNode<Smi> tmp290;
  TNode<BoolT> tmp291;
  if (block149.is_used()) {
    ca_.Bind(&block149, &phi_bb149_6, &phi_bb149_11, &phi_bb149_15, &phi_bb149_22, &phi_bb149_23);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp281 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp282 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp277}, TNode<IntPtrT>{tmp281});
    tmp283 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp272}, TNode<IntPtrT>{tmp282});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp284, tmp285) = NewReference_Object_0(state_, TNode<HeapObject>{tmp14}, TNode<IntPtrT>{tmp283}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 988);
    tmp286 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp284, tmp285});
    tmp287 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp286});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 987);
    tmp288 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp289 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kGallopLeft, p_context, p_sortState, tmp13, tmp287, tmp257, tmp269, tmp288));
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 990);
    tmp290 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp291 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{tmp289}, TNode<Smi>{tmp290});
    ca_.Branch(tmp291, &block152, std::vector<Node*>{phi_bb149_6, phi_bb149_11}, &block153, std::vector<Node*>{phi_bb149_6, phi_bb149_11});
  }

  TNode<Smi> phi_bb150_6;
  TNode<Smi> phi_bb150_11;
  TNode<Smi> phi_bb150_15;
  TNode<Smi> phi_bb150_22;
  TNode<Smi> phi_bb150_23;
  if (block150.is_used()) {
    ca_.Bind(&block150, &phi_bb150_6, &phi_bb150_11, &phi_bb150_15, &phi_bb150_22, &phi_bb150_23);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb153_6;
  TNode<Smi> phi_bb153_11;
  if (block153.is_used()) {
    ca_.Bind(&block153, &phi_bb153_6, &phi_bb153_11);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 990);
    CodeStubAssembler(state_).FailAssert("Torque assert 'nofWinsB >= 0' failed", "third_party/v8/builtins/array-sort.tq", 990);
  }

  TNode<Smi> phi_bb152_6;
  TNode<Smi> phi_bb152_11;
  TNode<Smi> tmp292;
  TNode<BoolT> tmp293;
  if (block152.is_used()) {
    ca_.Bind(&block152, &phi_bb152_6, &phi_bb152_11);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 991);
    tmp292 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp293 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{tmp289}, TNode<Smi>{tmp292});
    ca_.Branch(tmp293, &block154, std::vector<Node*>{phi_bb152_6, phi_bb152_11}, &block155, std::vector<Node*>{phi_bb152_6, tmp269, tmp241, phi_bb152_11, tmp257});
  }

  TNode<Smi> phi_bb154_6;
  TNode<Smi> phi_bb154_11;
  TNode<Object> tmp294;
  TNode<Smi> tmp295;
  TNode<Smi> tmp296;
  TNode<Smi> tmp297;
  TNode<Smi> tmp298;
  TNode<BoolT> tmp299;
  if (block154.is_used()) {
    ca_.Bind(&block154, &phi_bb154_6, &phi_bb154_11);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 992);
    tmp294 = CodeStubAssembler(state_).CallBuiltin(Builtins::kCopy, p_context, tmp13, tmp257, tmp13, tmp241, tmp289);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 994);
    tmp295 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp241}, TNode<Smi>{tmp289});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 995);
    tmp296 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp257}, TNode<Smi>{tmp289});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 996);
    tmp297 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp269}, TNode<Smi>{tmp289});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 998);
    tmp298 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp299 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp297}, TNode<Smi>{tmp298});
    ca_.Branch(tmp299, &block156, std::vector<Node*>{phi_bb154_6, phi_bb154_11}, &block157, std::vector<Node*>{phi_bb154_6, phi_bb154_11});
  }

  TNode<Smi> phi_bb156_6;
  TNode<Smi> phi_bb156_11;
  if (block156.is_used()) {
    ca_.Bind(&block156, &phi_bb156_6, &phi_bb156_11);
    ca_.Goto(&block31, phi_bb156_6, tmp297, tmp295, phi_bb156_11, tmp296);
  }

  TNode<Smi> phi_bb157_6;
  TNode<Smi> phi_bb157_11;
  if (block157.is_used()) {
    ca_.Bind(&block157, &phi_bb157_6, &phi_bb157_11);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 991);
    ca_.Goto(&block155, phi_bb157_6, tmp297, tmp295, phi_bb157_11, tmp296);
  }

  TNode<Smi> phi_bb155_6;
  TNode<Smi> phi_bb155_7;
  TNode<Smi> phi_bb155_10;
  TNode<Smi> phi_bb155_11;
  TNode<Smi> phi_bb155_12;
  TNode<IntPtrT> tmp300;
  TNode<IntPtrT> tmp301;
  TNode<IntPtrT> tmp302;
  TNode<Smi> tmp303;
  TNode<IntPtrT> tmp304;
  TNode<Smi> tmp305;
  TNode<Smi> tmp306;
  TNode<IntPtrT> tmp307;
  TNode<UintPtrT> tmp308;
  TNode<UintPtrT> tmp309;
  TNode<BoolT> tmp310;
  if (block155.is_used()) {
    ca_.Bind(&block155, &phi_bb155_6, &phi_bb155_7, &phi_bb155_10, &phi_bb155_11, &phi_bb155_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1000);
    tmp300 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp301 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp302 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp303 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp302});
    tmp304 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp303});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1000);
    tmp305 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp306 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb155_10}, TNode<Smi>{tmp305});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp307 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb155_10});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp308 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp307});
    tmp309 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp304});
    tmp310 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp308}, TNode<UintPtrT>{tmp309});
    ca_.Branch(tmp310, &block162, std::vector<Node*>{phi_bb155_6, phi_bb155_7, phi_bb155_11, phi_bb155_12, phi_bb155_10, phi_bb155_10}, &block163, std::vector<Node*>{phi_bb155_6, phi_bb155_7, phi_bb155_11, phi_bb155_12, phi_bb155_10, phi_bb155_10});
  }

  TNode<Smi> phi_bb162_6;
  TNode<Smi> phi_bb162_7;
  TNode<Smi> phi_bb162_11;
  TNode<Smi> phi_bb162_12;
  TNode<Smi> phi_bb162_21;
  TNode<Smi> phi_bb162_22;
  TNode<IntPtrT> tmp311;
  TNode<IntPtrT> tmp312;
  TNode<IntPtrT> tmp313;
  TNode<HeapObject> tmp314;
  TNode<IntPtrT> tmp315;
  TNode<IntPtrT> tmp316;
  TNode<IntPtrT> tmp317;
  TNode<IntPtrT> tmp318;
  TNode<Smi> tmp319;
  TNode<IntPtrT> tmp320;
  TNode<Smi> tmp321;
  TNode<Smi> tmp322;
  TNode<IntPtrT> tmp323;
  TNode<UintPtrT> tmp324;
  TNode<UintPtrT> tmp325;
  TNode<BoolT> tmp326;
  if (block162.is_used()) {
    ca_.Bind(&block162, &phi_bb162_6, &phi_bb162_7, &phi_bb162_11, &phi_bb162_12, &phi_bb162_21, &phi_bb162_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp311 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp312 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp307}, TNode<IntPtrT>{tmp311});
    tmp313 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp300}, TNode<IntPtrT>{tmp312});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp314, tmp315) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp313}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1000);
    tmp316 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp317 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp318 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp319 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp14, tmp318});
    tmp320 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp319});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1000);
    tmp321 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp322 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb162_11}, TNode<Smi>{tmp321});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp323 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb162_11});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp324 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp323});
    tmp325 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp320});
    tmp326 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp324}, TNode<UintPtrT>{tmp325});
    ca_.Branch(tmp326, &block169, std::vector<Node*>{phi_bb162_6, phi_bb162_7, phi_bb162_12, phi_bb162_21, phi_bb162_22, phi_bb162_11, phi_bb162_11}, &block170, std::vector<Node*>{phi_bb162_6, phi_bb162_7, phi_bb162_12, phi_bb162_21, phi_bb162_22, phi_bb162_11, phi_bb162_11});
  }

  TNode<Smi> phi_bb163_6;
  TNode<Smi> phi_bb163_7;
  TNode<Smi> phi_bb163_11;
  TNode<Smi> phi_bb163_12;
  TNode<Smi> phi_bb163_21;
  TNode<Smi> phi_bb163_22;
  if (block163.is_used()) {
    ca_.Bind(&block163, &phi_bb163_6, &phi_bb163_7, &phi_bb163_11, &phi_bb163_12, &phi_bb163_21, &phi_bb163_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb169_6;
  TNode<Smi> phi_bb169_7;
  TNode<Smi> phi_bb169_12;
  TNode<Smi> phi_bb169_21;
  TNode<Smi> phi_bb169_22;
  TNode<Smi> phi_bb169_29;
  TNode<Smi> phi_bb169_30;
  TNode<IntPtrT> tmp327;
  TNode<IntPtrT> tmp328;
  TNode<IntPtrT> tmp329;
  TNode<HeapObject> tmp330;
  TNode<IntPtrT> tmp331;
  TNode<Object> tmp332;
  TNode<Smi> tmp333;
  TNode<Smi> tmp334;
  TNode<Smi> tmp335;
  TNode<BoolT> tmp336;
  if (block169.is_used()) {
    ca_.Bind(&block169, &phi_bb169_6, &phi_bb169_7, &phi_bb169_12, &phi_bb169_21, &phi_bb169_22, &phi_bb169_29, &phi_bb169_30);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp327 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp328 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp323}, TNode<IntPtrT>{tmp327});
    tmp329 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp316}, TNode<IntPtrT>{tmp328});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp330, tmp331) = NewReference_Object_0(state_, TNode<HeapObject>{tmp14}, TNode<IntPtrT>{tmp329}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1000);
    tmp332 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp330, tmp331});
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp314, tmp315}, tmp332);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1001);
    tmp333 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp334 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb169_6}, TNode<Smi>{tmp333});
    tmp335 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp336 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp334}, TNode<Smi>{tmp335});
    ca_.Branch(tmp336, &block172, std::vector<Node*>{phi_bb169_7, phi_bb169_12}, &block173, std::vector<Node*>{phi_bb169_7, phi_bb169_12});
  }

  TNode<Smi> phi_bb170_6;
  TNode<Smi> phi_bb170_7;
  TNode<Smi> phi_bb170_12;
  TNode<Smi> phi_bb170_21;
  TNode<Smi> phi_bb170_22;
  TNode<Smi> phi_bb170_29;
  TNode<Smi> phi_bb170_30;
  if (block170.is_used()) {
    ca_.Bind(&block170, &phi_bb170_6, &phi_bb170_7, &phi_bb170_12, &phi_bb170_21, &phi_bb170_22, &phi_bb170_29, &phi_bb170_30);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb172_7;
  TNode<Smi> phi_bb172_12;
  if (block172.is_used()) {
    ca_.Bind(&block172, &phi_bb172_7, &phi_bb172_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1001);
    ca_.Goto(&block29, tmp334, phi_bb172_7, tmp306, tmp322, phi_bb172_12);
  }

  TNode<Smi> phi_bb173_7;
  TNode<Smi> phi_bb173_12;
  if (block173.is_used()) {
    ca_.Bind(&block173, &phi_bb173_7, &phi_bb173_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 959);
    ca_.Goto(&block102, tmp334, phi_bb173_7, tmp306, tmp322, phi_bb173_12, tmp203, tmp222, tmp289, tmp194);
  }

  TNode<Smi> phi_bb101_6;
  TNode<Smi> phi_bb101_7;
  TNode<Smi> phi_bb101_10;
  TNode<Smi> phi_bb101_11;
  TNode<Smi> phi_bb101_12;
  TNode<Smi> phi_bb101_13;
  TNode<Smi> phi_bb101_14;
  TNode<Smi> phi_bb101_15;
  TNode<BoolT> phi_bb101_16;
  TNode<Smi> tmp337;
  TNode<Smi> tmp338;
  TNode<IntPtrT> tmp339;
  if (block101.is_used()) {
    ca_.Bind(&block101, &phi_bb101_6, &phi_bb101_7, &phi_bb101_10, &phi_bb101_11, &phi_bb101_12, &phi_bb101_13, &phi_bb101_14, &phi_bb101_15, &phi_bb101_16);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1003);
    tmp337 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp338 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb101_13}, TNode<Smi>{tmp337});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1004);
    tmp339 = FromConstexpr_intptr_constexpr_int31_0(state_, 40);
    CodeStubAssembler(state_).StoreReference<Smi>(CodeStubAssembler::Reference{p_sortState, tmp339}, tmp338);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 918);
    ca_.Goto(&block38, phi_bb101_6, phi_bb101_7, phi_bb101_10, phi_bb101_11, phi_bb101_12, tmp338);
  }

  TNode<Smi> phi_bb37_6;
  TNode<Smi> phi_bb37_7;
  TNode<Smi> phi_bb37_10;
  TNode<Smi> phi_bb37_11;
  TNode<Smi> phi_bb37_12;
  TNode<Smi> phi_bb37_13;
  if (block37.is_used()) {
    ca_.Bind(&block37, &phi_bb37_6, &phi_bb37_7, &phi_bb37_10, &phi_bb37_11, &phi_bb37_12, &phi_bb37_13);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1006);
    ca_.Goto(&block30, phi_bb37_6, phi_bb37_7, phi_bb37_10, phi_bb37_11, phi_bb37_12);
  }

  TNode<Smi> phi_bb31_6;
  TNode<Smi> phi_bb31_7;
  TNode<Smi> phi_bb31_10;
  TNode<Smi> phi_bb31_11;
  TNode<Smi> phi_bb31_12;
  TNode<Smi> tmp340;
  TNode<BoolT> tmp341;
  if (block31.is_used()) {
    ca_.Bind(&block31, &phi_bb31_6, &phi_bb31_7, &phi_bb31_10, &phi_bb31_11, &phi_bb31_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1007);
    tmp340 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp341 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{phi_bb31_6}, TNode<Smi>{tmp340});
    ca_.Branch(tmp341, &block174, std::vector<Node*>{phi_bb31_6, phi_bb31_7, phi_bb31_10, phi_bb31_11, phi_bb31_12}, &block175, std::vector<Node*>{phi_bb31_6, phi_bb31_7, phi_bb31_10, phi_bb31_11, phi_bb31_12});
  }

  TNode<Smi> phi_bb174_6;
  TNode<Smi> phi_bb174_7;
  TNode<Smi> phi_bb174_10;
  TNode<Smi> phi_bb174_11;
  TNode<Smi> phi_bb174_12;
  TNode<Object> tmp342;
  if (block174.is_used()) {
    ca_.Bind(&block174, &phi_bb174_6, &phi_bb174_7, &phi_bb174_10, &phi_bb174_11, &phi_bb174_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1008);
    tmp342 = CodeStubAssembler(state_).CallBuiltin(Builtins::kCopy, p_context, tmp14, phi_bb174_11, tmp13, phi_bb174_10, phi_bb174_6);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1007);
    ca_.Goto(&block175, phi_bb174_6, phi_bb174_7, phi_bb174_10, phi_bb174_11, phi_bb174_12);
  }

  TNode<Smi> phi_bb175_6;
  TNode<Smi> phi_bb175_7;
  TNode<Smi> phi_bb175_10;
  TNode<Smi> phi_bb175_11;
  TNode<Smi> phi_bb175_12;
  if (block175.is_used()) {
    ca_.Bind(&block175, &phi_bb175_6, &phi_bb175_7, &phi_bb175_10, &phi_bb175_11, &phi_bb175_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 911);
    ca_.Goto(&block30, phi_bb175_6, phi_bb175_7, phi_bb175_10, phi_bb175_11, phi_bb175_12);
  }

  TNode<Smi> phi_bb30_6;
  TNode<Smi> phi_bb30_7;
  TNode<Smi> phi_bb30_10;
  TNode<Smi> phi_bb30_11;
  TNode<Smi> phi_bb30_12;
  if (block30.is_used()) {
    ca_.Bind(&block30, &phi_bb30_6, &phi_bb30_7, &phi_bb30_10, &phi_bb30_11, &phi_bb30_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1010);
    ca_.Goto(&block28, phi_bb30_6, phi_bb30_7, phi_bb30_10, phi_bb30_11, phi_bb30_12);
  }

  TNode<Smi> phi_bb29_6;
  TNode<Smi> phi_bb29_7;
  TNode<Smi> phi_bb29_10;
  TNode<Smi> phi_bb29_11;
  TNode<Smi> phi_bb29_12;
  TNode<Smi> tmp343;
  TNode<BoolT> tmp344;
  if (block29.is_used()) {
    ca_.Bind(&block29, &phi_bb29_6, &phi_bb29_7, &phi_bb29_10, &phi_bb29_11, &phi_bb29_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1011);
    tmp343 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp344 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{phi_bb29_6}, TNode<Smi>{tmp343});
    ca_.Branch(tmp344, &block178, std::vector<Node*>{phi_bb29_6, phi_bb29_7, phi_bb29_10, phi_bb29_11, phi_bb29_12}, &block179, std::vector<Node*>{phi_bb29_6, phi_bb29_7, phi_bb29_10, phi_bb29_11, phi_bb29_12});
  }

  TNode<Smi> phi_bb178_6;
  TNode<Smi> phi_bb178_7;
  TNode<Smi> phi_bb178_10;
  TNode<Smi> phi_bb178_11;
  TNode<Smi> phi_bb178_12;
  TNode<Smi> tmp345;
  TNode<BoolT> tmp346;
  if (block178.is_used()) {
    ca_.Bind(&block178, &phi_bb178_6, &phi_bb178_7, &phi_bb178_10, &phi_bb178_11, &phi_bb178_12);
    tmp345 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp346 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{phi_bb178_7}, TNode<Smi>{tmp345});
    ca_.Goto(&block180, phi_bb178_6, phi_bb178_7, phi_bb178_10, phi_bb178_11, phi_bb178_12, tmp346);
  }

  TNode<Smi> phi_bb179_6;
  TNode<Smi> phi_bb179_7;
  TNode<Smi> phi_bb179_10;
  TNode<Smi> phi_bb179_11;
  TNode<Smi> phi_bb179_12;
  TNode<BoolT> tmp347;
  if (block179.is_used()) {
    ca_.Bind(&block179, &phi_bb179_6, &phi_bb179_7, &phi_bb179_10, &phi_bb179_11, &phi_bb179_12);
    tmp347 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block180, phi_bb179_6, phi_bb179_7, phi_bb179_10, phi_bb179_11, phi_bb179_12, tmp347);
  }

  TNode<Smi> phi_bb180_6;
  TNode<Smi> phi_bb180_7;
  TNode<Smi> phi_bb180_10;
  TNode<Smi> phi_bb180_11;
  TNode<Smi> phi_bb180_12;
  TNode<BoolT> phi_bb180_14;
  if (block180.is_used()) {
    ca_.Bind(&block180, &phi_bb180_6, &phi_bb180_7, &phi_bb180_10, &phi_bb180_11, &phi_bb180_12, &phi_bb180_14);
    ca_.Branch(phi_bb180_14, &block176, std::vector<Node*>{phi_bb180_6, phi_bb180_7, phi_bb180_10, phi_bb180_11, phi_bb180_12}, &block177, std::vector<Node*>{phi_bb180_6, phi_bb180_7, phi_bb180_10, phi_bb180_11, phi_bb180_12});
  }

  TNode<Smi> phi_bb177_6;
  TNode<Smi> phi_bb177_7;
  TNode<Smi> phi_bb177_10;
  TNode<Smi> phi_bb177_11;
  TNode<Smi> phi_bb177_12;
  if (block177.is_used()) {
    ca_.Bind(&block177, &phi_bb177_6, &phi_bb177_7, &phi_bb177_10, &phi_bb177_11, &phi_bb177_12);
    CodeStubAssembler(state_).FailAssert("Torque assert 'lengthA == 1 && lengthB > 0' failed", "third_party/v8/builtins/array-sort.tq", 1011);
  }

  TNode<Smi> phi_bb176_6;
  TNode<Smi> phi_bb176_7;
  TNode<Smi> phi_bb176_10;
  TNode<Smi> phi_bb176_11;
  TNode<Smi> phi_bb176_12;
  TNode<Object> tmp348;
  TNode<IntPtrT> tmp349;
  TNode<IntPtrT> tmp350;
  TNode<IntPtrT> tmp351;
  TNode<Smi> tmp352;
  TNode<IntPtrT> tmp353;
  TNode<Smi> tmp354;
  TNode<IntPtrT> tmp355;
  TNode<UintPtrT> tmp356;
  TNode<UintPtrT> tmp357;
  TNode<BoolT> tmp358;
  if (block176.is_used()) {
    ca_.Bind(&block176, &phi_bb176_6, &phi_bb176_7, &phi_bb176_10, &phi_bb176_11, &phi_bb176_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1013);
    tmp348 = CodeStubAssembler(state_).CallBuiltin(Builtins::kCopy, p_context, tmp13, phi_bb176_12, tmp13, phi_bb176_10, phi_bb176_7);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1014);
    tmp349 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp350 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp351 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp352 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp351});
    tmp353 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp352});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1014);
    tmp354 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb176_10}, TNode<Smi>{phi_bb176_7});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp355 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp354});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp356 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp355});
    tmp357 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp353});
    tmp358 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp356}, TNode<UintPtrT>{tmp357});
    ca_.Branch(tmp358, &block185, std::vector<Node*>{phi_bb176_6, phi_bb176_7, phi_bb176_10, phi_bb176_11, phi_bb176_12}, &block186, std::vector<Node*>{phi_bb176_6, phi_bb176_7, phi_bb176_10, phi_bb176_11, phi_bb176_12});
  }

  TNode<Smi> phi_bb185_6;
  TNode<Smi> phi_bb185_7;
  TNode<Smi> phi_bb185_10;
  TNode<Smi> phi_bb185_11;
  TNode<Smi> phi_bb185_12;
  TNode<IntPtrT> tmp359;
  TNode<IntPtrT> tmp360;
  TNode<IntPtrT> tmp361;
  TNode<HeapObject> tmp362;
  TNode<IntPtrT> tmp363;
  TNode<IntPtrT> tmp364;
  TNode<IntPtrT> tmp365;
  TNode<IntPtrT> tmp366;
  TNode<Smi> tmp367;
  TNode<IntPtrT> tmp368;
  TNode<IntPtrT> tmp369;
  TNode<UintPtrT> tmp370;
  TNode<UintPtrT> tmp371;
  TNode<BoolT> tmp372;
  if (block185.is_used()) {
    ca_.Bind(&block185, &phi_bb185_6, &phi_bb185_7, &phi_bb185_10, &phi_bb185_11, &phi_bb185_12);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp359 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp360 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp355}, TNode<IntPtrT>{tmp359});
    tmp361 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp349}, TNode<IntPtrT>{tmp360});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp362, tmp363) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp361}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1014);
    tmp364 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp365 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp366 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp367 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp14, tmp366});
    tmp368 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp367});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp369 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb185_11});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp370 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp369});
    tmp371 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp368});
    tmp372 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp370}, TNode<UintPtrT>{tmp371});
    ca_.Branch(tmp372, &block192, std::vector<Node*>{phi_bb185_6, phi_bb185_7, phi_bb185_10, phi_bb185_11, phi_bb185_12, phi_bb185_11, phi_bb185_11}, &block193, std::vector<Node*>{phi_bb185_6, phi_bb185_7, phi_bb185_10, phi_bb185_11, phi_bb185_12, phi_bb185_11, phi_bb185_11});
  }

  TNode<Smi> phi_bb186_6;
  TNode<Smi> phi_bb186_7;
  TNode<Smi> phi_bb186_10;
  TNode<Smi> phi_bb186_11;
  TNode<Smi> phi_bb186_12;
  if (block186.is_used()) {
    ca_.Bind(&block186, &phi_bb186_6, &phi_bb186_7, &phi_bb186_10, &phi_bb186_11, &phi_bb186_12);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb192_6;
  TNode<Smi> phi_bb192_7;
  TNode<Smi> phi_bb192_10;
  TNode<Smi> phi_bb192_11;
  TNode<Smi> phi_bb192_12;
  TNode<Smi> phi_bb192_25;
  TNode<Smi> phi_bb192_26;
  TNode<IntPtrT> tmp373;
  TNode<IntPtrT> tmp374;
  TNode<IntPtrT> tmp375;
  TNode<HeapObject> tmp376;
  TNode<IntPtrT> tmp377;
  TNode<Object> tmp378;
  if (block192.is_used()) {
    ca_.Bind(&block192, &phi_bb192_6, &phi_bb192_7, &phi_bb192_10, &phi_bb192_11, &phi_bb192_12, &phi_bb192_25, &phi_bb192_26);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp373 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp374 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp369}, TNode<IntPtrT>{tmp373});
    tmp375 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp364}, TNode<IntPtrT>{tmp374});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp376, tmp377) = NewReference_Object_0(state_, TNode<HeapObject>{tmp14}, TNode<IntPtrT>{tmp375}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1014);
    tmp378 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp376, tmp377});
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp362, tmp363}, tmp378);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 911);
    ca_.Goto(&block28, phi_bb192_6, phi_bb192_7, phi_bb192_10, phi_bb192_11, phi_bb192_12);
  }

  TNode<Smi> phi_bb193_6;
  TNode<Smi> phi_bb193_7;
  TNode<Smi> phi_bb193_10;
  TNode<Smi> phi_bb193_11;
  TNode<Smi> phi_bb193_12;
  TNode<Smi> phi_bb193_25;
  TNode<Smi> phi_bb193_26;
  if (block193.is_used()) {
    ca_.Bind(&block193, &phi_bb193_6, &phi_bb193_7, &phi_bb193_10, &phi_bb193_11, &phi_bb193_12, &phi_bb193_25, &phi_bb193_26);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb28_6;
  TNode<Smi> phi_bb28_7;
  TNode<Smi> phi_bb28_10;
  TNode<Smi> phi_bb28_11;
  TNode<Smi> phi_bb28_12;
  if (block28.is_used()) {
    ca_.Bind(&block28, &phi_bb28_6, &phi_bb28_7, &phi_bb28_10, &phi_bb28_11, &phi_bb28_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 892);
    ca_.Goto(&block195);
  }

    ca_.Bind(&block195);
}

void MergeHigh_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<SortState> p_sortState, TNode<Smi> p_baseA, TNode<Smi> p_lengthAArg, TNode<Smi> p_baseB, TNode<Smi> p_lengthBArg) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block18(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block19(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block25(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block26(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block32(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block33(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block34(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block35(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi> block38(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi> block36(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block41(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block39(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block44(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block45(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT> block46(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block43(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block42(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block51(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block52(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block58(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block59(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block61(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block68(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block69(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block75(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block76(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block78(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block79(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block80(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block81(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block62(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block86(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block87(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block93(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block94(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block96(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block97(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block98(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block99(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block63(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block40(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT> block102(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT> block103(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT> block104(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT, BoolT> block105(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT> block106(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT> block107(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT, BoolT> block108(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT> block100(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block111(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block112(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT> block113(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block110(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block109(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block118(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block119(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi> block122(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi> block121(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi> block123(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block125(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block126(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi> block124(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi> block131(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi> block132(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block138(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi> block139(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block141(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block142(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block147(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block148(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block151(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block150(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block152(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block154(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block155(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block156(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block157(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block153(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi> block162(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi> block163(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi> block169(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi> block170(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block172(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block173(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi, Smi, Smi, BoolT> block101(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, Smi> block37(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block31(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block174(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block177(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block176(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block175(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block30(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block29(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block180(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block181(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi, BoolT> block182(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block179(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block178(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block187(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block188(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block194(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block195(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi, Smi> block28(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block197(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Smi> tmp0;
  TNode<BoolT> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1024);
    tmp0 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp1 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{tmp0}, TNode<Smi>{p_lengthAArg});
    ca_.Branch(tmp1, &block4, std::vector<Node*>{}, &block5, std::vector<Node*>{});
  }

  TNode<Smi> tmp2;
  TNode<BoolT> tmp3;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    tmp2 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp3 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{tmp2}, TNode<Smi>{p_lengthBArg});
    ca_.Goto(&block6, tmp3);
  }

  TNode<BoolT> tmp4;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    tmp4 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block6, tmp4);
  }

  TNode<BoolT> phi_bb6_7;
  if (block6.is_used()) {
    ca_.Bind(&block6, &phi_bb6_7);
    ca_.Branch(phi_bb6_7, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    CodeStubAssembler(state_).FailAssert("Torque assert '0 < lengthAArg && 0 < lengthBArg' failed", "third_party/v8/builtins/array-sort.tq", 1024);
  }

  TNode<Smi> tmp5;
  TNode<BoolT> tmp6;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1025);
    tmp5 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp6 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{tmp5}, TNode<Smi>{p_baseA});
    ca_.Branch(tmp6, &block9, std::vector<Node*>{}, &block10, std::vector<Node*>{});
  }

  TNode<Smi> tmp7;
  TNode<BoolT> tmp8;
  if (block9.is_used()) {
    ca_.Bind(&block9);
    tmp7 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp8 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{tmp7}, TNode<Smi>{p_baseB});
    ca_.Goto(&block11, tmp8);
  }

  TNode<BoolT> tmp9;
  if (block10.is_used()) {
    ca_.Bind(&block10);
    tmp9 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block11, tmp9);
  }

  TNode<BoolT> phi_bb11_7;
  if (block11.is_used()) {
    ca_.Bind(&block11, &phi_bb11_7);
    ca_.Branch(phi_bb11_7, &block7, std::vector<Node*>{}, &block8, std::vector<Node*>{});
  }

  if (block8.is_used()) {
    ca_.Bind(&block8);
    CodeStubAssembler(state_).FailAssert("Torque assert '0 <= baseA && 0 < baseB' failed", "third_party/v8/builtins/array-sort.tq", 1025);
  }

  TNode<Smi> tmp10;
  TNode<BoolT> tmp11;
  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1026);
    tmp10 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{p_baseA}, TNode<Smi>{p_lengthAArg});
    tmp11 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp10}, TNode<Smi>{p_baseB});
    ca_.Branch(tmp11, &block12, std::vector<Node*>{}, &block13, std::vector<Node*>{});
  }

  if (block13.is_used()) {
    ca_.Bind(&block13);
    CodeStubAssembler(state_).FailAssert("Torque assert 'baseA + lengthAArg == baseB' failed", "third_party/v8/builtins/array-sort.tq", 1026);
  }

  TNode<IntPtrT> tmp12;
  TNode<FixedArray> tmp13;
  TNode<FixedArray> tmp14;
  TNode<Smi> tmp15;
  TNode<Object> tmp16;
  TNode<Smi> tmp17;
  TNode<Smi> tmp18;
  TNode<Smi> tmp19;
  TNode<Smi> tmp20;
  TNode<Smi> tmp21;
  TNode<Smi> tmp22;
  TNode<Smi> tmp23;
  TNode<Smi> tmp24;
  TNode<IntPtrT> tmp25;
  TNode<IntPtrT> tmp26;
  TNode<IntPtrT> tmp27;
  TNode<Smi> tmp28;
  TNode<IntPtrT> tmp29;
  TNode<Smi> tmp30;
  TNode<Smi> tmp31;
  TNode<IntPtrT> tmp32;
  TNode<UintPtrT> tmp33;
  TNode<UintPtrT> tmp34;
  TNode<BoolT> tmp35;
  if (block12.is_used()) {
    ca_.Bind(&block12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1031);
    tmp12 = FromConstexpr_intptr_constexpr_int31_0(state_, 52);
    tmp13 = CodeStubAssembler(state_).LoadReference<FixedArray>(CodeStubAssembler::Reference{p_sortState, tmp12});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1032);
    tmp14 = GetTempArray_0(state_, TNode<Context>{p_context}, TNode<SortState>{p_sortState}, TNode<Smi>{p_lengthBArg});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1033);
    tmp15 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp16 = CodeStubAssembler(state_).CallBuiltin(Builtins::kCopy, p_context, tmp13, p_baseB, tmp14, tmp15, p_lengthBArg);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1036);
    tmp17 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{p_baseB}, TNode<Smi>{p_lengthBArg});
    tmp18 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp19 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp17}, TNode<Smi>{tmp18});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1037);
    tmp20 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp21 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{p_lengthBArg}, TNode<Smi>{tmp20});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1038);
    tmp22 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{p_baseA}, TNode<Smi>{p_lengthAArg});
    tmp23 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp24 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp22}, TNode<Smi>{tmp23});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1040);
    tmp25 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp26 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp27 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp28 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp27});
    tmp29 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp28});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1040);
    tmp30 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp31 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp19}, TNode<Smi>{tmp30});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp32 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp19});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp33 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp32});
    tmp34 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp29});
    tmp35 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp33}, TNode<UintPtrT>{tmp34});
    ca_.Branch(tmp35, &block18, std::vector<Node*>{}, &block19, std::vector<Node*>{});
  }

  TNode<IntPtrT> tmp36;
  TNode<IntPtrT> tmp37;
  TNode<IntPtrT> tmp38;
  TNode<HeapObject> tmp39;
  TNode<IntPtrT> tmp40;
  TNode<IntPtrT> tmp41;
  TNode<IntPtrT> tmp42;
  TNode<IntPtrT> tmp43;
  TNode<Smi> tmp44;
  TNode<IntPtrT> tmp45;
  TNode<Smi> tmp46;
  TNode<Smi> tmp47;
  TNode<IntPtrT> tmp48;
  TNode<UintPtrT> tmp49;
  TNode<UintPtrT> tmp50;
  TNode<BoolT> tmp51;
  if (block18.is_used()) {
    ca_.Bind(&block18);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp36 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp37 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp32}, TNode<IntPtrT>{tmp36});
    tmp38 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp25}, TNode<IntPtrT>{tmp37});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp39, tmp40) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp38}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1040);
    tmp41 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp42 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp43 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp44 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp43});
    tmp45 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp44});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1040);
    tmp46 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp47 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp24}, TNode<Smi>{tmp46});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp48 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp24});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp49 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp48});
    tmp50 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp45});
    tmp51 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp49}, TNode<UintPtrT>{tmp50});
    ca_.Branch(tmp51, &block25, std::vector<Node*>{}, &block26, std::vector<Node*>{});
  }

  if (block19.is_used()) {
    ca_.Bind(&block19);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<IntPtrT> tmp52;
  TNode<IntPtrT> tmp53;
  TNode<IntPtrT> tmp54;
  TNode<HeapObject> tmp55;
  TNode<IntPtrT> tmp56;
  TNode<Object> tmp57;
  TNode<Smi> tmp58;
  TNode<Smi> tmp59;
  TNode<Smi> tmp60;
  TNode<BoolT> tmp61;
  if (block25.is_used()) {
    ca_.Bind(&block25);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp52 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp53 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp48}, TNode<IntPtrT>{tmp52});
    tmp54 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp41}, TNode<IntPtrT>{tmp53});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp55, tmp56) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp54}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1040);
    tmp57 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp55, tmp56});
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp39, tmp40}, tmp57);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1043);
    tmp58 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp59 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{p_lengthAArg}, TNode<Smi>{tmp58});
    tmp60 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp61 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp59}, TNode<Smi>{tmp60});
    ca_.Branch(tmp61, &block32, std::vector<Node*>{}, &block33, std::vector<Node*>{});
  }

  if (block26.is_used()) {
    ca_.Bind(&block26);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  if (block32.is_used()) {
    ca_.Bind(&block32);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1043);
    ca_.Goto(&block31, tmp59, p_lengthBArg, tmp31, tmp21, tmp47);
  }

  TNode<Smi> tmp62;
  TNode<BoolT> tmp63;
  if (block33.is_used()) {
    ca_.Bind(&block33);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1044);
    tmp62 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp63 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{p_lengthBArg}, TNode<Smi>{tmp62});
    ca_.Branch(tmp63, &block34, std::vector<Node*>{}, &block35, std::vector<Node*>{});
  }

  if (block34.is_used()) {
    ca_.Bind(&block34);
    ca_.Goto(&block29, tmp59, p_lengthBArg, tmp31, tmp21, tmp47);
  }

  TNode<IntPtrT> tmp64;
  TNode<Smi> tmp65;
  if (block35.is_used()) {
    ca_.Bind(&block35);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1046);
    tmp64 = FromConstexpr_intptr_constexpr_int31_0(state_, 40);
    tmp65 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_sortState, tmp64});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1049);
    ca_.Goto(&block38, tmp59, p_lengthBArg, tmp31, tmp21, tmp47, tmp65);
  }

  TNode<Smi> phi_bb38_6;
  TNode<Smi> phi_bb38_7;
  TNode<Smi> phi_bb38_10;
  TNode<Smi> phi_bb38_11;
  TNode<Smi> phi_bb38_12;
  TNode<Smi> phi_bb38_13;
  TNode<BoolT> tmp66;
  if (block38.is_used()) {
    ca_.Bind(&block38, &phi_bb38_6, &phi_bb38_7, &phi_bb38_10, &phi_bb38_11, &phi_bb38_12, &phi_bb38_13);
    tmp66 = CodeStubAssembler(state_).Int32TrueConstant();
    ca_.Branch(tmp66, &block36, std::vector<Node*>{phi_bb38_6, phi_bb38_7, phi_bb38_10, phi_bb38_11, phi_bb38_12, phi_bb38_13}, &block37, std::vector<Node*>{phi_bb38_6, phi_bb38_7, phi_bb38_10, phi_bb38_11, phi_bb38_12, phi_bb38_13});
  }

  TNode<Smi> phi_bb36_6;
  TNode<Smi> phi_bb36_7;
  TNode<Smi> phi_bb36_10;
  TNode<Smi> phi_bb36_11;
  TNode<Smi> phi_bb36_12;
  TNode<Smi> phi_bb36_13;
  TNode<Smi> tmp67;
  TNode<Smi> tmp68;
  if (block36.is_used()) {
    ca_.Bind(&block36, &phi_bb36_6, &phi_bb36_7, &phi_bb36_10, &phi_bb36_11, &phi_bb36_12, &phi_bb36_13);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1050);
    tmp67 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1051);
    tmp68 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1057);
    ca_.Goto(&block41, phi_bb36_6, phi_bb36_7, phi_bb36_10, phi_bb36_11, phi_bb36_12, phi_bb36_13, tmp67, tmp68);
  }

  TNode<Smi> phi_bb41_6;
  TNode<Smi> phi_bb41_7;
  TNode<Smi> phi_bb41_10;
  TNode<Smi> phi_bb41_11;
  TNode<Smi> phi_bb41_12;
  TNode<Smi> phi_bb41_13;
  TNode<Smi> phi_bb41_14;
  TNode<Smi> phi_bb41_15;
  TNode<BoolT> tmp69;
  if (block41.is_used()) {
    ca_.Bind(&block41, &phi_bb41_6, &phi_bb41_7, &phi_bb41_10, &phi_bb41_11, &phi_bb41_12, &phi_bb41_13, &phi_bb41_14, &phi_bb41_15);
    tmp69 = CodeStubAssembler(state_).Int32TrueConstant();
    ca_.Branch(tmp69, &block39, std::vector<Node*>{phi_bb41_6, phi_bb41_7, phi_bb41_10, phi_bb41_11, phi_bb41_12, phi_bb41_13, phi_bb41_14, phi_bb41_15}, &block40, std::vector<Node*>{phi_bb41_6, phi_bb41_7, phi_bb41_10, phi_bb41_11, phi_bb41_12, phi_bb41_13, phi_bb41_14, phi_bb41_15});
  }

  TNode<Smi> phi_bb39_6;
  TNode<Smi> phi_bb39_7;
  TNode<Smi> phi_bb39_10;
  TNode<Smi> phi_bb39_11;
  TNode<Smi> phi_bb39_12;
  TNode<Smi> phi_bb39_13;
  TNode<Smi> phi_bb39_14;
  TNode<Smi> phi_bb39_15;
  TNode<Smi> tmp70;
  TNode<BoolT> tmp71;
  if (block39.is_used()) {
    ca_.Bind(&block39, &phi_bb39_6, &phi_bb39_7, &phi_bb39_10, &phi_bb39_11, &phi_bb39_12, &phi_bb39_13, &phi_bb39_14, &phi_bb39_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1058);
    tmp70 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp71 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{phi_bb39_6}, TNode<Smi>{tmp70});
    ca_.Branch(tmp71, &block44, std::vector<Node*>{phi_bb39_6, phi_bb39_7, phi_bb39_10, phi_bb39_11, phi_bb39_12, phi_bb39_13, phi_bb39_14, phi_bb39_15}, &block45, std::vector<Node*>{phi_bb39_6, phi_bb39_7, phi_bb39_10, phi_bb39_11, phi_bb39_12, phi_bb39_13, phi_bb39_14, phi_bb39_15});
  }

  TNode<Smi> phi_bb44_6;
  TNode<Smi> phi_bb44_7;
  TNode<Smi> phi_bb44_10;
  TNode<Smi> phi_bb44_11;
  TNode<Smi> phi_bb44_12;
  TNode<Smi> phi_bb44_13;
  TNode<Smi> phi_bb44_14;
  TNode<Smi> phi_bb44_15;
  TNode<Smi> tmp72;
  TNode<BoolT> tmp73;
  if (block44.is_used()) {
    ca_.Bind(&block44, &phi_bb44_6, &phi_bb44_7, &phi_bb44_10, &phi_bb44_11, &phi_bb44_12, &phi_bb44_13, &phi_bb44_14, &phi_bb44_15);
    tmp72 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp73 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{phi_bb44_7}, TNode<Smi>{tmp72});
    ca_.Goto(&block46, phi_bb44_6, phi_bb44_7, phi_bb44_10, phi_bb44_11, phi_bb44_12, phi_bb44_13, phi_bb44_14, phi_bb44_15, tmp73);
  }

  TNode<Smi> phi_bb45_6;
  TNode<Smi> phi_bb45_7;
  TNode<Smi> phi_bb45_10;
  TNode<Smi> phi_bb45_11;
  TNode<Smi> phi_bb45_12;
  TNode<Smi> phi_bb45_13;
  TNode<Smi> phi_bb45_14;
  TNode<Smi> phi_bb45_15;
  TNode<BoolT> tmp74;
  if (block45.is_used()) {
    ca_.Bind(&block45, &phi_bb45_6, &phi_bb45_7, &phi_bb45_10, &phi_bb45_11, &phi_bb45_12, &phi_bb45_13, &phi_bb45_14, &phi_bb45_15);
    tmp74 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block46, phi_bb45_6, phi_bb45_7, phi_bb45_10, phi_bb45_11, phi_bb45_12, phi_bb45_13, phi_bb45_14, phi_bb45_15, tmp74);
  }

  TNode<Smi> phi_bb46_6;
  TNode<Smi> phi_bb46_7;
  TNode<Smi> phi_bb46_10;
  TNode<Smi> phi_bb46_11;
  TNode<Smi> phi_bb46_12;
  TNode<Smi> phi_bb46_13;
  TNode<Smi> phi_bb46_14;
  TNode<Smi> phi_bb46_15;
  TNode<BoolT> phi_bb46_17;
  if (block46.is_used()) {
    ca_.Bind(&block46, &phi_bb46_6, &phi_bb46_7, &phi_bb46_10, &phi_bb46_11, &phi_bb46_12, &phi_bb46_13, &phi_bb46_14, &phi_bb46_15, &phi_bb46_17);
    ca_.Branch(phi_bb46_17, &block42, std::vector<Node*>{phi_bb46_6, phi_bb46_7, phi_bb46_10, phi_bb46_11, phi_bb46_12, phi_bb46_13, phi_bb46_14, phi_bb46_15}, &block43, std::vector<Node*>{phi_bb46_6, phi_bb46_7, phi_bb46_10, phi_bb46_11, phi_bb46_12, phi_bb46_13, phi_bb46_14, phi_bb46_15});
  }

  TNode<Smi> phi_bb43_6;
  TNode<Smi> phi_bb43_7;
  TNode<Smi> phi_bb43_10;
  TNode<Smi> phi_bb43_11;
  TNode<Smi> phi_bb43_12;
  TNode<Smi> phi_bb43_13;
  TNode<Smi> phi_bb43_14;
  TNode<Smi> phi_bb43_15;
  if (block43.is_used()) {
    ca_.Bind(&block43, &phi_bb43_6, &phi_bb43_7, &phi_bb43_10, &phi_bb43_11, &phi_bb43_12, &phi_bb43_13, &phi_bb43_14, &phi_bb43_15);
    CodeStubAssembler(state_).FailAssert("Torque assert 'lengthA > 0 && lengthB > 1' failed", "third_party/v8/builtins/array-sort.tq", 1058);
  }

  TNode<Smi> phi_bb42_6;
  TNode<Smi> phi_bb42_7;
  TNode<Smi> phi_bb42_10;
  TNode<Smi> phi_bb42_11;
  TNode<Smi> phi_bb42_12;
  TNode<Smi> phi_bb42_13;
  TNode<Smi> phi_bb42_14;
  TNode<Smi> phi_bb42_15;
  TNode<IntPtrT> tmp75;
  TNode<IntPtrT> tmp76;
  TNode<IntPtrT> tmp77;
  TNode<Smi> tmp78;
  TNode<IntPtrT> tmp79;
  TNode<IntPtrT> tmp80;
  TNode<UintPtrT> tmp81;
  TNode<UintPtrT> tmp82;
  TNode<BoolT> tmp83;
  if (block42.is_used()) {
    ca_.Bind(&block42, &phi_bb42_6, &phi_bb42_7, &phi_bb42_10, &phi_bb42_11, &phi_bb42_12, &phi_bb42_13, &phi_bb42_14, &phi_bb42_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1061);
    tmp75 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp76 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp77 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp78 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp14, tmp77});
    tmp79 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp78});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp80 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb42_11});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp81 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp80});
    tmp82 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp79});
    tmp83 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp81}, TNode<UintPtrT>{tmp82});
    ca_.Branch(tmp83, &block51, std::vector<Node*>{phi_bb42_6, phi_bb42_7, phi_bb42_10, phi_bb42_11, phi_bb42_12, phi_bb42_13, phi_bb42_14, phi_bb42_15, phi_bb42_11, phi_bb42_11}, &block52, std::vector<Node*>{phi_bb42_6, phi_bb42_7, phi_bb42_10, phi_bb42_11, phi_bb42_12, phi_bb42_13, phi_bb42_14, phi_bb42_15, phi_bb42_11, phi_bb42_11});
  }

  TNode<Smi> phi_bb51_6;
  TNode<Smi> phi_bb51_7;
  TNode<Smi> phi_bb51_10;
  TNode<Smi> phi_bb51_11;
  TNode<Smi> phi_bb51_12;
  TNode<Smi> phi_bb51_13;
  TNode<Smi> phi_bb51_14;
  TNode<Smi> phi_bb51_15;
  TNode<Smi> phi_bb51_21;
  TNode<Smi> phi_bb51_22;
  TNode<IntPtrT> tmp84;
  TNode<IntPtrT> tmp85;
  TNode<IntPtrT> tmp86;
  TNode<HeapObject> tmp87;
  TNode<IntPtrT> tmp88;
  TNode<Object> tmp89;
  TNode<Object> tmp90;
  TNode<IntPtrT> tmp91;
  TNode<IntPtrT> tmp92;
  TNode<IntPtrT> tmp93;
  TNode<Smi> tmp94;
  TNode<IntPtrT> tmp95;
  TNode<IntPtrT> tmp96;
  TNode<UintPtrT> tmp97;
  TNode<UintPtrT> tmp98;
  TNode<BoolT> tmp99;
  if (block51.is_used()) {
    ca_.Bind(&block51, &phi_bb51_6, &phi_bb51_7, &phi_bb51_10, &phi_bb51_11, &phi_bb51_12, &phi_bb51_13, &phi_bb51_14, &phi_bb51_15, &phi_bb51_21, &phi_bb51_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp84 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp85 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp80}, TNode<IntPtrT>{tmp84});
    tmp86 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp75}, TNode<IntPtrT>{tmp85});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp87, tmp88) = NewReference_Object_0(state_, TNode<HeapObject>{tmp14}, TNode<IntPtrT>{tmp86}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1061);
    tmp89 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp87, tmp88});
    tmp90 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp89});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1062);
    tmp91 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp92 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp93 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp94 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp93});
    tmp95 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp94});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp96 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb51_12});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp97 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp96});
    tmp98 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp95});
    tmp99 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp97}, TNode<UintPtrT>{tmp98});
    ca_.Branch(tmp99, &block58, std::vector<Node*>{phi_bb51_6, phi_bb51_7, phi_bb51_10, phi_bb51_11, phi_bb51_12, phi_bb51_13, phi_bb51_14, phi_bb51_15, phi_bb51_12, phi_bb51_12}, &block59, std::vector<Node*>{phi_bb51_6, phi_bb51_7, phi_bb51_10, phi_bb51_11, phi_bb51_12, phi_bb51_13, phi_bb51_14, phi_bb51_15, phi_bb51_12, phi_bb51_12});
  }

  TNode<Smi> phi_bb52_6;
  TNode<Smi> phi_bb52_7;
  TNode<Smi> phi_bb52_10;
  TNode<Smi> phi_bb52_11;
  TNode<Smi> phi_bb52_12;
  TNode<Smi> phi_bb52_13;
  TNode<Smi> phi_bb52_14;
  TNode<Smi> phi_bb52_15;
  TNode<Smi> phi_bb52_21;
  TNode<Smi> phi_bb52_22;
  if (block52.is_used()) {
    ca_.Bind(&block52, &phi_bb52_6, &phi_bb52_7, &phi_bb52_10, &phi_bb52_11, &phi_bb52_12, &phi_bb52_13, &phi_bb52_14, &phi_bb52_15, &phi_bb52_21, &phi_bb52_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb58_6;
  TNode<Smi> phi_bb58_7;
  TNode<Smi> phi_bb58_10;
  TNode<Smi> phi_bb58_11;
  TNode<Smi> phi_bb58_12;
  TNode<Smi> phi_bb58_13;
  TNode<Smi> phi_bb58_14;
  TNode<Smi> phi_bb58_15;
  TNode<Smi> phi_bb58_22;
  TNode<Smi> phi_bb58_23;
  TNode<IntPtrT> tmp100;
  TNode<IntPtrT> tmp101;
  TNode<IntPtrT> tmp102;
  TNode<HeapObject> tmp103;
  TNode<IntPtrT> tmp104;
  TNode<Object> tmp105;
  TNode<Object> tmp106;
  TNode<Number> tmp107;
  TNode<Number> tmp108;
  TNode<BoolT> tmp109;
  if (block58.is_used()) {
    ca_.Bind(&block58, &phi_bb58_6, &phi_bb58_7, &phi_bb58_10, &phi_bb58_11, &phi_bb58_12, &phi_bb58_13, &phi_bb58_14, &phi_bb58_15, &phi_bb58_22, &phi_bb58_23);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp100 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp101 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp96}, TNode<IntPtrT>{tmp100});
    tmp102 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp91}, TNode<IntPtrT>{tmp101});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp103, tmp104) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp102}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1062);
    tmp105 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp103, tmp104});
    tmp106 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp105});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1060);
    tmp107 = Method_SortState_Compare_0(state_, TNode<Context>{p_context}, TNode<SortState>{p_sortState}, TNode<Object>{tmp90}, TNode<Object>{tmp106});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1064);
    tmp108 = FromConstexpr_Number_constexpr_int31_0(state_, 0);
    tmp109 = NumberIsLessThan_0(state_, TNode<Number>{tmp107}, TNode<Number>{tmp108});
    ca_.Branch(tmp109, &block61, std::vector<Node*>{phi_bb58_6, phi_bb58_7, phi_bb58_10, phi_bb58_11, phi_bb58_12, phi_bb58_13, phi_bb58_14, phi_bb58_15}, &block62, std::vector<Node*>{phi_bb58_6, phi_bb58_7, phi_bb58_10, phi_bb58_11, phi_bb58_12, phi_bb58_13, phi_bb58_14, phi_bb58_15});
  }

  TNode<Smi> phi_bb59_6;
  TNode<Smi> phi_bb59_7;
  TNode<Smi> phi_bb59_10;
  TNode<Smi> phi_bb59_11;
  TNode<Smi> phi_bb59_12;
  TNode<Smi> phi_bb59_13;
  TNode<Smi> phi_bb59_14;
  TNode<Smi> phi_bb59_15;
  TNode<Smi> phi_bb59_22;
  TNode<Smi> phi_bb59_23;
  if (block59.is_used()) {
    ca_.Bind(&block59, &phi_bb59_6, &phi_bb59_7, &phi_bb59_10, &phi_bb59_11, &phi_bb59_12, &phi_bb59_13, &phi_bb59_14, &phi_bb59_15, &phi_bb59_22, &phi_bb59_23);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb61_6;
  TNode<Smi> phi_bb61_7;
  TNode<Smi> phi_bb61_10;
  TNode<Smi> phi_bb61_11;
  TNode<Smi> phi_bb61_12;
  TNode<Smi> phi_bb61_13;
  TNode<Smi> phi_bb61_14;
  TNode<Smi> phi_bb61_15;
  TNode<IntPtrT> tmp110;
  TNode<IntPtrT> tmp111;
  TNode<IntPtrT> tmp112;
  TNode<Smi> tmp113;
  TNode<IntPtrT> tmp114;
  TNode<Smi> tmp115;
  TNode<Smi> tmp116;
  TNode<IntPtrT> tmp117;
  TNode<UintPtrT> tmp118;
  TNode<UintPtrT> tmp119;
  TNode<BoolT> tmp120;
  if (block61.is_used()) {
    ca_.Bind(&block61, &phi_bb61_6, &phi_bb61_7, &phi_bb61_10, &phi_bb61_11, &phi_bb61_12, &phi_bb61_13, &phi_bb61_14, &phi_bb61_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1065);
    tmp110 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp111 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp112 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp113 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp112});
    tmp114 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp113});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1065);
    tmp115 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp116 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb61_10}, TNode<Smi>{tmp115});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp117 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb61_10});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp118 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp117});
    tmp119 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp114});
    tmp120 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp118}, TNode<UintPtrT>{tmp119});
    ca_.Branch(tmp120, &block68, std::vector<Node*>{phi_bb61_6, phi_bb61_7, phi_bb61_11, phi_bb61_12, phi_bb61_13, phi_bb61_14, phi_bb61_15, phi_bb61_10, phi_bb61_10}, &block69, std::vector<Node*>{phi_bb61_6, phi_bb61_7, phi_bb61_11, phi_bb61_12, phi_bb61_13, phi_bb61_14, phi_bb61_15, phi_bb61_10, phi_bb61_10});
  }

  TNode<Smi> phi_bb68_6;
  TNode<Smi> phi_bb68_7;
  TNode<Smi> phi_bb68_11;
  TNode<Smi> phi_bb68_12;
  TNode<Smi> phi_bb68_13;
  TNode<Smi> phi_bb68_14;
  TNode<Smi> phi_bb68_15;
  TNode<Smi> phi_bb68_21;
  TNode<Smi> phi_bb68_22;
  TNode<IntPtrT> tmp121;
  TNode<IntPtrT> tmp122;
  TNode<IntPtrT> tmp123;
  TNode<HeapObject> tmp124;
  TNode<IntPtrT> tmp125;
  TNode<IntPtrT> tmp126;
  TNode<IntPtrT> tmp127;
  TNode<IntPtrT> tmp128;
  TNode<Smi> tmp129;
  TNode<IntPtrT> tmp130;
  TNode<Smi> tmp131;
  TNode<Smi> tmp132;
  TNode<IntPtrT> tmp133;
  TNode<UintPtrT> tmp134;
  TNode<UintPtrT> tmp135;
  TNode<BoolT> tmp136;
  if (block68.is_used()) {
    ca_.Bind(&block68, &phi_bb68_6, &phi_bb68_7, &phi_bb68_11, &phi_bb68_12, &phi_bb68_13, &phi_bb68_14, &phi_bb68_15, &phi_bb68_21, &phi_bb68_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp121 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp122 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp117}, TNode<IntPtrT>{tmp121});
    tmp123 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp110}, TNode<IntPtrT>{tmp122});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp124, tmp125) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp123}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1065);
    tmp126 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp127 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp128 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp129 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp128});
    tmp130 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp129});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1065);
    tmp131 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp132 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb68_12}, TNode<Smi>{tmp131});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp133 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb68_12});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp134 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp133});
    tmp135 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp130});
    tmp136 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp134}, TNode<UintPtrT>{tmp135});
    ca_.Branch(tmp136, &block75, std::vector<Node*>{phi_bb68_6, phi_bb68_7, phi_bb68_11, phi_bb68_13, phi_bb68_14, phi_bb68_15, phi_bb68_21, phi_bb68_22, phi_bb68_12, phi_bb68_12}, &block76, std::vector<Node*>{phi_bb68_6, phi_bb68_7, phi_bb68_11, phi_bb68_13, phi_bb68_14, phi_bb68_15, phi_bb68_21, phi_bb68_22, phi_bb68_12, phi_bb68_12});
  }

  TNode<Smi> phi_bb69_6;
  TNode<Smi> phi_bb69_7;
  TNode<Smi> phi_bb69_11;
  TNode<Smi> phi_bb69_12;
  TNode<Smi> phi_bb69_13;
  TNode<Smi> phi_bb69_14;
  TNode<Smi> phi_bb69_15;
  TNode<Smi> phi_bb69_21;
  TNode<Smi> phi_bb69_22;
  if (block69.is_used()) {
    ca_.Bind(&block69, &phi_bb69_6, &phi_bb69_7, &phi_bb69_11, &phi_bb69_12, &phi_bb69_13, &phi_bb69_14, &phi_bb69_15, &phi_bb69_21, &phi_bb69_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb75_6;
  TNode<Smi> phi_bb75_7;
  TNode<Smi> phi_bb75_11;
  TNode<Smi> phi_bb75_13;
  TNode<Smi> phi_bb75_14;
  TNode<Smi> phi_bb75_15;
  TNode<Smi> phi_bb75_21;
  TNode<Smi> phi_bb75_22;
  TNode<Smi> phi_bb75_29;
  TNode<Smi> phi_bb75_30;
  TNode<IntPtrT> tmp137;
  TNode<IntPtrT> tmp138;
  TNode<IntPtrT> tmp139;
  TNode<HeapObject> tmp140;
  TNode<IntPtrT> tmp141;
  TNode<Object> tmp142;
  TNode<Smi> tmp143;
  TNode<Smi> tmp144;
  TNode<Smi> tmp145;
  TNode<Smi> tmp146;
  TNode<Smi> tmp147;
  TNode<Smi> tmp148;
  TNode<BoolT> tmp149;
  if (block75.is_used()) {
    ca_.Bind(&block75, &phi_bb75_6, &phi_bb75_7, &phi_bb75_11, &phi_bb75_13, &phi_bb75_14, &phi_bb75_15, &phi_bb75_21, &phi_bb75_22, &phi_bb75_29, &phi_bb75_30);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp137 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp138 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp133}, TNode<IntPtrT>{tmp137});
    tmp139 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp126}, TNode<IntPtrT>{tmp138});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp140, tmp141) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp139}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1065);
    tmp142 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp140, tmp141});
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp124, tmp125}, tmp142);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1067);
    tmp143 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp144 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb75_14}, TNode<Smi>{tmp143});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1068);
    tmp145 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp146 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb75_6}, TNode<Smi>{tmp145});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1069);
    tmp147 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1071);
    tmp148 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp149 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp146}, TNode<Smi>{tmp148});
    ca_.Branch(tmp149, &block78, std::vector<Node*>{phi_bb75_7, phi_bb75_11, phi_bb75_13}, &block79, std::vector<Node*>{phi_bb75_7, phi_bb75_11, phi_bb75_13});
  }

  TNode<Smi> phi_bb76_6;
  TNode<Smi> phi_bb76_7;
  TNode<Smi> phi_bb76_11;
  TNode<Smi> phi_bb76_13;
  TNode<Smi> phi_bb76_14;
  TNode<Smi> phi_bb76_15;
  TNode<Smi> phi_bb76_21;
  TNode<Smi> phi_bb76_22;
  TNode<Smi> phi_bb76_29;
  TNode<Smi> phi_bb76_30;
  if (block76.is_used()) {
    ca_.Bind(&block76, &phi_bb76_6, &phi_bb76_7, &phi_bb76_11, &phi_bb76_13, &phi_bb76_14, &phi_bb76_15, &phi_bb76_21, &phi_bb76_22, &phi_bb76_29, &phi_bb76_30);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb78_7;
  TNode<Smi> phi_bb78_11;
  TNode<Smi> phi_bb78_13;
  if (block78.is_used()) {
    ca_.Bind(&block78, &phi_bb78_7, &phi_bb78_11, &phi_bb78_13);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1071);
    ca_.Goto(&block31, tmp146, phi_bb78_7, tmp116, phi_bb78_11, tmp132);
  }

  TNode<Smi> phi_bb79_7;
  TNode<Smi> phi_bb79_11;
  TNode<Smi> phi_bb79_13;
  TNode<BoolT> tmp150;
  if (block79.is_used()) {
    ca_.Bind(&block79, &phi_bb79_7, &phi_bb79_11, &phi_bb79_13);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1072);
    tmp150 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{tmp144}, TNode<Smi>{phi_bb79_13});
    ca_.Branch(tmp150, &block80, std::vector<Node*>{phi_bb79_7, phi_bb79_11, phi_bb79_13}, &block81, std::vector<Node*>{phi_bb79_7, phi_bb79_11, phi_bb79_13});
  }

  TNode<Smi> phi_bb80_7;
  TNode<Smi> phi_bb80_11;
  TNode<Smi> phi_bb80_13;
  if (block80.is_used()) {
    ca_.Bind(&block80, &phi_bb80_7, &phi_bb80_11, &phi_bb80_13);
    ca_.Goto(&block40, tmp146, phi_bb80_7, tmp116, phi_bb80_11, tmp132, phi_bb80_13, tmp144, tmp147);
  }

  TNode<Smi> phi_bb81_7;
  TNode<Smi> phi_bb81_11;
  TNode<Smi> phi_bb81_13;
  if (block81.is_used()) {
    ca_.Bind(&block81, &phi_bb81_7, &phi_bb81_11, &phi_bb81_13);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1064);
    ca_.Goto(&block63, tmp146, phi_bb81_7, tmp116, phi_bb81_11, tmp132, phi_bb81_13, tmp144, tmp147);
  }

  TNode<Smi> phi_bb62_6;
  TNode<Smi> phi_bb62_7;
  TNode<Smi> phi_bb62_10;
  TNode<Smi> phi_bb62_11;
  TNode<Smi> phi_bb62_12;
  TNode<Smi> phi_bb62_13;
  TNode<Smi> phi_bb62_14;
  TNode<Smi> phi_bb62_15;
  TNode<IntPtrT> tmp151;
  TNode<IntPtrT> tmp152;
  TNode<IntPtrT> tmp153;
  TNode<Smi> tmp154;
  TNode<IntPtrT> tmp155;
  TNode<Smi> tmp156;
  TNode<Smi> tmp157;
  TNode<IntPtrT> tmp158;
  TNode<UintPtrT> tmp159;
  TNode<UintPtrT> tmp160;
  TNode<BoolT> tmp161;
  if (block62.is_used()) {
    ca_.Bind(&block62, &phi_bb62_6, &phi_bb62_7, &phi_bb62_10, &phi_bb62_11, &phi_bb62_12, &phi_bb62_13, &phi_bb62_14, &phi_bb62_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1074);
    tmp151 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp152 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp153 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp154 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp153});
    tmp155 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp154});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1074);
    tmp156 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp157 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb62_10}, TNode<Smi>{tmp156});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp158 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb62_10});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp159 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp158});
    tmp160 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp155});
    tmp161 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp159}, TNode<UintPtrT>{tmp160});
    ca_.Branch(tmp161, &block86, std::vector<Node*>{phi_bb62_6, phi_bb62_7, phi_bb62_11, phi_bb62_12, phi_bb62_13, phi_bb62_14, phi_bb62_15, phi_bb62_10, phi_bb62_10}, &block87, std::vector<Node*>{phi_bb62_6, phi_bb62_7, phi_bb62_11, phi_bb62_12, phi_bb62_13, phi_bb62_14, phi_bb62_15, phi_bb62_10, phi_bb62_10});
  }

  TNode<Smi> phi_bb86_6;
  TNode<Smi> phi_bb86_7;
  TNode<Smi> phi_bb86_11;
  TNode<Smi> phi_bb86_12;
  TNode<Smi> phi_bb86_13;
  TNode<Smi> phi_bb86_14;
  TNode<Smi> phi_bb86_15;
  TNode<Smi> phi_bb86_21;
  TNode<Smi> phi_bb86_22;
  TNode<IntPtrT> tmp162;
  TNode<IntPtrT> tmp163;
  TNode<IntPtrT> tmp164;
  TNode<HeapObject> tmp165;
  TNode<IntPtrT> tmp166;
  TNode<IntPtrT> tmp167;
  TNode<IntPtrT> tmp168;
  TNode<IntPtrT> tmp169;
  TNode<Smi> tmp170;
  TNode<IntPtrT> tmp171;
  TNode<Smi> tmp172;
  TNode<Smi> tmp173;
  TNode<IntPtrT> tmp174;
  TNode<UintPtrT> tmp175;
  TNode<UintPtrT> tmp176;
  TNode<BoolT> tmp177;
  if (block86.is_used()) {
    ca_.Bind(&block86, &phi_bb86_6, &phi_bb86_7, &phi_bb86_11, &phi_bb86_12, &phi_bb86_13, &phi_bb86_14, &phi_bb86_15, &phi_bb86_21, &phi_bb86_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp162 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp163 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp158}, TNode<IntPtrT>{tmp162});
    tmp164 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp151}, TNode<IntPtrT>{tmp163});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp165, tmp166) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp164}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1074);
    tmp167 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp168 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp169 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp170 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp14, tmp169});
    tmp171 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp170});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1074);
    tmp172 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp173 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb86_11}, TNode<Smi>{tmp172});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp174 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb86_11});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp175 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp174});
    tmp176 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp171});
    tmp177 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp175}, TNode<UintPtrT>{tmp176});
    ca_.Branch(tmp177, &block93, std::vector<Node*>{phi_bb86_6, phi_bb86_7, phi_bb86_12, phi_bb86_13, phi_bb86_14, phi_bb86_15, phi_bb86_21, phi_bb86_22, phi_bb86_11, phi_bb86_11}, &block94, std::vector<Node*>{phi_bb86_6, phi_bb86_7, phi_bb86_12, phi_bb86_13, phi_bb86_14, phi_bb86_15, phi_bb86_21, phi_bb86_22, phi_bb86_11, phi_bb86_11});
  }

  TNode<Smi> phi_bb87_6;
  TNode<Smi> phi_bb87_7;
  TNode<Smi> phi_bb87_11;
  TNode<Smi> phi_bb87_12;
  TNode<Smi> phi_bb87_13;
  TNode<Smi> phi_bb87_14;
  TNode<Smi> phi_bb87_15;
  TNode<Smi> phi_bb87_21;
  TNode<Smi> phi_bb87_22;
  if (block87.is_used()) {
    ca_.Bind(&block87, &phi_bb87_6, &phi_bb87_7, &phi_bb87_11, &phi_bb87_12, &phi_bb87_13, &phi_bb87_14, &phi_bb87_15, &phi_bb87_21, &phi_bb87_22);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb93_6;
  TNode<Smi> phi_bb93_7;
  TNode<Smi> phi_bb93_12;
  TNode<Smi> phi_bb93_13;
  TNode<Smi> phi_bb93_14;
  TNode<Smi> phi_bb93_15;
  TNode<Smi> phi_bb93_21;
  TNode<Smi> phi_bb93_22;
  TNode<Smi> phi_bb93_29;
  TNode<Smi> phi_bb93_30;
  TNode<IntPtrT> tmp178;
  TNode<IntPtrT> tmp179;
  TNode<IntPtrT> tmp180;
  TNode<HeapObject> tmp181;
  TNode<IntPtrT> tmp182;
  TNode<Object> tmp183;
  TNode<Smi> tmp184;
  TNode<Smi> tmp185;
  TNode<Smi> tmp186;
  TNode<Smi> tmp187;
  TNode<Smi> tmp188;
  TNode<Smi> tmp189;
  TNode<BoolT> tmp190;
  if (block93.is_used()) {
    ca_.Bind(&block93, &phi_bb93_6, &phi_bb93_7, &phi_bb93_12, &phi_bb93_13, &phi_bb93_14, &phi_bb93_15, &phi_bb93_21, &phi_bb93_22, &phi_bb93_29, &phi_bb93_30);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp178 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp179 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp174}, TNode<IntPtrT>{tmp178});
    tmp180 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp167}, TNode<IntPtrT>{tmp179});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp181, tmp182) = NewReference_Object_0(state_, TNode<HeapObject>{tmp14}, TNode<IntPtrT>{tmp180}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1074);
    tmp183 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp181, tmp182});
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp165, tmp166}, tmp183);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1076);
    tmp184 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp185 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb93_15}, TNode<Smi>{tmp184});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1077);
    tmp186 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp187 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb93_7}, TNode<Smi>{tmp186});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1078);
    tmp188 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1080);
    tmp189 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp190 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp187}, TNode<Smi>{tmp189});
    ca_.Branch(tmp190, &block96, std::vector<Node*>{phi_bb93_6, phi_bb93_12, phi_bb93_13}, &block97, std::vector<Node*>{phi_bb93_6, phi_bb93_12, phi_bb93_13});
  }

  TNode<Smi> phi_bb94_6;
  TNode<Smi> phi_bb94_7;
  TNode<Smi> phi_bb94_12;
  TNode<Smi> phi_bb94_13;
  TNode<Smi> phi_bb94_14;
  TNode<Smi> phi_bb94_15;
  TNode<Smi> phi_bb94_21;
  TNode<Smi> phi_bb94_22;
  TNode<Smi> phi_bb94_29;
  TNode<Smi> phi_bb94_30;
  if (block94.is_used()) {
    ca_.Bind(&block94, &phi_bb94_6, &phi_bb94_7, &phi_bb94_12, &phi_bb94_13, &phi_bb94_14, &phi_bb94_15, &phi_bb94_21, &phi_bb94_22, &phi_bb94_29, &phi_bb94_30);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb96_6;
  TNode<Smi> phi_bb96_12;
  TNode<Smi> phi_bb96_13;
  if (block96.is_used()) {
    ca_.Bind(&block96, &phi_bb96_6, &phi_bb96_12, &phi_bb96_13);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1080);
    ca_.Goto(&block29, phi_bb96_6, tmp187, tmp157, tmp173, phi_bb96_12);
  }

  TNode<Smi> phi_bb97_6;
  TNode<Smi> phi_bb97_12;
  TNode<Smi> phi_bb97_13;
  TNode<BoolT> tmp191;
  if (block97.is_used()) {
    ca_.Bind(&block97, &phi_bb97_6, &phi_bb97_12, &phi_bb97_13);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1081);
    tmp191 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{tmp185}, TNode<Smi>{phi_bb97_13});
    ca_.Branch(tmp191, &block98, std::vector<Node*>{phi_bb97_6, phi_bb97_12, phi_bb97_13}, &block99, std::vector<Node*>{phi_bb97_6, phi_bb97_12, phi_bb97_13});
  }

  TNode<Smi> phi_bb98_6;
  TNode<Smi> phi_bb98_12;
  TNode<Smi> phi_bb98_13;
  if (block98.is_used()) {
    ca_.Bind(&block98, &phi_bb98_6, &phi_bb98_12, &phi_bb98_13);
    ca_.Goto(&block40, phi_bb98_6, tmp187, tmp157, tmp173, phi_bb98_12, phi_bb98_13, tmp188, tmp185);
  }

  TNode<Smi> phi_bb99_6;
  TNode<Smi> phi_bb99_12;
  TNode<Smi> phi_bb99_13;
  if (block99.is_used()) {
    ca_.Bind(&block99, &phi_bb99_6, &phi_bb99_12, &phi_bb99_13);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1064);
    ca_.Goto(&block63, phi_bb99_6, tmp187, tmp157, tmp173, phi_bb99_12, phi_bb99_13, tmp188, tmp185);
  }

  TNode<Smi> phi_bb63_6;
  TNode<Smi> phi_bb63_7;
  TNode<Smi> phi_bb63_10;
  TNode<Smi> phi_bb63_11;
  TNode<Smi> phi_bb63_12;
  TNode<Smi> phi_bb63_13;
  TNode<Smi> phi_bb63_14;
  TNode<Smi> phi_bb63_15;
  if (block63.is_used()) {
    ca_.Bind(&block63, &phi_bb63_6, &phi_bb63_7, &phi_bb63_10, &phi_bb63_11, &phi_bb63_12, &phi_bb63_13, &phi_bb63_14, &phi_bb63_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1057);
    ca_.Goto(&block41, phi_bb63_6, phi_bb63_7, phi_bb63_10, phi_bb63_11, phi_bb63_12, phi_bb63_13, phi_bb63_14, phi_bb63_15);
  }

  TNode<Smi> phi_bb40_6;
  TNode<Smi> phi_bb40_7;
  TNode<Smi> phi_bb40_10;
  TNode<Smi> phi_bb40_11;
  TNode<Smi> phi_bb40_12;
  TNode<Smi> phi_bb40_13;
  TNode<Smi> phi_bb40_14;
  TNode<Smi> phi_bb40_15;
  TNode<Smi> tmp192;
  TNode<Smi> tmp193;
  TNode<BoolT> tmp194;
  if (block40.is_used()) {
    ca_.Bind(&block40, &phi_bb40_6, &phi_bb40_7, &phi_bb40_10, &phi_bb40_11, &phi_bb40_12, &phi_bb40_13, &phi_bb40_14, &phi_bb40_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1088);
    tmp192 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp193 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb40_13}, TNode<Smi>{tmp192});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1089);
    tmp194 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1090);
    ca_.Goto(&block102, phi_bb40_6, phi_bb40_7, phi_bb40_10, phi_bb40_11, phi_bb40_12, tmp193, phi_bb40_14, phi_bb40_15, tmp194);
  }

  TNode<Smi> phi_bb102_6;
  TNode<Smi> phi_bb102_7;
  TNode<Smi> phi_bb102_10;
  TNode<Smi> phi_bb102_11;
  TNode<Smi> phi_bb102_12;
  TNode<Smi> phi_bb102_13;
  TNode<Smi> phi_bb102_14;
  TNode<Smi> phi_bb102_15;
  TNode<BoolT> phi_bb102_16;
  TNode<Smi> tmp195;
  TNode<BoolT> tmp196;
  if (block102.is_used()) {
    ca_.Bind(&block102, &phi_bb102_6, &phi_bb102_7, &phi_bb102_10, &phi_bb102_11, &phi_bb102_12, &phi_bb102_13, &phi_bb102_14, &phi_bb102_15, &phi_bb102_16);
    tmp195 = FromConstexpr_Smi_constexpr_int31_0(state_, kMinGallopWins_0(state_));
    tmp196 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{phi_bb102_14}, TNode<Smi>{tmp195});
    ca_.Branch(tmp196, &block103, std::vector<Node*>{phi_bb102_6, phi_bb102_7, phi_bb102_10, phi_bb102_11, phi_bb102_12, phi_bb102_13, phi_bb102_14, phi_bb102_15, phi_bb102_16}, &block104, std::vector<Node*>{phi_bb102_6, phi_bb102_7, phi_bb102_10, phi_bb102_11, phi_bb102_12, phi_bb102_13, phi_bb102_14, phi_bb102_15, phi_bb102_16});
  }

  TNode<Smi> phi_bb103_6;
  TNode<Smi> phi_bb103_7;
  TNode<Smi> phi_bb103_10;
  TNode<Smi> phi_bb103_11;
  TNode<Smi> phi_bb103_12;
  TNode<Smi> phi_bb103_13;
  TNode<Smi> phi_bb103_14;
  TNode<Smi> phi_bb103_15;
  TNode<BoolT> phi_bb103_16;
  TNode<BoolT> tmp197;
  if (block103.is_used()) {
    ca_.Bind(&block103, &phi_bb103_6, &phi_bb103_7, &phi_bb103_10, &phi_bb103_11, &phi_bb103_12, &phi_bb103_13, &phi_bb103_14, &phi_bb103_15, &phi_bb103_16);
    tmp197 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    ca_.Goto(&block105, phi_bb103_6, phi_bb103_7, phi_bb103_10, phi_bb103_11, phi_bb103_12, phi_bb103_13, phi_bb103_14, phi_bb103_15, phi_bb103_16, tmp197);
  }

  TNode<Smi> phi_bb104_6;
  TNode<Smi> phi_bb104_7;
  TNode<Smi> phi_bb104_10;
  TNode<Smi> phi_bb104_11;
  TNode<Smi> phi_bb104_12;
  TNode<Smi> phi_bb104_13;
  TNode<Smi> phi_bb104_14;
  TNode<Smi> phi_bb104_15;
  TNode<BoolT> phi_bb104_16;
  TNode<Smi> tmp198;
  TNode<BoolT> tmp199;
  if (block104.is_used()) {
    ca_.Bind(&block104, &phi_bb104_6, &phi_bb104_7, &phi_bb104_10, &phi_bb104_11, &phi_bb104_12, &phi_bb104_13, &phi_bb104_14, &phi_bb104_15, &phi_bb104_16);
    tmp198 = FromConstexpr_Smi_constexpr_int31_0(state_, kMinGallopWins_0(state_));
    tmp199 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{phi_bb104_15}, TNode<Smi>{tmp198});
    ca_.Goto(&block105, phi_bb104_6, phi_bb104_7, phi_bb104_10, phi_bb104_11, phi_bb104_12, phi_bb104_13, phi_bb104_14, phi_bb104_15, phi_bb104_16, tmp199);
  }

  TNode<Smi> phi_bb105_6;
  TNode<Smi> phi_bb105_7;
  TNode<Smi> phi_bb105_10;
  TNode<Smi> phi_bb105_11;
  TNode<Smi> phi_bb105_12;
  TNode<Smi> phi_bb105_13;
  TNode<Smi> phi_bb105_14;
  TNode<Smi> phi_bb105_15;
  TNode<BoolT> phi_bb105_16;
  TNode<BoolT> phi_bb105_18;
  if (block105.is_used()) {
    ca_.Bind(&block105, &phi_bb105_6, &phi_bb105_7, &phi_bb105_10, &phi_bb105_11, &phi_bb105_12, &phi_bb105_13, &phi_bb105_14, &phi_bb105_15, &phi_bb105_16, &phi_bb105_18);
    ca_.Branch(phi_bb105_18, &block106, std::vector<Node*>{phi_bb105_6, phi_bb105_7, phi_bb105_10, phi_bb105_11, phi_bb105_12, phi_bb105_13, phi_bb105_14, phi_bb105_15, phi_bb105_16}, &block107, std::vector<Node*>{phi_bb105_6, phi_bb105_7, phi_bb105_10, phi_bb105_11, phi_bb105_12, phi_bb105_13, phi_bb105_14, phi_bb105_15, phi_bb105_16});
  }

  TNode<Smi> phi_bb106_6;
  TNode<Smi> phi_bb106_7;
  TNode<Smi> phi_bb106_10;
  TNode<Smi> phi_bb106_11;
  TNode<Smi> phi_bb106_12;
  TNode<Smi> phi_bb106_13;
  TNode<Smi> phi_bb106_14;
  TNode<Smi> phi_bb106_15;
  TNode<BoolT> phi_bb106_16;
  TNode<BoolT> tmp200;
  if (block106.is_used()) {
    ca_.Bind(&block106, &phi_bb106_6, &phi_bb106_7, &phi_bb106_10, &phi_bb106_11, &phi_bb106_12, &phi_bb106_13, &phi_bb106_14, &phi_bb106_15, &phi_bb106_16);
    tmp200 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    ca_.Goto(&block108, phi_bb106_6, phi_bb106_7, phi_bb106_10, phi_bb106_11, phi_bb106_12, phi_bb106_13, phi_bb106_14, phi_bb106_15, phi_bb106_16, tmp200);
  }

  TNode<Smi> phi_bb107_6;
  TNode<Smi> phi_bb107_7;
  TNode<Smi> phi_bb107_10;
  TNode<Smi> phi_bb107_11;
  TNode<Smi> phi_bb107_12;
  TNode<Smi> phi_bb107_13;
  TNode<Smi> phi_bb107_14;
  TNode<Smi> phi_bb107_15;
  TNode<BoolT> phi_bb107_16;
  if (block107.is_used()) {
    ca_.Bind(&block107, &phi_bb107_6, &phi_bb107_7, &phi_bb107_10, &phi_bb107_11, &phi_bb107_12, &phi_bb107_13, &phi_bb107_14, &phi_bb107_15, &phi_bb107_16);
    ca_.Goto(&block108, phi_bb107_6, phi_bb107_7, phi_bb107_10, phi_bb107_11, phi_bb107_12, phi_bb107_13, phi_bb107_14, phi_bb107_15, phi_bb107_16, phi_bb107_16);
  }

  TNode<Smi> phi_bb108_6;
  TNode<Smi> phi_bb108_7;
  TNode<Smi> phi_bb108_10;
  TNode<Smi> phi_bb108_11;
  TNode<Smi> phi_bb108_12;
  TNode<Smi> phi_bb108_13;
  TNode<Smi> phi_bb108_14;
  TNode<Smi> phi_bb108_15;
  TNode<BoolT> phi_bb108_16;
  TNode<BoolT> phi_bb108_18;
  if (block108.is_used()) {
    ca_.Bind(&block108, &phi_bb108_6, &phi_bb108_7, &phi_bb108_10, &phi_bb108_11, &phi_bb108_12, &phi_bb108_13, &phi_bb108_14, &phi_bb108_15, &phi_bb108_16, &phi_bb108_18);
    ca_.Branch(phi_bb108_18, &block100, std::vector<Node*>{phi_bb108_6, phi_bb108_7, phi_bb108_10, phi_bb108_11, phi_bb108_12, phi_bb108_13, phi_bb108_14, phi_bb108_15, phi_bb108_16}, &block101, std::vector<Node*>{phi_bb108_6, phi_bb108_7, phi_bb108_10, phi_bb108_11, phi_bb108_12, phi_bb108_13, phi_bb108_14, phi_bb108_15, phi_bb108_16});
  }

  TNode<Smi> phi_bb100_6;
  TNode<Smi> phi_bb100_7;
  TNode<Smi> phi_bb100_10;
  TNode<Smi> phi_bb100_11;
  TNode<Smi> phi_bb100_12;
  TNode<Smi> phi_bb100_13;
  TNode<Smi> phi_bb100_14;
  TNode<Smi> phi_bb100_15;
  TNode<BoolT> phi_bb100_16;
  TNode<BoolT> tmp201;
  TNode<Smi> tmp202;
  TNode<BoolT> tmp203;
  if (block100.is_used()) {
    ca_.Bind(&block100, &phi_bb100_6, &phi_bb100_7, &phi_bb100_10, &phi_bb100_11, &phi_bb100_12, &phi_bb100_13, &phi_bb100_14, &phi_bb100_15, &phi_bb100_16);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1092);
    tmp201 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1094);
    tmp202 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp203 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{phi_bb100_6}, TNode<Smi>{tmp202});
    ca_.Branch(tmp203, &block111, std::vector<Node*>{phi_bb100_6, phi_bb100_7, phi_bb100_10, phi_bb100_11, phi_bb100_12, phi_bb100_13, phi_bb100_14, phi_bb100_15}, &block112, std::vector<Node*>{phi_bb100_6, phi_bb100_7, phi_bb100_10, phi_bb100_11, phi_bb100_12, phi_bb100_13, phi_bb100_14, phi_bb100_15});
  }

  TNode<Smi> phi_bb111_6;
  TNode<Smi> phi_bb111_7;
  TNode<Smi> phi_bb111_10;
  TNode<Smi> phi_bb111_11;
  TNode<Smi> phi_bb111_12;
  TNode<Smi> phi_bb111_13;
  TNode<Smi> phi_bb111_14;
  TNode<Smi> phi_bb111_15;
  TNode<Smi> tmp204;
  TNode<BoolT> tmp205;
  if (block111.is_used()) {
    ca_.Bind(&block111, &phi_bb111_6, &phi_bb111_7, &phi_bb111_10, &phi_bb111_11, &phi_bb111_12, &phi_bb111_13, &phi_bb111_14, &phi_bb111_15);
    tmp204 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp205 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{phi_bb111_7}, TNode<Smi>{tmp204});
    ca_.Goto(&block113, phi_bb111_6, phi_bb111_7, phi_bb111_10, phi_bb111_11, phi_bb111_12, phi_bb111_13, phi_bb111_14, phi_bb111_15, tmp205);
  }

  TNode<Smi> phi_bb112_6;
  TNode<Smi> phi_bb112_7;
  TNode<Smi> phi_bb112_10;
  TNode<Smi> phi_bb112_11;
  TNode<Smi> phi_bb112_12;
  TNode<Smi> phi_bb112_13;
  TNode<Smi> phi_bb112_14;
  TNode<Smi> phi_bb112_15;
  TNode<BoolT> tmp206;
  if (block112.is_used()) {
    ca_.Bind(&block112, &phi_bb112_6, &phi_bb112_7, &phi_bb112_10, &phi_bb112_11, &phi_bb112_12, &phi_bb112_13, &phi_bb112_14, &phi_bb112_15);
    tmp206 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block113, phi_bb112_6, phi_bb112_7, phi_bb112_10, phi_bb112_11, phi_bb112_12, phi_bb112_13, phi_bb112_14, phi_bb112_15, tmp206);
  }

  TNode<Smi> phi_bb113_6;
  TNode<Smi> phi_bb113_7;
  TNode<Smi> phi_bb113_10;
  TNode<Smi> phi_bb113_11;
  TNode<Smi> phi_bb113_12;
  TNode<Smi> phi_bb113_13;
  TNode<Smi> phi_bb113_14;
  TNode<Smi> phi_bb113_15;
  TNode<BoolT> phi_bb113_18;
  if (block113.is_used()) {
    ca_.Bind(&block113, &phi_bb113_6, &phi_bb113_7, &phi_bb113_10, &phi_bb113_11, &phi_bb113_12, &phi_bb113_13, &phi_bb113_14, &phi_bb113_15, &phi_bb113_18);
    ca_.Branch(phi_bb113_18, &block109, std::vector<Node*>{phi_bb113_6, phi_bb113_7, phi_bb113_10, phi_bb113_11, phi_bb113_12, phi_bb113_13, phi_bb113_14, phi_bb113_15}, &block110, std::vector<Node*>{phi_bb113_6, phi_bb113_7, phi_bb113_10, phi_bb113_11, phi_bb113_12, phi_bb113_13, phi_bb113_14, phi_bb113_15});
  }

  TNode<Smi> phi_bb110_6;
  TNode<Smi> phi_bb110_7;
  TNode<Smi> phi_bb110_10;
  TNode<Smi> phi_bb110_11;
  TNode<Smi> phi_bb110_12;
  TNode<Smi> phi_bb110_13;
  TNode<Smi> phi_bb110_14;
  TNode<Smi> phi_bb110_15;
  if (block110.is_used()) {
    ca_.Bind(&block110, &phi_bb110_6, &phi_bb110_7, &phi_bb110_10, &phi_bb110_11, &phi_bb110_12, &phi_bb110_13, &phi_bb110_14, &phi_bb110_15);
    CodeStubAssembler(state_).FailAssert("Torque assert 'lengthA > 0 && lengthB > 1' failed", "third_party/v8/builtins/array-sort.tq", 1094);
  }

  TNode<Smi> phi_bb109_6;
  TNode<Smi> phi_bb109_7;
  TNode<Smi> phi_bb109_10;
  TNode<Smi> phi_bb109_11;
  TNode<Smi> phi_bb109_12;
  TNode<Smi> phi_bb109_13;
  TNode<Smi> phi_bb109_14;
  TNode<Smi> phi_bb109_15;
  TNode<Smi> tmp207;
  TNode<Smi> tmp208;
  TNode<Smi> tmp209;
  TNode<Smi> tmp210;
  TNode<IntPtrT> tmp211;
  TNode<IntPtrT> tmp212;
  TNode<IntPtrT> tmp213;
  TNode<IntPtrT> tmp214;
  TNode<Smi> tmp215;
  TNode<IntPtrT> tmp216;
  TNode<IntPtrT> tmp217;
  TNode<UintPtrT> tmp218;
  TNode<UintPtrT> tmp219;
  TNode<BoolT> tmp220;
  if (block109.is_used()) {
    ca_.Bind(&block109, &phi_bb109_6, &phi_bb109_7, &phi_bb109_10, &phi_bb109_11, &phi_bb109_12, &phi_bb109_13, &phi_bb109_14, &phi_bb109_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1096);
    tmp207 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp208 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb109_13}, TNode<Smi>{tmp207});
    tmp209 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp210 = CodeStubAssembler(state_).SmiMax(TNode<Smi>{tmp209}, TNode<Smi>{tmp208});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1097);
    tmp211 = FromConstexpr_intptr_constexpr_int31_0(state_, 40);
    CodeStubAssembler(state_).StoreReference<Smi>(CodeStubAssembler::Reference{p_sortState, tmp211}, tmp210);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1100);
    tmp212 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp213 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp214 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp215 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp14, tmp214});
    tmp216 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp215});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp217 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb109_11});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp218 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp217});
    tmp219 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp216});
    tmp220 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp218}, TNode<UintPtrT>{tmp219});
    ca_.Branch(tmp220, &block118, std::vector<Node*>{phi_bb109_6, phi_bb109_7, phi_bb109_10, phi_bb109_11, phi_bb109_12, phi_bb109_14, phi_bb109_15, phi_bb109_11, phi_bb109_11}, &block119, std::vector<Node*>{phi_bb109_6, phi_bb109_7, phi_bb109_10, phi_bb109_11, phi_bb109_12, phi_bb109_14, phi_bb109_15, phi_bb109_11, phi_bb109_11});
  }

  TNode<Smi> phi_bb118_6;
  TNode<Smi> phi_bb118_7;
  TNode<Smi> phi_bb118_10;
  TNode<Smi> phi_bb118_11;
  TNode<Smi> phi_bb118_12;
  TNode<Smi> phi_bb118_14;
  TNode<Smi> phi_bb118_15;
  TNode<Smi> phi_bb118_22;
  TNode<Smi> phi_bb118_23;
  TNode<IntPtrT> tmp221;
  TNode<IntPtrT> tmp222;
  TNode<IntPtrT> tmp223;
  TNode<HeapObject> tmp224;
  TNode<IntPtrT> tmp225;
  TNode<Object> tmp226;
  TNode<Object> tmp227;
  TNode<Smi> tmp228;
  TNode<Smi> tmp229;
  TNode<Smi> tmp230;
  TNode<Smi> tmp231;
  TNode<BoolT> tmp232;
  if (block118.is_used()) {
    ca_.Bind(&block118, &phi_bb118_6, &phi_bb118_7, &phi_bb118_10, &phi_bb118_11, &phi_bb118_12, &phi_bb118_14, &phi_bb118_15, &phi_bb118_22, &phi_bb118_23);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp221 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp222 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp217}, TNode<IntPtrT>{tmp221});
    tmp223 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp212}, TNode<IntPtrT>{tmp222});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp224, tmp225) = NewReference_Object_0(state_, TNode<HeapObject>{tmp14}, TNode<IntPtrT>{tmp223}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1100);
    tmp226 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp224, tmp225});
    tmp227 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp226});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1101);
    tmp228 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp229 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb118_6}, TNode<Smi>{tmp228});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1099);
    tmp230 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kGallopRight, p_context, p_sortState, tmp13, tmp227, p_baseA, phi_bb118_6, tmp229));
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1102);
    tmp231 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp232 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{tmp230}, TNode<Smi>{tmp231});
    ca_.Branch(tmp232, &block121, std::vector<Node*>{phi_bb118_6, phi_bb118_7, phi_bb118_10, phi_bb118_11, phi_bb118_12, phi_bb118_14, phi_bb118_15}, &block122, std::vector<Node*>{phi_bb118_6, phi_bb118_7, phi_bb118_10, phi_bb118_11, phi_bb118_12, phi_bb118_14, phi_bb118_15});
  }

  TNode<Smi> phi_bb119_6;
  TNode<Smi> phi_bb119_7;
  TNode<Smi> phi_bb119_10;
  TNode<Smi> phi_bb119_11;
  TNode<Smi> phi_bb119_12;
  TNode<Smi> phi_bb119_14;
  TNode<Smi> phi_bb119_15;
  TNode<Smi> phi_bb119_22;
  TNode<Smi> phi_bb119_23;
  if (block119.is_used()) {
    ca_.Bind(&block119, &phi_bb119_6, &phi_bb119_7, &phi_bb119_10, &phi_bb119_11, &phi_bb119_12, &phi_bb119_14, &phi_bb119_15, &phi_bb119_22, &phi_bb119_23);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb122_6;
  TNode<Smi> phi_bb122_7;
  TNode<Smi> phi_bb122_10;
  TNode<Smi> phi_bb122_11;
  TNode<Smi> phi_bb122_12;
  TNode<Smi> phi_bb122_14;
  TNode<Smi> phi_bb122_15;
  if (block122.is_used()) {
    ca_.Bind(&block122, &phi_bb122_6, &phi_bb122_7, &phi_bb122_10, &phi_bb122_11, &phi_bb122_12, &phi_bb122_14, &phi_bb122_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1102);
    CodeStubAssembler(state_).FailAssert("Torque assert 'k >= 0' failed", "third_party/v8/builtins/array-sort.tq", 1102);
  }

  TNode<Smi> phi_bb121_6;
  TNode<Smi> phi_bb121_7;
  TNode<Smi> phi_bb121_10;
  TNode<Smi> phi_bb121_11;
  TNode<Smi> phi_bb121_12;
  TNode<Smi> phi_bb121_14;
  TNode<Smi> phi_bb121_15;
  TNode<Smi> tmp233;
  TNode<Smi> tmp234;
  TNode<BoolT> tmp235;
  if (block121.is_used()) {
    ca_.Bind(&block121, &phi_bb121_6, &phi_bb121_7, &phi_bb121_10, &phi_bb121_11, &phi_bb121_12, &phi_bb121_14, &phi_bb121_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1103);
    tmp233 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb121_6}, TNode<Smi>{tmp230});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1105);
    tmp234 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp235 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{tmp233}, TNode<Smi>{tmp234});
    ca_.Branch(tmp235, &block123, std::vector<Node*>{phi_bb121_6, phi_bb121_7, phi_bb121_10, phi_bb121_11, phi_bb121_12, phi_bb121_15}, &block124, std::vector<Node*>{phi_bb121_6, phi_bb121_7, phi_bb121_10, phi_bb121_11, phi_bb121_12, phi_bb121_15});
  }

  TNode<Smi> phi_bb123_6;
  TNode<Smi> phi_bb123_7;
  TNode<Smi> phi_bb123_10;
  TNode<Smi> phi_bb123_11;
  TNode<Smi> phi_bb123_12;
  TNode<Smi> phi_bb123_15;
  TNode<Smi> tmp236;
  TNode<Smi> tmp237;
  TNode<Smi> tmp238;
  TNode<Smi> tmp239;
  TNode<Smi> tmp240;
  TNode<Smi> tmp241;
  TNode<Object> tmp242;
  TNode<Smi> tmp243;
  TNode<Smi> tmp244;
  TNode<BoolT> tmp245;
  if (block123.is_used()) {
    ca_.Bind(&block123, &phi_bb123_6, &phi_bb123_7, &phi_bb123_10, &phi_bb123_11, &phi_bb123_12, &phi_bb123_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1106);
    tmp236 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb123_10}, TNode<Smi>{tmp233});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1107);
    tmp237 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb123_12}, TNode<Smi>{tmp233});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1108);
    tmp238 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp239 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp237}, TNode<Smi>{tmp238});
    tmp240 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp241 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp236}, TNode<Smi>{tmp240});
    tmp242 = CodeStubAssembler(state_).CallBuiltin(Builtins::kCopy, p_context, tmp13, tmp239, tmp13, tmp241, tmp233);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1110);
    tmp243 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb123_6}, TNode<Smi>{tmp233});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1111);
    tmp244 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp245 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp243}, TNode<Smi>{tmp244});
    ca_.Branch(tmp245, &block125, std::vector<Node*>{phi_bb123_7, phi_bb123_11, phi_bb123_15}, &block126, std::vector<Node*>{phi_bb123_7, phi_bb123_11, phi_bb123_15});
  }

  TNode<Smi> phi_bb125_7;
  TNode<Smi> phi_bb125_11;
  TNode<Smi> phi_bb125_15;
  if (block125.is_used()) {
    ca_.Bind(&block125, &phi_bb125_7, &phi_bb125_11, &phi_bb125_15);
    ca_.Goto(&block31, tmp243, phi_bb125_7, tmp236, phi_bb125_11, tmp237);
  }

  TNode<Smi> phi_bb126_7;
  TNode<Smi> phi_bb126_11;
  TNode<Smi> phi_bb126_15;
  if (block126.is_used()) {
    ca_.Bind(&block126, &phi_bb126_7, &phi_bb126_11, &phi_bb126_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1105);
    ca_.Goto(&block124, tmp243, phi_bb126_7, tmp236, phi_bb126_11, tmp237, phi_bb126_15);
  }

  TNode<Smi> phi_bb124_6;
  TNode<Smi> phi_bb124_7;
  TNode<Smi> phi_bb124_10;
  TNode<Smi> phi_bb124_11;
  TNode<Smi> phi_bb124_12;
  TNode<Smi> phi_bb124_15;
  TNode<IntPtrT> tmp246;
  TNode<IntPtrT> tmp247;
  TNode<IntPtrT> tmp248;
  TNode<Smi> tmp249;
  TNode<IntPtrT> tmp250;
  TNode<Smi> tmp251;
  TNode<Smi> tmp252;
  TNode<IntPtrT> tmp253;
  TNode<UintPtrT> tmp254;
  TNode<UintPtrT> tmp255;
  TNode<BoolT> tmp256;
  if (block124.is_used()) {
    ca_.Bind(&block124, &phi_bb124_6, &phi_bb124_7, &phi_bb124_10, &phi_bb124_11, &phi_bb124_12, &phi_bb124_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1113);
    tmp246 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp247 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp248 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp249 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp248});
    tmp250 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp249});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1113);
    tmp251 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp252 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb124_10}, TNode<Smi>{tmp251});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp253 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb124_10});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp254 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp253});
    tmp255 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp250});
    tmp256 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp254}, TNode<UintPtrT>{tmp255});
    ca_.Branch(tmp256, &block131, std::vector<Node*>{phi_bb124_6, phi_bb124_7, phi_bb124_11, phi_bb124_12, phi_bb124_15, phi_bb124_10, phi_bb124_10}, &block132, std::vector<Node*>{phi_bb124_6, phi_bb124_7, phi_bb124_11, phi_bb124_12, phi_bb124_15, phi_bb124_10, phi_bb124_10});
  }

  TNode<Smi> phi_bb131_6;
  TNode<Smi> phi_bb131_7;
  TNode<Smi> phi_bb131_11;
  TNode<Smi> phi_bb131_12;
  TNode<Smi> phi_bb131_15;
  TNode<Smi> phi_bb131_22;
  TNode<Smi> phi_bb131_23;
  TNode<IntPtrT> tmp257;
  TNode<IntPtrT> tmp258;
  TNode<IntPtrT> tmp259;
  TNode<HeapObject> tmp260;
  TNode<IntPtrT> tmp261;
  TNode<IntPtrT> tmp262;
  TNode<IntPtrT> tmp263;
  TNode<IntPtrT> tmp264;
  TNode<Smi> tmp265;
  TNode<IntPtrT> tmp266;
  TNode<Smi> tmp267;
  TNode<Smi> tmp268;
  TNode<IntPtrT> tmp269;
  TNode<UintPtrT> tmp270;
  TNode<UintPtrT> tmp271;
  TNode<BoolT> tmp272;
  if (block131.is_used()) {
    ca_.Bind(&block131, &phi_bb131_6, &phi_bb131_7, &phi_bb131_11, &phi_bb131_12, &phi_bb131_15, &phi_bb131_22, &phi_bb131_23);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp257 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp258 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp253}, TNode<IntPtrT>{tmp257});
    tmp259 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp246}, TNode<IntPtrT>{tmp258});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp260, tmp261) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp259}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1113);
    tmp262 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp263 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp264 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp265 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp14, tmp264});
    tmp266 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp265});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1113);
    tmp267 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp268 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb131_11}, TNode<Smi>{tmp267});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp269 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb131_11});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp270 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp269});
    tmp271 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp266});
    tmp272 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp270}, TNode<UintPtrT>{tmp271});
    ca_.Branch(tmp272, &block138, std::vector<Node*>{phi_bb131_6, phi_bb131_7, phi_bb131_12, phi_bb131_15, phi_bb131_22, phi_bb131_23, phi_bb131_11, phi_bb131_11}, &block139, std::vector<Node*>{phi_bb131_6, phi_bb131_7, phi_bb131_12, phi_bb131_15, phi_bb131_22, phi_bb131_23, phi_bb131_11, phi_bb131_11});
  }

  TNode<Smi> phi_bb132_6;
  TNode<Smi> phi_bb132_7;
  TNode<Smi> phi_bb132_11;
  TNode<Smi> phi_bb132_12;
  TNode<Smi> phi_bb132_15;
  TNode<Smi> phi_bb132_22;
  TNode<Smi> phi_bb132_23;
  if (block132.is_used()) {
    ca_.Bind(&block132, &phi_bb132_6, &phi_bb132_7, &phi_bb132_11, &phi_bb132_12, &phi_bb132_15, &phi_bb132_22, &phi_bb132_23);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb138_6;
  TNode<Smi> phi_bb138_7;
  TNode<Smi> phi_bb138_12;
  TNode<Smi> phi_bb138_15;
  TNode<Smi> phi_bb138_22;
  TNode<Smi> phi_bb138_23;
  TNode<Smi> phi_bb138_30;
  TNode<Smi> phi_bb138_31;
  TNode<IntPtrT> tmp273;
  TNode<IntPtrT> tmp274;
  TNode<IntPtrT> tmp275;
  TNode<HeapObject> tmp276;
  TNode<IntPtrT> tmp277;
  TNode<Object> tmp278;
  TNode<Smi> tmp279;
  TNode<Smi> tmp280;
  TNode<Smi> tmp281;
  TNode<BoolT> tmp282;
  if (block138.is_used()) {
    ca_.Bind(&block138, &phi_bb138_6, &phi_bb138_7, &phi_bb138_12, &phi_bb138_15, &phi_bb138_22, &phi_bb138_23, &phi_bb138_30, &phi_bb138_31);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp273 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp274 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp269}, TNode<IntPtrT>{tmp273});
    tmp275 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp262}, TNode<IntPtrT>{tmp274});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp276, tmp277) = NewReference_Object_0(state_, TNode<HeapObject>{tmp14}, TNode<IntPtrT>{tmp275}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1113);
    tmp278 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp276, tmp277});
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp260, tmp261}, tmp278);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1114);
    tmp279 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp280 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb138_7}, TNode<Smi>{tmp279});
    tmp281 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp282 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp280}, TNode<Smi>{tmp281});
    ca_.Branch(tmp282, &block141, std::vector<Node*>{phi_bb138_6, phi_bb138_12, phi_bb138_15}, &block142, std::vector<Node*>{phi_bb138_6, phi_bb138_12, phi_bb138_15});
  }

  TNode<Smi> phi_bb139_6;
  TNode<Smi> phi_bb139_7;
  TNode<Smi> phi_bb139_12;
  TNode<Smi> phi_bb139_15;
  TNode<Smi> phi_bb139_22;
  TNode<Smi> phi_bb139_23;
  TNode<Smi> phi_bb139_30;
  TNode<Smi> phi_bb139_31;
  if (block139.is_used()) {
    ca_.Bind(&block139, &phi_bb139_6, &phi_bb139_7, &phi_bb139_12, &phi_bb139_15, &phi_bb139_22, &phi_bb139_23, &phi_bb139_30, &phi_bb139_31);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb141_6;
  TNode<Smi> phi_bb141_12;
  TNode<Smi> phi_bb141_15;
  if (block141.is_used()) {
    ca_.Bind(&block141, &phi_bb141_6, &phi_bb141_12, &phi_bb141_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1114);
    ca_.Goto(&block29, phi_bb141_6, tmp280, tmp252, tmp268, phi_bb141_12);
  }

  TNode<Smi> phi_bb142_6;
  TNode<Smi> phi_bb142_12;
  TNode<Smi> phi_bb142_15;
  TNode<IntPtrT> tmp283;
  TNode<IntPtrT> tmp284;
  TNode<IntPtrT> tmp285;
  TNode<Smi> tmp286;
  TNode<IntPtrT> tmp287;
  TNode<IntPtrT> tmp288;
  TNode<UintPtrT> tmp289;
  TNode<UintPtrT> tmp290;
  TNode<BoolT> tmp291;
  if (block142.is_used()) {
    ca_.Bind(&block142, &phi_bb142_6, &phi_bb142_12, &phi_bb142_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1117);
    tmp283 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp284 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp285 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp286 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp285});
    tmp287 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp286});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp288 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb142_12});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp289 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp288});
    tmp290 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp287});
    tmp291 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp289}, TNode<UintPtrT>{tmp290});
    ca_.Branch(tmp291, &block147, std::vector<Node*>{phi_bb142_6, phi_bb142_12, phi_bb142_15, phi_bb142_12, phi_bb142_12}, &block148, std::vector<Node*>{phi_bb142_6, phi_bb142_12, phi_bb142_15, phi_bb142_12, phi_bb142_12});
  }

  TNode<Smi> phi_bb147_6;
  TNode<Smi> phi_bb147_12;
  TNode<Smi> phi_bb147_15;
  TNode<Smi> phi_bb147_23;
  TNode<Smi> phi_bb147_24;
  TNode<IntPtrT> tmp292;
  TNode<IntPtrT> tmp293;
  TNode<IntPtrT> tmp294;
  TNode<HeapObject> tmp295;
  TNode<IntPtrT> tmp296;
  TNode<Object> tmp297;
  TNode<Object> tmp298;
  TNode<Smi> tmp299;
  TNode<Smi> tmp300;
  TNode<Smi> tmp301;
  TNode<Smi> tmp302;
  TNode<Smi> tmp303;
  TNode<BoolT> tmp304;
  if (block147.is_used()) {
    ca_.Bind(&block147, &phi_bb147_6, &phi_bb147_12, &phi_bb147_15, &phi_bb147_23, &phi_bb147_24);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp292 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp293 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp288}, TNode<IntPtrT>{tmp292});
    tmp294 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp283}, TNode<IntPtrT>{tmp293});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp295, tmp296) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp294}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1117);
    tmp297 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp295, tmp296});
    tmp298 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp297});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1118);
    tmp299 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp300 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp280}, TNode<Smi>{tmp299});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1116);
    tmp301 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp302 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kGallopLeft, p_context, p_sortState, tmp14, tmp298, tmp301, tmp280, tmp300));
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1119);
    tmp303 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp304 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{tmp302}, TNode<Smi>{tmp303});
    ca_.Branch(tmp304, &block150, std::vector<Node*>{phi_bb147_6, phi_bb147_12, phi_bb147_15}, &block151, std::vector<Node*>{phi_bb147_6, phi_bb147_12, phi_bb147_15});
  }

  TNode<Smi> phi_bb148_6;
  TNode<Smi> phi_bb148_12;
  TNode<Smi> phi_bb148_15;
  TNode<Smi> phi_bb148_23;
  TNode<Smi> phi_bb148_24;
  if (block148.is_used()) {
    ca_.Bind(&block148, &phi_bb148_6, &phi_bb148_12, &phi_bb148_15, &phi_bb148_23, &phi_bb148_24);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb151_6;
  TNode<Smi> phi_bb151_12;
  TNode<Smi> phi_bb151_15;
  if (block151.is_used()) {
    ca_.Bind(&block151, &phi_bb151_6, &phi_bb151_12, &phi_bb151_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1119);
    CodeStubAssembler(state_).FailAssert("Torque assert 'k >= 0' failed", "third_party/v8/builtins/array-sort.tq", 1119);
  }

  TNode<Smi> phi_bb150_6;
  TNode<Smi> phi_bb150_12;
  TNode<Smi> phi_bb150_15;
  TNode<Smi> tmp305;
  TNode<Smi> tmp306;
  TNode<BoolT> tmp307;
  if (block150.is_used()) {
    ca_.Bind(&block150, &phi_bb150_6, &phi_bb150_12, &phi_bb150_15);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1120);
    tmp305 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp280}, TNode<Smi>{tmp302});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1122);
    tmp306 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp307 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{tmp305}, TNode<Smi>{tmp306});
    ca_.Branch(tmp307, &block152, std::vector<Node*>{phi_bb150_6, phi_bb150_12}, &block153, std::vector<Node*>{phi_bb150_6, tmp280, tmp252, tmp268, phi_bb150_12});
  }

  TNode<Smi> phi_bb152_6;
  TNode<Smi> phi_bb152_12;
  TNode<Smi> tmp308;
  TNode<Smi> tmp309;
  TNode<Smi> tmp310;
  TNode<Smi> tmp311;
  TNode<Smi> tmp312;
  TNode<Smi> tmp313;
  TNode<Object> tmp314;
  TNode<Smi> tmp315;
  TNode<Smi> tmp316;
  TNode<BoolT> tmp317;
  if (block152.is_used()) {
    ca_.Bind(&block152, &phi_bb152_6, &phi_bb152_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1123);
    tmp308 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp252}, TNode<Smi>{tmp305});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1124);
    tmp309 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp268}, TNode<Smi>{tmp305});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1125);
    tmp310 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp311 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp309}, TNode<Smi>{tmp310});
    tmp312 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp313 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp308}, TNode<Smi>{tmp312});
    tmp314 = CodeStubAssembler(state_).CallBuiltin(Builtins::kCopy, p_context, tmp14, tmp311, tmp13, tmp313, tmp305);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1127);
    tmp315 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp280}, TNode<Smi>{tmp305});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1128);
    tmp316 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp317 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp315}, TNode<Smi>{tmp316});
    ca_.Branch(tmp317, &block154, std::vector<Node*>{phi_bb152_6, phi_bb152_12}, &block155, std::vector<Node*>{phi_bb152_6, phi_bb152_12});
  }

  TNode<Smi> phi_bb154_6;
  TNode<Smi> phi_bb154_12;
  if (block154.is_used()) {
    ca_.Bind(&block154, &phi_bb154_6, &phi_bb154_12);
    ca_.Goto(&block29, phi_bb154_6, tmp315, tmp308, tmp309, phi_bb154_12);
  }

  TNode<Smi> phi_bb155_6;
  TNode<Smi> phi_bb155_12;
  TNode<Smi> tmp318;
  TNode<BoolT> tmp319;
  if (block155.is_used()) {
    ca_.Bind(&block155, &phi_bb155_6, &phi_bb155_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1132);
    tmp318 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp319 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp315}, TNode<Smi>{tmp318});
    ca_.Branch(tmp319, &block156, std::vector<Node*>{phi_bb155_6, phi_bb155_12}, &block157, std::vector<Node*>{phi_bb155_6, phi_bb155_12});
  }

  TNode<Smi> phi_bb156_6;
  TNode<Smi> phi_bb156_12;
  if (block156.is_used()) {
    ca_.Bind(&block156, &phi_bb156_6, &phi_bb156_12);
    ca_.Goto(&block31, phi_bb156_6, tmp315, tmp308, tmp309, phi_bb156_12);
  }

  TNode<Smi> phi_bb157_6;
  TNode<Smi> phi_bb157_12;
  if (block157.is_used()) {
    ca_.Bind(&block157, &phi_bb157_6, &phi_bb157_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1122);
    ca_.Goto(&block153, phi_bb157_6, tmp315, tmp308, tmp309, phi_bb157_12);
  }

  TNode<Smi> phi_bb153_6;
  TNode<Smi> phi_bb153_7;
  TNode<Smi> phi_bb153_10;
  TNode<Smi> phi_bb153_11;
  TNode<Smi> phi_bb153_12;
  TNode<IntPtrT> tmp320;
  TNode<IntPtrT> tmp321;
  TNode<IntPtrT> tmp322;
  TNode<Smi> tmp323;
  TNode<IntPtrT> tmp324;
  TNode<Smi> tmp325;
  TNode<Smi> tmp326;
  TNode<IntPtrT> tmp327;
  TNode<UintPtrT> tmp328;
  TNode<UintPtrT> tmp329;
  TNode<BoolT> tmp330;
  if (block153.is_used()) {
    ca_.Bind(&block153, &phi_bb153_6, &phi_bb153_7, &phi_bb153_10, &phi_bb153_11, &phi_bb153_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1134);
    tmp320 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp321 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp322 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp323 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp322});
    tmp324 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp323});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1134);
    tmp325 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp326 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb153_10}, TNode<Smi>{tmp325});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp327 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb153_10});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp328 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp327});
    tmp329 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp324});
    tmp330 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp328}, TNode<UintPtrT>{tmp329});
    ca_.Branch(tmp330, &block162, std::vector<Node*>{phi_bb153_6, phi_bb153_7, phi_bb153_11, phi_bb153_12, phi_bb153_10, phi_bb153_10}, &block163, std::vector<Node*>{phi_bb153_6, phi_bb153_7, phi_bb153_11, phi_bb153_12, phi_bb153_10, phi_bb153_10});
  }

  TNode<Smi> phi_bb162_6;
  TNode<Smi> phi_bb162_7;
  TNode<Smi> phi_bb162_11;
  TNode<Smi> phi_bb162_12;
  TNode<Smi> phi_bb162_22;
  TNode<Smi> phi_bb162_23;
  TNode<IntPtrT> tmp331;
  TNode<IntPtrT> tmp332;
  TNode<IntPtrT> tmp333;
  TNode<HeapObject> tmp334;
  TNode<IntPtrT> tmp335;
  TNode<IntPtrT> tmp336;
  TNode<IntPtrT> tmp337;
  TNode<IntPtrT> tmp338;
  TNode<Smi> tmp339;
  TNode<IntPtrT> tmp340;
  TNode<Smi> tmp341;
  TNode<Smi> tmp342;
  TNode<IntPtrT> tmp343;
  TNode<UintPtrT> tmp344;
  TNode<UintPtrT> tmp345;
  TNode<BoolT> tmp346;
  if (block162.is_used()) {
    ca_.Bind(&block162, &phi_bb162_6, &phi_bb162_7, &phi_bb162_11, &phi_bb162_12, &phi_bb162_22, &phi_bb162_23);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp331 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp332 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp327}, TNode<IntPtrT>{tmp331});
    tmp333 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp320}, TNode<IntPtrT>{tmp332});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp334, tmp335) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp333}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1134);
    tmp336 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp337 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp338 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp339 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp338});
    tmp340 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp339});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1134);
    tmp341 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp342 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb162_12}, TNode<Smi>{tmp341});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp343 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb162_12});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp344 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp343});
    tmp345 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp340});
    tmp346 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp344}, TNode<UintPtrT>{tmp345});
    ca_.Branch(tmp346, &block169, std::vector<Node*>{phi_bb162_6, phi_bb162_7, phi_bb162_11, phi_bb162_22, phi_bb162_23, phi_bb162_12, phi_bb162_12}, &block170, std::vector<Node*>{phi_bb162_6, phi_bb162_7, phi_bb162_11, phi_bb162_22, phi_bb162_23, phi_bb162_12, phi_bb162_12});
  }

  TNode<Smi> phi_bb163_6;
  TNode<Smi> phi_bb163_7;
  TNode<Smi> phi_bb163_11;
  TNode<Smi> phi_bb163_12;
  TNode<Smi> phi_bb163_22;
  TNode<Smi> phi_bb163_23;
  if (block163.is_used()) {
    ca_.Bind(&block163, &phi_bb163_6, &phi_bb163_7, &phi_bb163_11, &phi_bb163_12, &phi_bb163_22, &phi_bb163_23);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb169_6;
  TNode<Smi> phi_bb169_7;
  TNode<Smi> phi_bb169_11;
  TNode<Smi> phi_bb169_22;
  TNode<Smi> phi_bb169_23;
  TNode<Smi> phi_bb169_30;
  TNode<Smi> phi_bb169_31;
  TNode<IntPtrT> tmp347;
  TNode<IntPtrT> tmp348;
  TNode<IntPtrT> tmp349;
  TNode<HeapObject> tmp350;
  TNode<IntPtrT> tmp351;
  TNode<Object> tmp352;
  TNode<Smi> tmp353;
  TNode<Smi> tmp354;
  TNode<Smi> tmp355;
  TNode<BoolT> tmp356;
  if (block169.is_used()) {
    ca_.Bind(&block169, &phi_bb169_6, &phi_bb169_7, &phi_bb169_11, &phi_bb169_22, &phi_bb169_23, &phi_bb169_30, &phi_bb169_31);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp347 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp348 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp343}, TNode<IntPtrT>{tmp347});
    tmp349 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp336}, TNode<IntPtrT>{tmp348});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp350, tmp351) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp349}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1134);
    tmp352 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp350, tmp351});
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp334, tmp335}, tmp352);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1135);
    tmp353 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp354 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb169_6}, TNode<Smi>{tmp353});
    tmp355 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp356 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp354}, TNode<Smi>{tmp355});
    ca_.Branch(tmp356, &block172, std::vector<Node*>{phi_bb169_7, phi_bb169_11}, &block173, std::vector<Node*>{phi_bb169_7, phi_bb169_11});
  }

  TNode<Smi> phi_bb170_6;
  TNode<Smi> phi_bb170_7;
  TNode<Smi> phi_bb170_11;
  TNode<Smi> phi_bb170_22;
  TNode<Smi> phi_bb170_23;
  TNode<Smi> phi_bb170_30;
  TNode<Smi> phi_bb170_31;
  if (block170.is_used()) {
    ca_.Bind(&block170, &phi_bb170_6, &phi_bb170_7, &phi_bb170_11, &phi_bb170_22, &phi_bb170_23, &phi_bb170_30, &phi_bb170_31);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb172_7;
  TNode<Smi> phi_bb172_11;
  if (block172.is_used()) {
    ca_.Bind(&block172, &phi_bb172_7, &phi_bb172_11);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1135);
    ca_.Goto(&block31, tmp354, phi_bb172_7, tmp326, phi_bb172_11, tmp342);
  }

  TNode<Smi> phi_bb173_7;
  TNode<Smi> phi_bb173_11;
  if (block173.is_used()) {
    ca_.Bind(&block173, &phi_bb173_7, &phi_bb173_11);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1090);
    ca_.Goto(&block102, tmp354, phi_bb173_7, tmp326, phi_bb173_11, tmp342, tmp210, tmp233, tmp305, tmp201);
  }

  TNode<Smi> phi_bb101_6;
  TNode<Smi> phi_bb101_7;
  TNode<Smi> phi_bb101_10;
  TNode<Smi> phi_bb101_11;
  TNode<Smi> phi_bb101_12;
  TNode<Smi> phi_bb101_13;
  TNode<Smi> phi_bb101_14;
  TNode<Smi> phi_bb101_15;
  TNode<BoolT> phi_bb101_16;
  TNode<Smi> tmp357;
  TNode<Smi> tmp358;
  TNode<IntPtrT> tmp359;
  if (block101.is_used()) {
    ca_.Bind(&block101, &phi_bb101_6, &phi_bb101_7, &phi_bb101_10, &phi_bb101_11, &phi_bb101_12, &phi_bb101_13, &phi_bb101_14, &phi_bb101_15, &phi_bb101_16);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1137);
    tmp357 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp358 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb101_13}, TNode<Smi>{tmp357});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1138);
    tmp359 = FromConstexpr_intptr_constexpr_int31_0(state_, 40);
    CodeStubAssembler(state_).StoreReference<Smi>(CodeStubAssembler::Reference{p_sortState, tmp359}, tmp358);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1049);
    ca_.Goto(&block38, phi_bb101_6, phi_bb101_7, phi_bb101_10, phi_bb101_11, phi_bb101_12, tmp358);
  }

  TNode<Smi> phi_bb37_6;
  TNode<Smi> phi_bb37_7;
  TNode<Smi> phi_bb37_10;
  TNode<Smi> phi_bb37_11;
  TNode<Smi> phi_bb37_12;
  TNode<Smi> phi_bb37_13;
  if (block37.is_used()) {
    ca_.Bind(&block37, &phi_bb37_6, &phi_bb37_7, &phi_bb37_10, &phi_bb37_11, &phi_bb37_12, &phi_bb37_13);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1140);
    ca_.Goto(&block30, phi_bb37_6, phi_bb37_7, phi_bb37_10, phi_bb37_11, phi_bb37_12);
  }

  TNode<Smi> phi_bb31_6;
  TNode<Smi> phi_bb31_7;
  TNode<Smi> phi_bb31_10;
  TNode<Smi> phi_bb31_11;
  TNode<Smi> phi_bb31_12;
  TNode<Smi> tmp360;
  TNode<BoolT> tmp361;
  if (block31.is_used()) {
    ca_.Bind(&block31, &phi_bb31_6, &phi_bb31_7, &phi_bb31_10, &phi_bb31_11, &phi_bb31_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1141);
    tmp360 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp361 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{phi_bb31_7}, TNode<Smi>{tmp360});
    ca_.Branch(tmp361, &block174, std::vector<Node*>{phi_bb31_6, phi_bb31_7, phi_bb31_10, phi_bb31_11, phi_bb31_12}, &block175, std::vector<Node*>{phi_bb31_6, phi_bb31_7, phi_bb31_10, phi_bb31_11, phi_bb31_12});
  }

  TNode<Smi> phi_bb174_6;
  TNode<Smi> phi_bb174_7;
  TNode<Smi> phi_bb174_10;
  TNode<Smi> phi_bb174_11;
  TNode<Smi> phi_bb174_12;
  TNode<Smi> tmp362;
  TNode<BoolT> tmp363;
  if (block174.is_used()) {
    ca_.Bind(&block174, &phi_bb174_6, &phi_bb174_7, &phi_bb174_10, &phi_bb174_11, &phi_bb174_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1142);
    tmp362 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp363 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{phi_bb174_6}, TNode<Smi>{tmp362});
    ca_.Branch(tmp363, &block176, std::vector<Node*>{phi_bb174_6, phi_bb174_7, phi_bb174_10, phi_bb174_11, phi_bb174_12}, &block177, std::vector<Node*>{phi_bb174_6, phi_bb174_7, phi_bb174_10, phi_bb174_11, phi_bb174_12});
  }

  TNode<Smi> phi_bb177_6;
  TNode<Smi> phi_bb177_7;
  TNode<Smi> phi_bb177_10;
  TNode<Smi> phi_bb177_11;
  TNode<Smi> phi_bb177_12;
  if (block177.is_used()) {
    ca_.Bind(&block177, &phi_bb177_6, &phi_bb177_7, &phi_bb177_10, &phi_bb177_11, &phi_bb177_12);
    CodeStubAssembler(state_).FailAssert("Torque assert 'lengthA == 0' failed", "third_party/v8/builtins/array-sort.tq", 1142);
  }

  TNode<Smi> phi_bb176_6;
  TNode<Smi> phi_bb176_7;
  TNode<Smi> phi_bb176_10;
  TNode<Smi> phi_bb176_11;
  TNode<Smi> phi_bb176_12;
  TNode<Smi> tmp364;
  TNode<Smi> tmp365;
  TNode<Smi> tmp366;
  TNode<Smi> tmp367;
  TNode<Object> tmp368;
  if (block176.is_used()) {
    ca_.Bind(&block176, &phi_bb176_6, &phi_bb176_7, &phi_bb176_10, &phi_bb176_11, &phi_bb176_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1143);
    tmp364 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp365 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb176_7}, TNode<Smi>{tmp364});
    tmp366 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb176_10}, TNode<Smi>{tmp365});
    tmp367 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp368 = CodeStubAssembler(state_).CallBuiltin(Builtins::kCopy, p_context, tmp14, tmp367, tmp13, tmp366, phi_bb176_7);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1141);
    ca_.Goto(&block175, phi_bb176_6, phi_bb176_7, phi_bb176_10, phi_bb176_11, phi_bb176_12);
  }

  TNode<Smi> phi_bb175_6;
  TNode<Smi> phi_bb175_7;
  TNode<Smi> phi_bb175_10;
  TNode<Smi> phi_bb175_11;
  TNode<Smi> phi_bb175_12;
  if (block175.is_used()) {
    ca_.Bind(&block175, &phi_bb175_6, &phi_bb175_7, &phi_bb175_10, &phi_bb175_11, &phi_bb175_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1042);
    ca_.Goto(&block30, phi_bb175_6, phi_bb175_7, phi_bb175_10, phi_bb175_11, phi_bb175_12);
  }

  TNode<Smi> phi_bb30_6;
  TNode<Smi> phi_bb30_7;
  TNode<Smi> phi_bb30_10;
  TNode<Smi> phi_bb30_11;
  TNode<Smi> phi_bb30_12;
  if (block30.is_used()) {
    ca_.Bind(&block30, &phi_bb30_6, &phi_bb30_7, &phi_bb30_10, &phi_bb30_11, &phi_bb30_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1145);
    ca_.Goto(&block28, phi_bb30_6, phi_bb30_7, phi_bb30_10, phi_bb30_11, phi_bb30_12);
  }

  TNode<Smi> phi_bb29_6;
  TNode<Smi> phi_bb29_7;
  TNode<Smi> phi_bb29_10;
  TNode<Smi> phi_bb29_11;
  TNode<Smi> phi_bb29_12;
  TNode<Smi> tmp369;
  TNode<BoolT> tmp370;
  if (block29.is_used()) {
    ca_.Bind(&block29, &phi_bb29_6, &phi_bb29_7, &phi_bb29_10, &phi_bb29_11, &phi_bb29_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1146);
    tmp369 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp370 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{phi_bb29_7}, TNode<Smi>{tmp369});
    ca_.Branch(tmp370, &block180, std::vector<Node*>{phi_bb29_6, phi_bb29_7, phi_bb29_10, phi_bb29_11, phi_bb29_12}, &block181, std::vector<Node*>{phi_bb29_6, phi_bb29_7, phi_bb29_10, phi_bb29_11, phi_bb29_12});
  }

  TNode<Smi> phi_bb180_6;
  TNode<Smi> phi_bb180_7;
  TNode<Smi> phi_bb180_10;
  TNode<Smi> phi_bb180_11;
  TNode<Smi> phi_bb180_12;
  TNode<Smi> tmp371;
  TNode<BoolT> tmp372;
  if (block180.is_used()) {
    ca_.Bind(&block180, &phi_bb180_6, &phi_bb180_7, &phi_bb180_10, &phi_bb180_11, &phi_bb180_12);
    tmp371 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp372 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{phi_bb180_6}, TNode<Smi>{tmp371});
    ca_.Goto(&block182, phi_bb180_6, phi_bb180_7, phi_bb180_10, phi_bb180_11, phi_bb180_12, tmp372);
  }

  TNode<Smi> phi_bb181_6;
  TNode<Smi> phi_bb181_7;
  TNode<Smi> phi_bb181_10;
  TNode<Smi> phi_bb181_11;
  TNode<Smi> phi_bb181_12;
  TNode<BoolT> tmp373;
  if (block181.is_used()) {
    ca_.Bind(&block181, &phi_bb181_6, &phi_bb181_7, &phi_bb181_10, &phi_bb181_11, &phi_bb181_12);
    tmp373 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block182, phi_bb181_6, phi_bb181_7, phi_bb181_10, phi_bb181_11, phi_bb181_12, tmp373);
  }

  TNode<Smi> phi_bb182_6;
  TNode<Smi> phi_bb182_7;
  TNode<Smi> phi_bb182_10;
  TNode<Smi> phi_bb182_11;
  TNode<Smi> phi_bb182_12;
  TNode<BoolT> phi_bb182_14;
  if (block182.is_used()) {
    ca_.Bind(&block182, &phi_bb182_6, &phi_bb182_7, &phi_bb182_10, &phi_bb182_11, &phi_bb182_12, &phi_bb182_14);
    ca_.Branch(phi_bb182_14, &block178, std::vector<Node*>{phi_bb182_6, phi_bb182_7, phi_bb182_10, phi_bb182_11, phi_bb182_12}, &block179, std::vector<Node*>{phi_bb182_6, phi_bb182_7, phi_bb182_10, phi_bb182_11, phi_bb182_12});
  }

  TNode<Smi> phi_bb179_6;
  TNode<Smi> phi_bb179_7;
  TNode<Smi> phi_bb179_10;
  TNode<Smi> phi_bb179_11;
  TNode<Smi> phi_bb179_12;
  if (block179.is_used()) {
    ca_.Bind(&block179, &phi_bb179_6, &phi_bb179_7, &phi_bb179_10, &phi_bb179_11, &phi_bb179_12);
    CodeStubAssembler(state_).FailAssert("Torque assert 'lengthB == 1 && lengthA > 0' failed", "third_party/v8/builtins/array-sort.tq", 1146);
  }

  TNode<Smi> phi_bb178_6;
  TNode<Smi> phi_bb178_7;
  TNode<Smi> phi_bb178_10;
  TNode<Smi> phi_bb178_11;
  TNode<Smi> phi_bb178_12;
  TNode<Smi> tmp374;
  TNode<Smi> tmp375;
  TNode<Smi> tmp376;
  TNode<Smi> tmp377;
  TNode<Smi> tmp378;
  TNode<Smi> tmp379;
  TNode<Object> tmp380;
  TNode<IntPtrT> tmp381;
  TNode<IntPtrT> tmp382;
  TNode<IntPtrT> tmp383;
  TNode<Smi> tmp384;
  TNode<IntPtrT> tmp385;
  TNode<IntPtrT> tmp386;
  TNode<UintPtrT> tmp387;
  TNode<UintPtrT> tmp388;
  TNode<BoolT> tmp389;
  if (block178.is_used()) {
    ca_.Bind(&block178, &phi_bb178_6, &phi_bb178_7, &phi_bb178_10, &phi_bb178_11, &phi_bb178_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1149);
    tmp374 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb178_10}, TNode<Smi>{phi_bb178_6});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1150);
    tmp375 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb178_12}, TNode<Smi>{phi_bb178_6});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1151);
    tmp376 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp377 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp375}, TNode<Smi>{tmp376});
    tmp378 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp379 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp374}, TNode<Smi>{tmp378});
    tmp380 = CodeStubAssembler(state_).CallBuiltin(Builtins::kCopy, p_context, tmp13, tmp377, tmp13, tmp379, phi_bb178_6);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1152);
    tmp381 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp382 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp383 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp384 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp13, tmp383});
    tmp385 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp384});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp386 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp374});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp387 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp386});
    tmp388 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp385});
    tmp389 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp387}, TNode<UintPtrT>{tmp388});
    ca_.Branch(tmp389, &block187, std::vector<Node*>{phi_bb178_6, phi_bb178_7, phi_bb178_11}, &block188, std::vector<Node*>{phi_bb178_6, phi_bb178_7, phi_bb178_11});
  }

  TNode<Smi> phi_bb187_6;
  TNode<Smi> phi_bb187_7;
  TNode<Smi> phi_bb187_11;
  TNode<IntPtrT> tmp390;
  TNode<IntPtrT> tmp391;
  TNode<IntPtrT> tmp392;
  TNode<HeapObject> tmp393;
  TNode<IntPtrT> tmp394;
  TNode<IntPtrT> tmp395;
  TNode<IntPtrT> tmp396;
  TNode<IntPtrT> tmp397;
  TNode<Smi> tmp398;
  TNode<IntPtrT> tmp399;
  TNode<IntPtrT> tmp400;
  TNode<UintPtrT> tmp401;
  TNode<UintPtrT> tmp402;
  TNode<BoolT> tmp403;
  if (block187.is_used()) {
    ca_.Bind(&block187, &phi_bb187_6, &phi_bb187_7, &phi_bb187_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp390 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp391 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp386}, TNode<IntPtrT>{tmp390});
    tmp392 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp381}, TNode<IntPtrT>{tmp391});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp393, tmp394) = NewReference_Object_0(state_, TNode<HeapObject>{tmp13}, TNode<IntPtrT>{tmp392}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1152);
    tmp395 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp396 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp397 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp398 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp14, tmp397});
    tmp399 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp398});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp400 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb187_11});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp401 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp400});
    tmp402 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp399});
    tmp403 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp401}, TNode<UintPtrT>{tmp402});
    ca_.Branch(tmp403, &block194, std::vector<Node*>{phi_bb187_6, phi_bb187_7, phi_bb187_11, phi_bb187_11, phi_bb187_11}, &block195, std::vector<Node*>{phi_bb187_6, phi_bb187_7, phi_bb187_11, phi_bb187_11, phi_bb187_11});
  }

  TNode<Smi> phi_bb188_6;
  TNode<Smi> phi_bb188_7;
  TNode<Smi> phi_bb188_11;
  if (block188.is_used()) {
    ca_.Bind(&block188, &phi_bb188_6, &phi_bb188_7, &phi_bb188_11);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb194_6;
  TNode<Smi> phi_bb194_7;
  TNode<Smi> phi_bb194_11;
  TNode<Smi> phi_bb194_25;
  TNode<Smi> phi_bb194_26;
  TNode<IntPtrT> tmp404;
  TNode<IntPtrT> tmp405;
  TNode<IntPtrT> tmp406;
  TNode<HeapObject> tmp407;
  TNode<IntPtrT> tmp408;
  TNode<Object> tmp409;
  if (block194.is_used()) {
    ca_.Bind(&block194, &phi_bb194_6, &phi_bb194_7, &phi_bb194_11, &phi_bb194_25, &phi_bb194_26);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp404 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp405 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp400}, TNode<IntPtrT>{tmp404});
    tmp406 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp395}, TNode<IntPtrT>{tmp405});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp407, tmp408) = NewReference_Object_0(state_, TNode<HeapObject>{tmp14}, TNode<IntPtrT>{tmp406}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1152);
    tmp409 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp407, tmp408});
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp393, tmp394}, tmp409);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1042);
    ca_.Goto(&block28, phi_bb194_6, phi_bb194_7, tmp374, phi_bb194_11, tmp375);
  }

  TNode<Smi> phi_bb195_6;
  TNode<Smi> phi_bb195_7;
  TNode<Smi> phi_bb195_11;
  TNode<Smi> phi_bb195_25;
  TNode<Smi> phi_bb195_26;
  if (block195.is_used()) {
    ca_.Bind(&block195, &phi_bb195_6, &phi_bb195_7, &phi_bb195_11, &phi_bb195_25, &phi_bb195_26);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb28_6;
  TNode<Smi> phi_bb28_7;
  TNode<Smi> phi_bb28_10;
  TNode<Smi> phi_bb28_11;
  TNode<Smi> phi_bb28_12;
  if (block28.is_used()) {
    ca_.Bind(&block28, &phi_bb28_6, &phi_bb28_7, &phi_bb28_10, &phi_bb28_11, &phi_bb28_12);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1022);
    ca_.Goto(&block197);
  }

    ca_.Bind(&block197);
}

TNode<Smi> ComputeMinRunLength_0(compiler::CodeAssemblerState* state_, TNode<Smi> p_nArg) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, BoolT> block14(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, BoolT> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block8(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block15(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Smi> tmp0;
  TNode<Smi> tmp1;
  TNode<BoolT> tmp2;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1167);
    tmp0 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1169);
    tmp1 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp2 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{p_nArg}, TNode<Smi>{tmp1});
    ca_.Branch(tmp2, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    CodeStubAssembler(state_).FailAssert("Torque assert 'n >= 0' failed", "third_party/v8/builtins/array-sort.tq", 1169);
  }

  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1170);
    ca_.Goto(&block6, p_nArg, tmp0);
  }

  TNode<Smi> phi_bb6_1;
  TNode<Smi> phi_bb6_2;
  TNode<Smi> tmp3;
  TNode<BoolT> tmp4;
  if (block6.is_used()) {
    ca_.Bind(&block6, &phi_bb6_1, &phi_bb6_2);
    tmp3 = FromConstexpr_Smi_constexpr_int31_0(state_, 64);
    tmp4 = CodeStubAssembler(state_).SmiGreaterThanOrEqual(TNode<Smi>{phi_bb6_1}, TNode<Smi>{tmp3});
    ca_.Branch(tmp4, &block4, std::vector<Node*>{phi_bb6_1, phi_bb6_2}, &block5, std::vector<Node*>{phi_bb6_1, phi_bb6_2});
  }

  TNode<Smi> phi_bb4_1;
  TNode<Smi> phi_bb4_2;
  TNode<Smi> tmp5;
  TNode<Smi> tmp6;
  TNode<Smi> tmp7;
  TNode<Smi> tmp8;
  if (block4.is_used()) {
    ca_.Bind(&block4, &phi_bb4_1, &phi_bb4_2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1171);
    tmp5 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp6 = CodeStubAssembler(state_).SmiAnd(TNode<Smi>{phi_bb4_1}, TNode<Smi>{tmp5});
    tmp7 = CodeStubAssembler(state_).SmiOr(TNode<Smi>{phi_bb4_2}, TNode<Smi>{tmp6});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1172);
    tmp8 = CodeStubAssembler(state_).SmiSar(TNode<Smi>{phi_bb4_1}, 1);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1170);
    ca_.Goto(&block6, tmp8, tmp7);
  }

  TNode<Smi> phi_bb5_1;
  TNode<Smi> phi_bb5_2;
  TNode<Smi> tmp9;
  TNode<Smi> tmp10;
  TNode<BoolT> tmp11;
  if (block5.is_used()) {
    ca_.Bind(&block5, &phi_bb5_1, &phi_bb5_2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1175);
    tmp9 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb5_1}, TNode<Smi>{phi_bb5_2});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1176);
    tmp10 = FromConstexpr_Smi_constexpr_int31_0(state_, 64);
    tmp11 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{p_nArg}, TNode<Smi>{tmp10});
    ca_.Branch(tmp11, &block9, std::vector<Node*>{phi_bb5_1, phi_bb5_2}, &block10, std::vector<Node*>{phi_bb5_1, phi_bb5_2});
  }

  TNode<Smi> phi_bb9_1;
  TNode<Smi> phi_bb9_2;
  TNode<BoolT> tmp12;
  if (block9.is_used()) {
    ca_.Bind(&block9, &phi_bb9_1, &phi_bb9_2);
    tmp12 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    ca_.Goto(&block11, phi_bb9_1, phi_bb9_2, tmp12);
  }

  TNode<Smi> phi_bb10_1;
  TNode<Smi> phi_bb10_2;
  TNode<Smi> tmp13;
  TNode<BoolT> tmp14;
  if (block10.is_used()) {
    ca_.Bind(&block10, &phi_bb10_1, &phi_bb10_2);
    tmp13 = FromConstexpr_Smi_constexpr_int31_0(state_, 32);
    tmp14 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{tmp13}, TNode<Smi>{tmp9});
    ca_.Branch(tmp14, &block12, std::vector<Node*>{phi_bb10_1, phi_bb10_2}, &block13, std::vector<Node*>{phi_bb10_1, phi_bb10_2});
  }

  TNode<Smi> phi_bb12_1;
  TNode<Smi> phi_bb12_2;
  TNode<Smi> tmp15;
  TNode<BoolT> tmp16;
  if (block12.is_used()) {
    ca_.Bind(&block12, &phi_bb12_1, &phi_bb12_2);
    tmp15 = FromConstexpr_Smi_constexpr_int31_0(state_, 64);
    tmp16 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{tmp9}, TNode<Smi>{tmp15});
    ca_.Goto(&block14, phi_bb12_1, phi_bb12_2, tmp16);
  }

  TNode<Smi> phi_bb13_1;
  TNode<Smi> phi_bb13_2;
  TNode<BoolT> tmp17;
  if (block13.is_used()) {
    ca_.Bind(&block13, &phi_bb13_1, &phi_bb13_2);
    tmp17 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block14, phi_bb13_1, phi_bb13_2, tmp17);
  }

  TNode<Smi> phi_bb14_1;
  TNode<Smi> phi_bb14_2;
  TNode<BoolT> phi_bb14_6;
  if (block14.is_used()) {
    ca_.Bind(&block14, &phi_bb14_1, &phi_bb14_2, &phi_bb14_6);
    ca_.Goto(&block11, phi_bb14_1, phi_bb14_2, phi_bb14_6);
  }

  TNode<Smi> phi_bb11_1;
  TNode<Smi> phi_bb11_2;
  TNode<BoolT> phi_bb11_5;
  if (block11.is_used()) {
    ca_.Bind(&block11, &phi_bb11_1, &phi_bb11_2, &phi_bb11_5);
    ca_.Branch(phi_bb11_5, &block7, std::vector<Node*>{phi_bb11_1, phi_bb11_2}, &block8, std::vector<Node*>{phi_bb11_1, phi_bb11_2});
  }

  TNode<Smi> phi_bb8_1;
  TNode<Smi> phi_bb8_2;
  if (block8.is_used()) {
    ca_.Bind(&block8, &phi_bb8_1, &phi_bb8_2);
    CodeStubAssembler(state_).FailAssert("Torque assert 'nArg < 64 || (32 <= minRunLength && minRunLength <= 64)' failed", "third_party/v8/builtins/array-sort.tq", 1176);
  }

  TNode<Smi> phi_bb7_1;
  TNode<Smi> phi_bb7_2;
  if (block7.is_used()) {
    ca_.Bind(&block7, &phi_bb7_1, &phi_bb7_2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1165);
    ca_.Goto(&block15);
  }

    ca_.Bind(&block15);
  return TNode<Smi>{tmp9};
}

TNode<BoolT> RunInvariantEstablished_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<FixedArray> p_pendingRuns, TNode<Smi> p_n) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Smi> tmp0;
  TNode<BoolT> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1183);
    tmp0 = FromConstexpr_Smi_constexpr_int31_0(state_, 2);
    tmp1 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{p_n}, TNode<Smi>{tmp0});
    ca_.Branch(tmp1, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  TNode<BoolT> tmp2;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    tmp2 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    ca_.Goto(&block1, tmp2);
  }

  TNode<Smi> tmp3;
  TNode<Smi> tmp4;
  TNode<Smi> tmp5;
  TNode<Smi> tmp6;
  TNode<Smi> tmp7;
  TNode<Smi> tmp8;
  TNode<Smi> tmp9;
  TNode<Smi> tmp10;
  TNode<BoolT> tmp11;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1185);
    tmp3 = GetPendingRunLength_0(state_, TNode<Context>{p_context}, TNode<FixedArray>{p_pendingRuns}, TNode<Smi>{p_n});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1186);
    tmp4 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp5 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{p_n}, TNode<Smi>{tmp4});
    tmp6 = GetPendingRunLength_0(state_, TNode<Context>{p_context}, TNode<FixedArray>{p_pendingRuns}, TNode<Smi>{tmp5});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1187);
    tmp7 = FromConstexpr_Smi_constexpr_int31_0(state_, 2);
    tmp8 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{p_n}, TNode<Smi>{tmp7});
    tmp9 = GetPendingRunLength_0(state_, TNode<Context>{p_context}, TNode<FixedArray>{p_pendingRuns}, TNode<Smi>{tmp8});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1189);
    tmp10 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp6}, TNode<Smi>{tmp3});
    tmp11 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{tmp9}, TNode<Smi>{tmp10});
    ca_.Goto(&block1, tmp11);
  }

  TNode<BoolT> phi_bb1_3;
  if (block1.is_used()) {
    ca_.Bind(&block1, &phi_bb1_3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1181);
    ca_.Goto(&block4);
  }

    ca_.Bind(&block4);
  return TNode<BoolT>{phi_bb1_3};
}

void MergeCollapse_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<SortState> p_sortState) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block14(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block16(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<FixedArray> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1202);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 48);
    tmp1 = CodeStubAssembler(state_).LoadReference<FixedArray>(CodeStubAssembler::Reference{p_sortState, tmp0});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1205);
    ca_.Goto(&block4);
  }

  TNode<Smi> tmp2;
  TNode<Smi> tmp3;
  TNode<BoolT> tmp4;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    tmp2 = GetPendingRunsSize_0(state_, TNode<Context>{p_context}, TNode<SortState>{p_sortState});
    tmp3 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp4 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{tmp2}, TNode<Smi>{tmp3});
    ca_.Branch(tmp4, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  TNode<Smi> tmp5;
  TNode<Smi> tmp6;
  TNode<Smi> tmp7;
  TNode<Smi> tmp8;
  TNode<Smi> tmp9;
  TNode<BoolT> tmp10;
  TNode<BoolT> tmp11;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1206);
    tmp5 = GetPendingRunsSize_0(state_, TNode<Context>{p_context}, TNode<SortState>{p_sortState});
    tmp6 = FromConstexpr_Smi_constexpr_int31_0(state_, 2);
    tmp7 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp5}, TNode<Smi>{tmp6});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1208);
    tmp8 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp9 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp7}, TNode<Smi>{tmp8});
    tmp10 = RunInvariantEstablished_0(state_, TNode<Context>{p_context}, TNode<FixedArray>{tmp1}, TNode<Smi>{tmp9});
    tmp11 = CodeStubAssembler(state_).Word32BinaryNot(TNode<BoolT>{tmp10});
    ca_.Branch(tmp11, &block7, std::vector<Node*>{}, &block8, std::vector<Node*>{});
  }

  TNode<BoolT> tmp12;
  if (block7.is_used()) {
    ca_.Bind(&block7);
    tmp12 = FromConstexpr_bool_constexpr_bool_0(state_, true);
    ca_.Goto(&block9, tmp12);
  }

  TNode<BoolT> tmp13;
  TNode<BoolT> tmp14;
  if (block8.is_used()) {
    ca_.Bind(&block8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1209);
    tmp13 = RunInvariantEstablished_0(state_, TNode<Context>{p_context}, TNode<FixedArray>{tmp1}, TNode<Smi>{tmp7});
    tmp14 = CodeStubAssembler(state_).Word32BinaryNot(TNode<BoolT>{tmp13});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1208);
    ca_.Goto(&block9, tmp14);
  }

  TNode<BoolT> phi_bb9_5;
  if (block9.is_used()) {
    ca_.Bind(&block9, &phi_bb9_5);
    ca_.Branch(phi_bb9_5, &block5, std::vector<Node*>{}, &block6, std::vector<Node*>{});
  }

  TNode<Smi> tmp15;
  TNode<Smi> tmp16;
  TNode<Smi> tmp17;
  TNode<Smi> tmp18;
  TNode<Smi> tmp19;
  TNode<Smi> tmp20;
  TNode<BoolT> tmp21;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1210);
    tmp15 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp16 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp7}, TNode<Smi>{tmp15});
    tmp17 = GetPendingRunLength_0(state_, TNode<Context>{p_context}, TNode<FixedArray>{tmp1}, TNode<Smi>{tmp16});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1211);
    tmp18 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp19 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp7}, TNode<Smi>{tmp18});
    tmp20 = GetPendingRunLength_0(state_, TNode<Context>{p_context}, TNode<FixedArray>{tmp1}, TNode<Smi>{tmp19});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1210);
    tmp21 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{tmp17}, TNode<Smi>{tmp20});
    ca_.Branch(tmp21, &block11, std::vector<Node*>{}, &block12, std::vector<Node*>{tmp7});
  }

  TNode<Smi> tmp22;
  TNode<Smi> tmp23;
  if (block11.is_used()) {
    ca_.Bind(&block11);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1212);
    tmp22 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp23 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp7}, TNode<Smi>{tmp22});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1210);
    ca_.Goto(&block12, tmp23);
  }

  TNode<Smi> phi_bb12_3;
  TNode<Smi> tmp24;
  if (block12.is_used()) {
    ca_.Bind(&block12, &phi_bb12_3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1215);
    tmp24 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kMergeAt, p_context, p_sortState, phi_bb12_3));
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1208);
    ca_.Goto(&block10, phi_bb12_3);
  }

  TNode<Smi> tmp25;
  TNode<Smi> tmp26;
  TNode<Smi> tmp27;
  TNode<Smi> tmp28;
  TNode<BoolT> tmp29;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1217);
    tmp25 = GetPendingRunLength_0(state_, TNode<Context>{p_context}, TNode<FixedArray>{tmp1}, TNode<Smi>{tmp7});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1218);
    tmp26 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp27 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp7}, TNode<Smi>{tmp26});
    tmp28 = GetPendingRunLength_0(state_, TNode<Context>{p_context}, TNode<FixedArray>{tmp1}, TNode<Smi>{tmp27});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1217);
    tmp29 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{tmp25}, TNode<Smi>{tmp28});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1216);
    ca_.Branch(tmp29, &block13, std::vector<Node*>{}, &block14, std::vector<Node*>{});
  }

  TNode<Smi> tmp30;
  if (block13.is_used()) {
    ca_.Bind(&block13);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1219);
    tmp30 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kMergeAt, p_context, p_sortState, tmp7));
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1208);
    ca_.Goto(&block10, tmp7);
  }

  if (block14.is_used()) {
    ca_.Bind(&block14);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1221);
    ca_.Goto(&block3);
  }

  TNode<Smi> phi_bb10_3;
  if (block10.is_used()) {
    ca_.Bind(&block10, &phi_bb10_3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1205);
    ca_.Goto(&block4);
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1201);
    ca_.Goto(&block16);
  }

    ca_.Bind(&block16);
}

void MergeForceCollapse_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<SortState> p_sortState) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<BoolT> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<FixedArray> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1230);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 48);
    tmp1 = CodeStubAssembler(state_).LoadReference<FixedArray>(CodeStubAssembler::Reference{p_sortState, tmp0});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1233);
    ca_.Goto(&block4);
  }

  TNode<Smi> tmp2;
  TNode<Smi> tmp3;
  TNode<BoolT> tmp4;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    tmp2 = GetPendingRunsSize_0(state_, TNode<Context>{p_context}, TNode<SortState>{p_sortState});
    tmp3 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp4 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{tmp2}, TNode<Smi>{tmp3});
    ca_.Branch(tmp4, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  TNode<Smi> tmp5;
  TNode<Smi> tmp6;
  TNode<Smi> tmp7;
  TNode<Smi> tmp8;
  TNode<BoolT> tmp9;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1234);
    tmp5 = GetPendingRunsSize_0(state_, TNode<Context>{p_context}, TNode<SortState>{p_sortState});
    tmp6 = FromConstexpr_Smi_constexpr_int31_0(state_, 2);
    tmp7 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp5}, TNode<Smi>{tmp6});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1236);
    tmp8 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp9 = CodeStubAssembler(state_).SmiGreaterThan(TNode<Smi>{tmp7}, TNode<Smi>{tmp8});
    ca_.Branch(tmp9, &block7, std::vector<Node*>{}, &block8, std::vector<Node*>{});
  }

  TNode<Smi> tmp10;
  TNode<Smi> tmp11;
  TNode<Smi> tmp12;
  TNode<Smi> tmp13;
  TNode<Smi> tmp14;
  TNode<Smi> tmp15;
  TNode<BoolT> tmp16;
  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1237);
    tmp10 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp11 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp7}, TNode<Smi>{tmp10});
    tmp12 = GetPendingRunLength_0(state_, TNode<Context>{p_context}, TNode<FixedArray>{tmp1}, TNode<Smi>{tmp11});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1238);
    tmp13 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp14 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp7}, TNode<Smi>{tmp13});
    tmp15 = GetPendingRunLength_0(state_, TNode<Context>{p_context}, TNode<FixedArray>{tmp1}, TNode<Smi>{tmp14});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1237);
    tmp16 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{tmp12}, TNode<Smi>{tmp15});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1236);
    ca_.Goto(&block9, tmp16);
  }

  TNode<BoolT> tmp17;
  if (block8.is_used()) {
    ca_.Bind(&block8);
    tmp17 = FromConstexpr_bool_constexpr_bool_0(state_, false);
    ca_.Goto(&block9, tmp17);
  }

  TNode<BoolT> phi_bb9_5;
  if (block9.is_used()) {
    ca_.Bind(&block9, &phi_bb9_5);
    ca_.Branch(phi_bb9_5, &block5, std::vector<Node*>{}, &block6, std::vector<Node*>{tmp7});
  }

  TNode<Smi> tmp18;
  TNode<Smi> tmp19;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1239);
    tmp18 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp19 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{tmp7}, TNode<Smi>{tmp18});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1236);
    ca_.Goto(&block6, tmp19);
  }

  TNode<Smi> phi_bb6_3;
  TNode<Smi> tmp20;
  if (block6.is_used()) {
    ca_.Bind(&block6, &phi_bb6_3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1241);
    tmp20 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kMergeAt, p_context, p_sortState, phi_bb6_3));
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1233);
    ca_.Goto(&block4);
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1228);
    ca_.Goto(&block10);
  }

    ca_.Bind(&block10);
}

void ArrayTimSortImpl_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<SortState> p_sortState, TNode<Smi> p_length) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block10(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block12(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Smi> tmp0;
  TNode<BoolT> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1247);
    tmp0 = FromConstexpr_Smi_constexpr_int31_0(state_, 2);
    tmp1 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{p_length}, TNode<Smi>{tmp0});
    ca_.Branch(tmp1, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.Goto(&block1);
  }

  TNode<Smi> tmp2;
  TNode<Smi> tmp3;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1252);
    tmp2 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1253);
    tmp3 = ComputeMinRunLength_0(state_, TNode<Smi>{p_length});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1254);
    ca_.Goto(&block6, p_length, tmp2);
  }

  TNode<Smi> phi_bb6_3;
  TNode<Smi> phi_bb6_4;
  TNode<Smi> tmp4;
  TNode<BoolT> tmp5;
  if (block6.is_used()) {
    ca_.Bind(&block6, &phi_bb6_3, &phi_bb6_4);
    tmp4 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp5 = CodeStubAssembler(state_).SmiNotEqual(TNode<Smi>{phi_bb6_3}, TNode<Smi>{tmp4});
    ca_.Branch(tmp5, &block4, std::vector<Node*>{phi_bb6_3, phi_bb6_4}, &block5, std::vector<Node*>{phi_bb6_3, phi_bb6_4});
  }

  TNode<Smi> phi_bb4_3;
  TNode<Smi> phi_bb4_4;
  TNode<Smi> tmp6;
  TNode<Smi> tmp7;
  TNode<BoolT> tmp8;
  if (block4.is_used()) {
    ca_.Bind(&block4, &phi_bb4_3, &phi_bb4_4);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1255);
    tmp6 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb4_4}, TNode<Smi>{phi_bb4_3});
    tmp7 = CountAndMakeRun_0(state_, TNode<Context>{p_context}, TNode<SortState>{p_sortState}, TNode<Smi>{phi_bb4_4}, TNode<Smi>{tmp6});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1258);
    tmp8 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{tmp7}, TNode<Smi>{tmp3});
    ca_.Branch(tmp8, &block7, std::vector<Node*>{phi_bb4_3, phi_bb4_4}, &block8, std::vector<Node*>{phi_bb4_3, phi_bb4_4, tmp7});
  }

  TNode<Smi> phi_bb7_3;
  TNode<Smi> phi_bb7_4;
  TNode<Smi> tmp9;
  TNode<Smi> tmp10;
  TNode<Smi> tmp11;
  if (block7.is_used()) {
    ca_.Bind(&block7, &phi_bb7_3, &phi_bb7_4);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1259);
    tmp9 = CodeStubAssembler(state_).SmiMin(TNode<Smi>{tmp3}, TNode<Smi>{phi_bb7_3});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1260);
    tmp10 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb7_4}, TNode<Smi>{tmp7});
    tmp11 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb7_4}, TNode<Smi>{tmp9});
    BinaryInsertionSort_0(state_, TNode<Context>{p_context}, TNode<SortState>{p_sortState}, TNode<Smi>{phi_bb7_4}, TNode<Smi>{tmp10}, TNode<Smi>{tmp11});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1258);
    ca_.Goto(&block8, phi_bb7_3, phi_bb7_4, tmp9);
  }

  TNode<Smi> phi_bb8_3;
  TNode<Smi> phi_bb8_4;
  TNode<Smi> phi_bb8_6;
  TNode<Smi> tmp12;
  TNode<Smi> tmp13;
  if (block8.is_used()) {
    ca_.Bind(&block8, &phi_bb8_3, &phi_bb8_4, &phi_bb8_6);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1265);
    PushRun_0(state_, TNode<Context>{p_context}, TNode<SortState>{p_sortState}, TNode<Smi>{phi_bb8_4}, TNode<Smi>{phi_bb8_6});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1267);
    MergeCollapse_0(state_, TNode<Context>{p_context}, TNode<SortState>{p_sortState});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1270);
    tmp12 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb8_4}, TNode<Smi>{phi_bb8_6});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1271);
    tmp13 = CodeStubAssembler(state_).SmiSub(TNode<Smi>{phi_bb8_3}, TNode<Smi>{phi_bb8_6});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1254);
    ca_.Goto(&block6, tmp13, tmp12);
  }

  TNode<Smi> phi_bb5_3;
  TNode<Smi> phi_bb5_4;
  TNode<Smi> tmp14;
  TNode<Smi> tmp15;
  TNode<BoolT> tmp16;
  if (block5.is_used()) {
    ca_.Bind(&block5, &phi_bb5_3, &phi_bb5_4);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1274);
    MergeForceCollapse_0(state_, TNode<Context>{p_context}, TNode<SortState>{p_sortState});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1275);
    tmp14 = GetPendingRunsSize_0(state_, TNode<Context>{p_context}, TNode<SortState>{p_sortState});
    tmp15 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp16 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp14}, TNode<Smi>{tmp15});
    ca_.Branch(tmp16, &block9, std::vector<Node*>{phi_bb5_3, phi_bb5_4}, &block10, std::vector<Node*>{phi_bb5_3, phi_bb5_4});
  }

  TNode<Smi> phi_bb10_3;
  TNode<Smi> phi_bb10_4;
  if (block10.is_used()) {
    ca_.Bind(&block10, &phi_bb10_3, &phi_bb10_4);
    CodeStubAssembler(state_).FailAssert("Torque assert 'GetPendingRunsSize(sortState) == 1' failed", "third_party/v8/builtins/array-sort.tq", 1275);
  }

  TNode<Smi> phi_bb9_3;
  TNode<Smi> phi_bb9_4;
  TNode<IntPtrT> tmp17;
  TNode<FixedArray> tmp18;
  TNode<Smi> tmp19;
  TNode<Smi> tmp20;
  TNode<BoolT> tmp21;
  if (block9.is_used()) {
    ca_.Bind(&block9, &phi_bb9_3, &phi_bb9_4);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1276);
    tmp17 = FromConstexpr_intptr_constexpr_int31_0(state_, 48);
    tmp18 = CodeStubAssembler(state_).LoadReference<FixedArray>(CodeStubAssembler::Reference{p_sortState, tmp17});
    tmp19 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    tmp20 = GetPendingRunLength_0(state_, TNode<Context>{p_context}, TNode<FixedArray>{tmp18}, TNode<Smi>{tmp19});
    tmp21 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp20}, TNode<Smi>{p_length});
    ca_.Branch(tmp21, &block11, std::vector<Node*>{phi_bb9_3, phi_bb9_4}, &block12, std::vector<Node*>{phi_bb9_3, phi_bb9_4});
  }

  TNode<Smi> phi_bb12_3;
  TNode<Smi> phi_bb12_4;
  if (block12.is_used()) {
    ca_.Bind(&block12, &phi_bb12_3, &phi_bb12_4);
    CodeStubAssembler(state_).FailAssert("Torque assert 'GetPendingRunLength(sortState.pendingRuns, 0) == length' failed", "third_party/v8/builtins/array-sort.tq", 1276);
  }

  TNode<Smi> phi_bb11_3;
  TNode<Smi> phi_bb11_4;
  if (block11.is_used()) {
    ca_.Bind(&block11, &phi_bb11_3, &phi_bb11_4);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1245);
    ca_.Goto(&block1);
  }

  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.Goto(&block13);
  }

    ca_.Bind(&block13);
}

TNode<Smi> CompactReceiverElementsIntoWorkArray_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<SortState> p_sortState) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, Smi, Smi, Smi> block14(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, Smi, Smi, Smi> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, Smi, Smi, Smi> block16(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, Smi, Smi, Smi> block17(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, Smi, Smi, Smi> block19(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, Smi, Smi, Smi> block20(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, Smi, Smi, Smi> block25(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, Smi, Smi, Smi> block24(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, Smi, Smi, Smi> block26(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, Smi, Smi, Smi> block30(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, Smi, Smi, Smi> block29(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, Smi, Smi, Smi> block32(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, Smi, Smi, Smi> block31(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, Smi, Smi, Smi> block34(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, Smi, Smi, Smi> block33(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, Smi, Smi, Smi> block27(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, Smi, Smi, Smi, FixedArray, FixedArray, IntPtrT, IntPtrT, HeapObject, IntPtrT, IntPtrT> block39(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, Smi, Smi, Smi, FixedArray, FixedArray, IntPtrT, IntPtrT, HeapObject, IntPtrT, IntPtrT> block40(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, Smi, Smi, Smi> block21(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, Smi, Smi, Smi> block18(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<FixedArray, IntPtrT, IntPtrT, Smi, Smi, Smi> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block42(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<FixedArray> tmp1;
  TNode<IntPtrT> tmp2;
  TNode<FixedArray> tmp3;
  TNode<IntPtrT> tmp4;
  TNode<Smi> tmp5;
  TNode<IntPtrT> tmp6;
  TNode<IntPtrT> tmp7;
  TNode<IntPtrT> tmp8;
  TNode<BuiltinPtr> tmp9;
  TNode<IntPtrT> tmp10;
  TNode<Number> tmp11;
  TNode<BoolT> tmp12;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1283);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 52);
    tmp1 = CodeStubAssembler(state_).LoadReference<FixedArray>(CodeStubAssembler::Reference{p_sortState, tmp0});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1284);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 52);
    tmp3 = CodeStubAssembler(state_).LoadReference<FixedArray>(CodeStubAssembler::Reference{p_sortState, tmp2});
    tmp4 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp5 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp3, tmp4});
    tmp6 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp5});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1282);
    tmp7 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1288);
    tmp8 = FromConstexpr_intptr_constexpr_int31_0(state_, 24);
    tmp9 = CodeStubAssembler(state_).LoadReference<BuiltinPtr>(CodeStubAssembler::Reference{p_sortState, tmp8});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1292);
    tmp10 = FromConstexpr_intptr_constexpr_int31_0(state_, 12);
    tmp11 = CodeStubAssembler(state_).LoadReference<Number>(CodeStubAssembler::Reference{p_sortState, tmp10});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1293);
    tmp12 = CodeStubAssembler(state_).IsNumberNormalized(TNode<Number>{tmp11});
    ca_.Branch(tmp12, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    CodeStubAssembler(state_).FailAssert("Torque assert 'IsNumberNormalized(receiverLength)' failed", "third_party/v8/builtins/array-sort.tq", 1293);
  }

  TNode<BoolT> tmp13;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1295);
    tmp13 = CodeStubAssembler(state_).TaggedIsSmi(TNode<Object>{tmp11});
    ca_.Branch(tmp13, &block4, std::vector<Node*>{}, &block5, std::vector<Node*>{});
  }

  TNode<Smi> tmp14;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1296);
    tmp14 = UnsafeCast_Smi_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp11});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1295);
    ca_.Goto(&block6, tmp14);
  }

  TNode<UintPtrT> tmp15;
  TNode<Smi> tmp16;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1297);
    tmp15 = kSmiMax_0(state_);
    compiler::CodeAssemblerLabel label17(&ca_);
    tmp16 = Convert_PositiveSmi_uintptr_0(state_, TNode<UintPtrT>{tmp15}, &label17);
    ca_.Goto(&block10);
    if (label17.is_used()) {
      ca_.Bind(&label17);
      ca_.Goto(&block11);
    }
  }

  if (block11.is_used()) {
    ca_.Bind(&block11);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at third_party/v8/builtins/array-sort.tq:1297:47");
    CodeStubAssembler(state_).Unreachable();
  }

  if (block10.is_used()) {
    ca_.Bind(&block10);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1295);
    ca_.Goto(&block6, tmp16);
  }

  TNode<Smi> phi_bb6_7;
  TNode<Smi> tmp18;
  TNode<Smi> tmp19;
  if (block6.is_used()) {
    ca_.Bind(&block6, &phi_bb6_7);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1301);
    tmp18 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1302);
    tmp19 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    ca_.Goto(&block14, tmp1, tmp6, tmp7, phi_bb6_7, tmp18, tmp19);
  }

  TNode<FixedArray> phi_bb14_2;
  TNode<IntPtrT> phi_bb14_3;
  TNode<IntPtrT> phi_bb14_4;
  TNode<Smi> phi_bb14_7;
  TNode<Smi> phi_bb14_8;
  TNode<Smi> phi_bb14_9;
  TNode<BoolT> tmp20;
  if (block14.is_used()) {
    ca_.Bind(&block14, &phi_bb14_2, &phi_bb14_3, &phi_bb14_4, &phi_bb14_7, &phi_bb14_8, &phi_bb14_9);
    tmp20 = NumberIsLessThan_0(state_, TNode<Number>{phi_bb14_9}, TNode<Number>{tmp11});
    ca_.Branch(tmp20, &block12, std::vector<Node*>{phi_bb14_2, phi_bb14_3, phi_bb14_4, phi_bb14_7, phi_bb14_8, phi_bb14_9}, &block13, std::vector<Node*>{phi_bb14_2, phi_bb14_3, phi_bb14_4, phi_bb14_7, phi_bb14_8, phi_bb14_9});
  }

  TNode<FixedArray> phi_bb12_2;
  TNode<IntPtrT> phi_bb12_3;
  TNode<IntPtrT> phi_bb12_4;
  TNode<Smi> phi_bb12_7;
  TNode<Smi> phi_bb12_8;
  TNode<Smi> phi_bb12_9;
  TNode<Object> tmp21;
  TNode<Oddball> tmp22;
  TNode<BoolT> tmp23;
  if (block12.is_used()) {
    ca_.Bind(&block12, &phi_bb12_2, &phi_bb12_3, &phi_bb12_4, &phi_bb12_7, &phi_bb12_8, &phi_bb12_9);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1303);
tmp21 = CodeStubAssembler(state_).CallBuiltinPointer(Builtins::CallableFor(ca_.isolate(),ExampleBuiltinForTorqueFunctionPointerType(5)).descriptor(), tmp9, p_context, p_sortState, phi_bb12_9);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1305);
    tmp22 = TheHole_0(state_);
    tmp23 = CodeStubAssembler(state_).TaggedEqual(TNode<Object>{tmp21}, TNode<HeapObject>{tmp22});
    ca_.Branch(tmp23, &block16, std::vector<Node*>{phi_bb12_2, phi_bb12_3, phi_bb12_4, phi_bb12_7, phi_bb12_8, phi_bb12_9}, &block17, std::vector<Node*>{phi_bb12_2, phi_bb12_3, phi_bb12_4, phi_bb12_7, phi_bb12_8, phi_bb12_9});
  }

  TNode<FixedArray> phi_bb16_2;
  TNode<IntPtrT> phi_bb16_3;
  TNode<IntPtrT> phi_bb16_4;
  TNode<Smi> phi_bb16_7;
  TNode<Smi> phi_bb16_8;
  TNode<Smi> phi_bb16_9;
  if (block16.is_used()) {
    ca_.Bind(&block16, &phi_bb16_2, &phi_bb16_3, &phi_bb16_4, &phi_bb16_7, &phi_bb16_8, &phi_bb16_9);
    ca_.Goto(&block18, phi_bb16_2, phi_bb16_3, phi_bb16_4, phi_bb16_7, phi_bb16_8, phi_bb16_9);
  }

  TNode<FixedArray> phi_bb17_2;
  TNode<IntPtrT> phi_bb17_3;
  TNode<IntPtrT> phi_bb17_4;
  TNode<Smi> phi_bb17_7;
  TNode<Smi> phi_bb17_8;
  TNode<Smi> phi_bb17_9;
  TNode<Oddball> tmp24;
  TNode<BoolT> tmp25;
  if (block17.is_used()) {
    ca_.Bind(&block17, &phi_bb17_2, &phi_bb17_3, &phi_bb17_4, &phi_bb17_7, &phi_bb17_8, &phi_bb17_9);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1308);
    tmp24 = Undefined_0(state_);
    tmp25 = CodeStubAssembler(state_).TaggedEqual(TNode<Object>{tmp21}, TNode<HeapObject>{tmp24});
    ca_.Branch(tmp25, &block19, std::vector<Node*>{phi_bb17_2, phi_bb17_3, phi_bb17_4, phi_bb17_7, phi_bb17_8, phi_bb17_9}, &block20, std::vector<Node*>{phi_bb17_2, phi_bb17_3, phi_bb17_4, phi_bb17_7, phi_bb17_8, phi_bb17_9});
  }

  TNode<FixedArray> phi_bb19_2;
  TNode<IntPtrT> phi_bb19_3;
  TNode<IntPtrT> phi_bb19_4;
  TNode<Smi> phi_bb19_7;
  TNode<Smi> phi_bb19_8;
  TNode<Smi> phi_bb19_9;
  TNode<Smi> tmp26;
  TNode<Smi> tmp27;
  if (block19.is_used()) {
    ca_.Bind(&block19, &phi_bb19_2, &phi_bb19_3, &phi_bb19_4, &phi_bb19_7, &phi_bb19_8, &phi_bb19_9);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1309);
    tmp26 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp27 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb19_8}, TNode<Smi>{tmp26});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1308);
    ca_.Goto(&block21, phi_bb19_2, phi_bb19_3, phi_bb19_4, phi_bb19_7, tmp27, phi_bb19_9);
  }

  TNode<FixedArray> phi_bb20_2;
  TNode<IntPtrT> phi_bb20_3;
  TNode<IntPtrT> phi_bb20_4;
  TNode<Smi> phi_bb20_7;
  TNode<Smi> phi_bb20_8;
  TNode<Smi> phi_bb20_9;
  TNode<BoolT> tmp28;
  if (block20.is_used()) {
    ca_.Bind(&block20, &phi_bb20_2, &phi_bb20_3, &phi_bb20_4, &phi_bb20_7, &phi_bb20_8, &phi_bb20_9);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 20);
    tmp28 = CodeStubAssembler(state_).IntPtrLessThanOrEqual(TNode<IntPtrT>{phi_bb20_4}, TNode<IntPtrT>{phi_bb20_3});
    ca_.Branch(tmp28, &block24, std::vector<Node*>{phi_bb20_2, phi_bb20_3, phi_bb20_4, phi_bb20_7, phi_bb20_8, phi_bb20_9}, &block25, std::vector<Node*>{phi_bb20_2, phi_bb20_3, phi_bb20_4, phi_bb20_7, phi_bb20_8, phi_bb20_9});
  }

  TNode<FixedArray> phi_bb25_2;
  TNode<IntPtrT> phi_bb25_3;
  TNode<IntPtrT> phi_bb25_4;
  TNode<Smi> phi_bb25_7;
  TNode<Smi> phi_bb25_8;
  TNode<Smi> phi_bb25_9;
  if (block25.is_used()) {
    ca_.Bind(&block25, &phi_bb25_2, &phi_bb25_3, &phi_bb25_4, &phi_bb25_7, &phi_bb25_8, &phi_bb25_9);
    CodeStubAssembler(state_).FailAssert("Torque assert 'this.length <= this.capacity' failed", "src/builtins/growable-fixed-array.tq", 20);
  }

  TNode<FixedArray> phi_bb24_2;
  TNode<IntPtrT> phi_bb24_3;
  TNode<IntPtrT> phi_bb24_4;
  TNode<Smi> phi_bb24_7;
  TNode<Smi> phi_bb24_8;
  TNode<Smi> phi_bb24_9;
  TNode<BoolT> tmp29;
  if (block24.is_used()) {
    ca_.Bind(&block24, &phi_bb24_2, &phi_bb24_3, &phi_bb24_4, &phi_bb24_7, &phi_bb24_8, &phi_bb24_9);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 21);
    tmp29 = CodeStubAssembler(state_).WordEqual(TNode<IntPtrT>{phi_bb24_3}, TNode<IntPtrT>{phi_bb24_4});
    ca_.Branch(tmp29, &block26, std::vector<Node*>{phi_bb24_2, phi_bb24_3, phi_bb24_4, phi_bb24_7, phi_bb24_8, phi_bb24_9}, &block27, std::vector<Node*>{phi_bb24_2, phi_bb24_3, phi_bb24_4, phi_bb24_7, phi_bb24_8, phi_bb24_9});
  }

  TNode<FixedArray> phi_bb26_2;
  TNode<IntPtrT> phi_bb26_3;
  TNode<IntPtrT> phi_bb26_4;
  TNode<Smi> phi_bb26_7;
  TNode<Smi> phi_bb26_8;
  TNode<Smi> phi_bb26_9;
  TNode<IntPtrT> tmp30;
  TNode<IntPtrT> tmp31;
  TNode<IntPtrT> tmp32;
  TNode<IntPtrT> tmp33;
  TNode<IntPtrT> tmp34;
  TNode<IntPtrT> tmp35;
  TNode<BoolT> tmp36;
  if (block26.is_used()) {
    ca_.Bind(&block26, &phi_bb26_2, &phi_bb26_3, &phi_bb26_4, &phi_bb26_7, &phi_bb26_8, &phi_bb26_9);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 24);
    tmp30 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp31 = CodeStubAssembler(state_).WordSar(TNode<IntPtrT>{phi_bb26_3}, TNode<IntPtrT>{tmp30});
    tmp32 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{phi_bb26_3}, TNode<IntPtrT>{tmp31});
    tmp33 = FromConstexpr_intptr_constexpr_int31_0(state_, 16);
    tmp34 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp32}, TNode<IntPtrT>{tmp33});
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 13);
    tmp35 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp36 = CodeStubAssembler(state_).IntPtrGreaterThanOrEqual(TNode<IntPtrT>{phi_bb26_4}, TNode<IntPtrT>{tmp35});
    ca_.Branch(tmp36, &block29, std::vector<Node*>{phi_bb26_2, phi_bb26_4, phi_bb26_7, phi_bb26_8, phi_bb26_9}, &block30, std::vector<Node*>{phi_bb26_2, phi_bb26_4, phi_bb26_7, phi_bb26_8, phi_bb26_9});
  }

  TNode<FixedArray> phi_bb30_2;
  TNode<IntPtrT> phi_bb30_4;
  TNode<Smi> phi_bb30_7;
  TNode<Smi> phi_bb30_8;
  TNode<Smi> phi_bb30_9;
  if (block30.is_used()) {
    ca_.Bind(&block30, &phi_bb30_2, &phi_bb30_4, &phi_bb30_7, &phi_bb30_8, &phi_bb30_9);
    CodeStubAssembler(state_).FailAssert("Torque assert 'this.length >= 0' failed", "src/builtins/growable-fixed-array.tq", 13);
  }

  TNode<FixedArray> phi_bb29_2;
  TNode<IntPtrT> phi_bb29_4;
  TNode<Smi> phi_bb29_7;
  TNode<Smi> phi_bb29_8;
  TNode<Smi> phi_bb29_9;
  TNode<IntPtrT> tmp37;
  TNode<BoolT> tmp38;
  if (block29.is_used()) {
    ca_.Bind(&block29, &phi_bb29_2, &phi_bb29_4, &phi_bb29_7, &phi_bb29_8, &phi_bb29_9);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 14);
    tmp37 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp38 = CodeStubAssembler(state_).IntPtrGreaterThanOrEqual(TNode<IntPtrT>{tmp34}, TNode<IntPtrT>{tmp37});
    ca_.Branch(tmp38, &block31, std::vector<Node*>{phi_bb29_2, phi_bb29_4, phi_bb29_7, phi_bb29_8, phi_bb29_9}, &block32, std::vector<Node*>{phi_bb29_2, phi_bb29_4, phi_bb29_7, phi_bb29_8, phi_bb29_9});
  }

  TNode<FixedArray> phi_bb32_2;
  TNode<IntPtrT> phi_bb32_4;
  TNode<Smi> phi_bb32_7;
  TNode<Smi> phi_bb32_8;
  TNode<Smi> phi_bb32_9;
  if (block32.is_used()) {
    ca_.Bind(&block32, &phi_bb32_2, &phi_bb32_4, &phi_bb32_7, &phi_bb32_8, &phi_bb32_9);
    CodeStubAssembler(state_).FailAssert("Torque assert 'newCapacity >= 0' failed", "src/builtins/growable-fixed-array.tq", 14);
  }

  TNode<FixedArray> phi_bb31_2;
  TNode<IntPtrT> phi_bb31_4;
  TNode<Smi> phi_bb31_7;
  TNode<Smi> phi_bb31_8;
  TNode<Smi> phi_bb31_9;
  TNode<BoolT> tmp39;
  if (block31.is_used()) {
    ca_.Bind(&block31, &phi_bb31_2, &phi_bb31_4, &phi_bb31_7, &phi_bb31_8, &phi_bb31_9);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 15);
    tmp39 = CodeStubAssembler(state_).IntPtrGreaterThanOrEqual(TNode<IntPtrT>{tmp34}, TNode<IntPtrT>{phi_bb31_4});
    ca_.Branch(tmp39, &block33, std::vector<Node*>{phi_bb31_2, phi_bb31_4, phi_bb31_7, phi_bb31_8, phi_bb31_9}, &block34, std::vector<Node*>{phi_bb31_2, phi_bb31_4, phi_bb31_7, phi_bb31_8, phi_bb31_9});
  }

  TNode<FixedArray> phi_bb34_2;
  TNode<IntPtrT> phi_bb34_4;
  TNode<Smi> phi_bb34_7;
  TNode<Smi> phi_bb34_8;
  TNode<Smi> phi_bb34_9;
  if (block34.is_used()) {
    ca_.Bind(&block34, &phi_bb34_2, &phi_bb34_4, &phi_bb34_7, &phi_bb34_8, &phi_bb34_9);
    CodeStubAssembler(state_).FailAssert("Torque assert 'newCapacity >= this.length' failed", "src/builtins/growable-fixed-array.tq", 15);
  }

  TNode<FixedArray> phi_bb33_2;
  TNode<IntPtrT> phi_bb33_4;
  TNode<Smi> phi_bb33_7;
  TNode<Smi> phi_bb33_8;
  TNode<Smi> phi_bb33_9;
  TNode<IntPtrT> tmp40;
  TNode<FixedArray> tmp41;
  if (block33.is_used()) {
    ca_.Bind(&block33, &phi_bb33_2, &phi_bb33_4, &phi_bb33_7, &phi_bb33_8, &phi_bb33_9);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 16);
    tmp40 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 17);
    tmp41 = ExtractFixedArray_0(state_, TNode<FixedArray>{phi_bb33_2}, TNode<IntPtrT>{tmp40}, TNode<IntPtrT>{phi_bb33_4}, TNode<IntPtrT>{tmp34});
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 21);
    ca_.Goto(&block27, tmp41, tmp34, phi_bb33_4, phi_bb33_7, phi_bb33_8, phi_bb33_9);
  }

  TNode<FixedArray> phi_bb27_2;
  TNode<IntPtrT> phi_bb27_3;
  TNode<IntPtrT> phi_bb27_4;
  TNode<Smi> phi_bb27_7;
  TNode<Smi> phi_bb27_8;
  TNode<Smi> phi_bb27_9;
  TNode<IntPtrT> tmp42;
  TNode<IntPtrT> tmp43;
  TNode<IntPtrT> tmp44;
  TNode<Smi> tmp45;
  TNode<IntPtrT> tmp46;
  TNode<IntPtrT> tmp47;
  TNode<IntPtrT> tmp48;
  TNode<UintPtrT> tmp49;
  TNode<UintPtrT> tmp50;
  TNode<BoolT> tmp51;
  if (block27.is_used()) {
    ca_.Bind(&block27, &phi_bb27_2, &phi_bb27_3, &phi_bb27_4, &phi_bb27_7, &phi_bb27_8, &phi_bb27_9);
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 10);
    tmp42 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp43 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp44 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp45 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{phi_bb27_2, tmp44});
    tmp46 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp45});
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 10);
    tmp47 = FromConstexpr_intptr_constexpr_int31_0(state_, 1);
    tmp48 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{phi_bb27_4}, TNode<IntPtrT>{tmp47});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp49 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{phi_bb27_4});
    tmp50 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp46});
    tmp51 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp49}, TNode<UintPtrT>{tmp50});
    ca_.Branch(tmp51, &block39, std::vector<Node*>{phi_bb27_2, phi_bb27_3, phi_bb27_7, phi_bb27_8, phi_bb27_9, phi_bb27_2, phi_bb27_2, phi_bb27_4, phi_bb27_4, phi_bb27_2, phi_bb27_4, phi_bb27_4}, &block40, std::vector<Node*>{phi_bb27_2, phi_bb27_3, phi_bb27_7, phi_bb27_8, phi_bb27_9, phi_bb27_2, phi_bb27_2, phi_bb27_4, phi_bb27_4, phi_bb27_2, phi_bb27_4, phi_bb27_4});
  }

  TNode<FixedArray> phi_bb39_2;
  TNode<IntPtrT> phi_bb39_3;
  TNode<Smi> phi_bb39_7;
  TNode<Smi> phi_bb39_8;
  TNode<Smi> phi_bb39_9;
  TNode<FixedArray> phi_bb39_13;
  TNode<FixedArray> phi_bb39_14;
  TNode<IntPtrT> phi_bb39_17;
  TNode<IntPtrT> phi_bb39_18;
  TNode<HeapObject> phi_bb39_19;
  TNode<IntPtrT> phi_bb39_22;
  TNode<IntPtrT> phi_bb39_23;
  TNode<IntPtrT> tmp52;
  TNode<IntPtrT> tmp53;
  TNode<IntPtrT> tmp54;
  TNode<HeapObject> tmp55;
  TNode<IntPtrT> tmp56;
  if (block39.is_used()) {
    ca_.Bind(&block39, &phi_bb39_2, &phi_bb39_3, &phi_bb39_7, &phi_bb39_8, &phi_bb39_9, &phi_bb39_13, &phi_bb39_14, &phi_bb39_17, &phi_bb39_18, &phi_bb39_19, &phi_bb39_22, &phi_bb39_23);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp52 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp53 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{phi_bb39_23}, TNode<IntPtrT>{tmp52});
    tmp54 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp42}, TNode<IntPtrT>{tmp53});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp55, tmp56) = NewReference_Object_0(state_, TNode<HeapObject>{phi_bb39_19}, TNode<IntPtrT>{tmp54}).Flatten();
    ca_.SetSourcePosition("../../src/builtins/growable-fixed-array.tq", 10);
    CodeStubAssembler(state_).StoreReference<Object>(CodeStubAssembler::Reference{tmp55, tmp56}, tmp21);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1308);
    ca_.Goto(&block21, phi_bb39_2, phi_bb39_3, tmp48, phi_bb39_7, phi_bb39_8, phi_bb39_9);
  }

  TNode<FixedArray> phi_bb40_2;
  TNode<IntPtrT> phi_bb40_3;
  TNode<Smi> phi_bb40_7;
  TNode<Smi> phi_bb40_8;
  TNode<Smi> phi_bb40_9;
  TNode<FixedArray> phi_bb40_13;
  TNode<FixedArray> phi_bb40_14;
  TNode<IntPtrT> phi_bb40_17;
  TNode<IntPtrT> phi_bb40_18;
  TNode<HeapObject> phi_bb40_19;
  TNode<IntPtrT> phi_bb40_22;
  TNode<IntPtrT> phi_bb40_23;
  if (block40.is_used()) {
    ca_.Bind(&block40, &phi_bb40_2, &phi_bb40_3, &phi_bb40_7, &phi_bb40_8, &phi_bb40_9, &phi_bb40_13, &phi_bb40_14, &phi_bb40_17, &phi_bb40_18, &phi_bb40_19, &phi_bb40_22, &phi_bb40_23);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 41);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:41:45");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<FixedArray> phi_bb21_2;
  TNode<IntPtrT> phi_bb21_3;
  TNode<IntPtrT> phi_bb21_4;
  TNode<Smi> phi_bb21_7;
  TNode<Smi> phi_bb21_8;
  TNode<Smi> phi_bb21_9;
  if (block21.is_used()) {
    ca_.Bind(&block21, &phi_bb21_2, &phi_bb21_3, &phi_bb21_4, &phi_bb21_7, &phi_bb21_8, &phi_bb21_9);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1305);
    ca_.Goto(&block18, phi_bb21_2, phi_bb21_3, phi_bb21_4, phi_bb21_7, phi_bb21_8, phi_bb21_9);
  }

  TNode<FixedArray> phi_bb18_2;
  TNode<IntPtrT> phi_bb18_3;
  TNode<IntPtrT> phi_bb18_4;
  TNode<Smi> phi_bb18_7;
  TNode<Smi> phi_bb18_8;
  TNode<Smi> phi_bb18_9;
  TNode<Smi> tmp57;
  TNode<Smi> tmp58;
  if (block18.is_used()) {
    ca_.Bind(&block18, &phi_bb18_2, &phi_bb18_3, &phi_bb18_4, &phi_bb18_7, &phi_bb18_8, &phi_bb18_9);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1302);
    tmp57 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp58 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb18_9}, TNode<Smi>{tmp57});
    ca_.Goto(&block14, phi_bb18_2, phi_bb18_3, phi_bb18_4, phi_bb18_7, phi_bb18_8, tmp58);
  }

  TNode<FixedArray> phi_bb13_2;
  TNode<IntPtrT> phi_bb13_3;
  TNode<IntPtrT> phi_bb13_4;
  TNode<Smi> phi_bb13_7;
  TNode<Smi> phi_bb13_8;
  TNode<Smi> phi_bb13_9;
  TNode<IntPtrT> tmp59;
  TNode<IntPtrT> tmp60;
  TNode<IntPtrT> tmp61;
  TNode<Smi> tmp62;
  if (block13.is_used()) {
    ca_.Bind(&block13, &phi_bb13_2, &phi_bb13_3, &phi_bb13_4, &phi_bb13_7, &phi_bb13_8, &phi_bb13_9);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1316);
    tmp59 = FromConstexpr_intptr_constexpr_int31_0(state_, 52);
    CodeStubAssembler(state_).StoreReference<FixedArray>(CodeStubAssembler::Reference{p_sortState, tmp59}, phi_bb13_2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1317);
    tmp60 = FromConstexpr_intptr_constexpr_int31_0(state_, 60);
    CodeStubAssembler(state_).StoreReference<Smi>(CodeStubAssembler::Reference{p_sortState, tmp60}, phi_bb13_7);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1318);
    tmp61 = FromConstexpr_intptr_constexpr_int31_0(state_, 64);
    CodeStubAssembler(state_).StoreReference<Smi>(CodeStubAssembler::Reference{p_sortState, tmp61}, phi_bb13_8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1320);
    tmp62 = Convert_Smi_intptr_0(state_, TNode<IntPtrT>{phi_bb13_4});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1279);
    ca_.Goto(&block42);
  }

    ca_.Bind(&block42);
  return TNode<Smi>{tmp62};
}

void CopyWorkArrayToReceiver_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<SortState> p_sortState, TNode<Smi> p_numberOfNonUndefined) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi> block14(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi, Smi, Smi, Smi> block15(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block19(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block17(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block18(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block23(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block21(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<Smi> block22(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block25(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<BuiltinPtr> tmp1;
  TNode<IntPtrT> tmp2;
  TNode<FixedArray> tmp3;
  TNode<IntPtrT> tmp4;
  TNode<Smi> tmp5;
  TNode<BoolT> tmp6;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1326);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 28);
    tmp1 = CodeStubAssembler(state_).LoadReference<BuiltinPtr>(CodeStubAssembler::Reference{p_sortState, tmp0});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1327);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 52);
    tmp3 = CodeStubAssembler(state_).LoadReference<FixedArray>(CodeStubAssembler::Reference{p_sortState, tmp2});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1329);
    tmp4 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp5 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp3, tmp4});
    tmp6 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{p_numberOfNonUndefined}, TNode<Smi>{tmp5});
    ca_.Branch(tmp6, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    CodeStubAssembler(state_).FailAssert("Torque assert 'numberOfNonUndefined <= workArray.length' failed", "third_party/v8/builtins/array-sort.tq", 1329);
  }

  TNode<IntPtrT> tmp7;
  TNode<Smi> tmp8;
  TNode<Smi> tmp9;
  TNode<IntPtrT> tmp10;
  TNode<Smi> tmp11;
  TNode<BoolT> tmp12;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1331);
    tmp7 = FromConstexpr_intptr_constexpr_int31_0(state_, 64);
    tmp8 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_sortState, tmp7});
    tmp9 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{p_numberOfNonUndefined}, TNode<Smi>{tmp8});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1332);
    tmp10 = FromConstexpr_intptr_constexpr_int31_0(state_, 60);
    tmp11 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_sortState, tmp10});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1331);
    tmp12 = CodeStubAssembler(state_).SmiLessThanOrEqual(TNode<Smi>{tmp9}, TNode<Smi>{tmp11});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1330);
    ca_.Branch(tmp12, &block4, std::vector<Node*>{}, &block5, std::vector<Node*>{});
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    CodeStubAssembler(state_).FailAssert("Torque assert 'numberOfNonUndefined + sortState.numberOfUndefined <= sortState.sortLength' failed", "third_party/v8/builtins/array-sort.tq", 1330);
  }

  TNode<Smi> tmp13;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1339);
    tmp13 = FromConstexpr_Smi_constexpr_int31_0(state_, 0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1340);
    ca_.Goto(&block8, tmp13);
  }

  TNode<Smi> phi_bb8_5;
  TNode<BoolT> tmp14;
  if (block8.is_used()) {
    ca_.Bind(&block8, &phi_bb8_5);
    tmp14 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{phi_bb8_5}, TNode<Smi>{p_numberOfNonUndefined});
    ca_.Branch(tmp14, &block6, std::vector<Node*>{phi_bb8_5}, &block7, std::vector<Node*>{phi_bb8_5});
  }

  TNode<Smi> phi_bb6_5;
  TNode<IntPtrT> tmp15;
  TNode<IntPtrT> tmp16;
  TNode<IntPtrT> tmp17;
  TNode<Smi> tmp18;
  TNode<IntPtrT> tmp19;
  TNode<IntPtrT> tmp20;
  TNode<UintPtrT> tmp21;
  TNode<UintPtrT> tmp22;
  TNode<BoolT> tmp23;
  if (block6.is_used()) {
    ca_.Bind(&block6, &phi_bb6_5);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1342);
    tmp15 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp16 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp17 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    ca_.SetSourcePosition("../../src/objects/fixed-array.tq", 15);
    tmp18 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{tmp3, tmp17});
    tmp19 = Convert_intptr_Smi_0(state_, TNode<Smi>{tmp18});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 54);
    tmp20 = Convert_intptr_Smi_0(state_, TNode<Smi>{phi_bb6_5});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 32);
    tmp21 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp20});
    tmp22 = Convert_uintptr_intptr_0(state_, TNode<IntPtrT>{tmp19});
    tmp23 = CodeStubAssembler(state_).UintPtrLessThan(TNode<UintPtrT>{tmp21}, TNode<UintPtrT>{tmp22});
    ca_.Branch(tmp23, &block14, std::vector<Node*>{phi_bb6_5, phi_bb6_5, phi_bb6_5, phi_bb6_5}, &block15, std::vector<Node*>{phi_bb6_5, phi_bb6_5, phi_bb6_5, phi_bb6_5});
  }

  TNode<Smi> phi_bb14_5;
  TNode<Smi> phi_bb14_8;
  TNode<Smi> phi_bb14_13;
  TNode<Smi> phi_bb14_14;
  TNode<IntPtrT> tmp24;
  TNode<IntPtrT> tmp25;
  TNode<IntPtrT> tmp26;
  TNode<HeapObject> tmp27;
  TNode<IntPtrT> tmp28;
  TNode<Object> tmp29;
  TNode<Object> tmp30;
  TNode<Smi> tmp31;
  TNode<Smi> tmp32;
  TNode<Smi> tmp33;
  if (block14.is_used()) {
    ca_.Bind(&block14, &phi_bb14_5, &phi_bb14_8, &phi_bb14_13, &phi_bb14_14);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 34);
    tmp24 = FromConstexpr_intptr_constexpr_int31_0(state_, kTaggedSize);
    tmp25 = CodeStubAssembler(state_).IntPtrMul(TNode<IntPtrT>{tmp20}, TNode<IntPtrT>{tmp24});
    tmp26 = CodeStubAssembler(state_).IntPtrAdd(TNode<IntPtrT>{tmp15}, TNode<IntPtrT>{tmp25});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 33);
    std::tie(tmp27, tmp28) = NewReference_Object_0(state_, TNode<HeapObject>{tmp3}, TNode<IntPtrT>{tmp26}).Flatten();
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1342);
    tmp29 = CodeStubAssembler(state_).LoadReference<Object>(CodeStubAssembler::Reference{tmp27, tmp28});
    tmp30 = UnsafeCast_JSAny_0(state_, TNode<Context>{p_context}, TNode<Object>{tmp29});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1341);
tmp31 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltinPointer(Builtins::CallableFor(ca_.isolate(),ExampleBuiltinForTorqueFunctionPointerType(6)).descriptor(), tmp1, p_context, p_sortState, phi_bb14_8, tmp30));
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1340);
    tmp32 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp33 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb14_5}, TNode<Smi>{tmp32});
    ca_.Goto(&block8, tmp33);
  }

  TNode<Smi> phi_bb15_5;
  TNode<Smi> phi_bb15_8;
  TNode<Smi> phi_bb15_13;
  TNode<Smi> phi_bb15_14;
  if (block15.is_used()) {
    ca_.Bind(&block15, &phi_bb15_5, &phi_bb15_8, &phi_bb15_13, &phi_bb15_14);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 55);
    CodeStubAssembler(state_).Print("halting because of 'unreachable' at src/builtins/torque-internal.tq:55:41");
    CodeStubAssembler(state_).Unreachable();
  }

  TNode<Smi> phi_bb7_5;
  TNode<IntPtrT> tmp34;
  TNode<Smi> tmp35;
  TNode<Smi> tmp36;
  if (block7.is_used()) {
    ca_.Bind(&block7, &phi_bb7_5);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1346);
    tmp34 = FromConstexpr_intptr_constexpr_int31_0(state_, 64);
    tmp35 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_sortState, tmp34});
    tmp36 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{tmp35}, TNode<Smi>{p_numberOfNonUndefined});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1347);
    ca_.Goto(&block19, phi_bb7_5);
  }

  TNode<Smi> phi_bb19_5;
  TNode<BoolT> tmp37;
  if (block19.is_used()) {
    ca_.Bind(&block19, &phi_bb19_5);
    tmp37 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{phi_bb19_5}, TNode<Smi>{tmp36});
    ca_.Branch(tmp37, &block17, std::vector<Node*>{phi_bb19_5}, &block18, std::vector<Node*>{phi_bb19_5});
  }

  TNode<Smi> phi_bb17_5;
  TNode<Oddball> tmp38;
  TNode<Smi> tmp39;
  TNode<Smi> tmp40;
  TNode<Smi> tmp41;
  if (block17.is_used()) {
    ca_.Bind(&block17, &phi_bb17_5);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1348);
    tmp38 = Undefined_0(state_);
tmp39 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltinPointer(Builtins::CallableFor(ca_.isolate(),ExampleBuiltinForTorqueFunctionPointerType(6)).descriptor(), tmp1, p_context, p_sortState, phi_bb17_5, tmp38));
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1347);
    tmp40 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp41 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb17_5}, TNode<Smi>{tmp40});
    ca_.Goto(&block19, tmp41);
  }

  TNode<Smi> phi_bb18_5;
  TNode<IntPtrT> tmp42;
  TNode<Smi> tmp43;
  TNode<IntPtrT> tmp44;
  TNode<BuiltinPtr> tmp45;
  if (block18.is_used()) {
    ca_.Bind(&block18, &phi_bb18_5);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1351);
    tmp42 = FromConstexpr_intptr_constexpr_int31_0(state_, 60);
    tmp43 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_sortState, tmp42});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1352);
    tmp44 = FromConstexpr_intptr_constexpr_int31_0(state_, 32);
    tmp45 = CodeStubAssembler(state_).LoadReference<BuiltinPtr>(CodeStubAssembler::Reference{p_sortState, tmp44});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1353);
    ca_.Goto(&block23, phi_bb18_5);
  }

  TNode<Smi> phi_bb23_5;
  TNode<BoolT> tmp46;
  if (block23.is_used()) {
    ca_.Bind(&block23, &phi_bb23_5);
    tmp46 = CodeStubAssembler(state_).SmiLessThan(TNode<Smi>{phi_bb23_5}, TNode<Smi>{tmp43});
    ca_.Branch(tmp46, &block21, std::vector<Node*>{phi_bb23_5}, &block22, std::vector<Node*>{phi_bb23_5});
  }

  TNode<Smi> phi_bb21_5;
  TNode<Smi> tmp47;
  TNode<Smi> tmp48;
  TNode<Smi> tmp49;
  if (block21.is_used()) {
    ca_.Bind(&block21, &phi_bb21_5);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1354);
tmp47 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltinPointer(Builtins::CallableFor(ca_.isolate(),ExampleBuiltinForTorqueFunctionPointerType(7)).descriptor(), tmp45, p_context, p_sortState, phi_bb21_5));
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1353);
    tmp48 = FromConstexpr_Smi_constexpr_int31_0(state_, 1);
    tmp49 = CodeStubAssembler(state_).SmiAdd(TNode<Smi>{phi_bb21_5}, TNode<Smi>{tmp48});
    ca_.Goto(&block23, tmp49);
  }

  TNode<Smi> phi_bb22_5;
  if (block22.is_used()) {
    ca_.Bind(&block22, &phi_bb22_5);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1323);
    ca_.Goto(&block25);
  }

    ca_.Bind(&block25);
}

TF_BUILTIN(ArrayTimSort, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<SortState> parameter1 = UncheckedCast<SortState>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<Smi> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1360);
    tmp0 = CompactReceiverElementsIntoWorkArray_0(state_, TNode<Context>{parameter0}, TNode<SortState>{parameter1});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1361);
    ArrayTimSortImpl_0(state_, TNode<Context>{parameter0}, TNode<SortState>{parameter1}, TNode<Smi>{tmp0});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1366);
    compiler::CodeAssemblerLabel label1(&ca_);
    Method_SortState_CheckAccessor_0(state_, TNode<Context>{parameter0}, TNode<SortState>{parameter1}, &label1);
    ca_.Goto(&block3);
    if (label1.is_used()) {
      ca_.Bind(&label1);
      ca_.Goto(&block4);
    }
  }

  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1368);
    Method_SortState_ResetToGenericAccessor_0(state_, TNode<SortState>{parameter1});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1363);
    ca_.Goto(&block1);
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1367);
    ca_.Goto(&block1);
  }

  TNode<Smi> tmp2;
  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1371);
    CopyWorkArrayToReceiver_0(state_, TNode<Context>{parameter0}, TNode<SortState>{parameter1}, TNode<Smi>{tmp0});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1372);
    tmp2 = kSuccess_0(state_);
    CodeStubAssembler(state_).Return(tmp2);
  }
}

TF_BUILTIN(ArrayPrototypeSort, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  Node* argc = Parameter(Descriptor::kJSActualArgumentsCount);
  TNode<IntPtrT> arguments_length(ChangeInt32ToIntPtr(UncheckedCast<Int32T>(argc)));
  TNode<RawPtrT> arguments_frame = UncheckedCast<RawPtrT>(LoadFramePointer());
  TorqueStructArguments torque_arguments(GetFrameArguments(arguments_frame, arguments_length));
  CodeStubArguments arguments(this, torque_arguments);
  TNode<NativeContext> parameter0 = UncheckedCast<NativeContext>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<Object> parameter1 = arguments.GetReceiver();
USE(parameter1);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<Object> tmp1;
  TNode<HeapObject> tmp2;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1381);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp1 = CodeStubAssembler(state_).GetArgumentValue(TorqueStructArguments{TNode<RawPtrT>{torque_arguments.frame}, TNode<RawPtrT>{torque_arguments.base}, TNode<IntPtrT>{torque_arguments.length}}, TNode<IntPtrT>{tmp0});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1382);
    compiler::CodeAssemblerLabel label3(&ca_);
    tmp2 = Cast_Undefined_OR_CallableApiObject_OR_CallableJSProxy_OR_JSFunction_OR_JSBoundFunction_1(state_, TNode<Context>{parameter0}, TNode<Object>{tmp1}, &label3);
    ca_.Goto(&block3);
    if (label3.is_used()) {
      ca_.Bind(&label3);
      ca_.Goto(&block4);
    }
  }

  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1383);
    CodeStubAssembler(state_).ThrowTypeError(TNode<Context>{parameter0}, MessageTemplate::kBadSortComparisonFunction, TNode<Object>{tmp1});
  }

  TNode<JSReceiver> tmp4;
  TNode<Number> tmp5;
  TNode<Number> tmp6;
  TNode<BoolT> tmp7;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1386);
    tmp4 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kToObject, parameter0, parameter1));
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1389);
    tmp5 = GetLengthProperty_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp4});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1391);
    tmp6 = FromConstexpr_Number_constexpr_int31_0(state_, 2);
    tmp7 = NumberIsLessThan_0(state_, TNode<Number>{tmp5}, TNode<Number>{tmp6});
    ca_.Branch(tmp7, &block5, std::vector<Node*>{}, &block6, std::vector<Node*>{});
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    arguments.PopAndReturn(parameter1);
  }

  TNode<SortState> tmp8;
  TNode<Object> tmp9;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1393);
    tmp8 = NewSortState_0(state_, TNode<Context>{parameter0}, TNode<JSReceiver>{tmp4}, TNode<HeapObject>{tmp2}, TNode<Number>{tmp5});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1394);
    tmp9 = CodeStubAssembler(state_).CallBuiltin(Builtins::kArrayTimSort, parameter0, tmp8);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1396);
    arguments.PopAndReturn(parameter1);
  }
}

TNode<JSReceiver> LoadSortStateReceiver_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<JSReceiver> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 43);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp1 = CodeStubAssembler(state_).LoadReference<JSReceiver>(CodeStubAssembler::Reference{p_o, tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<JSReceiver>{tmp1};
}

void StoreSortStateReceiver_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o, TNode<JSReceiver> p_v) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 43);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    CodeStubAssembler(state_).StoreReference<JSReceiver>(CodeStubAssembler::Reference{p_o, tmp0}, p_v);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
}

TNode<Map> LoadSortStateInitialReceiverMap_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<Map> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 48);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp1 = CodeStubAssembler(state_).LoadReference<Map>(CodeStubAssembler::Reference{p_o, tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<Map>{tmp1};
}

void StoreSortStateInitialReceiverMap_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o, TNode<Map> p_v) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 48);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    CodeStubAssembler(state_).StoreReference<Map>(CodeStubAssembler::Reference{p_o, tmp0}, p_v);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
}

TNode<Number> LoadSortStateInitialReceiverLength_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<Number> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 49);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 12);
    tmp1 = CodeStubAssembler(state_).LoadReference<Number>(CodeStubAssembler::Reference{p_o, tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<Number>{tmp1};
}

void StoreSortStateInitialReceiverLength_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o, TNode<Number> p_v) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 49);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 12);
    CodeStubAssembler(state_).StoreReference<Number>(CodeStubAssembler::Reference{p_o, tmp0}, p_v);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
}

TNode<HeapObject> LoadSortStateUserCmpFn_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<HeapObject> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 52);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 16);
    tmp1 = CodeStubAssembler(state_).LoadReference<HeapObject>(CodeStubAssembler::Reference{p_o, tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<HeapObject>{tmp1};
}

void StoreSortStateUserCmpFn_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o, TNode<HeapObject> p_v) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 52);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 16);
    CodeStubAssembler(state_).StoreReference<HeapObject>(CodeStubAssembler::Reference{p_o, tmp0}, p_v);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
}

TNode<BuiltinPtr> LoadSortStateSortComparePtr_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<BuiltinPtr> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 57);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 20);
    tmp1 = CodeStubAssembler(state_).LoadReference<BuiltinPtr>(CodeStubAssembler::Reference{p_o, tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<BuiltinPtr>{tmp1};
}

void StoreSortStateSortComparePtr_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o, TNode<BuiltinPtr> p_v) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 57);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 20);
    CodeStubAssembler(state_).StoreReference<BuiltinPtr>(CodeStubAssembler::Reference{p_o, tmp0}, p_v);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
}

TNode<BuiltinPtr> LoadSortStateLoadFn_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<BuiltinPtr> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 62);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 24);
    tmp1 = CodeStubAssembler(state_).LoadReference<BuiltinPtr>(CodeStubAssembler::Reference{p_o, tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<BuiltinPtr>{tmp1};
}

void StoreSortStateLoadFn_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o, TNode<BuiltinPtr> p_v) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 62);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 24);
    CodeStubAssembler(state_).StoreReference<BuiltinPtr>(CodeStubAssembler::Reference{p_o, tmp0}, p_v);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
}

TNode<BuiltinPtr> LoadSortStateStoreFn_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<BuiltinPtr> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 63);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 28);
    tmp1 = CodeStubAssembler(state_).LoadReference<BuiltinPtr>(CodeStubAssembler::Reference{p_o, tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<BuiltinPtr>{tmp1};
}

void StoreSortStateStoreFn_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o, TNode<BuiltinPtr> p_v) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 63);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 28);
    CodeStubAssembler(state_).StoreReference<BuiltinPtr>(CodeStubAssembler::Reference{p_o, tmp0}, p_v);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
}

TNode<BuiltinPtr> LoadSortStateDeleteFn_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<BuiltinPtr> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 64);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 32);
    tmp1 = CodeStubAssembler(state_).LoadReference<BuiltinPtr>(CodeStubAssembler::Reference{p_o, tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<BuiltinPtr>{tmp1};
}

void StoreSortStateDeleteFn_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o, TNode<BuiltinPtr> p_v) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 64);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 32);
    CodeStubAssembler(state_).StoreReference<BuiltinPtr>(CodeStubAssembler::Reference{p_o, tmp0}, p_v);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
}

TNode<BuiltinPtr> LoadSortStateCanUseSameAccessorFn_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<BuiltinPtr> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 65);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 36);
    tmp1 = CodeStubAssembler(state_).LoadReference<BuiltinPtr>(CodeStubAssembler::Reference{p_o, tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<BuiltinPtr>{tmp1};
}

void StoreSortStateCanUseSameAccessorFn_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o, TNode<BuiltinPtr> p_v) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 65);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 36);
    CodeStubAssembler(state_).StoreReference<BuiltinPtr>(CodeStubAssembler::Reference{p_o, tmp0}, p_v);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
}

TNode<Smi> LoadSortStateMinGallop_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<Smi> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 70);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 40);
    tmp1 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_o, tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<Smi>{tmp1};
}

void StoreSortStateMinGallop_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o, TNode<Smi> p_v) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 70);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 40);
    CodeStubAssembler(state_).StoreReference<Smi>(CodeStubAssembler::Reference{p_o, tmp0}, p_v);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
}

TNode<Smi> LoadSortStatePendingRunsSize_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<Smi> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 82);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 44);
    tmp1 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_o, tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<Smi>{tmp1};
}

void StoreSortStatePendingRunsSize_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o, TNode<Smi> p_v) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 82);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 44);
    CodeStubAssembler(state_).StoreReference<Smi>(CodeStubAssembler::Reference{p_o, tmp0}, p_v);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
}

TNode<FixedArray> LoadSortStatePendingRuns_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<FixedArray> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 83);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 48);
    tmp1 = CodeStubAssembler(state_).LoadReference<FixedArray>(CodeStubAssembler::Reference{p_o, tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<FixedArray>{tmp1};
}

void StoreSortStatePendingRuns_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o, TNode<FixedArray> p_v) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 83);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 48);
    CodeStubAssembler(state_).StoreReference<FixedArray>(CodeStubAssembler::Reference{p_o, tmp0}, p_v);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
}

TNode<FixedArray> LoadSortStateWorkArray_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<FixedArray> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 88);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 52);
    tmp1 = CodeStubAssembler(state_).LoadReference<FixedArray>(CodeStubAssembler::Reference{p_o, tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<FixedArray>{tmp1};
}

void StoreSortStateWorkArray_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o, TNode<FixedArray> p_v) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 88);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 52);
    CodeStubAssembler(state_).StoreReference<FixedArray>(CodeStubAssembler::Reference{p_o, tmp0}, p_v);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
}

TNode<FixedArray> LoadSortStateTempArray_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<FixedArray> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 91);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 56);
    tmp1 = CodeStubAssembler(state_).LoadReference<FixedArray>(CodeStubAssembler::Reference{p_o, tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<FixedArray>{tmp1};
}

void StoreSortStateTempArray_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o, TNode<FixedArray> p_v) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 91);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 56);
    CodeStubAssembler(state_).StoreReference<FixedArray>(CodeStubAssembler::Reference{p_o, tmp0}, p_v);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
}

TNode<Smi> LoadSortStateSortLength_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<Smi> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 94);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 60);
    tmp1 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_o, tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<Smi>{tmp1};
}

void StoreSortStateSortLength_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o, TNode<Smi> p_v) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 94);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 60);
    CodeStubAssembler(state_).StoreReference<Smi>(CodeStubAssembler::Reference{p_o, tmp0}, p_v);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
}

TNode<Smi> LoadSortStateNumberOfUndefined_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<Smi> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 98);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 64);
    tmp1 = CodeStubAssembler(state_).LoadReference<Smi>(CodeStubAssembler::Reference{p_o, tmp0});
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<Smi>{tmp1};
}

void StoreSortStateNumberOfUndefined_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_o, TNode<Smi> p_v) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 98);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 64);
    CodeStubAssembler(state_).StoreReference<Smi>(CodeStubAssembler::Reference{p_o, tmp0}, p_v);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
}

TNode<Number> Method_SortState_Compare_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<SortState> p_this, TNode<Object> p_x, TNode<Object> p_y) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<BuiltinPtr> tmp1;
  TNode<IntPtrT> tmp2;
  TNode<HeapObject> tmp3;
  TNode<Number> tmp4;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 19);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 20);
    tmp1 = CodeStubAssembler(state_).LoadReference<BuiltinPtr>(CodeStubAssembler::Reference{p_this, tmp0});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 20);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 16);
    tmp3 = CodeStubAssembler(state_).LoadReference<HeapObject>(CodeStubAssembler::Reference{p_this, tmp2});
tmp4 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltinPointer(Builtins::CallableFor(ca_.isolate(),ExampleBuiltinForTorqueFunctionPointerType(9)).descriptor(), tmp1, p_context, tmp3, p_x, p_y));
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 18);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
  return TNode<Number>{tmp4};
}

void Method_SortState_CheckAccessor_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<SortState> p_this, compiler::CodeAssemblerLabel* label_Bailout) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<JSReceiver> tmp1;
  TNode<BoolT> tmp2;
  TNode<BoolT> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 24);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp1 = CodeStubAssembler(state_).LoadReference<JSReceiver>(CodeStubAssembler::Reference{p_this, tmp0});
    tmp2 = IsFastJSArray_0(state_, TNode<Object>{tmp1}, TNode<Context>{p_context});
    tmp3 = CodeStubAssembler(state_).Word32BinaryNot(TNode<BoolT>{tmp2});
    ca_.Branch(tmp3, &block3, std::vector<Node*>{}, &block4, std::vector<Node*>{});
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.Goto(&block1);
  }

  TNode<IntPtrT> tmp4;
  TNode<BuiltinPtr> tmp5;
  TNode<IntPtrT> tmp6;
  TNode<JSReceiver> tmp7;
  TNode<IntPtrT> tmp8;
  TNode<Map> tmp9;
  TNode<IntPtrT> tmp10;
  TNode<Number> tmp11;
  TNode<Oddball> tmp12;
  TNode<BoolT> tmp13;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 27);
    tmp4 = FromConstexpr_intptr_constexpr_int31_0(state_, 36);
    tmp5 = CodeStubAssembler(state_).LoadReference<BuiltinPtr>(CodeStubAssembler::Reference{p_this, tmp4});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 30);
    tmp6 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp7 = CodeStubAssembler(state_).LoadReference<JSReceiver>(CodeStubAssembler::Reference{p_this, tmp6});
    tmp8 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp9 = CodeStubAssembler(state_).LoadReference<Map>(CodeStubAssembler::Reference{p_this, tmp8});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 31);
    tmp10 = FromConstexpr_intptr_constexpr_int31_0(state_, 12);
    tmp11 = CodeStubAssembler(state_).LoadReference<Number>(CodeStubAssembler::Reference{p_this, tmp10});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 29);
tmp12 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltinPointer(Builtins::CallableFor(ca_.isolate(),ExampleBuiltinForTorqueFunctionPointerType(8)).descriptor(), tmp5, p_context, tmp7, tmp9, tmp11));
    tmp13 = CodeStubAssembler(state_).IsFalse(TNode<Oddball>{tmp12});
    ca_.Branch(tmp13, &block5, std::vector<Node*>{}, &block6, std::vector<Node*>{});
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 32);
    ca_.Goto(&block1);
  }

  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 23);
    ca_.Goto(&block7);
  }

  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.Goto(label_Bailout);
  }

    ca_.Bind(&block7);
}

void Method_SortState_ResetToGenericAccessor_0(compiler::CodeAssemblerState* state_, TNode<SortState> p_this) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<IntPtrT> tmp1;
  TNode<IntPtrT> tmp2;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 37);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 24);
    CodeStubAssembler(state_).StoreReference<BuiltinPtr>(CodeStubAssembler::Reference{p_this, tmp0}, ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kLoad_GenericElementsAccessor_0)));
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 38);
    tmp1 = FromConstexpr_intptr_constexpr_int31_0(state_, 28);
    CodeStubAssembler(state_).StoreReference<BuiltinPtr>(CodeStubAssembler::Reference{p_this, tmp1}, ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kStore_GenericElementsAccessor_0)));
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 39);
    tmp2 = FromConstexpr_intptr_constexpr_int31_0(state_, 32);
    CodeStubAssembler(state_).StoreReference<BuiltinPtr>(CodeStubAssembler::Reference{p_this, tmp2}, ca_.UncheckedCast<BuiltinPtr>(ca_.SmiConstant(Builtins::kDelete_GenericElementsAccessor_0)));
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 36);
    ca_.Goto(&block2);
  }

    ca_.Bind(&block2);
}

TNode<SortState> DownCastForTorqueClass_SortState_0(compiler::CodeAssemblerState* state_, TNode<HeapObject> p_o, compiler::CodeAssemblerLabel* label_CastError) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block9(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block10(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block11(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block12(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block8(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block13(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block14(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block15(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<Map> tmp1;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 203);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp1 = CodeStubAssembler(state_).LoadReference<Map>(CodeStubAssembler::Reference{p_o, tmp0});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 206);
    if (((CodeStubAssembler(state_).ConstexprInt31Equal(static_cast<InstanceType>(179), static_cast<InstanceType>(179))))) {
      ca_.Goto(&block3);
    } else {
      ca_.Goto(&block4);
    }
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 207);
    if ((CodeStubAssembler(state_).ClassHasMapConstant<SortState>())) {
      ca_.Goto(&block6);
    } else {
      ca_.Goto(&block7);
    }
  }

  TNode<Map> tmp2;
  TNode<BoolT> tmp3;
  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 208);
    tmp2 = CodeStubAssembler(state_).GetClassMapConstant<SortState>();
    tmp3 = CodeStubAssembler(state_).TaggedNotEqual(TNode<HeapObject>{tmp1}, TNode<HeapObject>{tmp2});
    ca_.Branch(tmp3, &block9, std::vector<Node*>{}, &block10, std::vector<Node*>{});
  }

  if (block9.is_used()) {
    ca_.Bind(&block9);
    ca_.Goto(&block1);
  }

  if (block10.is_used()) {
    ca_.Bind(&block10);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 207);
    ca_.Goto(&block8);
  }

  TNode<IntPtrT> tmp4;
  TNode<Uint16T> tmp5;
  TNode<Uint32T> tmp6;
  TNode<BoolT> tmp7;
  if (block7.is_used()) {
    ca_.Bind(&block7);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 210);
    tmp4 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp5 = CodeStubAssembler(state_).LoadReference<Uint16T>(CodeStubAssembler::Reference{tmp1, tmp4});
    tmp6 = FromConstexpr_uint32_constexpr_uint32_0(state_, static_cast<InstanceType>(179));
    tmp7 = CodeStubAssembler(state_).Word32NotEqual(TNode<Uint32T>{tmp5}, TNode<Uint32T>{tmp6});
    ca_.Branch(tmp7, &block11, std::vector<Node*>{}, &block12, std::vector<Node*>{});
  }

  if (block11.is_used()) {
    ca_.Bind(&block11);
    ca_.Goto(&block1);
  }

  if (block12.is_used()) {
    ca_.Bind(&block12);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 207);
    ca_.Goto(&block8);
  }

  if (block8.is_used()) {
    ca_.Bind(&block8);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 206);
    ca_.Goto(&block5);
  }

  TNode<Int32T> tmp8;
  TNode<IntPtrT> tmp9;
  TNode<Uint16T> tmp10;
  TNode<Uint16T> tmp11;
  TNode<Int32T> tmp12;
  TNode<Uint16T> tmp13;
  TNode<Uint16T> tmp14;
  TNode<Int32T> tmp15;
  TNode<Int32T> tmp16;
  TNode<Uint32T> tmp17;
  TNode<Uint32T> tmp18;
  TNode<BoolT> tmp19;
  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 213);
    tmp8 = FromConstexpr_int32_constexpr_int32_0(state_, (CodeStubAssembler(state_).ConstexprUint32Sub(static_cast<InstanceType>(179), static_cast<InstanceType>(179))));
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 214);
    tmp9 = FromConstexpr_intptr_constexpr_int31_0(state_, 8);
    tmp10 = CodeStubAssembler(state_).LoadReference<Uint16T>(CodeStubAssembler::Reference{tmp1, tmp9});
    tmp11 = Convert_uint16_InstanceType_0(state_, TNode<Uint16T>{tmp10});
    tmp12 = Convert_int32_uint16_0(state_, TNode<Uint16T>{tmp11});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 216);
    tmp13 = FromConstexpr_InstanceType_constexpr_InstanceType_0(state_, static_cast<InstanceType>(179));
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 215);
    tmp14 = Convert_uint16_InstanceType_0(state_, TNode<Uint16T>{tmp13});
    tmp15 = Convert_int32_uint16_0(state_, TNode<Uint16T>{tmp14});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 214);
    tmp16 = CodeStubAssembler(state_).Int32Sub(TNode<Int32T>{tmp12}, TNode<Int32T>{tmp15});
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 217);
    tmp17 = CodeStubAssembler(state_).Unsigned(TNode<Int32T>{tmp16});
    tmp18 = CodeStubAssembler(state_).Unsigned(TNode<Int32T>{tmp8});
    tmp19 = CodeStubAssembler(state_).Uint32GreaterThan(TNode<Uint32T>{tmp17}, TNode<Uint32T>{tmp18});
    ca_.Branch(tmp19, &block13, std::vector<Node*>{}, &block14, std::vector<Node*>{});
  }

  if (block13.is_used()) {
    ca_.Bind(&block13);
    ca_.Goto(&block1);
  }

  if (block14.is_used()) {
    ca_.Bind(&block14);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 206);
    ca_.Goto(&block5);
  }

  TNode<SortState> tmp20;
  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../src/builtins/torque-internal.tq", 219);
    tmp20 = TORQUE_CAST(TNode<HeapObject>{p_o});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 17);
    ca_.Goto(&block15);
  }

  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.Goto(label_CastError);
  }

    ca_.Bind(&block15);
  return TNode<SortState>{tmp20};
}

TF_BUILTIN(CanUseSameAccessor_FastDoubleElements_0, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<JSReceiver> parameter1 = UncheckedCast<JSReceiver>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<Map> parameter2 = UncheckedCast<Map>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  TNode<Number> parameter3 = UncheckedCast<Number>(Parameter(Descriptor::ParameterIndex<2>()));
  USE(parameter3);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<Map> tmp1;
  TNode<BoolT> tmp2;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 384);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp1 = CodeStubAssembler(state_).LoadReference<Map>(CodeStubAssembler::Reference{parameter1, tmp0});
    tmp2 = CodeStubAssembler(state_).TaggedNotEqual(TNode<HeapObject>{tmp1}, TNode<HeapObject>{parameter2});
    ca_.Branch(tmp2, &block1, std::vector<Node*>{}, &block2, std::vector<Node*>{});
  }

  TNode<Oddball> tmp3;
  if (block1.is_used()) {
    ca_.Bind(&block1);
    tmp3 = False_0(state_);
    CodeStubAssembler(state_).Return(tmp3);
  }

  TNode<BoolT> tmp4;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 386);
    tmp4 = CodeStubAssembler(state_).TaggedIsSmi(TNode<Object>{parameter3});
    ca_.Branch(tmp4, &block3, std::vector<Node*>{}, &block4, std::vector<Node*>{});
  }

  if (block4.is_used()) {
    ca_.Bind(&block4);
    CodeStubAssembler(state_).FailAssert("Torque assert 'TaggedIsSmi(initialReceiverLength)' failed", "third_party/v8/builtins/array-sort.tq", 386);
  }

  TNode<JSArray> tmp5;
  TNode<Smi> tmp6;
  TNode<IntPtrT> tmp7;
  TNode<Number> tmp8;
  TNode<Smi> tmp9;
  TNode<BoolT> tmp10;
  TNode<Oddball> tmp11;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 387);
    tmp5 = UnsafeCast_JSArray_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter1});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 388);
    tmp6 = UnsafeCast_Smi_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter3});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 390);
    tmp7 = FromConstexpr_intptr_constexpr_int31_0(state_, 12);
    tmp8 = CodeStubAssembler(state_).LoadReference<Number>(CodeStubAssembler::Reference{tmp5, tmp7});
    tmp9 = UnsafeCast_Smi_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp8});
    tmp10 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp9}, TNode<Smi>{tmp6});
    tmp11 = CodeStubAssembler(state_).SelectBooleanConstant(TNode<BoolT>{tmp10});
    CodeStubAssembler(state_).Return(tmp11);
  }
}

TF_BUILTIN(CanUseSameAccessor_FastSmiElements_0, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<JSReceiver> parameter1 = UncheckedCast<JSReceiver>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<Map> parameter2 = UncheckedCast<Map>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  TNode<Number> parameter3 = UncheckedCast<Number>(Parameter(Descriptor::ParameterIndex<2>()));
  USE(parameter3);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<Map> tmp1;
  TNode<BoolT> tmp2;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 384);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp1 = CodeStubAssembler(state_).LoadReference<Map>(CodeStubAssembler::Reference{parameter1, tmp0});
    tmp2 = CodeStubAssembler(state_).TaggedNotEqual(TNode<HeapObject>{tmp1}, TNode<HeapObject>{parameter2});
    ca_.Branch(tmp2, &block1, std::vector<Node*>{}, &block2, std::vector<Node*>{});
  }

  TNode<Oddball> tmp3;
  if (block1.is_used()) {
    ca_.Bind(&block1);
    tmp3 = False_0(state_);
    CodeStubAssembler(state_).Return(tmp3);
  }

  TNode<BoolT> tmp4;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 386);
    tmp4 = CodeStubAssembler(state_).TaggedIsSmi(TNode<Object>{parameter3});
    ca_.Branch(tmp4, &block3, std::vector<Node*>{}, &block4, std::vector<Node*>{});
  }

  if (block4.is_used()) {
    ca_.Bind(&block4);
    CodeStubAssembler(state_).FailAssert("Torque assert 'TaggedIsSmi(initialReceiverLength)' failed", "third_party/v8/builtins/array-sort.tq", 386);
  }

  TNode<JSArray> tmp5;
  TNode<Smi> tmp6;
  TNode<IntPtrT> tmp7;
  TNode<Number> tmp8;
  TNode<Smi> tmp9;
  TNode<BoolT> tmp10;
  TNode<Oddball> tmp11;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 387);
    tmp5 = UnsafeCast_JSArray_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter1});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 388);
    tmp6 = UnsafeCast_Smi_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter3});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 390);
    tmp7 = FromConstexpr_intptr_constexpr_int31_0(state_, 12);
    tmp8 = CodeStubAssembler(state_).LoadReference<Number>(CodeStubAssembler::Reference{tmp5, tmp7});
    tmp9 = UnsafeCast_Smi_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp8});
    tmp10 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp9}, TNode<Smi>{tmp6});
    tmp11 = CodeStubAssembler(state_).SelectBooleanConstant(TNode<BoolT>{tmp10});
    CodeStubAssembler(state_).Return(tmp11);
  }
}

TF_BUILTIN(CanUseSameAccessor_FastObjectElements_0, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<JSReceiver> parameter1 = UncheckedCast<JSReceiver>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<Map> parameter2 = UncheckedCast<Map>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  TNode<Number> parameter3 = UncheckedCast<Number>(Parameter(Descriptor::ParameterIndex<2>()));
  USE(parameter3);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<Map> tmp1;
  TNode<BoolT> tmp2;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 384);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 0);
    tmp1 = CodeStubAssembler(state_).LoadReference<Map>(CodeStubAssembler::Reference{parameter1, tmp0});
    tmp2 = CodeStubAssembler(state_).TaggedNotEqual(TNode<HeapObject>{tmp1}, TNode<HeapObject>{parameter2});
    ca_.Branch(tmp2, &block1, std::vector<Node*>{}, &block2, std::vector<Node*>{});
  }

  TNode<Oddball> tmp3;
  if (block1.is_used()) {
    ca_.Bind(&block1);
    tmp3 = False_0(state_);
    CodeStubAssembler(state_).Return(tmp3);
  }

  TNode<BoolT> tmp4;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 386);
    tmp4 = CodeStubAssembler(state_).TaggedIsSmi(TNode<Object>{parameter3});
    ca_.Branch(tmp4, &block3, std::vector<Node*>{}, &block4, std::vector<Node*>{});
  }

  if (block4.is_used()) {
    ca_.Bind(&block4);
    CodeStubAssembler(state_).FailAssert("Torque assert 'TaggedIsSmi(initialReceiverLength)' failed", "third_party/v8/builtins/array-sort.tq", 386);
  }

  TNode<JSArray> tmp5;
  TNode<Smi> tmp6;
  TNode<IntPtrT> tmp7;
  TNode<Number> tmp8;
  TNode<Smi> tmp9;
  TNode<BoolT> tmp10;
  TNode<Oddball> tmp11;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 387);
    tmp5 = UnsafeCast_JSArray_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter1});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 388);
    tmp6 = UnsafeCast_Smi_0(state_, TNode<Context>{parameter0}, TNode<Object>{parameter3});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 390);
    tmp7 = FromConstexpr_intptr_constexpr_int31_0(state_, 12);
    tmp8 = CodeStubAssembler(state_).LoadReference<Number>(CodeStubAssembler::Reference{tmp5, tmp7});
    tmp9 = UnsafeCast_Smi_0(state_, TNode<Context>{parameter0}, TNode<Object>{tmp8});
    tmp10 = CodeStubAssembler(state_).SmiEqual(TNode<Smi>{tmp9}, TNode<Smi>{tmp6});
    tmp11 = CodeStubAssembler(state_).SelectBooleanConstant(TNode<BoolT>{tmp10});
    CodeStubAssembler(state_).Return(tmp11);
  }
}

TF_BUILTIN(Load_GenericElementsAccessor_0, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<SortState> parameter1 = UncheckedCast<SortState>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<Smi> parameter2 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<JSReceiver> tmp1;
  TNode<Oddball> tmp2;
  TNode<BoolT> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 237);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp1 = CodeStubAssembler(state_).LoadReference<JSReceiver>(CodeStubAssembler::Reference{parameter1, tmp0});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 238);
    tmp2 = CodeStubAssembler(state_).HasProperty_Inline(TNode<Context>{parameter0}, TNode<JSReceiver>{tmp1}, TNode<Object>{parameter2});
    tmp3 = CodeStubAssembler(state_).IsFalse(TNode<Oddball>{tmp2});
    ca_.Branch(tmp3, &block1, std::vector<Node*>{}, &block2, std::vector<Node*>{});
  }

  TNode<Oddball> tmp4;
  if (block1.is_used()) {
    ca_.Bind(&block1);
    tmp4 = TheHole_0(state_);
    CodeStubAssembler(state_).Return(tmp4);
  }

  TNode<Object> tmp5;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 239);
    tmp5 = CodeStubAssembler(state_).GetProperty(TNode<Context>{parameter0}, TNode<Object>{tmp1}, TNode<Object>{parameter2});
    CodeStubAssembler(state_).Return(tmp5);
  }
}

TF_BUILTIN(Store_GenericElementsAccessor_0, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<SortState> parameter1 = UncheckedCast<SortState>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<Smi> parameter2 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  TNode<Object> parameter3 = UncheckedCast<Object>(Parameter(Descriptor::ParameterIndex<2>()));
  USE(parameter3);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<JSReceiver> tmp1;
  TNode<Object> tmp2;
  TNode<Smi> tmp3;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 270);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp1 = CodeStubAssembler(state_).LoadReference<JSReceiver>(CodeStubAssembler::Reference{parameter1, tmp0});
    tmp2 = CodeStubAssembler(state_).CallBuiltin(Builtins::kSetProperty, parameter0, tmp1, parameter2, parameter3);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 271);
    tmp3 = kSuccess_0(state_);
    CodeStubAssembler(state_).Return(tmp3);
  }
}

TF_BUILTIN(Delete_GenericElementsAccessor_0, CodeStubAssembler) {
  compiler::CodeAssemblerState* state_ = state();  compiler::CodeAssembler ca_(state());
  TNode<Context> parameter0 = UncheckedCast<Context>(Parameter(Descriptor::kContext));
  USE(parameter0);
  TNode<SortState> parameter1 = UncheckedCast<SortState>(Parameter(Descriptor::ParameterIndex<0>()));
  USE(parameter1);
  TNode<Smi> parameter2 = UncheckedCast<Smi>(Parameter(Descriptor::ParameterIndex<1>()));
  USE(parameter2);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<IntPtrT> tmp0;
  TNode<JSReceiver> tmp1;
  TNode<Smi> tmp2;
  TNode<Oddball> tmp3;
  TNode<Smi> tmp4;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 303);
    tmp0 = FromConstexpr_intptr_constexpr_int31_0(state_, 4);
    tmp1 = CodeStubAssembler(state_).LoadReference<JSReceiver>(CodeStubAssembler::Reference{parameter1, tmp0});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 304);
    tmp2 = FromConstexpr_LanguageModeSmi_constexpr_LanguageMode_0(state_, LanguageMode::kStrict);
    tmp3 = TORQUE_CAST(CodeStubAssembler(state_).CallBuiltin(Builtins::kDeleteProperty, parameter0, tmp1, parameter2, tmp2));
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 305);
    tmp4 = kSuccess_0(state_);
    CodeStubAssembler(state_).Return(tmp4);
  }
}

TNode<JSObject> UnsafeCast_JSObject_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<Object> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<BoolT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/cast.tq", 622);
    tmp0 = Is_JSObject_Object_0(state_, TNode<Context>{p_context}, TNode<Object>{p_o});
    ca_.Branch(tmp0, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    CodeStubAssembler(state_).FailAssert("Torque assert 'Is<A>(o)' failed", "src/builtins/cast.tq", 622);
  }

  TNode<JSObject> tmp1;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../src/builtins/cast.tq", 623);
    tmp1 = TORQUE_CAST(TNode<Object>{p_o});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 244);
    ca_.Goto(&block4);
  }

    ca_.Bind(&block4);
  return TNode<JSObject>{tmp1};
}

TNode<HeapNumber> UnsafeCast_HeapNumber_0(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<Object> p_o) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block2(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<BoolT> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/cast.tq", 622);
    tmp0 = Is_HeapNumber_Object_0(state_, TNode<Context>{p_context}, TNode<Object>{p_o});
    ca_.Branch(tmp0, &block2, std::vector<Node*>{}, &block3, std::vector<Node*>{});
  }

  if (block3.is_used()) {
    ca_.Bind(&block3);
    CodeStubAssembler(state_).FailAssert("Torque assert 'Is<A>(o)' failed", "src/builtins/cast.tq", 622);
  }

  TNode<HeapNumber> tmp1;
  if (block2.is_used()) {
    ca_.Bind(&block2);
    ca_.SetSourcePosition("../../src/builtins/cast.tq", 623);
    tmp1 = TORQUE_CAST(TNode<Object>{p_o});
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 295);
    ca_.Goto(&block4);
  }

    ca_.Bind(&block4);
  return TNode<HeapNumber>{tmp1};
}

TNode<HeapObject> Cast_Undefined_OR_CallableApiObject_OR_CallableJSProxy_OR_JSFunction_OR_JSBoundFunction_1(compiler::CodeAssemblerState* state_, TNode<Context> p_context, TNode<Object> p_o, compiler::CodeAssemblerLabel* label_CastError) {
  compiler::CodeAssembler ca_(state_);
  compiler::CodeAssemblerParameterizedLabel<> block0(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block4(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block3(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block6(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block5(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block1(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
  compiler::CodeAssemblerParameterizedLabel<> block7(&ca_, compiler::CodeAssemblerLabel::kNonDeferred);
    ca_.Goto(&block0);

  TNode<HeapObject> tmp0;
  if (block0.is_used()) {
    ca_.Bind(&block0);
    ca_.SetSourcePosition("../../src/builtins/cast.tq", 157);
    compiler::CodeAssemblerLabel label1(&ca_);
    tmp0 = CodeStubAssembler(state_).TaggedToHeapObject(TNode<Object>{p_o}, &label1);
    ca_.Goto(&block3);
    if (label1.is_used()) {
      ca_.Bind(&label1);
      ca_.Goto(&block4);
    }
  }

  if (block4.is_used()) {
    ca_.Bind(&block4);
    ca_.Goto(&block1);
  }

  TNode<HeapObject> tmp2;
  if (block3.is_used()) {
    ca_.Bind(&block3);
    compiler::CodeAssemblerLabel label3(&ca_);
    tmp2 = Cast_Undefined_OR_CallableApiObject_OR_CallableJSProxy_OR_JSFunction_OR_JSBoundFunction_0(state_, TNode<HeapObject>{tmp0}, &label3);
    ca_.Goto(&block5);
    if (label3.is_used()) {
      ca_.Bind(&label3);
      ca_.Goto(&block6);
    }
  }

  if (block6.is_used()) {
    ca_.Bind(&block6);
    ca_.Goto(&block1);
  }

  if (block5.is_used()) {
    ca_.Bind(&block5);
    ca_.SetSourcePosition("../../third_party/v8/builtins/array-sort.tq", 1382);
    ca_.Goto(&block7);
  }

  if (block1.is_used()) {
    ca_.Bind(&block1);
    ca_.Goto(label_CastError);
  }

    ca_.Bind(&block7);
  return TNode<HeapObject>{tmp2};
}

}  // namespace internal
}  // namespace v8

